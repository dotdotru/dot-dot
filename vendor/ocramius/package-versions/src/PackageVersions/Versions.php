<?php

declare(strict_types=1);

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    public const ROOT_PACKAGE_NAME = 'dot/dot';
    public const VERSIONS          = array (
  'beberlei/doctrineextensions' => 'v1.2.6@af72c4a136b744f1268ca8bb4da47a2f8af78f86',
  'behat/transliterator' => 'v1.3.0@3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
  'cocur/slugify' => 'v4.0.0@3f1ffc300f164f23abe8b64ffb3f92d35cec8307',
  'doctrine/annotations' => 'v1.8.0@904dca4eb10715b92569fbcd79e201d5c349b6bc',
  'doctrine/cache' => '1.10.0@382e7f4db9a12dc6c19431743a2b096041bcdd62',
  'doctrine/collections' => '1.6.4@6b1e4b2b66f6d6e49983cebfe23a21b7ccc5b0d7',
  'doctrine/common' => '2.12.0@2053eafdf60c2172ee1373d1b9289ba1db7f1fc6',
  'doctrine/dbal' => 'v2.10.1@c2b8e6e82732a64ecde1cddf9e1e06cb8556e3d8',
  'doctrine/doctrine-bundle' => '2.0.7@6926771140ee87a823c3b2c72602de9dda4490d3',
  'doctrine/doctrine-migrations-bundle' => '2.1.2@856437e8de96a70233e1f0cc2352fc8dd15a899d',
  'doctrine/event-manager' => '1.1.0@629572819973f13486371cb611386eb17851e85c',
  'doctrine/inflector' => '1.3.1@ec3a55242203ffa6a4b27c58176da97ff0a7aec1',
  'doctrine/instantiator' => '1.3.0@ae466f726242e637cebdd526a7d991b9433bacf1',
  'doctrine/lexer' => '1.2.0@5242d66dbeb21a30dd8a3e66bf7a73b66e05e1f6',
  'doctrine/migrations' => '2.2.1@a3987131febeb0e9acb3c47ab0df0af004588934',
  'doctrine/orm' => 'v2.7.0@4d763ca4c925f647b248b9fa01b5f47aa3685d62',
  'doctrine/persistence' => '1.3.6@5dd3ac5eebef2d0b074daa4440bb18f93132dee4',
  'doctrine/reflection' => 'v1.1.0@bc420ead87fdfe08c03ecc3549db603a45b06d4c',
  'egulias/email-validator' => '2.1.15@e834eea5306d85d67de5a05db5882911d5b29357',
  'evenement/evenement' => 'v3.0.1@531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
  'exsyst/swagger' => 'v0.4.1@a02984db5edacdce2b4e09dae5ba8fe17a0e449e',
  'fresh/doctrine-enum-bundle' => 'v6.6.2@0ca57c8ba1f710a4823ed8535493049347a8d85a',
  'friendsofsymfony/ckeditor-bundle' => '2.2.0@7e1cfe2a83faba0be02661d44289d35e940bb5ea',
  'friendsofsymfony/oauth-server-bundle' => '1.6.2@fcaa25cc49474bdb0db7894f880976fe76ffed23',
  'friendsofsymfony/oauth2-php' => '1.2.3@a41fef63f81ef2ef632350a6c7dc66d15baa9240',
  'friendsofsymfony/user-bundle' => 'v2.1.2@1049935edd24ec305cc6cfde1875372fa9600446',
  'gedmo/doctrine-extensions' => 'v2.4.39@c549b40bff560380c53812283d25ce42ee0992e4',
  'gietos/dadata' => '1.1.15@f39e4fde48bac4aa0437ffac42b9f486516bad95',
  'guzzlehttp/guzzle' => '6.5.2@43ece0e75098b7ecd8d13918293029e555a50f82',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.6.1@239400de7a173fe9901b9ac7c06497751f00727a',
  'hoa/compiler' => '3.17.08.08@aa09caf0bf28adae6654ca6ee415ee2f522672de',
  'hoa/consistency' => '1.17.05.02@fd7d0adc82410507f332516faf655b6ed22e4c2f',
  'hoa/event' => '1.17.01.13@6c0060dced212ffa3af0e34bb46624f990b29c54',
  'hoa/exception' => '1.17.01.16@091727d46420a3d7468ef0595651488bfc3a458f',
  'hoa/file' => '1.17.07.11@35cb979b779bc54918d2f9a4e02ed6c7a1fa67ca',
  'hoa/iterator' => '2.17.01.10@d1120ba09cb4ccd049c86d10058ab94af245f0cc',
  'hoa/math' => '1.17.05.16@7150785d30f5d565704912116a462e9f5bc83a0c',
  'hoa/protocol' => '1.17.01.14@5c2cf972151c45f373230da170ea015deecf19e2',
  'hoa/regex' => '1.17.01.13@7e263a61b6fb45c1d03d8e5ef77668518abd5bec',
  'hoa/stream' => '1.17.02.21@3293cfffca2de10525df51436adf88a559151d82',
  'hoa/ustring' => '4.17.01.16@e6326e2739178799b1fe3fdd92029f9517fa17a0',
  'hoa/visitor' => '2.17.01.16@c18fe1cbac98ae449e0d56e87469103ba08f224a',
  'hoa/zformat' => '1.17.01.10@522c381a2a075d4b9dbb42eb4592dd09520e4ac2',
  'imagine/imagine' => '1.2.3@cb2361e5bb4410b681462d8e4f912bc5dabf84ab',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'jhaoda/smscenter' => 'v2.0.1@849317eb5096f25093c1fd8464e370bc957c810b',
  'jms/metadata' => '2.1.0@8d8958103485c2cbdd9a9684c3869312ebdaf73a',
  'jms/serializer' => '3.4.0@e2d3c49d9322a08ee32221a5623c898160dada79',
  'jms/serializer-bundle' => '3.5.0@5793ec59b2243365a625c0fd78415732097c11e8',
  'knplabs/gaufrette' => 'v0.9.0@786247eba04d4693e88a80ca9fdabb634675dcac',
  'knplabs/knp-menu' => '2.6.0@b6aade272c345b6fbd07fce5929a761cba0909b8',
  'knplabs/knp-menu-bundle' => 'v2.3.0@78b0cebf8e1490f12ba6555511282954de83f627',
  'knplabs/knp-snappy' => 'v1.2.1@7bac60fb729147b7ccd8532c07df3f52a4afa8a4',
  'knplabs/knp-snappy-bundle' => 'v1.7.0@717185618888b03daf85a54897a8a11e655a3eeb',
  'kriswallsmith/buzz' => 'v0.16.1@4977b7d44dbef49cdc641f14be6512fdcfe32f12',
  'markbaker/complex' => '1.4.7@1ea674a8308baf547cbcbd30c5fcd6d301b7c000',
  'markbaker/matrix' => '1.2.0@5348c5a67e3b75cd209d70103f916a93b1f1ed21',
  'monolog/monolog' => '2.0.2@c861fcba2ca29404dc9e617eedd9eff4616986b8',
  'nelmio/api-doc-bundle' => 'v3.5.0@f596adfb4d16e65d1a1941f907092a119d4c76cb',
  'ocramius/package-versions' => '1.4.2@44af6f3a2e2e04f2af46bcb302ad9600cba41c7d',
  'ocramius/proxy-manager' => '2.2.3@4d154742e31c35137d5374c998e8f86b54db2e2f',
  'paragonie/random_compat' => 'v2.0.18@0a58ef6e3146256cc3dc7cc393927bcc7d1b72db',
  'phpdocumentor/reflection-common' => '2.0.0@63a995caa1ca9e5590304cd845c15ad6d482a62a',
  'phpdocumentor/reflection-docblock' => '4.3.4@da3fd972d6bafd628114f7e7e036f45944b62e9c',
  'phpdocumentor/type-resolver' => '1.0.1@2e32a6d48972b2c1976ed5d8967145b6cec4a4a9',
  'phpoffice/phpspreadsheet' => '1.10.1@1648dc9ebef6ebe0c5a172e16cf66732918416e0',
  'presta/sitemap-bundle' => 'v1.7.2@04387d54f922386d2ff80a7945df3cdf98ffabde',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/http-client' => '1.0.0@496a823ef742b632934724bf769560c2a5c7c44e',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/log' => '1.1.2@446d54b4cb6bf489fc9d75f55843658e6f25d801',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'ralouphie/getallheaders' => '3.0.3@120b605dfeb996808c31b6477290a714d356e822',
  'ralouphie/mimey' => '2.1.0@8f74e6da73f9df7bd965e4e123f3d8fb9acb89ba',
  'ratchet/pawl' => 'v0.3.4@3a7d5b78e0deaec82f42513a4a3193a8eb12feb1',
  'ratchet/rfc6455' => 'v0.2.6@879e48c840f8dbc296d68d6a5030673df79bd916',
  'react/cache' => 'v1.0.0@aa10d63a1b40a36a486bdf527f28bac607ee6466',
  'react/dns' => 'v1.2.0@a214d90c2884dac18d0cac6176202f247b66d762',
  'react/event-loop' => 'v1.1.1@6d24de090cd59cfc830263cfba965be77b563c13',
  'react/promise' => 'v2.7.1@31ffa96f8d2ed0341a57848cbb84d88b89dd664d',
  'react/promise-timer' => 'v1.5.1@35fb910604fd86b00023fc5cda477c8074ad0abc',
  'react/socket' => 'v1.3.0@10f0629ec83ea0fa22597f348623f554227e3ca0',
  'react/stream' => 'v1.1.0@50426855f7a77ddf43b9266c22320df5bf6c6ce6',
  'sensio/framework-extra-bundle' => 'v5.5.3@98f0807137b13d0acfdf3c255a731516e97015de',
  'sonata-project/admin-bundle' => '3.58.0@9f7d8505c1cbea7a94563fe5f3eff94b66daf9c4',
  'sonata-project/block-bundle' => '3.18.3@3ed57dafb9ffee9541125fb2c38956b95021ba0d',
  'sonata-project/cache' => '2.0.1@2e2fdabf40bfe566c68406aae238a721cddabf74',
  'sonata-project/core-bundle' => '3.18.0@8e8089961a1185c0a40796054c09a89fd9f9cba9',
  'sonata-project/datagrid-bundle' => '2.5.0@3d67da1be314130c8b419a881582c93bedf809fa',
  'sonata-project/doctrine-extensions' => '1.5.1@0259461e9bb63723a16f1282f129cccb7856afc5',
  'sonata-project/doctrine-orm-admin-bundle' => '3.13.0@36e01d4874193767764986a0ef1507dd65f90971',
  'sonata-project/easy-extends-bundle' => '2.5.0@c62fb4f7e74f7fc5f32f122ffa8131d7cf05a1db',
  'sonata-project/exporter' => '2.0.1@9e84fd03ef0c696b9b4e9617b73cb6defaf3b476',
  'sonata-project/media-bundle' => '3.23.0@61726309c72d52def86800d4039607e42730b28c',
  'sonata-project/seo-bundle' => '2.10.0@6d346f5fe90661aaea2a2445ae2168a982d30df7',
  'sonata-project/translation-bundle' => '2.4.2@b9e66dbe65db28747146b01da6187a233a81a8f5',
  'sonata-project/user-bundle' => '4.5.1@6f67594a79c121a41277b7a32393abd179dbcb89',
  'stof/doctrine-extensions-bundle' => 'v1.3.0@46db71ec7ffee9122eca3cdddd4ef8d84bae269c',
  'swiftmailer/swiftmailer' => 'v6.2.3@149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
  'symfony/asset' => 'v4.4.4@2c67c89d064bfb689ea6bc41217c87100bb94c17',
  'symfony/cache' => 'v4.4.4@0198a01c8d918d6d717f96dfdcba9582bc5f6468',
  'symfony/cache-contracts' => 'v2.0.1@23ed8bfc1a4115feca942cb5f1aacdf3dcdf3c16',
  'symfony/config' => 'v4.4.4@4d3979f54472637169080f802dc82197e21fdcce',
  'symfony/console' => 'v4.4.4@f512001679f37e6a042b51897ed24a2f05eba656',
  'symfony/css-selector' => 'v4.4.4@a167b1860995b926d279f9bb538f873e3bfa3465',
  'symfony/debug' => 'v4.4.4@20236471058bbaa9907382500fc14005c84601f0',
  'symfony/dependency-injection' => 'v4.4.4@ec60a7d12f5e8ab0f99456adce724717d9c1784a',
  'symfony/doctrine-bridge' => 'v4.4.4@b8d43116f0e5abef4b7abcbeec81c3b9328ca7b7',
  'symfony/dom-crawler' => 'v4.4.4@b66fe8ccc850ea11c4cd31677706c1219768bea1',
  'symfony/error-handler' => 'v4.4.4@d2721499ffcaf246a743e01cdf6696d3d5dd74c1',
  'symfony/event-dispatcher' => 'v4.4.4@9e3de195e5bc301704dd6915df55892f6dfc208b',
  'symfony/event-dispatcher-contracts' => 'v1.1.7@c43ab685673fb6c8d84220c77897b1d6cdbe1d18',
  'symfony/expression-language' => 'v4.4.4@8b145496d7e2e7103b1a1b8f1fce81c6e084b380',
  'symfony/filesystem' => 'v4.4.4@266c9540b475f26122b61ef8b23dd9198f5d1cfd',
  'symfony/finder' => 'v4.4.4@3a50be43515590faf812fbd7708200aabc327ec3',
  'symfony/flex' => 'v1.6.2@e4f5a2653ca503782a31486198bd1dd1c9a47f83',
  'symfony/form' => 'v4.4.4@442d561fa10841183f94909830d9d27bd9cf7f77',
  'symfony/framework-bundle' => 'v4.4.4@afc96daad6049cbed34312b34005d33fc670d022',
  'symfony/http-foundation' => 'v4.4.4@491a20dfa87e0b3990170593bc2de0bb34d828a5',
  'symfony/http-kernel' => 'v4.4.4@62116a9c8fb15faabb158ad9cb785c353c2572e5',
  'symfony/inflector' => 'v5.0.4@e375603b6bd12e8e3aec3fc1b640ac18a4ef4cb2',
  'symfony/intl' => 'v5.0.4@519bcb27ea53835c1e8e7f7c8a799c867d570156',
  'symfony/lock' => 'v4.4.4@f5e49508fa495b400b475d43d850a3c33d1aa899',
  'symfony/mime' => 'v5.0.4@2a3c7fee1f1a0961fa9cf360d5da553d05095e59',
  'symfony/monolog-bridge' => 'v5.0.4@3e081905b32e24742c16f7bb2cf0cd182598a32d',
  'symfony/monolog-bundle' => 'v3.5.0@dd80460fcfe1fa2050a7103ad818e9d0686ce6fd',
  'symfony/options-resolver' => 'v4.4.4@9a02d6662660fe7bfadad63b5f0b0718d4c8b6b0',
  'symfony/orm-pack' => 'v1.0.7@c57f5e05232ca40626eb9fa52a32bc8565e9231c',
  'symfony/polyfill-ctype' => 'v1.13.1@f8f0b461be3385e56d6de3dbb5a0df24c0c275e3',
  'symfony/polyfill-iconv' => 'v1.13.1@a019efccc03f1a335af6b4f20c30f5ea8060be36',
  'symfony/polyfill-intl-icu' => 'v1.13.1@b3dffd68afa61ca70f2327f2dd9bbeb6aa53d70b',
  'symfony/polyfill-intl-idn' => 'v1.13.1@6f9c239e61e1b0c9229a28ff89a812dc449c3d46',
  'symfony/polyfill-mbstring' => 'v1.13.1@7b4aab9743c30be783b73de055d24a39cf4b954f',
  'symfony/polyfill-php72' => 'v1.13.1@66fea50f6cb37a35eea048d75a7d99a45b586038',
  'symfony/polyfill-php73' => 'v1.13.1@4b0e2222c55a25b4541305a053013d5647d3a25f',
  'symfony/process' => 'v5.0.4@f9ffd870f5ac01abec7b2b5e15f904ca9400ecd1',
  'symfony/property-access' => 'v4.4.4@090b4bc92ded1ec512f7e2ed1691210769dffdb3',
  'symfony/property-info' => 'v4.4.4@e6355ba81c738be31c3c3b3cd7929963f98da576',
  'symfony/routing' => 'v4.4.4@7bf4e38573728e317b926ca4482ad30470d0e86a',
  'symfony/security-acl' => 'v3.0.4@dc8f10b3bda34e9ddcad49edc7accf61f31fce43',
  'symfony/security-bundle' => 'v4.4.4@7829cc34b8231cb8d10621cdf27d04bfdc600334',
  'symfony/security-core' => 'v4.4.4@d2550b4ecd63f612763e0af2cbcb1a69a700fe99',
  'symfony/security-csrf' => 'v4.4.4@da4664d94164e2b50ce75f2453724c6c33222505',
  'symfony/security-guard' => 'v4.4.4@f457f2d6d7392259b1ede1d036a26b6c1fa20202',
  'symfony/security-http' => 'v4.4.4@736d09554f78f3444f5aeed3d18a928c7a8a53fb',
  'symfony/service-contracts' => 'v2.0.1@144c5e51266b281231e947b51223ba14acf1a749',
  'symfony/stopwatch' => 'v5.0.4@5d9add8034135b9a5f7b101d1e42c797e7f053e4',
  'symfony/swiftmailer-bundle' => 'v3.4.0@553d2474288349faed873da8ab7c1551a00d26ae',
  'symfony/templating' => 'v4.4.4@9995a4f65149d5ab7f0d9cca6d88136ae8bfaa72',
  'symfony/translation' => 'v4.4.4@f5d2ac46930238b30a9c2f1b17c905f3697d808c',
  'symfony/translation-contracts' => 'v2.0.1@8cc682ac458d75557203b2f2f14b0b92e1c744ed',
  'symfony/twig-bridge' => 'v4.4.4@d5f3e83e2cc2fcdd60c351b5be1beb9533cf698c',
  'symfony/twig-bundle' => 'v4.4.4@d3e3e46e9e683e946746219570299ba07506260a',
  'symfony/validator' => 'v4.4.4@eb3e15de5c63873ca6e2a88b56a029f7be4c5953',
  'symfony/var-dumper' => 'v5.0.4@923591cfb78a935f0c98968fedfad05bfda9d01f',
  'symfony/var-exporter' => 'v5.0.4@960f9ac0fdbd642461ed29d7717aeb2a94d428b9',
  'symfony/webpack-encore-bundle' => 'v1.7.3@5c0f659eceae87271cce54bbdfb05ed8ec9007bd',
  'symfony/yaml' => 'v4.4.4@cd014e425b3668220adb865f53bff64b3ad21767',
  'twig/extensions' => 'v1.5.4@57873c8b0c1be51caa47df2cdb824490beb16202',
  'twig/twig' => 'v2.12.3@97b6311585cae66a26833b14b33785f5797f7d39',
  'voronkovich/sberbank-acquiring-client' => 'v2.4@3a74b71e8ca25d3fa0d3c59ad72bf6552dc297fd',
  'vsavritsky/settingsbundle' => 'v1.1@8947344d675e49b119fa9e239999f89c73891f36',
  'wapmorgan/morphos' => '3.2.20@7212132f9094db12008a23503a9e6d3dea7fc3bc',
  'webmozart/assert' => '1.6.0@573381c0a64f155a0d9a23f4b0c797194805b925',
  'zendframework/zend-code' => '3.4.1@268040548f92c2bfcba164421c1add2ba43abaaa',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'zircote/swagger-php' => '2.0.15@5fd9439cfb76713925e23f206e9db4bf35784683',
  'doctrine/data-fixtures' => '1.4.2@39e9777c9089351a468f780b01cffa3cb0a42907',
  'doctrine/doctrine-fixtures-bundle' => '3.3.0@8f07fcfdac7f3591f3c4bf13a50cbae05f65ed70',
  'nikic/php-parser' => 'v4.3.0@9a9981c347c5c49d6dfe5cf826bb882b824080dc',
  'symfony/dotenv' => 'v4.4.4@b74a1638f53e3c65e4bbfc2a03c23fdc400fd243',
  'symfony/maker-bundle' => 'v1.14.3@c864e7f9b8d1e1f5f60acc3beda11299f637aded',
  'symfony/profiler-pack' => 'v1.0.4@99c4370632c2a59bb0444852f92140074ef02209',
  'symfony/web-profiler-bundle' => 'v5.0.4@8f4831567fc39bbe42af415a14a6039621349787',
  'symfony/web-server-bundle' => 'v4.4.4@92a37564d8577f01a21e7a77dab2f4fcad32f4ba',
  'dot/dot' => 'dev-master@b6d1f719c0092f29e87275194a35e53103461ae7',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException If a version cannot be located.
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
