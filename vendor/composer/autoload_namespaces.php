<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'SMSCenter\\' => array($vendorDir . '/jhaoda/smscenter/lib'),
    'ProxyManager\\' => array($vendorDir . '/ocramius/proxy-manager/src'),
    'Gaufrette' => array($vendorDir . '/knplabs/gaufrette/src'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
);
