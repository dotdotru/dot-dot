require("jquery");
require('../libs/input-mask/jquery.maskedinput.min.js');
require('../libs/swiper-slider/swiper.jquery.min.js');
require('../libs/magnific-popup/jquery.magnific-popup.min.js');
require('../libs/waypoint/jquery.waypoints.min.js');
require('../libs/waypoint/sticky.min.js');
require('../libs/waypoint/inview.js');
require('moment');
require('../libs/bootstrap-datepicker/bootstrap-datetimepicker.min.js');
require('../libs/dropzone/dropzone.js');
require('../libs/scrollbar/jquery.scrollbar.min.js');
require('../libs/d3/d3.v4.min.js');
require('./components/common.js');
// require('./components/site/block/subscribe.js');
require('../libs/fontawesome.js');
require('../libs/vue/vue-the-mask.js');
require('./components/vueconfig.js');


require('./components/site/auction/auction.js');
//require('./components/carrier.order.js');
require('./components/site/default/news.js');
//require('./components/overload.js');
require('./components/site/personal/profile/profile.js');
require('./components/site/personal/profile/changePassword.js');
require('./components/site/miles/auction.js');
require('./components/site/personal/authtoken.js');

require('./components/site/block/stock.js');
require('./components/site/default/calc.js');
require('./components/site/register/register.js');
require('./components/site/auth/auth.js');
require('./components/site/reset/reset.js');
require('./components/site/personal/orders/orders.js');
require('./components/site/personal/batches/batches.js');
require('./components/site/track/track.js');
require('./components/site/block/subscrib.js');