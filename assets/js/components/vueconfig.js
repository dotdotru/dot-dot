import VeeValidate from 'vee-validate';
import Vue from 'vue';
import moment from 'moment';
import $ from 'jquery';
import * as d3 from "d3";

import VModal from 'vue-js-modal';
import { BTooltip } from 'bootstrap-vue';

Vue.component('b-tooltip', BTooltip);
Vue.use(VModal, { dialog: true });

import VueMask from 'v-mask';
Vue.use(VueMask);

import Vuebar from 'vuebar';
Vue.use(Vuebar);

import VueScrollTo from 'vue-scrollto';
Vue.use(VueScrollTo);


import addressRule from "./validation-rules/address";

const config = {
    errorBagName: 'errors', // change if property conflicts
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'ru',
    dictionary: null,
    strict: true,
    classes: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'change',
    inject: true,
    validity: false,
    aria: true,
    debug: true
};

VeeValidate.Validator.extend('nozero', {
    validate: function(value) {
        return value > 0;
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});


VeeValidate.Validator.extend('cyrillic', {
    validate: function(value) {
        return /^([А-Яа-я-— ]+)$/ig.test(value);
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});

VeeValidate.Validator.extend('cyrillicNumeric', {
    validate: function(value) {
        return /^([А-Яа-я-— 0-9]+)$/ig.test(value);
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});

VeeValidate.Validator.extend('phone', {
    validate: function(value) {
        value = value.replace(/\D+/g, '');
        return value.length == 11 || value[0] != '+';
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});



VeeValidate.Validator.extend('address', addressRule);


Vue.use(VeeValidate, config);

Vue.config.devtools = true;

Vue.directive('dadata', {
    params: ['a', 'dadatatype'],
    twoWay: true,

    paramWatchers: {
        'dadatatype': function (val, oldVal) {

        }
    },

    update: function (newValue, oldValue) {
        let url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/';

        return;

        if (this.dadatatype)
            url = url + this.dadatatype;
        else
            return;

        let $this = this;
        let dadatatype = this.dadatatype;

        /*console.log(this.el);

        this.el.autocomplete({
            source: (request, response) => {
                let query = request.term;
                let arAnswer = [];

                console.log('sdfsdf');

                $.ajax ({
                    data: JSON.stringify({query: query}),
                    headers: {
                        "Content-Type":"application/json",
                        "Accept":"application/json",
                        "Authorization": "Token c0405a37b0a4d76fc84d794ee0b1b93d9d7f2122",
                    },
                    type: 'POST',
                    url: url,
                    async: false,
                    success: function (data) {
                        if (data.suggestions)
                            arAnswer = data.suggestions;
                    },
                });

                response (arAnswer);
            },
            select: function( event, ui ) {
                if (dadatatype == 'fio' && sexfield) {
                    let tmp = ui.item.data.gender.substr(0,1).toLowerCase();
                    if (sexfiledkey != undefined) {
                        sexfield = sexfield+'.'+sexfiledkey;
                    }
                    $this.vm.$dispatch(sexfield, tmp);
                    $this.vm.$set(sexfield, tmp);
                }
            },
            change: function (event,ui) {
                let value = $(event.currentTarget).val();
                let result;
                let sex;
                if (dadatatype == 'fio' && sexfield) {

                    if (sexfiledkey != undefined)
                        sexfield = sexfield+'.'+sexfiledkey;

                    result=dadataRequest (value,'fio');
                    if (result.length===1) {
                        sex = result[0].data.gender.substr(0,1).toLowerCase();
                        $this.vm.$dispatch(sexfield, sex);
                        $this.vm.$set(sexfield, tmp);
                    }
                }

            },
            autoFocus: true,
        });*/
    }
});

Vue.directive("filter", {
    bind: function(el, binding) {
        let inputHandler = function(e) {
            let ch = String.fromCharCode(e.which);
            let re = new RegExp(binding.value);

            if([37, 39, 8, 0].indexOf(e.which) == -1) {
                if (!ch.match(re)) {
                    e.preventDefault();
                }
            }
        };
        el.addEventListener("keypress", inputHandler);
    },
});

Vue.directive("min", {
    bind: function(el, binding) {
        this.changeHandler = function(e) {
            let currentValue = $(e.target).val();

            if (currentValue < binding.value) {
                $(e.target).val(binding.value)
            }
        };
        el.addEventListener("change", this.changeHandler);
    },
    unbind: function(el) {
        el.removeEventListener("change", this.changeHandler);
    },
    changeHandler: null
});

Vue.directive("max", {
    bind: function(el, binding) {
        this.changeHandler = function(e) {
            let currentValue = $(e.target).val();

            if (currentValue > binding.value) {
                $(e.target).val(binding.value)
            }
        };
        el.addEventListener("change", this.changeHandler);
    },
    unbind: function(el) {
        el.removeEventListener("change", this.changeHandler);
    },
    changeHandler: null
});

Vue.directive('select2', {
    inserted(el) {
        $(el).on('select2:select', () => {
            const event = new Event('change', { bubbles: true, cancelable: true });
            el.dispatchEvent(event);
        });

        $(el).on('select2:unselect', () => {
            const event = new Event('change', {bubbles: true, cancelable: true})
            el.dispatchEvent(event)
        })
    },
});

let ff = {
    counterEvent(event, action) {
        if (typeof gtag === 'function') {
            gtag('event', event, {'event_category' : action});
            //ga('send', 'event', event, action);
        }
        if (typeof yaCounter50156956 === 'function') {
            yaCounter50156956.reachGoal(event);
        }
    },
    Person() {
        return {
            id: null,
            uridical: 0,
            issuance: 'passport',
            type: 'ooo',
            inn: '',
            contactName: '',
            contactPhone: '',
            contactEmail: '',
            series: '',
            number: '',
            companyName: '',
            code: '',
            isPayer: false
        }
    },
    Package() {
        return {
            type: '',
            anotherType: '',
            count: null,
            calcCount: 1,
            depth: null,
            width: null,
            height: null,
            weight: null,
            packing: null,
            packingPrice: 0,
            calculateWeight: null,
            hidden: 0,
            useSize: null,
            enterVolume: false,
        }
    },
    WmsPackage() {
        return {
            id: '',
            packageType: null,
            anotherType: '',
            count: null,
            depth: null,
            width: null,
            height: null,
            weight: null,
            packing: null,
            packingPrice: null,
            calculateWeight: null,
            hidden: 0,
            palletId: null,
            damaged: false,
        }
    },
    Shipping() {
        return {
            id: null,
            externalId: null,
            active: false,
            date: '',
            stock: null,
            address: '',
            countLoader: 0,
            hydroBoard: false,
            price: 0,
        }
    },
    mask() {
        return {
            phone: '+7 (###) ###-##-##',
            code: '#######',
            date: '##.##.####',
            five: '##### ##### ##### #####'
        }
    },
    filter() {
        return {
            cyrillic: '[А-Яа-я-— \\b]+',
            cyrillicNumeric: '[А-Яа-я-— 0-9\\b]+',
            numeric: '[0-9\\b]+',
            address: '[А-Яа-я-— 0-9,\\b]+',
            float: '[0-9,.\\b]+',
            int: '[0-9]+\\b',
        }
    },
    convertDate(date) {
        if (date) {
            let dateTime = new Date();
            if (date.date) {
                dateTime = new Date(String(date.date).replace(' ', 'T'));
            } else {
                dateTime = new Date(date.toString().replace(' ', 'T'));
            }
            return dateTime;
        }
    },
    formatDateTime(date) {
        let dateTime = this.convertDate(date);

        if (dateTime) {
            return moment(dateTime, 'YYYY-MM-DD HH:mm').format('DD.MM.YYYY HH:mm')
        }
        return '';
    },
    formatDate(date) {
        let dateTime = this.convertDate(date);

        if (dateTime) {
            return moment(dateTime, 'YYYY-MM-DD').format('DD.MM.YYYY')
        }
        return '';
    },
    formatTime(date) {
        let dateTime = this.convertDate(date);

        if (dateTime) {
            return moment(dateTime, 'YYYY-MM-DD HH:mm').format('HH:mm')
        }
        return '';
    },
    formatPhone(phone) {
        phone = phone.replace(/[A-z\(\)-+ ]/g, '')

        if (phone[0] == '7') {
            phone = phone.substr(1, phone.length)
        }

        phone = phone.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "($1) $2-$3-$4");

        if (phone[0] == '7') {
            phone = phone.substr(1, phone.length)
        }

        return '+7 ' + phone;
    },
    comparePosition(a, b) {
        if (a.position < b.position) {
            return -1;
        }
        if (a.position > b.position) {
            return 1;
        }

        return 0;
    },
    formatUserName(username) {
        username = username.replace('/[^0-9]/');
        username = username.replace(/\s+/g, '');
        username = username.replace(/[^a-zA-Z0-9+]/g, "");
        if (username.indexOf('+7') == 0) {
            username = username.substr(2);
        }
        if (username.indexOf('8') == 0) {
            username = username.substr(1);
        }
        return username;
    },
    normalizeLogin(login) {
        login = login.toString();
        login = login.replace('/[^0-9]/');
        login = login.replace(/\s+/g, '');
        login = login.replace(/[^a-zA-Z0-9+]/g, "");

        if (login.indexOf('+7') == 0) {
            login = login.substr(2);
        }
        if (login.indexOf('8') == 0) {
            login = login.substr(1);
        }

        return login;
    },
    initDatepicker(element, countHours) {
        if (!countHours) {
            countHours = 0;
        }

        let startDate = new Date();
        let endDate = new Date();
        endDate.setHours(endDate.getHours()+countHours);
        element.each(function (index) {
            let $this = $(this);
            $this.attr('id', 'flatpickr' +index);
            $('#flatpickr' +index).flatpickr({
                wrap: true,
                enableTime: true,
                time_24hr: true,
                disableMobile: "true",
                "locale": "ru",
                onChange: function(selectedDates, dateStr, instance) {
                    let date = new Date(selectedDates[0]);

                },
                onOpen : function(selectedDates, dateStr, instance) {
                    let startDate = new Date();
                    let endDate = new Date();
                    endDate.setHours(endDate.getHours()+countHours);

                    instance.setDate(startDate);
                    instance.set('minDate', startDate);
                    instance.set('maxDate', endDate);
                    instance.redraw();
                },
                minDate: startDate,
                maxDate: endDate,
                clickOpens: false
            });

        });
    },
    initDatepickerDefault(element) {
        element.each(function (index) {
            let $this = $(this);

            let id = 'flatpickr-default' +index;
            $this.attr('id', id);
            $('#'+id).flatpickr({
                wrap: true,
                disableMobile: "true",
                dateFormat: "d.m.Y",
                "locale": "ru",
                allowInput: true,
                clickOpens: false
            });
        });
    },
    initSelect() {
        $('.selectpicker').selectpicker();
    },
    openPopupButton(head, text, addClass, callback) {
        if (!text) {
            text = '';
        }

        $( "#popup" ).html(
            '<div class="h2">'+head+'</div><p>'+text+'</p><a href="#" class="back-link">Нет</a><a id="button" href="javascript:void(0)" class="btn-default">Подтверждаю</a>'
        )

        if (addClass) {
            $( "#popup" ).addClass(addClass);
        }

        if (callback) {
            $('body').off('click', '#button').on('click', '#button', function(){
                callback();
                return false;
            })
        }

        $('body').on('click', '.back-link', function(){
            closePopup();
            return false;
        })

        $.magnificPopup.open({
            items: {
                src: '#popup',
            },
            type: 'inline'
        });
    },
    openPopup(head, text, addClass) {
        if (!text) {
            text = '';
        }
        $( "#popup" ).html('<div class="h2">'+head+'</div><p>'+text+'</p>');
        if (addClass) {
            $( "#popup" ).addClass(addClass);
        }

        $.magnificPopup.open({
            items: {
                src: '#popup',
            },
            type: 'inline'
        });
    },
    openPopupUrl(url, addClass, callback, callbackClose) {
        if (!callback) {
            $( "#popup" ).html('');

            $( "#popup" ).load( url );

            if (addClass) {
                $( "#popup" ).addClass(addClass);
            }

            $.magnificPopup.open({
                items: {
                    src: '#popup',
                },
                type: 'inline',
            });

        } else {

            $.magnificPopup.open({
                type: 'ajax',
                items: {
                    src: url,
                },
                closeOnBgClick: false,
                callbacks: {
                    ajaxContentAdded: function() {
                        //$('.mfp-content').css('width', 'auto');
                        //$('.mfp-content').addClass('main-popup');
                        $('.mfp-content').addClass(addClass);
                        if (callback) {
                            callback();
                        }
                    },
                    close: function () {
                        if (callbackClose) {
                            callbackClose();
                        }
                        return false;
                    }
                }
            });

        }
    },
    closePopup(el) {
        $.magnificPopup.close();
    },
    d3graphUpdateDotRed(idChart, currentStep, show) {
        d3.select(idChart+' .current-circle')
            .attr("class", "");
        d3.select(idChart + ' #dpoint-' + currentStep).attr("class", "current-circle");

        d3.select(idChart+' .current-text-price').attr("class", "current-text-price");
        d3.select(idChart+' .current-text-quantity').attr("class", "current-text-quantity");

        if (show) {
            d3.select(idChart + ' #dpoint-' + currentStep).attr("class", "red current-circle");
            d3.select('.current-text-price').attr("class", "red current-text-price");
            d3.select('.current-text-quantity').attr("class", "red current-text-quantity");
        }
    },
    d3graphSmallInit(idChart, dataArray, currentStep, priceHighlighting) {
        if (!dataArray.length) {
            return;
        }

        // set the dimensions and margins of the graph
        let margin = {top: 10, right: 5, bottom: 10, left: 5},
            smallWidth = 426 - margin.left - margin.right,
            smallHeight = 85 - margin.top - margin.bottom;
        let bigWidth = 650 - margin.left - margin.right,
            bigHeight = 190 - margin.top - margin.bottom;

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            smallWidth = 355 - margin.left - margin.right;
            if ($(window).width() < 374) {
                smallWidth = 300 - margin.left - margin.right;
            }
            if ($(window).width() > 767) {
                smallWidth = 425 - margin.left - margin.right;
            }
        };

        // set the ranges
        let xSmall = d3.scaleBand()
            .range([0, smallWidth])
            .padding(0.035);
        let ySmall = d3.scaleLinear().range([smallHeight, 0]);

        // define the line
        let valueline = d3.line()
            .x(function(d) { return xSmall(d.price); })
            .y(function(d) { return ySmall(d.quantity); });

        let svgSmall = d3.select(idChart).select("svg");

        dataArray.forEach(function(d){
            d.quantity = +d.quantity;
            d.price = +d.price;
        });

        dataArray.columns = ["quantity", "price"];

        let data = dataArray.columns.slice(1).map(function() {
            return {
                values: dataArray.map(function(d) {
                    return {quantity: d.quantity, price: d.price};
                })
            };
        });

        xSmall.domain(dataArray.map(function(d) { return d.price; }));
        ySmall.domain([0, d3.max(dataArray, function(d) { return d.quantity; })]);

        let dataQty = parseFloat(dataArray[(currentStep)].quantity);
        let dataPrice = parseFloat(dataArray[(currentStep)].price);

        if (svgSmall.empty()) {
            svgSmall = d3.select(idChart).append("svg")
                .attr("width", smallWidth)
                .attr("height", smallHeight + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                    "translate(" + 0 + "," + margin.top + ")");
            svgSmall.selectAll(".bar")
                .data(dataArray)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("fill", "#444549")
                .attr("stroke-dasharray", xSmall.bandwidth() + 0.95 +", 500")
                .attr("shape-rendering", " geometricPrecision")
                .attr("stroke", "#ffffff")
                .attr("stroke-width", "2")
                .attr("stroke-dashoffset", "1")
                .attr("x", function(d) { return xSmall(d.price); })
                .attr("width", xSmall.bandwidth())
                .attr("y", function(d) { return ySmall(d.quantity); })
                .attr("height", function(d) { return smallHeight - ySmall(d.quantity); })
                .attr('id', function(d,i) {return "dpoint-" + i;});
        }

        $('.bar').each(function () {
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
            }
        })
        $('.current-circle').each(function () {
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
                $(this).attr('stroke', '#ef483e')
            }
        })

        this.d3graphUpdateDotRed('#chart', currentStep, priceHighlighting)
        this.d3graphUpdateDotRed('#chart-big', currentStep, priceHighlighting)
    },

    d3graphBigInit(idChart, dataArray, currentStep, priceHighlighting) {

        if (!dataArray.length) {
            return;
        }

        // set the dimensions and margins of the graph
        let margin = {top: 10, right: 10, bottom: 10, left: 0},
            bigWidth = 650 - margin.left - margin.right,
            bigHeight = 190 - margin.top - margin.bottom;

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            if ($(window).width() < 992) {
                bigWidth = 650 - margin.left - margin.right;
            };
            if ($(window).width() <= 767) {
                bigWidth = 310 - margin.left - margin.right;
                bigHeight = $(window).height() - 240;
            };

            if($(window).width() < 390){
                bigWidth = 270 - margin.left - margin.right;
            };
            if($(window).width() < 321){
                bigWidth = 200 - margin.left - margin.right;
            };
        };

        let xBig= d3.scaleBand()
            .range([0, bigWidth])
            .padding(0.03)
        let yBig = d3.scaleLinear().range([bigHeight, 0]);

        let valuelineBig = d3.line()
            .x(function(d) { return xBig(d.price); })
            .y(function(d) { return yBig(d.quantity); });

        let tooltipBottom = d3.select('.chart-popup .chart-wrapper')
            .append("div")
            .attr("class", "chart-tooltip-bottom")
            .style("display", 'none');
        let tooltipLeft = d3.select('.chart-popup .chart-wrapper')
            .append("div")
            .attr("class", "chart-tooltip-left")
            .style("display", 'none');

        // append the svg object to the div
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin

        let svgBig = d3.select(idChart).select("svg");

        if (svgBig.empty()) {
            let svgBig = d3.select(idChart).append("svg")
                .attr("width", bigWidth + margin.left + margin.right + 75)
                .attr("height", bigHeight + margin.top + margin.bottom + 20)
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + 65 +"," + margin.top + ")");
        }

        svgBig.selectAll('circle').remove();
        svgBig.selectAll('path').remove();
        svgBig.selectAll('.axis').remove();

        dataArray.forEach(function(d){
            d.quantity = +d.quantity;
            d.price = +d.price;
        });

        dataArray.columns = ["quantity", "price"];

        let data = dataArray.columns.slice(1).map(function() {
            return {
                values: dataArray.map(function(d) {
                    return {quantity: d.quantity, price: d.price};
                })
            };
        });

        xBig.domain(dataArray.map(function(d) { return d.price; }));
        yBig.domain([0, d3.max(dataArray, function(d) { return d.quantity; })]);

        let dataQty = parseFloat(dataArray[(currentStep)].quantity);
        let dataPrice = parseFloat(dataArray[(currentStep)].price);

        svgBig.append("g")
            .attr('class', 'x-axis')
            .attr("transform", "translate(0," + bigHeight + ")")
            .call(
                d3.axisBottom(xBig)
                    .ticks(dataArray.length)
                    .tickSize(11, 0, 0)
                    .tickFormat(d3.format(".2f"))
            );

        // Add the Y Axis
        svgBig.append("g")
            .attr('class', 'y-axis')
            .call(
                d3.axisLeft(yBig)
                    .ticks(dataArray.length)
                    .tickFormat(d3.format("d"))
            );
        svgBig.select('.y-axis .domain')
            .attr('d', 'M-0,170.5H0.5V0.5H-0');
        svgBig.select('.x-axis .domain')
            .attr('d', 'M0.5,0V0.5H640.5V0');


        svgBig.selectAll("rect")
            .data(dataArray)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("fill", "#444549")
            .attr("stroke-dasharray", xBig.bandwidth() + 0.5 +", 1000")
            //.attr("shape-rendering", "optimizeSpeed")
            .attr("stroke", "#ffffff")
            .attr("stroke-width", "2")
            .attr("stroke-dashoffset", "1")
            .attr("x", function(d) { return xBig(d.price); })
            .attr("width", xBig.bandwidth())
            .attr("y", function(d) { return yBig(d.quantity); })
            .attr("height", function(d) { return bigHeight - yBig(d.quantity); })
            .attr('id', function(d,i) {return "dpoint-" + i;})
            .on("mouseover", function(d, i) {
                //
                // Change dots on hover
                //
                d3.select(this)
                    .transition()
                    .duration(200)
                    .attr('fill', '#d8d8d8')
                    .attr('fill-opacity', '0.5');


                let w = $('.chart-tooltip').width();
                if ($(this).is('.current-circle')) {
                    $('.current-text-quantity').show();
                    $('.current-text-price').show();
                    tooltipLeft.style('display', 'none');
                    tooltipBottom.style('display', 'none');
                } else{
                    tooltipLeft.html('<span>' + d.quantity + '<span>')
                        .style('display', 'inline');
                    tooltipBottom.html('<span>' + parseFloat(d.price).toFixed(2) + '<span>')
                        .style('display', 'inline');
                    $('.current-text-quantity').hide();
                    $('.current-text-price').hide();
                    tooltipLeft
                        .style('left', 0)
                        .style('top', (yBig(d.quantity)) + 'px');
                    tooltipBottom
                        .style('bottom', 7 + 'px')
                        .style('left', (xBig(d.price)) + (xBig.bandwidth() /2) -11 + 'px');
                }


            })
            .on("mouseout", function(d) {
                //
                // Change dots back
                //
                d3.select(this)
                    .transition()
                    .duration(200)
                    .attr('fill', '#444549')
                    .attr('fill-opacity', '1');
                //
                // Remove vertical line
                //
                svgBig.selectAll('.vertical-marker-bottom').remove();
                svgBig.selectAll('.horizontal-marker-left').remove();

                //
                // Remove tooltip
                //
                tooltipLeft.style('display', 'none');
                tooltipBottom.style('display', 'none');
                $('.current-text-quantity').show();
                $('.current-text-price').show();
            })

        // Append text with current step values
        svgBig.selectAll('text').remove();
        svgBig.append('text')
            .attr('class', 'current-text-price')
            .attr('x', xBig(dataArray[currentStep].price) + (xBig.bandwidth() /2) -11)
            .attr('y', bigHeight + 23)
            .text(parseFloat(dataArray[currentStep].price).toFixed(2));
        svgBig.append('text')
            .attr('class', 'current-text-quantity')
            .attr('x', -42)
            .attr('y', yBig(dataArray[currentStep].quantity) + 5)
            .text(parseInt(dataArray[currentStep].quantity));
        if ($('.current-text-quantity').attr('y') >= bigHeight -20 ) {
            $('.chart-popup .chart-wrapper .chart .y-axis .tick:nth-child(2)').hide();
        }
        if ($('.current-text-quantity').attr('y') <= 10 ) {
            $('.chart-popup .chart-wrapper .chart .y-axis .tick:last-of-type').hide();
        }
        if ($('.current-text-price').attr('x') <= 0 ) {
            $('.chart-popup .chart-wrapper .chart .x-axis .tick:nth-child(2)').hide();
        }
        // Select current circle based on data attr in html

        $('.bar').each(function () {
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
            }
        })
        var zeroHeight = 0;
        $('.chart rect').each(function () {
            
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
            };
            if ($(this).attr('height') == 32.5) {
                zeroHeight = zeroHeight + 1
            }
                      
        });
        if ( zeroHeight == $('.chart rect').length){
            $('.chart rect').each(function () {
                $(this).attr('height', 1)        
                $(this).attr('y', smallHeight)        
            })
        };
        var zeroBigHeight = 0;
        $('#chart-big rect').each(function () {
            
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
            };
            if ($(this).attr('height') == 85) {
                zeroBigHeight = zeroBigHeight + 1
            }
                      
        });
        if ( zeroBigHeight == $('#chart-big rect').length){
            $('#chart-big rect').each(function () {
                $(this).attr('height', 1)        
                $(this).attr('y', bigHeight)        
            })
        };                  

        this.d3graphUpdateDotRed('#chart', currentStep, priceHighlighting)
        this.d3graphUpdateDotRed('#chart-big', currentStep, priceHighlighting)
    },

    updateGraph(idChartBig, idChartSmall, dataArray, currentStep, priceHighlighting) {
        if (!priceHighlighting) {
            priceHighlighting = false;
        }

        let margin = {top: 10, right: 10, bottom: 10, left: 0},
            bigWidth = 650 - margin.left - margin.right,
            bigHeight = 190 - margin.top - margin.bottom,
            smallWidth = 426 - margin.left - margin.right,
            smallHeight = 85 - margin.top - margin.bottom;
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            smallWidth = 355 - margin.left - margin.right;
            if ($(window).width() < 400) {
                smallWidth = 300 - margin.left - margin.right;
            }
            if ($(window).width() > 767) {
                smallWidth = 425 - margin.left - margin.right;
            }
            if ($(window).width() < 992) {
                bigWidth = 650 - margin.left - margin.right;
            };
            if ($(window).width() <= 767) {
                bigWidth = 310 - margin.left - margin.right;
                //bigHeight = $(window).height() - 240;
            };

            if($(window).width() < 390){
                bigWidth = 270 - margin.left - margin.right;
            };
            if($(window).width() < 321){
                bigWidth = 200 - margin.left - margin.right;
            };            
        };     
        let svgBig = d3.select(idChartBig)
            .select('svg')
            .attr("width", bigWidth + margin.left + margin.right + 75)
            .attr("height", bigHeight + margin.top + margin.bottom + 20)
            .select("g")
            .attr("transform",
                "translate(" + margin.left + 65 +"," + margin.top + ")");
        if ($(window).width() <= 767) {
            svgBig = d3.select(idChartBig)
            .select('svg')
            .select("g")
            .attr("transform",
                "translate(" + margin.left + 50 +"," + margin.top + ")");
        };
        if ($(window).width() <=374) {
            svgBig = d3.select(idChartBig)
            .select('svg')
            .select("g")
            .attr("transform",
                "translate(" + margin.left + 43 +"," + margin.top + ")");
        };        
        let svgSmall = d3.select(idChartSmall)
            .select('svg')
            .attr("width", smallWidth + margin.left + margin.right + 75)
            .attr("height", smallHeight + margin.top + margin.bottom)
            .select("g")
            .attr("transform",
                "translate(" + 0 +"," + margin.top + ")");

        // set the dimensions and margins of the graph
        let xBig= d3.scaleBand()
            .range([0, bigWidth])
            .padding(0.03)
        let yBig = d3.scaleLinear().range([bigHeight, 0]);
        let xSmall = d3.scaleBand()
            .range([0, smallWidth])
            .padding(0.035);
        let ySmall = d3.scaleLinear().range([smallHeight, 0]);


        let valuelineBig = d3.line()
            .x(function(d) { return xBig(d.price); })
            .y(function(d) { return yBig(d.quantity); });
        let valueline = d3.line()
            .x(function(d) { return xSmall(d.price); })
            .y(function(d) { return ySmall(d.quantity); });

        let tooltipBottom = d3.select('.chart-popup .chart-wrapper')
            .append("div")
            .attr("class", "chart-tooltip-bottom")
            .style("display", 'none');
        let tooltipLeft = d3.select('.chart-popup .chart-wrapper')
            .append("div")
            .attr("class", "chart-tooltip-left")
            .style("display", 'none');

        d3.select(idChartBig).selectAll('svg .bar').remove();
        d3.select(idChartBig).selectAll('.x-axis').remove();
        d3.select(idChartBig).selectAll('.y-axis').remove();
        d3.select(idChartBig).selectAll('text').remove();
        d3.select(idChartSmall).selectAll('svg .bar').remove();
        d3.select(idChartSmall).selectAll('.x-axis').remove();
        d3.select(idChartSmall).selectAll('.y-axis').remove();
        d3.select(idChartSmall).selectAll('text').remove();
        // append the svg object to the div
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin

        dataArray.forEach(function(d){
            d.quantity = +d.quantity;
            d.price = +d.price;
        });

        dataArray.columns = ["quantity", "price"];

        let data = dataArray.columns.slice(1).map(function() {
            return {
                values: dataArray.map(function(d) {
                    return {quantity: d.quantity, price: d.price};
                })
            };
        });

        xBig.domain(dataArray.map(function(d) { return d.price; }));
        yBig.domain([0, d3.max(dataArray, function(d) { return d.quantity; })]);

        let dataQty = parseFloat(dataArray[(currentStep)].quantity);
        let dataPrice = parseFloat(dataArray[(currentStep)].price);

        svgBig.append("g")
            .attr('class', 'x-axis')
            .attr("transform", "translate(0," + bigHeight + ")")
            .call(
                d3.axisBottom(xBig)
                    .ticks(dataArray.length)
                    .tickSize(11, 0, 0)
                    .tickFormat(d3.format(".2f"))
            );

        // Add the Y Axis
        svgBig.append("g")
            .attr('class', 'y-axis')
            .call(
                d3.axisLeft(yBig)
                    .ticks(dataArray.length)
                    .tickFormat(d3.format("d"))
            );
        svgBig.select('.y-axis .domain')
            .attr('d', 'M-0,170.5H0.5V0.5H-0');
        svgBig.select('.x-axis .domain')
            .attr('d', 'M0.5,0V0.5H640.5V0');
        if ($(window).width() <= 767) {
            svgBig.select('.x-axis .domain')
                .attr('d', 'M0.5,0V0.5H345.5V0');
        };

        let bars = svgBig.selectAll("rect")
            .remove()
            .exit()
            .data(dataArray)
        bars.enter()
            .append("rect")
            .attr("class", "bar")
            .attr("fill", "#444549")
            .attr("stroke-dasharray", xBig.bandwidth() + 0.5 +", 1000")
            //.attr("shape-rendering", "optimizeSpeed")
            .attr("stroke", "#ffffff")
            .attr("stroke-width", "2")
            .attr("stroke-dashoffset", "1")
            .attr("x", function(d) { return xBig(d.price); })
            .attr("width", xBig.bandwidth())
            .attr("y", function(d) { return yBig(d.quantity); })
            .attr("height", function(d) { return bigHeight - yBig(d.quantity); })
            .attr('id', function(d,i) {return "dpoint-" + i;})
            .on("mouseover", function(d, i) {
                //
                // Change dots on hover
                //
                d3.select(this)
                    .transition()
                    .duration(200)
                    .attr('fill', '#d8d8d8')
                    .attr('fill-opacity', '0.5');


                let w = $('.chart-tooltip').width();
                if ($(this).is('.current-circle')) {
                    $('.current-text-quantity').show();
                    $('.current-text-price').show();
                    tooltipLeft.style('display', 'none');
                    tooltipBottom.style('display', 'none');
                } else{
                    tooltipLeft.html('<span>' + d.quantity + '<span>')
                        .style('display', 'inline');
                    tooltipBottom.html('<span>' + parseFloat(d.price).toFixed(2) + '<span>')
                        .style('display', 'inline');
                    $('.current-text-quantity').hide();
                    $('.current-text-price').hide();
                    tooltipLeft
                        .style('left', 0)
                        .style('top', (yBig(d.quantity)) + 'px');
                    tooltipBottom
                        .style('bottom', 7 + 'px')
                        .style('left', (xBig(d.price)) + (xBig.bandwidth() /2) -11 + 'px');
                }


            })
            .on("mouseout", function(d) {
                //
                // Change dots back
                //
                d3.select(this)
                    .transition()
                    .duration(200)
                    .attr('fill', '#444549')
                    .attr('fill-opacity', '1');
                //
                // Remove vertical line
                //
                svgBig.selectAll('.vertical-marker-bottom').remove();
                svgBig.selectAll('.horizontal-marker-left').remove();

                //
                // Remove tooltip
                //
                tooltipLeft.style('display', 'none');
                tooltipBottom.style('display', 'none');
                $('.current-text-quantity').show();
                $('.current-text-price').show();
            })
            var zeroBigHeight = 0;
            $('#chart-big rect').each(function () {
                
                if ($(this).attr('height') == 0) {
                    $(this).attr('height', 1)
                };
                if ($(this).attr('height') == 85) {
                    zeroBigHeight = zeroBigHeight + 1
                }
                          
            });
            if ( zeroBigHeight == $('#chart-big rect').length){
                $('#chart-big rect').each(function () {
                    $(this).attr('height', 1)        
                    $(this).attr('y', bigHeight)        
                })
            };            
        // Append text with current step values
        svgBig.append('text')
            .attr('class', 'current-text-price')
            .attr('x', xBig(dataArray[currentStep].price) + (xBig.bandwidth() /2) -11)
            .attr('y', bigHeight + 23)
            .text(parseFloat(dataArray[currentStep].price).toFixed(2));
        svgBig.append('text')
            .attr('class', 'current-text-quantity')
            .attr('x', -42)
            .attr('y', yBig(dataArray[currentStep].quantity) + 5)
            .text(parseInt(dataArray[currentStep].quantity));
        if ($('.current-text-quantity').attr('y') >= bigHeight -20 ) {
            $('.chart-popup .chart-wrapper .chart .y-axis .tick:nth-child(2)').hide();
        }
        if ($('.current-text-quantity').attr('y') <= 10 ) {
            $('.chart-popup .chart-wrapper .chart .y-axis .tick:last-of-type').hide();
        }
        if ($('.current-text-price').attr('x') <= 0 ) {
            $('.chart-popup .chart-wrapper .chart .x-axis .tick:nth-child(2)').hide();
        }

        xSmall.domain(dataArray.map(function(d) { return d.price; }));
        ySmall.domain([0, d3.max(dataArray, function(d) { return d.quantity; })]);

        let barsSmall = svgSmall.selectAll("rect")
            .remove()
            .exit()
            .data(dataArray)
        barsSmall.enter()
            .append("rect")
            .attr("class", "bar")
            .attr("fill", "#444549")
            .attr("stroke-dasharray", xSmall.bandwidth() + 0.95 +", 500")
            .attr("shape-rendering", " geometricPrecision")
            .attr("stroke", "#ffffff")
            .attr("stroke-width", "2")
            .attr("stroke-dashoffset", "1")
            .attr("x", function(d) { return xSmall(d.price); })
            .attr("width", xSmall.bandwidth())
            .attr("y", function(d) { return ySmall(d.quantity); })
            .attr("height", function(d) { return smallHeight - ySmall(d.quantity); })
            .attr('id', function(d,i) {return "dpoint-" + i;});

        var zeroHeight = 0;
        $('.chart rect').each(function () {
            
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
            };
            if ($(this).attr('height') == 32.5) {
                zeroHeight = zeroHeight + 1
            }
                      
        });
        if ( zeroHeight == $('.chart rect').length){
            $('.chart rect').each(function () {
                $(this).attr('height', 1)        
                $(this).attr('y', smallHeight)        
            })
        };
        $('.current-circle').each(function () {
            if ($(this).attr('height') == 0) {
                $(this).attr('height', 1)
                $(this).attr('stroke', '#ef483e')
            }
        })

        this.d3graphUpdateDotRed('#chart', currentStep, priceHighlighting)
        this.d3graphUpdateDotRed('#chart-big', currentStep, priceHighlighting)
    },
    DriverDocument() {
        return {
            name: '',
            type: null,
            series: null,
            number: null,
            date: null,
            place: null,
        }
    },
    getEmptyDriver() {
        var DriverDocumentPassport = Object.assign({}, this.DriverDocument());
        DriverDocumentPassport.type = 'passport';
        DriverDocumentPassport.name = 'Паспорт водителя';

        var DriverDocumentLicense = Object.assign({}, this.DriverDocument());
        DriverDocumentLicense.type = 'license';
        DriverDocumentLicense.name = 'Водительское удостоверение';

        var Driver = {
            id: false,
            verified: false,
            name: null,
            inn: null,
            birthdate: null,
            phone: null,
            documents: [
                Object.assign({}, DriverDocumentPassport),
                Object.assign({}, DriverDocumentLicense),
            ]
        };

        return Driver;
    },
    UserObject() {
        return {
            id: null,
            username: '',
            firstname: '',
            series: '',
            number: '',
            passport: '',
            email: '',
            password: '',
            reenteredPassword: '',
            group: 'sender',
            subscribe: true,
        }
    },
    Organization() {
        return {
            id: null,
            uridical: 0,
            type: 'ooo',
            companyName: '',
            nds: 1,
            inn: '',
            kpp: '',
            ceo: '',
            address: '',
            contactName: '',
            contactPhone: '',
            rs: '',
            bik: '',
            drivers: [
                this.getEmptyDriver()
            ]
        }
    }
}


export default ff;
