import Swiper from 'swiper';

$(document).ready(function () {

    $('body').on('click', '.scb_service_item', function () {
        $.magnificPopup.close();
    })

    $('.js-open-order').on("click", function() {
        var toggleID = $(this).data('order-details');
        $('.order-details[data-order-details=' + toggleID + ']').toggle();
        $(this).toggleClass('active');
    });

    $('.consignment-list').each(function () {
        var $this = $(this);
        if ($this.find('li').length > 2) {
            $($this).scrollbar();
        }
    })
    $('.pallets-wrapper').each(function () {
        var $this = $(this);
        if ($this.find('.pallet').length > 4) {
            $($this).scrollbar();
        }
    })
    $('.pallet-id-wrapper').each(function () {
        $($(this)).scrollbar();
    })

    $('.scrollbar-inner').scrollbar();

    // Phone Input
    jQuery(function($){
        $(".phone-number").mask("ps (000) 000-00-00", {
            translation: {
                'p': {
                    pattern: /[+]/,
                    fallback: '+'
                },
                's': {
                    pattern: /[7]/,
                    fallback: '7'
                }
            }
        });

        //Default Action
        $(".tab_content").hide(); //Hide all content
        $("ul.tabs li:first").addClass("active").show().find("label input:radio").attr("checked",""); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content

        //On Click Event
        $("ul.tabs li").click(function() {

            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $("ul.tabs li").find("label input:radio").attr("checked","");
            $(this).addClass("active").find("label input:radio").attr("checked","checked");
            $(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("label input:radio").val(); //Find the href attribute value to identify the active tab + content
            $('#' + activeTab).fadeIn(); //Fade in the active ID content
            return false;

        });

        $('#trackNumber').on('keypress keyup', function () {
            if ($(this).val().length > 7) {
                $('#track-btn').removeClass('disabled');
            } else{
                $('#track-btn').addClass('disabled');
            }
        });

        $('#track-btn').on('click', function () {
            $('#track-sms').removeClass('hidden');
        });

        $('.popup-link').each(function() {
            $(this).magnificPopup({
                mainClass: 'mfp-zoom-in',
                tLoading: '',
                removalDelay: 300,
                callbacks: {
                    open: function() {
                        var self = this;
                        setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
                        $.magnificPopup.instance.next = function() {
                            var self = this;
                            self.wrap.removeClass('mfp-image-loaded');
                            setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
                        };
                        $.magnificPopup.instance.prev = function() {
                            var self = this;
                            self.wrap.removeClass('mfp-image-loaded');
                            setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
                        };
                    }
                }
            });
        });

        $(function() {
            $('.selectpicker.package-type').on('change', function(){
                var selected = $(this).find("option:selected").val();
                if (selected == "Без упаковки") {
                    $('.info-ul-wrap').addClass('active');
                } else {
                    $('.info-ul-wrap').removeClass('active');
                }
            });
        });
        $(function() {
            $('#organization').on('change', function(){
                var selected = $(this).find("option:selected").val();
                if (selected == "ИП") {
                    $('#organizationKPP').addClass('disabled');
                    $('label[for="fioCEO"]').text('Ваше ФИО');
                } else {
                    $('#organizationKPP').removeClass('disabled');
                    $('label[for="fioCEO"]').text('ФИО ген. директора');
                }
            });
        });
        $('#terms').on('change', function () {
            if ($(this).is(":checked")) {
                $('.finished').removeClass('disabled');
            } else{
                $('.finished').addClass('disabled');
            }
        });
        $('.terms').each(function (index) {
            var $this = $(this);
            $this.find('input').attr('id', 'terms' +index);
            $('#terms' +index).on('change', function () {
                if ($(this).prop('checked') == false) {
                    $(this).parent().parent().find('.btn-default').addClass('disabled');
                } else{
                    $(this).parent().parent().find('.btn-default').removeClass('disabled');
                }
            });
        })

        $('.price-table input[type="radio"]').on('change', function () {
            if ($(this).is(":checked")) {
                $('.price-table').removeClass('selected');
                $('.price-table').addClass('not-selected');
                $(this).parent().parent().addClass('selected');
                $(this).parent().parent().removeClass('not-selected');
            }
        });

        $('body').on("click", '.js-cargo-quantity', function() {
            var $self = $(this);
            $self.addClass('active');
            $self.find('input').focus();
            var hideHandler = function (event) {
                if ($(event.target).closest('.js-cargo-quantity.active').length <= 0 && $self.find('input').val().length === 0 ) {
                    $self.removeClass('active');
                    $('body').off('click', hideHandler);
                }
            };
            $('body').on('click', hideHandler);
        });

    });

    // Client swiper
    var clientSwiper = new Swiper('.client-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.client-slider .swiper-button-next',
            prevEl: '.client-slider .swiper-button-prev',
        }
    });

    // News swiper
    var newsSwiper = new Swiper('.news-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.news-slider .swiper-button-next',
            prevEl: '.news-slider .swiper-button-prev',
        }
    });

    // Partner swiper
    var partnerSwiper = new Swiper('.partner-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.partner-slider .swiper-button-next',
            prevEl: '.partner-slider .swiper-button-prev',
        }
    });

    $(document).on('scroll_to_end', function(){ counterEvent('scrollToEnd '+window.location.pathname); });

    // Hamburger icon
    $(".hamburger").click(function() {
        $(this).toggleClass("is-active");
        $('#header').toggleClass('menu-opened');
        $('html').toggleClass('menu-opened');
        $(".js-mobile-menu").slideToggle();
        $('body').toggleClass('fixed');
    });

    $('.js-toggle-steps').on('click', function () {
        var $this = $(this);
        $('.hero-steps').slideToggle();
        if ($('.codes__steps').length) {
            $('.codes__steps').slideToggle()
        }
        $this.toggleClass('active');
        $(this).find('span').text(function(i, text) {
            return text === "Подробнее"
              ? "Свернуть"
              : "Подробнее";
          });
    })
    $('.js-toggle-codes').on('click', function () {
        $('.codes__table').toggleClass('opened');
        $(this).toggleClass('active');
        $(this).find('span').text(function(i, text) {
            return text === "Развернуть"
              ? "Свернуть"
              : "Развернуть";
          });
    })
    

    //$(function () {
    //    $('[data-toggle="tooltip"]').tooltip({
    //        template: '<div class="tooltip" role="tooltip"><div class="close-tooltip"><img src="/img/close.png" alt="" /></div><div class="arrow"></div><div class="tooltip-inner"></div></div>'
    //    })
    //})
    //$(document).on('click', '.close-tooltip', function () {
    //    $('[data-toggle="tooltip"]').tooltip('hide')
    //})
});

$(function () {
    //$('[data-toggle="tooltip"]').tooltip()
})
$(document).on('click', '.more-items', function () {
    $(this).parent().find('ul').addClass('opened');
    $(this).hide();
})


$('.selectpicker').on('change', function () {
    $(this).valid();
});

$(document).on('click', '.warning .close', function () {
    $(this).parent().remove();
})

// Date and time pickers
function initDatepicker(element, countHours) {
    if (!countHours) {
        countHours = 0;
    }

    var startDate = new Date();
    var endDate = new Date();
    endDate.setHours(endDate.getHours()+countHours);
    element.each(function (index) {
        var $this = $(this);
        $this.attr('id', 'flatpickr' +index);
        $('#flatpickr' +index).flatpickr({
            wrap: true,
            enableTime: true,
            time_24hr: true,
            disableMobile: "true",
            "locale": "ru",
            onChange: function(selectedDates, dateStr, instance) {
                var date = new Date(selectedDates[0]);

            },
            onOpen : function(selectedDates, dateStr, instance) {
                var startDate = new Date();
                var endDate = new Date();
                endDate.setHours(endDate.getHours()+countHours);

                instance.setDate(startDate);
                instance.set('minDate', startDate);
                instance.set('maxDate', endDate);
                instance.redraw();
            },
            minDate: startDate,
            maxDate: endDate,
            clickOpens: false
        });

    });
};

function initDatepickerDefault(element) {
    element.each(function (index) {
        var $this = $(this);

        var id = 'flatpickr-default' +index;
        $this.attr('id', id);
        $('#'+id).flatpickr({
            wrap: true,
            disableMobile: "true",
            dateFormat: "d.m.Y",
            "locale": "ru",
            allowInput: true,
            clickOpens: false
        });
    });
}

// Tabs
function customTabs() {
    $('body').on('click', '[data-toggle="tab"]', function(e){
      e.preventDefault();
        var $this = $(this),
            tabgroup = '#'+$this.parents('.custom-tabs__nav').data('tabgroup'),
            target = $this.attr('href');

        $('[data-toggle="tab"]').removeClass('active');
        $this.addClass('active');

        $(tabgroup).children('.tab-pane').removeClass('active');
        $(target).addClass('active');
      
    })
}
customTabs();

function openPopup(head, text, addClass) {
    if (!text) {
        text = '';
    }
    $( "#popup" ).html('<div class="h2">'+head+'</div><p>'+text+'</p>');
    if (addClass) {
        $( "#popup" ).addClass(addClass);
    }

    $.magnificPopup.open({
        items: {
            src: '#popup',
        },
        type: 'inline'
    });
}

function openPopupButton(head, text, addClass, callback) {
    if (!text) {
        text = '';
    }

    $( "#popup" ).html(
        '<div class="h2">'+head+'</div><p>'+text+'</p><a href="#" class="back-link">Нет</a><a id="button" href="javascript:void(0)" class="btn-default">Подтверждаю</a>'
    )

    if (addClass) {
        $( "#popup" ).addClass(addClass);
    }

    if (callback) {
        $('body').off('click', '#button').on('click', '#button', function(){
            callback();
            return false;
        })
    }

    $('body').on('click', '.back-link', function(){
        closePopup();
        return false;
    })

    $.magnificPopup.open({
        items: {
            src: '#popup',
        },
        type: 'inline'
    });
}

function openPopupUrl(url, addClass, callback, callbackClose) {
    if (!callback) {
        $( "#popup" ).html('');

        $( "#popup" ).load( url );

        if (addClass) {
            $( "#popup" ).addClass(addClass);
        }

        $.magnificPopup.open({
            items: {
                src: '#popup',
            },
            type: 'inline',
        });

    } else {

        $.magnificPopup.open({
            type: 'ajax',
            items: {
                src: url,
            },
            closeOnBgClick: false,
            callbacks: {
                ajaxContentAdded: function() {
                    //$('.mfp-content').css('width', 'auto');
                    //$('.mfp-content').addClass('main-popup');
                    $('.mfp-content').addClass(addClass);
                    if (callback) {
                        callback();
                    }
                },
                close: function () {
                    if (callbackClose) {
                        callbackClose();
                    }
                    return false;
                }
            }
        });

    }
}

function closePopup(el) {
    $.magnificPopup.close();
}

function counterEvent(event, action) {
    if (typeof gtag === 'function') {
        gtag('event', event, {'event_category' : action});
        //ga('send', 'event', event, action);
    }
    if (typeof yaCounter50156956 === 'function') {
        yaCounter50156956.reachGoal(event);
    }
}

function saveProfileEdit() {
    $('.js-save-edit').on("click", function(e) {
        e.preventDefault();
        // p - parent form
        var p = $(this).closest('.js-editable-form');
        p.find('.js-edit-profile').removeClass('clicked');
        p.find('.form-control').attr("readonly", true).removeClass('error');
        p.find('label.error').remove();
        p.find('.selectpicker[readonly]').attr("disabled", true);
        p.find('.selectpicker').selectpicker('refresh');
        p.find('.edit-buttons').addClass('hidden');
        p.find('.profile-buttons').removeClass('hidden');
        p.find('.control input[readonly]').parent().addClass('disabled readonly');
        p.find('.control input:checked').parent().removeClass('disabled');
        p.find('.flatpickr-default').addClass('disabled');
    });
}

function compareGraph() {
    var column = $('.graph-columns li');

    column.each(function () {
        var $this = $(this),
            columnHeight = $this.attr('data-percent-from'),
            economyColumn = $this.parent().find('.economy-column'),
            economyTotal = $this.parent().find('.economy-total b'),
            biggerColumn = parseInt($this.parent().find('.bigger-column').attr('data-percent-from'), 10),
            dotColumn = $this.parent().find('.dot-column');

        $this.css('height', columnHeight);

        // set economy value
        economyColumn.css('height', (100 - parseInt(dotColumn.attr('data-percent-from'))) - (100 - biggerColumn) + '%');
        economyColumn.css('top', 100 - biggerColumn + '%');
        //economyTotal.text(Math.floor((100 - parseInt(dotColumn.attr('data-percent-from'))) - (100 - biggerColumn)) + '%');
    })
}

$(document).ready(function () {
    $('body').on('mouseenter', '[data-text-hover]', function(){
        $(this).html($(this).data('text-hover'));
    })
    $('body').on('mouseout', '[data-text]', function(){
        $(this).text($(this).data('text'));
    })

    if ($('.js-delete-orders-wrapper').length) {
        $(".js-select-all").change(function () {
            var parent = $(this).parents('.order-table');

            parent.find('thead').toggleClass('blured');
            parent.find("td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));

            if (parent.find('.order-number input:checked').length > 0) {
                parent.find('thead').addClass('blured');
            }
        });

        $(".order-number input").change(function () {
            if ($('.order-number input:checked').length > 0) {
                $('.order-table thead').addClass('blured');
            } else {
                $('.order-table thead').removeClass('blured');
                $(".js-select-all").prop('checked', false);
            }
            if ($('.order-number input:checked').length == $(".order-table td:not(.disabled) input:checkbox").length) {
                $(".js-select-all").prop('checked', true);
            } else {
                $(".js-select-all").prop('checked', false);
            }

        });

        $(".js-delete-orders").on("click", function () {
            var parent = $(this).parents('.order-table');
            parent.find('.order-number')
                .find(":checkbox:checked")
                .closest('tr')
                .hide();
            parent.find('thead').removeClass('blured');
            parent.find(".js-select-all").prop('checked', false);
        });
        $(".js-undelete").on("click", function () {
            $('.order-number')
                .find(":checkbox:checked")
                .closest('tr')
                .show();
            $('.order-table thead').removeClass('blured');
            $('.order-number input').prop('checked', false);
            $(".js-select-all").prop('checked', false);
        });

        $('body').on('click', '.order-number', function(){
            if ($('.order-number input:checked').length > 0) {
                $('.order-table thead').addClass('blured');
            } else {
                $('.order-table thead').removeClass('blured');
            }
        })
    }

})
