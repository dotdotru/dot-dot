
let addressCached = {};

let checkAddress = function (address) {
    if (typeof addressCached[address] !== 'undefined') {
        return addressCached[address];
    }

    return Promise.resolve($.ajax({
        url: '/api/data-normalize/exists/address',
        method: 'GET',
        data: {'address': address},
    })).then(
        function (data) {
            let exists = false;
            if (data.status && data.exists) {
                exists = true;
            }
            addressCached[address] = exists;
            return exists;
        },
        function (err) {
            // Если произошла ошибка, считаем что адрес верный
            return true;
        }
    );
};


export default {
    validate: function(value) {
        return checkAddress(value);
    },
    getMessage: function(field, params, data) {
        return 'Укажите правильный адрес ';
    }
}