import Vue from 'vue';
import Track from './track.vue';

$('[data-attach-vue="track"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Track/>',
        data: {
            //username: item.data('username'),
            //restorePath: item.data('restore-path'),
            //loginPath: item.data('login-path'),
        },
        components: { Track }
    });

});