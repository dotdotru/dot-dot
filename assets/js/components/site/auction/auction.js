import Vue from 'vue';
import Auction from './auction.vue';

$('[data-attach-vue="auction"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<auction :route="route"/>',
        data: {
            route: item.data('route')
        },
        components: { Auction }
    });

});

