
import moment from 'moment';


/**
 * Работа со временем работы складов в городе
 */
export default class CityStocksTime
{

    /**
     * @param stocks
     * @param timezone
     * @param workingHours
     */
    constructor(stocks, timezone, workingHours, weekends) {

        // Список времени доступного для доставки
        this.timesList = [];
        // Список времени доступного для доставки, сгруппированный по дате
        this.timesGroupedList = {};
        // Список дат доступных для доставки
        this.datesList = [];
        // Интервалы между временем приезда
        this.interval = 60;
        // Таймзона, в которой работают склады
        this.timezone = timezone;

        // оставляем только склады этого города
        let tmpWorkingHours = [];
        stocks.forEach(stock => {
            workingHours.forEach(hours => {
                if (parseInt(stock.externalId) === parseInt(hours.externalId)) {
                    tmpWorkingHours.push(hours);
                }
            });
        });

        // оставляем только склады этого города
        let tmpWeekends = [];
        stocks.forEach(stock => {
            weekends.forEach(weekend => {
                if (parseInt(stock.externalId) === parseInt(weekend.externalId)) {
                    tmpWeekends.push(weekend);
                }
            });
        });

        this.workingHours = tmpWorkingHours;
        this.weekends = tmpWeekends;
    }

    /*
     * Инициализируем список дат и времени, на которые доступен заказ
     */
    initDates() {
        let interval = this.interval;

        let tmp = moment();
        tmp.utcOffset(this.timezone.offsetHour * 60);

        // округляем до ближайшего интервала
        let tmpMinutes = tmp.format('m');
        tmp.minutes(tmpMinutes - (tmpMinutes % this.interval) + this.interval);

        // В сумме должно получиться 48 календарных часов без учета выходных
        let totalMinutes = 0;
        let step = 0;
        while (totalMinutes < 48 * 60) {
            if (this.isWorkTime(tmp)) {
                this.addDateToList(tmp);
                totalMinutes += interval;
            } else {
                // выходные дни не учитываются
                if (this.isWorkDate(tmp) && !this.isWeekend(tmp)) {
                    totalMinutes += interval;
                }
            }

            tmp.add(interval, 'minutes');

            // Чтобы не падать в бесконечный цикл
            if (++step > 3000) {
                break;
            }
        }

        this.inited = true;
    }

    /**
     * Проверяем входит ли время в рабочее время склада
     */
    isWorkTime(date) {
        let tmp = moment(date);
        let working = false;

        this.workingHours.forEach(hours => {
            if (1 * hours.dow !== (1 * tmp.format('E'))) {
                return;
            }

            let timeStart = moment(hours.startTime, "HH:mm");
            let timeEnd = moment(hours.endTime, "HH:mm").subtract(2, 'hours'); // Склад перестает принимать грузы за 2 часа до закрытия


            // Если выходной, считаем что день у этого склада нерабочий
            if (!this.isWeekend(tmp, hours.stockId)) {
                if (tmp.format('HH:mm') >= timeStart.format('HH:mm') && tmp.format('HH:mm') < timeEnd.format('HH:mm')) {
                    working = true;
                }
            }
        });

        return working;
    }

    /**
     * Проверяем работает ли в этот день склад
     */
    isWorkDate(date) {
        let tmp = moment(date);
        let working = false;

        this.workingHours.forEach(hours => {
            // Если нашлось рабочее время склада на этот день, значит работает
            if (1 * hours.dow === (1 * tmp.format('E'))) {
                working = true;
            }
        });

        return working;
    }


    /**
     * Проверяем выходной ли этот день для данного склада
     *
     * @param checkDate
     * @param stockId
     * @returns {boolean}
     */
    isWeekend (checkDate, stockId) {
        let date = moment(checkDate);
        let weekend = false;
        this.weekends.forEach(tmpWeekend => {
            if (tmpWeekend.day !== date.format('Y-MM-DD')) {
                return;
            }
            // Если не передан склад, просто берем выходной для любого склада
            if (!stockId) {
                weekend = tmpWeekend;
            } else {
                if (stockId === tmpWeekend.stockId) {
                    weekend = tmpWeekend;
                }
            }
        });

        return weekend;
    }

    /**
     * Добавляем дату в списки
     */
    addDateToList(date) {
        this.timesList.push(date.format('Y-MM-DD HH:mm'));
        this.datesList.push(date.format('Y-MM-DD'));

        if (typeof this.timesGroupedList[date.format('Y-MM-DD')] === 'undefined') {
            this.timesGroupedList[date.format('Y-MM-DD')] = [];
        }

        this.timesGroupedList[date.format('Y-MM-DD')].push(date.format('HH:mm'));
    }

    /**
     * Возвращаем список времени на которое доступна доставка [Y-m-d H:i]
     */
    getTimes() {
        if (!this.inited) {
            this.initDates();
        }

        return this.timesList;
    }

    /**
     * Возвращаем список дата => [время] на которое доступна доставка {Y-m-d: [H:i]}
     */
    getTimesGrouped() {
        if (!this.inited) {
            this.initDates();
        }

        return this.timesGroupedList;
    }

    /**
     * Возвращаем список дат на которые доступна доставка [Y-m-d]
     */
    getDates() {
        if (!this.inited) {
            this.initDates();
        }

        return this.datesList;
    }

    /**
     * Проверяем что дата доступна для доставки
     * @param Date date
     */
    isDateAvailable(date) {
        let tmp = moment(date);
        let dates = this.getDates();

        if (dates.indexOf(tmp.format('Y-MM-DD')) !== -1) {
            return false;
        }

        return true;
    }

    /**
     * Проверяем что время доступно для доставки
     * @param Date date
     */
    isTimeAvailable(date) {
        let tmp = moment(date);
        let times = this.getTimes();

        if (times.indexOf(tmp.format('Y-MM-DD HH:mm')) !== -1) {
            return false;
        }

        return true;
    }

    /**
     * Получаем границы времени для выбранной даты
     */
    getTimeBoundaries(date) {
        let tmpDate = moment(date).format('Y-MM-DD');
        let timesGrouped = this.getTimesGrouped();

        let boundaries = {};

        // Берем первое и последнее время
        if (typeof timesGrouped[tmpDate] !== 'undefined') {
            boundaries.start = timesGrouped[tmpDate][0];
            boundaries.end = timesGrouped[tmpDate][timesGrouped[tmpDate].length - 1];
        }

        return boundaries;
    }


    /**
     * Получаем границы времени для выбранной даты
     */
    getDateTimes(date) {
        let tmpDate = moment(date).format('Y-MM-DD');
        let timesGrouped = this.getTimesGrouped();

        let times = [];

        // Берем первое и последнее время
        if (typeof timesGrouped[tmpDate] !== 'undefined') {
            times = timesGrouped[tmpDate];
        }

        return times;
    }

}
