
import moment from 'moment';
import Markercluster from '../../../../../libs/markercluster.js';


class MilesMap
{

    /**
     *
     */
    constructor(element, useCluster) {
        this.map = new google.maps.Map(element, {
            zoom: 14,
            center: { lat: 0, lng: 0 },
            disableDefaultUI: true,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "saturation": "-65"
                        },
                        {
                            "lightness": "45"
                        },
                        {
                            "gamma": "1.78"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "saturation": "-33"
                        },
                        {
                            "lightness": "22"
                        },
                        {
                            "gamma": "2.08"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "gamma": "2.08"
                        },
                        {
                            "hue": "#ffa200"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "saturation": "-55"
                        },
                        {
                            "lightness": "-2"
                        },
                        {
                            "gamma": "1.88"
                        },
                        {
                            "hue": "#ffab00"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#bbd9e5"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                }
            ]
        });

        this.markerCluster = null;
        if (useCluster) {
            this.pickupMarkerCluster = new Markercluster(this.map, [], {
                imagePath: '/img/marker-cluster/marker',
                gridSize: 70,
                minimumClusterSize: 10,
                styles: [
                    {
                        url: "/img/marker-pickup-cluster/marker1.svg",
                        height: 20,
                        width: 20,
                        anchor: [0, 0],
                        textSize: 0.01,
                    }
                ]
            });
            this.deliverMasterCluster = new Markercluster(this.map, [], {
                imagePath: '/img/marker-cluster/marker',
                gridSize: 70,
                minimumClusterSize: 10,
                styles: [
                    {
                        url: "/img/marker-deliver-cluster/marker1.svg",
                        height: 20,
                        width: 20,
                        anchor: [0, 0],
                        textSize: 0.01,
                    }
                ]
            });
        }

        this.markers = [];
        this.stockMarkers = [];

        this.directionsService = new google.maps.DirectionsService();
        this.directionsRenderer = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        });
        this.directionsRenderer.setMap(this.map);

        this.currentCheckedMile = null;
    }

    /**
     * Удаляем все маркеры и создаем новые
     * @param miles
     * @param checkedMile
     * @param markerCheckedCallback колбэк для клика по маркеру
     */
    reInitMapMarkers(miles, checkedMile = null, markerCheckedCallback = null) {
        // Удаляем с карты только те маркеры, которые удалились в ответе, чтобы не перерисовывать их
        let removedMarkers = [];
        let savedMarkers = [];
        let showedMiles = [];
        this.markers.forEach(marker => {
            let foundedMileId = false;
            miles.forEach(mile => {
                let positionEquals = marker.markerPosition.latitude === mile.latitude && marker.markerPosition.lotitude === mile.lotitude;
                if (marker.mile.id === mile.id && positionEquals) {
                    foundedMileId = mile.id;
                }
            });

            if (foundedMileId) {
                showedMiles.push(foundedMileId);
                savedMarkers.push(marker)
            } else {
                marker.setMap(null);
                removedMarkers.push(marker);
            }
        });


        this.pickupMarkerCluster.removeMarkers(removedMarkers);
        this.deliverMasterCluster.removeMarkers(removedMarkers);
        this.markers = savedMarkers.slice();

        let pickupMarkers = [];
        let deliverMarkers = [];

        miles.forEach((mile) => {
            if (showedMiles.indexOf(mile.id) !== -1) {
                return;
            }

            let position = {lat: parseFloat(mile.latitude), lng: parseFloat(mile.lotitude)};

            let marker = new google.maps.Marker({
                position: position,
                map: this.map,
                label: {
                    text: mile.price + ' ₽',
                    color: 'black',
                    fontSize: '12px',
                    fontWeight: '700'
                },
                icon: {
                    url: mile.type === 'pickup' ? '/img/map/miles-pickup-marker.svg' : '/img/map/miles-deliver-marker.svg',
                    size: { width: 94, height: 51 },
                    labelOrigin: { x: 58, y: 20 },

                },

                // миля, к которой привязан маркер
                mile: mile,
                markerPosition: {latitude: mile.latitude, lotitude: mile.lotitude},
            });

            google.maps.event.addListener(marker, 'click', () => {
                if (markerCheckedCallback) {
                    markerCheckedCallback(mile);
                }
            });

            this.markers.push(marker);

            if (mile.type === 'pickup') {
                pickupMarkers.push(marker);
            } else {
                deliverMarkers.push(marker);
            }
        });

        this.pickupMarkerCluster.addMarkers(pickupMarkers);
        this.deliverMasterCluster.addMarkers(deliverMarkers);

        // Центрируем карту на основе маркеров, если ниодна из пришедших миль не отображена на карте
        if (miles.length && !savedMarkers.length) {
            this.fitBounds();
        }

        if (checkedMile) {
            this.checkMile(checkedMile);
        }
    }

    /**
     * Меняем цену маркера, в зависимости от коэфицента
     */
    reInitMarkerPrice (percentCoef) {
        this.markers.forEach(marker => {
            let price = Math.round((marker.mile.price / 100) * percentCoef);
            let label = marker.getLabel();
            label.text = price + ' ₽';
            marker.setLabel(label)
        });
    }


    setCenter (lat, lng) {
        return;
        this.map.setCenter({ lat: parseFloat(lat), lng: parseFloat(lng) });
    }

    /**
     * Перецентрируем карту относительно маркеров
     */
    fitBounds () {
        if (!this.map) {
            return;
        }

        let bounds = new google.maps.LatLngBounds();
        this.markers.forEach(marker => {
            bounds.extend(marker.position);
        });
        this.stockMarkers.forEach(marker => {
            bounds.extend(marker.position);
        });

        this.map.fitBounds(bounds);
    }


    /**
     * Удаляем все маркеры складов и создаем новые
     * @param allStocks
     * @param currentCity
     * @param markerCheckedCallback колбэк для клика по маркеру
     */
    reInitStockMarkers(allStocks, currentCity = null, markerCheckedCallback = null) {
        let stocks = [];
        allStocks.forEach(stock => {
            if (currentCity && currentCity.id === stock.city.id) {
                stocks.push(stock);
            }
        });

        let newStocks = [];
        let savedMarkers = [];

        this.stockMarkers.forEach(marker => {
            let founded = false;
            stocks.forEach(stock => {
                if (stock.id == marker.stockId) {
                    founded = true;
                }
            });
            if (!founded) {
                marker.setMap(null)
            } else {
                savedMarkers.push(marker);
            }
        });
        stocks.forEach(stock => {
            let founded = false;
            this.stockMarkers.forEach(marker => {
                if (stock.id == marker.stockId) {
                    founded = true;
                }
            });
            if (!founded) {
                newStocks.push(stock);
            }
        });

        this.stockMarkers = savedMarkers.slice();

        newStocks.forEach((stock) => {
            let position = {lat: parseFloat(stock.lat), lng: parseFloat(stock.lng)};

            let marker = new google.maps.Marker({
                position: position,
                map: this.map,
                icon: {
                    url: '/img/map/miles-stock-marker.svg',
                    size: { width: 71, height: 42 },
                    labelOrigin: { x: 35, y: 14 },
                    scaledSize: { width: 71, height: 42 }
                },
                stockId: stock.id
            });

            google.maps.event.addListener(marker, 'click', () => {
                if (markerCheckedCallback) {
                    markerCheckedCallback(mile);
                }
            });

            this.stockMarkers.push(marker);
        });

        if (newStocks.length) {
            this.fitBounds();
        }
    }


    /**
     * Если есть выделенная миля, выделяем маркер, если нет, сбрасываем все изменения
     * Исключаем выделенную милю из кластеризации
     *
     * @param mile
     */
    checkMile (mile) {
        if (this.currentCheckedMile) {
            this.markers.forEach(marker => {
                if (marker.mile.id === this.currentCheckedMile.id) {
                    this.currentCheckedMile.type === 'pickup' ? this.pickupMarkerCluster.addMarker(marker) : this.deliverMasterCluster.addMarker(marker);
                    marker.setOptions({'zIndex': 1});
                }
            });
        }
        this.currentCheckedMile = mile;


        if (mile) {
            this.markers.forEach(marker => {
                if (marker.mile.id === mile.id) {
                    marker.setOptions({'opacity': 1});
                    marker.setOptions({'zIndex': 9999});

                    mile.type === 'pickup' ? this.pickupMarkerCluster.removeMarker(marker) : this.deliverMasterCluster.removeMarker(marker);
                    marker.setMap(this.map);
                } else {
                    marker.setOptions({'opacity': 0.5});
                }
            });
        } else {
            this.markers.forEach(marker => {
                marker.setOptions({'opacity': 1})
            });

            // удаляем все роуты
            this.directionsRenderer.set('directions', null);
        }
    }


    /**
     * Рисуем маршрут
     *
     * @param origin
     * @param destination
     * @param callback
     */
    drawRoute (origin, destination, callback = null) {
        let formData = new FormData();

        formData.append('position_from[lat]', origin.lat);
        formData.append('position_from[lng]', origin.lng);
        formData.append('position_to[lat]', destination.lat);
        formData.append('position_to[lng]', destination.lng);

        $.ajax({
            method: 'POST',
            url: "/api/geo/route",
            cache: false,
            async: true,
            data: formData,
            dataType: "json",
            processData: false,
            contentType: false,
        }).done((data) => {
            if (data.status && data.data.status !== 'NOT_FOUND') {
                let bounds = new google.maps.LatLngBounds();
                bounds.extend(data.data.routes[0].bounds['northeast']);
                bounds.extend(data.data.routes[0].bounds['southwest']);

                data.data.routes[0].bounds = bounds;

                if (callback) {
                    callback({distance: data.data.routes[0].legs[0].distance, duration: data.data.routes[0].legs[0].duration});
                }

                this.directionDisplay = this.directionsRenderer.setDirections(data.data);
            }
        }).catch(error => {});
    }



}



export default MilesMap
