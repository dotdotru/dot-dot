import Vue from 'vue';
import Miles from './auction.vue';

$('[data-attach-vue="miles-auction"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<miles />',
        data: {

        },
        components: { Miles }
    });



});