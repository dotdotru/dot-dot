import Vue from 'vue';
import Reset from './reset.vue';

$('[data-attach-vue="reset"]').each(function( index ) {

    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Reset/>',
        data: {
            //username: item.data('username'),
            //restorePath: item.data('restore-path'),
            //loginPath: item.data('login-path'),
        },
        components: { Reset }
    });

});