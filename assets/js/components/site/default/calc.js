import Vue from 'vue';
import Calc from './calc.vue';

$('[data-attach-vue="calc"]').each(function( index ) {

    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Calc :route="route" />',
        data: {
            route: item.data('route'),
            //count: item.data('count'),
            //filterUrl: item.data('filterurl'),
        },
        components: { Calc }
    });

});