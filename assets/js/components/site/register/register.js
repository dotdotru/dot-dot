import Vue from 'vue';
import Register from './register.vue';

$('[data-attach-vue="register"]').each(function( index ) {

    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Register :forcestep="step" :group="group" :userid="userid"/>',
        data: {
            step: item.data('step'),
            group: item.data('group'),
            userid: item.data('userid'),
        },
        components: { Register }
    });

});