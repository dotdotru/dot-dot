import Vue from 'vue';
import Batches from './batches.vue';
import OverloadBatches from './overload/overloadBatches.vue';

$('[data-attach-vue="batches"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<batches :batchId="batchId"/>',
        data: {
            batchId: item.data('batch-id'),
            //count: item.data('count'),
            //filterUrl: item.data('filterurl'),
        },
        components: { Batches }
    });

});

$('[data-attach-vue="overload-batches"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<overload-batches />',
        data: {

        },
        components: { OverloadBatches }
    });

});