import Vue from 'vue';
import Authtoken from './authtoken.vue';

$('[data-attach-vue="personal-authtoken"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<authtoken />',
        data: {

        },
        components: { Authtoken }
    });

});