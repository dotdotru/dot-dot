import Vue from 'vue';
import Orders from './orders.vue';

$('[data-attach-vue="orders"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Orders :orderId="orderId"/>',
        data: {
            orderId: item.data('order-id'),
            //count: item.data('count'),
            //filterUrl: item.data('filterurl'),
        },
        components: { Orders }
    });

});