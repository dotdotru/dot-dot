import Vue from 'vue';
import Profile from './profile.vue';

$('[data-attach-vue="profile"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<profile />',
        data: {

        },
        components: { Profile }
    });

});