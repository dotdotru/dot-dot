import Vue from 'vue';
import ChangePassword from './changePassword.vue';

$('[data-attach-vue="change-password"]').each(function( index ) {
    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<ChangePassword />',
        data: {

        },
        components: { ChangePassword }
    });

});