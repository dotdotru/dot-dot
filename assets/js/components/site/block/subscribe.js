// import $ from 'jquery';
// import ff from './../../vueconfig.js';
// import Vue from 'vue';
// $(function () {
//     // Не показываем на мобильниках
//     if ($(window).width() < 760) {
//         return;
//     }

//     setTimeout(
//         function () {
//             let formData = new FormData();
//             formData.append('page', location.pathname);

//             $.ajax({
//                 method: 'POST',
//                 url: "/api/subscribe/check",
//                 cache: false,
//                 async: false,
//                 data: formData,
//                 dataType: "json",
//                 processData: false,
//                 contentType: false,
//             }).done(function (data) {
//                 if (data.data.show) {
//                     ff.openPopupUrl('/static/popup/subscribe', 'main-popup register-popup', function () {
//                         initSubscribe();
//                     });
//                 }
//             });
//         }, 5000
//     );
// })

// function initSubscribe() {
//     let subscribe = new Vue({
//         el: '#subscribe',
//         data: {
//             email: '',
//             terms: true,
//             subscribe: true,
//             error: false,
//         },
//         methods: {
//             subscribeEmail: function ($event) {
//                 let $this = this;

//                 this.$validator.validateAll().then(function (result) {

//                     if (!result) {
//                         return;
//                     }

//                     $.ajax({
//                         method: 'POST',
//                         url: "/api/subscribe/subscribe",
//                         async: true,
//                         data: { email: $this.email, subscribe: $this.subscribe, url: location.href },
//                     }).done(function (data) {
//                         if (data.error != undefined) {
//                             $this.error = true;
//                         } else if (data.status) {
//                             ff.counterEvent('Mail', 'form_submit');
//                             ff.closePopup();
//                         }

//                     });

//                     return false;
//                 });

//                 return false;
//             },
//         },
//         watch: {
//             email: function (val) {
//                 this.error = false;
//             },
//         }

//     })
// }