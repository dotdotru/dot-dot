import Vue from 'vue';
import Subscrib from './subscrib.vue';

$('[data-attach-vue="subscrib"]').each(function (index) {

    var item = $(this);

    new Vue({
        el: '#' + item.attr('id'),
        template: '<Subscrib/>',
        data: {
            modal: true
        },
        components: { Subscrib },

    });

});