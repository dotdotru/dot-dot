import Vue from 'vue';
import Auth from './auth.vue';

$('[data-attach-vue="auth"]').each(function( index ) {

    var item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Auth :username="username" :restorePath="restorePath" :loginPath="loginPath"/>',
        data: {
            username: item.data('username'),
            restorePath: item.data('restore-path'),
            loginPath: item.data('login-path'),
        },
        components: { Auth }
    });

});