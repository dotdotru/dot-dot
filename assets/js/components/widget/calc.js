import Vue from 'vue';
import CalcWidget from './calc.vue';

let item = document.getElementById('widgetCalc');

new Vue({
    el: '#' + item.getAttribute('id'),
    template: '<CalcWidget :host="host" :predefinedDirection="direction" :predefinedWeight="weight" />',
    data: {
        host: item.getAttribute('data-host'),
        direction: item.getAttribute('data-direction'),
        weight: item.getAttribute('data-weight')
    },
    components: { CalcWidget }
});
