
export default {

    addPickUpToFormData: function(formData, pickUp, defaultStock) {
        var $this = this;

        formData.append('order[pickUp][id]', pickUp.id);
        formData.append('order[pickUp][active]', pickUp.active);
        formData.append('order[pickUp][date]', pickUp.date);

        if (pickUp.stock && pickUp.stock.toString().trim()) {
            formData.append('order[pickUp][stock]', pickUp.stock);
        } else {
            formData.append('order[pickUp][stock]', defaultStock.id);
        }

        formData.append('order[pickUp][address]', pickUp.address);
        formData.append('order[pickUp][countLoader]', pickUp.countLoader);
        formData.append('order[pickUp][hydroBoard]', pickUp.hydroBoard);

        return formData;
    },

    addDeliverToFormData: function(formData, deliver, defaultStock) {
        formData.append('order[deliver][id]', deliver.id);
        formData.append('order[deliver][active]', deliver.active);
        formData.append('order[deliver][date]', deliver.date);

        if (deliver.stock && deliver.stock.toString().trim()) {
            formData.append('order[deliver][stock]', deliver.stock);
        } else {
            formData.append('order[deliver][stock]', defaultStock.id);
        }

        formData.append('order[deliver][address]', deliver.address);
        formData.append('order[deliver][countLoader]', deliver.countLoader);
        formData.append('order[deliver][hydroBoard]', deliver.hydroBoard);

        return formData;
    }

}