
/**
 * Общий модуль для работы с токенами для заказов
 */
const store = {
  
    namespaced: true,
  
    state: {

    },

    getters: {

    },

    mutations: {

    },
    actions: {
        /**
         * Загружаем список использованных токенов
         */
        reserve: (context, payload) => {
            let formData = new FormData();

            if (payload) {
                formData.append('order[sendDate]', payload.sendDate);
                formData.append('order[direction]', payload.direction);
                formData.append('order[weight]', payload.weight);
                formData.append('order[maxWeight]', payload.maxWeight);
                formData.append('order[maxCount]', payload.maxCount);
                formData.append('order[price]', payload.price);
                formData.append('order[currentPricePerKilo]', payload.currentPricePerKilo);
                formData.append('order[stockFrom]', payload.stockFrom);
                formData.append('order[stockTo]', payload.stockTo);

                payload.pallets.forEach((item, index) => {
                    formData.append('order[pallets][' + index + ']', item);
                });

            }

            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/auction/reserve",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    if (result.status == true) {
                        resolve(result.id)
                    } else {
                        reject(result);
                    }
                });
            });
            
            return tmpPromise;
        },
    },

};


export default store;
