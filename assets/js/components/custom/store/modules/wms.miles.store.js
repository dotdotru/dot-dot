
/**
 * Общий модуль для работы с милями
 */
const store = {
  
    namespaced: true,
  
    state: {
        /**
         * Список миль доступных для заказа
         */
        availableMiles: [],

        availableDates: []
    },

    getters: {
        availableMiles: state => {
            return state.availableMiles;
        },
        availableDates: state => {
            return state.availableDates;
        },
    },

    mutations: {
        /**
         * Перезаписать мили
         */
        setAvailableMiles(state, miles) {
            let availableMiles = [];

            for (let i in miles) {
                availableMiles.push(miles[i]);
            }

            state.availableMiles = availableMiles;
        },

        setAvailableDates(state, dates) {
            let availableDates = [];

            for (let i in dates) {
                availableDates.push(dates[i]);
            }

            state.availableDates = availableDates;
        },

        /**
         * Добавить милю в список
         */
        addAvailableMiles(state, mile) {
            let availableMiles = [];

            // Перезаписываем заказ, если он уже есть в списке
            for (let i in state.availableMiles) {
                availableMiles.push(state.availableMiles[i]);
            }

            availableMiles.push(mile);
            state.availableMiles = availableMiles;
        }
    },
    actions: {
        /**
         * Загружаем список доступных миль
         */
        loadAvailableMiles: (context, payload) => {
            let formData = new FormData();

            if (payload) {
                if (payload.city_id) {
                    formData.append('city_id', payload.city_id);
                }
                if (payload.date) {
                    formData.append('date', payload.date);
                }
                if (payload.sort && payload.sort.field) {
                    formData.append('sort[field]', payload.sort.field);
                    formData.append('sort[direction]', payload.sort.direction ? payload.sort.direction : 'DESC');
                }
            }

            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/miles/available",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    context.commit('setAvailableMiles', result.data.miles);
                    context.commit('setAvailableDates', result.data.dates);
                    resolve(result.data)
                });
            });
            
            return tmpPromise;             
        },


        reserveAvailableMile: (context, payload) => {
            let tmpPromise = new Promise((resolve, reject) => {
                let formData = new FormData();
                formData.append('coef', payload.coef);

                $.ajax({
                    method: 'POST',
                    url: "/api/miles/batch/reserve/" + payload.mile_id,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    if (result.status) {
                        resolve(result);
                    } else {
                        reject(result);
                    }
                }).catch(error => {
                    reject(false);
                });
            });

            return tmpPromise;
        },


        confirmBatch: (context, payload) => {
            let tmpPromise = new Promise((resolve, reject) => {
                let formData = new FormData();
                formData.append('coef', payload.coef);

                $.ajax({
                    method: 'POST',
                    url: "/api/miles/batch/confirm/" + payload.batchId + '/' + payload.driverId,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    if (result.status) {
                        resolve(result);
                    } else {
                        reject(result);
                    }
                }).catch(error => {
                    reject(false);
                });
            });

            return tmpPromise;
        },

        rejectBatch: (context, payload) => {
            let tmpPromise = new Promise((resolve, reject) => {
                let formData = new FormData();

                $.ajax({
                    method: 'POST',
                    url: "/api/miles/batch/reject/" + payload.batchId,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    if (result.status) {
                        resolve(result);
                    } else {
                        reject(result);
                    }
                });
            });

            return tmpPromise;
        }


    },

};


export default store;
