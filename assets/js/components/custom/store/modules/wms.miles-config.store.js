
/**
 * Общий модуль для работы конфигом миль
 */
const store = {
  
    namespaced: true,
  
    state: {
        cities: [],
        stocks: [],
        availableSorts: [
            {id: 1, 'field': 'pickUpAt', 'title': 'По времени'},
            {id: 2, 'field': 'price', 'title': 'По доходу'}
        ],
        auctionSettings: {}
    },


    getters: {
        cities: state => {
            return state.cities;
        },
        stocks: state => {
            return state.stocks;
        },
        city: state => (cityId) => {
            let currentCity = null;
            state.cities.forEach(city => {
                if (city.id == cityId) {
                    currentCity = city;
                }
            });

            return currentCity;
        },
        stock: state => (stockId) => {
            let currentStock = null;
            state.stocks.forEach(stock => {
                if (stock.id == stockId) {
                    currentStock = stock;
                }
            });

            return currentStock;
        },
        stockByExternalId: state => (stockExternalId) => {
            let currentStock = null;
            state.stocks.forEach(stock => {
                if (stock.externalId == stockExternalId) {
                    currentStock = stock;
                }
            });

            return currentStock;
        },
        availableSorts: state => {
            return state.availableSorts;
        },
        auctionSettings: state => {
            return state.auctionSettings;
        }
    },

    mutations: {
        setCities(state, cities) {
            state.cities = cities;
        },
        setStocks(state, stocks) {
            state.stocks = stocks;
        },
        setAuctionSettings(state, auctionSettings) {
            state.auctionSettings = auctionSettings;
        }
    },


    actions: {
        loadConfig: (context, payload) => {
            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    url: '/api/miles/config',
                    method: 'GET',
                    data: {},
                }).done(function(result) {
                    context.commit('setCities', result.data.params.cities);
                    context.commit('setStocks', result.data.params.stocks);
                    context.commit('setAuctionSettings', result.data.params.auction_settings);

                    resolve(result.data)
                });
            });

            return tmpPromise;
        },
    },



};


export default store;
