
/**
 * Общий модуль для работы с токенами для заказов
 */
const store = {
  
    namespaced: true,
  
    state: {
        /**
         * Список использованных токенов
         */
        usedTokens: [],
        usedTokensPages: 0
    },

    getters: {
        usedTokens: state => {
            return state.usedTokens;
        },
        usedTokensPages: state => {
            return state.usedTokensPages;
        },
    },

    mutations: {
        /**
         * Добавить токен в список
         */
        setUsedToken(state, tokens) {
            let usedTokens = [];

            // Перезаписываем заказ, если он уже есть в списке
            for (let i in tokens) {
                usedTokens.push(tokens[i]);
            }

            state.usedTokens = usedTokens;
        },

        /**
         * Добавить токен в список
         */
        addUsedToken(state, token) {
            let usedTokens = [];

            // Перезаписываем заказ, если он уже есть в списке
            for (let i in state.usedTokens) {
                usedTokens.push(state.usedTokens[i]);
            }

            usedTokens.push(token);
            state.usedTokens = usedTokens;
        }
    },
    actions: {
        /**
         * Загружаем список использованных токенов
         */
        loadUsedTokens: (context, payload) => {
            let formData = new FormData();

            if (payload) {
                formData.append('page', payload.page);
                formData.append('limit', payload.limit);
            }

            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/user/authtoken/used",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    context.commit('setUsedToken', result.data.tokens);
                    context.state.usedTokensPages = result.data.pages;

                    resolve(result.data)
                });
            });
            
            return tmpPromise;             
        },
    },

};


export default store;
