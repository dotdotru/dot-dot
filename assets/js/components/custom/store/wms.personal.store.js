
import authtokenStore from './modules/wms.authtoken.store';

const store = {
    modules: {
        authtoken: authtokenStore,
    },
};

export default store;
