
import milesStore from './modules/wms.miles.store';
import configStore from './modules/wms.miles-config.store';

const store = {
    modules: {
        miles: milesStore,
        config: configStore
    },
};

export default store;
