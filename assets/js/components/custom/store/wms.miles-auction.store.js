
import milesStore from './modules/wms.miles.store';
import configStore from './modules/wms.miles-config.store';

// Создаем новый экземпляр, для миль карты
let mapMilesStore = jQuery.extend(true, {}, milesStore);

const store = {
    modules: {
        miles: milesStore,
        mapMiles: mapMilesStore,
        config: configStore
    },
};

export default store;
