
import auctionStore from './modules/wms.auction.store';

const store = {
    modules: {
        auction: auctionStore,
    },
};

export default store;
