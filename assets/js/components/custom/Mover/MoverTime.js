
import moment from 'moment';


class MoverTime
{

    /**
     * @param timeStart начало работы склада
     * @param timeEnd окончание работы склада
     */
    constructor(timeStart, timeEnd) {
        this.inited = false;
        // Список времени доступного для доставки
        this.timesList = [];
        // Список времени доступного для доставки, сгруппированный по дате
        this.timesGroupedList = {};
        // Список дат доступных для доставки
        this.datesList = [];

        // Интервалы между временем доставки
        this.interval = 10;

        // Время работы склада
        this.timeStart = moment(timeStart, "HH:mm");
        this.timeEnd = moment(timeEnd, "HH:mm");
    }

    /*
     * Инициализируем список дат и времени, на которые доступен заказ
     */
    initDates() {
        let interval = this.interval;

        // Начальная дата через 24 часа от текущей
        let tmp = moment().add(1, 'days');

        // округляем до ближайших 10 минут
        let tmpMinutes = tmp.format('m');
        tmp.minutes(tmpMinutes - (tmpMinutes % 10) + 10);

        // В сумме должно получиться 48 календарных часов без учета выходных
        let totalMinutes = 0;
        while (totalMinutes <= 48 * 60) {
            // выходные не учитываюся
            if (tmp.format('d') == 0 || tmp.format('d') == 6) {
                tmp.add(2, 'days');
                tmp.minutes(0).hours(0);
            }

            if (this.isWorkTime(tmp)) {
                this.addDateToList(tmp);
            }

            totalMinutes += interval;
            tmp = tmp.add(interval, 'minutes');
        }

        this.inited = true;
    }

    /**
     * Проверяем входит ли время в рабочее время склада
     */
    isWorkTime(date) {
        let tmp = moment(date);

        // исключаем выходные
        if (tmp.format('d') == 0 || tmp.format('d') == 6) {
            return false;
        }
        if (tmp.format('HH:mm') >= this.timeStart.format('HH:mm') && tmp.format('HH:mm') <= this.timeEnd.format('HH:mm')) {
            return true;
        }

        return false;
    }

    /**
     * Добавляем дату в списки
     */
    addDateToList(date) {
        this.timesList.push(date.format('Y-MM-DD HH:mm'));
        this.datesList.push(date.format('Y-MM-DD'));

        if (typeof this.timesGroupedList[date.format('Y-MM-DD')] === 'undefined') {
            this.timesGroupedList[date.format('Y-MM-DD')] = [];
        }

        this.timesGroupedList[date.format('Y-MM-DD')].push(date.format('HH:mm'));
    }

    /**
     * Возвращаем список времени на которое доступна доставка [Y-m-d H:i]
     */
    getTimes() {
        if (!this.inited) {
            this.initDates();
        }

        return this.timesList;
    }

    /**
     * Возвращаем список дата => [время] на которое доступна доставка {Y-m-d: [H:i]}
     */
    getTimesGrouped() {
        if (!this.inited) {
            this.initDates();
        }

        return this.timesGroupedList;
    }

    /**
     * Возвращаем список дат на которые доступна доставка [Y-m-d]
     */
    getDates() {
        if (!this.inited) {
            this.initDates();
        }

        return this.datesList;
    }

    /**
     * Проверяем что дата доступна для доставки
     * @param Date date
     */
    isDateAvailable(date) {
        let tmp = moment(date);
        let dates = this.getDates();

        if (dates.indexOf(tmp.format('Y-MM-DD')) !== -1) {
            return false;
        }

        return true;
    }

    /**
     * Проверяем что время доступно для доставки
     * @param Date date
     */
    isTimeAvailable(date) {
        let tmp = moment(date);
        let times = this.getTimes();

        if (times.indexOf(tmp.format('Y-MM-DD HH:mm')) !== -1) {
            return false;
        }

        return true;
    }

    /**
     * Получаем границы времени для выбранной даты
     */
    getTimeBoundaries(date) {
        let tmpDate = moment(date).format('Y-MM-DD');
        let timesGrouped = this.getTimesGrouped();

        let boundaries = {
            start: this.timeStart.format("HH:mm"),
            end: this.timeEnd.format("HH:mm"),
        };

        // Берем первое и последнее время
        if (typeof timesGrouped[tmpDate] !== 'undefined') {
            boundaries.start = timesGrouped[tmpDate][0];
            boundaries.end = timesGrouped[tmpDate][timesGrouped[tmpDate].length - 1];
        }

        return boundaries;
    }

}


class MoverPickUp extends MoverTime
{

    constructor(timeStart, timeEnd) {
        super(timeStart, timeEnd);
    }

}


class MoverDeliver extends MoverTime
{

    constructor(timeStart, timeEnd) {
        super(timeStart, timeEnd);
    }

}


let pickUp = new MoverPickUp('09:00', '16:00');
let deliver = new MoverDeliver('12:00', '21:00');


export {
    pickUp as MoverPickUp,
    deliver as MoverDeliver,
}