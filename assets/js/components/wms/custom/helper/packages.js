

export default {

    mapPackages: function (formData, packages) {

        $.each(packages, function( index, packageItem ) {
            formData.append('wmsOrder[packages]['+index+'][id]', packageItem.id);

            if (packageItem.packageType && packageItem.packageType.id) {
                formData.append('wmsOrder[packages]['+index+'][packageType]', packageItem.packageType.id);
            } else {
                if (packageItem.packageTypeAnother) {
                    formData.append('wmsOrder[packages][' + index + '][packageTypeAnother]', packageItem.packageTypeAnother);
                }
            }

            formData.append('wmsOrder[packages]['+index+'][count]', packageItem.count);
            formData.append('wmsOrder[packages]['+index+'][weight]', packageItem.weight);
            formData.append('wmsOrder[packages]['+index+'][damaged]', packageItem.damaged ? 1 : 0);

            if (packageItem.packing && packageItem.packing.id) {
                formData.append('wmsOrder[packages]['+index+'][packing]', packageItem.packing.id);
            }
            formData.append('wmsOrder[packages]['+index+'][packingPrice]', 1 * packageItem.packingPrice);

            formData.append('wmsOrder[packages]['+index+'][depth]', packageItem.depth);
            formData.append('wmsOrder[packages]['+index+'][width]', packageItem.width);
            formData.append('wmsOrder[packages]['+index+'][height]', packageItem.height);
            formData.append('wmsOrder[packages]['+index+'][calculateWeight]', packageItem.calculateWeight);
            formData.append('wmsOrder[packages]['+index+'][palletId]', packageItem.palletId);
        });

        return formData;
    },

    /**
     * Проверяем изменились ли списки грузов
     */
    isEquals: function (packagesA, packagesB) {
        packagesA = this.groupEqualsPackages(packagesA);
        packagesB = this.groupEqualsPackages(packagesB);

        if (!packagesA || !packagesB || packagesA.length !== packagesB.length) {
            return false;
        }

        let equals = true;

        $.each(packagesA, ( index, packageA ) => {
            if (!packagesA[index] || !packagesB[index]) {
                equals = false;
                return;
            }
          
            if (!this.isPackageEquals(packagesA[index], packagesB[index])) {
                equals = false;              
            }
            
            if (packagesA[index]['count'] != packagesB[index]['count']) {
                equals = false;              
            }            
        });

        return equals;
    },

    
    /**
     * Проверяем совпадает ли груз
     * Не проверяет, совпадает ли количество и ид
     */
    isPackageEquals: function (packageA, packageB) {
        if (!packageA || !packageB) {
            return false;
        }

        let fields = [
            //'id',
            'packageTypeAnother',
            //'count',
            'weight',
            'damaged',
            'depth',
            'width',
            'height',
            'calculateWeight',
            'palletId',
        ];
        let equals = true;

        
        fields.forEach(field => {
            if (packageA[field] != packageB[field]) {
                equals = false;
            }
        });
      
        // @todo сервер возвращает null
        if (1 * packageA['packingPrice'] != 1 * packageB['packingPrice']) {
            equals = false;
        }            

        let packageTypeA = !packageA['packageType'] ? null : packageA['packageType'].id;
        let packageTypeB = !packageB['packageType'] ? null : packageB['packageType'].id;        
        if (packageTypeA != packageTypeB) {
            equals = false;
        }
      
        
        let packingA = !packageA['packing'] || packageA['packing'].id == null ? null : packageA['packing'].id;
        let packingB = !packageB['packing'] || packageB['packing'].id == null ? null : packageB['packing'].id;
        if (packingA != packingB) {
            equals = false;
        }            
        

        return equals;
    },    
    

    /**
     * Группируем одинаковые грузы
     */
    groupEqualsPackages: function (originalPackages) {
        // Группируем грузы
        let packages = jQuery.extend(true, [], originalPackages);

        let groupedPackages = [];
        let groupedIndex = [];

        packages.forEach((currentPackage, currentPackageIndex) => {
            // Этот груз уже был сгруппирован
            if (groupedIndex.indexOf(currentPackageIndex) !== -1) {
                return;
            }
            groupedIndex.push(currentPackageIndex)

            packages.forEach((element, index) => {
                // Этот груз уже был сгруппирован
                if (groupedIndex.indexOf(index) !== -1) {
                    return;
                }
                
                if (this.isPackageEquals(currentPackage, element)) {
                    groupedIndex.push(index);
                    currentPackage.count += 1;
                }
            });

            groupedPackages.push(currentPackage);
        });

        return groupedPackages;
    },

  
    /**
     * Расчитываем стоимость упаковки груза
     */
    getPackingPrice: function (packageItem, packing) {
        let packingPrice = parseInt(packageItem.packingPrice) ? parseInt(packageItem.packingPrice) : 0;

        if (packing) {
            if (packing.type == 'for_one') {
                packingPrice = packing.price;
            } else if (packing.type == 'to_size') {
                if (packageItem.calculateWeight && packageItem.calculateWeight > 0) {
                    packingPrice = packing.price * packageItem.calculateWeight;
                } else {
                    packingPrice = packing.price * this.getVolume(packageItem);
                }
                if (packing.minPrice * 1 && packingPrice < packing.minPrice * 1) {
                    packingPrice = packing.minPrice * 1;
                }
            }
        }

        return Math.round(packingPrice);
    },  
    
    
    /**
     * Объем груза
     */
    getVolume: function (packageItem) {
        return packageItem.depth * packageItem.height * packageItem.width / 1000 / 1000;
    },    
  
}