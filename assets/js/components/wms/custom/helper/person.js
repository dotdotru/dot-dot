export default {

    mapPerson: function (formData, person) {
        formData.append('person[id]', person.id);
        formData.append('person[uridical]', person.uridical ? 1 : 0);
        if (person.uridical && person.type == null) {
            person.type = 'ООО';
        }
        formData.append('person[type]', person.type);
        formData.append('person[inn]', person.inn);
        formData.append('person[contactName]', person.contactName);
        formData.append('person[contactPhone]', person.contactPhone);
        formData.append('person[contactEmail]', person.contactEmail);
        formData.append('person[series]', person.series);
        formData.append('person[number]', person.number);
        formData.append('person[companyName]', person.companyName);
        formData.append('person[code]', person.code);
        formData.append('person[issuance]', person.issuance);

        // Не показывать в списке контактов
        if (person.hidden) {
            formData.append('person[hidden]', person.hidden);
        }

        return formData;

    },


    isEquals: function (personA, personB) {
        if (!personA || !personB) {
            return false;
        }

        let fields = [
            'uridical',
            'type',
            'inn',
            'contactName',
            'contactPhone',
            'contactEmail',
            'series',
            'number',
            'companyName',
            'code',
            'issuance',
        ];
        let equals = true;

        fields.forEach(field => {
            if (personA[field] !== personB[field]) {
                equals = false;
            }
        });

        return equals;
    },


    /**
     * Провеняем существует ли контакт с такими данными
     * Если организация компании проверяем по инн, если физлицо, по паспорту
     */
    isExists: function (existsPerson, checkPerson) {
        if (!existsPerson || !checkPerson) {
            return false;
        }

        if (existsPerson.uridical && checkPerson.uridical) {
            if (existsPerson.inn == checkPerson.inn) {
                return true;
            }
        } else {
            if (existsPerson.series == checkPerson.series && existsPerson.number == checkPerson.number) {
                return true;
            }
        }

        return false;
    },


    /**
     * Копируем свойства из одного контакта в другой
     */
    copy: function (personTo, personFrom) {
        if (!personTo || !personFrom) {
            return false;
        }

        let fields = [
            'id',
            'uridical',
            'type',
            'inn',
            'contactName',
            'contactPhone',
            'contactEmail',
            'series',
            'number',
            'companyName',
            'code',
            'issuance',
            'hidden'
        ];
        let equals = true;

        fields.forEach(field => {
            personTo[field] = personFrom[field];
        });
    }


}