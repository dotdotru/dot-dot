import PackagesHelper from "./packages";
import ShippingHelper from "./shipping";
import PersonHelper from "./person";

export default {

    /**
     * Проверяем одинаковость полей заказа, которые мы могли поменять в форме
     */
    isEquals: function (orderA, orderB) {
        let fields = [
            'declaredPrice',
        ];
        let equals = true;

        fields.forEach(field => {
            if (orderA[field] !== orderB[field]) {
                equals = false;
            }
        });

        if (!PersonHelper.isEquals(orderA['sender'], orderB['sender'])) {
            equals = false;
        }
        if (!PersonHelper.isEquals(orderA['receiver'], orderB['receiver'])) {
            equals = false;
        }
        if (!PersonHelper.isEquals(orderA['payer'], orderB['payer'])) {
            equals = false;
        }

        if (!ShippingHelper.isShippingEquals(orderA['deliver'], orderB['deliver'])) {
            equals = false;
        }

        if (!PackagesHelper.isEquals(orderA['packages'], orderB['packages'])) {
            equals = false;
        }

        return equals;
    }

}