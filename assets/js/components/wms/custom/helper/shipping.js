
export default {

    mapPickUp: function(formData, pickUp, defaultStockId) {
        formData.append('wmsOrder[pickUp][id]', pickUp.id);
        formData.append('wmsOrder[pickUp][active]', pickUp.active);
        formData.append('wmsOrder[pickUp][date]', pickUp.date);

        if (pickUp.stock && pickUp.stock.toString().trim()) {
            formData.append('wmsOrder[pickUp][stock]', pickUp.stock);
        } else {
            formData.append('wmsOrder[pickUp][stock]', defaultStockId);
        }

        formData.append('wmsOrder[pickUp][address]', pickUp.address);
        formData.append('wmsOrder[pickUp][countLoader]', pickUp.countLoader);
        formData.append('wmsOrder[pickUp][hydroBoard]', pickUp.hydroBoard);

        return formData;
    },

    mapDeliver: function(formData, deliver, defaultStockId) {
        formData.append('wmsOrder[deliver][id]', deliver.id);
        formData.append('wmsOrder[deliver][active]', deliver.active);
        formData.append('wmsOrder[deliver][date]', deliver.date);

        if (deliver.stock && deliver.stock.toString().trim()) {
            formData.append('wmsOrder[deliver][stock]', deliver.stock);
        } else {
            formData.append('wmsOrder[deliver][stock]', defaultStockId);
        }

        formData.append('wmsOrder[deliver][address]', deliver.address);
        formData.append('wmsOrder[deliver][countLoader]', deliver.countLoader);
        formData.append('wmsOrder[deliver][hydroBoard]', deliver.hydroBoard);

        return formData;
    },


    isShippingEquals: function (shippingA, shippingB) {
        if (!shippingA || !shippingB) {
            return false;
        }

        let fields = [
            'id',
            'active',
            'date',
            'stock',
            'address',
            'countLoader',
            'hydroBoard',
        ];
        let equals = true;

        fields.forEach(field => {
            if (shippingA[field] !== shippingB[field]) {
                equals = false;
            }
        });

        return equals;
    }

}