
/**
 * Общий модуль для работы со списком свободных паллет
 */
const store = {
  
    namespaced: true,
  
    state: {
        /**
         * Список свободных паллет для текущего склада 
         */
        freePallets: [],
    },


    getters: {
        freePallets: state => {
            return state.freePallets;
        },
    },

    mutations: {
        setFreePallets(state, freePallets) {
            state.freePallets = freePallets;
        },
        addFreePallet(state, pallet) {
            state.freePallets.push(pallet);
        },
    },

    actions: {
        /**
         * Загрузить список паллет для складов
         */
        loadPallets: function (context, payload) {
            if (!payload || !payload.stockFrom || !payload.stockTo) {
                return false;
            }

            let formData = new FormData();
            formData.append('data[stockFrom]', payload.stockFrom);
            formData.append('data[stockTo]', payload.stockTo);


            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmspallet/free_pallets",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        context.commit('setFreePallets', data.data.pallets);
                        resolve(data.data);
                    }
                });
            });

            return tmpPromise;
        },

        
        /**
         * Создать новую свободную паллету
         */
        createPallet: function (context, payload) {
            if (!payload.stockFrom || !payload.stockTo) {
                return [];
            }

            let formData = new FormData();
            formData.append('data[stockFrom]', payload.stockFrom);
            formData.append('data[stockTo]', payload.stockTo);


            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmspallet/create",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        // ид палетты приходит в неверном поле
                        data.data.externalId = data.data.palletId;
                        context.commit('addFreePallet', data.data);

                        resolve(data.data);
                    } else {
                        reject(false);
                    }
                });
            });

            return tmpPromise;
        },


        /**
         * Получить паллету следующую за указанным ид, если паллеты нет, создать новую
         */
        nextPallet: function (context, payload) {
            let palletId = payload.palletId
            let pallets = context.state.freePallets;
            let nextPallet = null;

            pallets.forEach(function(pallet, i, arr) {
                if (pallet.externalId === palletId && pallets[i + 1] !== undefined) {
                    nextPallet = pallets[i + 1];
                }
            });

            if (nextPallet) {
                return nextPallet;
            }

            return context.dispatch('createPallet', payload);
        },



        /**
         * Получить паллету предыдущую указанному ид
         */
        prevPallet: function (context, payload) {
            let palletId = payload.palletId
            let pallets = context.state.freePallets;

            for (var i in pallets) {
                if (pallets[i].externalId == palletId && pallets[i - 1] !== undefined) {
                    return pallets[i - 1];
                }
            }

            return null;
        }


    },



};


export default store;
