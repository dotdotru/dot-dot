
import ff from '../../../../vueconfig.js';


/**
 * Общий модуль для работы с заказами
 */
const store = {
  
    namespaced: true,
  
    state: {
        /**
         * Список загруженных заказов
         */
        orders: [],
        
        /**
         * Ведется загрузка заказа
         */
        ordersLoaded: false,
    },

    getters: {
        orders: state => {
            return state.orders;
        },
        ordersLoaded: state => {
            return state.ordersLoaded;
        },
        order: state => (orderId) => {
            let orders = state.orders;
            for (let i in orders) {
                if (orders[i].id == orderId) {
                    return orders[i];
                }
            }
            return false;
        },                
        
        fileByGroup: (state, getters) => (type, orderId) => {
            let order = getters.order(orderId);
            let result = null;

            $.each(order.files, function( index, file ) {
                if (file.group.slug == type) {
                    result = file;
                }
            });

            return result;
        },        
    },

    mutations: {
        /**
         * Очистить список заказов
         */
        clearOrders(state) {
            state.orders = [];
        },

        setOrdersLoaded(state, status) {
            state.ordersLoaded = status;
        },


        /**
         * Добавить заказ в список заказов, если такой заказ уже есть, он будет перезаписан
         */
        addOrder(state, order) {
            let orders = [];


            if (!order.receiver || !order.receiver.id) {
                order.receiver = ff.Person();
            }
            if (!order.sender || !order.sender.id) {
                order.sender = ff.Person();
            }
            if (!order.payer || !order.payer.id) {
                order.payer = ff.Person();
            }

            // Перезаписываем заказ, если он уже есть в списке
            for (let i in state.orders) {
                if (state.orders[i].id !== order.id) {
                    orders.push(state.orders[i]);
                }
            }

            orders.push(order);
            state.orders = orders;
        }
    },




    actions: {
        /**
         * Загружаем заказ с указанным id
         */
        loadOrder: (context, orderId) => {
            context.commit('setOrdersLoaded', false);

            let formData = new FormData();
            formData.append('orderId', orderId);
            
            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'GET',
                    url: "/api/wms/wmsorder/order/" + orderId,
                    dataType: "json",
                }).done(function(result) {
                    context.commit('addOrder', result.data.order);
                    context.commit('setOrdersLoaded', true);
                    
                    resolve(result.data)
                });
            })            
            
            return tmpPromise;             
        },

        
        /**
         * Изменения статуса нового заказа на "принят"
         */
        acceptedOrder: function (context, payload) {
            let orderId = payload.orderId;
            var formData = new FormData();

            
            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmsorder/order-new-accepted/" + orderId,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(result) {
                    resolve(result)
                });
            })            
            
            return tmpPromise;             
        },

    },



};


export default store;
