
/**
 * Общий модуль для работы конфигом заказов
 */
const store = {
  
    namespaced: true,
  
    state: {
        directions: [],
        stocks: [],
        fileGroups: [],
        organizationTypes: [],
        packageTypes: [],
        packings: [],
        currentStock: {},
        settings: {},
    },


    getters: {
        directions: state => {
            return state.directions;
        },
        stocks: state => {
            return state.stocks;
        },        
        fileGroups: state => {
            return state.fileGroups;
        },
        organizationTypes: state => {
            return state.organizationTypes;
        },        
        packageTypes: state => {
            return state.packageTypes;
        },
        packings: state => {
            return state.packings;
        },
        currentStock: state => {
            return state.currentStock;
        },
        settings: state => {
            return state.settings;
        },
        stockById: state => stockId => {
            let stocks = state.stocks;
            let stockFromObject = null;

            for (let i in stocks) {
                if (stocks[i].id === stockId) {
                    stockFromObject = stocks[i];
                }
            }

            return stockFromObject;
        },

    },

    mutations: {
        setDirections(state, directions) {
            state.directions = directions;
        },
        setStocks(state, stocks) {
            state.stocks = stocks;
        },
        setFileGroups(state, fileGroups) {
            state.fileGroups = fileGroups;
        },
        setOrganizationTypes(state, organizationTypes) {
            state.organizationTypes = organizationTypes;
        },
        setPackageTypes(state, packageTypes) {
            packageTypes.push({id: null, title: 'Другое'});
            state.packageTypes = packageTypes;
        },
        setPackings(state, packings) {
            state.packings = packings;
        },
        setCurrentStock(state, stock) {
            state.currentStock = stock;
        },
        setSettings(state, settings) {
            state.settings = settings;
        },
    },


    actions: {
        loadConfig: (context, payload) => {
            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    url: '/api/wms/wmsorder/config',
                    method: 'GET',
                    data: {},
                }).done(function(result) {
                    context.commit('setDirections', result.data.params.directions);
                    context.commit('setStocks', result.data.params.stocks);
                    context.commit('setOrganizationTypes', result.data.params.organizationTypes);

                    context.commit('setPackageTypes', result.data.params.packageTypes);
                    context.commit('setPackings', result.data.params.packings);
                    context.commit('setCurrentStock', result.data.params.currentStock);

                    context.commit('setSettings', result.data.settings);


                    resolve(result.data)
                });
            });

            return tmpPromise;
        },
    },



};


export default store;
