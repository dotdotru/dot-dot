
import ff from '../../../../vueconfig.js';
import PersonHelper from '../../helper/person';


/**
 * Общий модуль для работы с контактными данными
 */
const store = {
  
    namespaced: true,
  
    state: {
        /**
         * Список контактных данных
         */
        persons : [],
    },
    getters: {
        /**
         * Все контакты пользователя
         */
        persons: state => {
            return state.persons;
        },
        /**
         * Для списка контактов
         */
        publicPersons: state => {
            let persons = [];

            state.persons.forEach(person => {
               if (!person.hidden) {
                   persons.push(person);
               }
            });

            return persons;
        },
        /**
         * Скрытые контакты
         */
        hiddenPersons: state => {
            let persons = [];

            state.persons.forEach(person => {
                if (person.hidden) {
                    persons.push(person);
                }
            });

            return persons;
        },
        personById: state => (personId) => {
            let persons = state.persons;
            for (let i in persons) {
                if (persons[i].id === personId) {
                    return persons[i];
                }
            }
            return false;
        },
        publicPersonById: state => (personId) => {
            let persons = state.persons;
            for (let i in persons) {
                if (persons[i].id === personId && !persons[i].hidden) {
                    return persons[i];
                }
            }
            return false;
        },
    },

    mutations: {

    },

    actions: {
        
        setPersons (context, persons) {
            context.state.persons = persons;
        },

        /**
         * Добавить контактные данные в список
         */        
        addPerson: function (context, person) {
            let persons = [];

            // Перезаписываем контактные данные, если он уже есть в списке
            for (let i in context.state.persons) {
                if (context.state.persons[i].id !== person.id) {
                    persons.push(context.state.persons[i]);
                }
            }

            persons.push(person);

            context.state.persons = persons;
        },
        
        /**
         * Сохранить контактные данные
         */
        savePerson: function(context, payload) {
            let person = payload.person;
            let userId = payload.userId;

            if (!person) {
                console.log('Try to save empty person');
                return;
            }

            // Проверяем, есть ли контакт с таким ид в списке, и если они одинаковые, не сохраняем
            if (person.id) {
                let originalPerson = context.getters['personById'](person.id);
                if (originalPerson && PersonHelper.isEquals(person, originalPerson)) {
                    return person;
                }
            }

            let formData = new FormData();
            PersonHelper.mapPerson(formData, person);
            
            formData.append('user[id]', userId);

            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    method: 'POST',
                    url: " /api/order/customer/save-person-overwrite",
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        person.id = data.data.id;
                        context.dispatch('addPerson', person).then(() => {
                            resolve(person);
                        });
                    } else {
                        reject(data);
                    }
                }).catch(error => {
                    reject(error);
                });
            }).then(data => { return data; }, error => {
                ff.openPopup('Произошла ошибка при сохранении контакта');
            });
            
            return tmpPromise;            
        },

        /**
         * Синхронизируем контакт со списком контактов, чтобы не было коллизий
         * Возращаем нужно ли предложить пользователю пересохранить контакт
         *
         * @todo подумать можно ли оптимизировать эту логику
         */
        synchPersonWithList (context, person) {
            let needOverwrite = false;
            let founded = false;

            if (person.id) {
                // Если у этого контакта установлен id, но пользователь поменял ключевые данные, нужно сохранить новый контакт
                let currentContact = context.getters['personById'](person.id);
                if (!PersonHelper.isExists(currentContact, person)) {
                    person.hidden = false;
                    person.id = null;
                } else if (person.hidden) {
                    // Если контакт скрыт, ничего делать не нужно, он просто пересохранится
                    return false;
                }
            }

            // Скрытые и открытые списки нужно обрабатывать одтельно
            if (!person.hidden) {
                founded = false;
                context.getters.publicPersons.forEach(function(existsPerson, index) {
                    // Так как в списке есть контакты, которые дублируются, при первом совпадении подменяем ид и не перезаписываем
                    if (PersonHelper.isExists(existsPerson, person) && PersonHelper.isEquals(existsPerson, person)) {
                        person.id = existsPerson.id;
                        needOverwrite = false;
                        founded = true;
                    }
                });

                if (!founded) {
                    context.getters.publicPersons.forEach(function (existsPerson, index) {
                        // Если совпали ключевые данные и данные пользователя не равны, нужно перезаписать данные
                        if (PersonHelper.isExists(existsPerson, person) && !PersonHelper.isEquals(existsPerson, person)) {
                            person.id = existsPerson.id;
                            needOverwrite = true;
                        }
                    });
                }
            } else {

            }

            return needOverwrite;
        },

        /**
         * Удалить контактные данные
         */
        removePerson (context, person) {
            if (!person.id) {
                return;
            }
          
            context.state.persons.forEach(function(personItem, i) {
                if (personItem.id == person.id) {
                    context.state.persons.splice(i, 1);
                }
            });

            
            let tmpPromise = new Promise((resolve, reject) => {
                $.ajax({
                    url: "/api/order/customer/remove-person/" + person.id,
                    cache: false,
                    async: true,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    resolve(true)
                });            
            });
            
            return tmpPromise;                        
        }

    },



};


export default store;
