
import configStore from './modules/wms.order-config.store';
import ordersStore from './modules/wms.order.store';
import palletesStore from './modules/wms.free-palletes.store';
import personStore from './modules/wms.person.store';

import PackagesHelper from '../helper/packages';
import ShippingHelper from '../helper/shipping';
import OrderHelper from '../helper/order';

import Vue from 'vue';

import ff from '../../../vueconfig.js';

const store = {
    state: {
        /**
         *  Версия заказа для редактирования
         */
        dirtyOrder: null,
    },

    modules: {
        config: configStore,
        orders: ordersStore,
        freePallets: palletesStore,
        persons: personStore
    },

    getters: {
        stockFrom: state => {
            return state.dirtyOrder.stockFrom ? state.dirtyOrder.stockFrom.id : false;
        },
        stockTo: state => {
            return state.dirtyOrder.stockTo ? state.dirtyOrder.stockTo.id : false;
        },
        loaded: state => {
            return state.config.loaded && state.orders.loaded;
        },
        dirtyOrder: state => {
            return state.dirtyOrder;
        },
        originalOrder: (state, getters) => (orderId) => {
            return getters['orders/order'](orderId);
        },        
    },
    mutations: {


    },
    actions: {
        /**
         * Загружаем конфиги и заказ
         */
        init (context, payload) {
            context.dispatch('config/loadConfig').then(response => {
                context.dispatch('loadOrder', payload.orderId).then(data => {

                });
            }, error => {});
        },

        
        /**
         * Переопределяем метод из основного стораджа, чтобы получить список контактов
         */
        loadOrder: (context, orderId) => {
            return context.dispatch('orders/loadOrder', orderId).then(response => {
                if (response.persons && response.persons.length) {
                    // @todo сделать нормальное api для загрузки контактов
                    context.dispatch('persons/setPersons', response.persons);
                }
            }, error => {});
        },


        /**
         * Устанавливаем объект для редактирования
         */
        setDirtyOrder(context, originalOrder) {
            let order = jQuery.extend(true, {}, originalOrder);

            if (!order.stockFrom) {
                let currentStock = context.getters['config/currentStock'];
                order.stockFrom = stockFromObject;
            }            
            
            // Нужно для работы с формой
            if (!order.pickUp || typeof order.pickUp === 'undefined') {
                order.pickUp = ff.Shipping();
                order.pickUp.stock = order.stockFrom.id;
                order.pickUp.stockObject = order.stockFrom;                            
            }
            if (!order.deliver || typeof order.deliver === 'undefined') {
                order.deliver = ff.Shipping();
            }

            if (!order.deliver.stock && order.stockTo) {
                order.deliver.stock = order.stockTo.id;
                order.deliver.stockObject = order.stockTo;
            }

            // Добавляем дефолтные данные из заказа юзера
            if (order.packages && !order.packages.length) {
                if (order.orderPackages && order.orderPackages.length) {
                    order.orderPackages.forEach(orderPackage => {
                        let packageItem = Object.assign({}, ff.WmsPackage());

                        if (orderPackage.packageTypeAnother && orderPackage.packageTypeAnother.length) {
                            packageItem.packageType = null;
                            packageItem.packageTypeAnother = orderPackage.packageTypeAnother;
                            order.packages.push(packageItem);
                        } else {
                            packageItem.packageType = {id: orderPackage.packageType.id};
                            order.packages.push(packageItem);
                        }
                        if (orderPackage.packing && orderPackage.packing.id) {
                            packageItem.packing = orderPackage.packing;
                        }
                    });
                }
            }

            // Нужно для отслеживания изменения id груза
            for (let i in order.packages) {
                order.packages[i].packageId = null;
            }

            if (order.packages.length === 0) {
                let packageItem = Object.assign({}, ff.WmsPackage());
                order.packages.push(packageItem);
            }

            // Группируем одинаковые заказы
            order.packages = PackagesHelper.groupEqualsPackages(order.packages);

            context.state.dirtyOrder = order;

            if (order.stockTo && order.stockFrom) {
                context.dispatch('freePallets/loadPallets', {
                    stockTo: order.stockTo.id,
                    stockFrom: order.stockFrom.id,
                }).then(pallets => {
                    // Если поменялся ид склада доставки, надо переназначить паллеты
                    context.dispatch('regenerateOrdersPallets');
                }, error => {
                });
            }

            context.dispatch('regeneratePackagesIds');
        },


        /**
         *  Сохраняем отредактированный заказ. 
         */
        saveDirtyOrder (context, payload) {
            let senderId = null;
            let receiverId = null;
            let payerId = null;
            let order = context.state.dirtyOrder;


            let promises = [];
            let tmpPromise = null;

            // Сохраняем контактные данные
            tmpPromise = context.dispatch('persons/savePerson', {'person': order.sender, 'userId': order.user.id}).then(person => {
                senderId = person.id;
            }, error => {}).then(() => {
                return context.dispatch('persons/savePerson', {'person': order.receiver, 'userId': order.user.id}).then(person => {
                    receiverId = person.id;
                }, error => {})
            }).then(() => {
                return context.dispatch('persons/savePerson', {'person': order.payer, 'userId': order.user.id}).then(person => {
                    payerId = person.id;
                }, error => {});
            });

            promises.push(tmpPromise);


            // Когда все контакты созданные, сохраняем заказ
            tmpPromise = new Promise((resolve, reject) => {
                Promise.all(promises).then(values => {
                    let $this = this;
                    let formData = new FormData();

                    PackagesHelper.mapPackages(formData, order.packages);
                    ShippingHelper.mapPickUp(formData, order.pickUp);
                    ShippingHelper.mapDeliver(formData, order.deliver);

                    formData.append('wmsOrder[stockFrom]', order.pickUp.stock);
                    formData.append('wmsOrder[stockTo]', order.deliver.stock);

                    formData.append('wmsOrder[sender]', senderId);
                    formData.append('wmsOrder[receiver]', receiverId);
                    formData.append('wmsOrder[payer]', payerId);


                    formData.append('wmsOrder[declaredPrice]', order.declaredPrice);

                    $.ajax({
                        method: 'POST',
                        url: "/api/wms/wmsorder/order-new-save/" + order.id,
                        cache: false,
                        async: true,
                        data: formData,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                    }).done(function(data) {
                        if (data.status) {
                            context.dispatch('loadOrder', order.id).then(() => {
                                resolve(true);  
                            });                            
                        } else if (data.data) {
                            let errorPallettedIds = [];

                            if (data.data.errorPallettedIds) {
                                errorPallettedIds = data.data.errorPallettedIds;
                            }
                            if (errorPallettedIds) {
                                ff.openPopup('Ошибка паллетирования', 'Превышен общий вес в паллетах: ' + errorPallettedIds.join(', '));
                            }
                            reject(false);
                        }
                    });
                }, error => {
                    reject(false);
                });
            });
            
            return tmpPromise;                  
        },


        
        /**
         * Откуда отправляется заказ
         */
        setStockFrom(context, stockFromId) {
            let stockFromObject = context.getters['config/stockById'](stockFromId);
            let dirtyOrder =  context.state.dirtyOrder;

            if (dirtyOrder) {
                dirtyOrder.stockFrom = stockFromObject;
                dirtyOrder.pickUp.stock = stockFromId;
                dirtyOrder.pickUp.stockObject = context.state.stockFromObject;
            }
            
            if (dirtyOrder && dirtyOrder.stockTo && dirtyOrder.stockFrom !== stockFromId) {
                context.dispatch('freePallets/loadPallets', {
                    stockTo: dirtyOrder.stockTo.id,
                    stockFrom: dirtyOrder.stockFrom.id,
                }).then(pallets => {
                    // Если поменялся ид склада доставки, надо переназначить паллеты
                    context.dispatch('regenerateOrdersPallets', {force: true});
                }, error => {});
            }                
        },


        /**
         * Куда отправляется заказ
         */
        setStockTo(context, stockToId) {
            let stockToObject = context.getters['config/stockById'](stockToId);

            if (context.state.dirtyOrder) {
                context.state.dirtyOrder.stockTo = stockToObject;
                context.state.dirtyOrder.deliver.stock = stockToId;
                context.state.dirtyOrder.deliver.stockObject = context.state.stockToObject;
            }
            
            if (context.state.dirtyOrder && context.state.dirtyOrder.stockTo&& context.state.dirtyOrder.stockTo !== stockToId) {
                context.dispatch('freePallets/loadPallets', {
                    stockTo: context.state.dirtyOrder.stockTo.id,
                    stockFrom: context.state.dirtyOrder.stockFrom.id,
                }).then(pallets => {
                    // Если поменялся ид склада доставки, надо переназначить паллеты
                    context.dispatch('regenerateOrdersPallets', {force: true});
                }, error => {});
            }            
        },

        
        /**
         * После смены склада нужно переназначить грузам паллеты от новых складов
         */
        regenerateOrdersPallets(context, payload) {
            let order = context.state.dirtyOrder;
            let originalOrder = context.getters['originalOrder'](context.state.dirtyOrder.id);
            let freePallets = context.state.freePallets.freePallets;

            order.packages.forEach((packageItem, packageIndex) => {
                let founded = false;

                if ((!payload || !payload.force) && packageItem.palletId) {
                    founded = true;
                }

                freePallets.forEach((freePalletsItem) => {
                    if (freePalletsItem.externalId === packageItem.palletId) {
                        founded = true;
                    }
                });

                if (!founded) {
                    // Если не указана паллета, создаем новую
                    if (order.stockFrom && order.stockTo && !packageItem.palletLoading) {
                        packageItem.palletLoading = true;

                        let params = {stockFrom: order.stockFrom.id, stockTo: order.stockTo.id};
                        context.dispatch('freePallets/createPallet', params).then(data => {
                            if (data.palletId) {
                                packageItem.palletId = data.palletId;
                            }

                            packageItem.palletLoading = false;
                        });
                    }
                }
            });
        },

        
        /**
         * Перегенерируем идентификаторы грузов заказа
         */
        regeneratePackagesIds (context, payload) {
            let order = context.state.dirtyOrder;
            if (!order) {
                return;
            }

            let packages = order.packages;

            let currentPlace = 1;
            packages.forEach(function(packageItem, index) {
                let count = parseInt(packageItem.count);
                order.packages[index].packageId = order.externalId + '/' + currentPlace + (count > 1 ? '-' + (currentPlace + count - 1) : '');

                currentPlace += (count > 1 ? count : 1);
            });

            context.state.dirtyOrder = order;
        },


        /**
         * Добавляем новый груз в заказ
         */
        createPackage: function (context, payload) {
            let $this = this;
            let dirtyOrder = context.state.dirtyOrder;

            let packageItem = Object.assign({}, ff.WmsPackage());
            packageItem.packageId = null;
            dirtyOrder.packages.splice(dirtyOrder.packages.length + 1, 0, packageItem);

            if (dirtyOrder.packages.length) {
                if (dirtyOrder.packages[0].packageType) {
                    packageItem.packageType = dirtyOrder.packages[0].packageType;
                }
                if (dirtyOrder.packages[0].packageTypeAnother) {
                    packageItem.packageTypeAnother = dirtyOrder.packages[0].packageTypeAnother;
                }
                if (dirtyOrder.packages[0].packing) {
                    packageItem.packing = dirtyOrder.packages[0].packing;
                }
            }

            context.state.dirtyOrder = dirtyOrder;
            context.dispatch('regeneratePackagesIds');
            context.dispatch('regenerateOrdersPallets');

            return false;
        },

        
        /**
         * Удаляем груз с указанным индексом
         */
        removePackage: function (context, index) {
            let dirtyOrder = context.state.dirtyOrder;

            if (dirtyOrder.packages.length <= 1) {
                dirtyOrder.packages.splice(index, 1);
                state.dispatch('createPackage');
                return;
            }

            dirtyOrder.packages.splice(index, 1);
            context.state.dirtyOrder = dirtyOrder;
            context.dispatch('regeneratePackagesIds');

            return false;
        },

    },



};


export default store;
