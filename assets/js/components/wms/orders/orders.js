import Vue from 'vue';
import Orders from './orders.vue';
import OrderEdit from './order/orderEdit.vue';
import OrderCreate from './order/orderCreate.vue';
import OrderDelivered from './order/orderDelivered.vue';

$('[data-attach-vue="wms-orders"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Orders/>',
        data: {

        },
        components: { Orders }
    });

});

$('[data-attach-vue="wms-order-edit"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<order-edit :printdata="printdata" :orderId="orderId"/>',
        data: {
            'orderId': item.data('order-id'),
            'printdata' : item.data('print'),
        },
        components: { OrderEdit }
    });

});

$('[data-attach-vue="wms-order-create"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<order-create :printdata="printdata" :orderId="orderId"/>',
        data: {
            'orderId': item.data('order-id'),
            'printdata' : item.data('print'),
        },
        components: { OrderCreate }
    });

});

$('[data-attach-vue="wms-order-delivered"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<order-delivered :orderId="orderId"/>',
        data: {
            'orderId': item.data('order-id'),
        },
        components: { OrderDelivered }
    });

});