var wmsBatchArrived = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatcharrived',
    data: {
        loading: true,
        orderData: null,
        order: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPallets: [],
        maskfilter: filter(),
        batchSaved: false,
        palletId: null,
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('#wmsbatcharrived').css('visibility', 'visible');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        batchSave: function($event) {
            var $this = this;
            var errorWeight = false;

            var formData = new FormData();

            $.each($this.order.pallets, function( indexPallet, pallet ) {
                formData.append('batch[pallet]['+indexPallet+'][id]', pallet.id);
                formData.append('batch[pallet]['+indexPallet+'][damaged]', pallet.damaged ? 1 : 0);
                formData.append('batch[pallet]['+indexPallet+'][arriveWeight]', pallet.arriveWeight);

                if (!pallet.arriveWeight) {
                    errorWeight = true;
                }

                $.each(pallet.packages, function( indexPackage, package ) {
                    formData.append('batch[pallet]['+indexPallet+'][packages]['+indexPackage+'][id]', package.id);
                    formData.append('batch[pallet]['+indexPallet+'][packages]['+indexPackage+'][arriveDamaged]', package.arriveDamaged ? 1 : 0);
                });
            });

            if (errorWeight) {
                openPopup('Ошибка сохранения', 'Не указан вес всех паллет');
                return false;
            }

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-arrived/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                if ($event) {
                    $this.batchSaved = true;
                }
            }

            return false;
        },

        batchArrived: function($event) {
            var $this = this;

            var formData = new FormData();
            formData.append('batch[arrived]', true);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-arrived/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                location.href = '/wms/batch';
            }

            return false;
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('batch[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-upload-file/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.batchSave();
                $this.loadOrder();
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        addPallet: function (id) {
            var $this = this;
            if ($.inArray(id, this.selectPallets) == -1) {
                $this.selectPallets.push(id);
            }

            return $this.selectPallets;
        },

        removePallet: function (id) {
            var $this = this;
            $.each($this.selectPallets, function( index, palletId ) {
                if (id == palletId) {
                    $this.selectPallets.splice(index, 1);
                }
            })

            return $this.selectPallets;
        },

        togglePalletActive: function (pallet) {
            var $this = this;
            if ($this.isPalletActive(pallet)) {
                $this.removePallet(pallet);
            } else {
                $this.addPallet(pallet);
            }

            return $this.selectPallets;
        },

        isPalletActive: function (pallet) {
            var $this = this;
            if ($.inArray(pallet, $this.selectPallets) != -1) {
                return true;
            }

            return false;
        },

        selectAllPallets: function () {
            var $this = this;
            $.each($this.order.palletIds, function( index, palletId ) {
                if (!$this.isPalletActive(palletId.value)) {
                    $this.addPallet(palletId.value);
                }
            })

            return false;
        },

        showDetailPallet: function (pallet) {
            if(this.palletId != pallet.id) {
                this.palletId = pallet.id;
            } else {
                this.palletId = null;
            }

            return false;
        },

        isShowDetailPallet: function (pallet) {
            if (this.palletId == pallet.id) {
                return true;
            }

            return false;
        },

        toogleShowDetailPallet: function (pallet) {
            if(this.isNotEqualPallet(pallet)) {
                pallet.damaged = true;
                this.palletId = pallet.id;

                if (pallet.packages.length == 1) {
                    pallet.packages[0].arriveDamaged = true;
                }

            } else {
                this.palletId = null;

                $.each(pallet.packages, function( index, package ) {
                    package.arriveDamaged = false;
                })
            }

            return false;
        },

        isNotEqualPallet: function (pallet) {
            if (pallet.damaged) {
                return true;
            }

            if (pallet.weight != pallet.arriveWeight && pallet.arriveWeight > 0) {
                return true;
            }

            return false;
        },

        loadOrder: function() {

            var $this = this;

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch/"+$this.order.externalId,
                dataType: "json",
                success: function($data){
                    $this.order = $data.data;
                }
            });
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();
    },
    computed: {
        totalSelectPallet: function () {
            return this.selectPallets.length;
        },
    },
    watch: {
        'filter': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
        'orderData': function(val) {
            this.loading = false;
            this.order = JSON.parse(val);
        },
    }
})

$(function(){

    /*$(".js-select-all").change(function () {
        $('.order-table thead').toggleClass('blured');
        $(".order-table td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        }
    });

    $(".js-undelete").on("click", function() {
        $('.order-number')
            .find(":checkbox:checked")
            .closest('tr')
            .show();
        $('.order-table thead').removeClass('blured');
        $('.order-number input').prop('checked', false);
        $(".js-select-all").prop('checked', false);
    });

    $('body').on('click', '.order-number', function(){
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        } else {
            $('.order-table thead').removeClass('blured');
        }
    })*/

})
