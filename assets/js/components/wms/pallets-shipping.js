var wmsPalletsShipping = new Vue({
    delimiters: ['${', '}'],
    el: '#wms-pallets-shipping',
    data: {
        timeout: null,
        edit: false,
        filter: {
            direction: null,
        },
        weight: null,
        stockTo: null,
        stocks: [],
        selectOrders: [],
        packages: [],
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        directions: [],
        loadPackagesEmpty: true,
        mask: mask(),
        maskfilter: filter(),
        sort: {
            by: 'id',
            direction: 'desc'
        },
        selectAll: false,
        palletId: '',
        freePallets: []
    },
    methods: {
        showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.loadPackages();
            }
        },

        isSelectAll: function(order) {
            var $this = this;
            var result = true;
            $.each($this.orders, function( index, order ) {
                if (!$this.isSelectOrder(order)) {
                    result = false;
                }
            });

            return result;
        },

        unselectOrders: function() {
            this.selectOrders = [];
        },

        selectAllOrders: function(val) {
            var $this = this;

            if ($this.selectAll) {
                $this.unselectOrders();
            } else {
                $this.unselectOrders();
                $.each($this.orders, function( index, order ) {
                    $this.selectOrder(order);
                });
            }
        },

        selectOrder: function(order) {
            var key = this.selectOrders.indexOf(order.id);
            if (key == -1) {
                this.selectOrders.push(order.id);
            } else {
                this.selectOrders.splice(key,1);
            }
        },

        isSelectOrder: function(order) {
            var result = false;
            var key = this.selectOrders.indexOf(order.id);
            if (key == -1) {
                result = false;
            } else {
                result = true;
            }

            return result;
        },

        palleted: function($event) {
            var $this = this;

            var button = $($event.target);
            if (button.hasClass('disabled')) {
                return false;
            }

            button.addClass('disabled');

            this.$validator.validateAll().then(function(result) {

                if(!result) {
                    button.removeClass('disabled');
                    return;
                }

                if ($this.selectOrders.length == 0) {
                    button.removeClass('disabled');
                    openPopup('Выберите места');
                    return false;
                }

                var formData = new FormData();

                formData.append('pallet[palletId]', $this.palletId);

                $.each($this.selectOrders, function( index, package ) {
                    formData.append('pallet[packages]['+index+']', package);
                })

                $('#wms-pallets-shipping .create-pallets .styled-input-number input').removeClass('error');

                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmspallet/add-packages",
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status == true) {
                        button.removeClass('disabled');
                        $this.weight = 0;
                        $this.selectOrders = [];
                        $this.loadPackages();
                        wmsPalletsShippingList.loadOrders();
                    } else if (data.data) {
                        button.removeClass('disabled');

                        var errorPallettedIds = [];

                        if (data.data.errorPallettedIds) {
                            errorPallettedIds = data.data.errorPallettedIds;
                        }

                        if (errorPallettedIds) {
                            $('#wms-pallets-shipping .create-pallets .styled-input-number input').addClass('error');
                            openPopup('Ошибка паллетирования', 'Превышен общий вес в паллете '+errorPallettedIds);
                        }
                    }
                });

            });

            return false;
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        loadPackages: function() {

            var $this = this;

            var formData = new FormData();

            if ($this.stockTo) {
                formData.append('filter[stockTo]', $this.stockTo);
            }

            //formData.append('filter[palletId]', null);
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmspackage/packages-not-in-pallet",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result.data) {
                $this.packages = result.data.packages;
                $this.pagination = result.data.pagination;
                $this.loadPackagesEmpty = false;
            }
        },

        getData: function () {
            var jqxhr = $.ajax({
                url: "/api/wms/wmsorder/config",
                cache: true,
                async: false
            });

            var data = jqxhr.responseJSON.data;

            this.directions = data.params.directions;
            this.packageTypes = data.params.packageTypes;
            this.packings = data.params.packings;
            this.stocks = data.params.stocks;
            this.currentStock = data.params.currentStock;

        },

        getDirection: function (id) {
            var result = null;

            if (!id) {
                id = this.direction;
            }

            $.each(this.directions, function( index, item ) {
                if (item.id == id) {
                    result = item;
                }
            })

            return result;
        },

        getStocksFrom: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityFrom.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },

        getStocksTo: function () {
            var $this = this;
            var result = [];

            $.each(this.stocks, function( index, item ) {
                if (item.id != $this.currentStock.id) {
                    result.push(item);
                }
            });

            return result;
        },

        upPalletId: function (palletId) {
            var $this = this;
            var nextPalletId = null;

            $.each($this.freePallets, function( index, pallet ) {
                if ($this.palletId == pallet.externalId) {
                    if ($this.freePallets[index + 1] != undefined) {
                        nextPalletId = $this.freePallets[index + 1].externalId;
                    }
                }
            })

            if (!nextPalletId) {
                nextPalletId = $this.createPallet();
            }

            if (nextPalletId) {
                $this.palletId = nextPalletId;
            }

            $this.loadPallets();
        },

        downPalletId: function (package) {
            var $this = this;
            var prevPalletId = null;

            $.each($this.freePallets, function( index, pallet ) {
                if ($this.palletId == pallet.externalId) {
                    if ($this.freePallets[index - 1] != undefined) {
                        prevPalletId = $this.freePallets[index - 1].externalId;
                    }
                }
            })

            if (prevPalletId) {
                $this.palletId = prevPalletId;
            }
        },

        isCanSelectedPackage: function (package) {
            var $this = this;
            var result = false;
            var firstPackage = $this.getPackageById($this.selectOrders.slice(0, 1));

            if (firstPackage) {
                if (firstPackage.stockFrom.id == package.stockFrom.id
                    && firstPackage.stockTo.id == package.stockTo.id
                ) {
                    result = true;
                }
            } else {
                result = true;
            }

            return result;
        },

        getPackageById: function (id) {
            var $this = this;
            var result = null;

            $.each($this.packages, function( index, package ) {
                if (parseInt(package.id) == parseInt(id)) {
                    result = package;
                }
            });

            return result;
        },

        resetPallet: function () {
            this.palletId = null;
        },

        loadPallets: function () {
            var $this = this;

            var formData = new FormData();

            formData.append('data[stockTo]', $this.stockTo);

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/free_pallets",
                cache: false,
                async: true,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(data) {
                if (data.status) {
                    $this.freePallets = data.data.pallets;
                    $this.checkPalletId();
                }
            });
        },

        checkPalletId: function () {
            var $this = this;

            if ($this.palletId) {
                return false;
            }

            if ($this.freePallets.length) {
                var firstPallet = $this.freePallets[0];
            } else {
                $this.palletId = $this.createPallet();
                $this.loadPallets();
                return;
            }

            $this.palletId = firstPallet.externalId;
        },

        createPallet: function () {
            var $this = this;
            var formData = new FormData();

            var stockTo = $this.stockTo;

            formData.append('data[stockTo]', stockTo);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/create",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON.data;

            return data.palletId;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },
        isErrorWeightPallet: function(pallet) {
            if (pallet.weight && (parseFloat(pallet.weight) < 30 || parseFloat(pallet.weight) > 1250)) {
                return true;
            }
            return false;
        },
    },
    mounted: function() {
        this.htmlUp();

        this.loadPackagesEmpty = true;
        this.getData();
        this.loadPackages();

        this.weight = 0;

        $('#wms-pallets-shipping').css('visibility', 'visible');
    },
    computed: {
        stocksFrom: function () {
            return this.getStocksFrom();
        },
        stocksTo: function () {
            return this.getStocksTo();
        }
    },
    watch: {
        'stockTo': function(val) {
            if (val) {
                this.loadPackages();
                this.loadPallets();
                this.palletId = null;
            }
        },
        'filter.palletId': function(val) {
            this.loadPackages();
        },
        'sort.by': function(val) {
            this.loadPackages();
        },
        'sort.direction': function(val) {
            this.loadPackages();
        },
        'selectOrders': function(val) {
            var $this = this;
            $this.weight = 0;
            $.each($this.selectOrders, function( index, packageId ) {
                var package = $this.getPackageById(packageId);
                $this.weight += package.weight;
            })
        },
        'q': function() {
            var self = this;
            clearTimeout(self.timeout);
            self.timeout = setTimeout(function () {
                self.loadPallets();
            }, 600);
        },
    }
})
