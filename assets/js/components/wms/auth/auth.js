import Vue from 'vue';
import Auth from './auth.vue';

$('[data-attach-vue="wms-auth"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Auth/>',
        data: {

        },
        components: { Auth }
    });

});