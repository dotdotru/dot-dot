var wmsBatchArrived = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatcharrived',
    data: {
        loading: true,
        orderData: null,
        order: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPackages: [],
        maskfilter: filter(),
        batchSaved: false,
        packageId: null,
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('#wmsbatcharrived').css('visibility', 'visible');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        batchSave: function($event) {
            var $this = this;
            var errorWeight = false;

            var formData = new FormData();

            $.each($this.order.packages, function( indexPackage, package ) {
                formData.append('batch[package]['+indexPackage+'][id]', package.id);
                formData.append('batch[package]['+indexPackage+'][damaged]', package.damaged ? 1 : 0);
                formData.append('batch[package]['+indexPackage+'][weight]', package.weight);

                $.each(package.packages, function( indexPackage, package ) {
                    formData.append('batch[package]['+indexPackage+'][packages]['+indexPackage+'][id]', package.id);
                    formData.append('batch[package]['+indexPackage+'][packages]['+indexPackage+'][arriveDamaged]', package.arriveDamaged ? 1 : 0);
                });
            });

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-mover-arrived/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                if ($event) {
                    $this.batchSaved = true;
                }
            }

            return false;
        },

        batchArrived: function($event) {
            var $this = this;

            var formData = new FormData();
            formData.append('batch[arrived]', true);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-mover-arrived/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                location.href = '/wms/batch';
            }

            return false;
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('batch[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-upload-file/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.batchSave();
                $this.loadOrder();
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        addPackage: function (id) {
            var $this = this;
            if ($.inArray(id, this.selectPackages) == -1) {
                $this.selectPackages.push(id);
            }

            return $this.selectPackages;
        },

        removePackage: function (id) {
            var $this = this;
            $.each($this.selectPackages, function( index, packageId ) {
                if (id == packageId) {
                    $this.selectPackages.splice(index, 1);
                }
            })

            return $this.selectPackages;
        },

        togglePackageActive: function (package) {
            var $this = this;
            if ($this.isPackageActive(package)) {
                $this.removePackage(package);
            } else {
                $this.addPackage(package);
            }

            return $this.selectPackages;
        },

        isPackageActive: function (package) {
            var $this = this;
            if ($.inArray(package, $this.selectPackages) != -1) {
                return true;
            }

            return false;
        },

        selectAllPackages: function () {
            var $this = this;
            $.each($this.order.packageIds, function( index, packageId ) {
                if (!$this.isPackageActive(packageId.value)) {
                    $this.addPackage(packageId.value);
                }
            })

            return false;
        },

        showDetailPackage: function (package) {
            if(this.packageId != package.id) {
                this.packageId = package.id;
            } else {
                this.packageId = null;
            }

            return false;
        },

        isShowDetailPackage: function (package) {
            if (this.packageId == package.id) {
                return true;
            }

            return false;
        },

        toogleShowDetailPackage: function (package) {
            if(this.isNotEqualPackage(package)) {
                package.damaged = true;
                this.packageId = package.id;
            } else {
                this.packageId = null;
            }

            return false;
        },

        isNotEqualPackage: function (package) {
            if (package.damaged) {
                return true;
            }

            return false;
        },

        isDamaged: function (package) {
            if (this.order.shipping.type == 'pickup') {
                if (typeof(package.damaged) != 'undefined' && package.damaged) {
                    return true;
                }
            }

            if (this.order.shipping.type == 'deliver') {
                if (typeof(package.damaged) != 'undefined' && package.damaged) {
                    return true;
                }
            }

            return false;
        },

        existWeight: function (package) {
            if (this.order.shipping.type == 'pickup') {
                if (typeof(package.damaged) != 'undefined' && package.damaged) {
                    return package.damaged;
                }
            }

            if (this.order.shipping.type == 'deliver') {
                if (typeof(package.damaged) != 'undefined' && package.damaged) {
                    return package.damaged;
                }
            }

            return false;
        },

        loadOrder: function() {

            var $this = this;

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch/"+$this.order.externalId,
                dataType: "json",
                success: function($data){
                    $this.order = $data.data;

                    $.each($this.order.packages, function( index, package ) {
                        package.damaged = $this.isDamaged(package);
                        package.weight = $this.existWeight(package);
                    })

                    $.each($this.order.orderPackages, function( index, package ) {
                        package.damaged = $this.isDamaged(package);
                        package.weight = $this.existWeight(package);
                    })
                }
            });
        },

        filesByGroup: function (group) {
            var $this = this;

            var files = [];
            if ($this.order && $this.order.files) {
                $.each($this.order.files, function( index, file ) {
                    if (file.group.id == group.id) {
                        files.push(file);
                    }
                })
            }
            return files;
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();
    },
    computed: {
        totalSelectPackage: function () {
            return this.selectPackages.length;
        },
    },
    watch: {
        'filter': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
        'orderData': function(val) {
            var $this = this;
            this.loading = false;
            this.order = JSON.parse(val);


        },
    }
})

$(function(){

    /*$(".js-select-all").change(function () {
        $('.order-table thead').toggleClass('blured');
        $(".order-table td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        }
    });

    $(".js-undelete").on("click", function() {
        $('.order-number')
            .find(":checkbox:checked")
            .closest('tr')
            .show();
        $('.order-table thead').removeClass('blured');
        $('.order-number input').prop('checked', false);
        $(".js-select-all").prop('checked', false);
    });

    $('body').on('click', '.order-number', function(){
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        } else {
            $('.order-table thead').removeClass('blured');
        }
    })*/

})
