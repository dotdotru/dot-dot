import Vue from 'vue';
import Batches from './batches.vue';
import BatchLoad from './batch/batchLoad.vue';
import BatchArrived from './batch/batchArrived.vue';
import BatchShippingLoad from './batchShipping/batchShippingLoad.vue';
import BatchShippingArrived from './batchShipping/batchShippingArrived.vue';

$('[data-attach-vue="wms-batches"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<Batches/>',
        data: {

        },
        components: { Batches }
    });

});

$('[data-attach-vue="wms-batch-load"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<BatchLoad :batchId="batchId"/>',
        data: {
            'batchId': item.data('batch-id'),
        },
        components: { BatchLoad }
    });

});

$('[data-attach-vue="wms-batch-arrived"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<BatchArrived :batchId="batchId"/>',
        data: {
            'batchId': item.data('batch-id'),
        },
        components: { BatchArrived }
    });

});

$('[data-attach-vue="wms-batch-shipping-load"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<BatchShippingLoad :batchId="batchId"/>',
        data: {
            'batchId': item.data('batch-id'),
        },
        components: { BatchShippingLoad }
    });

});

$('[data-attach-vue="wms-batch-shipping-arrived"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<BatchShippingArrived :batchId="batchId"/>',
        data: {
            'batchId': item.data('batch-id'),
        },
        components: { BatchShippingArrived }
    });

});
