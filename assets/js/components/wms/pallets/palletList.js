import Vue from 'vue';
import PalletList from './palletList.vue';
import PalletManualCreate from './palletManualCreate.vue';

$('[data-attach-vue="pallet-list"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<PalletList :url="url" :title="title"/>',
        data: {
            'url': item.data('url'),
            'title': item.data('title'),
        },
        components: { PalletList }
    });

});

$('[data-attach-vue="pallet-manual-create"]').each(function( index ) {

    let item = $(this);

    new Vue({
        el: '#'+item.attr('id'),
        template: '<PalletManualCreate />',
        data: {
        },
        components: { PalletManualCreate }
    });

});