var wmsbatch = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatch',
    data: {
        timeout: null,
        loading: true,
        orderId: null,
        orders: [],
        order: null,
        filterFields: [
            {text: '№ партии', value: 'externalId'},
            {text: 'Дата выдачи', value: 'issueAt'},
            {text: 'Дата приемки', value: 'receptionAt'},
            {text: 'Вес', value: 'wight'},
            {text: 'Стоимость', value: 'price'},
            {text: 'Адрес отправителя', value: 'fromAddress'},
            {text: 'Адрес получателя', value: 'toAddress'},
            {text: 'Направление', value: 'direction'},
            {text: 'Перевозчик', value: 'carrier'},
            {text: 'Контакты перевозчика', value: 'carrierContact'},
            {text: 'Водитель', value: 'driver'},
            {text: 'Контакты водителя', value: 'driverContact'},
        ],
        filter: {
            search: '',
            status: '',
            field: 'externalId'
        },
        fileGroups: [],
        nextTime: 20,
        maxTime: 300,
        time: '',
        selectDrivers: null,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 1,
        },
        loadOrdersEmpty: true,
        mask: mask(),
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false
    },
    methods: {
        showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.loadOrders();
            }
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        orderRejected: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', '', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/wms/wmsbatch/batch-rejected/"+$this.orderId,
                    cache: false,
                    async: false,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    $($event.target).addClass('disabled');

                    $this.loadOrders();
                    closePopup();
                }
            });

            return false;
        },
        
        formatPhone: function (phone) {
			var phone = phone.replace(/[A-z\(\)-+ ]/g, '')
			
			if (phone[0] == '7') {
				phone = phone.substr(1, phone.length)
			}

			phone = phone.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "($1) $2-$3-$4");

			if (phone[0] == '7') {
				phone = phone.substr(1, phone.length)
			}

			return '+7 '+phone;
		},

        toggleOrder: function(orderId){

            var $this = this;

            if ($this.orderId == orderId) {
                $this.orderId = null;
                $this.selectDrivers = null;
            } else {
                var order = $this.loadOrder(orderId);
            }

            if (!order) {
                $this.orderId = null;
            }

            $this.edit = false;
            $this.htmlUp();
        },

        loadOrder: function(orderId){
            var $this = this;

            $this.orderId = orderId;

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    $this.order = item;

                    if ($this.order.drivers) {
                        $.each($this.order.drivers, function( index, driver ) {
                            $this.selectDrivers = driver.id
                        });
                    }

                    $this.htmlUp();
                }
            })

            $this.htmlUp();
            return $this.order;
        },

        loadOrders: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter[search]', $this.filter.search);
            formData.append('filter[status]', $this.filter.status);
            formData.append('filter[field]', $this.filter.search ? $this.filter.field : '');
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/orders",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result && result.data) {
                $this.orders = result.data.orders;
                $this.pagination = result.data.pagination;

                if ($this.orders.length && $this.loadOrdersEmpty) {
                    $this.loadOrdersEmpty = false;
                }

                $this.loading = false;

                $this.htmlUp();

                if ($this.orders) {
                    $($this.orders).each(function( index, order ) {
                        if (order.overload == false && order.chooseDrivers.length == 0 && order.realStatus == 'reserved') {
                            $(function(){
                                openPopupUrl('/static/popup/no_drivers');
                                $this.removeOrder(order.id);
                            })
                        }
                    });
                }
            }
        },

        filesByGroup: function (group) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (file.group.id == group.id || file.group.slug == group) {
                        files.push(file);
                    }
                })
            }
            return files;
        },

        filesWithoutGroups: function (groups) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var groupIds = [];
            $.each(groups, function( index, group ) {
                groupIds.push(group.id);
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    var group = file.group;
                    if (!group || group && $.inArray(group.id, groupIds) == -1 && !group.hidden) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        firstPathFileByGroup: function (group) {
            var files = this.filesByGroup(group);
            if (files.length > 0) {
                return files[0].file;
            }

            return null;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        loadConfig: function(orderId){
            var $this = this;

            $.ajax({url: "/api/wms/wmsbatch/config", success: function(result){
                $this.fileGroups = result.data.params.fileGroups;

                $this.errors.clear();
                $this.htmlUp();

            }});

        },
    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();

        $this.loadOrdersEmpty = true;

        $this.loadConfig();
        $this.loadOrders();
    },
    computed: {

    },
    watch: {
        'filter.search': function(val) {
            var self = this;
            self.pagination.page = 1;
            clearTimeout(self.timeout);
            self.timeout = setTimeout(function () {
                self.loadOrders();
            }, 600);
        },
        'filter.status': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
    }
})

$(function(){

    $(".js-select-all").change(function () {
        $('.order-table thead').toggleClass('blured');
        $(".order-table td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        }
    });

    $(".js-undelete").on("click", function() {
        $('.order-number')
            .find(":checkbox:checked")
            .closest('tr')
            .show();
        $('.order-table thead').removeClass('blured');
        $('.order-number input').prop('checked', false);
        $(".js-select-all").prop('checked', false);
    });

    $('body').on('click', '.order-number', function(){
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        } else {
            $('.order-table thead').removeClass('blured');
        }
    })

})
