var wmsOrderDelivered = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsorderdelivered',
    data: {
		loading: true,
        orderData: null,
        order: null,
        packageId: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPallets: []
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('#wmsbatcharrived').css('visibility', 'visible');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        orderDelivered: function($event) {
            var $this = this;

            var formData = new FormData();

            $.each($this.order.packages, function( index, package ) {
                formData.append('order[packages]['+index+'][id]', package.id);
                formData.append('order[packages]['+index+'][damaged]', package.damaged ? 1 : 0);
            })

            openPopupButton('Вы уверены?', '', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/wms/wmsorder/order-delivered/"+$this.order.id,
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    location.href = '/wms/';
                }
            });

            return false;
        },
        
        orderSaveDelivered: function($event) {
            var $this = this;

            var formData = new FormData();

            $.each($this.order.packages, function( index, package ) {
                formData.append('order[packages]['+index+'][id]', package.id);
                formData.append('order[packages]['+index+'][damaged]', package.damaged ? 1 : 0);
            })

            var jqxhr = $.ajax({
                method: 'POST',
                url: " /api/wms/wmsorder/order-save-delivered/"+$this.order.id,
                cache: false,
                async: true,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });
            
            return false;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        showDetailPackage: function (package) {
            if (this.packageId == package.id) {
                return true;
            }

            return false;
        },

        toogleShowDetailPackage: function (package) {
            if (this.packageId == package.id) {
                this.packageId = null;
            } else if(package.damaged == true) {
                this.packageId = package.id;
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('order[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsorder/order-upload-file/"+$this.order.externalId,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.loadOrder();
            }

            return false;
        },

        loadOrder: function () {
            var $this = this;
            var jqxhr = $.ajax({
                url: "/api/wms/wmsorder/order/"+$this.order.id,
                cache: false,
                async: false
            });

            var data = jqxhr.responseJSON.data;

            this.orderData = JSON.stringify(data.order);

            this.htmlUp();
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();


    },
    computed: {
        totalSelectPallet: function () {
            return this.selectPallets.length;
        },
    },
    watch: {
        'orderData': function(val) {
            this.order = JSON.parse(val);
            this.loading = false;
            $('#wmsbatcharrived').css('visibility', 'visible');
        },
    }
})
