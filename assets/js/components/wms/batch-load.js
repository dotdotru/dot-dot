var wmsBatchLoad = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatchload',
    data: {
        loading: true,
        orderData: null,
        order: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPallets: [],
        batchSaved: false,
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        batchSave: function($event) {
            var $this = this;

            var formData = new FormData();
            $.each($this.selectPallets, function( index, pallet ) {
                formData.append('wmsBatch[palletIds]['+index+']', pallet.id);
            })

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-save/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                $this.batchSaved = true;
            }

            return false;
        },

        batchLoad: function($event) {
            var $this = this;

            var formData = new FormData();
            $.each($this.selectPallets, function( index, pallet ) {
                formData.append('wmsBatch[palletIds]['+index+']', pallet.id);
            })

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-load/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                location.href = '/wms/batch';
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('batch[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-upload-file/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.loadOrder();
            }

            return false;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        addPallet: function (pallet) {
            var $this = this;
            if ($.inArray(pallet, this.selectPallets) == -1) {
                $this.selectPallets.push(pallet);
            }

            return $this.selectPallets;
        },

        removePallet: function (pallet) {
            var $this = this;
            $.each($this.selectPallets, function( index, selectPallet ) {
                if (pallet.id == selectPallet.id) {
                    $this.selectPallets.splice(index, 1);
                }
            })

            return $this.selectPallets;
        },

        togglePalletActive: function (pallet) {
            var $this = this;
            if ($this.isPalletActive(pallet)) {
                $this.removePallet(pallet);
            } else {
                $this.addPallet(pallet);
            }

            return $this.selectPallets;
        },

        isPalletActive: function (pallet) {
            var $this = this;
            var result = false;
            $.each($this.selectPallets, function( index, selectPallet ) {
                if (pallet.id == selectPallet.id) {
                    result = true;
                }
            })

            return result;
        },

        selectAllPallets: function () {
            var $this = this;
            if ($this.isTotalSelected()) {
                $this.removeAllPallets();
            } else {
                $this.addAllPallets();
            }

            return false;
        },

        addAllPallets: function () {
            var $this = this;
            $.each($this.order.pallets, function( index, pallet ) {
                if (!$this.isPalletActive(pallet)) {
                    $this.addPallet(pallet);
                }
            })

            return false;
        },

        removeAllPallets: function () {
            this.selectPallets = [];

            return false;
        },


        isTotalSelected: function () {
            var $this = this;
            var result = true;

            $.each($this.order.pallets, function( index, pallet ) {
                if (!$this.isPalletActive(pallet)) {
                    result = false;
                }
            })

            return result;
        },

        loadOrder: function() {

            var $this = this;

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch/"+$this.order.externalId,
                dataType: "json",
                success: function($data){
                    $this.order = $data.data;
                }
            });
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();
    },
    computed: {
        totalSelectPallet: function () {
            return this.selectPallets.length;
        },
        totalSelectWeightPallet: function () {
            var totalWeight = 0;
            $.each(this.selectPallets, function( index, pallet ) {
                totalWeight = totalWeight + pallet.weight;
            })
            return totalWeight;
        },
        totalSelectTimePallet: function () {
            var deliverToAt = null;
            $.each(this.selectPallets, function( index, pallet ) {
                if (!deliverToAt) {
                    deliverToAt = pallet.deliverToAt;
                }
                if (deliverToAt > convertDate(pallet.deliverToAt)) {
                    deliverToAt = pallet.deliverToAt;
                }
            })
            return deliverToAt;
        },
    },
    watch: {
        'orderData': function(val) {
            this.order = JSON.parse(val);
            this.loading = false;
            this.addAllPallets();
        },
    }
})
