Dot-Dot.ru Stack List
========================

# Используемые технологии
## База данных
- PostgreSQL 11
- MariaDB 10.3.8

## Бек и фронт
- Symphony 4.3.4
- PHP 7.2.19
- Vue.JS

## Бек и фронт
- Symphony 4.3.4
   * "beberlei/doctrineextensions": "^1.2",
   * "friendsofsymfony/ckeditor-bundle": "^2.1",
   * "friendsofsymfony/oauth-server-bundle": "^1.6",
   * "friendsofsymfony/user-bundle": "^2.1",
   * "gietos/dadata": "^1.1",
   * "guzzlehttp/guzzle": "^6.3",
   * "jhaoda/smscenter": "^2.0",
   * "knplabs/knp-snappy-bundle": "^1.5",
   * "nelmio/api-doc-bundle": "^3.4",
   * "phpoffice/phpspreadsheet": "^1.8",
   * "presta/sitemap-bundle": "^1.7",
   * "ralouphie/mimey": "^2.1",
   * "ratchet/pawl": "^0.3.4",
   * "sensio/framework-extra-bundle": "^5.4",
   * "sonata-project/admin-bundle": "^3.37",
   * "sonata-project/block-bundle": "^3.17",
   * "sonata-project/core-bundle": "^3.0",
   * "sonata-project/datagrid-bundle": "^2.3",
   * "sonata-project/doctrine-orm-admin-bundle": "^3.6",
v"sonata-project/media-bundle": "^3.15",
   * "sonata-project/seo-bundle": "^2.7",
   * "sonata-project/translation-bundle": "^2.3",
   * "sonata-project/user-bundle": "^4.2",
   * "stof/doctrine-extensions-bundle": "^1.3",
   * "symfony/asset": "^4.1",
   * "symfony/cache": "^4.1",
   * "symfony/css-selector": "^4.3",
   * "symfony/dom-crawler": "^4.3",
   * "symfony/finder": "^4.3",
   * "symfony/flex": "^1.0",
   * "symfony/form": "^4.1",
   * "symfony/lock": "^4.3",
   * "symfony/monolog-bundle": "^3.3",
   * "symfony/orm-pack": "^1.0",
   * "symfony/property-access": "^4.3",
   * "symfony/security-acl": "^3.0",
   * "symfony/security-bundle": "^4.1",
   * "symfony/swiftmailer-bundle": "^3.2",
   * "symfony/templating": "^4.1",
   * "symfony/translation": "^4.1",
   * "symfony/twig-bundle": "^4.1",
   * "symfony/validator": "^4.1",
   * "symfony/webpack-encore-bundle": "^1.0",
   * "symfony/yaml": "^4.1",
   * "twig/extensions": "^1.5",
   * "twig/twig": "^2.11",
   * "voronkovich/sberbank-acquiring-client": "^2.4",
   * "vsavritsky/settingsbundle": "1.1",
   * "wapmorgan/morphos": "^3.2"
- Vue.JS
   * "@fancyapps/fancybox": "^3.5.7",
   * "@symfony/webpack-encore": "^0.30.2",
   * "autoprefixer": "^9.5.1",
   * "axios": "^0.18.0",
   * "babel-polyfill": "^6.26.0",
   * "bootstrap": "^4.3.1",
   * "bootstrap-4-grid": "^3.1.0",
   * "bootstrap-grid": "^3.0.1",
   * "bootstrap-vue": "^2.0.1",
   * "chart.js": "^2.8.0",
   * "core-js": "2",
   * "css-loader": "^2.1.1",
   * "d3": "^5.12.0",
   * "file-loader": "^3.0.1",
   * "font-awesome": "^4.7.0",
   * "gsap": "^2.1.2",
   * "gulp-autoprefixer": "^6.0.0",
   * "gulp-concat": "^2.6.0",
   * "gulp-less": "^4.0.1",
   * "gulp-minify-css": "^1.2.1",
   * "gulp-plumber": "^1.0.1",
   * "gulp-sourcemaps": "^1.5.2",
   * "gulp-svg-sprite": "^1.2.19",
   * "gulp-uglify": "^1.5.3",
   * "jquery": "^3.4.1",
   * "jquery.tooltips": "^1.0.0",
   * "js-cookie": "^2.2.1",
   * "moment": "^2.24.0",
   * "node-sass": "^4.11.0",
   * "normalize.css": "^8.0.1",
   * "npm": "^6.9.0",
   * "npm-update-all": "^1.0.1",
   * "postcss-loader": "^3.0.0",
   * "requirejs": "^2.3.6",
   * "sass": "^1.18.0",
   * "sass-loader": "^7.1.0",
   * "stylus": "^0.54.5",
   * "stylus-loader": "^3.0.2",
   * "swiper": "^4.5.0",
   * "urijs": "^1.19.1",
   * "v-mask": "^1.3.3",
   * "v-money": "^0.8.1",
   * "vee-validate": "^2.2.14",
   * "velocity-animate": "^1.5.2",
   * "vinyl": "^2.2.0",
   * "vue": "^2.6.10",
   * "vue-analytics": "^5.16.4",
   * "vue-cookies": "^1.5.13",
   * "vue-js-modal": "^1.3.31",
   * "vue-loader": "^15.7.0",
   * "vue-password": "^2.0.3",
   * "vue-router": "^3.0.5",
   * "vue-scrollto": "^2.17.1",
   * "vue-select": "^3.0.2",
   * "vue-select2": "^0.2.6",
   * "vue-slider-component": "^3.0.30",
   * "vue-social-sharing": "^2.4.5",
   * "vue-suggestions": "^1.4.0",
   * "vue-symfony-form": "^1.2.2",
   * "vue-template-compiler": "^2.6.10",
   * "vue-the-mask": "^0.11.1",
   * "vue2-datepicker": "^2.12.0",
   * "vuebar": "^0.0.20",
   * "vuejs-paginate": "^2.1.0",
   * "vuex": "^3.1.1",
   * "webpack-notifier": "^1.7.0"


## Чатбот
 - Telegram Sherlock.im 
 - Viber Sherlock.im 
 - WhatsAppAPI chat-api.com
 
## Интеграции
- Dadata - нормализация адресов, получение данных компании по ИНН Google - получение расстояния между точками
- СК Спасские ворота - верификация водителя и генерация страхового полиса
- API различных ТК - расчет цен конкурентов для сравнения стоимости заказа
- Сбербанк онлайн эквайринг - оплата онлайн
- Google Cloud Platform - Расчет расстояния между точками отправления и доставки
- СМС-Центр - ООО "СМС-Центр"
- Sherlock - Платформа для создания чатбота
- CoMagic - Сервис аналитики
