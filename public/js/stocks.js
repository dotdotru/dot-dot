function initMap() {
    var map_element = document.getElementById('map');
    var bounds = new google.maps.LatLngBounds();

    var markers = [];
    var activeIcon = {
        url: 'img/marker-big.png'
    };
    var normalIcon = {
        url: "img/marker.png"
    };

    if (map_element) {
        var map = new google.maps.Map(map_element, {
            zoom: document.zoom ? document.zoom : 11,
            center: {
                lat: 56.0097823,
                lng: 37.4366505
            },
            disableDefaultUI: false,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "saturation": "-65"
                        },
                        {
                            "lightness": "45"
                        },
                        {
                            "gamma": "1.78"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "saturation": "-33"
                        },
                        {
                            "lightness": "22"
                        },
                        {
                            "gamma": "2.08"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "gamma": "2.08"
                        },
                        {
                            "hue": "#ffa200"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "saturation": "-55"
                        },
                        {
                            "lightness": "-2"
                        },
                        {
                            "gamma": "1.88"
                        },
                        {
                            "hue": "#ffab00"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#bbd9e5"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                }
            ]

        });

        $.each(window.stocks, function( i, item ) {

            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(item.lat),
                    lng: parseFloat(item.lng)
                },
                map: map,
                icon: '/img/marker.png'
            });

            bounds.extend(marker.position);

            //marker.addListener('click', function () {
            //    $('.marker-popup').removeClass('active')
            //    $('#markerPopup'+item.id).addClass('active');
            //    if( $('#delivery-store').length>=1 ){
            //        $('#delivery-store').show();
            //        $('#delivery-store span').text($('#markerPopup1 h5').text());
            //    }
            //});

            markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    var $this = this;
                    for (var j = 0; j < markers.length; j++) {
                        markers[j].setIcon(normalIcon);
                        $('.marker-popup').removeClass('active')
                    }
                    $this.setIcon(activeIcon)
                    $('#markerPopup' + i ).addClass('active');

                    $('#markerPopup' + i + ' .close').on('click', function () {
                        $(this).parent().removeClass('active');
                        $this.setIcon(normalIcon)
                    });
                }
            })(marker, i));
        });

        map.fitBounds(bounds);
        google.maps.event.addListenerOnce(map, 'idle', function() {
            if (window.stocks.length == 1) {
                map.setZoom(13);
            }
        });

        /*$('.marker-popup .close').on('click', function () {
            $(this).parent().removeClass('active');
        });*/
    }
}