$(function(){

    var app = new Vue({
        el: '#track',
        data: {
            url: '',
            number: '',
            numberCheck: false,
            code: '',
            checkCode: null,
            smsSend: false,
            mask: mask(),
        },
        methods: {
            checkNumber: function () {
                var number = this.number;

                this.smsSend = false;
                this.checkCode = null;

                var jqxhr = $.ajax({
                    url: "/api/order/customer/check_number",
                    cache: false,
                    async: false,
                    data: {number: number}
                });

                var data = jqxhr.responseJSON.data;

                this.numberCheck = data.result;

                if (data.auth) {
                    location.href = data.url;
                }

                return data;
            },

            sendSms: function () {
                var number = this.number;

                if (this.numberCheck == false) {
                    return false;
                }

                var jqxhr = $.ajax({
                    url: "/api/order/customer/sendsms",
                    cache: false,
                    async: false,
                    data: {number: number}
                });

                var data = jqxhr.responseJSON;

                this.smsSend = data.status;

                return data.status;
            },

            checkSmsCode: function () {
                var number = this.number;
                var code = this.code;

                var jqxhr = $.ajax({
                    url: "/api/order/customer/checksms",
                    cache: true,
                    async: false,
                    data: {code: code, number: number}
                });

                var data = jqxhr.responseJSON;

                this.checkCode = data.status;

                if (this.checkCode) {
                    location.href = data.data.url;
                }

                return data.status;
            },
        },
        watch: {
            number: function (val) {
                this.checkNumber();
            },
            code: function (val) {
                this.checkSmsCode();
            }
        },
        mounted : function () {
            $('#track').css('visibility', 'visible');
        },

    })
})
