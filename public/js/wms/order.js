var wmsOrder = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsorder',
    data: {
        loading: true,
        orderData: null,
        order: null,
        directions: null,
        packageTypes: null,
        packings: null,
        stocks: null,
        mask: mask(),
        maskfilter: filter(),
        filter: filter(),

        maxWeight: 1450,
        totalPriceMin: 0,
        totalPriceMax: 0,
        maxWidth: 80,
        maxHeight: 180,
        maxDepth: 120,
        edit: false,
        hiddenIndex: 1,
        orderCreate: false,
        freePallets: [],

        cityFrom: null,

        organizationTypes: [],

        print: {},
    },
    methods: {
        getStocksFrom: function () {
            var $this = this;
            var result = [];

            if (!this.order) {
                return result;
            }

            if ($this.cityFrom) {
                $.each(this.stocks, function( index, item ) {
                    if ($this.cityFrom == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
        getStocksTo: function () {
            var $this = this;
            var result = [];

            if (!this.order) {
                return result;
            }

            var cityList = [];
            $.each(this.directions, function( index, item ) {
                if ($this.cityFrom == item.cityFrom.id) {
                    cityList.push(item.cityTo.id);
                }
            })

            if (cityList.length > 0) {
                $.each(this.stocks, function( index, item ) {
                    if (cityList.indexOf(item.city.id) >= 0) {
                        result.push(item);
                    }
                });
            }

            return result;
        },

        getData: function () {
            var jqxhr = $.ajax({
                url: "/api/wms/wmsorder/config",
                cache: true,
                async: false
            });

            var data = jqxhr.responseJSON.data;

            this.directions = data.params.directions;
            this.packageTypes = data.params.packageTypes;
            this.packings = data.params.packings;
            this.stocks = data.params.stocks;
            this.organizationTypes = data.params.organizationTypes;
        },


        createPallet: function () {
            var $this = this;
            var formData = new FormData();

            var stockFrom = $this.order.stockFrom.id;
            var stockTo = $this.order.stockTo.id;

            formData.append('data[stockFrom]', stockFrom);
            formData.append('data[stockTo]', stockTo);
            formData.append('data[direction]', $this.order.direction.id);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/create",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON.data;

            return data.palletId;
        },

        loadPallets: function () {
            var $this = this;
            var formData = new FormData();

            var stockFrom = $this.order.stockFrom.id;
            var stockTo = $this.order.stockTo.id;

            formData.append('data[stockFrom]', stockFrom);
            formData.append('data[stockTo]', stockTo);

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/free_pallets",
                cache: false,
                async: true,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(data) {
                if (data.status) {
                    $this.freePallets = data.data.pallets;
                    $this.checkPalletId();
                }
            });
        },

        checkPalletId: function () {
            var $this = this;
            if ($this.freePallets.length) {
                var firstPallet = $this.freePallets[0];
            } else {
                $this.createPallet();
                $this.loadPallets();
                return;
            }

            $.each($this.order.packages, function( index, realPackage ) {
                if (!realPackage.palletId) {
                    realPackage.palletId = firstPallet.externalId;
                }
            })
        },

        loadOrder: function () {
            var $this = this;

            var jqxhr = $.ajax({
                url: "/api/wms/wmsorder/order/"+$this.order.id,
                cache: false,
                async: false
            });

            var data = jqxhr.responseJSON.data;

            this.orderData = JSON.stringify(data.order);

            this.htmlUp();
        },

        upPalletId: function (package) {
            var $this = this;
            var nextPalletId = null;

            $.each($this.freePallets, function( index, pallet ) {
                if (package.palletId == pallet.externalId) {
                    if ($this.freePallets[index + 1] != undefined) {
                        nextPalletId = $this.freePallets[index + 1].externalId;
                    }
                }
            })

            if (!nextPalletId) {
                nextPalletId = $this.createPallet();
            }

            if (nextPalletId) {
                package.palletId = nextPalletId;
            }

            $this.clearError();
            $this.loadPallets();
        },

        downPalletId: function (package) {
            var $this = this;
            var prevPalletId = null;

            $.each($this.freePallets, function( index, pallet ) {
                if (package.palletId == pallet.externalId) {
                    if ($this.freePallets[index - 1] != undefined) {
                        prevPalletId = $this.freePallets[index - 1].externalId;
                    }
                }
            })

            if (prevPalletId) {
                package.palletId = prevPalletId;
            }

            $this.clearError();
        },

        getPackings: function (package) {
            var width = this.width;
            var height = this.height;
            var depth = this.depth;
            var result = [];

            return this.packings;
        },
        createPackage: function (index) {
            var $this = this;

            var package = Object.assign({}, WmsPackage);

            $this.order.packages.splice(this.order.packages.length + 1, 0, package);

            $.each($this.order.packages, function( index, package ) {
                if (!package.hidden) {
                    package.hidden = $this.hiddenIndex + 1;
                    $this.hiddenIndex = $this.hiddenIndex + 1;
                }
            })

            if ($('.form-row-wrap').length >= 2) {
                $('.form-wrapper').scrollbar();
                setTimeout(function(){
					$('.form-wrapper').scrollTop($('.form-row-inner')[0].scrollHeight);
				}, 200);
            }

            this.htmlUp();

            $this.checkPalletId();
            
            this.orderCreate = false;

            return false;
        },
        removePackage: function (index) {
            if (this.order.packages.length <= 1) {
                this.order.packages.splice(index, 1);
                this.createPackage(index);
                return;
            }

            this.order.packages.splice(index, 1);

			this.orderCreate = false;

            return false;
        },
        htmlUp: function () {
            setTimeout(function () {
                $('.selectpicker').selectpicker();
                $('[data-toggle="tooltip"]').tooltip();
            }, 100);
        },

        saveOrderDetail: function ($event) {
            var $this = this;

            $this.$validator.validateAll('form-2').then(function(result) {
                if (!result) {
                    return;
                }

                if ($event) {
                    var button = $($event.target);
                    if (button.hasClass('disabled')) {
                        return false;
                    }

                    button.addClass('disabled');
                }

                var packages = $this.order.packages;

                var formData = new FormData();

                formData.append('wmsOrder[receiver][id]', $this.order.receiver.id);
                formData.append('wmsOrder[receiver][uridical]', $this.order.receiver.uridical);
                formData.append('wmsOrder[receiver][contactName]', $this.order.receiver.contactName);
                formData.append('wmsOrder[receiver][series]', $this.order.receiver.series);
                formData.append('wmsOrder[receiver][number]', $this.order.receiver.number);
                formData.append('wmsOrder[receiver][contactPhone]', $this.order.receiver.contactPhone);
                formData.append('wmsOrder[receiver][contactEmail]', $this.order.receiver.contactEmail);
                formData.append('wmsOrder[receiver][type]', $this.order.receiver.type);
                formData.append('wmsOrder[receiver][companyName]', $this.order.receiver.companyName);
                formData.append('wmsOrder[receiver][inn]', $this.order.receiver.inn);

                formData.append('wmsOrder[declaredPrice]', $this.order.declaredPrice);
                formData.append('wmsOrder[stockTo][id]', $this.order.stockTo.id);

                $.each(packages, function( index, package ) {
                    formData.append('wmsOrder[packages]['+index+'][id]', package.id);
                    formData.append('wmsOrder[packages]['+index+'][packageType]', package.packageType);
                    formData.append('wmsOrder[packages]['+index+'][packageTypeAnother]', package.anotherType);
                    formData.append('wmsOrder[packages]['+index+'][count]', package.count);
                    formData.append('wmsOrder[packages]['+index+'][weight]', package.weight);
                    formData.append('wmsOrder[packages]['+index+'][damaged]', package.damaged);
                    formData.append('wmsOrder[packages]['+index+'][generateId]', $this.getGenerateIdPackage(index));
                    if (package.packing > 0) {
                        formData.append('wmsOrder[packages]['+index+'][packing]', package.packing);
                        formData.append('wmsOrder[packages]['+index+'][packingPrice]', package.packingPrice);
                    }
                    formData.append('wmsOrder[packages]['+index+'][depth]', package.depth);
                    formData.append('wmsOrder[packages]['+index+'][width]', package.width);
                    formData.append('wmsOrder[packages]['+index+'][height]', package.height);
                    formData.append('wmsOrder[packages]['+index+'][calculateWeight]', package.calculateWeight);
                    formData.append('wmsOrder[packages]['+index+'][palletId]', package.palletId);
                });

                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmsorder/order-save/"+$this.order.id,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        if ($event) {
                            button.removeClass('disabled');
                        }
                        $this.edit = false;
                        $this.loadOrder();
                    }
                });
            })
        },

        createOrder: function ($event) {
            var $this = this;

            $this.$validator.validateAll('form-1').then(function(result) {
                if(!result) {
                    return;
                }

                if ($event) {
                    var button = $($event.target);
                    if (button.hasClass('disabled')) {
                        return false;
                    }

                    button.addClass('disabled');
                }


                var packages = $this.order.packages;

                var formData = new FormData();

                $.each(packages, function( index, package ) {
                    formData.append('wmsOrder[packages]['+index+'][id]', package.id);
                    formData.append('wmsOrder[packages]['+index+'][packageType]', package.packageType);
                    formData.append('wmsOrder[packages]['+index+'][packageTypeAnother]', package.anotherType);
                    formData.append('wmsOrder[packages]['+index+'][count]', package.count);
                    formData.append('wmsOrder[packages]['+index+'][weight]', package.weight);
                    formData.append('wmsOrder[packages]['+index+'][damaged]', package.damaged ? 1 : 0);
                    formData.append('wmsOrder[packages]['+index+'][generateId]', $this.getGenerateIdPackage(index));
                    if (package.packing > 0) {
                        formData.append('wmsOrder[packages]['+index+'][packing]', package.packing);
                        formData.append('wmsOrder[packages]['+index+'][packingPrice]', package.packingPrice);
                    }
                    formData.append('wmsOrder[packages]['+index+'][depth]', package.depth);
                    formData.append('wmsOrder[packages]['+index+'][width]', package.width);
                    formData.append('wmsOrder[packages]['+index+'][height]', package.height);
                    formData.append('wmsOrder[packages]['+index+'][calculateWeight]', package.calculateWeight);
                    formData.append('wmsOrder[packages]['+index+'][palletId]', package.palletId);
                });

                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmsorder/order-save/"+$this.order.id,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        if ($event) {
                            $this.orderCreate = true;
                            button.removeClass('disabled');
                        }
                        $this.loadOrder();
                    } else if (data.data) {
                        if ($event) {
                            button.removeClass('disabled');
                        }

                        var errorPallettedIds = [];

                        if (data.data.errorPallettedIds) {
                            errorPallettedIds = data.data.errorPallettedIds;
                        }

                        if (errorPallettedIds) {
                            $.each(errorPallettedIds, function( index, errorPallettedId ) {
                                $.each($this.order.packages, function( indexPackage, package ) {
                                    $('[name="package.palletId'+indexPackage+'"]').removeClass('error');
                                    if (package.palletId == errorPallettedId) {
                                        $('[name="package.palletId'+indexPackage+'"]').addClass('error');
                                    }
                                });
                            })
                            openPopup('Ошибка паллетирования', 'Превышен общий вес в паллетах: '+errorPallettedIds.join(', '));
                        }
                    }
                });

            });

            return false; 
        },

        acceptedOrder: function ($event) {
            var $this = this;

            $this.$validator.validateAll('form-1').then(function(result) {
                if(!result) {
                    return;
                }

                var button = $($event.target);
                if (button.hasClass('disabled')) {
                    return false;
                }

                button.addClass('disabled');

                var formData = new FormData();

                $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmsorder/order-accepted/"+$this.order.id,
                    cache: false,
                    async: true,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status) {
                        location.href = '/wms';
                        //button.removeClass('disabled');
                        $this.loadOrder();
                    }
                });

            });

            return false;
        },


        getGenerateIdPackage: function (indexPackage) {
            var $this = this;
            var generateIdPackage = 0;

            $.each($this.order.packages, function( index, package ) {
                if (indexPackage > index) {
                    if (package.count > 0) {
                        generateIdPackage = generateIdPackage + parseInt(package.count);
                    } else {
                        generateIdPackage = generateIdPackage + 1;
                    }
                } else if (indexPackage == index) {
                    if (package.count > 1) {
                        generateIdPackage = parseInt(generateIdPackage + 1) + '-' + (generateIdPackage + parseInt(package.count));
                    } else {
                        generateIdPackage = generateIdPackage + 1;
                    }
                }
            })

            return $this.order.externalId + '/' + generateIdPackage;
        },

        fixSize: function (package) {
            if (package) {
                width = parseInt(package.width);
                height = parseInt(package.height);

                if (isNaN(width)) {
                    width = 1;
                }
                if (isNaN(height)) {
                    height = 1;
                }

                if (parseInt(package.height) > 0) {
                    package.height = parseInt(package.height);

                    if (package.height > this.maxHeight) {
                        package.height = this.maxHeight;
                    }
                }

                if (parseInt(package.width) > 0) {
                    package.width = parseInt(package.width);

                    if (package.width > this.maxWidth) {
                        package.width = this.maxWidth;
                    }
                }

                if (parseInt(package.depth) > 0) {
                    package.depth = parseInt(package.depth);

                    if (package.depth > this.maxDepth) {
                        package.depth = this.maxDepth;
                    }
                }

                if (package.weight > this.maxWeight) {
                    package.weight = this.maxWeight;
                }

                if (package.weight < 1 && package.weight != null && package.weight != '') {
                    package.weight = 1;
                }

                if (package.count < 1 && package.count != null && package.count != '') {
                    package.count = 1;
                }

                if (package.height < 1 && package.height != null && package.height != '') {
                    package.height = 1;
                }

                if (package.width < 1 && package.width != null && package.width != '') {
                    package.width = 1;
                }

                if (package.depth < 1 && package.depth != null && package.depth != '') {
                    package.depth = 1;
                }

                package.packingPrice = this.getPackingPrice(package);
            }

            if (this.price < 1 && this.price != null && this.price != '') {
                this.price = 1;
            }

            return package;
        },

        getPacking: function (packingId) {
            var result = null;
            $.each(this.packings, function( index, item ) {
                if (item.id == packingId) {
                    result = item;
                }
            });

            return result;
        },

        getVolume: function (width, height, depth) {
            return depth*height*width / 1000 / 1000;
        },

        getPackingPrice: function (package) {
            var $this = this;
            var packingPrice = 0;
            var packing = this.getPacking(package.packing);
            var exist = false;

            if (packing) {
                $.each(this.getPackings(package), function( index, item ) {
                    if (item.id == packing.id) {
                        exist = true;
                    }
                });

                if (exist) {
                    if (packing.type == 'for_one') {
                        packingPrice = packing.price;
                    } else if (packing.type == 'to_size') {
                        if (package.calculateWeight > 0) {
                            packingPrice = packing.price * package.calculateWeight;
                        } else {
                            packingPrice = packing.price * $this.getVolume(package.width, package.height, package.depth);
                        }
                    }
                } else {
                    $.each(this.packages, function( index, packageItem ) {
                        if (packageItem.id == package.id) {
                            $this.packages[index].packing = '';
                        }
                    })
                }
            }

            return Math.round(packingPrice);
        },

        clearSave: function($event, $group) {
            this.orderCreate = false;
        },

        clearError: function () {
            $.each(this.order.packages, function( indexPackage, package ) {
                $('[name="package.palletId'+indexPackage+'"]').removeClass('error');
            });
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('order[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsorder/order-upload-file/"+$this.order.externalId,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.createOrder();
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        cancelEdit: function() {
            var $this = this;
            var $result = null;

            $this.edit = false;
            $this.loadOrder();

            return $result;
        },
    },
    mounted: function() {
        this.getData();
    },
    computed: {
        stocksFrom: function () {
            return this.getStocksFrom();
        },
        stocksTo: function () {
            return this.getStocksTo();
        }
    },
    watch: {
        'edit': function(val) {
            this.clearSave();
            this.htmlUp();
        },
        'order.receiver.uridical': function(val) {
            this.htmlUp();
        },
        'print': function(val) {
            if (typeof val === 'string' || val instanceof String) {
                this.print = JSON.parse(val);
            }
        },
        'orderData': function(val) {
            var $this = this;
            $this.order = JSON.parse(val);

            $this.loading = false;

            var $wmsPackage = null;

            var packages = jQuery.extend(true, {}, $this.order.packages);
            $this.order.packages = [];

            if ($this.order.stockFrom) {
                $this.stockFrom = $this.order.stockFrom.id;
                $this.cityFrom = $this.order.stockFrom.city.id;
            }

            var found = false;
            $.each(packages, function( index, package ) {

                found = false;
                $.each($this.order.packages, function( index, realPackage ) {
                     if (package.packageType.id == realPackage.packageType
                         && package.depth == realPackage.depth
                         && package.damaged == realPackage.damaged
                         && package.width == realPackage.width
                         && package.height == realPackage.height
                         && package.weight == realPackage.weight
                         && ((!realPackage.packing && !package.packing) || (realPackage.packing && package.packing && package.packing.id == realPackage.packing))
                         && package.packingPrice == realPackage.packingPrice
                         && package.palletId == realPackage.palletId
                     ) {
                         found = true;
                         realPackage.count = realPackage.count + 1;
                     }
                })

                if (!found) {
                    $wmsPackage = Object.assign({}, WmsPackage);

                    $wmsPackage.id = package.id;

                    if (package.packageType) {
                        $wmsPackage.packageType = package.packageType.id;
                    }
                    
                    $wmsPackage.damaged = package.damaged;
                    $wmsPackage.count = package.count;
                    $wmsPackage.depth = package.depth;
                    $wmsPackage.width = package.width;
                    $wmsPackage.height = package.height;
                    $wmsPackage.weight = package.weight;
                    if (package.packing) {
                        $wmsPackage.packing = package.packing.id;
                    }

                    $wmsPackage.packingPrice = package.packingPrice;

                    $wmsPackage.palletId = package.palletId;

                    if (!package.hidden) {
                        package.hidden = $this.hiddenIndex + 1;
                        $this.hiddenIndex = $this.hiddenIndex + 1;
                    }

                    $this.order.packages.push($wmsPackage);
                }
            })

            if ($this.order.packages.length == 0) {
                var package = Object.assign({}, WmsPackage);
                $this.order.packages.push(package);
            }

            if ($this.order.packages.length > 2) {
                setTimeout(function(){
                    $('.form-wrapper').scrollbar();
                    $('.form-wrapper').scrollTop($('.form-row-inner')[0].scrollHeight);
                }, 1000);
            }

            setTimeout(function(){
                $this.htmlUp();
            }, 1000);

            this.loadPallets();
        },
    }
})

$(function(){
    $('body').on("input", '.js-cargo-quantity input', function() {
        var $self = $(this);
        $self.css('width', ($self.val().length + 1)*7 + 'px');
        $self.parent().css('width', $self.width() + 52 + 'px');
    });
})
