var wmsAuth = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsauth',
    data: {
        code: null,
        error: false
    },
    methods: {
        auth: function ($event) {
            $event.preventDefault();

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/security/auth",
                cache: false,
                async: false,
                data: {
                    code: this.code,
                }
            });

            json = jqxhr.responseJSON;

            if (json.status != undefined && json.status) {
				location.reload();
                
                this.error = false;
            } else {
                this.error = true;
            }

            return false;
        },
    },
    watch: {
        code: function(val) {
            this.error = false;
        }
    },
    mounted: function() {
        $('#wmsauth').css('visibility', 'visible');
    }
})
