var customerOrders = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsorders',
    data: {
        timeout: null,
        loading: true,
        edit: false,
        filterFields: [
            {text: '№ Заказа', value: 'externalId'},
            {text: 'Дата выдачи', value: 'issueAt'},
            {text: 'Дата приемки', value: 'receptionAt'},
            {text: 'Заявленная ценность', value: 'declaredPrice'},
            {text: 'Получатель', value: 'receiver'},
            {text: 'Контакты получателя', value: 'receiverContact'},
            {text: 'Адрес получателя', value: 'toAddress'},
            {text: 'Отправитель', value: 'sender'},
            {text: 'Контакты отправителя', value: 'senderContact'},
            {text: 'Адрес отправителя', value: 'fromAddress'},
            {text: 'Направление', value: 'direction'},
            {text: 'Перевозчик', value: 'carrier'},
            {text: 'Тип груза', value: 'packageType'},
        ],
        filter: {
            search: '',
            status: '',
            field: 'externalId'
        },
        order: null,
        stockFrom: {
            id: null
        },
        stockTo: {
            id: null
        },
        orderId: null,
        receiverId: null,
        uridical: 0,
        type: 'ООО',
        inn: '',
        contactName: '',
        contactPhone: '',
        contactEmail: '',
        series: '',
        number: '',
        companyName: '',
        stocks: [],
        fileGroups: [],
        direction: {
            from: null,
            to: null,
        },
        orders: [],
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        organizationTypes: [],
        loadOrdersEmpty: true,
        mask: mask(),
        maskfilter: filter(),
        sort: {
            by: 'externalId',
            direction: 'desc'
        },
    },
    methods: {
        showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.loadOrders();
            }
        },

        orderRejected: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', '', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/wms/wmsorder/order-rejected/"+$this.orderId,
                    cache: false,
                    async: false,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    $($event.target).addClass('disabled');

                    $this.loadOrders();
                    closePopup();
                }
            });

            return false;
        },

        pauseDelivery: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', 'Заказ будет остановлен, изменить его статус можно будет только при обращении к администрации сайта', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/order/customer/"+$this.orderId+"/pause_delivery",
                    cache: false,
                    async: false,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    $($event.target).addClass('disabled');

                    $this.loadOrders();
                    closePopup();
                }
            });

            return false;
        },

        removeOrders: function($event) {

            var $this = this;

            openPopupButton('Вы уверены?', 'Выбранные заказы будут удалены!', '', function () {

                var ids = [];
                $('.order-number').find(":checkbox:checked").each(function () {
                    ids.push($(this).val());
                })

                var formData = new FormData();
                $.each(ids, function (index, id) {
                    formData.append('ids[]', id);
                })

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/order/customer/remove",
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {

                    $('.order-number').find('input[type="checkbox"]:checked').prop('checked', false);
                    $('.order-table thead').removeClass('blured');
                    $(".js-select-all").prop('checked', false);

                    $this.loadOrders();
                }

            })

            return false;
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        toggleOrder: function(orderId) {
            var $this = this;

            if ($this.orderId == orderId) {
                $this.orderId = null;
            } else {
                $this.loadOrder(orderId)
            }
        },

        loadOrder: function(orderId){
            var $this = this;

            $this.orderId = orderId;

            $this.receiverId = null;
            $this.uridical = 0;
            $this.type = null;
            $this.inn = null;
            $this.contactName = null;
            $this.contactPhone = null;
            $this.contactEmail = null;
            $this.series = null;
            $this.number = null;
            $this.companyName = null;
            $this.stockFrom.id = null;
            $this.stockTo.id = null;

            $this.errors.clear();
            $this.htmlUp();

            $.ajax({url: "/api/wms/wmsorder/config", success: function(result){
                    $this.directions = result.data.params.directions;
                    $this.stocks = result.data.params.stocks;
                    $this.fileGroups = result.data.params.fileGroups;

                    $this.organizationTypes = result.data.params.organizationTypes;

                    $this.errors.clear();
                    $this.htmlUp();

                    $.each($this.orders, function( index, order ) {

                        if (order.id == $this.orderId) {

                            $this.order = Object.assign({}, order);

                            if ($this.order.receiver) {
                                var receiver = $this.order.receiver;
                                $this.receiverId = receiver.id;
                                $this.uridical = receiver.uridical ? 1 : 0;
                                $this.type = receiver.type != 'null' ? receiver.type : '';
                                $this.inn = receiver.inn != 'null' ? receiver.inn : '';
                                $this.contactName = receiver.contactName;
                                $this.contactPhone = receiver.contactPhone;
                                $this.contactEmail = receiver.contactEmail;
                                $this.series = receiver.series != 'null' ? receiver.series : '';
                                $this.number = receiver.number != 'null' ? receiver.number : '';
                                $this.companyName = receiver.companyName != 'null' ? receiver.companyName : '';
                            }

                            if ($this.order.direction) {
                                Vue.set($this, 'direction', $this.order.direction.id)
                            }

                            if ($this.order.stockFrom) {
                                Vue.set($this.stockFrom, 'id', parseInt($this.order.stockFrom.id))
                            } else {
                                Vue.set($this.stockFrom, 'id', null)
                                Vue.set($this.stockTo, 'id', null)
                            }

                            if ($this.order.stockTo) {
                                Vue.set($this.stockTo, 'id', parseInt($this.order.stockTo.id))
                            }

                            $this.errors.clear();
                            $this.htmlUp();
                        }
                    })
                }});


        },

        loadOrders: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter[search]', $this.filter.search);
            formData.append('filter[status]', $this.filter.status);
            formData.append('filter[field]', $this.filter.search ? $this.filter.field : '');
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsorder/orders",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result && result.data) {
                $this.orders = result.data.orders;
                $this.pagination = result.data.pagination;
                $this.loadOrdersEmpty = false;

                $this.loading = false;
            }
        },

        getDirection: function (id) {
            var result = null;

            if (!id) {
                id = this.direction;
            }

            $.each(this.directions, function( index, item ) {
                if (item.id == id) {
                    result = item;
                }
            })

            return result;
        },

        getStocksFrom: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityFrom.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
        getStocksTo: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityTo.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
        
        formatPhone: function (phone) {
			var phone = phone.replace(/[A-z\(\)-+ ]/g, '')
			
			if (phone[0] == '7') {
				phone = phone.substr(1, phone.length)
			}

			phone = phone.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "($1) $2-$3-$4");

			if (phone[0] == '7') {
				phone = phone.substr(1, phone.length)
			}

			return '+7 '+phone;
		},

        filesByGroup: function (group) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (file.group.id == group.id) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        filesWithoutGroups: function (groups) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var groupIds = [];
            $.each(groups, function( index, group ) {
                groupIds.push(group.id);
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    var group = file.group;
                    if (!group || group && $.inArray(group.id, groupIds) == -1 && !group.hidden) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        firstPathFileByGroup: function (group) {
            var files = this.filesByGroup(group);
            if (files.length > 0) {
                return files[0].file;
            }

            return null;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

    },
    mounted: function() {
        this.htmlUp();

        this.loadOrdersEmpty = true;
        this.loadOrders()
    },
    computed: {
        stocksFrom: function () {
            return this.getStocksFrom();
        },
        stocksTo: function () {
            return this.getStocksTo();
        },
        disabledAllOrders: function () {
            var $this = this;
            var result = true;
            $.each($this.orders, function( index, order ) {
                if (order.isRemoveExactStatus) {
                    result = false;
                }
            })

            return result;
        }
    },
    watch: {
        'uridical': function(val) {
            this.htmlUp();
        },
        'edit': function(val) {
            this.htmlUp();
        },
        'filter.search': function(val) {
            var self = this;
            self.pagination.page = 1;
            clearTimeout(self.timeout);
            self.timeout = setTimeout(function () {
                self.loadOrders();
            }, 600);

        },
        'filter.status': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
    }
})

$(function(){

    $(".js-select-all").change(function () {
        $('.order-table thead').toggleClass('blured');
        $(".order-table td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        }
    });

    $(".js-undelete").on("click", function() {
        $('.order-number')
            .find(":checkbox:checked")
            .closest('tr')
            .show();
        $('.order-table thead').removeClass('blured');
        $('.order-number input').prop('checked', false);
        $(".js-select-all").prop('checked', false);
    });

    $('body').on('click', '.order-number', function(){
        if ($('.order-number input:checked').length > 0) {
            $('.order-table thead').addClass('blured');
        } else {
            $('.order-table thead').removeClass('blured');
        }
    })

})
