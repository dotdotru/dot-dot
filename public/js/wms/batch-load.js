var wmsBatchLoad = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatchload',
    data: {
        loading: true,
        orderData: null,
        order: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPallets: [],
        batchSaved: false,
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },



    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();
    },
    computed: {
        totalSelectPallet: function () {
            return this.selectPallets.length;
        },  
        totalSelectWeightPallet: function () {
            var totalWeight = 0;
            $.each(this.selectPallets, function( index, pallet ) {
                totalWeight = totalWeight + pallet.weight;
            })
            return totalWeight;
        },
        totalSelectTimePallet: function () {
            var deliverToAt = null;
            $.each(this.selectPallets, function( index, pallet ) {
                if (!deliverToAt) {
                    deliverToAt = pallet.deliverToAt;
                }
                if (deliverToAt > convertDate(pallet.deliverToAt)) {
                    deliverToAt = pallet.deliverToAt;
                }
            })
            return deliverToAt;
        },
    },
    watch: {
        'orderData': function(val) {
            this.order = JSON.parse(val);
            this.loading = false;
            this.addAllPallets();
        },
    }
})
