var wmsOrderDelivered = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsorderdelivered',
    data: {
		loading: true,
        orderData: null,
        order: null,
        packageId: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPallets: []
    },
    methods: {

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('#wmsbatcharrived').css('visibility', 'visible');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },



        loadOrder: function () {
            var $this = this;
            var jqxhr = $.ajax({
                url: "/api/wms/wmsorder/order/"+$this.order.id,
                cache: false,
                async: false
            });

            var data = jqxhr.responseJSON.data;

            this.orderData = JSON.stringify(data.order);

            this.htmlUp();
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();


    },
    computed: {
        totalSelectPallet: function () {
            return this.selectPallets.length;
        },
    },
    watch: {
        'orderData': function(val) {
            this.order = JSON.parse(val);
            this.loading = false;
            $('#wmsbatcharrived').css('visibility', 'visible');
        },
    }
})
