var wmsPalletsIssue = new Vue({
    delimiters: ['${', '}'],
    el: '#wms-pallets-issue',
    data: {
        timeout: null,
        q: '',
        pallets: [],
        directions: [],
        mask: mask(),
        maskfilter: filter(),
    },
    methods: {
        unpalleted: function($pallet, $event) {

            var $this = this;

            var formData = new FormData();
            formData.append('pallet[id]', $pallet.id);

            openPopupButton('Вы уверены?', 'Паллета будет расформирована', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmspallet/unpalleted",
                    data: formData,
                    dataType: "json",
                    async: false,
                    processData: false,
                    contentType: false,
                });

                var data = jqxhr.responseJSON;

                if (data.status == true) {
                    $this.loadPallets();
                    closePopup();
                }
            });

            return false;
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        loadPallets: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter[search]', $this.q);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/issue-pallets",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var result = jqxhr.responseJSON;

            if (result.data) {
                $this.pallets = result.data.pallets;
                $this.pagination = result.data.pagination;
            }
        },
        
       isErrorWeightPallet: function(pallet) {
            if (pallet.weight && (parseFloat(pallet.weight) < 30 || parseFloat(pallet.weight) > 1500)) {
                return true;
            }
            return false;
       },
    },
    mounted: function() {
        this.htmlUp();
        this.loadPallets();

        $('#wms-pallets-issue').css('visibility', 'visible');

    },
    computed: {

    },
    watch: {
        'q': function() {
            var self = this;
            clearTimeout(self.timeout);
            self.timeout = setTimeout(function () {
                self.loadPallets();
            }, 600);
        },
    }
})
