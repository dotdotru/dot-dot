var wmsBatchLoad = new Vue({
    delimiters: ['${', '}'],
    el: '#wmsbatchload',
    data: {
        loading: true,
        orderData: null,
        order: null,
        filter: '',
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        selectPackages: [],
        batchSaved: false,
        countPackages: 1,
    },
    methods: {
        batchSave: function($event) {
            var $this = this;

            var formData = new FormData();
            formData.append('wmsBatch[countPackages]', this.countPackages);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-mover-save/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                $this.batchSaved = true;
                $this.loadOrder();
            }

            return false;
        },

        batchLoad: function($event) {
            var $this = this;

            var formData = new FormData();
            $.each($this.selectPackages, function( index, package ) {
                formData.append('wmsBatch[packageIds]['+index+']', package.id);
            })

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-mover-load/"+$this.order.id,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                location.href = '/wms/batch';
            }

            return false;
        },

        getFileByGroup: function($type) {
            var $this = this;
            var $result = null;

            $.each($this.order.files, function( index, file ) {
                if (file.group.slug == $type) {
                    $result = file;
                }
            })

            return $result;
        },

        uploadFile: function($event, $group) {
            var el = $event.target;
            var $this = this;

            var formData = new FormData();
            formData.append('file', el.files[0]);
            formData.append('batch[groupCode]', $group);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch-upload-file/"+$this.order.externalId,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var data = jqxhr.responseJSON;

            if (data.status) {
                $this.loadOrder();
            }

            return false;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        loadOrder: function() {

            var $this = this;

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmsbatch/batch/"+$this.order.externalId,
                dataType: "json",
                success: function($data){
                    $this.order = $data.data;
                    $this.countPackages = $this.order.packages.length;
                }
            });
        },

    },
    mounted: function() {
        var $this = this;
    },
    computed: {
        totalSelectPackage: function () {
            return this.selectPackages.length;
        },  
        totalSelectWeightPackage: function () {
            var totalWeight = 0;
            $.each(this.selectPackages, function( index, package ) {
                totalWeight = totalWeight + package.weight;
            })
            return totalWeight;
        },
        totalSelectTimePackage: function () {
            var deliverToAt = null;
            $.each(this.selectPackages, function( index, package ) {
                if (!deliverToAt) {
                    deliverToAt = package.deliverToAt;
                }
                if (deliverToAt > convertDate(package.deliverToAt)) {
                    deliverToAt = package.deliverToAt;
                }
            })
            return deliverToAt;
        },
    },
    watch: {
        'orderData': function(val) {
            this.order = JSON.parse(val);
            this.loading = false;
            this.countPackages = this.order.packages.length;
        },
        'countPackages': function(val) {
            this.batchSave();
        },
    }
})
