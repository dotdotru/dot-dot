var wmsPalletsShippingList = new Vue({
    delimiters: ['${', '}'],
    el: '#wms-pallets-shipping-list',
    data: {
        timeout: null,
        q: '',
        orders: [],
        directions: [],
        mask: mask(),
        maskfilter: filter(),
        maxWeight: 30,
        minWeight: 1500
    },
    methods: {
        unpalleted: function($pallet, $event) {

            var $this = this;

            var formData = new FormData();
            formData.append('pallet[id]', $pallet.id);

            openPopupButton('Вы уверены?', 'Паллета будет расформирована', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/wms/wmspallet/unpalleted",
                    data: formData,
                    dataType: "json",
                    async: false,
                    processData: false,
                    contentType: false,
                });

                var data = jqxhr.responseJSON;

                if (data.status == true) {
                    $this.loadOrders();
                    closePopup();
                    wmsPalletsShipping.loadPackages();
                }
            });

            return false;
        },

        palleted: function($pallet, $event) {
            var minWeight = $($event.target).attr('min');
            var maxWeight = $($event.target).attr('max');

            var button = $($event.target);
            if (button.hasClass('disabled')) {
                return false;
            }

            button.addClass('disabled');

            var $this = this;

            if (!$pallet.weight) {
                button.removeClass('disabled');
                return false;
            }

            var formData = new FormData();

            formData.append('pallet[palletId]', $pallet.id);
            formData.append('pallet[weight]', $pallet.weight);

            $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/palleted",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(data) {
                if (data.status == true) {
                    //button.removeClass('disabled');
                   
                    $this.loadOrders();
                    
                    if (wmsPalletsShipping) {
						wmsPalletsShipping.resetPallet();
						wmsPalletsShipping.loadPallets();
					}
                }
            });

            return false;
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);

            $('.pallets-wrapper').each(function () {
                var $this = $(this);
                if ($this.find('.pallet').length > 4) {
                    jQuery($this).scrollbar();
                }
            })
        },

        loadOrders: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter[search]', $this.q);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/wms/wmspallet/new-pallets",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var result = jqxhr.responseJSON;

            if (result.data) {
                $this.orders = result.data.orders;
                $this.pagination = result.data.pagination;
            }

            $this.htmlUp();
        },
        
        isAllowedPalleted: function(pallet) {
          
        },
        
        isErrorWeightPallet: function(pallet) {
            if (pallet.weight && (parseFloat(pallet.weight) < 30 || parseFloat(pallet.weight) > 1500)) {
                return true;
            }
            return false;
        },
    },
    computed: function() {

    },
    mounted: function() {
        this.htmlUp();
        this.loadOrders();

        $('#wms-pallets-shipping-list').css('visibility', 'visible');
    },
    computed: {

    },
    watch: {
        'q': function() {
            var self = this;
            clearTimeout(self.timeout);
            self.timeout = setTimeout(function () {
                self.loadPallets();
            }, 600);
        },
    }
})
