var carrierOrders = new Vue({
    delimiters: ['${', '}'],
    el: '#orders',
    data: {
        orderId: null,
        orders: [],
        order: null,
        filter: '',
        fileGroups: [],
        nextTime: 20,
        maxTime: 300,
        time: '',
        selectDrivers: null,
        pagination: {
            page : 1,
            perPage : 10,
            pages : 1,
        },
        loadOrdersEmpty: true,
        mask: mask(),
        sort: {
            by: 'id',
            direction: 'desc'
        },
        edit: false,
        loading: true
    },
    methods: {
		showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.loadOrders();
            }
        },
        
        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-toggle="tooltip"]').tooltip({
                    template: '<div class="tooltip" role="tooltip"><div class="close-tooltip"><img src="/img/close.png" alt="" /></div><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                })
            }, 200);
        },

        getSendDateMax: function() {
		    if (this.order && this.order.sendDate) {
                var date = new Date(this.order.sendDate.date);
                date.setHours(date.getHours() + 1);

                return date;
            }

		    return null;
        },

        sendDrivers: function() {

            var $this = this;

            this.$validator.validateAll().then(function(result) {

                if(!result) {
                    return;
                }

                var editStatus = $this.edit;

                $this.edit = false;

                var formData = new FormData();
                formData.append('drivers', $this.selectDrivers);

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/order/carrier/"+$this.orderId+"/send_drivers#",
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    if (!editStatus) {
                        counterEvent('ZAKAZ_PEREVOZCHIK', 'form_submit');
                    }

                    $this.loadOrders();
                }

            });

            return false;
        },

        stopSend: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', 'Заказ будет удален. После - можно разместить только новый.', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/order/carrier/"+$this.orderId+"/stop_send",
                    cache: false,
                    async: false,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    $($event.target).addClass('disabled');

                    $this.loadOrders();
                    closePopup();
                }
            });

            return false;
        },

        removeOrders: function($event) {

            var $this = this;

            openPopupButton('Вы уверены?', 'Выбранные заказы будут удалены!', '', function () {

                var ids = [];
                $('.order-number').find(":checkbox:checked").each(function () {
                    ids.push($(this).val());
                })

                var formData = new FormData();
                $.each(ids, function (index, id) {
                    formData.append('ids[]', id);
                })

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/order/carrier/remove",
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {

                    $('.order-number').find('input[type="checkbox"]:checked').prop('checked', false);
                    $('.order-table thead').removeClass('blured');
                    $(".js-select-all").prop('checked', false);

                    $this.loadOrders();
                    closePopup();
                }

            })

            return false;
        },

        editOrder: function($event) {

            var $this = this;

            if (!$this.edit) {

                $this.edit = true;

                setTimeout(function(){
                    $('.selectpicker').selectpicker('refresh');
                }, 200);

                return false;
            } else {
                $this.edit = false;
            }

            $this.htmlUp();

            return false;
        },

        acceptanceOrder: function($event) {
            var $this = this;

            var formData = new FormData();

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/order/carrier/acceptance/"+$this.orderId,
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                $this.loadOrders();
            }

            return false;
        },

        toggleOrder: function(orderId){

            var $this = this;

            if ($this.orderId == orderId) {
                $this.orderId = null;
                $this.selectDrivers = null;
            } else {
                var order = $this.loadOrder(orderId);

                $.ajax({url: "/api/order/carrier/config", success: function(result){
                    $this.fileGroups = result.data.params.fileGroups;

                    $this.htmlUp();
                }});
            }

            if (!order) {
                $this.orderId = null;
            }

            $this.edit = false;
            $this.htmlUp();
        },

        loadOrder: function(orderId){
            var $this = this;

            $this.orderId = orderId;

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    $this.order = item;

                    if ($this.order.drivers) {
                        $.each($this.order.drivers, function( index, driver ) {
                            $this.selectDrivers = driver.id
                        });
                    }

                    $this.htmlUp();
                }
            })

            $this.htmlUp();
            return $this.order;
        },

        loadOrders: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter', $this.filter);
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/order/carrier/orders",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result.data) {
                $this.orders = result.data.orders;
                $this.pagination = result.data.pagination;

                if ($this.orders.length && $this.loadOrdersEmpty) {
                    $this.loadOrdersEmpty = false;
                }
    
                $this.htmlUp();

                if ($this.orders) {
                    $($this.orders).each(function( index, order ) {
                        if (order.overload == false && order.chooseDrivers.length == 0 && order.realStatus == 'reserved') {
                            $(function(){
                                openPopupUrl('/static/popup/no_drivers');
                                $this.removeOrder(order.id);
                            })
                        }
                    });
                }
                
                $this.loading = false;
            }
        },

        filesByGroup: function (group) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (file.group && file.group.id == group.id) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        filesWithoutGroups: function (groups) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var groupIds = [];
            $.each(groups, function( index, group ) {
                groupIds.push(group.id);
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (!file.group || file.group && $.inArray(file.group.id, groupIds) == -1) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        firstPathFileByGroup: function (group) {
            var files = this.filesByGroup(group);
            if (files.length > 0) {
                return files[0].file;
            }

            return null;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },

        removeOrder: function (orderId, callback) {
            var formData = new FormData();
            formData.append('ids[]', orderId);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/order/carrier/remove",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                if (callback) {
                    callback();
                }
            }

            return '';
        },


        nextTimeUpdate: function () {
            var $this = this;

            if (this.order && this.orderId && !this.overload) {
                var dateTime = new Date();

                var createdAt = new Date(this.order.createdAt.date);

                this.nextTime = this.maxTime + parseInt((createdAt - dateTime)/1000);

                if (this.nextTime > 0) {
                    var date = new Date(2018, 0, 1, 0, 0, this.nextTime);
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    if (min < 10) {
                        min = "0" + min;
                    }
                    if (sec < 10) {
                        sec = "0" + sec;
                    }
                    this.time = min+':'+sec;
                }
                
                /*if (this.nextTime == 0 && !this.selectDrivers && this.order.organization.isWithoutDrivers == false) {
                    openPopupUrl('/static/popup/time');

                    this.removeOrder(this.orderId, function(){
                        $this.loadOrders();
                    });
                }*/
            }

            if ($this.orders && !this.overload) {
                var removeOrders = false;
                $($this.orders).each(function( index, order ) {
                    var dateTime = new Date();
                    var createdAt = new Date(order.createdAt.date);
                    var nextTime = $this.maxTime + parseInt((createdAt - dateTime)/1000);
        
                    if (nextTime == 0 && order.drivers.length == 0 && order.organization.isWithoutDrivers == false && order.realStatus == 'reserved') {
                        removeOrders = true;
                        $this.removeOrder(order.id);
                    }
                });
                
                if (removeOrders) {
                    openPopupUrl('/static/popup/time');
                    $this.loadOrders();
                }
            }

            return this.time;
        },

    },
    mounted: function() {
        var $this = this;
        $this.htmlUp();

        $this.loadOrdersEmpty = true;
        $this.loadOrders();

        $('#orders').css('visibility', 'visible');

        setInterval(function(){
            $this.nextTime--;
            $this.nextTimeUpdate();

        }, 1000);
    },
    computed: {
        disabledAllOrders: function () {
            var $this = this;
            var result = true;
            $.each($this.orders, function( index, order ) {
                if (order.isRemoveExactStatus) {
                    result = false;
                }
            })
            
            return result;
        },
    },
    watch: {
        'filter': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
        'pagination.page': function(val) {
            $('.order-table').find('thead').removeClass('blured');
        },
    }
})

$(function(){

    $("#orders .js-select-all").change(function () {
        $('#orders .order-table thead').toggleClass('blured');
        $("#orders .order-table td:not(.disabled) input:checkbox").prop('checked', $(this).prop("checked"));
        if ($('#orders .order-number input:checked').length > 0) {
            $('#orders .order-table thead').addClass('blured');
        }
    });

    $("#orders .js-undelete").on("click", function() {
        $('#orders .order-number')
            .find(":checkbox:checked")
            .closest('tr')
            .show();
        $('#orders .order-table thead').removeClass('blured');
        $('#orders .order-number input').prop('checked', false);
        $("#orders .js-select-all").prop('checked', false);
    });

    $('body').on('click', '#orders .order-number', function(){
        if ($('#orders .order-number input:checked').length > 0) {
            $('#orders .order-table thead').addClass('blured');
        } else {
            $('#orders .order-table thead').removeClass('blured');
        }
    })

})
