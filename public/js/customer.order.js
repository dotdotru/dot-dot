var customerOrders = new Vue({
    delimiters: ['${', '}'],
    el: '#orders',
    data: {
        autocompletePickUpAddress: [],
        autocompletePickUpAddressSelected: '',
        autocompletePickUpsTypingTimer: null,
        autocompletePickUpAddressFromSelect: false,
        autocompleteDeliverAddress: [],
        autocompleteDeliverAddressSelected: '',
        autocompleteDeliverTypingTimer: null,
        autocompleteDeliverAddressFromSelect: false,

        deliverHydroBoard: true,
        pickUpHydroBoard: true,

        loading: true,
        loadingOrder: false,
        step: 1,
        edit: false,
        filter: '',
        order: null,
        stockFrom: {
            id: null
        },
        stockTo: {
            id: null
        },
        pickUp: null,
        moverLoad: false,
        deliver: null,

        orderId: null,
        receiverId: null,
        uridical: 0,
        issuance: 'passport',
        type: 'ООО',
        inn: '',
        contactName: '',
        contactPhone: '',
        contactEmail: '',
        series: '',
        number: '',
        companyName: '',
        code: '',
        stocks: [],
        fileGroups: [],
        direction: null,
        orders: [],
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        organizationTypes: [],
        loadOrdersEmpty: true,
        mask: mask(),
        maskfilter: filter(),
        sort: {
            by: 'id',
            direction: 'desc'
        },
        notification: {
            firstPromocode: false
        },

        pickUpPrice: 0,
        deliverPrice: 0,
    },
    methods: {
        formatPhone: function (phone) {
            return formatPhone(phone);
        },

        openPopupOrderDelivery: function (order) {
            var $this = this;
            openPopupUrl('/static/popup/in_stock_delivery?orderId='+order.id, function(){
                $('body').off('click', '#openEdit').on('click', '#openEdit', function(){
                    $this.edit = true;
                    closePopup();
                })
            })

            return null;
        },

        timeOptions: function () {
            var $this = this;
            var start = '09:00';
            if (this.pickUp.date) {
                var checkDateItem = new Date(this.pickUp.date.getTime());
                checkDateItem.setHours(9);
                checkDateItem.setMinutes(0);

                for (var i = 1; i <= 42; i++) {
                    checkDateItem.setMinutes(checkDateItem.getMinutes() + 10);

                    if ($this.isDisabledDateTime(checkDateItem) == false) {
                        start = checkDateItem.getHours()+':'+checkDateItem.getMinutes();
                        break;
                    } 
                }
            }

            return { start: start, step: '00:10', end: '16:00' };
        },

        timeOptionsDeliver: function () {
            var $this = this;
            var start = '12:00';
            if (this.deliver.date) {
                var checkDateItem = new Date(this.deliver.date.getTime());
                checkDateItem.setHours(12);
                checkDateItem.setMinutes(0);

                for (var i = 1; i <= 54; i++) {
                    checkDateItem.setMinutes(checkDateItem.getMinutes() + 10);

                    if ($this.isDisabledDateTime(checkDateItem) == false) {
                        start = checkDateItem.getHours()+':'+checkDateItem.getMinutes();
                        break;
                    }
                }
            }

            return { start: start, step: '00:10', end: '21:00' };
        },

        isDisabledDateTime : function (date) {
            var result = true;
            var currentDate = new Date();
            var checkDate = new Date(date.getTime());

            if (this.order) {
                var orderCreateDate = new Date(this.order.createdAt.date);

                // 7 дней с момента создания заказа
                if (currentDate.getTime() < checkDate.getTime() && checkDate.getTime() - orderCreateDate.getTime() >= 0 && checkDate.getTime() - orderCreateDate.getTime() <= 604800000) {
                    result = false;
                }
            }

            return result;
        },

        disabledDates: function (date) {
            var $this = this;
            var result = true;

            var checkDate = new Date(date.getTime());

            if (this.order) {
                var orderCreateDate = new Date(this.order.createdAt.date);

                var checkDateItem = new Date(date.getTime());
                checkDateItem.setHours(9);
                checkDateItem.setMinutes(0);

                for (var i = 1; i <= 48; i++) {
                    checkDateItem.setMinutes(checkDateItem.getMinutes() + 10);

                    if ($this.isDisabledDateTime(checkDateItem) == false) {
                        result = false;
                    }
                }
            }

            if (checkDate.getDay() === 0 || checkDate.getDay() === 6) {
                result = true;
            }

            return result;
        },

        showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.loadOrders();
            }
        },

        showStep: function (step, validateStep, $event) {
            $this = this;

            $this.validateStep = step;

            $this.htmlUp();

            this.$validator.validateAll().then(function(result) {

                if(!result && validateStep == undefined) {
                    return;
                }

                if (step == 2) {
                    $this.autocompletePickUpAddress = [];
                    $this.autocompleteDeliverAddress = [];
                    $this.calculateMover();
                }

                $this.step = step;

                if ($this.step == 1) {
                    setTimeout(function(){
                        initDatepicker($('.flatpickr'), 24*7);
                    }, 250);
                }
            });

            return false;
        },

        getShippingFormData: function(formData) {
            var $this = this;

            formData.append('order[pickUp][id]', $this.pickUp.id);
            formData.append('order[pickUp][active]', $this.pickUp.active);
            formData.append('order[pickUp][date]', formatDateTime($this.pickUp.date));

            if ($this.pickUp.stock && $this.pickUp.stock.toString().trim()) {
                formData.append('order[pickUp][stock]', $this.pickUp.stock);
            } else {
                var stocks = $this.getStocksFrom();
                formData.append('order[pickUp][stock]', stocks[0]['id']);
            }

            formData.append('order[pickUp][address]', $this.pickUp.address);
            formData.append('order[pickUp][countLoader]', $this.pickUp.countLoader);
            formData.append('order[pickUp][hydroBoard]', $this.pickUp.hydroBoard);

            formData.append('order[deliver][id]', $this.deliver.id);
            formData.append('order[deliver][active]', $this.deliver.active);
            formData.append('order[deliver][date]', formatDateTime($this.deliver.date));

            if ($this.deliver.stock && $this.deliver.stock.toString().trim()) {
                formData.append('order[deliver][stock]', $this.deliver.stock);
            } else {
                var stocks = $this.getStocksTo();
                formData.append('order[deliver][stock]', stocks[0]['id']);
            }

            formData.append('order[deliver][address]', $this.deliver.address);
            formData.append('order[deliver][countLoader]', $this.deliver.countLoader);
            formData.append('order[deliver][hydroBoard]', $this.deliver.hydroBoard);

            return formData;
        },

        calculateMover: function() {
            var $this = this;

            if ($this.pickUp.active || $this.deliver.active) {
                $this.moverLoad = true;
            }

            var formData = new FormData();

            formData = $this.getShippingFormData(formData);
            formData.append('order[id]', $this.order.id);

            $.ajax({
                method: 'POST',
                url: "/api/order/customer/calcmover",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(answer) {

                counterEvent('recipient_details_form1', 'form_submit');

                if (answer.status == true) {
                    $this.moverLoad = false;
                    $this.pickUpPrice = answer.data.pickUpPrice;
                    $this.deliverPrice = answer.data.deliverPrice;
                }

                return false;
            });
        },

        createOrder: function($event) {

            var button = $($event.target);
            if (button.hasClass('disabled')) {
                return false;
            }

            button.addClass('disabled');

            var $this = this;

            this.$validator.validateAll().then(function(result) {

                if(!result) {
                    button.removeClass('disabled');
                    return false;
                }

                var formData = new FormData();
                formData.append('order[orderId]', $this.orderId);
                formData.append('order[receiver][id]', $this.receiverId);
                formData.append('order[receiver][uridical]', $this.uridical);
                if ($this.uridical && $this.type == null) {
                    $this.type = 'ООО';
                }
                formData.append('order[receiver][type]', $this.type);
                formData.append('order[receiver][inn]', $this.inn);
                formData.append('order[receiver][contactName]', $this.contactName);
                formData.append('order[receiver][contactPhone]', $this.contactPhone);
                formData.append('order[receiver][contactEmail]', $this.contactEmail);
                formData.append('order[receiver][series]', $this.series);
                formData.append('order[receiver][number]', $this.number);
                formData.append('order[receiver][companyName]', $this.companyName);

                formData.append('order[receiver][code]', $this.code);
                formData.append('order[receiver][issuance]', $this.issuance);

                formData = $this.getShippingFormData(formData);

                if ($this.stockFrom.id) {
                    formData.append('order[stockFrom]', $this.stockFrom.id);
                } else {
                    var stocks = $this.getStocksFrom();
                    formData.append('order[stockFrom]', stocks[0]['id']);
                }

                if ($this.stockTo.id) {
                    formData.append('order[stockTo]', $this.stockTo.id);
                } else {
                    var stocks = $this.getStocksTo();
                    formData.append('order[stockTo]', stocks[0]['id']);
                }

                $.ajax({
                    method: 'POST',
                    url: "/api/order/customer/receiver",
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                }).done(function(data) {
                    if (data.status == true) {
                        $this.edit = false;

                        counterEvent('recipient_details_form2', 'form_submit');
                        button.removeClass('disabled', 'form_submit');

                        $this.loadOrders(function(){
                            $this.loadOrder($this.orderId);
                        });
                    }
                });

                return false;

            })

            return false;
        },

        sendReceiver: function($event) {

            var jqxhr = $.ajax({
                method: 'POST',
                url: " /api/order/customer/"+this.orderId+"/send_receiver_email#",
                cache: false,
                async: false,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            data = jqxhr.responseJSON;

            if (data.status == true) {
                openPopup('Успешно отправлено');
            }

            return false;
        },

        stopSend: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', 'Заказ будет остановлен, изменить его статус можно будет только при обращении к администрации сайта', '', function () {
               var jqxhr = $.ajax({
                   method: 'POST',
                   url: " /api/order/customer/"+$this.orderId+"/stop_send",
                   cache: false,
                   async: false,
                   dataType: "json",
                   processData: false,
                   contentType: false,
               });

               data = jqxhr.responseJSON;

               if (data.status == true) {
                   $($event.target).addClass('disabled');

                   $this.loadOrders();
                   closePopup();
                   $this.showSpecialOfferPopup(data.result);
               }
            });

            return false;
        },

        pauseDelivery: function($event) {
            var $this = this;

            openPopupButton('Вы уверены?', 'Заказ будет остановлен, изменить его статус можно будет только при обращении к администрации сайта', '', function () {
                var jqxhr = $.ajax({
                    method: 'POST',
                    url: " /api/order/customer/"+$this.orderId+"/pause_delivery",
                    cache: false,
                    async: false,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {
                    $($event.target).addClass('disabled');

                    $this.loadOrders();
                    closePopup();
                }
            });

            return false;
        },

        showSpecialOfferPopup: function(specialPromoData) {
            this.notification.firstPromocode = true;
            
            if (typeof specialPromoData.calc !== "undefined" && typeof specialPromoData.calc.promocode !== "undefined" && specialPromoData.calc.autoAddPromocode) {
                var priceWithoutPromocode = specialPromoData.calc.minPriceWithoutPromocode + ' - ' + specialPromoData.calc.maxPriceWithoutPromocode;
                if (specialPromoData.calc.minPriceWithoutPromocode === specialPromoData.calc.maxPriceWithoutPromocode) {
                    priceWithoutPromocode = specialPromoData.calc.minPriceWithoutPromocode;
                }

                var priceWithPromocode = specialPromoData.calc.minPriceWithPromocode + ' - ' + specialPromoData.calc.maxPriceWithPromocode;
                if (specialPromoData.calc.minPriceWithPromocode === specialPromoData.calc.maxPriceWithPromocode) {
                    priceWithPromocode = specialPromoData.calc.minPriceWithPromocode;
                }

                setTimeout(function () {
                    openPopup(
                        'Поздравляем с первым заказом!',
                        'Дарим Вам скидку на первые ' + specialPromoData.calc.promocode.discountKilo + ' кг <br>по промокоду <br> <div class="promocode">'
                        + specialPromoData.calc.promocode.title + '</div> ' +
                        '<p class="promocode-price">Стоимость перевозки <span>' + priceWithoutPromocode + ' руб.</span>' +
                        ' ' + priceWithPromocode + ' руб.',
                        ''
                    );
                }, 1000);
            }
        },

        removeOrders: function($event) {

            var $this = this;

            openPopupButton('Вы уверены?', 'Выбранные заказы будут удалены!', '', function () {

                var ids = [];
                $('.order-number').find(":checkbox:checked").each(function () {
                    ids.push($(this).val());
                })

                var formData = new FormData();
                $.each(ids, function (index, id) {
                    formData.append('ids[]', id);
                })

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/order/customer/remove",
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status == true) {

                    $('.order-number').find('input[type="checkbox"]:checked').prop('checked', false);
                    $('.order-table thead').removeClass('blured');
                    $(".js-select-all").prop('checked', false);

                    $this.loadOrders();
                    closePopup();
                    $this.showSpecialOfferPopup(data.result);
                }

            })

            return false;
        },
        selectRefresh: function() {
            setTimeout(function(){
                $('.selectpicker').selectpicker('refresh');
            }, 300);
        },
        htmlUp: function(){
            setTimeout(function(){
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip({
                    template: '<div class="tooltip" role="tooltip"><div class="close-tooltip"><img src="/img/close.png" alt="" /></div><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                })
            }, 200);
        },

        toggleOrder: function(orderId) {
            var $this = this;

            if ($this.orderId == orderId) {
                $this.orderId = null;
            } else {
                $this.loadOrder(orderId)
            }
        },

        loadOrder: function(orderId){
            var $this = this;

            $this.loadingOrder = true;
            $this.orderId = orderId;

            $this.edit = false;
            $this.receiverId = null;
            $this.uridical = 0;
            $this.type = 'ООО';
            $this.inn = null;
            $this.contactName = null;
            $this.contactPhone = null;
            $this.contactEmail = null;
            $this.series = null;
            $this.number = null;
            $this.companyName = null;
            $this.stockFrom.id = null;
            $this.stockTo.id = null;

            $this.pickUp = Object.assign({}, Shipping);
            $this.deliver = Object.assign({}, Shipping);
            
            $this.errors.clear();
            $this.htmlUp();
            
            $.ajax({url: "/api/order/customer/config", success: function(result){
                $this.directions = result.data.params.directions;
                $this.stocks = result.data.params.stocks;
                $this.fileGroups = result.data.params.fileGroups;

                $this.organizationTypes = result.data.params.organizationTypes;

                $this.errors.clear();
                $this.htmlUp();

                $.each($this.orders, function( index, order ) {

                    if (order.id == $this.orderId) {

                        $this.order = Object.assign({}, order);

                        if (!$this.order.receiver) {
                            $this.edit = true;
                        }

                        if ($this.order.receiver) {
                            var receiver = $this.order.receiver;
                            $this.receiverId = receiver.id;
                            $this.uridical = receiver.uridical ? 1 : 0;
                            $this.type = receiver.type == null ? receiver.type : 'ООО';
                            $this.inn = receiver.inn != 'null' ? receiver.inn : '';
                            $this.contactName = receiver.contactName;
                            $this.contactPhone = receiver.contactPhone;
                            $this.contactEmail = receiver.contactEmail;
                            $this.series = receiver.series != 'null' ? receiver.series : '';
                            $this.number = receiver.number != 'null' ? receiver.number : '';
                            $this.companyName = receiver.companyName != 'null' ? receiver.companyName : '';
                            $this.code = receiver.code;
                            $this.issuance = receiver.issuance;
                        }

                        $this.pickUpHydroBoard = $this.order.pickUpHydroBoard;
                        $this.deliverHydroBoard = $this.order.deliverHydroBoard;

                        if ($this.order.direction) {
                            Vue.set($this, 'direction', $this.order.direction.id)
                        }

                        if ($this.order.stockFrom) {
                            Vue.set($this.stockFrom, 'id', parseInt($this.order.stockFrom.id))
                        } else {
							Vue.set($this.stockFrom, 'id', null)
							Vue.set($this.stockTo, 'id', null)
						}

                        if ($this.order.shippings && $this.order.shippings.pickUp) {
                            $this.pickUp = Object.assign({}, $this.order.shippings.pickUp);
                            if ($this.pickUp.date) {
                                $this.pickUp.date = new Date($this.pickUp.date)
                            }
                        }

                        if ($this.order.shippings && $this.order.shippings.deliver) {
                            $this.deliver = Object.assign({}, $this.order.shippings.deliver);
                            if ($this.deliver.date) {
                                $this.deliver.date = new Date($this.deliver.date)
                            }
                        }

                        if ($this.order.direction) {
                            Vue.set($this, 'direction', $this.order.direction.id)
                        }
                        
                        $this.errors.clear();
                        $this.htmlUp();
                        initDatepicker($('.flatpickr'), 24*7);

                        $this.deliverTooltipShow();

                        if ($this.order.promocode && $this.order.autoAddPromocode && !$this.notification.firstPromocode && $this.order.isCreated) {
							var priceWithoutPromocode = $this.order.minPriceWithoutPromocode + ' - ' + $this.order.maxPriceWithoutPromocode;
							if ($this.order.minPriceWithoutPromocode == $this.order.maxPriceWithoutPromocode) {
								priceWithoutPromocode = $this.order.minPriceWithoutPromocode;
							}
							
							var priceWithPromocode = $this.order.minPriceWithPromocode + ' - ' + $this.order.maxPriceWithPromocode;
							if ($this.order.minPriceWithPromocode == $this.order.maxPriceWithPromocode) {
								priceWithPromocode = $this.order.minPriceWithPromocode;
							}
							
                            openPopup(
                                'Поздравляем с первым заказом!',
                                'Дарим Вам скидку на первые '+$this.order.promocode.discountKilo+' кг <br>по промокоду <br> <div class="promocode">'+$this.order.promocode.title+'</div> ' +
                                '<p class="promocode-price">Стоимость перевозки <span>'+priceWithoutPromocode + ' руб.</span>' +
                                ' '+priceWithPromocode + ' руб.',
                                ''
                            );

                            $this.notification.firstPromocode = true;
                        }

                        setTimeout(function(){
                            $this.loadingOrder = false;
                        }, 200);
                    }
                })
            }});


        },

        setPickerText: function (text) {
            let datePicker = this.$children.find((item) => { return item.$options._componentTag === 'date-picker' });
            this.$set(datePicker, 'userInput', text);
        },

        deliverTooltipShow: function() {
            var $this = this;

            if ($this.order) {
                setTimeout(function(){

                    setTimeout(function(){
                        initDatepicker($('.flatpickr-deliver'), 24*7);
                    }, 250);

                    var tooltip = $('.js-deliver-tooltip-link');
                    if ($this.order.isEditDeliver) {
                        tooltip.tooltip('hide').tooltip('fixTitle');
                    } else {
                        tooltip.tooltip('fixTitle').tooltip('show');
                    }
                }, 500)
            }
        },

        loadOrders: function($callback) {

            var $this = this;

            var formData = new FormData();
            formData.append('filter', $this.filter);
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/order/customer/orders",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            var result = jqxhr.responseJSON;

            if (result.data) {
                $this.orders = result.data.orders;
                $this.pagination = result.data.pagination;
                $this.loadOrdersEmpty = false;
                $this.loading = false;

                if ($callback) {
                    $callback();
                }
            }
        },

        getDirection: function (id) {
            var result = null;

            if (!id) {
                id = this.direction;
            }

            $.each(this.directions, function( index, item ) {
                if (item.id == id) {
                    result = item;
                }
            })

            return result;
        },

        getStocksFrom: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityFrom.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
        getStocksTo: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityTo.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },

        filesByGroup: function (group) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (file.group && file.group.id == group.id) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        filesWithoutGroups: function (groups) {
            var $this = this;
            var order = null;

            if (!$this.orderId) {
                return [];
            }

            $.each($this.orders, function( index, item ) {
                if (item.id == $this.orderId) {
                    order = item;
                }
            })

            var groupIds = [];
            $.each(groups, function( index, group ) {
                groupIds.push(group.id);
            })

            var files = [];
            if (order && order.files) {
                $.each(order.files, function( index, file ) {
                    if (!file.group || file.group && $.inArray(file.group.id, groupIds) == -1) {
                        files.push(file);
                    }
                })
            }

            return files;
        },

        firstPathFileByGroup: function (group) {
            var files = this.filesByGroup(group);
            if (files.length > 0) {
                return files[0].file;
            }

            return null;
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
            return formatTime(date);
        },
        suggestPickUpAddress: function () {
            if(this.pickUp && this.pickUp.address) {
                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/suggest/address',
                    method: 'GET',
                    data: {'address':this.pickUp.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.autocompletePickUpAddress = data.data;
                        $this.autocompletePickUpAddressFromSelect = true;
                        $('.order-address-from .custom-autocomplete-select .selectpicker').selectpicker('show');
                        $('.order-address-from .custom-autocomplete-select .selectpicker').selectpicker('toggle');
                        $('.order-address-from .custom-autocomplete-select .bootstrap-select').toggleClass('open');
            
                        setTimeout(function(){
                            $('.order-address-from .selectpicker').selectpicker('refresh');
                        }, 300)
                    }
                });
            }
        },
        suggestDeliverAddress: function () {
            if(this.deliver && this.deliver.address) {
                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/suggest/address',
                    method: 'GET',
                    data: {'address':this.deliver.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.autocompleteDeliverAddress = data.data;
                        $this.autocompleteDeliverAddressFromSelect = true;
                        $('.order-address-to .custom-autocomplete-select .selectpicker').selectpicker('show');
                        $('.order-address-to .custom-autocomplete-select .selectpicker').selectpicker('toggle');
                        $('.order-address-to .custom-autocomplete-select .bootstrap-select').toggleClass('open');

                        setTimeout(function(){
                            $('.order-address-to .selectpicker').selectpicker('refresh');
                        }, 300)
                    }
                });
            }
        },
        pickUpAddressChange: function () {
            if(this.pickUp && this.pickUp.address) {

                if($('.order-address-from .custom-autocomplete-select .dropdown-menu').is(':visible')) {
                    return;
                }

                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/clean/address',
                    method: 'GET',
                    data: {'address':this.pickUp.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.pickUp.address = data.data;
                    }
                });
            }
        },
        deliverAddressChange: function () {
            if(this.deliver && this.deliver.address) {

                if($('.order-address-to .custom-autocomplete-select .dropdown-menu').is(':visible')) {
                    return;
                }

                var $this = this;
                $.ajax({ 
                    url: '/api/data-normalize/clean/address',
                    method: 'GET',
                    data: {'address':this.deliver.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.deliver.address = data.data;
                    }
                });
            }
        }
    },
    mounted: function() {
        this.htmlUp();

        this.pickUp = Object.assign({}, Shipping);
        this.deliver = Object.assign({}, Shipping);

        this.loadOrdersEmpty = true;
        this.loadOrders();

        initDatepicker($('.flatpickr'), 24*7);
    },
    computed: {
        stocksFrom: function () {
            return this.getStocksFrom();
        },
        stocksTo: function () {
            return this.getStocksTo();
        },
        disabledAllOrders: function () {
            var $this = this;
            var result = true;
            $.each($this.orders, function( index, order ) {
                if (order.isRemoveExactStatus) {
                    result = false;
                }
            })
            
            return result;
        },
        selectedOrders: function () {
            var $this = this;
            var ids = [];

            $('.order-number').find("checkbox:checked").each(function () {
                ids.push($(this).val());
            })

            return ids.length;
        }
    },
    watch: {
        'autocompletePickUpAddressSelected': function(val) {
            if(val) {
                this.pickUp.address = val;

                $('.order-address-from.custom-autocomplete-select .selectpicker').selectpicker('hide');
            }
        },
        'autocompleteDeliverAddressSelected': function(val) {
            if(val) {
                this.deliver.address = val;

                $('.order-address-to .custom-autocomplete-select .selectpicker').selectpicker('hide');
            }
        },
        'pickUp.address': function(val, oldValue) {
            if (val != oldValue) {
                var $this = this;
                clearTimeout(this.autocompletePickUpsTypingTimer);
                if (this.pickUp.address == this.autocompletePickUpAddressSelected) {
                    return false;  
                }
                if (this.autocompletePickUpAddressFromSelect) {
                    this.autocompletePickUpAddressFromSelect = false;
                    return false;
                }

                this.autocompletePickUpsTypingTimer = setTimeout(function () {
                    $this.suggestPickUpAddress();
                }, 600);
            }
        },
        'deliver.address': function(val, oldValue) {
            if (val != oldValue) {
                var $this = this;
                clearTimeout(this.autocompleteDeliverTypingTimer);
                if(this.deliver.address == this.autocompleteDeliverSelected) {
                    return false;
                }
                if(this.autocompleteDeliverAddressFromSelect) {
                    this.autocompleteDeliverAddressFromSelect = false;
                    return false;
                }

                this.autocompleteDeliverTypingTimer = setTimeout(function() {
                    $this.suggestDeliverAddress();
                }, 600);
            }
        },
        'uridical': function(val) {
            this.htmlUp();
        },
        'edit': function(val) {
            this.htmlUp();
            setTimeout(function(){
                initDatepicker($('.flatpickr-deliver'), 24*7);
            }, 250);
        },
        'filter': function(val) {
            this.loadOrders();
        },
        'sort.by': function(val) {
            this.loadOrders();
        },
        'sort.direction': function(val) {
            this.loadOrders();
        },
        'deliver.active': function(val) {
            var direction = this.getDirection();
            this.htmlUp();
            if (val && !this.deliver.address && direction) {
                this.deliver.address = direction.cityTo.title+', ';
            }
            this.deliverTooltipShow();
        },
        'pickUp.active': function(val) {
            var direction = this.getDirection();
            this.htmlUp();
            if (val && !this.pickUp.address && direction) {
                this.pickUp.address = direction.cityFrom.title+', ';
            }
        },
        'pagination.page': function(val) {
            $('.order-table').find('thead').removeClass('blured');
        },
    }
})
