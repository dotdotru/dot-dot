var DriverDocument = {
    name: '',
    type: null,
    series: null,
    number: null,
    date: null,
    place: null,
};

function getEmptyDriver() {
    var DriverDocumentPassport = Object.assign({}, DriverDocument);
    DriverDocumentPassport.type = 'passport';
    DriverDocumentPassport.name = 'Паспорт водителя';

    var DriverDocumentLicense = Object.assign({}, DriverDocument);
    DriverDocumentLicense.type = 'license';
    DriverDocumentLicense.name = 'Водительское удостоверение';

    var Driver = {
        id: false,
        verified: false,
        name: null,
        inn: null,
        birthdate: null,
        phone: null,
        documents: [
            Object.assign({}, DriverDocumentPassport),
            Object.assign({}, DriverDocumentLicense),
        ]
    };

    return Driver;
}

var UserObject = {
    id: null,
    username: '',
    firstname: '',
    series: '',
    number: '',
    passport: '',
    email: '',
    password: '',
    reenteredPassword: '',
    group: 'sender',
    subscribe: true,
};

var Organization = {
    id: null,
    uridical: 0,
    type: 'ООО',
    companyName: '',
    nds: 1,
    inn: '',
    kpp: '',
    ceo: '',
    address: '',
    contactName: '',
    contactPhone: '',
    rs: '',
    bik: '',
    drivers: [
        getEmptyDriver()
    ]
};