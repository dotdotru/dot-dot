$(function(){
    $('.news-slider__news-content').click(function(){
        $(this).toggleClass('open');
    });

    if ($(window).width() < 768) {
        $('.articles__top-section').addClass('small');
    }

    $('.tags-show-img').click(function(){
        $('.img1').toggleClass('img-hidden');
        $('.img2').toggleClass('img-hidden');
        $('.tags').toggleClass('tags-list_hide');
    });

    $('.tags__checkbox').on('change', function() {
        if($('.tags__checkbox:checked').length >= 3) {
            $.each($('.tags__checkbox'), function(){
                if(!$(this).is(':checked')){
                    $(this).attr('disabled','disabled');
                }
            });
        } else {
            $('.tags__checkbox').removeAttr('disabled');
        }

        var tagIds = [];
        $.each($('.tags__checkbox:checked'), function(){
            if($(this).is(':checked')){
                tagIds.push($(this).attr('value'));
            }
        });

        $.post("/api/news/list/html", { 'filter[tags][]': tagIds})
        .done(function( response ) {
            var parsed = $.parseHTML(response.data.html);
            var result = $("#article-slider", parsed);
            $('#article-slider').html(result.html());
            initArticleSlider();
        });
    });

    initArticleSlider();
})

function initArticleSlider()
{
    // Article swiper
    var articleSwiper = new Swiper('.article-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.article-slider .swiper-button-next',
            prevEl: '.article-slider .swiper-button-prev',
        }
    });
}