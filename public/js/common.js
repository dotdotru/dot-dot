function openPopup(head, text, addClass) {
    if (!text) {
        text = '';
    }
    $( "#popup" ).html('<div class="h2">'+head+'</div><p>'+text+'</p>');
    if (addClass) {
        $( "#popup" ).addClass(addClass);
    }

    $.magnificPopup.open({
        items: {
            src: '#popup',
        },
        type: 'inline'
    });
}

function openPopupButton(head, text, addClass, callback) {
    if (!text) {
        text = '';
    }

    $( "#popup" ).html(
        '<div class="h2">'+head+'</div><p>'+text+'</p><a href="#" class="back-link">Нет</a><a id="button" href="javascript:void(0)" class="btn-default">Подтверждаю</a>'
    )

    if (addClass) {
        $( "#popup" ).addClass(addClass);
    }

    if (callback) {
        $('body').off('click', '#button').on('click', '#button', function(){
            callback();
            return false;
        })
    }

    $('body').on('click', '.back-link', function(){
        closePopup();
        return false;
    })

    $.magnificPopup.open({
        items: {
            src: '#popup',
        },
        type: 'inline'
    });
}

function openPopupUrl(url, addClass, callback, callbackClose) {
    if (!callback) {
        $( "#popup" ).html('');

        $( "#popup" ).load( url );

        if (addClass) {
            $( "#popup" ).addClass(addClass);
        }

        $.magnificPopup.open({
            items: {
                src: '#popup',
            },
            type: 'inline',
        });

    } else {

        $.magnificPopup.open({
            type: 'ajax',
            items: {
                src: url,
            },
            closeOnBgClick: false,
            callbacks: {
                ajaxContentAdded: function() {
                    //$('.mfp-content').css('width', 'auto');
                    //$('.mfp-content').addClass('main-popup');
                    $('.mfp-content').addClass(addClass);
                    if (callback) {
                        callback();
                    }
                },
                close: function () {
                    if (callbackClose) {
                        callbackClose();
                    }
                    return false;
                }
            }
        });

    }
}

function closePopup(el) {
    $.magnificPopup.close();
}

function counterEvent(event, action) {
    if (typeof gtag === 'function') {
        gtag('event', event, {'event_category' : action});
        //ga('send', 'event', event, action);
    }
    if (typeof yaCounter50156956 === 'function') {
        yaCounter50156956.reachGoal(event);
    }
}

function compareGraph() {
    var column = $('.graph-columns li');

    column.each(function () {
        var $this = $(this),
            columnHeight = $this.attr('data-percent-from'),
            economyColumn = $this.parent().find('.economy-column'),
            economyTotal = $this.parent().find('.economy-total b'),
            biggerColumn = parseInt($this.parent().find('.bigger-column').attr('data-percent-from'), 10),
            dotColumn = $this.parent().find('.dot-column');

        $this.css('height', columnHeight);

        // set economy value
        economyColumn.css('height', (100 - parseInt(dotColumn.attr('data-percent-from'))) - (100 - biggerColumn) + '%');
        economyColumn.css('top', 100 - biggerColumn + '%');
        //economyTotal.text(Math.floor((100 - parseInt(dotColumn.attr('data-percent-from'))) - (100 - biggerColumn)) + '%');
    })
}