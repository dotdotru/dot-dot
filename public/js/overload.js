var overload = new Vue({
    delimiters: ['${', '}'],
    el: '#overload',
    data: {
        selectPallets: [],
        palletsTotal: [],
        palletsAll: [],
        pallets: [],
        directions: [],
        organizations: [],
        organization: null,
        driver: null,
        filter: {
            direction: '',
            stockFrom: '',
            stockTo: '',
        },
        pagination: {
            page : 1,
            perPage : 10,
            pages : 0,
        },
        mask: mask(),
        maskfilter: filter(),
        loadEmpty: false,
        sort: {
            by: 'id',
            direction: 'desc'
        },
        loading: true
    },
    methods: {
        showPage: function(page) {
            if (page >= 1 && page <= this.pagination.pages) {
                this.pagination.page = page;
                this.load();
            }

            location.href = '#overload';
        },

        htmlUp: function(){
            setTimeout(function(){
                initDatepicker($('.flatpickr'));
                initDatepickerDefault($('.flatpickr-default'));
                $('.selectpicker').selectpicker('refresh');

                $('[data-mask="phone"]').mask('+7 (999) 999 99-99');
                $('[data-toggle="tooltip"]').tooltip()
            }, 200);
        },

        formatDateTime: function (date) {
            return formatDateTime(date);
        },

        formatDate: function (date) {
            return formatDate(date);
        },

        formatTime: function (date) {
           return formatTime(date);
        },

        load: function() {

            var $this = this;

            var formData = new FormData();
            formData.append('filter[direction]', $this.filter.direction);
            formData.append('filter[stockFrom]', $this.filter.stockFrom);
            formData.append('filter[stockTo]', $this.filter.stockTo);
            formData.append('page', $this.pagination.page);
            formData.append('perPage', $this.pagination.perPage);
            formData.append('sort[by]', $this.sort.by);
            formData.append('sort[direction]', $this.sort.direction);

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/overload/pallets",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result.data) {

                $this.palletsTotal = result.data.palletsTotal;
                //$this.palletsAll = result.data.palletsAll;
                $this.pallets = result.data.pallets;

                $this.pagination = result.data.pagination;
                $this.loadEmpty = false;

                $this.loading = false;
            }
        },

        init: function() {

            var $this = this;

            var formData = new FormData();

            var jqxhr = $.ajax({
                method: 'POST',
                url: "/api/overload/config",
                cache: false,
                async: false,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
            });

            result = jqxhr.responseJSON;

            if (result.data) {
                $this.organizations = result.data.params.organizations;
                $this.directions = result.data.params.directions;
            }
        },

        reserve: function($event) {

            var $this = this;

            if ($this.selectPallets.length == 0) {
                openPopup('Выберите паллеты!');
                return false;
            }

            if ($($event.target).hasClass('disabled')) {
                return false;
            }

            this.$validator.validateAll().then(function(result) {

                if(!result) {
                    return false;
                }

                $($event.target).addClass('disabled');

                var formData = new FormData();
                formData.append('overload[direction]', $this.direction.id);
                formData.append('overload[stockFrom]', $this.stockFrom.id);
                formData.append('overload[stockTo]', $this.stockTo.id);
                formData.append('overload[weight]', $this.totalWeight);
                formData.append('overload[carrier]', $this.getOrganization.user.id);
                formData.append('overload[drivers][]', $this.driver);

                $.each($this.selectPallets, function( index, pallet ) {
                    formData.append('overload[pallets]['+index+']', pallet);
                });

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/overload/reserve",
                    cache: false,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                result = jqxhr.responseJSON;

                if (result.id) {
                    $this.load();
                    $this.selectPallets = [];
                    $this.organization = null;
                    $this.driver = null;

                    carrierOrders.loadOrders();
                }

            });

            return false;
        },

        getDirectionById: function($id) {
            var $this = this;
            var result = null;

            $.each($this.directions, function( index, direction ) {
                if (direction.id == $id) {
                    result = direction;
                }
            });

            return result;
        },

        addPallet: function (id) {
            var $this = this;
            if ($.inArray(id, this.selectPallets) == -1) {
                $this.selectPallets.push(id);
            }

            return $this.selectPallets;
        },

        removePallet: function (id) {
            var $this = this;
            $.each($this.selectPallets, function( index, palletId ) {
                if (id == palletId) {
                    $this.selectPallets.splice(index, 1);
                }
            })

            return $this.selectPallets;
        },

        togglePalletActive: function (pallet) {
            var $this = this;
            if ($this.isPalletActive(pallet)) {
                $this.removePallet(pallet);
            } else {
                $this.addPallet(pallet);
            }

            return $this.selectPallets;
        },

        isPalletActive: function (pallet) {
            var $this = this;
            if ($.inArray(pallet, $this.selectPallets) != -1) {
                return true;
            }

            return false;
        },

        isPalletCanSelected: function (pallet) {
            var $this = this;
            var firstPallet = $this.getPalletById($this.selectPallets.slice(0, 1));

            if (firstPallet) {
                if (firstPallet.direction.id == pallet.direction.id
                    && firstPallet.stockFrom.id == pallet.stockFrom.id
                    && firstPallet.stockTo.id == pallet.stockTo.id
                ) {
                    return true;
                }
            } else {
                return true;
            }

            return false;
        },

        getPalletById: function (id) {
            var $this = this;
            var result = null;

            $.each($this.palletsTotal, function( index, pallet ) {
                if (parseInt(pallet.externalId) == parseInt(id)) {
                    result = pallet ;
                }
            });

            return result;
        },

        getStocksFrom: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityFrom.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
        getStocksTo: function () {
            var direction = this.getDirection();

            var result = [];

            if (direction) {
                $.each(this.stocks, function( index, item ) {
                    if (direction.cityTo.id == item.city.id) {
                        result.push(item);
                    }
                });
            }

            return result;
        },
    },
    mounted: function() {
        this.htmlUp();

        $('#overload').css('visibility', 'visible');

        this.init();
        this.load();
    },
    computed: {
        getStockFromIds: function () {
            var $this = this;
            var result = [];
            $.each(this.palletsTotal, function( index, pallet ) {
                var stockId = pallet.stockFrom.id;
                if ($.inArray(stockId, result) == -1) {
                    result.push(stockId);
                }
            });

            return result.sort();
        },

        getStockToIds: function () {
            var result = [];
            var $this = this;

            var direction = $this.getDirectionById($this.filter.direction);

            $.each($this.palletsTotal, function( index, pallet ) {
                var stockId = pallet.stockTo.id;
                if ($.inArray(stockId, result) == -1) {
                    if (direction && direction.cityTo.id == pallet.stockTo.city.id) {
                        result.push(stockId);
                    } else if(!direction) {
                        result.push(stockId);
                    }
                }
            });
            return result.sort();
        },

        totalWeight: function (id) {
            var $this = this;
            var result = 0;
            $.each($this.selectPallets, function( index, palletId ) {
                if (palletId) {
                    var pallet = $this.getPalletById(palletId);
                    result = result + pallet.weight;
                }

            });

            return result;
        },

        direction: function () {
            var $this = this;

            var firstPallet = $this.getPalletById($this.selectPallets.slice(0, 1));

            if (!firstPallet) {
                return false;
            }
            return firstPallet.direction;
        },

        stockFrom: function () {
            var $this = this;

            var firstPallet = $this.getPalletById($this.selectPallets.slice(0, 1));

            if (!firstPallet) {
                return false;
            }

            return firstPallet.stockFrom;
        },

        stockTo: function () {
            var $this = this;

            var firstPallet = $this.getPalletById($this.selectPallets.slice(0, 1));

            if (!firstPallet) {
                return false;
            }

            return firstPallet.stockTo;
        },

        deliveryDate: function () {
            var $this = this;

            var result = false;

            $.each($this.selectPallets, function( index, palletId ) {
                var pallet = $this.getPalletById(palletId);
                var date = convertDate(pallet.maxDeliveryTime);
                if (!result || result.getTime() >= date.getTime()) {
                    result = new Date(date.getTime());
                }

            });

            return result;
        },

        getOrganization: function () {
            var $this = this;

            var result = false;

            $.each($this.organizations, function( index, organization ) {
                if ($this.organization == organization.id) {
                    result = organization;
                }
            });

            return result;
        },

    },
    watch: {
        'uridical': function(val) {
            this.htmlUp();
        },
        'edit': function(val) {
            this.htmlUp();
        },
        'filter.direction': function(val) {
           this.load();
        },
        'filter.stockFrom': function(val) {
            this.load();
            this.htmlUp();
        },
        'filter.stockTo': function(val) {
            this.load();
            this.htmlUp();
        },
        'sort.by': function(val) {
            this.load();
        },
        'sort.direction': function(val) {
            this.load();
        },
        'organization': function(val) {
            this.driver = null;
            this.htmlUp();
        },
        'driver': function(val) {
            this.htmlUp();
        },
    }
})
