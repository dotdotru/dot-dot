$(function(){
        var app = new Vue({
            el: '#customer_calc',
            data: {
                confirm: true,
                user: false,
                maxPrevStep: 0,
                prevStep: 1,
                step: 1,
                price: null,
                directions: [],
                packageTypes: [],
                packings: [],
                packing: null,
                stocks: [],
                direction: null,
                cityFrom: null,
                cityTo: null,
                packages: [
                    Object.assign({}, Package),
                ],
                maxWeight: 1450,
                totalPriceMin: 0,
                totalPriceMax: 0,

                totalFirstMilePriceMin: 0,
                totalFirstMilePriceMax: 0,
                totalLastMilePriceMin: 0,
                totalLastMilePriceMax: 0,

                maxWidth: 80,
                maxHeight: 180,
                maxDepth: 120,
                hiddenIndex: 1,
                mask: mask(),
                filter: filter(),
                promocode: '',
                appliedPromocode: '',
                usePromocode: false,
                competitorPrices: [],
                competitorPricesLoad: false,
                validateStep: 1,
                isEmptyPacking: false,
                byCargo: false,
            },
            methods: {
                showStep: function (step, validateStep, $event) {
                    $this = this;

                    this.validateStep = step;
                    
                    $this.htmlUp();

                    this.$validator.validateAll().then(function(result) {

                        if(!result && validateStep == undefined) {
                            return;
                        }

                        if (step == 2 && validateStep == undefined) {
							counterEvent('sender_calc_step1', 'click');
							
                            $.each($this.packages, function( index, item ) {
                                $this.packages[index].packing = '';
                            });

                            $this.packingRecommended();
                            $this.checkEmptyPacking();
                        }

                        if (step == 3 && validateStep == undefined) {
                            setTimeout(function () {
                                $('.js-lead').trigger('shown.bs.popover');
                                $('.js-lead').popover('show');
                            }, 200);

                            $this.getPrice();

                            $this.competitorPrices = [];
                            $this.competitorPricesLoad = false;
                            $this.getCompetitorPrice(); 
                        }

                        if (step == 4) {
                            $this.createOrder($event);
                            return false;
                        }

                        if ($this.maxPrevStep < $this.step) {
                            $this.maxPrevStep = $this.step;
                        }
                        $this.prevStep = $this.step;
                        $this.step = step;

                        $this.htmlUp();

                        $('html, body').animate({
                            scrollTop: $('#customer_calc').offset().top
                        }, 200);

                    });

                    return false;
                },

                getData: function () {
                    var jqxhr = $.ajax({
                        url: "/api/customer/calc/config",
                        cache: true,
                        async: false
                    });

                    data = jqxhr.responseJSON.data;

                    this.directions = data.params.directions;

                    /* Предустановим направление */
                    //this.direction = this.directions[0].id;

                    this.packageTypes = data.params.packageTypes;
                    this.packings = data.params.packings;
                    this.stocks = data.params.stocks;
                    this.maxWeight = data.settings.max_weight;
                    this.maxWidth = data.settings.max_width;
                    this.maxHeight = data.settings.max_height;
                    this.maxDepth = data.settings.max_depth;
                },
                getPacking: function (packingId) {
                    var result = null;
                    $.each(this.packings, function( index, item ) {
                        if (item.id == packingId) {
                            result = item;
                        }
                    });

                    return result;
                },
                getPackageType: function (packageType) {
                    var result = null;
                    $.each(this.packageTypes, function( index, item ) {
                        if (item.id == packageType) {
                            result = item;
                        }
                    });

                    return result;
                },
                getPackageTypeTitle: function (package) {
                    var result = null;

                    if (package.anotherType && !parseInt(package.type)) {
                        return package.anotherType;
                    }

                    $.each(this.packageTypes, function( index, item ) {
                        if (item.id == package.type) {
                            result = item;
                        }
                    });

                    return result != null ? result.title : '';
                },
                calculateWeight: function (width, height, depth) {
                    return (width*height*depth)/4000;
                },
                calculateWeightVolume: function (volume) {
                    return volume*1000/4;
                },
                getTariff: function () {
                    return 5;
                },
                /*getPricePacking: function (packingId, ) {
                    var result = null;
                    $.each(this.packings, function( index, item ) {
                        if (item.id == packingId) {
                            result = item;
                        }
                    });

                    return result;
                },*/
                getPrice: function () {
                    
                    var jqxhr = $.ajax({
                        url: "/api/customer/calc/price",
                        cache: true,
                        async: false,
                        data: {
                            price: this.price,
                            direction: this.direction,
                            packages: this.packages,
                            promocode: this.appliedPromocode,
                            byCargo: this.byCargo,
                        }
                    });

                    data = jqxhr.responseJSON.data;

                    if (data != undefined) {

                        if (typeof data.error != "undefined") {
                            this.totalPriceMin = 0;
                            this.totalPriceMax = 0;
                            this.totalFirstMilePriceMin = 0;
                            this.totalFirstMilePriceMax = 0;
                            this.totalLastMilePriceMin = 0;
                            this.totalLastMilePriceMax = 0;
                        } else {
                            this.usePromocode = true;
                            this.totalPriceMin = data.minPricePromocode;
                            this.totalPriceMax = data.maxPricePromocode;

                            this.totalFirstMilePriceMin = data.totalFirstMilePriceMin;
                            this.totalFirstMilePriceMax = data.totalFirstMilePriceMax;
                            this.totalLastMilePriceMin = data.totalLastMilePriceMin;
                            this.totalLastMilePriceMax = data.totalLastMilePriceMax;
                        }
                    }

                },
                getCompetitorPrice: function () {
                    var $this = this;

                    $this.competitorPricesLoad = false;

                    var jqxhr = $.ajax({
                        url: "/api/customer/calc/competitor_price",
                        cache: true,
                        async: true,
                        dataType: "json",
                        data: {
                            price: this.price,
                            direction: this.direction,
                            packages: this.packages,
                            promocode: this.appliedPromocode,
                            byCargo: this.byCargo,
                        },
                        success: function(data){
                            if (typeof data.error != "undefined") {
                                $this.competitorPrices = [];
                            } else {
                                $this.competitorPrices = data.data;
                            }
                            $this.competitorPricesLoad = true;

                            setTimeout(function () {
                                $('.js-lead').trigger('shown.bs.popover');
                            }, 200);


                        },
                    });
                },
                checkPromocode: function () {
                    this.appliedPromocode = this.promocode;
                    this.getPrice();
                },

                getDirection: function (id) {
                    var result = null;

                    if (!id) {
                        id = this.direction;
                    }

                    $.each(this.directions, function( index, item ) {
                        if (item.id == id) {
                            result = item;
                        }
                    })

                    return result;
                },
                checkPacking: function () {
                    var packing = this.packing;
                    var found = false;
                    $.each(this.getPackingsForSize, function( index, item ) {
                        if (item.id == packing) {
                            found = true;
                        }
                    })

                    if (!found) {
                        this.packing = null;
                    }

                    return found;
                },
                checkSize: function (package, packingItem) {
                    var width = package.width;
                    var height = package.height;
                    var depth = package.depth;
                    var packageType = package.type;

                    var calculateWeight = package.calculateWeight;

                    if (packingItem.size) {
                        var size = packingItem.size.split('x');

                        size[0] = parseInt(size[0]);
                        size[1] = parseInt(size[1]);
                        size[2] = size[2] == undefined ? false : parseInt(size[2]);
                    } else {
                        var size = [];
                        size[0] = parseInt(maxWidth);
                        size[1] = parseInt(maxHeight);
                        size[2] = parseInt(maxDepth);
                    }

                    // мешок + указан только объем
                    if (!width && !height && !depth && !size[2])
                    {
                        //V < (x*y^2)/4(*100*100*100)
                        var V1 = (size[0] * size[1] * size[1]) / 4000000;
                        var V2 = (size[1] * size[0] * size[0]) / 4000000;

                        // добавляем 50 %
                        var V1 = V1 - (V1*50)/100;
                        var V2 = V2 - (V2*50)/100;

                        if (calculateWeight < V1) {
                            return true;
                        }

                        if (calculateWeight < V2) {
                            return true;
                        }

                        return false;
                    }

                    // указкан только объем
                    if (!width && !height && !depth) {
                        if (calculateWeight < ((size[0] * size[1] * size[2]) / 1000000)) {
                            return true;
                        }
                        return false;
                    }

                    // мешок
                    if(size[2] == false)
                    {
                        var minL1 = width + height;
                        var minH1= (width*height + height*depth + width*depth)/minL1;

                        var minL2 = width + depth;
                        var minH2= (width*depth + height*depth + width*height)/minL2;

                        var minL3 = depth + height;
                        var minH3= (depth*height + height*width + width*depth)/minL3;

                        // добавляем 25 %
                        var sizeW = size[0] - (size[0]*25)/100;
                        var sizeH = size[1] - (size[1]*25)/100;

                        if((sizeW >= minL1 && sizeH >= minH1) || (sizeH >= minL1 && sizeW >= minH1)) {
                            return true;
                        }

                        if((sizeW >= minL2 && sizeH >= minH2) || (sizeH >= minL2 && sizeW >= minH2)) {
                            return true;
                        }

                        if((sizeW >= minL3 && sizeH >= minH3) || (sizeH >= minL3 && sizeW >= minH3)) {
                            return true;
                        }
                        return false;
                    }

                    if (
                        ((size[0] >= width && size[1] >= height) || (size[1] >= width && size[0] >= height ))
                        && ((size[2] > 0 && size[2] >= depth) || (size[2] == false))) {

                        return true;
                    }
                    return false;
                },
                getPackingsForSize: function (package) {
                    var $this = this;
                    var result = [];
                    $.each(this.packings, function( index, item ) {
                        if (item.type == 'for_one') {
                            if (package.weight && $this.checkSize(package, item))  {
                                result.push(item);
                            }

                        } else if (item.type == 'to_size') {
                            result.push(item);
                        }
                    });
                    return result;
                },
                changeCalculateWeight: function (package) {
                    package.enterVolume = true;

                    if (!this.byCargo && package.calculateWeight > 1.728) {
                        package.calculateWeight = 1.728;
                    }
                },

                htmlUp: function () {
                    setTimeout(function () {
                        initSelect();

                        $('[data-toggle="tooltip"]').tooltip({
                            template: '<div class="tooltip" role="tooltip"><div class="close-tooltip"><img src="/img/close.png" alt="" /></div><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                        })

                        $('.js-send-type').each(function(){
                            var parent = $(this).parents('.packages');
                            var block = $('#'+parent.attr('id'));

                            var selected = block.find('.js-send-type').find("option:selected").text();
                            if (selected == "Другое") {
                                setTimeout(function() {
                                    block.find('.bootstrap-select.js-send-type').parent().addClass('another-active');
                                    block.find('.js-another-field').addClass('active');
                                    setTimeout(function() {
                                        block.find('.js-another-field').focus();
                                    }, 500);
                                }, 100);
                            } else {
                                block.find('.bootstrap-select.js-send-type').parent().removeClass('another-active');
                                block.find('.js-another-field').removeClass('active');
                            }
						})

                        $('body').on('click', '.js-lead', function (e) {
                            $('.js-lead').trigger('shown.bs.popover');
                        });

                        $('.js-lead').popover({
                            trigger: 'click | focus',
                            container: 'body',
                            placement: function (context, src) {
                                $(context).addClass('p-lead');
                                $(context).addClass('in');
                                return 'right';
                            },
                            html: true,
                            content: $('.js-lead-content').html()
                        });

                        $('.js-lead').on('shown.bs.popover', function () {
                            var _popover = $(this).attr('data-content',  $('.js-lead-content').html()).data('bs.popover');
                            _popover.setContent();
                            $('.js-lead').addClass('in');
                        });

                        $(document).on('click', '.js-lead-close', function (e) {
                            e.preventDefault();
                            $('.js-lead').popover('hide')
                            $('.js-lead').removeClass('in');
                        });

                        // клик за поповером
                        $(document).on('click', function (e) {
                            $('.js-lead').each(function () {
                                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                                    (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false  // fix for BS 3.3.6
                                }
                            });
                        });

                    }, 100);
                },

                createPackage: function (index) {
                    var $this = this;

                    this.packages.splice(index + 1, 0, Object.assign({}, Package));

                    $.each(this.packages, function( index, package ) {
                        if (!package.hidden) {
                            package.hidden = $this.hiddenIndex + 1;
                            $this.hiddenIndex = $this.hiddenIndex + 1;
                        }
                    })

                    this.htmlUp();

                    return false;
                },

                removePackage: function (index) {
                    if (this.packages.length <= 1) {
                        return;
                    }

                    this.packages.splice(index, 1);

                    return false;
                },

                getPackings: function (package) {
                    var width = this.width;
                    var height = this.height;
                    var depth = this.depth;
                    var result = [];

                    return this.getPackingsForSize(package);
                },

                getPackingPrice: function (package) {
                    var $this = this;
                    var packingPrice = 0;
                    var packing = this.getPacking(package.packing);
                    var exist = false;
                    var result = 0;

                    if (packing) {
                        $.each(this.getPackings(package), function( index, item ) {
                            if (item.id == packing.id) {
                                exist = true;
                            }
                        });

                        if (exist) {
                            if (packing.type == 'for_one') {
                                packingPrice = packing.price;
                            } else if (packing.type == 'to_size') {
                                if (package.calculateWeight > 0) {
                                    packingPrice = packing.price * package.calculateWeight;
                                } else {
                                    packingPrice = packing.price * $this.getVolume(package.width, package.height, package.depth);
                                }
                            }
                        } else {
                            $.each(this.packages, function( index, packageItem ) {
                                if (packageItem.id == package.id) {
                                    $this.packages[index].packing = '';
                                }
                            })
                        }
                    }
                    
                    
                    if ($this.byCargo) {
						result = Math.ceil(packingPrice);
					} else {
						result = Math.ceil(packingPrice * package.count);
					}
                    return result;
                },

                getPackingPriceTotal: function () {
                    var totalPackingPrice = 0;
                    var packingPrice = 0;

                    var $this = this;

                    $.each(this.packages, function( index, package ) {
                        var packing = $this.getPacking(package.packing);
                        if (packing) {
                            if (packing.type == 'for_one') {
                                packingPrice = packing.price;
                            } else if (packing.type == 'to_size') {
                                if (package.calculateWeight > 0) {
                                    packingPrice = packing.price * package.calculateWeight;
                                } else {
                                    packingPrice = packing.price * $this.getVolume(package.width, package.height, package.depth);
                                }
                            }
                        }
                        
                        if ($this.byCargo) {
							totalPackingPrice = totalPackingPrice + packingPrice;
						} else {
							totalPackingPrice = totalPackingPrice + packingPrice * package.count;
						}
                    })

                    return Math.ceil(totalPackingPrice);
                },

                getVolume: function (width, height, depth) {
                    return depth*height*width / 1000 / 1000;
                },

                createOrder: function ($event) {

                    var button = $($event.target);
                    if (button.hasClass('disabled')) {
                        return false;
                    }

                    button.addClass('disabled');

                    var packages = this.packages;

                    var formData = new FormData();
                    formData.append('orderCustomer[sender]', this.user);
                    formData.append('orderCustomer[direction]', this.direction);
                    formData.append('orderCustomer[declaredPrice]', this.price);
                    formData.append('orderCustomer[minPrice]', this.totalPriceMin);
                    formData.append('orderCustomer[maxPrice]', this.totalPriceMax);
                    formData.append('orderCustomer[promocode]', this.appliedPromocode);
                    formData.append('orderCustomer[byCargo]', this.byCargo ? 1 : 0);

                    $.each(packages, function( index, package ) {
                        formData.append('orderCustomer[packages]['+index+'][packageType]', package.type);
                        formData.append('orderCustomer[packages]['+index+'][packageTypeAnother]', package.anotherType);
                        formData.append('orderCustomer[packages]['+index+'][count]', package.count);
                        formData.append('orderCustomer[packages]['+index+'][weight]', package.weight);
                        if (package.packing > 0) { 
                            formData.append('orderCustomer[packages]['+index+'][packing]', package.packing);
                        }
                        formData.append('orderCustomer[packages]['+index+'][depth]', package.depth);
                        formData.append('orderCustomer[packages]['+index+'][width]', package.width);
                        formData.append('orderCustomer[packages]['+index+'][height]', package.height);
                        formData.append('orderCustomer[packages]['+index+'][calculateWeight]', package.calculateWeight);

                    });

                    $.ajax({
                        method: 'POST',
                        url: "/api/customer/calc/create_order",
                        cache: false,
                        async: true,
                        data: formData,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                    }).done(function(data) {
                        console.log(data)

/*
                        if(jqxhr.status == 403) {
                            openPopupUrl('/static/popup/sender/auth');
                        }
*/

                        if (data.id > 0) {
                            counterEvent('ZAKAZ', 'form_submit');

                            if(data.notCustomer) {
                                openPopupUrl('/static/popup/customer/auth?orderId'+data.id);
                            } else {
                                location.href = '/personal?group=sender&orderId='+data.id;
                            }
                            //button.removeClass('disabled');
                        }
                    });

                    return true;
                },
                fixSize: function (package) {
					if (package) {
						width = parseInt(package.width);
						height = parseInt(package.height);

						if (isNaN(width)) {
							width = 1;
						}
						if (isNaN(height)) {
							height = 1;
						}

						if (parseInt(package.height) > 0) {
							package.height = parseInt(package.height);

							if (package.height > this.maxHeight) {
								package.height = this.maxHeight;
							}
						}

						if (parseInt(package.width) > 0) {
							package.width = parseInt(package.width);

							if (package.width > this.maxWidth) {
								package.width = this.maxWidth;
							}
						}

						if (parseInt(package.depth) > 0) {
							package.depth = parseInt(package.depth);

							if (package.depth > this.maxDepth) {
								package.depth = this.maxDepth;
							}
						}

						if (package.weight > this.maxWeight && !this.byCargo) {
							package.weight = this.maxWeight;
						}
						
						if (package.weight < 1 && package.weight != null && package.weight != '') {
							package.weight = 1;
						}
						
						if (package.count < 1 && package.count != null && package.count != '') {
							package.count = 1;
						}
						
						if (package.height < 1 && package.height != null && package.height != '') {
							package.height = 1;
						}
						
						if (package.width < 1 && package.width != null && package.width != '') {
							package.width = 1;
						}

						if (package.depth < 1 && package.depth != null && package.depth != '') {
							package.depth = 1;
						}

						this.useSize(package);
					}
                    
                    if (this.price < 1 && this.price != null && this.price != '') {
                        this.price = 1;
                    }

                    return package;
                },
                useSize: function (package) {
                    var $this = this;
                    package.useSize = null;
                    if (package && ((package.width || package.height || package.depth) || package.calculateWeight) ) {
						if (package.calculateWeight) {
							package.calculateWeight = package.calculateWeight.toString().replace(',', '.')
						}

                        width = parseInt(package.width);
                        height = parseInt(package.height);
                        depth = parseInt(package.depth);

                        if (width || height || depth) {
                            package.useSize = true;
                            package.enterVolume = false;
                        } else if (package.calculateWeight) {
                            package.useSize = false;
                        }

                        if (!package.enterVolume && !(width || height || depth)) {
                            package.calculateWeight = '';
                            package.useSize = null;
                        }
                    }

                    if (package.depth || package.height || package.width) {
                        depth = parseInt(package.depth);
                        height = parseInt(package.height);
                        width = parseInt(package.width);

                        var calculateWeight = parseFloat((depth * height * width) / 1000000);
                        if (calculateWeight > 0) {
                            package.calculateWeight = parseFloat((depth * height * width) / 1000000);
                        } else {
                            package.calculateWeight = '';
                        }
                    }

                    this.validateStep = 1;

                    return package;
                },
                packingRecommended: function () {
                    var $this = this;

                    $.each($this.packages, function( index, package ) {
                        var packageType = $this.getPackageType(package.type);
                        var packings = $this.getPackings(package);
                        if (packings) {
                            $.each($this.getPackings(package), function( index2, packing ) {
                                if (packageType && packageType.packingsRecommended) {
                                    $.each(packageType.packingsRecommended, function( index2, packingRecommended ) {
                                        if (!$this.packages[index].packing && packingRecommended.id == packing.id) {
                                            $this.packages[index].packing = packing.id;
                                            $this.checkEmptyPacking();
                                        }
                                    })
                                }
                            })
                        }
                    })

                    return '';
                },
                changeType: function (package) {
                    package.packing = null;
                    this.htmlUp();  
                    return '';
                },
                checkEmptyPacking: function () {
                    var $this = this;
                    var result = false;
                    $.each($this.packages, function( index, item ) {
                        if (item.packing == null || item.packing == '' || item.packing == 'null') {
                            result = true;
                        }
                    });
                    this.isEmptyPacking = result;
                    return result;
                },

                toggleCalculateByCargo: function () {
                    var $this = this;
                    $this.byCargo = !$this.byCargo;

                    if (!$this.byCargo) {
                        $.each($this.packages, function( index, item ) {
                            $this.fixSize(item)
                            $this.changeCalculateWeight(item);
                        })

                    }
                },

                changeDirection: function () {
                    var $this = this;
                    var result = null;

                    $.each($this.directions, function( index, item ) {
                        if (item.cityFrom.id == $this.cityFrom && item.cityTo.id == $this.cityTo) {
                            result = item;
                        }
                    })

                    if (result) {
                        this.direction = result.id;
                    }
                    return false;
                },
            },
            computed: {
                stocksFrom: function () {
                    var direction = this.getDirection();

                    var result = [];

                    if (direction) {
                        $.each(this.stocks, function( index, item ) {
                            if (direction.cityFrom.id == item.city.id) {
                                result.push(item);
                            }
                        });
                    }

                    return result;
                },
                stocksTo: function () {
                    var direction = this.getDirection();

                    var result = [];

                    if (direction) {
                        $.each(this.stocks, function( index, item ) {
                            if (direction.cityTo.id == item.city.id) {
                                result.push(item);
                            }
                        });
                    }

                    return result;
                },
                getCityFromList: function () {
                    var $this = this;
                    var result = [];

                    var resultIds = [];

                    $.each(this.directions, function( index, item ) {
                        if ($this.cityTo != item.cityFrom.id && (!parseInt($this.cityTo) || $this.cityTo == item.cityTo.id)) {
                            if (resultIds.indexOf(item.cityFrom.id) == -1) {
                                result.push(item.cityFrom);
                                resultIds.push(item.cityFrom.id);
                            }
                        }
                    })
                    
                    result.sort(comparePosition)

                    return result;
                },
                getCityToList: function () {
                    var $this = this;
                    var result = [];

                    var resultIds = [];

                    $.each(this.directions, function( index, item ) {
                        if ($this.cityFrom != item.cityTo.id && (!parseInt($this.cityFrom) || $this.cityFrom == item.cityFrom.id)) {
                            if (resultIds.indexOf(item.cityTo.id) == -1) {
                                result.push(item.cityTo);
                                resultIds.push(item.cityTo.id);
                            }
                        }
                    })
                    
                    result.sort(comparePosition)

                    return result;
                },
                packingRequired: function () {
                    var packageType = this.getPackageType();

                    if (packageType && packageType.packingRequired) {
                        return true;
                    }
                    return false;
                },
                packingsRecommended: function () {
                    var packageType = this.getPackageType();

                    if (packageType && packageType.packingsRecommended) {
                        return packageType.packingsRecommended;
                    }
                    return [];
                },
                validationErrors: function () {
                    return this.$validator.fields.items.filter(function (item, pos, array) {
                        return array.map(function (item) {
                                return item.field;
                            }).indexOf(item.field) === pos;
                    });
                    return [];
                },
                totalWeight: function () {
                    var $this = this;
                    var result = 0;

                    $.each(this.packages, function( index, item ) {
                        var calculatedVolumeWeight = $this.calculateWeightVolume(item.calculateWeight);
                        var calculatedWeight = $this.calculateWeight(item.width, item.height, item.depth);
                        var weight = item.weight;

                        if (calculatedWeight < calculatedVolumeWeight) {
                            calculatedWeight = calculatedVolumeWeight;
                        }

                        var resultWeight = 0;

                        if (weight > calculatedWeight) {
                            resultWeight = weight;
                        } else {
                            resultWeight = calculatedWeight;
                        }

                        if ($this.byCargo) {
                            result += parseFloat(resultWeight);
                        } else {
                            if (parseFloat(resultWeight) > 0 && parseFloat(item.count) > 0) {
                                result += parseFloat(resultWeight) * parseFloat(item.count);
                            }
                        }

                    })

                    console.log(result);

                    return result.toFixed(2);
                },
                getPackageCount: function () {
                    var result = 0;
                    $.each(this.packages, function( index, item ) {
                        result = result + parseInt(item.count);
                    })
                    return result;
                },
                placeholderVolume: function () {
                    var result = '≤ 1.728';

                    if (this.byCargo) {
                        result = '';
                    }

                    return result;
                }

            },
            watch: {
                'promocode': function (val) {
                    this.usePromocode = false;
                },
                'cityTo': function (val) {
                    this.changeDirection();
                    this.htmlUp();
                },
                'cityFrom': function (val) {
                    this.changeDirection();
                    this.htmlUp();
                },
            },

            mounted : function () {

                $this = this;

                $(function(){

                    $('#customer_calc').css('visibility', 'visible');

                    $('body').on('change', '.js-send-type', function(){
                        var selected = $(this).find("option:selected").text();

                        var parent = $(this).parents('.packages');
                        var block = $('#'+parent.attr('id'));

                        var selected = block.find('.js-send-type').find("option:selected").text();
                        if (selected == "Другое") {
                            setTimeout(function() {
                                block.find('.bootstrap-select.js-send-type').parent().addClass('another-active');
                                block.find('.js-another-field').addClass('active');

                                setTimeout(function() {
                                    block.find('.js-another-field').focus();
                                }, 500);
                            }, 500);
                        } else {
                            block.find('.bootstrap-select.js-send-type').parent().removeClass('another-active');
                            block.find('.js-another-field').removeClass('active');
                        }
                    });
                })


                this.getData();
                this.htmlUp();

                $('#calculator-form').show();
            },
        })
})
