    var register = new Vue({
        delimiters: ['${', '}'],
        el: '#register',
        data: {
            step: 1,
            organizationInnTypingTimer: null,
            autocompleteOrganizationAddress: [],
            autocompleteOrganizationAddressSelected: '',
            autocompleteOrganizationAddressTypingTimer: null,
            autocompleteOrganizationAddressFromSelect: false,
            countstep: 4,
            resultUser: {'username': false, 'email': false},
            user: Object.assign({}, UserObject),
            organization: Object.assign({}, Organization),
            code: '',
            checkCode: null,
            confirm: true,
            checkConfirmedPass: null,
            reenteredPasswordChange: false,
            organizationTypes: [],
            subscribe: true,
            mask: mask(),
            filter: filter(),
            ar: null,
        },
        methods: {
            showStep: function (step, prevStep, $event) {

                var $this = this;
                $this.errors.clear();

                this.$validator.validateAll().then(function(result) {

                    $this.reenteredPasswordChangeEvent();

                    $this.checkCode = null;

                    if(!result && prevStep == undefined) {
                        return;
                    }

                    if (step == 2 && !prevStep) {
                        counterEvent('reg_1', 'form_submit');
                        $this.beforeRegister();

                        if ($this.user.password != $this.user.reenteredPassword) {
                            $this.checkConfirmedPass = true;
                            return false;
                        } else {
                            $this.checkConfirmedPass = false;
                        }

                        var resultUser = $this.checkLogin();
                        if (resultUser.username || resultUser.email) {
                            return false;
                        }
                    }

                    if (step == 2 && !prevStep) {
                        $this.checkCode = null;
                        $this.code = '';
                    }

                    if (step == 2 && !prevStep) {
                        $this.sendSms();
                    }

                    if (step == 3 && !prevStep) {
                        counterEvent('reg_2', 'form_submit');
                        /* Простите.. */
                        $this.showOffer();

                        if (!$this.checkSmsCode()) {
                            return false;
                        }
                    }

                    if (step == 5 && !prevStep) {

                        if ($this.user.group == 'carrier') {
                            $this.verification($event);
                        } else {
                            $this.registration($event);
                        }
                        return false;
                    }

                    $this.step = step;

                    $this.htmlUp();

                });

                $this.errors.clear();
                $this.htmlUp();

                return false;
            },

            getData: function () {
                var jqxhr = $.ajax({
                    url: "/api/security/config",
                    cache: true,
                    async: false
                });

                data = jqxhr.responseJSON.data;

                this.organizationTypes = data.params.organizationTypes;
            },

            getAbandonedRegisterData: function (id) {
                if (id) {
                    var jqxhr = $.ajax({
                        url: "/api/security/abandonedRegisterData/"+id,
                        cache: false,
                        async: false
                    });

                    json = jqxhr.responseJSON;

                    if (json.status != undefined) {
                        this.user.username = json.data.username;
                        this.user.password = json.data.password;
                        this.user.reenteredPassword = json.data.password;
                        this.user.email = json.data.email;
                        this.user.subscribe = json.data.subscribe;
                    }
                }
            },

            checkLogin: function () {
                var username = this.user.username;
                var email = this.user.email;
                var jqxhr = $.ajax({
                    url: "/api/security/checkusername",
                    cache: false,
                    async: false,
                    data: {username: username, email: email}
                });

                data = jqxhr.responseJSON.data;

                this.resultUser = data;
                return data;
            },

            showOffer: function() {
                if (this.user.group == 'carrier') {
                    $('#offer_carrier').show();
                    $('#offer_customer').hide();
                } else {
                    $('#offer_customer').show();
                    $('#offer_carrier').hide();
                }
            },

            reenteredPasswordChangeEvent: function() {
                this.reenteredPasswordChange = true;
            },
            selectRefresh: function() {
                setTimeout(function(){
                    $('.selectpicker').selectpicker('refresh');
                }, 300);
            },
            htmlUp: function () {
                var user = this.user;

                initDatepickerDefault($('.flatpickr-default'));

                setTimeout(function(){
                    passwordView();

                    $('.selectpicker').selectpicker();

                    $('[data-toggle="tooltip"]').tooltip({
                        template: '<div class="tooltip" role="tooltip"><div class="close-tooltip"><img src="/img/close.png" alt="" /></div><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })

                    var tooltip = $('.js-subscribe-tooltip-link');
                    if (user.subscribe) {
                        tooltip.tooltip('hide')
                            .attr('data-original-title', 'Отказаться можно в личном кабинете')
                            .tooltip('fixTitle');
                    } else {
                        tooltip
                            .attr('data-original-title', 'Не сможем сообщать скидки')
                            .tooltip('fixTitle')
                            .tooltip('show');
                    }

                    passwordView();

                    $('.flatpickr-default input').attr('readonly', false);
                }, 500);
            },

            sendSms: function () {
                var phone = this.user.username;
                var jqxhr = $.ajax({
                    url: "/api/security/sendsms",
                    cache: true,
                    async: false,
                    data: {phone: phone}
                });

                data = jqxhr.responseJSON;

                setTimeout(function(){
                    $('#resendSms').addClass('disabled');
                }, 200)

                setTimeout(function(){
                    $('#resendSms').removeClass('disabled');
                }, 20000)

                return data.status;
            },

            checkSmsCode: function () {
                this.checkCode = null;

                var code = this.code;
                var jqxhr = $.ajax({
                    url: "/api/security/checksms",
                    cache: true,
                    async: false,
                    data: {code: code}
                });

                data = jqxhr.responseJSON;

                this.checkCode = data.status;

                return data.status;
            },

            resendSms: function ($event) {
                var button = $($event.target);
                if (button.hasClass('disabled')) {
                    return false;
                }

                this.sendSms();
                button.addClass('disabled');

                setTimeout(function(){
                    button.removeClass('disabled');
                }, 20000)

                return false;
            },

            beforeRegister: function () {
                var user = this.user;

                var formData = new FormData();
                formData.append('user[username]', user.username);
                formData.append('user[email]', user.email);
                formData.append('user[password]', user.password);
                formData.append('user[subscribe]', user.subscribe);

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/security/beforeRegister",
                    cache: true,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                return false;
            },

            register: function () {
                var user = this.user;

                var formData = new FormData();
                formData.append('user[username]', user.username);
                formData.append('user[email]', user.email);
                formData.append('user[password]', user.password);
                formData.append('user[group]', [user.group]);
                formData.append('user[subscribe]', [user.subscribe]);

                var jqxhr = $.ajax({
                    method: 'POST',
                    url: "/api/security/register",
                    cache: true,
                    async: false,
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                });

                data = jqxhr.responseJSON;

                if (data.status) {
                    user.id = data.id;
                }

                return data.status;
            },

            verification: function ($event) {

                var button = $($event.target);
                if (button.hasClass('disabled')) {
                    return false;
                }

                button.addClass('disabled');

                var $this = this;
                var user = $this.user;
                var organization = $this.organization;

                $this.$validator.validateAll().then(function(result) {
                    if(!result) {
                        button.removeClass('disabled');
                        return;
                    }

                    if (user.id <= 0) {
                        $this.register();
                    }

                    var formData = new FormData();
                    formData.append('action', 'register');
                    formData.append('url', window.location.pathname);
                    formData.append('user[id]', user.id);
                    formData.append('user[group]', [user.group]);
                    formData.append('user[subscribe]', user.subscribe);
                    formData.append('organization[type]', organization.type);
                    formData.append('organization[companyName]', organization.companyName);
                    formData.append('organization[uridical]', organization.uridical);
                    formData.append('organization[nds]', organization.nds);
                    formData.append('organization[inn]', organization.inn);
                    formData.append('organization[kpp]', organization.kpp);
                    formData.append('organization[ceo]', organization.ceo);
                    formData.append('organization[address]', organization.address);
                    formData.append('organization[contactName]', organization.contactName);
                    formData.append('organization[contactPhone]', organization.contactPhone);
                    formData.append('organization[rs]', organization.rs);
                    formData.append('organization[bik]', organization.bik);

                    $.each(organization.drivers, function( index, driver ) {
                        formData.append('organization[drivers]['+index+'][name]', driver.name);
                        formData.append('organization[drivers]['+index+'][inn]', driver.inn);
                        formData.append('organization[drivers]['+index+'][birthdate]', driver.birthdate);
                        formData.append('organization[drivers]['+index+'][phone]', driver.phone);

                        $.each(driver.documents, function( indexDocument, document) {
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][type]', document.type);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][series]', document.series);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][number]', document.number);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][date]', document.date);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][place]', document.place);
                        })
                    });

                    $.ajax({
                        method: 'POST',
                        url: "/api/security/register_organization",
                        cache: false,
                        data: formData,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                    }).done(function(data) {
                        if (data.status) {
                            counterEvent('REG_PEREVOZCHIK', 'form_submit');

                            if (data.verifiedCode == 'verified') {
                                openPopupUrl('/static/popup/verification_success');
                            } else if (data.verifiedCode == 'delay') {
                                $this.authorize();
                                window.location.href = '/auction#verify_delay';
                            } else if (data.verifiedCode == 'not_verified') {
                                $this.authorize();
                                window.location.href = '/personal/profile#verify_fail';
                            } else {
                                openPopupUrl('/static/popup/error');
                            }
                        }

                        button.removeClass('disabled');
                    });

                    return false;
                });

                return false;
            },

            registration: function ($event) {

                var button = $($event.target);
                if (button.hasClass('disabled')) {
                    return false;
                }

                button.addClass('disabled');

                var $this = this;
                var user = $this.user;
                var organization = $this.organization;

                this.$validator.validateAll().then(function(result) {
                    if(!result) {
                        button.removeClass('disabled');
                        return;
                    }

                    if (user.id <= 0) {
                        $this.register();
                    }

                    if ($this.organization.uridical == 1) {
                        var formData = new FormData();
                        formData.append('action', 'register');
                        formData.append('url', window.location.pathname);
                        formData.append('user[id]', user.id);
                        formData.append('user[group]', [user.group]);
                        formData.append('user[subscribe]', user.subscribe);
                        formData.append('organization[type]', organization.type);
                        formData.append('organization[companyName]', organization.companyName);
                        formData.append('organization[uridical]', organization.uridical);
                        formData.append('organization[nds]', organization.nds);
                        formData.append('organization[inn]', organization.inn);
                        formData.append('organization[kpp]', organization.kpp);
                        formData.append('organization[ceo]', organization.ceo);
                        formData.append('organization[address]', organization.address);
                        formData.append('organization[contactName]', organization.contactName);
                        formData.append('organization[contactPhone]', organization.contactPhone);
                        formData.append('organization[rs]', organization.rs);
                        formData.append('organization[bik]', organization.bik);

                        $.ajax({
                            method: 'POST',
                            url: "/api/security/register_organization",
                            data: formData,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                        }).done(function(data) {
                            if (data.status) {
                                counterEvent('REG_OTPRAVITEL', 'form_submit');
                                $this.authorize();
                            }

                            button.removeClass('disabled');
                        });

                    } else {
                        var formData = new FormData();
                        formData.append('url', window.location.pathname);
                        formData.append('user[id]', user.id);
                        formData.append('user[subscribe]', user.subscribe);
                        formData.append('user[group]', [user.group]);
                        formData.append('user[firstname]', user.firstname);
                        formData.append('user[number]', user.number);
                        formData.append('user[series]', user.series);

                        $.ajax({
                            method: 'POST',
                            url: "/api/security/register_person",
                            data: formData,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                        }).done(function(data) {
                            if (data.status) {
                                counterEvent('REG_OTPRAVITEL', 'form_submit');
                                $this.authorize();
                            }

                            button.removeClass('disabled');
                        });
                    }

                    return false;
                });

                return false;
            },

            authorize: function () {
                if(this.user.id) {
                    window.location.href = '/';
                }
                if(!this.user.username||!this.user.password) {
                    return false;
                }

                let username = formatUserName(this.user.username);
                let password = this.user.password;

                $.ajax({
                    url: '/auth',
                    type: 'POST',
                    contentType: "application/json",
                    dataType: 'json',
                    data: JSON.stringify({username: username, password: password}),
                    async: false,
                    success: function (data, status) {
                        window.location.href = '/';
                    },
                });
                window.location.href = '/';
            },

            createDriver: function () {
                this.organization.drivers.push(getEmptyDriver());

                this.htmlUp();
            },

            removeDriver: function (index) {
                if (this.organization.drivers.length <= 1) {
                    return;
                }
                this.organization.drivers.splice(index, 1);
            },
            suggestInn: function () {
                if(!this.organization.inn) {
                    return;
                }
                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/suggest/inn',
                    method: 'GET',
                    data: {'number':this.organization.inn},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        if(data.data.name && !$this.organization.companyName) {
                            $this.organization.companyName = data.data.name;
                        }
                        if(data.data.kpp && !$this.organization.kpp) {
                            $this.organization.kpp = data.data.kpp;
                        }
                        if(data.data.ceo && !$this.organization.ceo) {
                            $this.organization.ceo = data.data.ceo;
                        }
                        if(data.data.address && !$this.organization.address) {
                            $this.organization.address = data.data.address;
                        }

                        if(data.data.type == "LEGAL") {
                            $this.organization.type = "ООО";
                            $this.selectRefresh();
                        }

                        if(data.data.type == "INDIVIDUAL") {
                            $this.organization.type = "ИП";
                            $this.selectRefresh();
                        }

                        if(data.data.type &&  data.data.type != "INDIVIDUAL" && data.data.type != "LEGAL") {
                            $this.organization.type = "Другое";
                            $this.selectRefresh();
                        }
                    }
                });
            },
            cleanOrganizationAddress: function () {
                if(!this.organization.address) {
                    return;
                }
                if($('.custom-autocomplete-select .dropdown-menu').is(':visible')) {
                    return;
                }
                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/clean/address',
                    method: 'GET',
                    data: {'address':this.organization.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.organization.address = data.data;
                        $this.autocompleteOrganizationAddressFromSelect = true;
                        $('.custom-autocomplete-select .selectpicker').selectpicker('hide');
                    }
                });
            },
            suggestOrganizationAddress: function () {
                if(this.organization.address) {
                    var $this = this;
                    $.ajax({
                        url: '/api/data-normalize/suggest/address',
                        method: 'GET',
                        data: {'address':this.organization.address},
                    })
                    .done(function(data) {
                        if (data.status && data.data) {
                            $this.autocompleteOrganizationAddress = data.data;
                            $this.htmlUp();
                            $this.selectRefresh();
                            $('.custom-autocomplete-select .selectpicker').selectpicker('show');
                            $('.custom-autocomplete-select .selectpicker').selectpicker('toggle');
                        }
                    });
                }
            },
        },
        computed: {
            'user.group': function (val) {
                if (val == 'carrier') {
                    this.data.countstep = 4;
                }
                if (val == 'sender') {
                    this.data.countstep = 3;
                }
            }
        },
        watch: {
           'organization.inn': function() {
                var $this = this;
                clearTimeout(this.organizationInnTypingTimer);
                this.organizationInnTypingTimer = setTimeout(function() {
                    $this.suggestInn();
                }, 600);
            },
            'autocompleteOrganizationAddressSelected': function(val) {
                if(this.autocompleteOrganizationAddressSelected) {
                    this.organization.address = this.autocompleteOrganizationAddressSelected;
                    $('.custom-autocomplete-select .selectpicker').selectpicker('hide');
                }
            },
            'organization.address': function(val) {
                var $this = this;
                clearTimeout(this.autocompleteOrganizationAddressTypingTimer);
                if(this.organization.address != this.autocompleteOrganizationAddressSelected && 
                    !this.autocompleteOrganizationAddressFromSelect) 
                {
                    this.autocompleteOrganizationAddressTypingTimer = setTimeout(function() {
                        $this.suggestOrganizationAddress();
                    }, 500);
                }
                
                if(this.autocompleteOrganizationAddressFromSelect) {
                    this.autocompleteOrganizationAddressFromSelect = false;
                }
            },
            'organization.uridical': function () {
                this.htmlUp();
            },
            'organization.type': function (val) {
                if (val == 'ИП') {
                    this.organization.kpp = '';
                }
            },
            'user.group': function (val) {
                this.showOffer();

                if (val == 'carrier') {
                    this.organization.uridical = 1;
                } else {
                    this.organization.uridical = 0;
                }
            },
            'user.subscribe': function (val) {
                this.htmlUp();
            },

            'ar': function (val) {
                this.getAbandonedRegisterData(this.ar);
            },
            
            'step': function (val) {
				var $this = this;
                setTimeout(function(){
					$this.htmlUp();
				}, 500);
            },
            
            'organization.companyName': function (val) {
                if (this.organization.type == 'ИП') {
                    this.organization.ceo = val;
                }
            },
        },

        mounted : function () {
            var $this = this;

            $('#register').css('visibility', 'visible');

            $('#wizard-register-sender').on('submit', function () {
                $this.showStep($this.step + 1);

                return false;
            })

            this.getData();

            this.htmlUp();
        },
    })



