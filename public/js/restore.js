$(function(){
    $('#loginPhone').on('change', function(){
        var phone = $(this);
        var username = $('#username');

        username.val(phone.val().replace('/[^0-9]/'));
        username.val(phone.val().replace(/\s+/g, ''));
        username.val(phone.val().replace(/[^a-zA-Z0-9+]/g, ""));

        if (username.val().indexOf('+7') == 0) {
            username.val(username.val().substr(2));
        }
        if (username.val().indexOf('8') == 0) {
            username.val(username.val().substr(1));
        }

    })
})
