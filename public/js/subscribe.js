$(function(){
	setTimeout(
		function(){
            var formData = new FormData();
            formData.append('page', location.pathname);

			 $.ajax({
                 method: 'POST',
                 url: "/api/subscribe/check",
                 cache: false,
                 async: false,
                 data: formData,
                 dataType: "json",
                 processData: false,
                 contentType: false,
			}).done(function(data) {
				if (data.data.show) {
					openPopupUrl('/static/popup/subscribe', 'main-popup register-popup', function(){
					    initSubscribe();
					});
				}
			});
		}, 5000
	);
})

function initSubscribe()
{
    var app = new Vue({
        el: '#subscribe',
        data: {
            email: '',
            terms: true,
            subscribe: true,
            error: false,
        },
        methods: {
            subscribeEmail: function ($event) {
                $this = this;

                this.$validator.validateAll().then(function(result) {

                    if(!result) {
                        return;
                    }

                    $.ajax({
                        method: 'POST',
                        url: "/api/subscribe/subscribe",
                        async: true,
                        data: { email: $this.email, subscribe: $this.subscribe, url: location.href},
                    }).done(function(data) {
                        if (data.error != undefined) {
                            $this.error = true;
                        } else if (data.status) {
                            counterEvent('Mail', 'form_submit');
                            closePopup();
                        }

                    });

                    return false;
                });

                return false;
            },
        },
        watch: {
            email: function (val) {
                this.error = false;
            },
        }

    })
}
