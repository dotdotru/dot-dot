const config = {
    errorBagName: 'errors', // change if property conflicts
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'ru',
    dictionary: null,
    strict: true,
    classes: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'change',
    inject: true,
    validity: false,
    aria: true,
    debug: true
};

VeeValidate.Validator.extend('password', {
	validate: function(value) {
		return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value);
	},
	getMessage: function(field, params, data) {
		return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
	}
});

VeeValidate.Validator.extend('nozero', {
    validate: function(value) {
        return value > 0;
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});


VeeValidate.Validator.extend('cyrillic', {
    validate: function(value) {
        return /^([А-Яа-я-— ]+)$/ig.test(value);
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});

VeeValidate.Validator.extend('cyrillicNumeric', {
    validate: function(value) {
        return /^([А-Яа-я-— 0-9]+)$/ig.test(value);
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});

VeeValidate.Validator.extend('phone', {
    validate: function(value) {
        value = value.replace(/\D+/g, '');
        return value.length == 11 || value[0] != '+';
    },
    getMessage: function(field, params, data) {
        return 'Insert a strong password, it should contain Uppercase letter, lowercase letter, number and special character';
    }
});

function mask() {
    return {
        phone: '+7 (###) ###-##-##',
        code: '#######',
        date: '##.##.####',
        five: '##### ##### ##### #####'
    }
}

function filter() {
    return {
        cyrillic: '[А-Яа-я-— \\b]+',
        cyrillicNumeric: '[А-Яа-я-— 0-9\\b]+',
        numeric: '[0-9\\b]+',
        address: '[А-Яа-я-— 0-9,\\b]+',
        float: '[0-9,.\\b]+',
        int: '[0-9]+\\b',
    }
}

function convertDate(date) {
    if (date) {
        var dateTime = new Date();
        if (date.date) {
            dateTime = moment(date.date, 'YYYY-MM-DD HH:mm');
        } else {
            dateTime = moment(date.toString());
        }
        return dateTime;
    }
}

function formatDateTime(date) {
    var dateTime = convertDate(date);

    if (dateTime) {
       return moment(dateTime, 'YYYY-MM-DD HH:mm').format('DD.MM.YYYY HH:mm')
    }
    return '';
}

function formatDate(date) {
    var dateTime = convertDate(date);

    if (dateTime) {
        return moment(dateTime, 'YYYY-MM-DD').format('DD.MM.YYYY')
    }
    return '';
}

function formatTime(date) {
    var dateTime = convertDate(date);

    if (dateTime) {
        return moment(dateTime, 'YYYY-MM-DD HH:mm').format('HH:mm')
    }
    return '';
}

function formatPhone(phone) {	
	var phone = phone.replace(/[A-z\(\)-+ ]/g, '')
	
	if (phone[0] == '7') {
		phone = phone.substr(1, phone.length)
	}

    phone = phone.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "($1) $2-$3-$4");

	if (phone[0] == '7') {
		phone = phone.substr(1, phone.length)
	}

    return '+7 '+phone;
}

function comparePosition(a, b) {
  if (a.position < b.position) {
    return -1;
  }
  if (a.position > b.position) {
    return 1;
  }

  return 0;
}

function formatUserName(username) {
  username = username.replace('/[^0-9]/');
  username = username.replace(/\s+/g, '');
  username = username.replace(/[^a-zA-Z0-9+]/g, "");
  if (username.indexOf('+7') == 0) {
    username = username.substr(2);
  }
  if (username.indexOf('8') == 0) {
    username = username.substr(1);
  }
  return username;
}

Vue.use(VeeValidate, config);

Vue.config.devtools = true;

Vue.directive('dadata', {
    params: ['a', 'dadatatype'],
    twoWay: true,

    paramWatchers: {
        'dadatatype': function (val, oldVal) {

        }
    },

    bind: function(el, binding) {
        this.el = $(el);
        this.dadatatype = this.el.attr('dadatatype')
    },

    update: function (newValue, oldValue) {
        var url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/';

        if (this.dadatatype)
            url = url + this.dadatatype;
        else
            return;

        var $this = this;
        var dadatatype = this.dadatatype;

        /*console.log(this.el);

        this.el.autocomplete({
            source: (request, response) => {
                var query = request.term;
                var arAnswer = [];

                console.log('sdfsdf');

                $.ajax ({
                    data: JSON.stringify({query: query}),
                    headers: {
                        "Content-Type":"application/json",
                        "Accept":"application/json",
                        "Authorization": "Token c0405a37b0a4d76fc84d794ee0b1b93d9d7f2122",
                    },
                    type: 'POST',
                    url: url,
                    async: false,
                    success: function (data) {
                        if (data.suggestions)
                            arAnswer = data.suggestions;
                    },
                });

                response (arAnswer);
            },
            select: function( event, ui ) {
                if (dadatatype == 'fio' && sexfield) {
                    var tmp = ui.item.data.gender.substr(0,1).toLowerCase();
                    if (sexfiledkey != undefined) {
                        sexfield = sexfield+'.'+sexfiledkey;
                    }
                    $this.vm.$dispatch(sexfield, tmp);
                    $this.vm.$set(sexfield, tmp);
                }
            },
            change: function (event,ui) {
                var value = $(event.currentTarget).val();
                var result;
                var sex;
                if (dadatatype == 'fio' && sexfield) {

                    if (sexfiledkey != undefined)
                        sexfield = sexfield+'.'+sexfiledkey;

                    result=dadataRequest (value,'fio');
                    if (result.length===1) {
                        sex = result[0].data.gender.substr(0,1).toLowerCase();
                        $this.vm.$dispatch(sexfield, sex);
                        $this.vm.$set(sexfield, tmp);
                    }
                }

            },
            autoFocus: true,
        });*/
    }
});

Vue.directive("filter", {
    bind: function(el, binding) {
        this.inputHandler = function(e) {
            var ch = String.fromCharCode(e.which);
            var re = new RegExp(binding.value);

            if([37, 39, 8, 0].indexOf(e.which) == -1) {
                if (!ch.match(re)) {
                    e.preventDefault();
                }
            }
        };
        el.addEventListener("keypress", this.inputHandler);
    },
    unbind: function(el) {
        el.removeEventListener("keypress", this.inputHandler);
    },
    inputHandler: null
});

Vue.directive("min", {
    bind: function(el, binding) {
        this.changeHandler = function(e) {
            var currentValue = $(e.target).val();

            if (currentValue < binding.value) {
                $(e.target).val(binding.value)
            }
        };
        el.addEventListener("change", this.changeHandler);
    },
    unbind: function(el) {
        el.removeEventListener("change", this.changeHandler);
    },
    changeHandler: null
});

Vue.directive("max", {
    bind: function(el, binding) {
        this.changeHandler = function(e) {
            var currentValue = $(e.target).val();

            if (currentValue > binding.value) {
                $(e.target).val(binding.value)
            }
        };
        el.addEventListener("change", this.changeHandler);
    },
    unbind: function(el) {
        el.removeEventListener("change", this.changeHandler);
    },
    changeHandler: null
});

Vue.directive('select2', {
    bind: function(el, binding) {
        $(el).on('select2:select', function(e) {
            el.dispatchEvent(new Event('change', { target: e.target }));
        });

        $(el).on('select2:unselect', function(e) {
            el.dispatchEvent(new Event('change', { target: e.target }));
        })
    },
});
/*Vue.directive('select2', {
    twoWay: true,
    bind: function(el, binding) {
        $(el).select2().on("select2:select", function(e) {
            this.set($(this.el).val());
        }.bind(this));
    },
    update: function (el, binding, newVnode, oldVnode) {
        var select = $(el);
        select.val(binding.value).trigger('change');
    }
});
*/

var Package = {
    type: '',
    anotherType: '',
    count: null,
    depth: null,
    width: null,
    height: null,
    weight: null,
    packing: null,
    calculateWeight: null,
    hidden: 0,
    useSize: null,
    enterVolume: false,
};

var WmsPackage = {
    id: '',
    packageType: null,
    anotherType: '',
    count: null,
    depth: null,
    width: null,
    height: null,
    weight: null,
    packing: null,
    packingPrice: null,
    calculateWeight: null,
    hidden: 0,
    palletId: null,
    damaged: false,
};

var Shipping = {
    id: null,
    externalId: null,
    active: false,
    date: '',
    stock: null,
    address: '',
    countLoader: 0,
    hydroBoard: false,
    price: 0,
};
