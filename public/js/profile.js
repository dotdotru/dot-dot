var profile = new Vue({
    delimiters: ['${', '}'],
    el: '#profile',
    data: {
        edit: false,
        editDrivers: false,
        organizationInnTypingTimer: null,
        autocompleteOrganizationAddress: [],
        autocompleteOrganizationAddressSelected: '',
        autocompleteOrganizationAddressTypingTimer: null,
        autocompleteOrganizationAddressFromSelect: false,
        driverActive: 1,
        user: Object.assign({}, UserObject),
        organization: Object.assign({}, Organization),
        hiddenIndex: 0,
        organizationTypes: [],
        mask: mask(),
        filter: filter(),
    },
    methods: {
        nextDriver: function () {
            var $this = this;
            if (this.organization.drivers.length >= this.driverActive + 1) {
                this.driverActive = this.driverActive + 1;

                setTimeout(function(){
                    $this.editFormDrivers($this.editDrivers);
                }, 200);
            }
        },

        prevDriver: function () {
            var $this = this;
            if (this.driverActive - 1 > 0) {
                this.driverActive = this.driverActive - 1;

                setTimeout(function(){
                    $this.editFormDrivers($this.editDrivers);
                }, 200);
            }
        },

        createDriver: function (index) {
            var $this = this;

            $this.organization.drivers.splice(index + 1, 0, Object.assign({}, getEmptyDriver()));

            $.each($this.organization.drivers, function( index, driver ) {
                if (!driver.hidden) {
                    driver.hidden = $this.hiddenIndex + 1;
                    $this.hiddenIndex = $this.hiddenIndex + 1;
                }
            })

            $this.driverActive = index + 2;

            setTimeout(function(){
                initDatepickerDefault($('.flatpickr-default'));
            }, 200);


            $this.htmlUp();
        },

        removeDriver: function (index) {
            var $this = this;

            if ($this.organization.drivers.length <= 1) {
                return;
            }
            $this.organization.drivers.splice(index, 1);

            $this.driverActive = index;

            setTimeout(function(){
                initDatepickerDefault($('.flatpickr-default'));
            }, 200);

            $this.htmlUp();
        },

        save: function ($event, updateOrganization, updateDrivers) {

            if (!updateOrganization) {
                updateOrganization = false;
            }
            if (!updateDrivers) {
                updateDrivers = false;
            }

            var $this = this;
            var user = this.user;
            var organization = this.organization;

            var button = $($event.target);

            this.$validator.validateAll().then(function(result) {

                if(!result) {
                    button.removeClass('disabled');
                    return;
                }

                if (button.hasClass('disabled')) {
                    return false;
                }

                button.addClass('disabled');

                if (user.carrier || user.customer && organization.id) {

                    let action = 'full';
                    if(updateDrivers && !updateOrganization) {
                        action = 'driver';
                    }
                    else if (!updateDrivers && updateOrganization) {
                        action = 'organization';
                    }

                    var formData = new FormData();
                    formData.append('action', action);
                    formData.append('url', window.location.pathname);
                    formData.append('user[id]', user.id);
                    formData.append('user[subscribe]', user.subscribe);
                    formData.append('user[email]', user.email);
                    formData.append('edit[organization]', updateOrganization);
                    formData.append('edit[drivers]', updateDrivers);
                    formData.append('organization[id]', organization.id);
                    formData.append('organization[type]', organization.type);
                    formData.append('organization[companyName]', organization.companyName);
                    formData.append('organization[uridical]', organization.uridical);
                    formData.append('organization[nds]', organization.nds);
                    formData.append('organization[inn]', organization.inn);
                    formData.append('organization[kpp]', organization.kpp);
                    formData.append('organization[ceo]', organization.ceo);
                    formData.append('organization[address]', organization.address);
                    formData.append('organization[contactName]', organization.contactName);
                    formData.append('organization[contactPhone]', organization.contactPhone);
                    formData.append('organization[rs]', organization.rs);
                    formData.append('organization[bik]', organization.bik);

                    $.each(organization.drivers, function( index, driver ) {
                        formData.append('organization[drivers]['+index+'][id]', driver.id);
                        formData.append('organization[drivers]['+index+'][name]', driver.name);
                        formData.append('organization[drivers]['+index+'][inn]', driver.inn);
                        formData.append('organization[drivers]['+index+'][birthdate]', driver.birthdate);
                        formData.append('organization[drivers]['+index+'][phone]', driver.phone);

                        $.each(driver.documents, function( indexDocument, document) {
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][id]', document.id);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][type]', document.type);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][series]', document.series);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][number]', document.number);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][date]', document.date);
                            formData.append('organization[drivers]['+index+'][documents]['+indexDocument+'][place]', document.place);
                        })
                    });

                    $.ajax({
                        method: 'POST',
                        url: "/api/security/register_organization",
                        data: formData,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                    }).done(function(data) {
                        button.removeClass('disabled');

                        setTimeout(function() {
                            if (updateDrivers) {
                                $this.editDrivers = false;

                                if (data.verificationEvent) {
                                    openPopupUrl('/static/popup/verification_status?organizationId='+organization.id);
                                }

                            }
                            if (updateOrganization) {
                                $this.edit = false;
                                if (data.verifiedCode == 'verified') {
                                    openPopupUrl('/static/popup/verification_success');
                                } else if (data.verifiedCode == 'delay') {
                                    openPopupUrl('/static/popup/verification_delay');
                                } else if (data.verifiedCode == 'not_verified') {
                                    openPopupUrl('/static/popup/verification_error');
                                } else {
                                    openPopupUrl('/static/popup/error');
                                }
                            }
                            $this.loadData();
                        }, 200);

                    });
                } else {
                    var formData = new FormData();
                    formData.append('url', window.location.pathname);
                    formData.append('user[id]', user.id);
                    formData.append('user[subscribe]', user.subscribe);
                    formData.append('user[firstname]', user.firstname);
                    formData.append('user[number]', user.number);
                    formData.append('user[series]', user.series);
                    formData.append('user[username]', user.username);
                    formData.append('user[email]', user.email);

                    $.ajax({
                        method: 'POST',
                        url: "/api/security/register_update",
                        data: formData,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                    }).done(function(data) {
                        button.removeClass('disabled');

                        setTimeout(function(){
                            $this.edit = false;
                            $this.editDrivers = false;

                            $this.loadData();
                        }, 200);

                    });
                }
                
                return false;
            });

            return false;
        },

        loadData: function (callback) {
            var $this = this;
            var user = this.user;
            var organization = this.organization;

            $.ajax({
                method: "POST",
                url: "/api/security/profile",
                data: {
                    userId: $this.user.id,
                    organizationId: $this.organization.id
                },
                dataType: "json",
            })
            .done(function( data ) {

                if (data.params.organizationTypes) {
                    $this.organizationTypes = data.params.organizationTypes;
                }

                if (data.user) {
                    $this.user.id = data.user.id;
                    $this.user.firstname = data.user.firstname;
                    $this.user.series = data.user.series;
                    $this.user.number = data.user.number;
                    $this.user.email = data.user.email;
                    $this.user.username = data.user.username;
                    $this.user.carrier = data.user.carrier;
                    $this.user.customer = data.user.customer;
                    $this.user.subscribe = data.user.subscribe;
                }

                if (data.organization) {
                    $this.organization.id = data.organization.id;
                    $this.organization.uridical = data.organization.uridical;
                    $this.organization.type = data.organization.type;
                    $this.organization.companyName = data.organization.companyName;
                    $this.organization.nds = data.organization.nds;
                    $this.organization.inn = data.organization.inn;
                    $this.organization.kpp = data.organization.kpp;
                    $this.organization.ceo = data.organization.ceo;
                    $this.organization.address = data.organization.address;
                    $this.organization.contactName = data.organization.contactName;
                    $this.organization.contactPhone = data.organization.contactPhone;
                    $this.organization.rs = data.organization.rs;
                    $this.organization.bik = data.organization.bik;
                    $this.organization.verifiedCode = data.organization.verifiedCode;
                    $this.organization.verifiedDate = data.organization.verifiedDate;
                    $this.organization.drivers = data.organization.drivers;

                    $.each($this.organization.drivers, function( indexDriver, driver ) {
                        $.each(driver.documents, function( indexDoc, document ) {
                            if (document.type == 'passport') {
                                $this.organization.drivers[indexDriver].documents[indexDoc].type = 'passport';
                                $this.organization.drivers[indexDriver].documents[indexDoc].name = 'Паспорт водителя';
                            }
                            if (document.type == 'license') {
                                $this.organization.drivers[indexDriver].documents[indexDoc].type = 'license';
                                $this.organization.drivers[indexDriver].documents[indexDoc].name = 'Водительское удостоверение';
                            }
                        })
                    });
                }

                if (callback) {
                    callback(data);
                }

                $('.selectpicker').selectpicker('refresh');
                setTimeout(function(){
                    $('.selectpicker').selectpicker('refresh');
                    $('[data-toggle="tooltip"]').tooltip();
                }, 250);

            });

            return false;
        },
        selectRefresh: function() {
            setTimeout(function(){
                $('.selectpicker').selectpicker('refresh');
            }, 300);
        },
        htmlUp: function () {
            var $this = this;

            initDatepickerDefault($('.flatpickr-default'));

            setTimeout(function(){

                $('.selectpicker').selectpicker('refresh');

                $('[data-toggle="tooltip"]').tooltip();
                passwordView();

                $this.editFormProfile($this.edit);
                $this.editFormDrivers($this.editDrivers);

            }, 100);
        },

        showAdditionalPopups: function() {
            $(function () {
                if(window.location.hash === '#verify_fail') {
                    openPopupUrl('/static/popup/verification_error_special');
                }
            })
        },

        editFormProfile: function (edit) {
            var column = $('#profile .profile-column.profile');
            this.editForm(column, edit);
        },
        editFormDrivers: function (edit) {
            var column = $('#profile .profile-column.drivers');
            this.editForm(column, edit);
        },

        editForm: function (column, edit) {
            setTimeout(function(){
                if (edit) {
                    column.find('.selectpicker').prop('disabled', false);
                    column.find('.selectpicker').removeAttr( "readonly");
                    column.find('.selectpicker').parents('.bootstrap-select').removeClass('disabled readonly');
                    column.find('.flatpickr-default input').prop('readonly', false);
                    column.find('.form-control').removeAttr("readonly");
                    column.find('.control input').parent().removeClass('disabled readonly');
                    column.find('.selectpicker').selectpicker('refresh');
                } else {
                    column.find('.control input[readonly]').parent().addClass('disabled readonly');
                    column.find('.flatpickr-default input[readonly]').parent().addClass('disabled');
                    column.find('.control input:checked').parent().removeClass('disabled');
                    column.find('.selectpicker[readonly]').prop('disabled', true).addClass('');
                    column.find('.selectpicker[readonly]').parents('.bootstrap-select').addClass('disabled readonly');
                    column.find('.selectpicker').selectpicker('refresh');
                }
            }, 100)

        },
        suggestInn: function () {
            if(!this.edit) {
                return false;
            }
            if(!this.organization.inn) {
                return;
            }
            var $this = this;
            $.ajax({
                url: '/api/data-normalize/suggest/inn',
                method: 'GET',
                data: {'number':this.organization.inn},
            })
            .done(function(data) {
                if (data.status && data.data) {
                    if(data.data.name && !$this.organization.companyName) {
                        $this.organization.companyName = data.data.name;
                    }
                    if(data.data.kpp && !$this.organization.kpp) {
                        $this.organization.kpp = data.data.kpp;
                    }
                    if(data.data.ceo && !$this.organization.ceo) {
                        $this.organization.ceo = data.data.ceo;
                    }
                    if(data.data.address && !$this.organization.address) {
                        $this.organization.address = data.data.address;
                    }
                    if(data.data.type == "LEGAL") {
                        $this.organization.type = "ООО";
                        $this.selectRefresh();
                    }

                    if(data.data.type == "INDIVIDUAL") {
                        $this.organization.type = "ИП";
                        $this.selectRefresh();
                    }
                    if(data.data.type &&  data.data.type != "INDIVIDUAL" && data.data.type != "LEGAL") {
                        $this.organization.type = "Другое";
                        $this.selectRefresh();
                    }
                }
            });
        },
        cleanOrganizationAddress: function () {
            if(!this.organization.address) {
                return;
            }
            if($('.custom-autocomplete-select .dropdown-menu').is(':visible')) {
                return;
            }

            var $this = this;
            $.ajax({
                url: '/api/data-normalize/clean/address',
                method: 'GET',
                data: {'address':this.organization.address},
            })
            .done(function(data) {
                if (data.status && data.data) {
                    $this.organization.address = data.data;
                    $this.autocompleteOrganizationAddressFromSelect = true;
                    $('.custom-autocomplete-select .selectpicker').selectpicker('hide');
                }
            });
        },
        suggestOrganizationAddress: function () {
            if(this.organization.address) {
                var $this = this;
                $.ajax({
                    url: '/api/data-normalize/suggest/address',
                    method: 'GET',
                    data: {'address':this.organization.address},
                })
                .done(function(data) {
                    if (data.status && data.data) {
                        $this.autocompleteOrganizationAddress = data.data;
                        $this.htmlUp();
                        $('.custom-autocomplete-select .selectpicker').selectpicker('toggle');
                        $('.custom-autocomplete-select .selectpicker').selectpicker('show');
                    }
                });
            }
        },
    },
    mounted: function() {

        $(function(){
            $('#profile').css('visibility', 'visible');
        })

        this.loadData();

        this.htmlUp();

        this.showAdditionalPopups();
    },
    watch: {
       'organization.inn': function() {
            var $this = this;
            clearTimeout(this.organizationInnTypingTimer);
            this.organizationInnTypingTimer = setTimeout(function() {
                $this.suggestInn();
            }, 600);
        },
        'autocompleteOrganizationAddressSelected': function(val) {
            if(val) {
                this.organization.address = val;
                $('.custom-autocomplete-select .selectpicker').selectpicker('hide');
            }
        },
        'organization.address': function(val) {
            var $this = this;
            clearTimeout(this.autocompleteOrganizationAddressTypingTimer);
            if(this.organization.address == this.autocompleteOrganizationAddressSelected) {
                return false;
            }
            if(this.autocompleteOrganizationAddressFromSelect) {
                this.autocompleteOrganizationAddressFromSelect = false;
                return false;
            }
            this.autocompleteOrganizationAddressTypingTimer = setTimeout(function() {
                $this.suggestOrganizationAddress();
            }, 500);
        },
        'organization.type': function (val) {
            if (val == 'ИП') {
                this.organization.kpp = '';
            }
        },
        'editDrivers': function (val) {
            var $this = this;
            if (!val) {
                $this.driverActive = 1;
                $this.editFormDrivers(val);
                $this.loadData(function () {
                    $this.editFormDrivers(val);
                    $this.errors.clear();
                    //$this.htmlUp();
                });
            } else {
                $this.editFormDrivers(val);
                $this.htmlUp();
            }
        },
        'edit': function (val) {
            var $this = this;
            if (!val) {
                $this.editFormProfile(val);

                $this.loadData(function () {
                    $this.editFormProfile(val);
                    ///$this.htmlUp();
 
                    $this.errors.clear();
                });
            } else {
                $this.editFormProfile(val);
                $this.htmlUp();

            }
        }
    },
})
