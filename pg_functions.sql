-- FUNCTION: dotdot.rucksack(numeric, integer, smallint, smallint)

-- DROP FUNCTION dotdot.rucksack(numeric, integer, smallint, smallint);

CREATE OR REPLACE FUNCTION dotdot.rucksack(
	pwmax numeric,
	pqmax integer,
	psetv1 smallint DEFAULT 1,
	psetv2 smallint DEFAULT 1)
    RETURNS TABLE(vers_id smallint, weight smallint, count integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
declare
	vCurrW integer DEFAULT pWmax;
	vAllQ integer DEFAULT 0;
	vCurrQ integer;

	vWonemax smallint;
	vWonemax1 smallint;
	vWonemax2 smallint;
	curdown CURSOR (i integer) FOR
		SELECT w, countp
		FROM   auction_pallet_aggr p
		WHERE  w <= i
		ORDER BY w DESC
	;

BEGIN
  create temporary table if not exists auction_pallet_aggr(w smallint, countp smallint) on commit delete rows;
  truncate table auction_pallet_aggr;
	insert into auction_pallet_aggr(w,countp)
	SELECT p.w, count(*)
	FROM auction_pallet p
	GROUP BY p.w
	;
	SELECT MAX(w) INTO vWonemax FROM auction_pallet_aggr;
	create temporary table if not exists auction_pallet_aggr1(
		versid smallint,
		ids smallint,
		w smallint,
		countp integer,
		wp integer,
		allp integer,
		typec smallint
	)
	on commit delete rows;
	truncate auction_pallet_aggr1;

	FOR i in reverse vWonemax..0 LOOP
		vCurrW:=pWmax;
		vAllQ:=0;
		for rough in curdown(i) loop
			vCurrQ:= CASE WHEN DIV(vCurrW,rough.w) > rough.countp THEN rough.countp ELSE DIV(vCurrW,rough.w) END;
			vCurrW:= vCurrW - vCurrQ*rough.w;
			vAllQ:= vAllQ + vCurrQ;
			vCurrQ:= CASE WHEN (vAllQ > pQmax) THEN vCurrQ - vAllQ + pQmax ELSE vCurrQ END;
			vAllQ:= CASE WHEN (vAllQ > pQmax) THEN pQmax ELSE vAllQ END;
			--
			INSERT INTO auction_pallet_aggr1 (w, countp, allp, wp, versid)
			SELECT rough.w, vCurrQ, vAllQ, vCurrW, i WHERE  vCurrQ > 0;
			--если набрали максимальное кол-во
			EXIT WHEN (vAllQ = pQmax);
		end loop;
	END LOOP;
	--
	SELECT DISTINCT
		first_value(maxw) over (order by allw desc, countp desc) w1,
		first_value(maxw) over (order by allw desc, countp     ) w2
	INTO vWonemax1,	vWonemax2
	FROM (
		SELECT versid, max(w) maxw, sum(countp) countp, sum(w*countp) allw
		FROM  auction_pallet_aggr1 t
		GROUP BY versid
	) s;
	--
	TRUNCATE auction_pallet_aggr1;
	TRUNCATE auction_pallet_aggr;
	INSERT INTO auction_pallet_aggr (w, countp)
	SELECT w, count(*) countp
	FROM auction_pallet p
	GROUP BY w;
    --
	for vWonemax1 in (
		select distinct unnest(
			array_remove(
				array[coalesce(vWonemax1,0),coalesce(vWonemax2,0)],
				0
			)
		)
	)
	loop
		for i in reverse case when vWonemax1 + 1 > pWmax THEN pWmax ELSE vWonemax1 + 1 END .. vWonemax1 - 1 loop
			vCurrW:=pWmax;
			vAllQ:=0;
			for rough in curdown(i) loop
				vCurrQ:= CASE WHEN DIV(vCurrW,rough.w) > rough.countp THEN rough.countp ELSE DIV(vCurrW,rough.w) END;
				vCurrW:= vCurrW - vCurrQ*rough.w;
				vAllQ:= vAllQ + vCurrQ;
				vCurrQ:= CASE WHEN (vAllQ > pQmax) THEN vCurrQ - vAllQ + pQmax ELSE vCurrQ END;
				vAllQ:= CASE WHEN (vAllQ > pQmax) THEN pQmax ELSE vAllQ END;
				INSERT INTO auction_pallet_aggr1 (w, countp, allp, wp, versid)
				SELECT rough.w, vCurrQ, vAllQ, vCurrW, i WHERE  vCurrQ > 0;
				--если набрали максимальное кол-во
				EXIT WHEN (vAllQ = pQmax);
			end loop;
		end loop;
	end loop;
	--
	return query
	SELECT t.versid, t.w, t.countp
	FROM
		auction_pallet_aggr1 t,
		(
			select * from (SELECT versid, SUM(countp) countp, round(SUM(w*countp),-1) allw FROM auction_pallet_aggr1 GROUP BY versid ORDER BY allw desc, countp LIMIT pSetv1) less_pals
			union
			select * from (SELECT versid, SUM(countp) countp, round(SUM(w*countp),-1) allw FROM auction_pallet_aggr1 GROUP BY versid ORDER BY allw desc, countp desc LIMIT pSetv2) more_pals
		) sub
	WHERE  t.versid = sub.versid;
END
$BODY$;

