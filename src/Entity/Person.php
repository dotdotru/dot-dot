<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits;

/**
 * Person
 * @Entity()
 * @ORM\Table(name="app_person")
 * @ORM\HasLifecycleCallbacks()
 */
class Person implements \JsonSerializable
{
    const ISSUANCE_PASSPORT = 'passport';
    const ISSUANCE_CODE = 'code';

    use Traits\IdTrait;
    use Traits\RemoveTrait;
    use Traits\ExternalIdEmptyTrait;
    use Traits\UserTrait;
    use Traits\TimestampableTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $uridical;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $inn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $series;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $issuance;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $contactName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $contactPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $contactEmail;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $companyName;


    protected $vat = false;


    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function isUridical()
    {
        return $this->uridical;
    }

    public function getUridical()
    {
        return $this->uridical;
    }

    public function setUridical($uridical)
    {
        $this->uridical = intval($uridical);

        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setSeries($series)
    {
        $this->series = $series;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getIssuance()
    {
        return $this->issuance;
    }

    /**
     * @param mixed $issuance
     */
    public function setIssuance($issuance): void
    {
        $this->issuance = $issuance;
    }

    public function getInn()
    {
        return $this->inn.'';
    }

    public function setInn($inn)
    {
        $this->inn = $inn;
    }

    public function getContactName()
    {
        return $this->contactName;
    }

    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getPhone()
    {
        return $this->contactPhone;
    }

    public function getEmail()
    {
        return $this->contactEmail;
    }


    /**
     * @return bool
     */
    public function getVat(): ?bool
    {
        return $this->vat;
    }

    /**
     * @param bool $vat
     * @return Person
     */
    public function setVat(?bool $vat): Person
    {
        $this->vat = $vat;
        return $this;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        $name = '';
        if ($this->isUridical()) {
            $name = sprintf('id %s, extid %s, %s %s %s %s', $this->getId(), $this->getExternalId(), $this->getType(), $this->getCompanyName(), $this->getContactName(), $this->getContactPhone());
        } else {
            $name = sprintf('id %s, extid %s, %s %s', $this->getId(), $this->getExternalId(), $this->getContactName(), $this->getContactPhone());
        }
        return $name;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        $fullName = '';
        if ($this->getUridical()) {
            $fullName = sprintf('%s %s (ИНН %s)', $this->getType(), $this->getCompanyName(), $this->getInn());
        } else {
            $fullName = sprintf('%s (Паспорт %s)', $this->getContactName(), $this->getSeries().$this->getNumber());
        }

        $shortName = '';
        if ($this->getUridical()) {
            $shortName = sprintf('%s %s', $this->getType(), $this->getCompanyName());
        } else {
            $shortName = sprintf('%s', $this->getContactName());
        }

        return [
            'id' => $this->getId() ?? '',
            'type' => $this->getType() ?? '',
            'uridical' => $this->getUridical(),
            'inn' => $this->getInn() ?? '',
            'number'=> $this->getNumber() ?? '',
            'series'     => $this->getSeries() ?? '',
            'issuance' => $this->getIssuance() ?? '',
            'code' => $this->getCode() ?? '',
            'contactName'     => $this->getContactName() ?? '',
            'contactPhone'     => $this->getContactPhone() ?? '',
            'contactEmail'     => $this->getContactEmail() ?? '',
            'companyName' => $this->getCompanyName() ?? '',

            'fullName' => $fullName,
            'shortName' => $shortName,

            'hidden' => $this->isRemove()
        ];
    }
}
