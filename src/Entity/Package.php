<?php

namespace App\Entity;

use App\Entity\Traits;
use App\Model\PackageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;
use App\Entity\Packing;

/**
 * Package
 *
 * @Entity(repositoryClass="App\Repository\PackageRepository")
 * @ORM\Table(name="package",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Package implements PackageInterface, \JsonSerializable
{
	use Traits\IdTrait;
	use Traits\TitleEmptyTrait;
    use Traits\ExternalIdTrait;
    use Traits\WeightTrait;
    use Traits\TimestampableTrait;
    use Traits\SizeTrait;
    use Traits\PackingTrait;
    use Traits\PackageTypeTrait;
    use Traits\StatusTrait;
    use Traits\MoverWeightTrait;
    use Traits\MoverDamagedTrait;


    /**
     * Дата прибытия на склад
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $arrivedAt;


    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = (int)$count;

        return $this;
    }

    CONST STATUS_ACCEPTED = 'accepted'; // Принят
    CONST STATUS_PREPARING = 'preparing'; // Подготовка к отгрузке
    CONST STATUS_INTRANSIT = 'in transit'; // Ожидает оплаты

    public static function getStatusList()
    {
        return [
            'Принято' => self::STATUS_ACCEPTED,
            'Подготовка к отгрузке' => self::STATUS_PREPARING,
            'В пути' => self::STATUS_INTRANSIT,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = '';
        if ($this->getStatus() == self::STATUS_ACCEPTED) {
            $publicStatus =  'принят';
        }
        if ($this->getStatus() == self::STATUS_PREPARING) {
            $publicStatus =  'подготовка к отгрузке';
        }
        if ($this->getStatus() == self::STATUS_INTRANSIT) {
            $publicStatus =  'в пути';
        }

        return $publicStatus;
    }

    /**
     * @return DateTime
     */
    public function getArrivedAt(): ?DateTime
    {
        return $this->arrivedAt;
    }

    /**
     * @param DateTime $arrivedAt
     * @return Package
     */
    public function setArrivedAt(DateTime $arrivedAt): Package
    {
        $this->arrivedAt = $arrivedAt;
        return $this;
    }



    /**
     * @return string
     */
    public function __toString() {
        return $this->getExternalId() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }


    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'weight' => $this->getWeight(),
            'size' => $this->getSize(),
            'count' => $this->getCount(),
            'packing' => $this->getPacking(),
            'packageType' => $this->getPackageType(),
            'packageTypeAnother' => $this->getPackageTypeAnother(),
            'status' => $this->getPublicStatus(),

            'moverLastMileDamaged' => $this->getMoverlastMileDamaged(),
            'moverLastMileWeight' => $this->getMoverLastMileWeight(),
        ];
    }
}
