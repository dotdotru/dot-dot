<?php

namespace App\Entity;

use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\PriceTrait;
use App\Entity\Traits\WeightTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Pallet
 *
 * @Entity(repositoryClass="App\Repository\PalletRepository")
 * @ORM\Table(name="pallet",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Pallet implements \JsonSerializable
{
	use IdTrait;
    use PublishedTrait;
    use ExternalIdTrait;
    use TimestampableTrait;
    use PriceTrait;
    use DirectionTrait;
    use WeightTrait;
    use DirectionStockTrait;

    /**
     * @ORM\ManyToMany(targetEntity="Package", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinTable(name="pallet_packages",
     *      joinColumns={@ORM\JoinColumn(name="pallet_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="package_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $packages;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $maxDeliveryTime;

    public function __construct()
    {
        $this->packages = new ArrayCollection();
    }

    public function getPackages()
    {
        return $this->packages;
    }

    public function addPackage(Package $package)
    {
        $this->packages->add($package);

        return $this;
    }

    public function setPackages(ArrayCollection $packages)
    {
        $this->packages = $packages;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxDeliveryTime()
    {
        return $this->maxDeliveryTime;
    }

    /**
     * @param mixed $maxDeliveryTime
     */
    public function setMaxDeliveryTime(DateTime $maxDeliveryTime = null): void
    {
        $this->maxDeliveryTime = $maxDeliveryTime;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getExternalId() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return array(
            'id'     => $this->getId(),
            'externalId'     => $this->getExternalId(),
            'weight'=> $this->getWeight(),
            'stockFrom'=> $this->getStockFrom(),
            'stockTo'=> $this->getStockTo(),
            'direction'     => $this->getDirection(),
            'maxDeliveryTime' => $this->getMaxDeliveryTime(),
        );

    }
}
