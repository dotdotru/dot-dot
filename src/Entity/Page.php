<?php

namespace App\Entity;

use App\Entity\Traits\FilesOldTrait;
use App\Entity\Traits\FilesTrait;
use App\Entity\Traits\LocationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Page
 *
 * @Entity(repositoryClass="App\Repository\PageRepository")
 * @ORM\Table(name="page",indexes={})
 * @ORM\HasLifecycleCallbacks()
 */
class Page
{
    use IdTrait;
    use PublishedTrait;
    use TitleTrait;
    use TimestampableTrait;
    use FilesTrait;
    use FilesOldTrait;
    use DescriptionTrait;

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
