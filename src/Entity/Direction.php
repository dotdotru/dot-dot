<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Direction
 *
 * @Entity(repositoryClass="App\Repository\DirectionRepository")
 * @ORM\Table(name="direction")
 * @ORM\HasLifecycleCallbacks()
 */
class Direction implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TitleTrait;
    use PublishedTrait;
    use TimestampableTrait;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_from_id", referencedColumnName="id")
     */
    protected $cityFrom;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_to_id", referencedColumnName="id")
     */
    protected $cityTo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $startPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $endPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceUpPeriod;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceUpInPeriod;



    /** @var Stock */
    /**
     * Куда везти первую милю
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $priorityStockFrom;


    /** @var Stock */
    /**
     * Куда везти последнюю милю
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $priorityStockTo;


    public function getCityFrom()
    {
        return $this->cityFrom;
    }

    public function setCityFrom(City $cityFrom)
    {
        $this->cityFrom = $cityFrom;

        return $this;
    }

    public function getCityTo()
    {
        return $this->cityTo;
    }

    public function setCityTo(City $cityTo)
    {
        $this->cityTo = $cityTo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartPrice()
    {
        return $this->startPrice;
    }

    /**
     * @param mixed $startPrice
     */
    public function setStartPrice($startPrice): void
    {
        $this->startPrice = $startPrice;
    }

    /**
     * @return mixed
     */
    public function getEndPrice()
    {
        return $this->endPrice;
    }

    /**
     * @param mixed $endPrice
     */
    public function setEndPrice($endPrice): void
    {
        $this->endPrice = $endPrice;
    }

    /**
     * @return mixed
     */
    public function getPriceUpPeriod()
    {
        return $this->priceUpPeriod;
    }

    /**
     * @param mixed $priceUpPeriod
     */
    public function setPriceUpPeriod($priceUpPeriod): void
    {
        $this->priceUpPeriod = $priceUpPeriod;
    }

    /**
     * @return mixed
     */
    public function getPriceUpInPeriod()
    {
        return $this->priceUpInPeriod;
    }

    /**
     * @param mixed $priceUpInPeriod
     */
    public function setPriceUpInPeriod($priceUpInPeriod): void
    {
        $this->priceUpInPeriod = $priceUpInPeriod;
    }

    /**
     * @return mixed
     */
    public function getPriorityStockFrom()
    {
        return $this->priorityStockFrom;
    }

    /**
     * @param mixed $priorityStockFrom
     * @return Direction
     */
    public function setPriorityStockFrom($priorityStockFrom)
    {
        $this->priorityStockFrom = $priorityStockFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriorityStockTo()
    {
        return $this->priorityStockTo;
    }

    /**
     * @param mixed $priorityStockTo
     * @return Direction
     */
    public function setPriorityStockTo($priorityStockTo)
    {
        $this->priorityStockTo = $priorityStockTo;
        return $this;
    }





    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getTitle() : '11';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    public function getSlug($forRoute = false)
    {
        if ($forRoute) {
            return str_replace('iz-', '', $this->slug);
        }
        return $this->slug;
    }


    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
            'cityFrom'     => $this->getCityFrom(),
            'cityTo'     => $this->getCityTo(),
        ];
    }
}
