<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Lot
 */
class Lot
{
    private $items;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getItems()
    {
        return $this->items;
    }

    public function addItem(Pallet $item)
    {
        $this->items->add($item);

        return $this;
    }

    public function setItems(ArrayCollection $items)
    {
        $this->items = $items;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}
