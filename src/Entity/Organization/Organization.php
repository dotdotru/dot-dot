<?php

namespace App\Entity\Organization;

use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\DriversTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\VerifiedTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\UserBundle\Entity\User;

/**
 * Organization
 *
 * @Entity(repositoryClass="App\Repository\Organization\OrganizationRepository")
 * @ORM\Table(name="organization")
 * @ORM\HasLifecycleCallbacks()
 */
class Organization implements \JsonSerializable
{
    use IdTrait;
    use TimestampableTrait;
    use VerifiedTrait;
    use DriversTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $uridical = false;

    /**
     * @ORM\Column(type="string")
     */
    protected $companyName;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $nds;

    /**
     * @ORM\Column(type="string")
     */
    protected $inn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $kpp;

    /**
     * @ORM\Column(type="string")
     */
    protected $ceo;

    /**
     * @ORM\Column(type="string")
     */
    protected $address;

    /**
     * @ORM\Column(type="string")
     */
    protected $contactName;

    /**
     * @ORM\Column(type="string")
     */
    protected $contactPhone;

    /**
     * @ORM\Column(type="string")
     */
    protected $rs;

    /**
     * @ORM\Column(type="string")
     */
    protected $bik;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $innerCarrier = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $withoutDrivers = false;

    /**
     * @ORM\ManyToMany(targetEntity="OrganizationDriver", cascade={"persist","remove"})
     * @ORM\JoinTable(name="organization_drivers",
     *      joinColumns={@ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="driver_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $drivers;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", fetch="EAGER")
     */
    protected $city;


    public function __construct()
    {
        $this->drivers = new ArrayCollection();
    }

    public function isVerifiedAll()
    {
        /** @var OrganizationDriver $driver */
        foreach ($this->getDrivers() as $driver) {
            if (!$driver->isVerifiedSuccess()) {
                return false;
            }
        }

        return $this->isVerifiedSuccess();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getUridical()
    {
        return $this->uridical;
    }

    public function setUridical($uridical)
    {
        $this->uridical = $uridical;

        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getNds()
    {
        return $this->nds;
    }

    public function setNds($nds)
    {
        $this->nds = $nds;

        return $this;
    }

    public function getInn()
    {
        return $this->inn;
    }

    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    public function getKpp()
    {
        return $this->kpp;
    }

    public function setKpp($kpp)
    {
        $this->kpp = $kpp;

        return $this;
    }

    public function getCeo()
    {
        return $this->ceo;
    }

    public function setCeo($ceo)
    {
        $this->ceo = $ceo;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    public function getContactName()
    {
        return $this->contactName;
    }

    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getRs()
    {
        return $this->rs;
    }

    public function setRs($rs)
    {
        $this->rs = $rs;

        return $this;
    }

    public function getBik()
    {
        return $this->bik;
    }

    public function setBik($bik)
    {
        $this->bik = $bik;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInnerCarrier()
    {
        return $this->innerCarrier;
    }

    /**
     * @param mixed $inner
     */
    public function setInnerCarrier($innerCarrier): void
    {
        $this->innerCarrier = $innerCarrier;
    }

    /**
     * @return mixed
     */
    public function isWithoutDrivers()
    {
        return $this->withoutDrivers;
    }

    /**
     * @param mixed $withoutDrivers
     */
    public function setWithoutDrivers($withoutDrivers): void
    {
        $this->withoutDrivers = $withoutDrivers;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Organization
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getCompanyName().'';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'user' => $this->getUser()->getId(),
            'type' => $this->getType(),
            'uridical' => $this->getUridical(),
            'companyName' => $this->getCompanyName(),
            'nds' => (int)$this->getNds(),
            'inn' => $this->getInn(),
            'kpp' => $this->getKpp(),
            'ceo' => $this->getCeo(),
            'address' => $this->getAddress(),
            'contactName' => $this->getContactName(),
            'contactPhone' => $this->getContactPhone(),
            'rs' => $this->getRs(),
            'bik' => $this->getBik(),
            'drivers' => $this->getDrivers()->toArray(),
            'verifiedDrivers' => $this->getVerifiedDrivers(),
            'isWithoutDrivers' => (int)$this->isWithoutDrivers(),
            'verified' => (int)$this->getVerified(),
            'verifiedCode' => $this->getVerifiedCode(),
            'verifiedDate' => ($this->getVerifiedDate())?$this->getVerifiedDate()->format("Y-m-d"):null,
        ];

    }
}
