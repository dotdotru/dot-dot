<?php

namespace App\Entity\Organization;

use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\VerifiedTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Organization\OrganizationDriverDocument;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * OrganizationDriver
 *
 * @Entity(repositoryClass="App\Repository\Organization\OrganizationDriverRepository")
 * @ORM\Table(name="organization_driver")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationDriver implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use VerifiedTrait;
    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $inn;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $birthdate;

    /**
     * @ORM\Column(type="string")
     */
    protected $phone;

    /**
     * @ORM\Column(type="string")
     */
    protected $gender = 'M';

    /**
     * @ORM\ManyToMany(targetEntity="OrganizationDriverDocument", cascade={"persist","remove"})
     * @ORM\JoinTable(name="organization_driver_documents",
     *      joinColumns={@ORM\JoinColumn(name="organization_driver_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $documents;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getShortName()
    {
        $name = str_replace("  ", " ", trim($this->name));
        $nameParts = explode(" ", $name);
        
        if (isset($nameParts[2])) {
            return sprintf('%s %s. %s.', $nameParts[0], mb_substr($nameParts[1], 0, 1), mb_substr($nameParts[2], 0, 1));
        } elseif(isset($nameParts[1])) {
            return sprintf('%s %s.', $nameParts[0], mb_substr($nameParts[1], 0, 1));    
        }
        
        return $name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getGender()
    {
        return $this->gender ?: 'M';
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getInn()
    {
        return $this->inn;
    }

    public function setInn($inn)
    {
        if ($inn != $this->getInn()) {
            $this->setVerified(false);
            $this->setVerifiedCode('');
            $this->setVerifiedDate(null);
        }
        $this->inn = $inn;

        return $this;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function setBirthdate($birthdate)
    {
        if (!is_a($birthdate, DateTime::class)) {
            $dateObject = DateTime::createFromFormat('D M d Y H:i:s e+', $birthdate);

            if (!is_a($dateObject, DateTime::class)) {
                $dateObject = DateTime::createFromFormat('m.d.Y', $birthdate);
            }

            $this->birthdate = $dateObject;
        } else {
            $this->birthdate = $birthdate;
        }

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDocuments()
    {
        return $this->documents;
    }

    public function addDocument(OrganizationDriverDocument $driverDocument)
    {
        $this->documents->add($driverDocument);

        return $this;
    }

    public function setDocuments($driverDocuments)
    {
        $this->documents = $driverDocuments;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId().' '.$this->getName().'';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'inn' => $this->getInn(),
            'birthdate' => $this->getBirthdate() ? $this->getBirthdate()->format('Y-m-d') : '',
            'phone' => $this->getPhone(),
            'gender' => (int)$this->getGender(),
            'documents' => $this->getDocuments()->toArray(),
            'verified' => (int)$this->getVerified(),
            'verifiedCode' => $this->getVerifiedCode(),
            'verifiedDate' => ($this->getVerifiedDate())?$this->getVerifiedDate()->format("Y-m-d"):null,
        ];
    }
}
