<?php

namespace App\Entity\Organization;

use App\Entity\Document;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\SizeTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Organization
 *
 * @Entity(repositoryClass="App\Repository\Organization\OrganizationRepository")
 * @ORM\Table(name="organization_driver_document")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationDriverDocument extends Document
{
    use IdTrait;
    use TimestampableTrait;

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
