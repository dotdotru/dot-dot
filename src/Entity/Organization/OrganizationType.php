<?php

namespace App\Entity\Organization;

use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\DriversTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\SortableTrait;
use App\Entity\Traits\VerifiedTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\UserBundle\Entity\User;

/**
 * Organization
 *
 * @Entity(repositoryClass="App\Repository\Organization\OrganizationTypeRepository")
 * @ORM\Table(name="organization_type")OrganizationType
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationType implements \JsonSerializable
{
    use IdTrait;
    use TitleTrait;
    use SortableTrait;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'slug' => $this->getSlug(),
        ];

    }
}
