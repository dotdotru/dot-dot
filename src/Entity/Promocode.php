<?php

namespace App\Entity;

use App\Entity\Traits\DateRangeTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TitleEmptyTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Traits\UserTrait;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Promocode
 *
 * @Entity()
 * @ORM\Table(name="promocode")
 * @ORM\HasLifecycleCallbacks()
 */
class Promocode implements \JsonSerializable
{
    const TYPE_FIRSTREGISTER = 'firstRegister';

    use IdTrait;
    use PublishedTrait;
    use TitleTrait;
    use DateRangeTrait;
    use UserTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $discountKilo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $discountDeclaredValue;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $percent;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;


    public static function getTypesList()
    {
        return [
            'Первая регистрация' => self::TYPE_FIRSTREGISTER
        ];
    }

    public function typeName()
    {
        $types = self::getTypesList();
        $keyType = array_search($this->getType(), $types);
        return $keyType;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getDiscountKilo()
    {
        return $this->discountKilo;
    }

    public function setDiscountKilo($discountKilo)
    {
        $this->discountKilo = $discountKilo;

        return $this;
    }

    public function getDiscountDeclaredValue()
    {
        return $this->discountDeclaredValue;
    }

    public function setDiscountDeclaredValue($discountDeclaredValue)
    {
        $this->discountDeclaredValue = $discountDeclaredValue;

        return $this;
    }

    public function getPercent()
    {
        return $this->percent;
    }

    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle() ? $this->getTitle() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'discountKilo' => $this->getDiscountKilo(),
        ];
    }
}
