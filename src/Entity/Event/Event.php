<?php

namespace App\Entity\Event;

use App\Entity\Traits\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks()
 */

abstract class Event
{
    use IdTrait;
    use TimestampableTrait;
    use StatusTrait;

    /**
     * Дата начала выполнения
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startAt;

    /**
     * Дата окончания выполнения
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endAt;

    /**
     * Процент выполнения
     *
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $progress;

    /**
     * Время в секундах после которого считать процес мертвым
     *
     * @var float
     *
     * @ORM\Column(type="integer")
     */
    protected $timeInvalidate = 60;

    const STATUS_WAIT = 'wait';
    const STATUS_WORK = 'work';
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';

    public static function getStatusList()
    {
        return [
            'Ожидание обработки' => self::STATUS_WAIT,
            'В работе' => self::STATUS_WORK,
            'Ошибка выполнения' => self::STATUS_ERROR,
            'Успешно выполнено' => self::STATUS_SUCCESS
        ];
    }

    /**
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTime $dateTime)
    {
        $this->startAt = $dateTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTime $dateTime)
    {
        $this->endAt = $dateTime;

        return $this;
    }

    public function getProgress()
    {
        return $this->progress;
    }

    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    public function getTimeInvalidate()
    {
        return $this->timeInvalidate;
    }

    public function setTimeInvalidate($timeInvalidate)
    {
        $this->timeInvalidate = $timeInvalidate;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getId().'' : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
