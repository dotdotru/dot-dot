<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * AbandonedRegister
 *
 * @Entity(repositoryClass="App\Repository\AbandonedRegisterRepository")
 * @ORM\Table(name="abandoned_register")
 * @ORM\HasLifecycleCallbacks()
 */
class AbandonedRegister implements \JsonSerializable
{
    use IdTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $subscribe = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sendAt;

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getSubscribe()
    {
        return $this->subscribe;
    }

    public function setSubscribe($subscribe)
    {
        $this->subscribe = $subscribe;

        return $this;
    }

    public function getSendAt()
    {
        return $this->sendAt;
    }

    public function setSendAt(DateTime $sendAt = null)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getEmail() ? $this->getEmail() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'username'     => $this->getUsername(),
            'email'     => $this->getEmail(),
            'password'     => $this->getPassword(),
            'subscribe'     => $this->getSubscribe()
        ];
    }
}
