<?php

namespace App\Entity;

class Size
{
    protected $width;
    protected $height;
    protected $depth;
    protected $calculateWeight;

    public function __construct($size, $calculateWeight = null)
    {
        $this->setSize($size);
        $this->setCalculateWeight($calculateWeight);
    }

    public function getSizeString()
    {
        return sprintf('%sx%sx%sx', $this->getDepth(), $this->getWidth(), $this->getHeight());
    }

    public function setSize($size = false)
    {
        if ($size) {
            @list($depth, $width, $height) = explode('x', $size);
            $this->setWidth((int)$width);
            $this->setHeight((int)$height);
            $this->setDepth((int)$depth);
        }
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getDepth()
    {
        return $this->depth;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    public function getCalculateWeight()
    {
        return $this->calculateWeight;
    }

    public function setCalculateWeight($calculateWeight)
    {
        $this->calculateWeight = $calculateWeight;
    }

    public function getVolume()
    {
        return $this->calculateWeight ? $this->calculateWeight : $this->getDepth()*$this->getHeight()*$this->getWidth() / 1000 / 1000;
    }
}
