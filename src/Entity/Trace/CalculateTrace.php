<?php

namespace App\Entity\Trace;

use App\Entity\Trace\Trace;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/** @ORM\Entity */

class CalculateTrace extends Trace
{
    /**
     * @ORM\Column(type="text")
     */
    protected $code;

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
}
