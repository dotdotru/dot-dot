<?php

namespace App\Entity\Trace;

use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\Trace\Trace;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/** @ORM\Entity */

class InsuranceTrace extends Trace
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", fetch="EAGER", cascade={"remove"})
     * @ORM\JoinColumn(name="order_customer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $orderCustomer;

    public function getOrder()
    {
        return $this->orderCustomer;
    }

    public function setOrder(Order $orderCustomer)
    {
        $this->orderCustomer = $orderCustomer;

        return $this;
    }
}
