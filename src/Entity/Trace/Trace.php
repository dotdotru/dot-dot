<?php

namespace App\Entity\Trace;

use Doctrine\ORM\Mapping as ORM;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Trace
 *
 * @ORM\Table(name="trace")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\Entity(repositoryClass="App\Repository\TraceRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\DiscriminatorMap({
 *  "0" = "App\Entity\Trace\VerificationTrace",
 *  "1" = "App\Entity\Trace\InsuranceTrace",
 *  "2" = "App\Entity\Trace\CalculateTrace",
 * })
 */
abstract class Trace
{
    use IdTrait;
    use TimestampableTrait;

    const STATUS_WAIT = 'wait';
    const STATUS_WORK = 'work';
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';

    /**
     * @ORM\Column(type="string")
     */
    protected $status = self::STATUS_WAIT;

    /**
     * @ORM\ManyToMany(targetEntity="Log", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="trace_log",
     *      joinColumns={@ORM\JoinColumn(name="trace_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="log_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)}
     * )
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public static function getStatusList()
    {
        return [
            'Ожидание обработки' => self::STATUS_WAIT,
            'В работе' => self::STATUS_WORK,
            'Ошибка выполнения' => self::STATUS_ERROR,
            'Успешно выполнено' => self::STATUS_SUCCESS
        ];
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if (!in_array($status, self::getStatusList())) {
            throw new \Exception('Status not found');
        }

        $this->status = $status;

        return $this;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function addItem(Log $item)
    {
        $this->items->add($item);

        return $this;
    }

    public function setItems(ArrayCollection $items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId().'';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
