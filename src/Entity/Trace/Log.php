<?php

namespace App\Entity\Trace;

use Doctrine\ORM\Mapping as ORM;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Log
 *
 * @ORM\Entity(repositoryClass="App\Repository\LogRepository")
 * @ORM\Table(name="log")
 * @ORM\HasLifecycleCallbacks()
 */
class Log
{
    use IdTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $request;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $response;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $requestIp;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $responseIp;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setRequestIp($requestIp)
    {
        $this->requestIp = $requestIp;
    }

    public function getRequestIp()
    {
        return $this->requestIp;
    }

    public function setResponseIp($responseIp)
    {
        $this->responseIp = $responseIp;
    }

    public function getResponseIp()
    {
        return $this->responseIp;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
