<?php

namespace App\Entity\Trace;

use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Trace\Trace;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/** @ORM\Entity */

class VerificationTrace extends Trace
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization\Organization", fetch="EAGER")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization\OrganizationDriver", fetch="EAGER")
     * @ORM\JoinColumn(name="organization_driver_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $driver;

    public function getOrganization()
    {
        return $this->organization;
    }

    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;

        return $this;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver(OrganizationDriver $driver)
    {
        $this->driver = $driver;

        return $this;
    }
}
