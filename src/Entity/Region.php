<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits;
/**
 * Region
 *
 * @Entity()
 * @ORM\Table(name="region")
 * @ORM\HasLifecycleCallbacks()
 */
class Region implements \JsonSerializable
{
    use Traits\IdTrait;
    use Traits\TitleTrait;
    use Traits\TimestampableTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $timezone;

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone): void
    {
        $this->timezone = $timezone;
    }

    public function getOffsetHour()
    {
        if ($this->getTimezone()) {
            $targetTimeZone = new \DateTimeZone($this->getTimezone());
            $dateTime = new \DateTime('now', $targetTimeZone);
            return (int)$dateTime->format('P');
        }

        return null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
            'timezone' => $this->getTimezone(),
            'offsetHour' => $this->getOffsetHour()
        ];
    }
}
