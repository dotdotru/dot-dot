<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * String
 *
 * @Entity()
 * @ORM\Table(name="string")
 * @ORM\HasLifecycleCallbacks()
 */
class StringEntity implements \JsonSerializable
{
    use IdTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $value;

    public function __construct($value = '')
    {
        $this->setValue($value);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->value;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue()
        ];
    }
}
