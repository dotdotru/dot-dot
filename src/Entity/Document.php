<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\MappedSuperclass()
 */
abstract class Document implements \JsonSerializable
{
    use IdTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="string")
     */
    protected $series;

    /**
     * @ORM\Column(type="string")
     */
    protected $number;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $place;

    public static function getTypes()
    {
        return ['Паспорт' => 'passport', 'Водительское удостоверение' => 'license'];
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setSeries($series)
    {
        $this->series = $series;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        if (!is_a($date, DateTime::class)) {
            $dateObject = DateTime::createFromFormat('D M d Y H:i:s e+', $date);

            if (!is_a($dateObject, DateTime::class)) {
                $dateObject = DateTime::createFromFormat('m.d.Y', $date);
            }
            $this->date = $dateObject;
        } else {
            $this->date = $date;
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'series' => $this->getSeries(),
            'number' => $this->getNumber(),
            'date' => $this->getDate() ? $this->getDate()->format('Y-m-d') : '',
            'place' => $this->getPlace(),
        ];
    }
}
