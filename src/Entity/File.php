<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\LocalExternalIdEmptyTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\FileGroup;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * File
 *
 * * @Entity(repositoryClass="App\Repository\FileRepository")
 * @ORM\Table(name="file")
 * @ORM\HasLifecycleCallbacks()
 */
class File implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use LocalExternalIdEmptyTrait;
    use PublishedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $date;

    /**
     * Название
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="media", referencedColumnName="id")
     * })
     */
    private $media;

    /**
     * @var FileGroup
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FileGroup")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="file_group", referencedColumnName="id")
     * })
     */
    private $group;

    private $path;

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia(Media $media = null)
    {
        $this->media = $media;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function setGroup(FileGroup $group = null)
    {
        $this->group = $group;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(DateTime $date = null): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? (string)$this->getId() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'group' => $this->getGroup(),
            'file' => $this->getPath(),
        ];
    }
}
