<?php

namespace App\Entity;

use App\Entity\Traits\LocationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use App\Repository\AppRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

use App\Application\Sonata\UserBundle\Entity\Group;

/**
 * Stock
 *
 * @Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\Table(name="stock",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Stock implements \JsonSerializable
{

	use IdTrait;
    use PublishedTrait;
    use ExternalIdTrait;
    use TitleTrait;
    use TimestampableTrait;
    use LocationTrait;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    protected $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $mode;

    /**
     * @ORM\Column(type="string")
     */
    protected $phone;

    /**
     * @ORM\Column(type="text")
     */
    protected $howToGet;


    /**
     * Группы пользователей, которым доступен этот склад
     *
     * @ORM\ManyToMany(targetEntity="App\Application\Sonata\UserBundle\Entity\Group", cascade={"persist"})
     * @ORM\JoinTable(name="stock_user_group",
     *      joinColumns={@ORM\JoinColumn(name="stock_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $groups;


    /** @return City */
    public function getCity()
    {
        return $this->city;
    }

    public function setCity(City $city)
    {
        $this->city = $city;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getHowToGet()
    {
        return $this->howToGet;
    }

    public function setHowToGet($howToGet)
    {
        $this->howToGet = $howToGet;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     * @return Stock
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * Ищем группу для фтл складов
     * @return bool
     */
    public function getFtlGroup ()
    {
        foreach ($this->getGroups() as $group) {
            // Пока просто проверим префикс
            if (preg_match('/^ftl.*/is', $group->getName())) {
                return $group;
            }
        }

        return null;
    }


    public function jsonSerialize()
    {

        $groups = [];
        foreach ($this->getGroups() as $group) {
            $groups[] = $group->getName();
        }

        return [
            'id'       => $this->getId(),
            'externalId'       => $this->getExternalId(),
            'city'     => $this->getCity(),
            'address'  => $this->getAddress(),
            'mode'     => $this->getMode(),
            'title'     => $this->getTitle(),
            'published'     => $this->isPublished(),
            'createdAt'   => $this->getCreatedAt(),
            'updatedAt'   => $this->getUpdatedAt(),
            'groups' => $groups,
            'lat' => $this->getLat(),
            'lng' => $this->getLng(),
            'ftlStock' => $this->getFtlGroup() ? true : false
        ];
    }
}
