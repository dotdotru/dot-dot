<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\SynchTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Stock
 *
 * @Entity()
 * @ORM\Table(name="base_stock_worker")
 * @ORM\HasLifecycleCallbacks()
 */
class StockWorker implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Base\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code = '';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstname = '';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastname = '';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $secondname = '';

    /**
     * @return Stock|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock(Stock $stock = null): void
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getSecondname(): ?string
    {
        return $this->secondname;
    }

    /**
     * @param string $secondname
     */
    public function setSecondname(?string $secondname): void
    {
        $this->secondname = $secondname;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s ', $this->getFirstname());
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
            'cityFrom'     => $this->getCityFrom(),
            'cityTo'     => $this->getCityTo(),
        ];
    }
}
