<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;

/**
 * StockWorkingHours
 *
 * @Entity()
 * @ORM\Table(name="base_stock_working_hours")
 * @ORM\HasLifecycleCallbacks()
 */
class StockWorkingHours implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Base\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $dow = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $startTime;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $endTime;

    /**
     * @return Stock|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock(Stock $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getDow()
    {
        return $this->dow;
    }

    /**
     * @param string $dow
     */
    public function setDow(string $dow): void
    {
        $this->dow = $dow;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     */
    public function setStartTime($startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     */
    public function setEndTime($endTime): void
    {
        $this->endTime = $endTime;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s ', $this->getId());
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'dow'     => $this->getDow(),
            'startTime'     => $this->getStartTime(),
            'endTime'     => $this->getEndTime(),
            'stockId'     => $this->getStock()->getId(),
            'externalId'     => $this->getStock()->getExternalId(),
        ];
    }
}
