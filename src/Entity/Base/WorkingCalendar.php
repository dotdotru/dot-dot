<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;

/**
 * WorkingCalendar
 *
 * @Entity()
 * @ORM\Table(name="base_working_calendar")
 * @ORM\HasLifecycleCallbacks()
 */
class WorkingCalendar implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Base\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @var datetime
     * @ORM\Column(type="datetime")
     */
    protected $day;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $startTime;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $endTime;

    /**
     * @return DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param DateTime $day
     */
    public function setDay(DateTime $day = null): void
    {
        $this->day = $day;
    }

    /**
     * @return Stock|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock(Stock $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     */
    public function setStartTime($startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     */
    public function setEndTime($endTime): void
    {
        $this->endTime = $endTime;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s ', $this->getId());
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'day'     => $this->getDay()->format('Y-m-d'),
            'startTime'     => $this->getStartTime(),
            'endTime'     => $this->getEndTime(),
            'stockId'     => $this->getStock()->getId(),
        ];
    }
}
