<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\SortableTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;
use App\Entity\Base\Direction;

/**
 * HandlingPrice
 *
 * @Entity()
 * @ORM\Table(name="base_handling_price")
 * @ORM\HasLifecycleCallbacks()
 */
class HandlingPrice
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $externalId = null;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $weightFrom = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $weightTo = null;

    /**
     * @ORM\Column(type="float")
     */
    protected $price = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWeightFrom(): ?int
    {
        return $this->weightFrom;
    }

    /**
     * @param int $weightFrom
     */
    public function setWeightFrom(?int $weightFrom): void
    {
        $this->weightFrom = $weightFrom;
    }

    /**
     * @return int
     */
    public function getWeightTo(): ?int
    {
        return $this->weightTo;
    }

    /**
     * @param int $weightTo
     */
    public function setWeightTo(?int $weightTo): void
    {
        $this->weightTo = $weightTo;
    }

    /**
     * @return int
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    /**
     * @param int $externalId
     */
    public function setExternalId(int $externalId): void
    {
        $this->externalId = $externalId;
    }





}
