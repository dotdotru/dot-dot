<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;

/**
 * Stock
 *
 * @Entity()
 * @ORM\Table(name="base_stock")
 * @ORM\HasLifecycleCallbacks()
 */
class Stock implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $address = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $timezone = '';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $extSystem = '';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $extId = '';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $code = '';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $login = '';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $pass = '';

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity(City $city = null): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(string $timezone): void
    {
        $this->timezone = $timezone;
    }

    /**
     * @return string
     */
    public function getExtSystem()
    {
        return $this->extSystem;
    }

    /**
     * @param string $extSystem
     */
    public function setExtSystem(string $extSystem = null): void
    {
        $this->extSystem = $extSystem;
    }

    /**
     * @return string
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * @param string $extId
     */
    public function setExtId(string $extId = null): void
    {
        $this->extId = $extId;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass): void
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s', $this->getAddress());
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
            'cityFrom'     => $this->getCityFrom(),
            'cityTo'     => $this->getCityTo(),
        ];
    }
}
