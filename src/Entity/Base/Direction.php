<?php

namespace App\Entity\Base;

use App\Entity\Traits\CustomerDeliveryTime;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;

/**
 * Direction
 *
 * @Entity()
 * @ORM\Table(name="base_direction")
 * @ORM\HasLifecycleCallbacks()
 */
class Direction implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;
    use CustomerDeliveryTime;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_from_id", referencedColumnName="id")
     */
    protected $cityFrom;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_to_id", referencedColumnName="id")
     */
    protected $cityTo;

    /**
     * @ORM\ManyToMany(targetEntity="DirectionPrice", cascade={"all"}, orphanRemoval=true)
     * @ORM\JoinTable(name="base_direction_prices",
     *      joinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="direction_price_id", referencedColumnName="id", nullable=true)}
     * )
     * @ORM\OrderBy({"wmin" = "ASC"})
     */
    private $directionPrices;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $carrierDeliveryTime = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $reloadTime = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $standardOfDelivery = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $standardOfSupplyDelivery = '';

    public function __construct()
    {
        $this->directionPrices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCityFrom()
    {
        return $this->cityFrom;
    }

    /**
     * @param mixed $cityFrom
     */
    public function setCityFrom($cityFrom): void
    {
        $this->cityFrom = $cityFrom;
    }

    /**
     * @return mixed
     */
    public function getCityTo()
    {
        return $this->cityTo;
    }

    /**
     * @param mixed $cityTo
     */
    public function setCityTo($cityTo): void
    {
        $this->cityTo = $cityTo;
    }

    /**
     * @return string
     */
    public function getCarrierDeliveryTime(): string
    {
        return $this->carrierDeliveryTime;
    }

    /**
     * @param string $carrierDeliveryTime
     */
    public function setCarrierDeliveryTime(string $carrierDeliveryTime): void
    {
        $this->carrierDeliveryTime = $carrierDeliveryTime;
    }

    /**
     * @return string
     */
    public function getReloadTime(): string
    {
        return $this->reloadTime;
    }

    /**
     * @param string $reloadTime
     */
    public function setReloadTime(string $reloadTime): void
    {
        $this->reloadTime = $reloadTime;
    }

    /**
     * @return string
     */
    public function getStandardOfDelivery()
    {
        return $this->standardOfDelivery;
    }

    /**
     * @param string $standardOfDelivery
     */
    public function setStandardOfDelivery(string $standardOfDelivery): void
    {
        $this->standardOfDelivery = $standardOfDelivery;
    }

    /**
     * @return string
     */
    public function getStandardOfSupplyDelivery()
    {
        return $this->standardOfSupplyDelivery;
    }

    /**
     * @param string $standardOfSupplyDelivery
     */
    public function setStandardOfSupplyDelivery(string $standardOfSupplyDelivery): void
    {
        $this->standardOfSupplyDelivery = $standardOfSupplyDelivery;
    }

    /**
     * @return ArrayCollection
     */
    public function getDirectionPrices()
    {
        return $this->directionPrices;
    }

    /**
     * @return mixed
     */
    public function addDirectionPrice(DirectionPrice $directionPrice)
    {
        $this->directionPrices->add($directionPrice);

        return $this;
    }

    /**
     * @param mixed $directionPrices
     */
    public function setDirectionPrices($directionPrices): void
    {
        $this->directionPrices = $directionPrices;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $cityForm = '';
        if ($this->getCityFrom()) {
            $cityForm = $this->getCityFrom()->getTitle();
        }

        $cityTo = '';
        if ($this->getCityTo()) {
            $cityTo = $this->getCityTo()->getTitle();
        }

        return sprintf('из %s в %s', $cityForm, $cityTo);
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'cityFrom'     => $this->getCityFrom(),
            'cityTo'     => $this->getCityTo(),
        ];
    }
}
