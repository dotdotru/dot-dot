<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\SortableTrait;
use App\Entity\Traits\SynchTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Base\City;
use App\Entity\Base\Direction;

/**
 * DirectionPrice
 *
 * @Entity()
 * @ORM\Table(name="base_direction_price")
 * @ORM\HasLifecycleCallbacks()
 */
class DirectionPrice implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Base\Direction", fetch="EAGER")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    protected $direction;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $wmin = 0;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $price = 0;

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection(Direction $direction): void
    {
        $this->direction = $direction;
    }

    /**
     * @return integer
     */
    public function getWmin()
    {
        return $this->wmin;
    }

    /**
     * @param integer $wmin
     */
    public function setWmin($wmin): void
    {
        $this->wmin = $wmin;
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param integer $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function __toString() {
        return sprintf('%s', $this->getId());
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
            'cityFrom'     => $this->getCityFrom(),
            'cityTo'     => $this->getCityTo(),
        ];
    }
}
