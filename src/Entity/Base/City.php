<?php

namespace App\Entity\Base;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\SynchTrait;
use App\Entity\Traits\TitleEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\CityCode;

/**
 * City
 *
 * @Entity()
 * @ORM\Table(name="base_city")
 * @ORM\HasLifecycleCallbacks()
 */
class City implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TitleEmptyTrait;
    use TimestampableTrait;
    use SynchTrait;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $carrierArrivalLimit = '';

    /**
     * @return string
     */
    public function getCarrierArrivalLimit(): string
    {
        return $this->carrierArrivalLimit;
    }

    /**
     * @param string $carrierArrivalLimit
     */
    public function setCarrierArrivalLimit(string $carrierArrivalLimit): void
    {
        $this->carrierArrivalLimit = $carrierArrivalLimit;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
        ];
    }
}
