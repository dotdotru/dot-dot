<?php

namespace App\Entity;

use App\Entity\Traits\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * BaseEvent
 *
 * @ORM\Entity(repositoryClass="App\Repository\BaseEventRepository")
 * @ORM\Table(name="base_event")
 * @ORM\HasLifecycleCallbacks()
 */
class BaseEvent implements \JsonSerializable
{
    const STATUS_CREATE = 'create';
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';

    use IdTrait;
    use ExternalIdTrait;
    use StatusTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $entityType;

    /**
     * @ORM\Column(type="string")
     */
    protected $entityId;

    /**
     * @ORM\Column(type="string")
     */
    protected $event;

    public static function getStatusList()
    {
        return [
            'Создан' => self::STATUS_CREATE,
            'Ошибка обработки' => self::STATUS_ERROR,
            'Обработано' => self::STATUS_SUCCESS,
        ];
    }

    public function getEntityType()
    {
        return $this->entityType;
    }

    public function setEntityType($entityType)
    {
        $this->entityType = $entityType;

        return $this;
    }

    public function getEntityId()
    {
        return $this->entityId;
    }

    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getEntityType().' '.$this->getEvent();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId()
        ];
    }
}
