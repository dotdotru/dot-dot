<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\PriceTrait;
use App\Entity\Traits\SizeTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Packing
 *
 * @Entity(repositoryClass="App\Repository\PackingRepository")
 * @ORM\Table(name="packing",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Packing implements \JsonSerializable
{
    use IdTrait;
    use TitleTrait;
    use PublishedTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use PriceTrait;
    use SizeTrait;

    const TYPE_FOR_ONE = 'for_one';
    const TYPE_TO_SIZE = 'to_size';

    /**
     * @ORM\Column(type="float")
     */
    protected $minPrice;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    public static function getTypes()
    {
        return [
            'За один' => self::TYPE_FOR_ONE,
            'По размеру' => self::TYPE_TO_SIZE,
        ];
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getTitle().'' : '11';
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * @param mixed $minPrice
     * @return Packing
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;
        return $this;
    }





    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'price' => $this->getPrice(),
            'minPrice' => $this->getMinPrice(),
            'type' => $this->getType(),
        ];
    }
}
