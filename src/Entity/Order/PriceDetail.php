<?php

namespace App\Entity\Order;


use App\Entity\Traits\SortableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;


/**
 * @Entity()
 * @ORM\Table()
 */
class PriceDetail implements \JsonSerializable
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $price;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $priceVat;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="priceDetails")
     */
    private $order;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PriceDetail
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return PriceDetail
     */
    public function setType(string $type): PriceDetail
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string $price
     * @return PriceDetail
     */
    public function setPrice(?string $price): PriceDetail
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getPriceVat(): ?string
    {
        return $this->priceVat;
    }

    /**
     * @param string $priceVat
     * @return PriceDetail
     */
    public function setPriceVat(?string $priceVat): PriceDetail
    {
        $this->priceVat = $priceVat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return PriceDetail
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }


    public function jsonSerialize()
    {
        return [
            'type' => $this->getType(),
            'price' => $this->getPrice(),
            'priceVat' => $this->getPriceVat(),
        ];

    }
}
