<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;

/**
 * String
 *
 * @Entity()
 * @ORM\Table(name="city_code")
 * @ORM\HasLifecycleCallbacks()
 */
class CityCode
{
    use IdTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @ORM\Column(type="string")
     */
    protected $value;

    public static function getCodes()
    {
        return [
            'Деловые линии' => 'dellin',
            'ТК КИТ' => 'tkkit',
            'ПЭК' => 'pecom',
            'Ратек' => 'ratek',
            'ЖелДорЭкспедиция' => 'jde',
        ];
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function __toString()
    {
        return $this->value;
    }
}
