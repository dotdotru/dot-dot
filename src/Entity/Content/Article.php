<?php

namespace App\Entity\Content;

use App\Entity\Content\Block;
use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="App\Repository\Content\ArticleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Article
{
    use Traits\IdTrait;
    use Traits\ExternalIdEmptyTrait;
    use Traits\PublishedTrait;
    use Traits\TitleTrait;
    use Traits\TimestampableTrait;
    use Traits\DescriptionTrait;
    use Traits\PictureTrait;
    use Traits\PicturePreviewTrait;
    use Traits\PreviewTrait;
    use Traits\TagsTrait;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->blocks = new ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Content\Block", inversedBy="block", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="app_article_block")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $blocks;

    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    public function addBlock(Block $block)
    {
        $this->blocks->add($block);
    }

    /**
     * @param mixed $blocks
     */
    public function setBlocks($blocks): void
    {
        $this->blocks = $blocks;
    }

    public function __toString()
    {
        return sprintf('%s', $this->getTitle() ? $this->getTitle() : 'Новая статья');
    }
}
