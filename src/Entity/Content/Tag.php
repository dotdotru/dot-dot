<?php

namespace App\Entity\Content;

use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Tag
{
    use Traits\IdTrait;
    use Traits\TitleTrait;

    public function __toString()
    {
        return sprintf('%s', $this->getTitle());
    }
}
