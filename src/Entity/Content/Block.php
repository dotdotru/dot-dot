<?php

namespace App\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits as Traits;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Block
 * @ORM\Entity()
 * @ORM\Table(name="block")})
 *
 */
class Block
{
    use Traits\IdTrait;
    use Traits\SortableTrait;

    use Traits\TitleEmptyTrait;
    use Traits\DescriptionTrait;
    use Traits\PictureTrait;
    use Traits\PublishedTrait;

    const TYPE_HEADING = 'heading';
    const TYPE_SUBHEADING = 'sub_heading';
    const TYPE_TEXT = 'text';
    const TYPE_TEXT_WITH_IMAGE = 'text_image';
    const TYPE_IMAGE = 'image';
    const TYPE_LINK = 'link';
    const TYPE_LIST = 'list';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    protected $type = 'text';

    /**
     * @var string
     *
     * @ORM\Column(name="heading", type="string", nullable=true)
     */
    protected $heading;

    /**
     * @var string
     *
     * @ORM\Column(name="subheading", type="string", nullable=true)
     */
    protected $subheading;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", nullable=true)
     */
    protected $link;


    public function __construct()
    {
        $this->objects = new ArrayCollection();
        if (!$this->getPosition()) {
            $this->setPosition(100);
        }
    }

    public static function getTypes()
    {
        return [
            'Текст' => self::TYPE_TEXT,
            'Картинка' => self::TYPE_IMAGE,
            'Ссылка' => self::TYPE_LINK,
        ];
    }

    /**
     * @return string
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }

    /**
     * @param string $heading
     */
    public function setHeading($heading): void
    {
        $this->heading = $heading;
    }

    /**
     * @return string
     */
    public function getSubheading(): ?string
    {
        return $this->subheading;
    }

    /**
     * @param string $subheading
     */
    public function setSubheading($subheading): void
    {
        $this->subheading = $subheading;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getType():?string
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     * @return void
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function __toString()
    {
        return $this->getType();
    }
}
