<?php

namespace App\Entity\Content;

use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Faq
 *
 * @ORM\Table(name="faq")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Faq
{
    use Traits\IdTrait;
    use Traits\TitleTrait;
    use Traits\TimestampableTrait;
    use Traits\PublishedTrait;
    use Traits\DescriptionTrait;
    use Traits\PositionTrait;

    /**
     * @ORM\ManyToOne(targetEntity="FaqCategory", fetch="EAGER")
     * @ORM\JoinColumn(name="faq_category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }
}
