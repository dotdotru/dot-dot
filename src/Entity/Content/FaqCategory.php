<?php

namespace App\Entity\Content;

use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * FaqCategory
 *
 * @ORM\Table(name="faq_category")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class FaqCategory
{
    use Traits\IdTrait;
    use Traits\TitleTrait;
    use Traits\TimestampableTrait;
    use Traits\PublishedTrait;
    use Traits\DescriptionTrait;
    use Traits\PositionTrait;

    const TYPE_CUSTOMER = 'customer';
    const TYPE_CARRIER = 'carrier';

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    public function getTypeName()
    {
        foreach (self::getTypeList() as $title => $value) {
            if ($value == $this->type) {
                return $title;
            }
        }

        return '';
    }

    public static function getTypeList()
    {
        return [
            'Для отправителей' => self::TYPE_CUSTOMER,
            'Для перевозчиков' => self::TYPE_CARRIER,
        ];
    }

    public function __toString()
    {
        return sprintf('%s', $this->getTitle());
    }
}
