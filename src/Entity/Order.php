<?php

namespace App\Entity;

use App\Entity\Order\PriceDetail;
use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Payment;
use App\Application\Sonata\MediaBundle\Entity\Media;
use JsonSerializable;
use Symfony\Component\Translation\Interval;

/**
 * Order
 *
 * @Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="u_order_customer",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Order implements OrderInterface, JsonSerializable
{
    use Traits\IdTrait;
    use Traits\ExternalIdEmptyTrait;
    use Traits\TimestampableTrait;
    use Traits\DirectionTrait;
    use Traits\DirectionStockTrait;
    use Traits\StatusTrait;
    use Traits\PayedTrait;
    use Traits\InsuranceTrait;
    use Traits\SenderTrait;
    use Traits\OrderPersonTrait;
    use Traits\FilesTrait;
    use Traits\RemoveTrait;
    use Traits\PromocodeTrait;
    use Traits\NotificationListTrait;
    use Traits\CustomerDeliveryTime;
    use Traits\ShippingsTrait;
    use Traits\UtmTrait;

    /**
     * @ORM\ManyToMany(targetEntity="Package", inversedBy="packages", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="order_package2")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    protected $packages;

    /**
     * @ORM\ManyToMany(targetEntity="OrderPackage", inversedBy="packages", cascade={"remove", "persist"})
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    protected $orderPackages;

    /**
     * @ORM\ManyToMany(targetEntity="Payment", cascade={"persist"})
     * @ORM\JoinTable(name="order_customer_payment",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $payments;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $declaredPrice;

    /**
     * Дата приему на склад
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $receptionAt;

    /**
     * Дата выдачи
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $issueAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $minPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $minPriceWithPromocode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxPriceWithPromocode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $minPriceWithoutPromocode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxPriceWithoutPromocode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $suspendedPrice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $editRecevier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $stopSend;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pauseDelivery;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $first;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $byCargo;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $suspendDate;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Order\PriceDetail", mappedBy="order", cascade={"persist"})
     */
    protected $priceDetails;


    public function __construct()
    {
        $this->packages = new ArrayCollection();
        $this->orderPackages = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->shippings = new ArrayCollection();
        $this->priceDetails = new ArrayCollection();
    }

    CONST STATUS_NEW = 'new';
    CONST STATUS_BOOKED = 'booked'; // Требует оформления
    CONST STATUS_WAITING = 'waiting'; // Ожидается
    CONST STATUS_ACCEPTED = 'accepted'; // Принят
    CONST STATUS_REJECTED = 'rejected'; // Отказан
    CONST STATUS_PREPARING = 'preparing'; // Подготовка к отгрузке
    CONST STATUS_NOTPAID = 'not paid'; // Ожидает оплаты
    CONST STATUS_PAID = 'paid'; // Оплачено
    CONST STATUS_READY = 'ready'; // Готов к выдаче
    CONST STATUS_DELIVERED = 'delivered'; // Выдан
    CONST STATUS_SUSPENDED = 'suspended'; // Остановлен


    #const STATUS_SHIPPING = 'shipping';

    #const STATUS_CANCELED = 'canceled';

    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Требует оформления' => self::STATUS_BOOKED,
            'Ожидается' => self::STATUS_WAITING,
            'Принят' => self::STATUS_ACCEPTED,
            'Отказан' => self::STATUS_REJECTED,
            'Подготовка к отгрузке' => self::STATUS_PREPARING,
            'Ожидает оплаты' => self::STATUS_NOTPAID,
            'Оплачено' => self::STATUS_PAID,
            'Готов к выдаче' => self::STATUS_READY,
            'Выдан' => self::STATUS_DELIVERED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = 'требует оформления';
        if ($this->getStatus() == self::STATUS_NEW || $this->getStatus() == self::STATUS_BOOKED) {
            $publicStatus = 'требует оформления';
        }
        if ($this->getStatus() == self::STATUS_WAITING) {
            $publicStatus =  'ожидается';
        }
        if ($this->getStatus() == self::STATUS_ACCEPTED) {
            $publicStatus =  'принят';
        }
        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'отказан';
        }
        if ($this->getStatus() == self::STATUS_PREPARING) {
            $publicStatus =  'подготовка к отгрузке';
        }
        if ($this->getStatus() == self::STATUS_NOTPAID) {
            $publicStatus =  'ожидает оплаты';
        }
        if ($this->getStatus() == self::STATUS_PAID) {
            $publicStatus =  'оплачено';
        }
        if ($this->getStatus() == self::STATUS_READY) {
            $publicStatus =  'готов к выдаче';
        }
        if ($this->getStatus() == self::STATUS_DELIVERED) {
            $publicStatus =  'выдан';

            $shipping = $this->getShipping(Shipping::TYPE_DELIVER);
            if ($shipping && $shipping->isActive() && !$this->getFileByGroup('act')) {
                $publicStatus =  'выдан*';
            }
        }

        if ($this->isPauseDelivery() || $this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        //if ($this->isStopSend()) {
        //    $publicStatus =  'отказ от отправления';
        //}

        return $publicStatus;
    }

    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_BOOKED,
            self::STATUS_NOTPAID,
            self::STATUS_PAID,
        ])) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    public function getGroupingPackages()
    {
        $groupPackages = [];
        $found = false;
        foreach ($this->getPackages() as $package) {
            $found = false;
            foreach ($groupPackages as &$groupPackage) {
                if (
                    $package->getSize() == $groupPackage->getSize()
                    && $package->getWeight() == $groupPackage->getWeight()
                    && $package->getPacking() == $groupPackage->getPacking()
                    && $package->getPackingPrice() == $groupPackage->getPackingPrice()
                    && $package->getPackageType() && $groupPackage->getPackageType() && $package->getPackageType()->getId() == $groupPackage->getPackageType()->getId()
                    && $package->getPackageTypeAnother() == $groupPackage->getPackageTypeAnother()
                ) {
                    $found = true;
                    $groupPackage->setCount($groupPackage->getCount() + 1);
                    break;
                }
            }

            if (!$found) {
                $groupPackages[] = $package;
            }
        }

        return $groupPackages;
    }

    public function getPackages()
    {
        foreach ($this->packages as &$package) {
            $package->setCount(1);
        }
        return $this->packages;
    }

    public function getPackagesTypes()
    {
        $result = [];
        if ($this->packages->count() > 0) {
            foreach ($this->getPackages() as $package) {
                $typeTitle = $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother();
                $result[$typeTitle] = $typeTitle;
            }
        } else {
            foreach ($this->orderPackages as $package) {
                $typeTitle = $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother();
                $result[$typeTitle] = $typeTitle;
            }
        }


        return array_values($result);
    }

    public function getPackagesGroupStatus()
    {
        $result = [];
        /** @var Package $package */
        foreach ($this->getPackages() as $package) {
            if (!isset($result[$package->getStatus()])) {
                $result[$package->getStatus()] = ['count' => 0, 'status' => $package->getPublicStatus()];
            }
            $result[$package->getStatus()]['count']++;
        }

        return array_values($result);
    }

    public function addPackage(Package $item)
    {
        $this->packages->add($item);

        return $this;
    }

    public function setPackages(ArrayCollection $packages = null)
    {
        $this->packages = $packages;

        return $this;
    }

    public function getOrderPackages()
    {
        return $this->orderPackages;
    }

    public function addOrderPackage(OrderPackage $item)
    {
        $this->orderPackages->add($item);

        return $this;
    }

    public function setOrderPackages(ArrayCollection $orderPackages = null)
    {
        $this->orderPackages = $orderPackages;

        return $this;
    }

    public function getDeclaredPrice()
    {
        return $this->declaredPrice;
    }

    public function setDeclaredPrice($declaredPrice)
    {
        $this->declaredPrice = $declaredPrice;

        return $this;
    }

    public function getPayments()
    {
        return $this->payments;
    }

    public function getPayment()
    {
        if ($this->payments) {
            return $this->payments->last();
        }

        return false;
    }

    public function addPayment(Payment $item)
    {
        $this->payments->add($item);

        return $this;
    }

    public function setPayments(ArrayCollection $payments = null)
    {
        $this->payments = $payments;

        return $this;
    }

    public function getTotalWeight()
    {
        $weight = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $weight += $package->getWeight();
            } else {
                $weight += $package->getWeight() * $package->getCount();
            }
        }

        if ($weight <= 0) {
            $weight = 0;
            foreach ($this->getOrderPackages() as $orderPackage) {
                if ($this->isByCargo()) {
                    $weight += $orderPackage->getWeight();
                } else {
                    $weight += $orderPackage->getWeight() * $orderPackage->getCount();
                }
            }
        }

        return $weight;
    }

    public function getTotalVolume()
    {
        $volume = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $volume += $package->getSizeObject()->getVolume();
            } else {
                $volume += $package->getSizeObject()->getVolume() * $package->getCount();
            }
        }

        if ($volume <= 0) {
            $volume = 0;
            foreach ($this->getOrderPackages() as $orderPackage) {
                if ($this->isByCargo()) {
                    $volume += $orderPackage->getSizeObject()->getVolume();
                } else {
                    $volume += $orderPackage->getSizeObject()->getVolume() * $orderPackage->getCount();
                }
            }
        }

        return $volume;
    }

    public function getTotalCount()
    {
        $count = 0;
        foreach ($this->getPackages() as $package) {
            $count += $package->getCount();
        }

        if ($count <= 0) {
            $count = 0;
            foreach ($this->getOrderPackages() as $orderPackage) {
                $count += $orderPackage->getCount();
            }
        }

        return $count;
    }

    public function getMinPrice()
    {
        return ceil($this->minPrice);
    }

    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getMaxPrice()
    {
        return ceil($this->maxPrice);
    }

    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getMinTotalPrice()
    {
        return $this->getMinPrice() + $this->getTotalPriceShipping();
    }

    public function getMaxTotalPrice()
    {
        return $this->getMaxPrice() + $this->getTotalPriceShipping();
    }

    public function getMinTotalPriceWithPromocode()
    {
        return ceil($this->getMinPriceWithPromocode()) + $this->getTotalPriceShipping();
    }

    public function getMaxTotalPriceWithPromocode()
    {
        return ceil($this->getMaxPriceWithPromocode()) + $this->getTotalPriceShipping();
    }

    public function getMinTotalPriceWithoutPromocode()
    {
        return ceil($this->getMinPriceWithoutPromocode()) + $this->getTotalPriceShipping();
    }

    public function getMaxTotalPriceWithoutPromocode()
    {
        return ceil($this->getMaxPriceWithoutPromocode()) + $this->getTotalPriceShipping();
    }

    public function setPrice($price)
    {
        return $this->price = $price;
    }

    public function getPrice()
    {
        return ceil($this->price) + $this->getTotalPriceShipping();
    }

    public function getTotalPriceShipping()
    {
        $totalPrice = 0;

        $shipping = $this->getShipping(Shipping::TYPE_PICKUP);
        if ($shipping && $shipping->isActive()) {
            if (is_array($shipping->getPrice())) dump($this->getId(), $shipping->getPrice());
            $totalPrice += $shipping->getPrice();
        }

        $shipping = $this->getShipping(Shipping::TYPE_DELIVER);
        if ($shipping && $shipping->isActive()) {
            $totalPrice += $shipping->getPrice();
        }

        return ceil($totalPrice);
    }

    public function getTotalPrice()
    {
        return $this->getPrice();
    }

    public function setEditRecevier($editRecevier)
    {
        return $this->editRecevier = $editRecevier;
    }

    public function getEditRecevier()
    {
        return $this->editRecevier;
    }

    public function setSuspendedPrice($suspendedPrice)
    {
        return $this->suspendedPrice = $suspendedPrice;
    }

    public function getSuspendedPrice()
    {
        return $this->suspendedPrice;
    }

    public function getTotalPricePackage()
    {
        $packingPrice = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $packingPrice += $package->getPackingPrice();
            }
            else {
                $packingPrice += $package->getPackingPrice() / $package->getCount();
            }
        }

        if (count($this->getPackages()) == 0) {
            $packingPrice = 0;
            /** @var OrderPackage $orderPackage */
            foreach ($this->getOrderPackages() as $orderPackage) {
                if ($this->isByCargo()) {
                    $packingPrice += $orderPackage->getPackingPrice();
                }
                else {
                    $packingPrice += $orderPackage->getPackingPrice() / $orderPackage->getCount();
                }
            }
        }

        return round($packingPrice);
    }

    public function getTotalPricePacking()
    {
        $packingPrice = 0;
        foreach ($this->getPackages() as $package) {
            $packingPrice += $package->getPackingPrice();
        }

        if (count($this->getPackages()) == 0) {
            $packingPrice = 0;
            /** @var OrderPackage $orderPackage */
            foreach ($this->getOrderPackages() as $orderPackage) {
                $packingPrice += $orderPackage->getPackingPrice();
            }
        }

        return round($packingPrice);
    }

    public function setReceptionAt(DateTime $dateTime)
    {
        return $this->receptionAt = $dateTime;
    }

    /**::L
     * @return \DateTime
     */
    public function getReceptionAt()
    {
        return $this->receptionAt;
    }

    public function setIssueAt(DateTime $dateTime)
    {
        return $this->issueAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getIssueAt()
    {
        return $this->issueAt;
    }

    public function setSuspendDate(DateTime $dateTime = null)
    {
        return $this->suspendDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getSuspendDate()
    {
        return $this->suspendDate;
    }


    /**
     * Проверяет все грузы и если они доставлены, возвращает дату доставки последнего груза
     *
     * @return \DateTime
     */
    public function getArrivedAt(): ?DateTime
    {
        $arrivedAt = null;
        $packages = $this->getPackages();

        foreach ($packages as $package) {
            if ($package->getArrivedAt()) {
                if (!$arrivedAt || $arrivedAt < $package->getArrivedAt()) {
                    $arrivedAt  = $package->getArrivedAt();
                }
            } else {
                // Если есть недоставленный груз, дата поступления на склад неизвестна
                return null;
            }
        }

        return $arrivedAt;
    }


    public function getHash()
    {
        $hash = sha1($this->getId().''.$this->getExternalId().''.$this->getUser());
        return $hash;
    }

    public function isStopSend()
    {
        return $this->stopSend;
    }

    public function setStopSend($stopSend)
    {
        $this->setSuspendDate(null);
        $this->stopSend = $stopSend;
    }

    public function isPauseDelivery()
    {
        return $this->pauseDelivery;
    }

    public function setPauseDelivery($pauseDelivery)
    {
        $this->setSuspendDate(null);
        $this->pauseDelivery = $pauseDelivery;
    }

    public function isFirst()
    {
        return $this->first;
    }

    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * @return mixed
     */
    public function getMinPriceWithPromocode()
    {
        return $this->minPriceWithPromocode;
    }

    /**
     * @param mixed $minPriceWithPromocode
     */
    public function setMinPriceWithPromocode($minPriceWithPromocode): void
    {
        $this->minPriceWithPromocode = $minPriceWithPromocode;
    }

    /**
     * @return mixed
     */
    public function getMaxPriceWithPromocode()
    {
        return $this->maxPriceWithPromocode;
    }

    /**
     * @param mixed $maxPriceWithPromocode
     */
    public function setMaxPriceWithPromocode($maxPriceWithPromocode): void
    {
        $this->maxPriceWithPromocode = $maxPriceWithPromocode;
    }

    /**
     * @return mixed
     */
    public function getMinPriceWithoutPromocode()
    {
        return $this->minPriceWithoutPromocode;
    }

    /**
     * @param mixed $minPriceWithoutPromocode
     */
    public function setMinPriceWithoutPromocode($minPriceWithoutPromocode): void
    {
        $this->minPriceWithoutPromocode = $minPriceWithoutPromocode;
    }

    /**
     * @return mixed
     */
    public function getMaxPriceWithoutPromocode()
    {
        return $this->maxPriceWithoutPromocode;
    }

    /**
     * @param mixed $maxPriceWithoutPromocode
     */
    public function setMaxPriceWithoutPromocode($maxPriceWithoutPromocode): void
    {
        $this->maxPriceWithoutPromocode = $maxPriceWithoutPromocode;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    public function isSuspended()
    {
        return in_array($this->getStatus(), [self::STATUS_SUSPENDED]) || $this->isPauseDelivery() || $this->isStopSend() ;
    }

    public function isNew()
    {
        return in_array($this->getStatus(), [self::STATUS_BOOKED, self::STATUS_NEW]);
    }

    public function isRemoveExactStatus()
    {
        return $this->isNew() || $this->isCreated();
    }

    public function isStopSendExactStatus()
    {
        return !$this->isFinished() && !$this->stopSend && !$this->isNew();
    }

    public function isCreated()
    {
        return in_array($this->getStatus(), [self::STATUS_BOOKED]);
    }

    public function isWaiting()
    {
        return in_array($this->getStatus(), [self::STATUS_WAITING]);
    }

    public function isAccepted()
    {
        return in_array($this->getStatus(), [self::STATUS_ACCEPTED]);
    }

    public function isFinished()
    {
        return in_array($this->getStatus(), [self::STATUS_DELIVERED, self::STATUS_SUSPENDED]);
    }

    public function isPauseDeliveryExactStatus()
    {
        return !in_array($this->getStatus(), []) && !$this->isCreated() && !$this->isNew() && !$this->pauseDelivery;
    }

    public function isShipped()
    {
        return in_array($this->getStatus(), [self::STATUS_DELIVERED]);
    }

    public function isEditDeliver()
    {
        $shipping = $this->getShipping(Shipping::TYPE_DELIVER);

        return $shipping && $shipping->isActive() && in_array($this->getStatus(), [Order::STATUS_READY, Order::STATUS_NOTPAID]);
    }

    public function isAddDeliver()
    {
        $shipping = $this->getShipping(Shipping::TYPE_DELIVER);

        return !$this->isPayed() && $shipping;
    }

    /**
     * @return mixed
     */
    public function isByCargo()
    {
        return $this->byCargo;
    }

    /**
     * @param mixed $byCargo
     */
    public function setByCargo($byCargo)
    {
        $this->byCargo = $byCargo;
    }

    public function isShowRealPrice()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NOTPAID,
            self::STATUS_PAID,
            self::STATUS_READY,
            self::STATUS_DELIVERED,
            self::STATUS_SUSPENDED
        ]);
    }

    /**
     * @return mixed
     */
    public function getPriceDetails()
    {
        return $this->priceDetails;
    }

    /**
     * @param mixed $priceDetails
     * @return Order
     */
    public function setPriceDetails($priceDetails)
    {
        $this->priceDetails = $priceDetails;
        return $this;
    }

    /**
     * @param mixed $priceDetail
     * @return Order
     */
    public function addPriceDetails(PriceDetail $priceDetail)
    {
        $this->priceDetails[] = $priceDetail;
        return $this;
    }



    public function formatPackages()
    {
        $result = '';
        /** @var Package $package */
        foreach ($this->getGroupingPackages() as $package) {
            $item = '';
            if ($package->getPackageType()) {
                $item .= sprintf('%s ', $package->getPackageType()->getTitle());
            } elseif ($package->getPackageTypeAnother()) {
                $item .= sprintf('%s ', $package->getPackageTypeAnother());
            }
            if ($package->getWeight()) {
                $item .= sprintf('%s кг ', $package->getWeight());
            }
            if ($package->getSize()) {
                $item .= sprintf('Размер %s', $package->getSize());
            }
            if ($package->getPacking()) {
                $item .= sprintf('Упаковка %s стоимостью %s', $package->getPacking()->getTitle(), $package->getPacking()->getPrice());
            }

            $result .= PHP_EOL.$item;
        }

        return $result;
    }

    public function formatOrderPackages()
    {
        $result = '';
        /** @var OrderPackage $package */
        foreach ($this->getOrderPackages() as $package) {
            $item = '';
            if ($package->getPackageType()) {
                $item .= sprintf('%s ', $package->getPackageType()->getTitle());
            } elseif ($package->getPackageTypeAnother()) {
                $item .= sprintf('%s ', $package->getPackageTypeAnother());
            }
            if ($package->getWeight()) {
                $item .= sprintf('%s кг ', $package->getWeight());
            }
            if ($package->getSize()) {
                $item .= sprintf('Размер %s', $package->getSize());
            }
            if ($package->getPacking()) {
                $item .= sprintf('Упаковка %s стоимостью %s', $package->getPacking()->getTitle(), $package->getPacking()->getPrice());
            }

            $result .= PHP_EOL.$item;
        }

        return $result;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        //$this->updatedAt = new \DateTime();
    }

    public function isEditStatus()
    {
        return in_array($this->getStatus(), ['', self::STATUS_BOOKED, self::STATUS_NEW, self::STATUS_WAITING]);
    }

    public function isEditFirstMile()
    {
        $result = true;
        $today = new DateTime();
        $shipping = $this->getShipping(Shipping::TYPE_PICKUP);

        if ($shipping && $shipping->getDone()) {
            return false;
        }

        if ($shipping && $shipping->getPickAt() && $shipping->getDriver()) {
            $pickAt = $shipping->getPickAt();
            $pickAt->sub(new \DateInterval('PT12H'));
            if ($today > $pickAt) {
                $result = false;
            }
        }

        return
            $result
            && in_array($this->getStatus(), [self::STATUS_WAITING, self::STATUS_BOOKED, self::STATUS_NEW]);
    }

    public function isEditLastMile()
    {
        $result = true;
        $today = new DateTime();
        $shipping = $this->getShipping(Shipping::TYPE_DELIVER);

        if ($shipping && $shipping->getDone()) {
            return false;
        }

        if ($shipping && $shipping->getPickAt()) {
            $pickAt = $shipping->getPickAt();
            $pickAt->sub(new \DateInterval('PT12H'));
            if ($today > $pickAt) {
                $result = false;
            }
        }

        return $result && in_array($this->getStatus(), [self::STATUS_WAITING, self::STATUS_BOOKED, self::STATUS_NEW, self::STATUS_PREPARING, self::STATUS_ACCEPTED, self::STATUS_NOTPAID]) || $this->isEditDeliver();
    }

    public function jsonSerialize()
    {
        $shippingPickUp = $this->getShipping(Shipping::TYPE_PICKUP);
        $shippingPickUpJson = $shippingPickUp ? $shippingPickUp->jsonSerialize() : null;

        $shippingDeliver = $this->getShipping(Shipping::TYPE_DELIVER);
        $shippingDeliverJson = $shippingDeliver ? $shippingDeliver->jsonSerialize() : null;




        $priceDetails = [];
        $dotDetails = [
            'type' => 'dotdot',
            'name' => 'Вознаграждение',
            'price' => 0,
            'priceVat' => null
        ];
        /** @var PriceDetail $priceDetail */
        foreach ($this->getPriceDetails() as $priceDetail) {
            if (in_array($priceDetail->getType(), ['dotdot', 'insurance', 'handling', 'documents'])) {
                $dotDetails['price'] += 1 * $priceDetail->getPrice();
                $dotDetails['priceVat'] += 1 * $priceDetail->getPriceVat();
            }
            $tmpNames = [
                'total' => 'Итого',
                'pack' => 'Упаковка',
                'pickup_total' => 'Забор груза',
                'deliver_total' => 'Доставка груза',
                'transit' => 'Перевозка'
            ];
            if (in_array($priceDetail->getType(), ['pack', 'total', 'pickup_total', 'deliver_total', 'transit'])) {
                $priceDetails[$priceDetail->getType()] = $priceDetail->jsonSerialize();
                $priceDetails[$priceDetail->getType()]['name'] = !empty($tmpNames[$priceDetail->getType()]) ? $tmpNames[$priceDetail->getType()] : $priceDetail->getType();
            }
        }

        if (!empty($priceDetails['total']) && !empty($priceDetails['pickup_total'])) {
            $priceDetails['total']['price'] += $priceDetails['pickup_total']['price'];
            $priceDetails['total']['priceVat'] += $priceDetails['pickup_total']['priceVat'];
        }
        if (!empty($priceDetails['total']) && !empty($priceDetails['deliver_total'])) {
            $priceDetails['total']['price'] += $priceDetails['deliver_total']['price'];
            $priceDetails['total']['priceVat'] += $priceDetails['deliver_total']['priceVat'];
        }

        $priceDetails['dotdot'] = $dotDetails;



        return array(
            'id'     => $this->getId(),
            'user' => $this->getUser()->jsonSerialize(),
            'userPerson' => $this->getUser()->getPerson(),
            'organization' => $this->getUser()->getOrganizationCustomer(),
            'sender'=> $this->getSender(),
            'receiver'=> $this->getReceiver(),
            'payer'=> $this->getPayer(),
            'customer'=> $this->getCustomer(),
            'editRecevier'=> $this->getEditRecevier(),
            'stockFrom'=> $this->getStockFrom(),
            'stockTo'=> $this->getStockTo(),
            'direction'     => $this->getDirection(),
            'status'     => $this->getPublicStatus(),
            'statusClass'     => $this->getPublicStatusClass(),
            'payed'     => $this->getPayed(),
            'totalWeight'     => ceil($this->getTotalWeight()),
            'totalVolume'     => $this->getTotalVolume(),
            'totalPricePackage' => ceil($this->getTotalPricePackage()),
            'totalPricePacking' => ceil($this->getTotalPricePacking()),
            'minPrice'     => ceil($this->getMinTotalPrice()),
            'maxPrice'     => ceil($this->getMaxTotalPrice()),
            'minPriceRoute'     => ceil($this->getMinPrice()),
            'maxPriceRoute'     => ceil($this->getMaxPrice()),
            'minPriceWithPromocode' => ceil($this->getMinTotalPriceWithPromocode()),
            'maxPriceWithPromocode' => ceil($this->getMaxTotalPriceWithPromocode()),
            'minPriceWithoutPromocode' => ceil($this->getMinTotalPriceWithoutPromocode()),
            'maxPriceWithoutPromocode' => ceil($this->getMaxTotalPriceWithoutPromocode()),
            'price'     => $this->getTotalPrice(),
            'suspendedPrice'     => $this->getSuspendedPrice(),

            'priceDetails' => $priceDetails,

            'insuredNumber'     => $this->getInsuredNumber(),
            'insuredPdf'     => $this->getInsuredPdf(),

            'packages'     => $this->getPackages()->toArray(),
            'packagesTypes'     => $this->getPackagesTypes(),
            'packagesGroupStatus'     => $this->getPackagesGroupStatus(),
            'orderPackages'     => $this->getOrderPackages()->toArray(),

            'receptionAt'     => $this->getReceptionAt(),
            'arrivedAt'     => $this->getArrivedAt(),
            'issueAt'     => $this->getIssueAt(),

            'realStatus'     => $this->getStatus(),
            'isSuspended'     => $this->isSuspended(),
            'isStopSendExactStatus'     => $this->isStopSendExactStatus(),
            'isRemoveExactStatus'     => $this->isRemoveExactStatus(),
            'isPauseDeliveryExactStatus'     => $this->isPauseDeliveryExactStatus(),
            'isShipped'     => $this->isShipped(),
            'isNew'     => $this->isNew(),
            'isCreated'     => $this->isCreated(),
            'isWaiting'     => $this->isWaiting(),
            'isEditDeliver' => $this->isEditDeliver(),
            'isAddDeliver' => $this->isAddDeliver(),
            'isStopSend'     => $this->isStopSend(),
            'isPauseDelivery'     => $this->isPauseDelivery(),

            'totalCount'     => $this->getTotalCount(),

            'isShowRealPrice' => $this->isShowRealPrice(),
            'isEditStatus' => $this->isEditStatus(),
            'isEditFirstMile' => $this->isEditFirstMile(),
            'isEditLastMile' => $this->isEditLastMile(),

            'promocode' => $this->getPromocode(),
            'autoAddPromocode' => $this->isAutoAddPromocode(),

            'createdAt' => $this->getCreatedAt(),

            'files'     => $this->getFiles()->toArray(),
            'shippings'     => [
                'pickUp' => $shippingPickUpJson,
                'deliver' => $shippingDeliverJson,
            ],
        );
    }

}
