<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\FilesPathTrait;
use App\Entity\Traits\FilesTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\FileGroup;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Email
 *
 * @Entity(repositoryClass="App\Repository\EmailRepository")
 * @ORM\Table(name="email_result")
 * @ORM\HasLifecycleCallbacks()
 */
class Email implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use StatusTrait;
    use TimestampableTrait;
    use FilesPathTrait;

    const STATUS_CREATE = 'create';
    const STATUS_ERROR = 'error';
    const STATUS_SEND = 'send';

    /**
     * @ORM\Column(type="text")
     */
    protected $subject;

    /**
     * @ORM\Column(type="text")
     */
    protected $body;

    /**
     * @ORM\Column(type="string")
     */
    protected $fromEmail;

    /**
     * @ORM\Column(type="string")
     */
    protected $toEmail;

    /**
     * @ORM\Column(type="text")
     */
    protected $template;


    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $params = [];

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * @return $this
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getToEmail()
    {
        return $this->toEmail;
    }

    /**
     * @return $this
     */
    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }


    /**
     * @return array
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(?array $params): void
    {
        $this->params = $params;
    }



    /**
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    public static function getStatusList()
    {
        return [
            'Создан' => self::STATUS_CREATE,
            'Ошибка отправки' => self::STATUS_ERROR,
            'Отправлено' => self::STATUS_SEND,
        ];
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? (string)$this->getId() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle()
        ];
    }
}
