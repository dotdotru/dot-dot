<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Package
 *
 * @Entity(repositoryClass="App\Repository\Personal\AuthtokenRepository")
 * @ORM\Table(
 *     name="authtoken",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="authtoken", columns={"code"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Authtoken implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $code;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;


    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getCodeHumanReadable()
    {
        return implode('-', str_split(str_replace('-', '', $this->code), 5));
    }

    /**
     * @param mixed $code
     * @return Authtoken
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Authtoken
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Authtoken
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


    public function jsonSerialize()
    {
        $data = [
            'id' => $this->getId(),
            'code' => $this->getCodeHumanReadable(),
        ];

        if ($this->getOrder()) {
            $data['order'] = [
                'id' => $this->getOrder()->getId(),
                'createdAt' => $this->getOrder()->getCreatedAt()
            ];
        }

        return $data;
    }


}
