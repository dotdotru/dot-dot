<?php

namespace App\Entity;

use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;

/**
 * Batch
 *
 * @Entity(repositoryClass="App\Repository\BatchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Batch extends AbstractBatch implements \JsonSerializable
{

    /**
     * @ORM\ManyToMany(targetEntity="StringEntity", cascade={"persist"})
     * @ORM\JoinTable(name="order_weights",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="string_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $weights;

    /**
     * @ORM\ManyToMany(targetEntity="StringEntity", cascade={"persist"})
     * @ORM\JoinTable(name="order_pallets",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="pallet_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $pallets;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxWeight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxCount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $currentPricePerKilo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $overload = false;

    /**
     * Дата приему на склад
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $receptionAt;

    /**
     * Дата выдачи
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $issueAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    protected $direction;


    /** @var Stock */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_from_id", referencedColumnName="id")
     */
    protected $stockFrom;

    /** @var Stock */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_to_id", referencedColumnName="id")
     */
    protected $stockTo;


    public function __construct()
    {
        $this->weights = new ArrayCollection();
        $this->pallets = new ArrayCollection();
        parent::__construct();
    }


    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Требует оформления' => self::STATUS_RESERVED,
            'Ожидает загрузки' => self::STATUS_READY,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_ARRIVED,
            'Оплачено' => self::STATUS_PAYED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }


    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_RESERVED,
        ]) && !$this->getAcceptanceAt()) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    public function getTotalPallets()
    {
        $totalPallets = 0;
        if ($this->getOverload()) {
            $totalPallets = $this->getPallets()->count();
        } else {
            $totalPallets = $this->getWeights()->count();
        }

        return $totalPallets;
    }

    public function getWeights()
    {
        return $this->weights;
    }

    public function addWeightItem(StringEntity $item)
    {
        $this->weights->add($item);

        return $this;
    }

    public function setWeights($weights)
    {
        foreach ($weights as $key => &$weight) {
            if (!($weight instanceof StringEntity)) {
                $weights[$key] = new StringEntity($weight);
            }
        }

        $this->weights = $weights;

        return $this;
    }

    public function getPallets()
    {
        return $this->pallets;
    }

    public function addPalletItem(StringEntity $item)
    {
        $this->pallets->add($item);

        return $this;
    }

    public function setPallets($pallets)
    {
        foreach ($pallets as $key => &$pallet) {
            if (!($pallet instanceof StringEntity)) {
                $pallets[$key] = new StringEntity($pallet);
            }
        }

        $this->pallets = $pallets;

        return $this;
    }

    public function setPrice($price)
    {
        return $this->price = $price;
    }

    public function getPrice()
    {
        return ceil($this->price);
    }

    public function getCurrentPricePerKilo()
    {
        return $this->currentPricePerKilo;
    }

    public function setCurrentPricePerKilo($currentPricePerKilo)
    {
        $this->currentPricePerKilo = $currentPricePerKilo;

        return $this;
    }


    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    public function setMaxWeight($maxWeight)
    {
        $this->maxWeight= $maxWeight;

        return $this;
    }

    public function getMaxCount()
    {
        return $this->maxCount;
    }

    public function setMaxCount($maxCount)
    {
        $this->maxCount= $maxCount;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getId().'' : '11';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function isNew()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW]);
    }

    public function isRemoveExactStatus()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW, self::STATUS_RESERVED]);
    }

    public function isSuspended()
    {
        return $this->stopSend;
    }

    public function isCreated()
    {
        return $this->getStatus() != self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->getStatus() == self::STATUS_ARRIVED;
    }

    public function isStopSendExactStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_READY]) && !$this->stopSend;
    }

    /**
     * @param DateTime $updatePayedDate
     */
    public function setUpdatePayedDate(DateTime $updatePayedDate = null): void
    {
        $this->updatePayedDate = $updatePayedDate;
    }

    public function setPayed($payed)
    {
        $this->setUpdatePayedDate(null);
        $this->payed = (bool)$payed;
    }

    /**
     * @return mixed
     */
    public function getOverload()
    {
        return $this->overload;
    }

    /**
     * @param mixed $overload
     */
    public function setOverload($overload): void
    {
        $this->overload = $overload;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function setDirection($direction)
    {
        $this->direction = $direction;
    }



    /** @return Stock|null */
    public function getStockFrom()
    {
        return $this->stockFrom;
    }

    public function setStockFrom(Stock $stockFrom)
    {
        $this->stockFrom = $stockFrom;

        return $this;
    }

    /** @return Stock|null */
    public function getStockTo()
    {
        return $this->stockTo;
    }

    public function setStockTo(Stock $stockTo)
    {
        $this->stockTo = $stockTo;

        return $this;
    }

    public function isEdit()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_READY,
            self::STATUS_RESERVED
        ]);
    }

    public function jsonSerialize()
    {
        return array(
            'id'     => $this->getId(),
            'carrier' => $this->getCarrier()->jsonSerialize(),
            'weight'=> $this->getWeight(),
            'stockFrom'=> $this->getStockFrom(),
            'stockTo'=> $this->getStockTo(),
            'direction'     => $this->getDirection(),
            'status'     => $this->getPublicStatus(),
            'realStatus'     => $this->getStatus(),
            'statusClass'     => $this->getPublicStatusClass(),
            'payed'     => $this->getPayed(),
            'weights'     => $this->getWeights()->toArray(),

            'sendDate'     => $this->getSendDate(),

            'receptionAt'     => $this->getReceptionAt(),
            'issueAt'     => $this->getIssueAt(),

            'price' => $this->getPrice(),

            'totalWeight' => $this->getWeight(),
            'totalPallets' => $this->getTotalPallets(),

            'isFinished'     => $this->isFinished(),
            'isSuspended'     => $this->isSuspended(),
            'isNew'     => $this->isNew(),
            'isCreated'     => $this->isCreated(),
            'isStopSendExactStatus'     => $this->isStopSendExactStatus(),
            'isRemoveExactStatus'     => $this->isRemoveExactStatus(),
            'isEdit'     => $this->isEdit(),
            'overload'     => $this->getOverload(),
            'drivers'     => $this->getDrivers()->toArray(),
            'files'     => $this->getFiles()->toArray(),

            'acceptanceAt' => $this->getAcceptanceAt(),
            'createdAt' => $this->getCreatedAt(),
            'type' => 'batch'
        );

    }
}
