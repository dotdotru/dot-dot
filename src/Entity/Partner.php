<?php

namespace App\Entity;

use App\Entity\Traits\SortableTrait;
use App\Entity\Traits\UrlTrait;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use App\Repository\AppRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * Partner
 *
 * @Entity(repositoryClass="App\Repository\PartnerRepository")
 * @ORM\Table(name="partner")
 * @ORM\HasLifecycleCallbacks()
 */
class Partner implements \JsonSerializable
{
    use IdTrait;
    use PublishedTrait;
    use TitleTrait;
    use UrlTrait;
    use TimestampableTrait;
    use SortableTrait;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="media", referencedColumnName="id")
     * })
     */
    private $media;

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia(Media $media = null)
    {
        $this->media = $media;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'title'     => $this->getTitle(),
        ];
    }
}
