<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PayedTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Payment
 *
 * @Entity()
 * @ORM\Table(name="payment")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment
{
    use IdTrait;
    use StatusTrait;
    use PayedTrait;
    use TimestampableTrait;
    use ExternalIdEmptyTrait;

    const STATUS_CREATED = 'CREATED';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REVERSED = 'REVERSED';
    const STATUS_REFUNDED = 'REFUNDED';
    const STATUS_DECLINED = 'DECLINED';
    const STATUS_DEPOSITED = 'DEPOSITED';

    public static function getStatusList()
    {
        return [
            'Ожидание' => self::STATUS_CREATED,
            'Подтвержден' => self::STATUS_APPROVED,
            'Отказ (REVERSED)' => self::STATUS_REVERSED,
            'Возвращен' => self::STATUS_REFUNDED,
            'Отказ' => self::STATUS_DECLINED,
            'Выполнен' => self::STATUS_DEPOSITED,
        ];
    }

    public function isDeposited()
    {
        return $this->status === self::STATUS_DEPOSITED;
    }

    public function isApproved()
    {
        return $this->status === self::STATUS_APPROVED;
    }

    public function isReversed()
    {
        return $this->status === self::STATUS_REVERSED;
    }

    public function isRefunded()
    {
        return $this->status === self::STATUS_REFUNDED;
    }

    public function isDeclined()
    {
        return $this->status === self::STATUS_DECLINED;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        if ($this->isDeposited()) {
            $this->setPayed(true);
            $this->setPayedDate(new DateTime());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId().'';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
