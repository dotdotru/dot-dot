<?php

namespace App\Entity\Ad;

use App\Entity\StringEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits;

/**
 * Utm
 * @Entity()
 * @ORM\Table(name="app_utm")
 * @ORM\HasLifecycleCallbacks()
 */
class Utm
{
    use Traits\IdTrait;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $utmSource;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $utmMedium;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $utmCampaign;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $utmContent;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $utmTerm;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $url;

    /**
     * @return string
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @param string $utmSource
     */
    public function setUtmSource($utmSource): void
    {
        $this->utmSource = $utmSource;
    }

    /**
     * @return string
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @param string $utmMedium
     */
    public function setUtmMedium($utmMedium): void
    {
        $this->utmMedium = $utmMedium;
    }

    /**
     * @return string
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @param string $utmCampaign
     */
    public function setUtmCampaign($utmCampaign): void
    {
        $this->utmCampaign = $utmCampaign;
    }

    /**
     * @return string
     */
    public function getUtmContent(): ?string
    {
        return $this->utmContent;
    }

    /**
     * @param string $utmContent
     */
    public function setUtmContent($utmContent): void
    {
        $this->utmContent = $utmContent;
    }

    /**
     * @return string
     */
    public function getUtmTerm(): ?string
    {
        return $this->utmTerm;
    }

    /**
     * @param string $utmTerm
     */
    public function setUtmTerm($utmTerm): void
    {
        $this->utmTerm = $utmTerm;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    public function addParameter($parameterName, $parameterValue)
    {
        switch ($parameterName) {
            case 'utm_source':
                $this->setUtmSource($parameterValue);
                break;
            case 'utm_medium':
                $this->setUtmMedium($parameterValue);
                break;
            case 'utm_campaign':
                $this->setUtmCampaign($parameterValue);
                break;
            case 'utm_content':
                $this->setUtmContent($parameterValue);
                break;
            case 'utm_term':
                $this->setUtmTerm($parameterValue);
                break;
            case 'url':
                $this->setUrl($parameterValue);
                break;
        }
    }

    public function __toString()
    {
        return implode(', ', [
            'utm_source:'.$this->getUtmSource(),
            'utm_medium:'.$this->getUtmMedium(),
            'utm_campaign:'.$this->getUtmCampaign(),
            'utm_content:'.$this->getUtmContent(),
            'utm_term:'.$this->getUtmTerm(),
        ]);
    }
}
