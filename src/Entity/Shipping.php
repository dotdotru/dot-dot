<?php

namespace App\Entity;

use App\Entity\Organization\OrganizationDriver;
use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\StockTrait;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Shipping
 * @Entity(repositoryClass="App\Repository\ShippingRepository")
 * @ORM\Table(name="shipping")
 * @ORM\HasLifecycleCallbacks()
 */
class Shipping implements \JsonSerializable
{
    use IdTrait;
    use TimestampableEntity;
    use StockTrait;

    const TYPE_PICKUP = 'pickup';
    const TYPE_DELIVER = 'deliver';


    /**
     * @var string
     *
     * @ORM\Column(name="externalId", type="string", length=255, nullable=true)
     */
    protected $externalId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $reject = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $provider;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $distance;

    /**
    * Дата забора грузка
    *
    * @var \DateTime
    *
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $pickAt;

    /**
    * @ORM\Column(type="string", nullable=true)
    */
    protected $address;

    /**
    * @ORM\Column(type="integer", nullable=true)
    */
    protected $countLoader = 0;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $hydroBoard = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $price = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $driverName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $driverPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $driverDocument;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization\OrganizationDriver")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $driver;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $done;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lat;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lng;



    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Order", mappedBy="shippings")
     */
    private $order;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsOrder", mappedBy="shippings")
     */
    private $wmsOrder;

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return Shipping
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param mixed $driver
     * @return Shipping
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
        return $this;
    }





    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function isReject()
    {
        return $this->reject;
    }

    /**
     * @param mixed $reject
     */
    public function setReject($reject): void
    {
        $this->reject = $reject;
    }

    /**
     * @return mixed
     */
    public function hasDriver()
    {
        return $this->getDriverName() ? true : false;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getExternalIdPostfix()
    {
        if ($this->type == self::TYPE_PICKUP) {
            return 'M-1';
        } elseif ($this->type == self::TYPE_DELIVER) {
            return 'M-L';
        }
        return '';
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCountLoader()
    {
        return $this->countLoader;
    }

    /**
     * @param mixed $countLoader
     */
    public function setCountLoader($countLoader): void
    {
        $this->countLoader = $countLoader;
    }

    /**
     * @return mixed
     */
    public function getHydroBoard()
    {
        return $this->hydroBoard;
    }

    /**
     * @param mixed $hydroBoard
     */
    public function setHydroBoard($hydroBoard): void
    {
        $this->hydroBoard = $hydroBoard;
    }

    public function setPickAt(DateTime $dateTime = null)
    {
        return $this->pickAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getPickAt()
    {
        return $this->pickAt;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }


    /**
     * @return mixed
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * @param mixed $done
     */
    public function setDone($done): void
    {
        $this->done = $done;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     * @return Shipping
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     * @return Shipping
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWmsOrder()
    {
        return $this->wmsOrder;
    }

    /**
     * @param mixed $wmsOrder
     * @return Shipping
     */
    public function setWmsOrder($wmsOrder)
    {
        $this->wmsOrder = $wmsOrder;
        return $this;
    }



    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }


    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function formatText()
    {
        $result = '';
        if ($this->isActive()) {
            if ($this->getType() == 'ip') {
                $result .= sprintf('%s ', 'ИП');
            } elseif ($this->getType() == 'mover') {
                $result .= sprintf('%s ', 'МУВЕР');
            }

            if ($this->getExternalId()) {
                $result .= sprintf('%s (id %s)', 'Оформлена', $this->getExternalId());
            }

            if ($this->getType() == Shipping::TYPE_PICKUP) {
                $result .= sprintf('от %s до %s ', $this->getAddress(), $this->getStock()->getTitle());
            } elseif ($this->getType() == Shipping::TYPE_DELIVER) {
                $result .= sprintf('от %s до %s ', $this->getStock()->getTitle(), $this->getAddress());
            }

            if ($this->getPickAt()) {
                $result .= sprintf('время %s', $this->getPickAt()->format('d.m.Y H:i'));
            }

            if ($this->getPickAt()) {
                $result .= sprintf('стоимость %s р.', $this->getPrice());
            }

            if ($this->getDriverName()) {
                $result .= sprintf('Водитель %s (паспорт %s)', $this->getDriverName(), $this->getDriverPhone(), $this->getDriverDocument());
            }
        }

        return $result;
    }






    /**
     * Берем из сущности водителя
     */
    /**
     * @return mixed
     */
    public function getDriverName()
    {
        return $this->getDriver() ? $this->getDriver()->getName() : null;
    }
    /**
     * @return mixed
     */
    public function getDriverPhone()
    {
        return $this->getDriver() ? $this->getDriver()->getPhone() : null;
    }
    /**
     * @return mixed
     */
    public function getDriverDocument($type = 'passport')
    {
        $driver = $this->getDriver();
        if (!$driver) {
            return null;
        }

        $driverDocument = '';

        /** @var Document $document */
        foreach ($driver->getDocuments() as $document) {
            if ($document->getType() == $type) {
                $driverDocument = $document->getSeries() . ' ' . $document->getNumber();
            }
        }

        return $driverDocument;
    }






    public function jsonSerialize()
    {
        $data = [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'active' => $this->isActive(),
            'externalId' => $this->getExternalId(),
            'date' => $this->getPickAt() ? $this->getPickAt()->format('Y-m-d H:i') : '',
            'dateObject' => $this->getPickAt() ? $this->getPickAt()->format('Y-m-d H:i') : '',
            'countLoader' => $this->getCountLoader(),
            'hydroBoard' => $this->getHydroBoard() ? 1 : 0,
            'address' => $this->getAddress(),
            'stock' => $this->getStock() ? $this->getStock()->getId() : '',
            'stockObject' => $this->getStock() ? $this->getStock() : [],
            'price' => $this->getPrice(),
            'driverName' => $this->getDriverName(),
            'driverPhone' => $this->getDriverPhone(),
            'driverDocument' => $this->getDriverDocument()
        ];

        return $data;
    }
}