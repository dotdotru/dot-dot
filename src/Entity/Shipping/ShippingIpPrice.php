<?php

namespace App\Entity\Shipping;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;

/**
 * String
 *
 * @Entity()
 * @ORM\Table(name="shipping_ip_price")
 */
class ShippingIpPrice
{
    use IdTrait;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $priceAfterLast;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $distantion;

    /**
     * @ORM\Column(type="float")
     */
    protected $weightTo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @return mixed
     */
    public function getPriceAfterLast()
    {
        return $this->priceAfterLast;
    }

    /**
     * @param mixed $priceAfterLast
     */
    public function setPriceAfterLast($priceAfterLast): void
    {
        $this->priceAfterLast = $priceAfterLast;
    }

    /**
     * @return mixed
     */
    public function getDistantion()
    {
        return $this->distantion;
    }

    /**
     * @param mixed $distantion
     */
    public function setDistantion($distantion): void
    {
        $this->distantion = $distantion;
    }

    /**
     * @return mixed
     */
    public function getWeightTo()
    {
        return $this->weightTo;
    }

    /**
     * @param mixed $weightTo
     */
    public function setWeightTo($weightTo): void
    {
        $this->weightTo = $weightTo;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }
}
