<?php

namespace App\Entity\Shipping;

use App\Entity\Traits\TitleEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;

/**
 * String
 *
 * @Entity()
 * @ORM\Table(name="shipping_ip_block")
 */
class ShippingIpBlock
{
    use IdTrait;
    use TitleEmptyTrait;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hydroboard = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $countLoader = 0;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Shipping\ShippingIpPrice", cascade={"persist"})
     * @ORM\JoinTable(name="city_shipping_ip_block_price",
     *      joinColumns={@ORM\JoinColumn(name="block_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="shipping_price_id", referencedColumnName="id", nullable=true)}
     * )
     * @ORM\OrderBy({"weightTo" = "asc", "distantion" = "asc",  "priceAfterLast" = "asc"})
     */
    protected $prices;

    /**
     * @return mixed
     */
    public function getHydroboard()
    {
        return $this->hydroboard;
    }

    /**
     * @param mixed $hydroboard
     */
    public function setHydroboard($hydroboard): void
    {
        $this->hydroboard = $hydroboard;
    }

    /**
     * @return mixed
     */
    public function getCountLoader()
    {
        return $this->countLoader;
    }

    /**
     * @param mixed $countLoader
     */
    public function setCountLoader($countLoader): void
    {
        $this->countLoader = $countLoader;
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param mixed $prices
     */
    public function addPrice(ShippingIpPrice $price): void
    {
        $this->prices->add($price);
    }

    /**
     * @param mixed $prices
     */
    public function setPrices($prices): void
    {
        $this->prices = $prices;
    }
}
