<?php

namespace App\Entity;

use App\Entity\Traits;
use App\Model\PackageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;

/**
 * OrderPackage
 *
 * @Entity()
 * @ORM\Table(name="order_customer_package")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderPackage implements PackageInterface, \JsonSerializable
{
    use Traits\IdTrait;
    use Traits\TitleEmptyTrait;
    use Traits\SizeTrait;
    use Traits\WeightTrait;
    use Traits\PackingTrait;
    use Traits\PackageTypeTrait;
    use Traits\MoverWeightTrait;
    use Traits\MoverDamagedTrait;

    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = (int)$count;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'weight' => $this->getWeight(),
            'size' => $this->getSize(),
            'count' => $this->getCount(),
            'packing' => $this->getPacking(),
            'packageType' => $this->getPackageType(),
            'packageTypeAnother' => $this->getPackageTypeAnother(),

            'moverFirstMileDamaged' => $this->getMoverFirstMileDamaged(),
            'moverFirstMileWeight' => $this->getMoverFirstMileWeight(),
        ];
    }
}
