<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\UserTrait;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * DispatchLog
 *
 * @Entity()
 * @ORM\Table(name="dispatchLog")
 * @ORM\HasLifecycleCallbacks()
 */
class DispatchLog
{
    use IdTrait;
    use UserTrait;
    use TimestampableTrait;

    /**
     * event
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $event;

    /**
     * ip
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $ip;

    /**
     * email
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * page
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $page;

    const EVENT_SUBSCRIBE = 'subscribe';
    const EVENT_UNSUBSCRIBE = 'unsubscribe';

    public static function getEventList()
    {
        return [
            self::EVENT_SUBSCRIBE => 'Подписка',
            self::EVENT_UNSUBSCRIBE => 'Отписка',
        ];
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function setEvent($event)
    {
        $this->event = $event;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getUserFio()
    {
        return $this->getUser() ? $this->getUser()->getFirstName() : '';
    }

    public function getUserEmail()
    {
        return $this->getEmail() ? $this->getEmail() : $this->getUser()->getEmail();
    }

    public function getUserPhone()
    {
        return $this->getUser() ? $this->getUser()->getUsername() : '';
    }

    public function isRegister()
    {
        return $this->getUser() ? true : false;
    }

    public function isSubscribe()
    {
        return $this->getUser() ? $this->getUser()->isSubscribe() : $this->event == self::EVENT_SUBSCRIBE;
    }

    public function getUserGroups()
    {
        $groups = [];
        if ($this->getUser()) {
            $groups = $this->getUser()->getGroupNames();
        }
        return implode(',', $groups);
    }
    
    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getEvent() : ' ';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
