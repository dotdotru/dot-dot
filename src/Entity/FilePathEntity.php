<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\TitleEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * FilePathEntity
 *
 * @Entity()
 * @ORM\Table(name="file_path")
 * @ORM\HasLifecycleCallbacks()
 */
class FilePathEntity
{
    use IdTrait;
    use TitleEmptyTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $value;

    public function __construct($title = '', $value = '')
    {
        $this->setTitle($title);
        $this->setValue($value);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle().' '.$this->getValue();
    }
}
