<?php

namespace App\Entity;

use App\Entity\OrderInterface;

interface PaymentInterface
{
    /**
     * @param OrderInterface $order
     * @return mixed
     */
    public function setPayed(OrderInterface $order);

    /**
     * @param OrderInterface $order
     * @return mixed
     */
    public function checkPayment(OrderInterface $order);

    /**
     * @param OrderInterface $order
     * @return mixed
     */
    public function payOrder(OrderInterface $order);
}