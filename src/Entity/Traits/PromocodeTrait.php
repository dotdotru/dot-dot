<?php
namespace App\Entity\Traits;

use App\Entity\Promocode;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait PromocodeTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promocode", cascade={"persist"})
     * @ORM\JoinColumn(name="promocode_id", referencedColumnName="id")
     */
    protected $promocode;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $autoAddPromocode = false;

    public function getPromocode()
    {
        return $this->promocode;
    }

    public function setPromocode(Promocode $promocode = null)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isAutoAddPromocode()
    {
        return $this->autoAddPromocode;
    }

    /**
     * @param mixed $autoAddPromocode
     */
    public function setAutoAddPromocode($autoAddPromocode): void
    {
        $this->autoAddPromocode = $autoAddPromocode;
    }
}
