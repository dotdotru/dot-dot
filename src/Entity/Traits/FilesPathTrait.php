<?php
namespace App\Entity\Traits;

use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\FilePathEntity;
use Doctrine\ORM\Mapping as ORM;

trait FilesPathTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="FilePathEntity", cascade={"all"})
     */
    private $filePaths;

    public function getFilePaths()
    {
        return $this->filePaths;
    }

    public function addFile(FilePathEntity $filePath)
    {
        return $this->filePaths->add($filePath);
    }

    public function setFilePaths($filePaths)
    {
		$this->filePaths = [];
		foreach($filePaths as $name => $filePath) {
			$this->filePaths[] = new FilePathEntity($name, $filePath);
		}
    }
}
