<?php
namespace App\Entity\Traits;

use App\Entity\Packing;
use Doctrine\ORM\Mapping as ORM;

trait LocationTrait
{
    /**
     * @ORM\Column(type="string")
     */
    protected $lat;

    /**
     * @ORM\Column(type="string")
     */
    protected $lng;

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng()
    {
        return $this->lng;
    }

    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }
}
