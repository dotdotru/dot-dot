<?php

namespace App\Entity\Traits;

use App\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

trait OrderPersonTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", cascade={"persist"})
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id")
     */
    protected $receiver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", cascade={"persist"})
     * @ORM\JoinColumn(name="person_sender_id", referencedColumnName="id")
     */
    protected $sender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", cascade={"persist"})
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id")
     */
    protected $payer;

    /** @return Person */
    public function getReceiver()
    {
        return $this->receiver;
    }

    public function setReceiver(\App\Entity\Person $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /** @return Person */
    public function getSender()
    {
        return $this->sender;
    }

    public function setSender(\App\Entity\Person $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /** @return Person */
    public function getPayer()
    {
        return $this->payer;
    }

    public function setPayer(\App\Entity\Person $payer)
    {
        $this->payer = $payer;

        return $this;
    }
}
