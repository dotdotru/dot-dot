<?php
namespace App\Entity\Traits;

use App\Entity\Size;
use App\Entity\Stock;
use Doctrine\ORM\Mapping as ORM;

trait StockTrait
{
    /** @var Stock */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /** @return Stock|null */
    public function getStock()
    {
        return $this->stock;
    }

    public function setStock(Stock $stock)
    {
        $this->stock = $stock;

        return $this;
    }
}
