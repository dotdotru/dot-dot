<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Receiver;

trait ReceiverTrait
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Receiver", cascade={"persist"})
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id")
     */
    protected $receiver;

    public function getReceiver()
    {
        return $this->receiver;
    }

    public function setReceiver(\App\Entity\Receiver $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }
}
