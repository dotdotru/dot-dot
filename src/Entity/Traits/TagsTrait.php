<?php

namespace App\Entity\Traits;

use App\Entity\Content\Tag;
use Doctrine\ORM\Mapping as ORM;

trait TagsTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\Content\Tag", cascade={"persist"})
     */
    protected $tags;

    public function getTags()
    {
        return $this->tags;
    }

    public function addTag(Tag $tag)
    {
        return $this->tags->add($tag);
    }

    public function setTags($tags)
    {
        $this->tags->clear();
        foreach ($tags as $tag) {
            $this->tags->add($tag);
        }
    }
}