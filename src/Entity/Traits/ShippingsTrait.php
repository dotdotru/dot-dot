<?php
namespace App\Entity\Traits;

use App\Entity\Shipping;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

trait ShippingsTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Shipping", inversedBy="order", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="order_customer_shipping")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     * @ORM\OrderBy({"reject" = "asc", "type" = "desc"})
     */
    private $shippings;

    /**
     * @return Shipping[]
     */
    public function getShippings()
    {
        return $this->shippings;
    }

    public function removeShipping(Shipping $shipping = null)
    {
        if (!$shipping) {
            return true;
        }

        foreach ($this->shippings as $key => $shippingItem) {
            if ($shipping == $shippingItem) {
                $this->shippings->remove($key);
            }
        }

        return true;
    }

    public function getShipping($type)
    {
        // @todo подумать
        $shippings = array_reverse((array)$this->getShippings());

        /** @var Shipping $shipping */
        foreach ($this->getShippings() as $shipping) {
            if ($type == $shipping->getType() && false == $shipping->isReject()) {
                return $shipping;
            }
        }

        return null;
    }

    public function getShippingPickUp()
    {
        return $this->getShipping(Shipping::TYPE_PICKUP);
    }

    public function getShippingDeliver()
    {
        return $this->getShipping(Shipping::TYPE_DELIVER);
    }

    public function addShipping(Shipping $shipping)
    {
        $this->shippings->add($shipping);

        return $this;
    }

    public function setShippings(ArrayCollection $shippings = null)
    {
        $this->shippings = $shippings;

        return $this;
    }

    public function getTotalPriceShippings()
    {
        $totalPrice = 0;

        $shipping = $this->getShipping(Shipping::TYPE_PICKUP);
        if ($shipping) {
            $totalPrice += $shipping->getPrice();
        }

        $shipping = $this->getShipping(Shipping::TYPE_DELIVER);
        if ($shipping) {
            $totalPrice += $shipping->getPrice();
        }

        return $totalPrice;
    }
}
