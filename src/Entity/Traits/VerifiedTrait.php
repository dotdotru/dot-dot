<?php
namespace App\Entity\Traits;

use App\Entity\Organization\OrganizationDriver;
use Doctrine\ORM\Mapping as ORM;

trait VerifiedTrait
{
    /**
     * @ORM\Column(type="boolean")
     */
    protected $verified = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $verifiedDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $verifiedNotificationDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $verifiedCode;

    /**
     * Сбрасывает данные верефикации
     *
     * @return self
     */
    public function resetVerification(): self
    {
        $this->setVerified(false);
        $this->setVerifiedCode('');
        $this->setVerifiedDate(null);
        return $this;
    }

    public function getVerified()
    {
        return $this->verified;
    }

    public function isVerified()
    {
        return $this->verified;
    }

    public function isVerifiedSuccess()
    {
        return $this->verifiedCode === 'verified';
    }

    public function isVerifiedDelay()
    {
        return $this->verifiedCode === 'delay';
    }

    public function isVerifiedError()
    {
        return $this->verifiedCode === 'not_verified';
    }

    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    public function getVerifiedCode()
    {
        return $this->verifiedCode;
    }

    public function setVerifiedCode($verifiedCode)
    {
        $this->verifiedCode = $verifiedCode;

        if ($this->isVerifiedSuccess() && !$this->isVerified()) {
            $this->setVerified(true);
            $this->setVerifiedDate(new \DateTime());
        }

        return $this;
    }

    public function getVerifiedDate()
    {
        return $this->verifiedDate;
    }

    public function setVerifiedNotificationDate(\DateTime $verifiedNotificationDate = null)
    {
        $this->verifiedNotificationDate = $verifiedNotificationDate;

        return $this;
    }
    
    public function getVerifiedNotificationDate()
    {
        return $this->verifiedNotificationDate;
    }

    public function setVerifiedDate(\DateTime $verifiedDate = null)
    {
        $this->verifiedDate = $verifiedDate;

        return $this;
    }
}
