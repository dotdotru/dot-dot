<?php
namespace App\Entity\Traits;

use App\Entity\Size;
use Doctrine\ORM\Mapping as ORM;

trait SizeTrait
{
    public $width = 0;
    public $height = 0;
    public $depth = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $size;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $calculateWeight;

    public function getSize()
    {
        return $this->size;
    }

    public function getSizeObject()
    {
        return new Size($this->size, $this->calculateWeight);
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function setWidth($width)
    {
        $this->width = $width;

        $this->updateSize();

        return $this;
    }

    public function setHeight($height)
    {
        $this->height = $height;

        $this->updateSize();

        return $this;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;

        $this->updateSize();

        return $this;
    }

    public function updateSize()
    {
        $this->setSize($this->getDepth().'x'.$this->getWidth().'x'.$this->getHeight());
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getDepth()
    {
        return $this->depth;
    }

    public function setCalculateWeight($calculateWeight)
    {
        $this->calculateWeight = $calculateWeight;

        return $this;
    }

    public function getCalculateWeight()
    {
        return $this->calculateWeight;
    }
}
