<?php

namespace App\Entity\Traits;

trait PayedTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $payedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $payedNotificationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $needPayNotificationDate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $payed;

    public function isPayed()
    {
        return $this->payed === true;
    }

    public function setPayed($payed)
    {
        $this->payed = (bool)$payed;
    }

    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @return \DateTime
     */
    public function getPayedDate()
    {
        return $this->payedDate;
    }

    public function setPayedDate(\DateTime $payedDate = null)
    {
        $this->payedDate = $payedDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPayedNotificationDate()
    {
        return $this->payedNotificationDate;
    }

    public function setPayedNotificationDate(\DateTime $payedNotificationDate = null)
    {
        $this->payedNotificationDate = $payedNotificationDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getNeedPayNotificationDate()
    {
        return $this->needPayNotificationDate;
    }

    public function setNeedPayNotificationDate(\DateTime $needPayNotificationDate = null)
    {
        $this->needPayNotificationDate = $needPayNotificationDate;

        return $this;
    }
}