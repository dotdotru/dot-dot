<?php

namespace App\Entity\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;

trait PicturePreviewTrait
{
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="picture", referencedColumnName="id")
     * })
     */
    private $picturePreview;

    /**
     * Set picture
     *
     * @param Media $media
     * @return $this
     */
    public function setPicturePreview(Media $media = null)
    {
        $this->picturePreview = $media;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Media
     */
    public function getPicturePreview()
    {
        return $this->picturePreview;
    }
}