<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait CustomerDeliveryTime
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $customerDeliveryTime = '';

    /**
     * @return string
     */
    public function getCustomerDeliveryTime()
    {
        return $this->customerDeliveryTime;
    }

    /**
     * @param string $customerDeliveryTime
     */
    public function setCustomerDeliveryTime($customerDeliveryTime): void
    {
        $this->customerDeliveryTime = $customerDeliveryTime;
    }
}
