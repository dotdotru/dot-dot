<?php
namespace App\Entity\Traits;

use App\Entity\Organization\OrganizationDriver;
use Doctrine\ORM\Mapping as ORM;

trait InsuranceTrait
{
    /**
     * @ORM\Column(type="boolean")
     */
    protected $insured = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $insuredDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $insuredCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $insuredNumber;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $insuredDeclaredPrice;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $insuredPdf;


    public function getInsured()
    {
        return $this->insured;
    }

    public function isInsured()
    {
        return $this->insured;
    }

    public function isInsuredSuccess()
    {
        return $this->insuredCode == 'insured';
    }

    public function isInsuredError()
    {
        return $this->insuredCode == 'error';
    }

    public function isInsuredCancelled()
    {
        return $this->insuredCode == 'cancelled';
    }

    public function setInsured($insured)
    {
        $this->insured = $insured;

        return $this;
    }

    public function getInsuredCode()
    {
        return $this->insuredCode;
    }

    public function setInsuredCode($insuredCode)
    {
        $this->insuredCode = $insuredCode;

        if ($this->isInsuredSuccess() && !$this->isInsured()) {
            $this->setInsured(true);
            $this->setInsuredDate(new \DateTime());
        }

        if ($this->isInsuredCancelled()) {
            $this->setInsured(false);
        }

        return $this;
    }

    public function getInsuredDate()
    {
        return $this->insuredDate;
    }

    public function setInsuredDate(\DateTime $insuredDate = null)
    {
        $this->insuredDate = $insuredDate;

        return $this;
    }

    public function getInsuredNumber()
    {
        return $this->insuredNumber;
    }

    public function setInsuredNumber($insuredNumber)
    {
        $this->insuredNumber = $insuredNumber;

        return $this;
    }

    public function getInsuredPdf()
    {
        return $this->insuredPdf;
    }

    public function setInsuredPdf($insuredPdf)
    {
        $this->insuredPdf = $insuredPdf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInsuredDeclaredPrice()
    {
        return $this->insuredDeclaredPrice;
    }

    /**
     * @param mixed $insuredDeclaredPrice
     * @return InsuranceTrait
     */
    public function setInsuredDeclaredPrice($insuredDeclaredPrice)
    {
        $this->insuredDeclaredPrice = $insuredDeclaredPrice;
        return $this;
    }




}
