<?php

namespace App\Entity\Traits;

trait DirectionAddressTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="addressFrom", type="string", nullable=true)
     */
    protected $addressFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="addressTo", type="string", nullable=true)
     */
    protected $addressTo;

    /**
     * @return string
     */
    public function getAddressFrom()
    {
        return $this->addressFrom;
    }

    /**
     * @param string $addressFrom
     */
    public function setAddressFrom($addressFrom): void
    {
        $this->addressFrom = $addressFrom;
    }

    /**
     * @return string
     */
    public function getAddressTo()
    {
        return $this->addressTo;
    }

    /**
     * @param string $addressTo
     */
    public function setAddressTo($addressTo): void
    {
        $this->addressTo = $addressTo;
    }
}