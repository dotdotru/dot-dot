<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait DateRangeTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $dateActiveFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $dateActiveTo;

    /**
     * @return \DateTime
     */
    public function getDateActiveFrom()
    {
        return $this->dateActiveFrom;
    }

    public function setDateActiveFrom(\DateTime $dateTime = null)
    {
        $this->dateActiveFrom = $dateTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateActiveTo()
    {
        return $this->dateActiveTo;
    }

    public function setDateActiveTo(\DateTime $dateTime = null)
    {
        $this->dateActiveTo = $dateTime;

        return $this;
    }
}
