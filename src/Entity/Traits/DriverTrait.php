<?php

namespace App\Entity\Traits;

use App\Entity\Organization\OrganizationDriver;

trait DriverTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization\OrganizationDriver", cascade={"persist"})
     */
    protected $driver;

    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;

        return $this;
    }
}