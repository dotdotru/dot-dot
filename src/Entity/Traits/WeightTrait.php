<?php
namespace App\Entity\Traits;

use App\Entity\Size;
use Doctrine\ORM\Mapping as ORM;

trait WeightTrait
{
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }
}
