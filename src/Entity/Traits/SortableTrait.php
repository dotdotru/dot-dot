<?php

namespace App\Entity\Traits;

use Gedmo\Sortable\Sortable;

trait SortableTrait
{
    /**
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}