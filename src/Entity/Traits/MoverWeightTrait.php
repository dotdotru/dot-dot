<?php
namespace App\Entity\Traits;

use App\Entity\Size;
use Doctrine\ORM\Mapping as ORM;

trait MoverWeightTrait
{
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $moverFirstMileWeight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $moverLastMileWeight;

    /**
     * @return mixed
     */
    public function getMoverFirstMileWeight()
    {
        return $this->moverFirstMileWeight;
    }

    /**
     * @param mixed $moverFirstMileWeight
     */
    public function setMoverFirstMileWeight($moverFirstMileWeight): void
    {
        $this->moverFirstMileWeight = $moverFirstMileWeight;
    }

    /**
     * @return mixed
     */
    public function getMoverLastMileWeight()
    {
        return $this->moverLastMileWeight;
    }

    /**
     * @param mixed $moverLastMileWeight
     */
    public function setMoverLastMileWeight($moverLastMileWeight): void
    {
        $this->moverLastMileWeight = $moverLastMileWeight;
    }
}
