<?php
namespace App\Entity\Traits;

use App\Entity\Ad\Utm;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Stock;
use Doctrine\ORM\Mapping as ORM;

trait UtmTrait
{
    /** @var Utm
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad\Utm", cascade={"persist"})
     * @ORM\JoinColumn(name="utm_id", referencedColumnName="id")
     */
    protected $utm;

    /**
     * @return Utm
     */
    public function getUtm()
    {
        return $this->utm;
    }

    /**
     * @param Utm $utm
     */
    public function setUtm(Utm $utm = null): void
    {
        $this->utm = $utm;
    }
}
