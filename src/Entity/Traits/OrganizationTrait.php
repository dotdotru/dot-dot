<?php
namespace App\Entity\Traits;

use App\Entity\Organization\Organization;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait OrganizationTrait
{
    /** @var Organization $organization */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization\Organization", cascade={"persist"})
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;

    public function getOrganization()
    {
        return $this->organization;
    }

    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }
}
