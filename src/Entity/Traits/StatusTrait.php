<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait StatusTrait
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $status = '';

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if (in_array($status, $this->getStatusList())) {
            $this->status = $status;
        }

        return $this;
    }

    public static function getStatusList()
    {
        return [];
    }

    public function getPublicStatus()
    {
        return '';
    }
}
