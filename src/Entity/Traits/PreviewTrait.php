<?php

namespace App\Entity\Traits;


trait PreviewTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="preview", type="text", nullable=true)
     */
    protected $preview;

    /**
     * Set preview
     *
     * @param string $preview
     *
     * @return $this
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get preview
     *
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }
}