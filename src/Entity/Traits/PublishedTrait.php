<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait PublishedTrait
{
    /**
     * Публикация
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $published = false;

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     *
     * @return $this
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }
}
