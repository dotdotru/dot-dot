<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait ArriveDamagedTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $arriveDamaged = false;

    /**
     * @return mixed
     */
    public function getArriveDamaged()
    {
        return $this->arriveDamaged;
    }
    
    public function isArriveDamaged()
    {
        return $this->arriveDamaged;
    }

    /**
     * @param mixed $arriveDamaged
     */
    public function setArriveDamaged($arriveDamaged)
    {
        $this->arriveDamaged = $arriveDamaged;
    }
}
