<?php

namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Document\User;

trait UserTrait
{
    /** @var User */
    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** @return \App\Application\Sonata\UserBundle\Entity\User */
    public function getUser()
    {
        return $this->user;
    }

    public function setUser(\App\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}