<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait RemoveTrait
{
    /**
     * @ORM\Column(type="boolean")
     */
    protected $remove = false;

    public function isRemove()
    {
        return $this->remove;
    }

    public function setRemove($remove = true)
    {
        $this->remove = $remove;

        return $this;
    }
}
