<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait DamagedTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $damaged = false;

    public function isDamaged()
    {
        return $this->damaged;
    }

    /**
     * @return mixed
     */
    public function getDamaged()
    {
        return $this->damaged;
    }

    /**
     * @param mixed $damaged
     */
    public function setDamaged($damaged)
    {
        $this->damaged = $damaged;
    }
}
