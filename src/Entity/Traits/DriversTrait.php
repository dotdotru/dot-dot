<?php

namespace App\Entity\Traits;


use App\Entity\Organization\OrganizationDriver;

trait DriversTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Organization\OrganizationDriver", cascade={"persist"})
     */
    protected $drivers;

    public function getVerifiedDrivers()
    {
        $result = [];
        if ($this->drivers) {
            /** @var OrganizationDriver $driver */
            foreach ($this->drivers as $driver) {
                if ($driver->isVerified()) {
                    $result[] = $driver;
                }
            }
        }

        return $result;
    }

    public function getDrivers()
    {
        return $this->drivers;
    }

    public function addDriver(OrganizationDriver $driver)
    {
        $this->drivers->add($driver);

        return $this;
    }

    public function setDrivers($drivers)
    {
        $this->drivers = $drivers;

        return $this;
    }
}