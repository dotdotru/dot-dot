<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait LocalExternalIdEmptyTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="localExternalId", type="string", length=255, nullable=true)
     */
    protected $localExternalId;

    /**
     * Set localExternalId
     *
     * @param string $localExternalId
     *
     * @return $this
     */
    public function setLocalExternalId($localExternalId)
    {
        $this->localExternalId = $localExternalId;

        return $this;
    }

    /**
     * Get localExternalId
     *
     * @return string
     */
    public function getLocalExternalId()
    {
        return $this->localExternalId;
    }
}
