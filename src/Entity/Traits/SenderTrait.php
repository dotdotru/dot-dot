<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait SenderTrait
{
    /** @var User $sender */
    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    protected $user;

    /** @return User */
    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getCustomer()
    {
        return $this->user;
    }

    public function setCustomer(User $customer)
    {
        $this->user = $customer;

        return $this;
    }
}
