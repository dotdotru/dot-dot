<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait CarrierTrait
{
    /** @var User */
    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     */
    protected $carrier;

    public function getCarrier()
    {
        return $this->carrier;
    }

    public function setCarrier(\App\Application\Sonata\UserBundle\Entity\User $carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }
}
