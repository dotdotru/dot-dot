<?php

namespace App\Entity\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;

trait PictureTrait
{
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="picture", referencedColumnName="id")
     * })
     */
    private $picture;

    /**
     * Set picture
     *
     * @param Media $media
     * @return $this
     */
    public function setPicture(Media $media = null)
    {
        $this->picture = $media;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Media
     */
    public function getPicture()
    {
        return $this->picture;
    }
}