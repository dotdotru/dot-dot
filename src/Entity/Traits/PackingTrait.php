<?php
namespace App\Entity\Traits;

use App\Entity\Packing;
use Doctrine\ORM\Mapping as ORM;

trait PackingTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Packing", cascade={"persist"})
     * @ORM\JoinColumn(name="packing_id", referencedColumnName="id", nullable=true)
     */
    protected $packing;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $packingPrice;

    /** @return Packing */
    public function getPacking()
    {
        return $this->packing;
    }

    public function setPacking(Packing $packing = null)
    {
        $this->packing = $packing;

        return $this;
    }

    public function getPackingPrice()
    {
        return $this->packingPrice;
    }

    public function setPackingPrice($packingPrice)
    {
        $this->packingPrice = $packingPrice;

        return $this;
    }
}
