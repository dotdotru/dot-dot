<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait SynchTrait
{
    /**
     * Дата синхронизации
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $synchAt;

    /**
     * @return $this
     */
    public function setSynchAt(\DateTime $synchAt = null)
    {
        $this->synchAt = $synchAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSynchAt()
    {
        return $this->synchAt;
    }
}
