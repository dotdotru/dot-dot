<?php
namespace App\Entity\Traits;

use App\Entity\File;
use App\Entity\FileGroup;
use Doctrine\ORM\Mapping as ORM;

trait FilesTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="files", cascade={"persist","remove"})
     */
    protected $files;

    public function getFileByExtId($id)
    {
        foreach ($this->files as $file) {
            if ($file->getExternalId() == $id) {
                return $file;
            }
        }
        return null;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function addFile(File $file = null)
    {
        return $this->files->add($file);
    }

    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function removeFile(File $file)
    {
        foreach ($this->getFiles() as $key => $fileItem) {
            if ($fileItem->getId() == $file->getId()) {
                $this->files->remove($key);
            }
        }
    }

    public function replaceFile(File $file)
    {
        /** @var File $file */
        foreach ($this->getFiles() as $fileItem) {
            if ($fileItem->getGroup() == $file->getGroup() && $fileItem->getLocalExternalId() == $file->getLocalExternalId()) {
                $this->removeFile($fileItem);
            }
        }

        $this->addFile($file);

        return $this;
    }

    public function getFileByGroup($group)
    {
        $file = null;
        /** @var File $file */
        foreach ($this->getFiles() as $fileItem) {
            if (($fileItem->getGroup() === $group || $fileItem->getGroup()->getSlug() == $group) && $fileItem->isPublished()) {
                $file = $fileItem;
            }
        }

        return $file;
    }

    public function getFilesByGroup(FileGroup $group)
    {
        $files = [];
        /** @var File $file */
        foreach ($this->getFiles() as $file) {
            if ($file->getGroup() === $group && $file->isPublished()) {
                $files[] = $file;
            }
        }

        return $files;
    }

    public function getFilesWithoutGroups($groups)
    {
        $files = [];
        /** @var File $file */
        foreach ($this->getFiles() as $file) {
            $foundGroup = false;
            foreach ($groups as $group) {
                if ($file->getGroup() === $group) {
                    $foundGroup = true;
                }
            }

            if (!$foundGroup && $file->isPublished()) {
                $files[] = $file;
            }
        }

        return $files;
    }
}
