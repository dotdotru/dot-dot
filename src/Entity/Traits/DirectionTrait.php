<?php

namespace App\Entity\Traits;


trait DirectionTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    protected $direction;

    public function getDirection()
    {
        return $this->direction;
    }

    public function setDirection($direction)
    {
        $this->direction = $direction;
    }
}