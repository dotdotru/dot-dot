<?php

namespace App\Entity\Traits;

use App\Entity\Stock;

trait DirectionStockTrait
{
    /** @var Stock */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_from_id", referencedColumnName="id")
     */
    protected $stockFrom;

    /** @var Stock */
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", fetch="EAGER")
     * @ORM\JoinColumn(name="stock_to_id", referencedColumnName="id")
     */
    protected $stockTo;

    /** @return Stock|null */
    public function getStockFrom()
    {
        return $this->stockFrom;
    }

    public function setStockFrom(Stock $stockFrom)
    {
        $this->stockFrom = $stockFrom;

        return $this;
    }

    /** @return Stock|null */
    public function getStockTo()
    {
        return $this->stockTo;
    }

    public function setStockTo(Stock $stockTo)
    {
        $this->stockTo = $stockTo;

        return $this;
    }
}