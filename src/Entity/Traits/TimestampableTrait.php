<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    /**
     * Дата создания
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * Дата изменения
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
