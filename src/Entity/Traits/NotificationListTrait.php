<?php
namespace App\Entity\Traits;

use App\Entity\Notification\Notification;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

trait NotificationListTrait
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Notification\Notification", cascade={"persist"})
     */
    private $notifications;

    /**
     * @param string $group
     * @return Notification|false
     */
    public function existNotification($group)
    {
        foreach ($this->getNotifications() as $notification) {
            if ($notification->getGroup() && $notification->getGroup()->getExternalId() == $group) {
                return $notification;
            }
        }

        return false;
    }

    /**
     * @param string $group
     * @return Notification|null
     */
    public function removeNotification($group)
    {
        foreach ($this->notifications as $key => $notification) {
            if ($notification->getGroup() && $notification->getGroup()->getExternalId() == $group) {
                $this->notifications->remove($key);
            }
        }

        return null;
    }

    /**
     * @return ArrayCollection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Notification[]
     */
    public function setNotifications($notifications): void
    {
        $this->notifications = $notifications;
    }

    /**
     * @param Notification
     */
    public function addNotification(Notification $notification)
    {
        $this->notifications->add($notification);
    }
}
