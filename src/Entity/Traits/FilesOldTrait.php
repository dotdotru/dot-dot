<?php
namespace App\Entity\Traits;

use App\Entity\File;
use App\Entity\FileGroup;
use Doctrine\ORM\Mapping as ORM;

trait FilesOldTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="File", cascade={"all"})
     * * @ORM\JoinTable(name="page_old_file")
     */
    private $oldFiles;

    public function getOldFiles()
    {
        return $this->oldFiles;
    }

    public function addOldFile(File $file = null)
    {
        return $this->oldFiles->add($file);
    }

    public function setOldFiles($oldFiles)
    {
        $this->oldFiles = $oldFiles;
    }
}
