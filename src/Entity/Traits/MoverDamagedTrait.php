<?php
namespace App\Entity\Traits;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait MoverDamagedTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $moverFirstMileDamaged = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $moverLastMileDamaged = false;

    /**
     * @return mixed
     */
    public function getMoverFirstMileDamaged()
    {
        return $this->moverFirstMileDamaged;
    }

    /**
     * @param mixed $moverFirstMileDamaged
     */
    public function setMoverFirstMileDamaged($moverFirstMileDamaged): void
    {
        $this->moverFirstMileDamaged = $moverFirstMileDamaged;
    }

    /**
     * @return mixed
     */
    public function getMoverLastMileDamaged()
    {
        return $this->moverLastMileDamaged;
    }

    /**
     * @param mixed $moverLastMileDamaged
     */
    public function setMoverLastMileDamaged($moverLastMileDamaged): void
    {
        $this->moverLastMileDamaged = $moverLastMileDamaged;
    }
}
