<?php
namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait ExternalIdTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="externalId", type="string", length=255, unique=true)
     */
    protected $externalId;

    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }
}
