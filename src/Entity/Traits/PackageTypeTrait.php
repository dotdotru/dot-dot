<?php
namespace App\Entity\Traits;

use App\Entity\PackageType;
use App\Entity\Packing;
use Doctrine\ORM\Mapping as ORM;

trait PackageTypeTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PackageType", cascade={"persist"})
     * @ORM\JoinColumn(name="package_type_id", referencedColumnName="id", nullable=true)
     */
    protected $packageType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $packageTypeAnother = '';

    public function getPackageType()
    {
        return $this->packageType;
    }

    public function setPackageType(PackageType $packageType = null)
    {
        $this->packageType = $packageType;

        return $this;
    }

    public function getPackageTypeAnother()
    {
        return $this->packageTypeAnother;
    }

    public function setPackageTypeAnother($packageTypeAnother)
    {
        $this->packageTypeAnother = $packageTypeAnother;

        return $this;
    }
}
