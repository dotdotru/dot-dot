<?php

namespace App\Entity;

use App\Entity\Traits;
use App\Entity\Wms\AbstractWmsBatch;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;

/**
 * Batch
 *
 * @Entity(repositoryClass="App\Repository\BatchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BatchMile extends AbstractBatch implements \JsonSerializable
{

    const STATUS_NEW = 'new';
    const STATUS_RESERVED = 'reserved';
    const STATUS_WAITING = 'waiting';
    const STATUS_SHIPPING = 'in transit';
    const STATUS_NOT_PAID = 'not paid';
    const STATUS_PAYED = 'paid';

    const STATUS_REJECTED = 'rejected';


    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Зарезервировано' => self::STATUS_RESERVED,
            'Ожидает загрузки' => self::STATUS_WAITING,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_NOT_PAID,
            'Оплачено' => self::STATUS_PAYED,
            'Отказ' => self::STATUS_REJECTED,
        ];
    }


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Shipping", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="order_batch_mile_shipping")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     * @ORM\OrderBy({"reject" = "asc", "type" = "desc"})
     */
    private $shippings;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;



    public function __construct()
    {
        $this->shippings = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return ArrayCollection
     */
    public function getShippings()
    {
        return $this->shippings;
    }

    /**
     * @param ArrayCollection $shippings
     * @return BatchMile
     */
    public function setShippings($shippings)
    {
        $this->shippings = $shippings;
        return $this;
    }

    /**
     * Костыль, из-за того что хранится массив
     * @return Shipping|null
     */
    public function getShipping()
    {
        $shipping = null;
        if (count($this->getShippings()) && $this->getShippings()[0]) {
            $shipping = $this->getShippings()[0];
        }

        return $shipping;
    }


    /**
     * @param ArrayCollection $shippings
     * @return BatchMile
     */
    public function addShipping(Shipping $shipping)
    {
        $this->shippings[] = $shipping;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return BatchMile
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }


    public function isCreated()
    {
        return $this->getStatus() != self::STATUS_NEW;
    }

    public function isStopSendExactStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_WAITING]) && !$this->stopSend;
    }

    public function isEditStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_WAITING]) && !$this->stopSend;
    }


    public function isUploadDocuments()
    {
        return in_array($this->getStatus(), [self::STATUS_SHIPPING, self::STATUS_NOT_PAID]);
    }


    public function getTitleId()
    {
        $type = '';
        foreach ($this->getShippings() as $shipping) {
            $type = $shipping->getType();
        }
        $titleId = ($this->getOrder() ? $this->getOrder()->getId() : $this->getReserveId()) . ($type == Shipping::TYPE_PICKUP ? '-1' : '-L');

        return $titleId;
    }


    public function jsonSerialize()
    {
        $type = '';
        foreach ($this->getShippings() as $shipping) {
            $type = $shipping->getType();
        }

        $titleId = ($this->getOrder() ? $this->getOrder()->getId() : $this->getReserveId()) . ($type == Shipping::TYPE_PICKUP ? '-1' : '-L');

        $data = [
            'id'     => $this->getId(),
            'externalId'     => $this->getReserveId(),
            'title' => $titleId,
            'batchType' => $type,
            'addressFrom'=> '',
            'addressTo'=> '',
            'price' => $this->getPrice(),
            'realStatus' => $this->getStatus(),
            'status' => array_search($this->getStatus(), $this->getStatusList()),
            'files'     => $this->getFiles()->toArray(),
            'createdAt' => $this->getCreatedAt(),
            'drivers' => $this->getDrivers()->toArray(),
            'shippings' => $this->getShippings()->getValues(),
            'files'     => $this->getFiles()->toArray(),
            'isStopSendExactStatus' => $this->isStopSendExactStatus(),
            'isCreated' => $this->isCreated(),

            'type' => 'batch_mile',
            'isUploadDocuments' => $this->isUploadDocuments(),

            'isEdit' => $this->isEditStatus(),
        ];


        if ($this->getShipping() && $this->getShipping()->getPickAt()) {
            $pickAt = (clone $shipping->getPickAt())->modify('-1 days')->setTime(18, 0);
            $now = new DateTime();
            if ($now > $pickAt) {
                $data['isEdit'] = false;
            }
        }

        if ($this->getOrder()) {
            $data += [
                'orderPackages' => $this->getOrder()->getOrderPackages()->toArray(),
                'orderRealPackages' => $this->getOrder()->getPackages()->toArray(),
                'orderPackagesTypes' => $this->getOrder()->getPackagesTypes(),
                'orderTotalPackages' => $this->getOrder()->getTotalCount(),
                'orderTotalWeight' => $this->getOrder()->getTotalWeight(),
                'orderTotalVolume' => $this->getOrder()->getTotalVolume(),
            ];

            if ($type == Shipping::TYPE_PICKUP) {
                $data['sender'] = $this->getOrder()->getSender();
            }
            if ($type == Shipping::TYPE_DELIVER) {
                $data['receiver'] = $this->getOrder()->getReceiver();
            }
        }


        return $data;
    }
}
