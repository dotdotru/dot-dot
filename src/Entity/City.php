<?php

namespace App\Entity;

use App\Entity\Shipping\ShippingIpBlock;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Region;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;
use App\Entity\Shipping\ShippingIpPrice;
use App\Entity\Shipping\ShippingIpLoaderPrice;

use App\Entity\Traits;
use App\Entity\CityCode;
/**
 * City
 *
 * @Entity(repositoryClass="App\Repository\CityRepository")
 * @ORM\Table(name="city")
 * @ORM\HasLifecycleCallbacks()
 */
class City implements \JsonSerializable
{
    use Traits\IdTrait;
    use Traits\PublishedTrait;
    use Traits\TitleTrait;
    use Traits\TimestampableTrait;
    use Traits\SortableTrait;

    /**
     * @ORM\ManyToMany(targetEntity="CityCode", cascade={"persist"})
     * @ORM\JoinTable(name="city_codes",
     *      joinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_code_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $cityCodes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Shipping\ShippingIpBlock", cascade={"persist"})
     * @ORM\JoinTable(name="city_shipping_ip_block",
     *      joinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="block_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $priceBlocks;
    //* @ORM\OrderBy({"distantion" = "asc", "weightTo" = "asc"})

    /**
     * @ORM\ManyToMany(targetEntity="CityPricePerKilo", cascade={"persist"})
     * @ORM\JoinTable(name="city_prices_per_kilo",
     *      joinColumns={@ORM\JoinColumn(name="stock_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="price_per_kilo_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $pricesPerKilo;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region;

    public function __construct()
    {
        $this->pricesPerKilo = new ArrayCollection();
        $this->cityCodes = new ArrayCollection();
        $this->priceBlocks = new ArrayCollection();
    }

    public function getPricesPerKilo(City $city = null)
    {
        $result = [];
        if ($city && $this->pricesPerKilo) {
            foreach ($this->pricesPerKilo as $pricePerKilo) {
                if ($pricePerKilo->getCityTo()->getSlug() == $city->getSlug()) {
                    $result[] = $pricePerKilo;
                }
            }
        } else {
            $result = $this->pricesPerKilo;
        }
        return $result;
    }

    public function addPricesPerKilo(CityPricePerKilo $stockPricePerKilo)
    {
        $this->pricesPerKilo->add($stockPricePerKilo);

        return $this;
    }

    public function setPricesPerKilo(array $pricesPerKilo)
    {
        $this->pricesPerKilo = $pricesPerKilo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceBlocks($countLoader = null, $hydroboard = null)
    {
        $list = $this->priceBlocks;

        if ($countLoader !== null) {
            /**
             * @var int $key
             * @var ShippingIpBlock $priceBlock
             */
            foreach ($list as $key => $priceBlock) {
               if ($countLoader != $priceBlock->getCountLoader()) {
                   unset($list[$key]);
               }
            }
        }

        if ($hydroboard !== null) {
            /**
             * @var int $key
             * @var ShippingIpBlock $priceBlock
             */
            foreach ($list as $key => $priceBlock) {
                if ($hydroboard != $priceBlock->getHydroboard()) {
                    unset($list[$key]);
                }
            }
        }

        return $this->priceBlocks;
    }

    /**
     * @return mixed
     */
    public function getPriceBlock($countLoader = null, $hydroboard = null)
    {
        $list = $this->priceBlocks->toArray();

        if ($countLoader !== null) {
            /**
             * @var int $key
             * @var ShippingIpBlock $priceBlock
             */
            foreach ($list as $key => $priceBlock) {
                if ($countLoader != $priceBlock->getCountLoader()) {
                    unset($list[$key]);
                }
            }
        }

        if ($hydroboard !== null) {
            /**
             * @var int $key
             * @var ShippingIpBlock $priceBlock
             */
            foreach ($list as $key => $priceBlock) {
                if ($hydroboard != $priceBlock->getHydroboard()) {
                    unset($list[$key]);
                }
            }
        }

        return reset($list);
    }

    public function addPriceBlock(ShippingIpBlock $block)
    {
        $this->priceBlocks->add($block);
    }

    /**
     * @param mixed $priceBlocks
     */
    public function setPriceBlocks($priceBlocks): void
    {
        $this->priceBlocks = $priceBlocks;
    }

    public function getCityCode($code)
    {
        /** @var CityCode $cityCode */
        foreach ($this->cityCodes as $cityCode) {
            if ($cityCode->getCode() == $code) {
                return $cityCode->getValue();
            }
        }
        return null;
    }

    public function getCityCodes()
    {
        return $this->cityCodes;
    }

    public function addCityCode(CityCode $cityCode)
    {
        $this->cityCodes->add($cityCode);

        return $this;
    }

    public function setCityCodes(array $cityCodes)
    {
        $this->cityCodes = $cityCodes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion(Region $region): void
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'published'     => $this->isPublished(),
            'title'     => $this->getTitle(),
            'slug'     => $this->getSlug(),
            'position'     => $this->getPosition(),
            'region'     => $this->getRegion(),
        ];
    }
}
