<?php

namespace App\Entity\Wms;

use App\Entity\OrderInterface;
use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\WeightTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;
use JsonSerializable;

/**
 * WmsBatchPallet
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsBatchPalletRepository")
 * @ORM\Table(name="wms_batch_pallet", indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class WmsBatchPallet implements JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use DirectionTrait;
    use DirectionStockTrait;
    use WeightTrait;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsBatchPackage", inversedBy="packages", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="wms_batch_packages")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    private $packages;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deliverToAt;

    public function __construct()
    {
        $this->packages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @param mixed $packages
     */
    public function addPackage(WmsBatchPackage $package)
    {
        $this->packages->add($package);
    }

    /**
     * @param mixed $packages
     */
    public function setPackages($packages)
    {
        $this->packages = $packages;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    /**
     * @return DateTime
     */
    public function getDeliverToAt()
    {
        return $this->deliverToAt;
    }

    /**
     * @param DateTime $deliverToAt
     */
    public function setDeliverToAt(DateTime $deliverToAt): void
    {
        $this->deliverToAt = $deliverToAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return array(
            'id'     => $this->getId(),
            'externalId' => $this->getExternalId(),
            'direction' => $this->getDirection(),
            'stockFrom' => $this->getStockFrom(),
            'stockTo' => $this->getStockTo(),
            'packages' => $this->getPackages()->toArray(),
            'weight' => $this->getWeight(),
        );
    }

}
