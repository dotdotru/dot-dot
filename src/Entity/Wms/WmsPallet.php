<?php

namespace App\Entity\Wms;

use App\Entity\OrderInterface;
use App\Entity\Stock;
use App\Entity\StringEntity;
use App\Entity\Traits\DamagedTrait;
use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\WeightTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;
use JsonSerializable;

/**
 * WmsPallet
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsPalletRepository")
 * @ORM\Table(name="wms_pallet")
 * @ORM\HasLifecycleCallbacks()
 */
class WmsPallet implements JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;
    use DirectionTrait;
    use DirectionStockTrait;
    use WeightTrait;
    use DamagedTrait;

    const MAX_WEIGHT_PACKAGES = 1500;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $arriveWeight;

    public function getArriveWeight()
    {
        return $this->arriveWeight;
    }

    public function setArriveWeight($arriveWeight)
    {
        $this->arriveWeight = $arriveWeight;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wms\WmsBatch", cascade={"persist"})
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="id")
     */
    protected $batch;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsPackage", inversedBy="packages", cascade={"persist"})
     * @ORM\JoinTable(name="wms_pallet_packages")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    private $packages;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsPackage", inversedBy="packages", cascade={"persist"})
     * @ORM\JoinTable(name="wms_pallet_real_packages")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
    */
    private $realPackages;

    /**
     * @var \string
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $palletId;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deliverToAt;

    /**
     * @var \string
     * @ORM\Column(type="boolean")
     */
    protected $palleted = false;

    public function __construct()
    {
        $this->packages = new ArrayCollection();
        $this->realPackages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @param mixed $packages
     */
    public function addPackage(WmsPackage $package)
    {
        $this->packages->add($package);
    }

    /**
     * @param mixed $packages
     */
    public function setPackages($packages)
    {
        $this->packages = $packages;
    }

    /**
     * @return mixed
     */
    public function getRealPackages()
    {
        return $this->realPackages;
    }

    /**
     * @param mixed $packages
     */
    public function addRealPackage(WmsPackage $package)
    {
        $this->realPackages->add($package);
    }

    /**
     * @param mixed $packages
     */
    public function setRealPackages($packages)
    {
        $this->realPackages = $packages;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    /**
     * @return mixed
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @param mixed $batch
     */
    public function setBatch(WmsBatch $batch = null): void
    {
        $this->batch = $batch;
    }

    /**
     * @return DateTime
     */
    public function getDeliverToAt()
    {
		$deliverToAt = null;
		foreach ($this->getPackages() as $package) {
            if (!$deliverToAt || $deliverToAt > $package->getDeliveryToAt()) {
                $deliverToAt = $package->getDeliveryToAt();
            }
        }
        
        return $deliverToAt;
    }

    /**
     * @param DateTime $deliverToAt
     */
    public function setDeliverToAt(DateTime $deliverToAt = null): void
    {
        $this->deliverToAt = $deliverToAt;
    }

    /**
     * @return string
     */
    public function getPalletId()
    {
        return $this->palletId;
    }

    /**
     * @param string $palletId
     */
    public function setPalletId($palletId): void
    {
        $this->palletId = $palletId;
        $this->updateExternalId();
    }

    public function setStockFrom(Stock $stockFrom)
    {
        $this->stockFrom = $stockFrom;
        $this->updateExternalId();
        return $this;
    }

    public function updateExternalId()
    {
        if ($this->getStockFrom()) {
            $this->setExternalId($this->getStockFrom()->getExternalId().'-'.$this->getPalletId());
        }
    }

    public function getTotalVolume()
    {
        $totalVolume = 0;
        if ($this->getPackages()->count()) {
            /** @var WmsPackage $package */
            foreach ($this->getPackages() as $package) {
                $totalVolume += $package->getSizeObject()->getVolume();
            }
        }
        return $totalVolume;
    }

    /**
     * @return string
     */
    public function isPalleted()
    {
        return $this->palleted;
    }

    /**
     * @param string $palleted
     */
    public function setPalleted($palleted)
    {
        $this->palleted = $palleted;
    }

    public function getWeightPackages()
    {
        $totalWeight = 0;
        foreach ($this->getPackages() as $package) {
            $totalWeight += $package->getWeight();
        }

        return $totalWeight;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id'     => $this->getId(),
            'externalId'  => $this->getExternalId(),
            'palletId'  => $this->getPalletId(),
            'deliverToAt'  => $this->getDeliverToAt(),
            'direction' => $this->getDirection(),
            'stockFrom' => $this->getStockFrom(),
            'stockTo' => $this->getStockTo(),
            'packages' => $this->getPackages()->toArray(),
            'weight' => $this->getWeight(),
            'arriveWeight' => (float)$this->getArriveWeight(),
            'palleted' => $this->isPalleted(),
            'damaged' => $this->getDamaged(),
            'isShipping' => $this->getBatch() ? $this->getBatch()->isShipping() : null,
        ];
    }

}
