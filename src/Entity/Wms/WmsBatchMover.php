<?php

namespace App\Entity\Wms;

use App\Entity\Document;
use App\Entity\Order;
use App\Entity\OrderPackage;
use App\Entity\Pallet;
use App\Entity\Shipping;
use App\Entity\StringEntity;
use App\Entity\Traits;
use App\Service\IntegrationBaseService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;
use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;

/**
 * WmsBatch
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsBatchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WmsBatchMover extends AbstractWmsBatch implements \JsonSerializable
{

    /**
     * Список грузомест, принятых с милей
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsPackage", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="wms_batch_mover_packages")
     *
     */
    protected $packages;




    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shipping")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $shipping;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $paid;


    public function __construct()
    {
        parent::__construct();
        $this->packages = new ArrayCollection();
        $this->shippings = new ArrayCollection();
    }


    const STATUS_NEW = 'new';
    const STATUS_RESERVED = 'reserved';
    const STATUS_WAITING = 'waiting';
    const STATUS_SHIPPING = 'shipping';
    const STATUS_NOT_PAID = 'not paid';
    const STATUS_PAYED = 'paid';
    const STATUS_ARRIVED = 'arrived';

    const STATUS_REJECTED = 'rejected';


    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Зарезервировано' => self::STATUS_RESERVED,
            'Ожидает загрузки' => self::STATUS_WAITING,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_NOT_PAID,
            'Оплачено' => self::STATUS_PAYED,
            'Отказ' => self::STATUS_REJECTED,
        ];
    }


    public function getPublicStatus()
    {

        $publicStatus = 'Ожидает загрузки';
        if ($this->getStatus() == self::STATUS_READY || $this->getAcceptanceAt()) {
            $publicStatus = 'Ожидает загрузки';
        }

        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'Отказано';
        }
        if ($this->getStatus() == self::STATUS_SHIPPING) {
            $publicStatus =  'В пути';
        }
        if ($this->getStatus() == self::STATUS_NOT_PAID) {
            $publicStatus =  'Ожидает оплаты';

            if ((!$this->getFileByGroup('tn') || !$this->getFileByGroup('akty'))) {
                $publicStatus = $publicStatus.'*';
            }
        }
        if ($this->getStatus() == self::STATUS_ARRIVED) {
            $publicStatus =  'Ожидает оплаты';

            if ((!$this->getFileByGroup('tn') || !$this->getFileByGroup('akty'))) {
                $publicStatus = $publicStatus.'*';
            }
        }
        if ($this->getStatus() == self::STATUS_PAYED) {
            $publicStatus =  'Оплачено';
        }

        if ($this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        return $publicStatus;
    }


    
    public function isDamaged()
    {
        $damaged = false;

        foreach ($this->getPackages() as $package) {
            if ($package->getDamaged()) {
                $damaged = true;
            }
        }

        return $damaged;
    }

    public function getDamagedCount()
    {
        $damagedCount = 0;

        /** @var WmsPallet $pallet */
        foreach ($this->getPackages() as $package) {
            if ($package->getDamaged()) {
                $damagedCount++;
            }
        }

        return $damagedCount;
    }


    /**
     * @return ArrayCollection
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @param ArrayCollection $packages
     * @return WmsBatchMover
     */
    public function setPackages(ArrayCollection $packages): WmsBatchMover
    {
        $this->packages = $packages;
        return $this;
    }

    /**
     * @param ArrayCollection $package
     * @return WmsBatchMover
     */
    public function addPackage(WmsPackage $package): WmsBatchMover
    {
        $this->packages->add($package);
        return $this;
    }



    /**
     * @return mixed
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param mixed $shipping
     * @return WmsBatchMover
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     * @return WmsBatchMover
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return WmsBatchMover
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param mixed $paid
     * @return WmsBatchMover
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return |null
     */
    public function getStock ()
    {
        if ($this->getShipping()) {
            return $this->getShipping()->getStock();
        }

        return null;
    }


    /**
     * @return |null
     */
    public function getOrder ()
    {
        if ($this->getShipping()) {
            return $this->getShipping()->getOrder()[0];
        }

        return null;
    }


    public function getTitleId()
    {
        $titleSuffix = ($this->getShipping() && $this->getShipping()->getType() == Shipping::TYPE_PICKUP ? '-1' : '-L');
        $titleId = ($this->getOrder() ? $this->getOrder()->getId() : $this->getExternalId()) . $titleSuffix;

        return $titleId;
    }


    public function jsonSerialize()
    {

        // @todo исправить это
        $order = null;
        if ($this->getShipping()) {
            $order = $this->getShipping()->getOrder()[0];
        }

        $titleSuffix = ($this->getShipping() && $this->getShipping()->getType() == Shipping::TYPE_PICKUP ? '-1' : '-L');
        $titleId = ($this->getOrder() ? $this->getOrder()->getId() : $this->getExternalId()) . $titleSuffix;

        $wmsOrder = $this->getShipping()->getWmsOrder()[0];


        $data = [
            'id'     => $this->getId(),

            'externalId'     => $this->getExternalId(),
            'title' => $titleId,

            'carrier' => $this->getCarrier(),

            'price' => $this->getPrice(),
            'type' => $this->getType(),

            'status'     => $this->getPublicStatus(),
            'realStatus' => $this->getStatus(),
            'files'     => $this->getFiles()->toArray(),

            'entityType'     => AbstractWmsBatch::ENTITY_TYPE_BATCH_MOVER,
            'packages'     => $this->getPackages()->toArray(),

            'receptionAt'     => $this->getReceptionAt(),
            'issueAt'     => $this->getIssueAt(),
            'acceptanceAt' => $this->getAcceptanceAt(),
            'createdAt' => $this->getCreatedAt(),
            'shipping' => $this->getShipping(),

            'driver' => $this->getDriver(),

            'orderPackages'     => $order ? $order->getOrderPackages()->toArray() : [],
            'orderRealPackages'     => $order ? $order->getPackages()->toArray() : [],
            'orderPackagesTypes'     => $order ? $order->getPackagesTypes() : [],
            'orderTotalPackages'     => $order ? $order->getTotalCount() : 0,

            'orderTotalWeight'     => $order ? $order->getTotalWeight() : 0,
            'orderTotalVolume'     => $order ? $order->getTotalVolume() : 0,

            'orderId'     => $order ? $order->getId() : null,

            'receiver' => $order ? $order->getReceiver() : null,

            'deliverToAt' => null,
        ];

        $data['driverDocument'] = '';
        $data['driverName'] = '';
        $data['driverPhone'] = '';

        if ($this->getShipping() && $this->getShipping()->getDriver()) {
            /** @var OrganizationDriver $driver */
            $driver = $this->getShipping()->getDriver();

            $data['driverName'] = $driver->getName();
            $data['driverPhone'] = $driver->getPhone();

            /** @var Document $document */
            foreach ($driver->getDocuments() as $document) {
                if ($document->getType() == 'passport') {
                    $data['driverDocument'] = $document->getSeries() . $document->getNumber();
                }
                if ($document->getType() == 'license') {
                    $data['driverDocument'] = $document->getSeries() . ' ' . $document->getNumber();
                }
            }

        }

        $data['client'] = '';
        if ($this->getOrder() && $this->getOrder()->getUser()) {
            $user = $this->getOrder()->getUser();

            if ($user->isPrivatePerson()) {
                $contactName = $user->getFirstname();
                $contactPhone = $user->getUsername();
                $contactEmail = $user->getEmail();

                $contact = sprintf('%s, %s, %s', $contactName, $contactPhone, $contactEmail);
            } else {
                /** @var OrganizationRepository $organizationRepository */
                $organization = $user->getOrganizationCustomer();

                if ($organization) {
                    $contactName = $organization->getContactName();
                    $contactEmail = $user->getEmail();
                    $contactPhone = $organization->getContactPhone();

                    $contact = sprintf('%s, %s %s %s', $organization->getType() . $organization->getCompanyName(), $contactName, $contactPhone, $contactEmail);
                }
            }
            $data['client'] = $contact;
        }

       // $driverDocument = (isset($driverData['passport']['series']) && isset($driverData['passport']['number'])) ?
       // "{$driverData['passport']['series']} {$driverData['passport']['number']}" : '';


        return $data;
    }
}
