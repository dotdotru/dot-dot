<?php

namespace App\Entity\Wms;

use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\InsuranceTrait;
use App\Entity\Traits\PackageTypeTrait;
use App\Entity\Traits\PackingTrait;
use App\Entity\Traits\SenderTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\TitleEmptyTrait;
use App\Entity\Traits\WeightTrait;
use App\Model\PackageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Packing;

/**
 * WmsPackage
 *
 * @Entity()
 * @ORM\Table(name="wms_batch_package",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class WmsBatchPackage implements PackageInterface, \JsonSerializable
{
    use IdTrait;
    use TitleEmptyTrait;
    use ExternalIdEmptyTrait;
    use WeightTrait;
    use TimestampableTrait;
    use SizeTrait;
    use PackingTrait;
    use PackageTypeTrait;
    use StatusTrait;

    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = (int)$count;

        return $this;
    }

    CONST STATUS_ACCEPTED = 'accepted'; // Принят
    CONST STATUS_PREPARING = 'preparing'; // Подготовка к отгрузке
    CONST STATUS_INTRANSIT = 'in transit'; // Ожидает оплаты

    public static function getStatusList()
    {
        return [
            'Принято' => self::STATUS_ACCEPTED,
            'Подготовка к отгрузке' => self::STATUS_PREPARING,
            'В пути' => self::STATUS_INTRANSIT,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = '';
        if ($this->getStatus() == self::STATUS_ACCEPTED) {
            $publicStatus =  'принят';
        }
        if ($this->getStatus() == self::STATUS_PREPARING) {
            $publicStatus =  'подготовка к отгрузке';
        }
        if ($this->getStatus() == self::STATUS_INTRANSIT) {
            $publicStatus =  'в пути';
        }

        return $publicStatus;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getExternalId() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'weight' => $this->getWeight(),
            'size' => $this->getSize(),
            'count' => $this->getCount(),
            'packing' => $this->getPacking(),
            'packageType' => $this->getPackageType(),
            'packageTypeAnother' => $this->getPackageTypeAnother(),
            'status' => $this->getPublicStatus(),
        ];
    }
}
