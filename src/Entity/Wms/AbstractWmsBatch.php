<?php

namespace App\Entity\Wms;

use App\Entity\Pallet;
use App\Entity\StringEntity;
use App\Entity\Traits;
use App\Entity\Traits\ExternalIdEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;

/**
 * WmsBatch
 * @Entity(repositoryClass="App\Repository\Wms\AbstractWmsBatchRepository")
 * @ORM\Table(name="wms_batch",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorMap({
 *     "batch" = "WmsBatch",
 *     "mbatch" = "WmsBatchMover"
 * })
 */
abstract class AbstractWmsBatch
{
    const ENTITY_TYPE_BATCH_MOVER = 'mbatch';
    const ENTITY_TYPE_BATCH = 'batch';


    use Traits\TimestampableTrait;
    use Traits\DirectionTrait;
    use Traits\DirectionStockTrait;
    use Traits\CarrierTrait;
    use Traits\WeightTrait;
    use Traits\StatusTrait;
    use Traits\PayedTrait;
    use Traits\FilesTrait;
    use Traits\RemoveTrait;
    use Traits\OrganizationTrait;
    use Traits\DriverTrait;
    use Traits\DriversTrait;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="files", cascade={"persist","remove"})
     * @ORM\JoinTable(name="wms_batch_file")
     */
     protected $files;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxWeight;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sendDate;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reserveId;


    /**
     * Дата приему на склад
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $receptionAt;

    /**
     * Дата выдачи
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $issueAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $needPickPartyNotificationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateBatchDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $acceptanceAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatePayedDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $stopSend;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(name="externalId", type="string", length=255, nullable=true)
     */
    protected $externalId;



    public function __construct()
    {
        $this->pallets = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->drivers = new ArrayCollection();
    }

    const STATUS_NEW = 'new';
    const STATUS_RESERVED = 'reserved';
    const STATUS_READY = 'ready';
    const STATUS_REJECTED = 'rejected';
    const STATUS_SHIPPING = 'shipping';
    const STATUS_ARRIVED = 'arrived';
    const STATUS_PAYED = 'paid';
    const STATUS_SUSPENDED = 'suspended'; // Остановлен

    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Требует оформления' => self::STATUS_RESERVED,
            'Ожидает загрузки' => self::STATUS_READY,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_ARRIVED,
            'Оплачено' => self::STATUS_PAYED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = 'Ожидает загрузки';
        if ($this->getStatus() == self::STATUS_READY || $this->getAcceptanceAt()) {
            $publicStatus = 'Ожидает загрузки';
        }

        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'Отказано';
        }
        if ($this->getStatus() == self::STATUS_SHIPPING) {
            $publicStatus =  'В пути';
        }
        if ($this->getStatus() == self::STATUS_ARRIVED) {
            $publicStatus =  'Ожидает оплаты';
        }
        if ($this->getStatus() == self::STATUS_PAYED) {
            $publicStatus =  'Оплачено';
        }

        if ($this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        return $publicStatus;
    }

    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), []) && !$this->getAcceptanceAt()) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    public function getTotalWeight()
    {
        $totalWeight = $this->getWeight();
        return $totalWeight;
    }

    public function addPalletItem(WmsPallet $item)
    {
        $this->pallets->add($item);

        return $this;
    }

    public function setPallets($pallets)
    {
        $this->pallets = $pallets;

        return $this;
    }

    public function getCurrentPricePerKilo()
    {
        return $this->currentPricePerKilo;
    }

    public function setCurrentPricePerKilo($currentPricePerKilo)
    {
        $this->currentPricePerKilo = $currentPricePerKilo;

        return $this;
    }

    public function getReserveId()
    {
        return $this->reserveId;
    }

    public function setReserveId($reserveId)
    {
        $this->reserveId = $reserveId;

        return $this;
    }

    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    public function setMaxWeight($maxWeight)
    {
        $this->maxWeight= $maxWeight;

        return $this;
    }

    public function getMaxCount()
    {
        return $this->maxCount;
    }

    public function setMaxCount($maxCount)
    {
        $this->maxCount= $maxCount;

        return $this;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }

    public function setSendDate($sendDate)
    {
        if (!($sendDate instanceof DateTime)) {
            $sendDate = new DateTime($sendDate);
        }
        $this->sendDate = $sendDate;

        return $this;
    }


    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getId().'' : '11';
    }

    public function setReceptionAt(DateTime $dateTime)
    {
        return $this->receptionAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getReceptionAt()
    {
        return $this->receptionAt;
    }

    public function setIssueAt(DateTime $dateTime)
    {
        return $this->issueAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getIssueAt()
    {
        return $this->issueAt;
    }

    /**
     * @return \DateTime
     */
    public function getNeedPickPartyNotificationDate()
    {
        return $this->needPickPartyNotificationDate;
    }

    public function setNeedPickPartyNotificationDate(DateTime $dateTime)
    {
        return $this->needPickPartyNotificationDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateBatchDate()
    {
        return $this->updateBatchDate;
    }

    public function setUpdateBatchDate(DateTime $dateTime = null)
    {
        return $this->updateBatchDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getAcceptanceAt()
    {
        return $this->acceptanceAt;
    }

    public function setAcceptanceAt(DateTime $dateTime = null)
    {
        return $this->acceptanceAt = $dateTime;
    }

    public function isStopSend()
    {
        return $this->stopSend;
    }

    public function setStopSend($stopSend)
    {
        $this->stopSend = $stopSend;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function isNew()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW]);
    }

    public function isRemoveExactStatus()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW, self::STATUS_RESERVED]);
    }

    public function isSuspended()
    {
        return $this->stopSend;
    }

    public function isCreated()
    {
        return $this->getStatus() != self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->getStatus() == self::STATUS_ARRIVED;
    }

    public function isArrived()
    {
        return in_array($this->getStatus(), [self::STATUS_ARRIVED]);
    }

    public function isStopSendExactStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_READY]) && !$this->stopSend;
    }

    public function isReady()
    {
        return in_array($this->getStatus(), [self::STATUS_READY]);
    }

    /**
     * @return DateTime
     */
    public function getUpdatePayedDate()
    {
        return $this->updatePayedDate;
    }

    /**
     * @param DateTime $updatePayedDate
     */
    public function setUpdatePayedDate(DateTime $updatePayedDate = null): void
    {
        $this->updatePayedDate = $updatePayedDate;
    }

    public function setPayed($payed)
    {
        $this->setUpdatePayedDate(null);
        $this->payed = (bool)$payed;
    }

    /**
     * @return mixed
     */
    public function getOverload()
    {
        return $this->overload;
    }

    /**
     * @param mixed $overload
     */
    public function setOverload($overload): void
    {
        $this->overload = $overload;
    }

    public function isEdit()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_READY,
            self::STATUS_RESERVED
        ]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AbstractWmsBatch
     */
    public function setId(int $id): AbstractWmsBatch
    {
        $this->id = $id;
        return $this;
    }


    public function setPrice ($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice ()
    {
        return $this->price;
    }


    public function isPayed()
    {
        return $this->payed === true || $this->getStatus() == self::STATUS_PAYED;
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }


    public function jsonSerialize()
    {

    }
}
