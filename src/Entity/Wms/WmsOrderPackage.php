<?php

namespace App\Entity\Wms;

use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\InsuranceTrait;
use App\Entity\Traits\PackageTypeTrait;
use App\Entity\Traits\PackingTrait;
use App\Entity\Traits\SenderTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\TitleEmptyTrait;
use App\Entity\Traits\WeightTrait;
use App\Model\PackageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;

/**
 * WmsOrderPackage
 *
 * @Entity()
 * @ORM\Table(name="wms_order_package")
 * @ORM\HasLifecycleCallbacks()
 */
class WmsOrderPackage implements PackageInterface, \JsonSerializable
{
    use IdTrait;
    use TitleEmptyTrait;
    use SizeTrait;
    use WeightTrait;
    use PackingTrait;
    use PackageTypeTrait;

    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = (int)$count;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'weight' => $this->getWeight(),

            'size' => $this->getSize(),
            'count' => $this->getCount(),
            'packing' => $this->getPacking(),
            'packageType' => $this->getPackageType(),
            'packageTypeAnother' => $this->getPackageTypeAnother(),
        ];
    }
}
