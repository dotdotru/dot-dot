<?php

namespace App\Entity\Wms;

use App\Entity\Traits\ArriveDamagedTrait;
use App\Entity\Traits\DamagedTrait;
use App\Entity\Traits\DirectionStockTrait;
use App\Entity\Traits\DirectionTrait;
use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\InsuranceTrait;
use App\Entity\Traits\PackageTypeTrait;
use App\Entity\Traits\PackingTrait;
use App\Entity\Traits\SenderTrait;
use App\Entity\Traits\SizeTrait;
use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\TitleEmptyTrait;
use App\Entity\Traits\WeightTrait;
use App\Model\PackageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Packing;
use DoctrineExtensions\Query\Mysql\Date;

/**
 * WmsPackage
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsPackageRepository")
 * @ORM\Table(name="wms_package",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class WmsPackage implements PackageInterface, \JsonSerializable
{
	use IdTrait;
	use TitleEmptyTrait;
    use ExternalIdEmptyTrait;
    use WeightTrait;
    use TimestampableTrait;
    use SizeTrait;
    use PackingTrait;
    use PackageTypeTrait;
    use StatusTrait;
    use DamagedTrait;
    use ArriveDamagedTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wms\WmsOrder", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $palletId = '';

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $deliveryToAt;

    /**
     * Дата прибытия на склад
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $arrivedAt;

    /**
     * @return \DateTime
     */
    public function getDeliveryToAt()
    {
        return $this->deliveryToAt;
    }

    public function setDeliveryToAt(DateTime $dateTime = null)
    {
        $this->deliveryToAt = $dateTime;
    }

    /**
     * @return DateTime
     */
    public function getArrivedAt(): ?DateTime
    {
        return $this->arrivedAt;
    }

    /**
     * @param DateTime $arrivedAt
     * @return WmsPackage
     */
    public function setArrivedAt(DateTime $arrivedAt): ?WmsPackage
    {
        $this->arrivedAt = $arrivedAt;
        return $this;
    }


    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = (int)$count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPalletId()
    {
        return $this->palletId;
    }

    /**
     * @param mixed $palletId
     */
    public function setPalletId($palletId): void
    {
        $this->palletId = $palletId;
    }

    CONST STATUS_ACCEPTED = 'accepted'; // Принят
    CONST STATUS_PREPARING = 'preparing'; // Подготовка к отгрузке
    CONST STATUS_INTRANSIT = 'in transit'; // Ожидает оплаты

    public static function getStatusList()
    {
        return [
            'Принято' => self::STATUS_ACCEPTED,
            'Подготовка к отгрузке' => self::STATUS_PREPARING,
            'В пути' => self::STATUS_INTRANSIT,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = '';
        if ($this->getStatus() == self::STATUS_ACCEPTED) {
            $publicStatus =  'принят';
        }
        if ($this->getStatus() == self::STATUS_PREPARING) {
            $publicStatus =  'подготовка к отгрузке';
        }
        if ($this->getStatus() == self::STATUS_INTRANSIT) {
            $publicStatus =  'в пути';
        }

        return $publicStatus;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getExternalId() ?: '';
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }
    
    public function getGroupExternalId()
    {
        $realIndex = $this->getGroupIndex();
        
        if ($this->getCount() > 1) {
			$realIndex = $realIndex.'-'.($realIndex+$this->getCount() - 1);
		}

        return $realIndex;
    }
    
    public function getGroupIndex()
    {
        $realIndex = 0;
        if ($this->getOrder()) {
            foreach ($this->getOrder()->getGroupingPackages() as $index => $package) {
                if ($package->getId() == $this->getId()) {
                    return $realIndex + 1;
                }
                $realIndex = $realIndex + $package->getCount();
            }
        }

        return $realIndex;
    }

    public function getIndex()
    {
        $realIndex = $this->getId();
        if ($this->getOrder()) {
            foreach ($this->getOrder()->getPackages() as $index => $package) {
                if ($package->getId() == $this->getId()) {
                    $realIndex = $index + 1;
                }
            }
        }

        return $realIndex;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getExternalId()
    {
        if (!$this->externalId) {
            $this->externalId = $this->getTmpExternalId();
        }

        return $this->externalId;
    }

    public function getTmpExternalId()
    {
        $externalId = '';
        if ($this->getOrder()) {
            $externalId = $this->getOrder()->getExternalId().'/'.$this->getIndex();
        } else {
            $externalId = $this->getId();
        }

        return $externalId;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'externalId' => $this->getExternalId(),
            'palletId' => $this->getPalletId(),
            'title' => $this->getTitle(),
            'weight' => $this->getWeight(),
            'size' => $this->getSize(),
            'width' => $this->getSizeObject()->getWidth(),
            'height' => $this->getSizeObject()->getHeight(),
            'depth' => $this->getSizeObject()->getDepth(),
            'volume' => round($this->getSizeObject()->getVolume(), 4),
            'count' => $this->getCount(),
            'packing' => $this->getPacking(),
            'packingPrice' => $this->getPackingPrice(),
            'packageType' => $this->getPackageType(),
            'packageTypeAnother' => $this->getPackageTypeAnother(),
            'status' => $this->getPublicStatus(),
            'orderId' => $this->getOrder() ? $this->getOrder()->getExternalId() : '',
            'direction' => $this->getOrder() ? $this->getOrder()->getDirection() : null,
            'deliveryToAt' => $this->getDeliveryToAt(),
            'stockFrom' => $this->getOrder() ? $this->getOrder()->getStockFrom() : null,
            'stockTo' => $this->getOrder() ? $this->getOrder()->getStockTo() : null,
            'damaged' => $this->getDamaged(),
            'arriveDamaged' => $this->getArriveDamaged()
        ];
    }
}
