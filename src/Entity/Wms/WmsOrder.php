<?php

namespace App\Entity\Wms;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\OrderPackage;
use App\Entity\PackageType;
use App\Entity\Shipping;
use App\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Payment;
use App\Application\Sonata\MediaBundle\Entity\Media;
use JsonSerializable;
use App\Entity\Package;

/**
 * WmsOrder
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsOrderRepository")
 * @ORM\Table(name="wms_order",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class WmsOrder implements OrderInterface, JsonSerializable
{
    use Traits\IdTrait;
    use Traits\ExternalIdEmptyTrait;
    use Traits\TimestampableTrait;
    use Traits\DirectionTrait;
    use Traits\DirectionStockTrait;
    use Traits\StatusTrait;
    use Traits\PayedTrait;
    use Traits\InsuranceTrait;
    use Traits\SenderTrait;
    use Traits\OrderPersonTrait;
    use Traits\FilesTrait;
    use Traits\RemoveTrait;
    use Traits\WeightTrait;
    use Traits\OrganizationTrait;
    use Traits\PromocodeTrait;

    use Traits\ShippingsTrait;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Shipping", inversedBy="wmsOrder", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="wms_order_shipping")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     * @ORM\OrderBy({"reject" = "asc", "type" = "desc"})
     */
    private $shippings;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $tmp = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $first;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsPackage", inversedBy="packages", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="wms_order_packages")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    protected $packages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsOrderPackage", inversedBy="packages", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="wms_preorder_packages")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    protected $orderPackages;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $declaredPrice;

    /**
     * Дата приему на склад
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $receptionAt;

    /**
     * Дата выдачи
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $issueAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $suspendedPrice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $editRecevier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $stopSend;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pauseDelivery;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $minPrice;

    /**
     * @ORM\Column(type="float", nullable=true))
     */
    protected $maxPrice;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $suspendDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $palletId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $byCargo;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    protected $baseId;



    public function __construct()
    {
        $this->packages = new ArrayCollection();
        $this->orderPackages = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->shippings = new ArrayCollection();
    }


    CONST STATUS_NEW = 'new';
    CONST STATUS_BOOKED = 'booked'; // Требует оформления
    CONST STATUS_WAITING = 'waiting'; // Ожидается
    CONST STATUS_ACCEPTED = 'accepted'; // Принят
    CONST STATUS_REJECTED = 'rejected'; // Отказан
    CONST STATUS_PREPARING = 'preparing'; // Подготовка к отгрузке
    CONST STATUS_NOTPAID = 'not paid'; // Ожидает оплаты
    CONST STATUS_PAID = 'paid'; // Оплачено
    CONST STATUS_READY = 'ready'; // Готов к выдаче
    CONST STATUS_DELIVERED = 'delivered'; // Выдан
    CONST STATUS_SUSPENDED = 'suspended'; // Остановлен


    #const STATUS_SHIPPING = 'shipping';

    #const STATUS_CANCELED = 'canceled';

    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Требует оформления' => self::STATUS_BOOKED,
            'Ожидается' => self::STATUS_WAITING,
            'Принят' => self::STATUS_ACCEPTED,
            'Отказан' => self::STATUS_REJECTED,
            'Подготовка к отгрузке' => self::STATUS_PREPARING,
            'Ожидает оплаты' => self::STATUS_NOTPAID,
            'Оплачено' => self::STATUS_PAID,
            'Готов к выдаче' => self::STATUS_READY,
            'Выдан' => self::STATUS_DELIVERED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus =  'ожидается';
        if ($this->getStatus() == self::STATUS_WAITING) {
            $publicStatus =  'ожидается';
        }
        if ($this->getStatus() == self::STATUS_ACCEPTED) {
            $publicStatus =  'принят';
        }
        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'отказан';
        }
        if ($this->getStatus() == self::STATUS_PREPARING) {
            $publicStatus =  'подготовка к отгрузке';
        }
        if ($this->getStatus() == self::STATUS_NOTPAID) {
            $publicStatus =  'ожидает оплаты';
        }
        if ($this->getStatus() == self::STATUS_PAID) {
            $publicStatus =  'оплачено';
        }
        if ($this->getStatus() == self::STATUS_READY) {
            $publicStatus =  'готов к выдаче';
        }
        if ($this->getStatus() == self::STATUS_DELIVERED) {
            $publicStatus =  'выдан';

        	if ($this->getOrder()) {
				$shipping = $this->getOrder()->getShipping(Shipping::TYPE_DELIVER);
				if ($shipping && $shipping->isActive() && !$this->getFileByGroup('act')) {
					$publicStatus =  'выдан*';
				}
			}
        }

        if ($this->isPauseDelivery() || $this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        //if ($this->isStopSend()) {
        //    $publicStatus =  'отказ от отправления';
        //}

        return $publicStatus;
    }

    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_BOOKED,
            self::STATUS_NOTPAID,
            self::STATUS_PAID,
        ])) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    public function getGroupingPackages()
    {
        $groupPackages = [];
        $found = false;
        foreach ($this->getPackages() as $package) {
            $package->setCount(1);
            $found = false;
            foreach ($groupPackages as &$groupPackage) {
                if (
                    $package->getSize() == $groupPackage->getSize()
                    && $package->getWeight() == $groupPackage->getWeight()
                    && $package->getPacking() == $groupPackage->getPacking()
                    && $package->getPackingPrice() == $groupPackage->getPackingPrice()
                    && $package->getPackageType() && $groupPackage->getPackageType() && $package->getPackageType()->getId() == $groupPackage->getPackageType()->getId()
                    && $package->getPackageTypeAnother() == $groupPackage->getPackageTypeAnother()
                    && $package->getPalletId() == $groupPackage->getPalletId()
                ) {
                    $found = true;
                    $groupPackage->setCount($groupPackage->getCount() + 1);
                    break;
                }
            }

            if (!$found) {
                $groupPackages[] = $package;
            }
        }

        return $groupPackages;
    }

    public function getDeliveryToAt()
    {
        $deliveryToAt = null;
        /** @var WmsPackage $package */
        foreach ($this->getPackages() as $package) {
            if ($deliveryToAt == null || $deliveryToAt > $package->getDeliveryToAt()) {
                $deliveryToAt = $package->getDeliveryToAt();
            }
        }
        return $deliveryToAt;
    }


    /**
     * Проверяет все грузы и если они доставлены, возвращает дату доставки последнего груза
     *
     * @return \DateTime
     */
    public function getArrivedAt(): ?DateTime
    {
        $arrivedAt = null;
        $packages = $this->getPackages();

        foreach ($packages as $package) {
            if ($package->getArrivedAt()) {
                if (!$arrivedAt || $arrivedAt < $package->getArrivedAt()) {
                    $arrivedAt  = $package->getArrivedAt();
                }
            } else {
                // Если есть недоставленный груз, дата поступления на склад неизвестна
                return null;
            }
        }

        return $arrivedAt;
    }


    public function getPackages()
    {
        foreach ($this->packages as &$package) {
            $package->setCount(1);
        }
        return $this->packages;
    }

    public function getPackagesTypes()
    {
        $result = [];
        if ($this->getPackages()->count() > 0) {
            foreach ($this->getPackages() as $package) {
                $typeTitle = $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother();
                $result[$typeTitle] = $typeTitle;
            }
        } else {
            foreach ($this->orderPackages as $package) {
                $typeTitle = $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother();
                $result[$typeTitle] = $typeTitle;
            }
        }

        return array_values($result);
    }

    public function getTotalPackages()
    {
        $count = 0;

        $packages = $this->getPackages();

        if ($packages->count()) {
            /** @var WmsPackage $package */
            foreach ($packages as $package) {
                $count += $package->getCount() ? $package->getCount() : 1;
            }
        } else if($this->orderPackages->count()) {
            /** @var WmsOrderPackage $package */
            foreach ($this->orderPackages as $package) {
                $count += $package->getCount() ? $package->getCount() : 1;
            }
        }

        return $count;
    }

    public function getPackagesGroupStatus()
    {
        $result = [];
        /** @var Package $package */
        foreach ($this->getPackages() as $package) {
            if (!isset($result[$package->getStatus()])) {
                $result[$package->getStatus()] = ['count' => 0, 'status' => $package->getPublicStatus()];
            }
            $result[$package->getStatus()]['count']++;
        }

        return array_values($result);
    }

    public function addPackage(WmsPackage $item)
    {
        $this->packages->add($item);

        return $this;
    }

    public function setPackages(ArrayCollection $packages = null)
    {
        $this->packages = $packages;

        return $this;
    }

    public function getOrderPackages()
    {
        return $this->orderPackages;
    }

    public function addOrderPackage(WmsOrderPackage $item)
    {
        $this->orderPackages->add($item);

        return $this;
    }

    public function setOrderPackages(ArrayCollection $orderPackages = null)
    {
        $this->orderPackages = $orderPackages;

        return $this;
    }

    public function getDeclaredPrice()
    {
        return $this->declaredPrice;
    }

    public function setDeclaredPrice($declaredPrice)
    {
        $this->declaredPrice = $declaredPrice;

        return $this;
    }

    public function getTotalVolume()
    {
        $volume = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $volume += $package->getSizeObject()->getVolume();
            } else {
                $volume += $package->getSizeObject()->getVolume() * $package->getCount();
            }
        }

        if ($volume <= 0) {
            $volume = 0;
            foreach ($this->getOrderPackages() as $orderPackage) {
                if ($this->isByCargo()) {
                    $volume += $orderPackage->getSizeObject()->getVolume();
                } else {
                    $volume += $orderPackage->getSizeObject()->getVolume() * $orderPackage->getCount();
                }
            }
        }

        return $volume;
    }

    public function getTotalWeight()
    {
        $weight = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $weight += $package->getWeight();
            } else {
                $weight += $package->getWeight() * $package->getCount();
            }
        }

        if ($this->getPackages()->count() == 0) {
            $weight = 0;
            foreach ($this->getOrderPackages() as $package) {
                if ($this->isByCargo()) {
                    $weight += $package->getWeight();
                } else {
                    $weight += $package->getWeight() * $package->getCount();
                }
            }
        }

        return $weight;
    }

    public function getTotalCount()
    {
        $count = 0;
        foreach ($this->getPackages() as $package) {
            $count += $package->getCount();
        }

        return $count;
    }

    public function setPrice($price)
    {
        return $this->price = $price;
    }

    public function getPrice()
    {
        return ceil($this->price);
    }

    public function setEditRecevier($editRecevier)
    {
        return $this->editRecevier = $editRecevier;
    }

    public function getEditRecevier()
    {
        return $this->editRecevier;
    }

    public function setSuspendedPrice($suspendedPrice)
    {
        return $this->suspendedPrice = $suspendedPrice;
    }

    public function getSuspendedPrice()
    {
        return $this->suspendedPrice;
    }

    public function getTotalPricePackage()
    {
        $packingPrice = 0;
        foreach ($this->getPackages() as $package) {
            if ($this->isByCargo()) {
                $packingPrice += $package->getPackingPrice() / $package->getCount();
            }
            else {
                $packingPrice += $package->getPackingPrice();
            }
        }

        if (count($this->getPackages()) == 0) {
            $packingPrice = 0;
            /** @var OrderPackage $orderPackage */
            foreach ($this->getOrderPackages() as $orderPackage) {
                if ($this->isByCargo()) {
                    $packingPrice += $orderPackage->getPackingPrice();
                }
                else {
                    $packingPrice += $orderPackage->getPackingPrice() / $orderPackage->getCount();
                }
            }
        }

        return round($packingPrice);
    }

    public function setReceptionAt(DateTime $dateTime)
    {
        return $this->receptionAt = $dateTime;
    }

    /**::L
     * @return \DateTime
     */
    public function getReceptionAt()
    {
        return $this->receptionAt;
    }

    public function setIssueAt(DateTime $dateTime)
    {
        return $this->issueAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getIssueAt()
    {
        return $this->issueAt;
    }

    public function setSuspendDate(DateTime $dateTime = null)
    {
        return $this->suspendDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getSuspendDate()
    {
        return $this->suspendDate;
    }

    public function getHash()
    {
        $hash = sha1($this->getId().''.$this->getExternalId().''.$this->getUser());
        return $hash;
    }

    public function isStopSend()
    {
        return $this->stopSend;
    }

    public function setStopSend($stopSend)
    {
        $this->setSuspendDate(null);
        $this->stopSend = $stopSend;
    }

    public function isPauseDelivery()
    {
        return $this->pauseDelivery;
    }

    public function setPauseDelivery($pauseDelivery)
    {
        $this->setSuspendDate(null);
        $this->pauseDelivery = $pauseDelivery;
    }

    public function getMinPrice()
    {
        return ceil($this->minPrice);
    }

    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getMaxPrice()
    {
        return ceil($this->maxPrice);
    }

    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPalletId()
    {
        return $this->palletId;
    }

    /**
     * @param mixed $palletId
     */
    public function setPalletId($palletId): void
    {
        $this->palletId = $palletId;
    }

    public function isDamaged()
    {
        $damaged = false;

        /** @var WmsPackage $package */
        foreach ($this->getPackages() as $package) {
            if ($package->isDamaged()) {
                $damaged = true;
            }
        }

        return $damaged;
    }

    public function isArriveDamaged()
    {
        $damaged = false;

        /** @var WmsPackage $package */
        foreach ($this->getPackages() as $package) {
            if ($package->isArriveDamaged()) {
                $damaged = true;
            }
        }

        return $damaged;
    }

    public function getDamagedCount()
    {
        $damagedCount = 0;

        /** @var WmsPackage $package */
        foreach ($this->getPackages() as $package) {
            if ($package->isDamaged()) {
                $damagedCount++;
            }
        }

        return $damagedCount;
    }

    public function getArriveDamagedCount()
    {
        $damagedCount = 0;

        /** @var WmsPackage $package */
        foreach ($this->getPackages() as $package) {
            if ($package->isArriveDamaged()) {
                $damagedCount++;
            }
        }

        return $damagedCount;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId().'';
    }

    public function isSuspended()
    {
        return in_array($this->getStatus(), [self::STATUS_SUSPENDED]) || $this->isPauseDelivery() || $this->isStopSend() ;
    }

    public function isNew()
    {
        return in_array($this->getStatus(), [self::STATUS_BOOKED, self::STATUS_NEW]);
    }

    public function isEditStatus()
    {
        $shippingPickupDone = true;
        if ($this->getOrder()) {
            $shipping = $this->getOrder()->getShipping(Shipping::TYPE_PICKUP);

            if ($shipping && $shipping->isActive() && !$shipping->getDone()) {
                $shippingPickupDone = false;
            }
        }

        return in_array($this->getStatus(), ['', self::STATUS_BOOKED, self::STATUS_NEW, self::STATUS_WAITING]) && $shippingPickupDone;
    }

    public function isRemoveExactStatus()
    {
        return $this->isNew() || $this->isCreated();
    }

    public function isStopSendExactStatus()
    {
        return !$this->isFinished() && !$this->stopSend && !$this->isNew();
    }

    public function isCreated()
    {
        return in_array($this->getStatus(), [self::STATUS_BOOKED]);
    }

    public function isWaiting()
    {
        return in_array($this->getStatus(), [self::STATUS_WAITING]);
    }

    public function isFinished()
    {
        return in_array($this->getStatus(), [self::STATUS_DELIVERED, self::STATUS_SUSPENDED]);
    }

    public function isPauseDeliveryExactStatus()
    {
        return !in_array($this->getStatus(), []) && !$this->isCreated() && !$this->isNew() && !$this->pauseDelivery;
    }

    public function isShipped()
    {
        return in_array($this->getStatus(), [self::STATUS_DELIVERED]);
    }

    public function getPayment()
    {

    }

    public function isFirst()
    {
        return $this->first;
    }

    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * @return mixed
     */
    public function isByCargo()
    {
        return $this->byCargo;
    }

    /**
     * @param mixed $byCargo
     */
    public function setByCargo($byCargo)
    {
        $this->byCargo = $byCargo;
    }

    /**
     * @return string
     */
    public function getBaseId(): string
    {
        return $this->baseId;
    }

    /**
     * @param string $baseId
     * @return WmsOrder
     */
    public function setBaseId(string $baseId): WmsOrder
    {
        $this->baseId = $baseId;
        return $this;
    }





    /**
     * @return mixed
     */
    public function isTmp()
    {
        return $this->tmp;
    }

    /**
     * @param mixed $tmp
     */
    public function setTmp($tmp): void
    {
        if (!$tmp) {
            $tmp = null;
        }
        $this->tmp = $tmp;
    }

    public function isShowRealPrice()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NOTPAID,
            self::STATUS_PAID,
            self::STATUS_READY,
            self::STATUS_DELIVERED,
            self::STATUS_SUSPENDED
        ]);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->setWeight($this->getTotalWeight());
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->setWeight($this->getTotalWeight());
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        $shippingPickUp = $this->getShipping(Shipping::TYPE_PICKUP);
        $shippingPickUpJson = $shippingPickUp ? $shippingPickUp->jsonSerialize() : null;

        $shippingDeliver = $this->getShipping(Shipping::TYPE_DELIVER);
        $shippingDeliverJson = $shippingDeliver ? $shippingDeliver->jsonSerialize() : null;


        $data = [
            'id'     => $this->getId(),
            'externalId' => $this->getExternalId(),
            'user' => $this->getUser() ? $this->getUser()->jsonSerialize() : null,
            'userPerson'=> $this->getUser() ? $this->getUser()->getPerson() : null,
            'sender'=> $this->getSender(),
            'receiver'=> $this->getReceiver(),
            'payer'=> $this->getPayer(),
            'customer'=> $this->getCustomer(),
            'organization'=> $this->getOrganization(),
            'editRecevier'=> $this->getEditRecevier(),
            'stockFrom'=> $this->getStockFrom(),
            'stockTo'=> $this->getStockTo(),
            'direction'     => $this->getDirection(),
            'status'     => $this->getPublicStatus(),
            'statusClass'     => $this->getPublicStatusClass(),
            'payed'     => $this->getPayed(),
            'totalPackages'     => $this->getTotalPackages(),
            'totalWeight'     => ceil($this->getTotalWeight()),
            'totalVolume'     => ceil($this->getTotalVolume() * 1000) / 1000,
            'totalPricePackage' => ceil($this->getTotalPricePackage()),
            'price'     => $this->getPrice(),
            'minPrice'     => $this->getMinPrice(),
            'maxPrice'     => $this->getMaxPrice(),
            'declaredPrice'     => (float)$this->getDeclaredPrice(),
            'suspendedPrice'     => $this->getSuspendedPrice(),

            'insuredNumber'     => $this->getInsuredNumber(),
            'insuredPdf'     => $this->getInsuredPdf(),

            'packages'     => $this->getPackages()->toArray(),
            //'groupPackages'     => $this->getGroupPackages(),
            'packagesTypes'     => $this->getPackagesTypes(),
            'packagesGroupStatus'     => $this->getPackagesGroupStatus(),
            'orderPackages'     => $this->getOrderPackages()->toArray(),

            'receptionAt'     => $this->getReceptionAt(),
            'issueAt'     => $this->getIssueAt(),
            'arrivedAt'   => $this->getArrivedAt(),

            'isDamaged'     => $this->isDamaged(),
            'isArriveDamaged'     => $this->isArriveDamaged(),


            'realStatus'     => $this->getStatus(),
            'isEditStatus'     => $this->isEditStatus(),
            'isStopSendExactStatus'     => $this->isStopSendExactStatus(),
            'isRemoveExactStatus'     => $this->isRemoveExactStatus(),
            'isPauseDeliveryExactStatus'     => $this->isPauseDeliveryExactStatus(),
            'isShipped'     => $this->isShipped(),
            'isNew'     => $this->isNew(),
            'isCreated'     => $this->isCreated(),
            'isWaiting'     => $this->isWaiting(),

            'isStopSend'     => $this->isStopSend(),
            'isPauseDelivery'     => $this->isPauseDelivery(),

            'isShowRealPrice' => $this->isShowRealPrice(),

            'files'     => $this->getFiles()->toArray(),

            'price_add' => null,

            'pickUp' => $shippingPickUpJson,

            'deliver' => $shippingDeliverJson,
            'totalPriceShipping' => $this->getTotalPriceShippings(),

            'shippings'     => [
                'pickUp' => $shippingPickUpJson,
                'deliver' => $shippingDeliverJson,
            ],
        ];

        return $data;


    }

}
