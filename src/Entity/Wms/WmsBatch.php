<?php

namespace App\Entity\Wms;

use App\Entity\Pallet;
use App\Entity\StringEntity;
use App\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;

/**
 * WmsBatch
 *
 * @Entity(repositoryClass="App\Repository\Wms\WmsBatchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WmsBatch extends AbstractWmsBatch implements \JsonSerializable
{


    /**
     * @ORM\Column(type="float", nullable=true)
    */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $priceVat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Wms\WmsPallet", cascade={"persist"})
     * @ORM\JoinTable(name="wms_batch_pallets",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="pallet_id", referencedColumnName="id", nullable=true)}
     * )
     */
    protected $pallets;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxWeight;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $maxCount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $currentPricePerKilo;


    /**
     * @ORM\Column(type="boolean")
     */
    protected $overload = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $needPickPartyNotificationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateBatchDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $acceptanceAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatePayedDate;




    public function __construct()
    {
        parent::__construct();
        $this->pallets = new ArrayCollection();

        $this->drivers = new ArrayCollection();
    }

    const STATUS_NEW = 'new';
    const STATUS_RESERVED = 'reserved';
    const STATUS_READY = 'ready';
    const STATUS_REJECTED = 'rejected';
    const STATUS_SHIPPING = 'shipping';
    const STATUS_ARRIVED = 'arrived';
    const STATUS_PAYED = 'paid';
    const STATUS_SUSPENDED = 'suspended'; // Остановлен

    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Требует оформления' => self::STATUS_RESERVED,
            'Ожидает загрузки' => self::STATUS_READY,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_ARRIVED,
            'Оплачено' => self::STATUS_PAYED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }

    public function getPublicStatus()
    {
        $publicStatus = 'Ожидает загрузки';
        if ($this->getStatus() == self::STATUS_READY || $this->getAcceptanceAt()) {
            $publicStatus = 'Ожидает загрузки';
        }

        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'Отказано';
        }
        if ($this->getStatus() == self::STATUS_SHIPPING) {
            $publicStatus =  'В пути';
        }
        if ($this->getStatus() == self::STATUS_ARRIVED) {
            $publicStatus =  'Ожидает оплаты';
        }
        if ($this->getStatus() == self::STATUS_PAYED) {
            $publicStatus =  'Оплачено';
        }

        if ($this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        return $publicStatus;
    }

    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), []) && !$this->getAcceptanceAt()) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    public function setPrice($price)
    {
        return $this->price = $price;
    }

    public function getPrice()
    {
        if (!$this->price) {
            return ceil($this->getTotalWeight() * ($this->getCurrentPricePerKilo() / 100));
        }

        return ceil($this->price);
    }



    public function getDeliveryToAt()
    {
        $deliveryToAt = null;
        /** @var WmsPallet $pallet */
        foreach ($this->getPallets() as $pallet) {
            if ($deliveryToAt == null || $deliveryToAt > $pallet->getDeliverToAt()) {
                $deliveryToAt = $pallet->getDeliverToAt();
            }
        }
        return $deliveryToAt;
    }

    public function getTotalWeight()
    {
        $totalWeight = $this->getWeight();
        return $totalWeight;
    }

    public function getTotalVolume()
    {
        $totalVolume = 0;
        if ($this->getPallets()->count()) {
            foreach ($this->getPallets() as $pallet) {
                $totalVolume += $pallet->getTotalVolume();
            }
        }
        return $totalVolume;
    }

    public function getTotalPallets()
    {
        $totalPallets = 0;
        if ($this->getPallets()->count()) {
            $totalPallets = $this->getPallets()->count();
        }

        return $totalPallets;
    }

    public function getPallets()
    {
        return $this->pallets;
    }

    public function addPalletItem(WmsPallet $item)
    {
        $this->pallets->add($item);

        return $this;
    }

    public function setPallets($pallets)
    {
        $this->pallets = $pallets;

        return $this;
    }

    public function getCurrentPricePerKilo()
    {
        return $this->currentPricePerKilo;
    }

    public function setCurrentPricePerKilo($currentPricePerKilo)
    {
        $this->currentPricePerKilo = $currentPricePerKilo;

        return $this;
    }

    public function getReserveId()
    {
        return $this->reserveId;
    }

    public function setReserveId($reserveId)
    {
        $this->reserveId = $reserveId;

        return $this;
    }

    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    public function setMaxWeight($maxWeight)
    {
        $this->maxWeight= $maxWeight;

        return $this;
    }

    public function getMaxCount()
    {
        return $this->maxCount;
    }

    public function setMaxCount($maxCount)
    {
        $this->maxCount= $maxCount;

        return $this;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }

    public function setSendDate($sendDate)
    {
        if (!($sendDate instanceof DateTime)) {
            $sendDate = new DateTime($sendDate);
        }
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getId().'' : '11';
    }

    public function setReceptionAt(DateTime $dateTime)
    {
        return $this->receptionAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getReceptionAt()
    {
        return $this->receptionAt;
    }

    public function setIssueAt(DateTime $dateTime)
    {
        return $this->issueAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getIssueAt()
    {
        return $this->issueAt;
    }

    /**
     * @return \DateTime
     */
    public function getNeedPickPartyNotificationDate()
    {
        return $this->needPickPartyNotificationDate;
    }

    public function setNeedPickPartyNotificationDate(DateTime $dateTime)
    {
        return $this->needPickPartyNotificationDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateBatchDate()
    {
        return $this->updateBatchDate;
    }

    public function setUpdateBatchDate(DateTime $dateTime = null)
    {
        return $this->updateBatchDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getAcceptanceAt()
    {
        return $this->acceptanceAt;
    }

    public function setAcceptanceAt(DateTime $dateTime = null)
    {
        return $this->acceptanceAt = $dateTime;
    }

    public function isStopSend()
    {
        return $this->stopSend;
    }

    public function setStopSend($stopSend)
    {
        $this->stopSend = $stopSend;
    }

    public function isDamaged()
    {
        $damaged = false;

        /** @var WmsPallet $pallet */
        foreach ($this->getPallets() as $pallet) {
            if ($pallet->getDamaged()) {
                $damaged = true;
            }
        }

        return $damaged;
    }

    public function getDamagedCount()
    {
        $damagedCount = 0;

        /** @var WmsPallet $pallet */
        foreach ($this->getPallets() as $pallet) {
            if ($pallet->getDamaged()) {
                $damagedCount++;
            }
        }

        return $damagedCount;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function isNew()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW]);
    }

    public function isRemoveExactStatus()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW, self::STATUS_RESERVED]);
    }

    public function isSuspended()
    {
        return $this->stopSend;
    }

    public function isCreated()
    {
        return $this->getStatus() != self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->getStatus() == self::STATUS_ARRIVED;
    }

    public function isArrived()
    {
        return in_array($this->getStatus(), [self::STATUS_ARRIVED]);
    }

    public function isStopSendExactStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_READY]) && !$this->stopSend;
    }

    /**
     * @return DateTime
     */
    public function getUpdatePayedDate()
    {
        return $this->updatePayedDate;
    }

    /**
     * @param DateTime $updatePayedDate
     */
    public function setUpdatePayedDate(DateTime $updatePayedDate = null): void
    {
        $this->updatePayedDate = $updatePayedDate;
    }

    public function setPayed($payed)
    {
        $this->setUpdatePayedDate(null);
        $this->payed = (bool)$payed;
    }

    /**
     * @return mixed
     */
    public function getOverload()
    {
        return $this->overload;
    }

    /**
     * @param mixed $overload
     */
    public function setOverload($overload): void
    {
        $this->overload = $overload;
    }

    public function isEdit()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_READY,
            self::STATUS_RESERVED
        ]);
    }

    /**
     * @return mixed
     */
    public function getPriceVat()
    {
        return $this->priceVat;
    }

    /**
     * @param mixed $priceVat
     */
    public function setPriceVat($priceVat): void
    {
        $this->priceVat = $priceVat;
    }


    public function isShipping()
    {
        return in_array($this->getStatus(), [
            self::STATUS_SHIPPING
        ]);
    }

    public function isPayed()
    {
        return $this->payed === true || $this->getStatus() == self::STATUS_PAYED;
    }

    public function jsonSerialize()
    {
        return array(
            'id'     => $this->getId(),
            'externalId'     => $this->getExternalId(),
            'carrier' => $this->getCarrier(),
            'weight'=> $this->getWeight(),
            'stockFrom'=> $this->getStockFrom(),
            'stockTo'=> $this->getStockTo(),
            'direction'     => $this->getDirection(),
            'status'     => $this->getPublicStatus(),
            'realStatus'     => $this->getStatus(),
            'statusClass'     => $this->getPublicStatusClass(),
            'payed'     => $this->isPayed(),

            'sendDate'     => $this->getSendDate(),

            'receptionAt'     => $this->getReceptionAt(),
            'issueAt'     => $this->getIssueAt(),

            'price' => $this->getPrice(),

            'totalWeight' => $this->getTotalWeight(),
            'totalPallets' => $this->getTotalPallets(),

            'isDamaged'     => $this->isDamaged(),

            'isFinished'     => $this->isFinished(),
            'isSuspended'     => $this->isSuspended(),
            'isNew'     => $this->isNew(),
            'isShipping' => $this->isShipping(),
            'isCreated'     => $this->isCreated(),
            'isStopSendExactStatus'     => $this->isStopSendExactStatus(),
            'isRemoveExactStatus'     => $this->isRemoveExactStatus(),
            'isEdit'     => $this->isEdit(),
            'overload'     => $this->getOverload(),
            'drivers'     => $this->getDrivers()->toArray(),
            'files'     => $this->getFiles()->toArray(),
            'pallets'     => $this->getPallets()->toArray(),
            'acceptanceAt' => $this->getAcceptanceAt(),
            'createdAt' => $this->getCreatedAt(),
            'entityType'     => AbstractWmsBatch::ENTITY_TYPE_BATCH,
        );

    }
}
