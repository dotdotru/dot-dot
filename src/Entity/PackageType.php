<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\SizeTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * PackageType
 *
 * @Entity(repositoryClass="App\Repository\PackageTypeRepository")
 * @ORM\Table(name="package_type",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class PackageType implements \JsonSerializable
{
	use IdTrait;
    use PublishedTrait;
    use TitleTrait;
    use ExternalIdEmptyTrait;
    use TimestampableTrait;

    public function __construct()
    {
        $this->packingsRecommended = new ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="Packing", cascade={"persist"})
     * @ORM\JoinTable(name="package_type_packings_recommended",
     *      joinColumns={@ORM\JoinColumn(name="package_type_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="packing_id", referencedColumnName="id", nullable=true)}
     * )
     */
    private $packingsRecommended;

    /**
     * @ORM\Column(type="boolean")
     */
    private $packingRequired;

    /**
     * @return bool
     */
    public function isPackingRequired()
    {
        return $this->packingRequired;
    }

    /**
     * @param bool $packingRequired
     *
     * @return $this
     */
    public function setPackingRequired($packingRequired)
    {
        $this->packingRequired = $packingRequired;

        return $this;
    }

    public function getPackingsRecommended()
    {
        return $this->packingsRecommended;
    }

    public function addItem(Packing $packingRecommended)
    {
        $this->packingsRecommended->add($packingRecommended);

        return $this;
    }

    public function setPackingsRecommended(ArrayCollection $packingsRecommended)
    {
        $this->packingsRecommended = $packingsRecommended;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle() ?: '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
        ];
    }
}
