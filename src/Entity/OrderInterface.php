<?php

namespace App\Entity;

interface OrderInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return float
     */
    public function getPrice();

    /**
     * @return bool
     */
    public function isPayed();

    /**
     * @return Payment|null
     */
    public function getPayment();

    /**
     * @return Package[]|null
     */
    public function getPackages();

}