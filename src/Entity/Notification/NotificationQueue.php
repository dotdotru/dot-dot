<?php

namespace App\Entity\Notification;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\TimestampableTrait;

/**
 * @Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class NotificationQueue
{

    use TimestampableTrait;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $payload = [];


    /**
     * @ORM\Column(type="boolean")
     */
    protected $processed = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return NotificationQueue
     */
    public function setId(int $id): NotificationQueue
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return NotificationQueue
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getPayload(): ?array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     * @return NotificationQueue
     */
    public function setPayload(?array $payload): NotificationQueue
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->processed;
    }

    /**
     * @param bool $processed
     * @return NotificationQueue
     */
    public function setProcessed(bool $processed): NotificationQueue
    {
        $this->processed = $processed;
        return $this;
    }



    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

}
