<?php

namespace App\Entity\Notification;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * Notification
 *
 * @Entity()
 * @ORM\Table(name="notification")
 * @ORM\HasLifecycleCallbacks()
 */
class Notification
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Notification\NotificationGroup", cascade={"persist"})
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $leadTime;

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param NotificationGroup|null $group
     */
    public function setGroup(NotificationGroup $group = null)
    {
        $this->group = $group;
    }

    /**
     * @return DateTime
     */
    public function getLeadTime(): DateTime
    {
        return $this->leadTime;
    }

    /**
     * @param DateTime $leadTime
     */
    public function setLeadTime(DateTime $leadTime = null): void
    {
        $this->leadTime = $leadTime;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            '%s %s',
            ($this->getGroup() ? $this->getGroup()->getTitle() : ''),
            ($this->getLeadTime() ? $this->getLeadTime()->format('d.m.Y H:i:s') : '')
        );
    }
}
