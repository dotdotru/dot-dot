<?php

namespace App\Entity;

use App\Entity\Traits\FilesTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\SortableTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\MediaBundle\Entity\Media;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;


/**
 * FileGroup
 *
 * @Entity(repositoryClass="App\Repository\FileGroupRepository")
 * @ORM\Table(name="file_group")
 * @ORM\HasLifecycleCallbacks()
 */
class FileGroup implements \JsonSerializable
{
    use IdTrait;
    use TitleTrait;
    use TimestampableTrait;
    use SortableTrait;

    use FilesTrait;

    /**
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $defaultCustomer;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $defaultCarrier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $defaultWmsOrder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $defaultWmsBatch;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $showTitle;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $showList;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hidden;
    
    /**
     * @return bool
     */
    public function isShowTitle()
    {
        return $this->showTitle;
    }

    /**
     * @param bool $showTitle
     *
     * @return $this
     */
    public function setShowTitle($showTitle)
    {
        $this->showTitle = $showTitle;

        return $this;
    }
    
    /**
     * @return bool
     */
    public function isShowList()
    {
        return $this->showList;
    }

    /**
     * @param bool $showList
     *
     * @return $this
     */
    public function setShowList($showList)
    {
        $this->showList = $showList;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultCustomer()
    {
        return $this->defaultCustomer;
    }

    /**
     * @param bool $defaultCustomer
     *
     * @return $this
     */
    public function setDefaultCustomer($defaultCustomer)
    {
        $this->defaultCustomer = $defaultCustomer;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultCarrier()
    {
        return $this->defaultCarrier;
    }

    /**
     * @param bool $published
     *
     * @return $this
     */
    public function setDefaultCarrier($defaultCarrier)
    {
        $this->defaultCarrier = $defaultCarrier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultWmsOrder()
    {
        return $this->defaultWmsOrder;
    }

    /**
     * @param mixed $defaultWmsOrder
     */
    public function setDefaultWmsOrder($defaultWmsOrder): void
    {
        $this->defaultWmsOrder = $defaultWmsOrder;
    }

    /**
     * @return mixed
     */
    public function getDefaultWmsBatch()
    {
        return $this->defaultWmsBatch;
    }

    /**
     * @param mixed $defaultWmsBatch
     */
    public function setDefaultWmsBatch($defaultWmsBatch): void
    {
        $this->defaultWmsBatch = $defaultWmsBatch;
    }

    /**
     * @return mixed
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     */
    public function setHidden($hidden): void
    {
        $this->hidden = $hidden;
    }


    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getTitle() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'showTitle' => $this->isShowTitle(),
            'showList' => $this->isShowList(),
            'slug' => $this->getSlug(),
            'position' => $this->getPosition(),
            'hidden' => $this->isHidden(),
        ];
    }
}
