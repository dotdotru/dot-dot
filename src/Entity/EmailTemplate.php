<?php

namespace App\Entity;

use App\Entity\Traits\ExternalIdEmptyTrait;
use App\Entity\Traits\FilesTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\FileGroup;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

/**
 * EmailTemplate
 *
 * @Entity(repositoryClass="App\Repository\EmailTemplateRepository")
 * @ORM\Table(name="email")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailTemplate implements \JsonSerializable
{
    use IdTrait;
    use ExternalIdEmptyTrait;
    use PublishedTrait;
    use TitleTrait;
    use FilesTrait;

    use TimestampableTrait;

    /**
     * @ORM\Column(type="string")
     */
    protected $subject;

    /**
     * @ORM\Column(type="text")
     */
    protected $body;


    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? (string)$this->getId() : '';
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle()
        ];
    }
}
