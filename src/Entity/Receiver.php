<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Receiver
 * @Entity()
 * @ORM\Table(name="receiver")
 * @ORM\HasLifecycleCallbacks()
 */
class Receiver implements \JsonSerializable
{
    const ISSUANCE_PASSPORT = 'passport';
    const ISSUANCE_CODE = 'code';

    use IdTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $uridical;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $inn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $series;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $issuance;

    /**
     * @ORM\Column(type="string")
     */
    protected $contactName;

    /**
     * @ORM\Column(type="string")
     */
    protected $contactPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $contactEmail;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $companyName;


    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function isUridical()
    {
        return $this->uridical;
    }

    public function getUridical()
    {
        return $this->uridical;
    }

    public function setUridical($uridical)
    {
        $this->uridical = $uridical;

        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setSeries($series)
    {
        $this->series = $series;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getIssuance()
    {
        return $this->issuance;
    }

    /**
     * @param mixed $issuance
     */
    public function setIssuance($issuance): void
    {
        $this->issuance = $issuance;
    }

    public function getInn()
    {
        return $this->inn;
    }

    public function setInn($inn)
    {
        $this->inn = $inn;
    }

    public function getContactName()
    {
        return $this->contactName;
    }

    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $name = '';
        if ($this->isUridical()) {
            $name = sprintf('%s %s %s %s', $this->getType(), $this->getCompanyName(), $this->getContactName(), $this->getContactPhone());
        } else {
            $name = sprintf('%s %s', $this->getContactName(), $this->getContactPhone());
        }
        return $name;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function jsonSerialize()
    {
        return [
			'id' => $this->getId() ?? '',
            'type' => $this->getType() ?? '',
            'uridical' => $this->getUridical(),
            'inn' => $this->getInn() ?? '',
            'number'=> $this->getNumber() ?? '',
            'series'     => $this->getSeries() ?? '',
            'issuance' => $this->getIssuance() ?? '',
            'code' => $this->getCode() ?? '',
            'contactName'     => $this->getContactName() ?? '',
            'contactPhone'     => $this->getContactPhone() ?? '',
            'contactEmail'     => $this->getContactEmail() ?? '',
            'companyName' => $this->getCompanyName() ?? '',
        ];
    }
}
