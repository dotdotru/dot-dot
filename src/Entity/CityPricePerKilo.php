<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use App\Repository\AppRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\ExternalIdTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PublishedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\TitleTrait;

/**
 * CityPricePerKilo
 *
 * @Entity(repositoryClass="App\Repository\CityPricePerKiloRepository")
 * @ORM\Table(name="city_price_per_kilo")})
 * @ORM\HasLifecycleCallbacks()
 */
class CityPricePerKilo
{
	use IdTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(type="float")
     */
    protected $weightFrom;

    /**
     * @ORM\Column(type="float")
     */
    protected $weightTo;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    protected $cityTo;

    public function getWeightFrom()
    {
        return $this->weightFrom;
    }

    public function setWeightFrom($weightFrom)
    {
        $this->weightFrom = $weightFrom;

        return $this;
    }

    public function getWeightTo()
    {
        return $this->weightTo;
    }

    public function setWeightTo($weightTo)
    {
        $this->weightTo = $weightTo;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getCityTo()
    {
        return $this->cityTo;
    }

    public function setCityTo(City $cityTo)
    {
        $this->cityTo = $cityTo;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
