<?php

namespace App\Entity;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Traits;
use App\Entity\Traits\ExternalIdEmptyTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\DBAL\Schema\Index;

use Symfony\Component\Validator\Constraints\Date;

use App\Entity\Organization\OrganizationDriver;

use App\Entity\File;



/**
 * Batch
 *
 * @Entity(repositoryClass="App\Repository\BatchRepository")
 * @ORM\Table(name="uorder",indexes={@ORM\Index(name="externalId", columns={"externalId"})})
 * @ORM\HasLifecycleCallbacks()
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorMap({
 *     "batch" = "Batch",
 *     "mbatch" = "BatchMile"
 * })
 */
abstract class AbstractBatch implements \JsonSerializable
{
    use Traits\TimestampableTrait;
    use Traits\FilesTrait;
    use Traits\PayedTrait;
    use Traits\RemoveTrait;
    use Traits\UtmTrait;


    const STATUS_NEW = 'new';
    const STATUS_RESERVED = 'reserved';
    const STATUS_READY = 'ready';
    const STATUS_REJECTED = 'rejected';
    const STATUS_SHIPPING = 'shipping';
    const STATUS_ARRIVED = 'arrived';
    const STATUS_PAYED = 'paid';
    const STATUS_SUSPENDED = 'suspended'; // Остановлен


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Organization\OrganizationDriver", cascade={"persist"})
     */
    protected $drivers;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /** @var User */
    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     */
    protected $carrier;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $status = '';


    /**
     * @var string
     *
     * @ORM\Column(name="externalId", type="string", length=255, nullable=true)
     */
    protected $externalId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sendDate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $priceVat;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reserveId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $needPickPartyNotificationDate;

     /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateBatchDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $acceptanceAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatePayedDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $stopSend;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;




    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->drivers = new ArrayCollection();
    }


    public static function getStatusList()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Ожидает загрузки' => self::STATUS_READY,
            'Отказано' => self::STATUS_REJECTED,
            'В пути' => self::STATUS_SHIPPING,
            'Ожидает оплаты' => self::STATUS_ARRIVED,
            'Оплачено' => self::STATUS_PAYED,
            'Остановлен' => self::STATUS_SUSPENDED,
        ];
    }

    public function getPublicStatus()
    {
        if (true || $this->getOverload()) {
            $publicStatus = 'Требует подтверждения';
            if ($this->getStatus() == self::STATUS_RESERVED) {
                $publicStatus = 'Требует подтверждения';
            }
        } else {
            $publicStatus = 'Требует оформления';
            if ($this->getStatus() == self::STATUS_RESERVED) {
                $publicStatus = 'Требует оформления';
            }
        }

        if ($this->getStatus() == self::STATUS_READY || $this->getAcceptanceAt()) {
            $publicStatus = 'Ожидает загрузки';
        }

        if ($this->getStatus() == self::STATUS_REJECTED) {
            $publicStatus =  'Отказано';
        }
        if ($this->getStatus() == self::STATUS_SHIPPING) {
            $publicStatus =  'В пути';
        }
        if ($this->getStatus() == self::STATUS_ARRIVED) {
            $publicStatus =  'Ожидает оплаты';
        }
        if ($this->getStatus() == self::STATUS_PAYED) {
            $publicStatus =  'Оплачено';
        }

        if ($this->isStopSend() || $this->getStatus() == self::STATUS_SUSPENDED) {
            $publicStatus =  'остановлен';
        }

        return $publicStatus;
    }

    public function getPublicStatusClass()
    {
        $publicStatusClass = '';
        if (in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_RESERVED,
        ]) && !$this->getAcceptanceAt()) {
            $publicStatusClass =  'red';
        }

        return $publicStatusClass;
    }

    public function addWeightItem(StringEntity $item)
    {
        $this->weights->add($item);

        return $this;
    }

    public function setWeights($weights)
    {
        foreach ($weights as $key => &$weight) {
            if (!($weight instanceof StringEntity)) {
                $weights[$key] = new StringEntity($weight);
            }
        }

        $this->weights = $weights;

        return $this;
    }

    public function getPallets()
    {
        return $this->pallets;
    }

    public function addPalletItem(StringEntity $item)
    {
        $this->pallets->add($item);

        return $this;
    }

    public function setPallets($pallets)
    {
        foreach ($pallets as $key => &$pallet) {
            if (!($pallet instanceof StringEntity)) {
                $pallets[$key] = new StringEntity($pallet);
            }
        }

        $this->pallets = $pallets;

        return $this;
    }

    public function setPrice($price)
    {
        return $this->price = $price;
    }

    public function getPrice()
    {
        return ceil($this->price);
    }

    public function getCurrentPricePerKilo()
    {
        return $this->currentPricePerKilo;
    }

    public function setCurrentPricePerKilo($currentPricePerKilo)
    {
        $this->currentPricePerKilo = $currentPricePerKilo;

        return $this;
    }

    public function getReserveId()
    {
        return $this->reserveId;
    }

    public function setReserveId($reserveId)
    {
        $this->reserveId = $reserveId;

        return $this;
    }

    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    public function setMaxWeight($maxWeight)
    {
        $this->maxWeight= $maxWeight;

        return $this;
    }

    public function getMaxCount()
    {
        return $this->maxCount;
    }

    public function setMaxCount($maxCount)
    {
        $this->maxCount= $maxCount;

        return $this;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }

    public function setSendDate($sendDate)
    {
        if (!($sendDate instanceof DateTime)) {
            $sendDate = \DateTime::createFromFormat('D M d Y H:i:s e+', $sendDate);
        }
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getId() ? $this->getId().'' : '11';
    }

    public function setReceptionAt(DateTime $dateTime)
    {
        return $this->receptionAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getReceptionAt()
    {
        return $this->receptionAt;
    }

    public function setIssueAt(DateTime $dateTime)
    {
        return $this->issueAt = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getIssueAt()
    {
        return $this->issueAt;
    }

    /**
     * @return \DateTime
     */
    public function getNeedPickPartyNotificationDate()
    {
        return $this->needPickPartyNotificationDate;
    }

    public function setNeedPickPartyNotificationDate(DateTime $dateTime)
    {
        return $this->needPickPartyNotificationDate = $dateTime;
    }

     /**
     * @return \DateTime
     */
    public function getUpdateBatchDate()
    {
        return $this->updateBatchDate;
    }

    public function setUpdateBatchDate(DateTime $dateTime = null)
    {
        return $this->updateBatchDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getAcceptanceAt()
    {
        return $this->acceptanceAt;
    }

    public function setAcceptanceAt(DateTime $dateTime = null)
    {
        return $this->acceptanceAt = $dateTime;
    }

    public function isStopSend()
    {
        return $this->stopSend;
    }

    public function setStopSend($stopSend)
    {
        $this->stopSend = $stopSend;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function isNew()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW]);
    }

    public function isRemoveExactStatus()
    {
        return in_array($this->getStatus(), ['', self::STATUS_NEW, self::STATUS_RESERVED]);
    }

    public function isSuspended()
    {
        return $this->stopSend;
    }

    public function isCreated()
    {
        return $this->getStatus() != self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->getStatus() == self::STATUS_ARRIVED;
    }

    public function isStopSendExactStatus()
    {
        return in_array($this->getStatus(), [self::STATUS_NEW, self::STATUS_RESERVED, self::STATUS_READY]) && !$this->stopSend;
    }

    /**
     * @return DateTime
    */
    public function getUpdatePayedDate()
    {
        return $this->updatePayedDate;
    }

    /**
     * @param DateTime $updatePayedDate
     */
    public function setUpdatePayedDate(DateTime $updatePayedDate = null): void
    {
        $this->updatePayedDate = $updatePayedDate;
    }

    public function setPayed($payed)
    {
        $this->setUpdatePayedDate(null);
        $this->payed = (bool)$payed;
    }

    /**
     * @return mixed
     */
    public function getOverload()
    {
        return $this->overload;
    }

    /**
     * @param mixed $overload
     */
    public function setOverload($overload): void
    {
        $this->overload = $overload;
    }

    public function isEdit()
    {
        return in_array($this->getStatus(), [
            self::STATUS_NEW,
            self::STATUS_READY,
            self::STATUS_RESERVED
        ]);
    }

    public function getDriver()
    {
        return $this->getDrivers()->first();
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }


    public function getDrivers()
    {
        return $this->drivers;
    }

    public function addDriver(OrganizationDriver $driver)
    {
        $this->drivers->add($driver);

        return $this;
    }

    public function setDrivers($drivers)
    {
        $this->drivers = $drivers;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getWeight()
    {
        return $this->weight;
    }


    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if (in_array($status, $this->getStatusList())) {
            $this->status = $status;
        }

        return $this;
    }

    public function getCarrier()
    {
        return $this->carrier;
    }

    public function setCarrier(\App\Application\Sonata\UserBundle\Entity\User $carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }


    public function getVerifiedDrivers()
    {
        $result = [];
        if ($this->drivers) {
            /** @var OrganizationDriver $driver */
            foreach ($this->drivers as $driver) {
                if ($driver->isVerified()) {
                    $result[] = $driver;
                }
            }
        }

        return $result;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceVat()
    {
        return $this->priceVat;
    }

    /**
     * @param mixed $priceVat
     */
    public function setPriceVat($priceVat): void
    {
        $this->priceVat = $priceVat;
    }





    public function jsonSerialize()
    {
        return array();
    }
}
