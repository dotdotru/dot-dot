<?php

namespace App\Model;

interface PackageInterface
{
    public function getSizeObject();
    public function getWeight();
    public function getCount();
    public function getPacking();
}
