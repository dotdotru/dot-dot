<?php
namespace App\Repository;

use App\Entity\Lot;
use App\Entity\Pallet;
use App\Entity\Stock;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PalletTestRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getAllWeight()
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->select('SUM(p.weight) as total')
            ->andWhere($qb->expr()->eq('p.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
        ;

        $res = (array)$qb->getQuery()->getArrayResult();
        return reset($res)['total'] ?? 0;
    }

    public function getByFilter(Stock $stockFrom, Stock $stockTo, $weight, $price, $maxWeight = false, $maxCount = false)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->andWhere($qb->expr()->eq('m.stockFrom', ':stockFrom'))
            ->andWhere($qb->expr()->eq('m.stockTo', ':stockTo'))
            ->andWhere('m.weight <= :weight')
            ->setParameters([
                'published' => true,
                'stockFrom' => $stockFrom,
                'stockTo' => $stockTo,
                'weight' => $weight,
            ])
        ;

        if ($maxWeight > 0) {
            $qb->andWhere('m.weight <= :maxWeight');
            $qb->setParameter('maxWeight', $maxWeight);
        }

        $qb->addOrderBy('m.calculatedWeight', 'desc');
        $qb->addOrderBy('m.createdAt', 'asc');

        $result = $qb->getQuery()->getResult();

        $this->usort($result, ['calculatedWeight' => 'desc', 'createdAt' => 'asc']);

        $resultPallets = $this->getFilterPallets($result, $stockFrom, $stockTo, $weight, $price, $maxWeight, $maxCount);

        return $resultPallets;
    }

    private function usort($list, $sort)
    {
        foreach ($sort as $sortField => $sortDirection) {
            $this->sortField = $sortField;
            $this->sortDirection = $sortDirection;
            usort($list, function($a, $b){
                $methodName = 'get'.ucfirst($this->sortField);
                if ($a->{$methodName}() == $b->{$methodName}()) {
                    return 0;
                }

                if ($this->sortDirection == 'desc') {
                    return ($a->{$methodName}() > $b->{$methodName}()) ? -1 : 1;
                }
                return ($a->{$methodName}() < $b->{$methodName}()) ? -1 : 1;
            });
        }

    }

    public function getFilterPallets($pallets, Stock $stockFrom, Stock $stockTo, $weight, $price, $maxWeight = false, $maxCount = false)
    {
        /**
         * @var $pallet Pallet;
         */
        foreach ($pallets as $key => $pallet) {
            if (
                !$pallet->isPublished()
                || $pallet->getStockFrom() != $stockFrom
                || $pallet->getStockTo() != $stockTo
                || ($maxWeight && $pallet->getWeight() <= $maxWeight)
            ) {
                unset($pallets[$key]);
            }
        }

        $this->usort($pallets, ['calculatedWeight' => 'desc', 'createdAt' => 'asc']);

        $resultCount = 4;
        $i = 0;
        $usePalletIds = [];
        do {
            $i++;
            if (!isset($resultPallets[$i])) {
                $resultPallets[$i] = [];
            }
            $totalWeight = 0;
            /**@var \App\Entity\Pallet $item */
            foreach ($pallets as $item) {
                if (in_array($item->getId(), $usePalletIds)) {
                    continue;
                }

                if ($maxCount > 0 && count($resultPallets[$i]) == $maxCount) {
                    break;
                }
                if ($totalWeight + $item->getCalculatedWeight() <= $weight && $item->getPrice() <= $price * $item->getCalculatedWeight()) {
                    $totalWeight += $item->getCalculatedWeight();
                    $resultPallets[$i][$item->getId()] = $item;

                    $usePalletIds[] = $item->getId();
                }
            }

        } while (count($resultPallets) < $resultCount && $i <= $resultCount);

        foreach ($resultPallets as $key => $resultPallet) {
            if (!$resultPallet) {
                unset($resultPallets[$key]);
            }
        }

        return $resultPallets;
    }


}
