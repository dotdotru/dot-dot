<?php
namespace App\Repository\Wms;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\PackageType;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Stock;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class WmsOrderRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->andWhere($qb->expr()->isNull('m.tmp'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByUser(User $user, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.user', ':user'))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->andWhere($qb->expr()->isNull('m.tmp'))
            ->setParameters([
                'user' => $user,
                'remove' => false,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
            } else {
                $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
            }

            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            if ($sortItemKey == 'price') {
                $qb->addSelect('greatest(IFNULL(m.price, 0), IFNULL(m.minPrice, 0), IFNULL(m.suspendedPrice, 0)) AS HIDDEN price');
                $qb->addOrderBy('price', $sortItem);
            } else {
                $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
            }
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByStock(Stock $stock, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $searchField  = '';
        if (isset($filter['field'])) {
            $searchField = $filter['field'] ?? '';
            unset($filter['field']);
        }

        $qb = $this->createQueryBuilder('m');
        $qb
            ->leftJoin(Receiver::class, 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.receiver = r.id')
            ->leftJoin(Stock::class, 'sf', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.stockFrom = sf.id')
            ->leftJoin(Stock::class, 'st', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.stockTo = st.id')
            ->leftJoin(Person::class, 'sender', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.sender = sender.id')
            ->leftJoin(Person::class, 'receiver', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.receiver = receiver.id')
            ->leftJoin(Person::class, 'payer', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.payer = payer.id')
            ->leftJoin('m.organization', 'o')
            ->leftJoin(Direction::class, 'd', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.direction = d.id')
            ->leftJoin('m.packages', 'p')
            ->leftJoin(PackageType::class, 'pt', \Doctrine\ORM\Query\Expr\Join::WITH, 'p.packageType = pt.id')
            #->leftJoin('m.orderPackages', 'op')
            ->leftJoin(Order::class, 'ord', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.order = ord.id')
            ->leftJoin('ord.shippings', 'ordsh')

            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('m.stockTo', ':stock'),
                $qb->expr()->eq('m.stockFrom', ':stock')
            ))
            ->andWhere($qb->expr()->not($qb->expr()->eq('m.status', ':notstatus')))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->andWhere($qb->expr()->isNull('m.tmp'))
            ->setParameters([
				'notstatus' => 'rejected',
                'stock' => $stock,
                'remove' => false,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        if (isset($filter['search']) && $filter['search']) {
            $conditions = [];
            $term = trim($filter['search']);
            unset($filter['search']);
            $dateTimeFormats = [
                'd.m.Y' => 'Y-m-d',
                'd.m.Y H:i'  => 'Y-m-d H:i',
                'd.m.Y H:i:s'  => 'Y-m-d H:i:s',
            ];
            foreach ($dateTimeFormats as $dateTimeFormat => $dateTimeFormatFilter) {
                if (\DateTime::createFromFormat($dateTimeFormat, $term) !== FALSE) {
                    $dateTime = \DateTime::createFromFormat($dateTimeFormat, $term);
                    // Дата выдачи
                    if ($searchField === 'issueAt' || empty($searchField)){
                        $conditions[] = $qb->expr()->like('m.issueAt', ':createdAt');
                    }
                    // Дата приемки
                    if ($searchField === 'receptionAt' || empty($searchField)){
                        $conditions[] = $qb->expr()->like('m.receptionAt', ':createdAt');
                    }
                    !empty($conditions)
                        && $qb->setParameter("createdAt", '%'.$dateTime->format($dateTimeFormatFilter).'%');
                }
            }
            $termParam = false;
            // Заявленная ценность
            if ($searchField === 'declaredPrice' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('m.declaredPrice', ':term');
                $termParam = true;
            }
            if ($searchField === 'externalId' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('m.externalId', ':term');
                $termParam = true;
            }

            // Контакты отправителя
            if ($searchField === 'receiverContact' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('receiver.contactPhone', ':term');
                $conditions[] = $qb->expr()->like('receiver.contactEmail', ':term');
                $termParam = true;
            }

            // Отправитель
            if ($searchField === 'receiver' || empty($searchField)) {;
                $conditions[] = $qb->expr()->like('receiver.contactName', ':term');
                $conditions[] = $qb->expr()->like('receiver.companyName', ':term');
                $conditions[] = $qb->expr()->like('receiver.inn', ':term');
                $conditions[] = $qb->expr()->like('receiver.series', ':term');
                $conditions[] = $qb->expr()->like('receiver.number', ':term');
                $termParam = true;
            }

            // Адрес отправителя
            if ($searchField === 'fromAddress' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('sf.address', ':term');
                $termParam = true;
            }
            // Адрес получателя
            if ($searchField === 'toAddress' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('st.address', ':term');
                $conditions[] = $qb->expr()->like('ordsh.address', ':term');
                $termParam = true;

            }
            // Направление
            if ($searchField === 'direction' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('d.title', ':term');
                $termParam = true;
            }

            // Перевозчик
            if ($searchField === 'carrier' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('o.companyName', ':term');
                $termParam = true;
            }

            // Контакты отправителя
            if ($searchField === 'senderContact' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('sender.contactPhone', ':term');
                $conditions[] = $qb->expr()->like('sender.contactEmail', ':term');
                $termParam = true;
            }

            // Отправитель
            if ($searchField === 'sender' || empty($searchField)) {;
                $conditions[] = $qb->expr()->like('sender.contactName', ':term');
                $conditions[] = $qb->expr()->like('sender.companyName', ':term');
                $conditions[] = $qb->expr()->like('sender.inn', ':term');
                $conditions[] = $qb->expr()->like('sender.series', ':term');
                $conditions[] = $qb->expr()->like('sender.number', ':term');
                $termParam = true;
            }

            // Тип груза
            if ($searchField === 'packageType' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('pt.title', ':term');
                $termParam = true;
            }
            $orX = $qb->expr()->orX();
            foreach ($conditions as $condition) {
                $orX->add($condition);
            }

            if ($conditions) {
                $qb->andWhere($orX);
            }

            $termParam && $qb->setParameter("term", "%" . $term . "%");
        }

        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                if (isset($filterItem['sign'])) {
                    $qb->andWhere('m.'.$filterItemKey.' '.$filterItem['sign'].' :'.$filterItemKey);
                    $qb->setParameter($filterItemKey, $filterItem['value']);
                } else {
                    $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }
            } else {
                if ($filterItem == 'null' || $filterItem == NULL) {
                    $qb->andWhere($qb->expr()->isNull('m.'.$filterItemKey));
                } else {
                    $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }
            }
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            if ($sortItemKey == 'price') {
                $qb->addSelect('greatest(IFNULL(m.price, 0), IFNULL(m.minPrice, 0), IFNULL(m.suspendedPrice, 0)) AS HIDDEN price');
                $qb->addOrderBy('price', $sortItem);
            } else {
                $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
            }
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getById($id)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getByExternalId($externalId)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.externalId', ':externalId'))
            ->setParameters([
                'externalId' => $externalId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getByUpdate()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.externalId > 0')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function getByNoCreate()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.externalId <= 0')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
            ])
        ;

        return $qb->getQuery()->getResult();
    }
    
        
    public function getByPackages($packages)
	{
		$packageIds = []; 
		foreach($packages as $package) {
			$packageIds[] = $package->getId();
		}
		$qb = $this->createQueryBuilder("m");
		$qb->join('m.packages', 'p', 'WITH', $qb->expr()->in('p.id', $packageIds));

		return $qb->getQuery()->getResult();
	}

    public function getByFile($fileId)
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->leftJoin('m.files', 'f')
            ->addSelect('f')
            ->where('f.id = :id')
            ->setParameter('id', $fileId)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
