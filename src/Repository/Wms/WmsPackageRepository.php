<?php
namespace App\Repository\Wms;

use App\Entity\Order;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class WmsPackageRepository extends EntityRepository
{
    public function getNotInPallet(Stock $stock, $page = 1, $count = 10, $sort = [], $filter = [])
    {
        if (isset($filter['stockTo'])) {
            $filter['o.stockTo'] = $filter['stockTo'];
            unset($filter['stockTo']);
        }
        
        $alias = 'p';
        $qb = $this->createQueryBuilder($alias);
        $qb->leftJoin(WmsOrder::class, 'o', \Doctrine\ORM\Query\Expr\Join::WITH, $alias.'.order = o');
        $qb->andWhere("o.stockFrom = :stock");
        $qb->setParameter('stock', $stock);

        $qb->andWhere("p.status != :intransit and p.status != '' and p.status IS NOT NULL");
        $qb->setParameter('intransit', WmsPackage::STATUS_INTRANSIT);
        
        $qb->andWhere($alias.".palletId = '' or ".$alias.".palletId IS NULL");
        $qb->setFirstResult(($page-1) * $count)
            ->setMaxResults($count);

        foreach ($filter as $filterItemKey => $filterItem) {
            $aliasFilter = $alias;
            $foundDot = strpos($filterItemKey, '.');
            if ($foundDot !== false) {
                $aliasFilter = substr($filterItemKey, 0, $foundDot);
                $filterItemKey = substr($filterItemKey, $foundDot + 1, strlen($filterItemKey));
            }

            if (is_array($filterItem)) {
                $qb->andWhere($qb->expr()->in($aliasFilter.'.'.$filterItemKey, ':'.$filterItemKey));
            } else {
                $qb->andWhere($qb->expr()->eq($aliasFilter.'.'.$filterItemKey, ':'.$filterItemKey));
            }

            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('p.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }
}
