<?php
namespace App\Repository\Wms;

use App\Entity\Stock;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class WmsPalletRepository extends EntityRepository
{
    public function getByStock(Stock $stock, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->leftJoin('m.stockTo', 'stockTo')
            ->leftJoin('m.stockFrom', 'stockFrom')
            ->leftJoin('m.packages', 'p')
            ->andWhere($qb->expr()->eq('m.stockFrom', ':stockFrom'))
            ->setParameters([
                'stockFrom' => $stock,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        if (isset($filter['search']) && $filter['search']) {
            $conditions = [];
            $term = trim($filter['search']);
            unset($filter['search']);

            if (empty($filter['field'])) {
                $conditions[] = $qb->expr()->like('m.externalId', ':term');
                $conditions[] = $qb->expr()->like('m.weight', ':term');
                $conditions[] = $qb->expr()->like('stockFrom.address', ':term');
                $conditions[] = $qb->expr()->like('stockTo.address', ':term');
                $conditions[] = $qb->expr()->like('p.externalId', ':term');
            } else {
                if ($filter['field'] === 'externalId') {
                    $conditions[] = $qb->expr()->like('m.externalId', ':term');
                }
                if ($filter['field'] === 'packageId') {
                    $conditions[] = $qb->expr()->like('p.externalId', ':term');
                }
                if ($filter['field'] === 'weight') {
                    $conditions[] = $qb->expr()->like('m.weight', ':term');
                }
            }

            $orX = $qb->expr()->orX();
            foreach ($conditions as $condition) {
                $orX->add($condition);
            }

            if ($conditions) {
                $qb->andWhere($orX);
            }

            $qb->setParameter("term", "%" . $term . "%");
        }

        if (isset($filter['field'])) {
            unset($filter['field']);
        }

        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                if (isset($filterItem['sign'])) {
                    $qb->andWhere('m.'.$filterItemKey.' '.$filterItem['sign'].' :'.$filterItemKey);
                    $qb->setParameter($filterItemKey, $filterItem['value']);
                } else {
                    $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }
            } else {

                if ($filterItem == 'null' || $filterItem == NULL) {
                    $qb->andWhere($qb->expr()->isNull('m.'.$filterItemKey));
                } else {
                    $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }

            }
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByStockTo(Stock $stock, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->leftJoin('m.stockTo', 'stockTo')
            ->leftJoin('m.stockFrom', 'stockFrom')
            ->leftJoin('m.packages', 'p')
            ->andWhere($qb->expr()->eq('m.stockTo', ':stockTo'))
            ->setParameters([
                'stockTo' => $stock,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        if (isset($filter['search']) && $filter['search']) {
            $conditions = [];
            $term = trim($filter['search']);
            unset($filter['search']);

            if (empty($filter['field'])) {
                $conditions[] = $qb->expr()->like('m.externalId', ':term');
                $conditions[] = $qb->expr()->like('m.weight', ':term');
                $conditions[] = $qb->expr()->like('stockFrom.address', ':term');
                $conditions[] = $qb->expr()->like('stockTo.address', ':term');
                $conditions[] = $qb->expr()->like('p.externalId', ':term');
            } else {
                if ($filter['field'] === 'externalId') {
                    $conditions[] = $qb->expr()->like('m.externalId', ':term');
                }
                if ($filter['field'] === 'packageId') {
                    $conditions[] = $qb->expr()->like('p.externalId', ':term');
                }
                if ($filter['field'] === 'weight') {
                    $conditions[] = $qb->expr()->like('m.weight', ':term');
                }
            }

            $orX = $qb->expr()->orX();
            foreach ($conditions as $condition) {
                $orX->add($condition);
            }

            if ($conditions) {
                $qb->andWhere($orX);
            }

            $qb->setParameter("term", "%" . $term . "%");
        }
        if (isset($filter['field'])) {
            unset($filter['field']);
        }

        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                if (isset($filterItem['sign'])) {
                    $qb->andWhere('m.'.$filterItemKey.' '.$filterItem['sign'].' :'.$filterItemKey);
                    $qb->setParameter($filterItemKey, $filterItem['value']);
                } else {
                    $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }
            } else {

                if ($filterItem == 'null' || $filterItem == NULL) {
                    $qb->andWhere($qb->expr()->isNull('m.'.$filterItemKey));
                } else {
                    $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }

            }
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

}