<?php
namespace App\Repository\Wms;

use App\Entity\Direction;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Stock;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Wms\AbstractWmsBatch;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;


class AbstractWmsBatchRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByUser(User $user, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.carrier', ':carrier'))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            #->andWhere($qb->expr()->eq(':createdAt > :endAt'))
            ->setParameters([
                'carrier' => $user,
                'remove' => false,
                #'createdAt' => 'm.createdAt',
                #'endAt' => false,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':').$filterItemKey);
            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByStock(Stock $stock, $page = 1, $count = 20, $sort = [], $filter = [])
    {

        $qb = $this->createQueryBuilder('m');
        $qb->select('m')
            // Обычная партия
            ->leftJoin(WmsBatch::class, 'wb', 'WITH', 'm.id = wb.id')
            // Партия мили
            ->leftJoin(WmsBatchMover::class, 'wbm', 'WITH', 'm.id = wbm.id')

            ->leftJoin('wb.stockFrom', 'wb_sf')
            ->leftJoin('wb.stockTo', 'wb_st')
            ->leftJoin('wb.organization', 'wb_o')
            ->leftJoin('wb.direction', 'wb_d')

            ->leftJoin('wbm.shipping', 'wbm_s')
            ->leftJoin('wbm_s.order', 'wbm_s_o')

            ->leftJoin('m.driver', 'driver')

            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->andWhere($qb->expr()->not($qb->expr()->eq('m.status', ':notstatus')))


            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('wb.stockTo', ':stock'),
                $qb->expr()->eq('wb.stockFrom', ':stock'),
                $qb->expr()->eq('wbm_s.stock', ':stock')
            ))

            ->setParameters([
                'stock' => $stock,
                'remove' => false,
            ])

            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;



        if (isset($filter['search']) && $filter['search']) {
            $conditions = [];
            $term = trim($filter['search']);
            $searchField = !empty($filter['field']) ? $filter['field'] : '';
            unset($filter['search'], $filter['field']);

            $dateTimeFormats = [
                'd.m.Y' => 'Y-m-d',
                'd.m.Y H:i'  => 'Y-m-d H:i',
                'd.m.Y H:i:s'  => 'Y-m-d H:i:s',
            ];
            foreach ($dateTimeFormats as $dateTimeFormat => $dateTimeFormatFilter) {
                if (\DateTime::createFromFormat($dateTimeFormat, $term) !== FALSE) {
                    $dateTime = \DateTime::createFromFormat($dateTimeFormat, $term);
                    // Дата выдачи
                    if ($searchField === 'issueAt' || empty($searchField)) {
                        $conditions[] = $qb->expr()->like('m.issueAt', ':createdAt');
                    }
                    // Дата приемки
                    if ($searchField === 'receptionAt' || empty($searchField)) {
                        $conditions[] = $qb->expr()->like('m.receptionAt', ':createdAt');
                    }
                    !empty($conditions)
                    && $qb->setParameter("createdAt", '%'.$dateTime->format($dateTimeFormatFilter).'%');
                }
            }
            $termParam = false;

            // № партии
            if ($searchField === 'externalId' || empty($searchField)) {
                if (preg_match('/L$/', $term)) {
                    // Костыль для поиска последних миль
                    $conditions[] = $qb->expr()->eq('wbm_s.type', ':shippingType');
                    $qb->setParameter("shippingType", 'deliver');
                } else {
                    $conditions[] = $qb->expr()->orX(
                        $qb->expr()->like('m.externalId', ':term'),
                        $qb->expr()->like('wbm_s_o.id', ':term')
                    );
                    $termParam = true;
                }
            }
            // Вес
            if ($searchField === 'weight' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('m.weight', ':term');
                $termParam = true;
            }
            // Стоимость
            if ($searchField === 'price' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('m.price', ':term');
                $termParam = true;
            }



            // Адрес отправителя
            if ($searchField === 'fromAddress' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('wb_sf.address', ':term');
                $termParam = true;
            }
            // Адрес получателя
            if ($searchField === 'toAddress' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('wb_st.address', ':term');
                $termParam = true;
            }
            // Направление
            if ($searchField === 'direction' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('wb_d.title', ':term');
                $termParam = true;
            }
            // Перевозчик
            if ($searchField === 'carrier' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('wb_o.companyName', ':term');
                $termParam = true;
            }
            // Контакты перевозчика
            if ($searchField === 'carrierContact' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('wb_o.address', ':term');
                $conditions[] = $qb->expr()->like('wb_o.contactPhone', ':term');
                $termParam = true;
            }
            //$conditions[] = $qb->expr()->like('drivers.name', ':term');
            //$conditions[] = $qb->expr()->like('drivers.phone', ':term');
            // Водитель
            if ($searchField === 'driver' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('driver.name', ':term');
                $termParam = true;
            }
            // Контакты водителя
            if ($searchField === 'driverContact' || empty($searchField)) {
                $conditions[] = $qb->expr()->like('driver.phone', ':term');
                $termParam = true;
            }



            $orX = $qb->expr()->orX();
            foreach ($conditions as $condition) {
                $orX->add($condition);
            }

            if ($conditions) {
                $qb->andWhere($orX);
            }

            $termParam && $qb->setParameter("term", "%" . $term . "%");
        }




        if (!isset($filter['status'])) {
            $qb->setParameter('notstatus', 'rejected');
        } else {
            $qb->setParameter('notstatus', '');
        }


        /*


        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                if (isset($filterItem['sign'])) {
                    $qb->andWhere('m.'.$filterItemKey.' '.$filterItem['sign'].' :'.$filterItemKey);
                    $qb->setParameter($filterItemKey, $filterItem['value']);
                } else {
                    $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }
            } else {

                if ($filterItem == 'null' || $filterItem == NULL) {
                    $qb->andWhere($qb->expr()->isNull('m.'.$filterItemKey));
                } else {
                    $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
                    $qb->setParameter($filterItemKey, $filterItem);
                }

            }
        }
        */

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }



    public function getNew()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.status = :status')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
                'status' => Order::STATUS_RESERVED,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function getByUpdate()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.externalId > 0')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function getByExternalId($externalId)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.externalId', ':externalId'))
            ->setParameters([
                'externalId' => $externalId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
