<?php
namespace App\Repository\Wms;

use App\Entity\Stock;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class WmsBatchPalletRepository extends EntityRepository
{
    public function getByStock(Stock $stock, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.stockFrom', ':stockFrom'))
            ->setParameters([
                'stockFrom' => $stock,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':').$filterItemKey);
            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }
}