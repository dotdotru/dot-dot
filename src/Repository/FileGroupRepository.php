<?php
namespace App\Repository;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class FileGroupRepository extends EntityRepository
{
    public function getDefaultCustomer()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.defaultCustomer', ':defaultCustomer'))
            ->setParameters([
                'defaultCustomer' => true,
            ])
        ;

        $qb->addOrderBy('m.position', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getDefaultCarrier()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.defaultCarrier', ':defaultCarrier'))
            ->setParameters([
                'defaultCarrier' => true,
            ])
        ;

        $qb->addOrderBy('m.position', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getDefaultWmsOrder()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.defaultWmsOrder', ':defaultWmsOrder'))
            ->setParameters([
                'defaultWmsOrder' => true,
            ])
        ;

        $qb->addOrderBy('m.position', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getDefaultWmsBatch()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.defaultWmsBatch', ':defaultWmsBatch'))
            ->setParameters([
                'defaultWmsBatch' => true,
            ])
        ;

        $qb->addOrderBy('m.position', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
