<?php
namespace App\Repository\Content;

use App\Entity\Batch;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ArticleRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->leftJoin('m.tags', 'tags')
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        if ($filter) {
            if (isset($filter['tags'])) {
                $qb->andWhere($qb->expr()->in('tags.id', ':tags'));
                $qb->setParameter('tags', $filter['tags']);
            }
        }


        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }
}
