<?php
namespace App\Repository;

use App\Entity\Batch;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BatchRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByUser(User $user, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.carrier', ':carrier'))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            #->andWhere($qb->expr()->eq(':createdAt > :endAt'))
            ->setParameters([
                'carrier' => $user,
                'remove' => false,
                #'createdAt' => 'm.createdAt',
                #'endAt' => false,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':').$filterItemKey);
            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByAdmin(User $user, $page = 1, $count = 20, $sort = [], $filter = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('m.carrier', ':carrier'),
                $qb->expr()->eq('m.overload', ':overload')
            ))
            ->setParameters([
                'carrier' => $user,
                'overload' => true,
                'remove' => false,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':').$filterItemKey);
            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getNew()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.status = :status')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
                'status' => Batch::STATUS_RESERVED,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function getByUpdate()
    {
        $fromDate = (new \DateTime())->modify('-2 month');

        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.externalId > 0')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->andWhere($qb->expr()->gt('m.createdAt', ':dateFrom'))
            ->setParameters([
                'remove' => false,
                'dateFrom' => $fromDate
            ])
        ;

        $qb->addOrderBy('m.id', 'desc');

        return $qb->getQuery()->getResult();
    }

    public function getByFile($fileId)
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->leftJoin('m.files', 'f')
            ->addSelect('f')
            ->where('f.id = :id')
            ->setParameter('id', $fileId)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
