<?php
namespace App\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BaseEventRepository extends EntityRepository
{
    public function getByExternalId($externalId)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.externalId', ':externalId'))
            ->setParameters([
                'externalId' => $externalId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getByStatus($status)
    {
        return $this->findBy(['status' => $status]);
    }
}
