<?php
namespace App\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DirectionRepository extends EntityRepository
{
    public function getAllArray()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->innerJoin('m.cityTo', 'c')
            ->addSelect('c')
            ->innerJoin('m.cityFrom', 'c2')
            ->addSelect('c2')
            ->setParameters([
                'published' => true,
            ])
        ;
        $qb->addOrderBy('m.id');
        
        return $qb->getQuery()->getArrayResult();
    }
}
