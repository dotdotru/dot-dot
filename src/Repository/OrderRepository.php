<?php
namespace App\Repository;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use App\Entity\Order;

class OrderRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByUserForPromocode(User $user, $page = 1, $count = 20)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.user', ':user'))
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        $qb
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->neq('m.remove', true)
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->eq('m.remove', true),
                        $qb->expr()->neq('m.status', ':new'),
                        $qb->expr()->neq('m.status', ':booked'),
                        $qb->expr()->neq('m.status', ':waiting'),
                        $qb->expr()->neq('m.status', ':suspended')
                    )
                )
            );

        $qb
            ->setParameters([
                'user' => $user,
                'new' => Order::STATUS_NEW,
                'booked' => Order::STATUS_BOOKED,
                'waiting' => Order::STATUS_WAITING,
                'suspended' => Order::STATUS_SUSPENDED,
            ]);


        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getByUser(User $user, $page = 1, $count = 20, $sort = [], $filter = []) 
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.user', ':user'))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'user' => $user,
                'remove' => false
            ])

            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($filter as $filterItemKey => $filterItem) {
            if (is_array($filterItem)) {
                $qb->andWhere($qb->expr()->in('m.'.$filterItemKey, ':'.$filterItemKey));
            } else {
                $qb->andWhere($qb->expr()->eq('m.'.$filterItemKey, ':'.$filterItemKey));
            }

            $qb->setParameter($filterItemKey, $filterItem);
        }

        foreach ($sort as $sortItemKey => $sortItem) {
            if ($sortItemKey == 'price') {
                $qb->addSelect('greatest(IFNULL(m.price, 0), IFNULL(m.minPrice, 0), IFNULL(m.suspendedPrice, 0)) AS HIDDEN price');
                $qb->addOrderBy('price', $sortItem);
            } else {
                $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
            }
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getOrderByUser(User $user, $id)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.user', ':user'))
            ->andWhere($qb->expr()->eq('m.id', ':id'))
            ->setParameters([
                'user' => $user,
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getById($id)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.id', ':id'))
            ->setParameters([
                'id' => $id,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getByExternalId($externalId)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.externalId', ':externalId'))
            ->setParameters([
                'externalId' => $externalId,
            ])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getByUpdate()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere('m.externalId > 0')
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
            ])
        ;

        $qb->addOrderBy('m.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function getByNoCreate()
    {
        $qb = $this->createQueryBuilder('m');
        $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('m.status', ':booked'),
                $qb->expr()->eq('m.status', ':waiting')
            ))
            ->andWhere($qb->expr()->eq('m.remove', ':remove'))
            ->setParameters([
                'remove' => false,
                'booked' => 'booked',
                'waiting' => 'waiting',
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function getByShipping($shippingExtId)
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->leftJoin('m.shippings', 's')
            ->addSelect('s')
            ->where('s.externalId = :externalId')
            ->andWhere('s.reject = 0')
            ->setParameter('externalId', $shippingExtId)
        ;

        $result = $qb->getQuery()->getResult();

	return empty($result) ? null : $result[0];
    }

    public function getByFile($fileId)
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->leftJoin('m.files', 'f')
            ->addSelect('f')
            ->where('f.id = :id')
            ->setParameter('id', $fileId)
        ;

        // @todo разобраться почему есть дубли
        try {
            return $qb->getQuery()->getOneOrNullResult();
        } Catch (\Exception $e) {
            return null;
        }
    }
}
