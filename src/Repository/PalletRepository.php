<?php
namespace App\Repository;

use App\Entity\City;
use App\Entity\Direction;
use App\Entity\Lot;
use App\Entity\Pallet;
use App\Entity\Stock;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Simplex;

class PalletRepository extends EntityRepository
{
    public function getBy($page = 1, $count = 20, $sort = [])
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
            ->setFirstResult(($page-1) * $count)
            ->setMaxResults($count)
        ;

        foreach ($sort as $sortItemKey => $sortItem) {
            $qb->addOrderBy('m.'.$sortItemKey, $sortItem);
        }

        $paginator = new Paginator($qb);
        return $paginator;
    }

    public function getAllWeight()
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->select('SUM(p.weight) as total')
            ->andWhere($qb->expr()->eq('p.published', ':published'))
            ->setParameters([
                'published' => true,
            ])
        ;

        $res = (array)$qb->getQuery()->getArrayResult();
        return reset($res)['total'] ?? 0;
    }

    public function getByFilter(Direction $direction, $weight, $pricePerKilo, $maxWeight = false, $maxCount = false)
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->andWhere($qb->expr()->eq('m.published', ':published'))
            ->andWhere($qb->expr()->eq('m.direction', ':direction'))
            ->andWhere('m.weight <= :weight')
            ->setParameters([
                'published' => true,
                'direction' => $direction,
                'weight' => $weight,
            ])
        ;

        if ($maxWeight > 0) {
            $qb->andWhere('m.weight <= :maxWeight');
            $qb->setParameter('maxWeight', $maxWeight);
        }

        $qb->addOrderBy('m.weight', 'desc');
        $qb->addOrderBy('m.createdAt', 'asc');

        $resultPallets = $qb->getQuery()->getResult();

        $resultPallets = $this->usort($resultPallets, ['weight' => 'desc', 'createdAt' => 'asc']);

        $resultPallets = $this->getFilterPallets($resultPallets, $direction, $maxWeight, $pricePerKilo / 100);

        $result = $this->combineList($resultPallets, $weight, $maxCount ? $maxCount : 33);

        return $result;
    }

    public function usort($list, $sort)
    {
        usort($list, function($a, $b) use ($sort){

            $result = 0;

            foreach ($sort as $sortField => $sortDirection) {

                $methodName = 'get'.ucfirst($sortField);

                if ($a->{$methodName}() == $b->{$methodName}()) {
                    continue;
                }

                $result = ($a->{$methodName}() < $b->{$methodName}()) ? -1 : 1;

                if ($sortDirection == 'desc') {
                    $result = -$result;
                }

                break;
            }

            return $result;
        });

        return $list;
    }

    public function getFilterPallets($pallets, Direction $direction, $maxWeight = false, $pricePerKilo = false)
    {
        /**
         * @var $pallet Pallet;
         */
        foreach ($pallets as $key => $pallet) {
            if (
                !$pallet->isPublished()
                || $pallet->getDirection()->getId() != $direction->getId()
                || ($maxWeight && (float)$pallet->getWeight() > (float)$maxWeight)
                || ($pricePerKilo && (float)($pricePerKilo * $pallet->getWeight()) > (float)$pallet->getPrice())
            ) {
                unset($pallets[$key]);
            }
        }

        return $pallets;
    }

    public function combineList($pallets, $maxWeight, $maxCount = false)
    {
        $weights = [];
        /* Соберем по группам */
        /** @var Pallet $item */
        foreach ($pallets as $item) {
            if (!isset($weights[$item->getWeight()])) {
                $weights[$item->getWeight()] = 0;
            }
            $weights[$item->getWeight()]++;
        }
        ksort($weights);

        /* Определим ограничения */
        $i = 0;
        $weightIndex = [];
        $weightValues = [];
        $weightLimits = [];
        foreach ($weights as $weight => $weightCount) {
            $i++;
            $weightIndex['x'.$i] = $weightCount;
            $weightValues['x'.$i] = $weight;
            $weightLimits[$weight] = $weightCount;
        }

        $z = new Simplex\Func($weightIndex);

        $task = new Simplex\Task($z);
        $task->addRestriction(new Simplex\Restriction($weightValues, Simplex\Restriction::TYPE_EQ, $maxWeight));

        if ($maxCount > 0) {
            $i = 0;
            $params = [];
            foreach ($weights as $weight => $weightCount) {
                $i++;
                $params['x'.$i] = 1;
            }
            $task->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_EQ, $maxCount));
        }

        $i = 0;
        foreach ($weights as $weight => $weightCount) {
            $i++;
            $params = [];
            for($j=1;$j<=count($weights);$j++) {
                if ($i == $j) {
                    $params['x' . $j] = 1;
                    $weightCountItem = $weightCount;
                } else {
                    $params['x' . $j] = 0;
                }
            }

            $task->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_LOE, $weightCountItem));
        }

        $config = [];
        $config['weight'] = $maxWeight;
        $config['maxCount'] = $maxCount;

        $solver = new Simplex\Solver($task);
        $parts[] = $this->combine($solver, $config, $pallets, $weightIndex, $weightValues, $weightLimits);

        $task = $this->randomize($task, $solver, $weights, $weightIndex);
        $solver = new Simplex\Solver($task);
        $parts[] = $this->combine($solver, $config, $pallets, $weightIndex, $weightValues, $weightLimits);

        $task = $this->randomize($task, $solver, $weights, $weightIndex);
        $solver = new Simplex\Solver($task);
        $parts[] = $this->combine($solver, $config, $pallets, $weightIndex, $weightValues, $weightLimits);

        $task = new Simplex\Task($z);
        $task->addRestriction(new Simplex\Restriction($weightValues, Simplex\Restriction::TYPE_EQ, $config['weight']));

        $i = 0;
        foreach ($weights as $weight => $weightCount) {
            $i++;
            $params = [];
            for($j=1;$j<=count($weights);$j++) {
                if ($i == $j) {
                    $params['x' . $j] = 1;
                    $weightCountItem = $weightCount;
                } else {
                    $params['x' . $j] = 0;
                }
            }
            $task->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_LOE, $weightCountItem));
        }

        $i = 0;
        $params = [];
        foreach ($weights as $weight => $weightCount) {
            $i++;
            $params['x'.$i] = 1;
        }

        $tmpList = [];
        $configTmp = $config;
        for ($j=2;$j<$config['maxCount'];$j++) {
            $tmpTask = clone $task;
            $configTmp['maxCount'] = $j;
            $tmpTask->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_LOE, $j));
            $solver = new Simplex\Solver($tmpTask);
            $testPart = $this->combine($solver, $configTmp, $pallets, $weightIndex, $weightValues, $weightLimits);

            $tmpList[$j] = 0;
            foreach ($testPart as $item) {
                $tmpList[$j] += $item->getWeight();
            }
        }

        asort($tmpList);

        foreach ($tmpList as $count => $weight) {
            $config['maxCount'] = $count;
            if ($weight >= $config['weight']) {
                break;
            }
        }

        $task->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_LOE, $config['maxCount']));
        $solver = new Simplex\Solver($task);
        $parts[] = $this->combine($solver, $config, $pallets, $weightIndex, $weightValues, $weightLimits);

		usort($parts, function($a,$b){
			if (count($a) == count($b)) {
				return 0;
			}
			return (count($a) > count($b)) ? -1 : 1;
		});

        return $parts;
    }

    private function combine($solver, $config, $pallets, $weightIndex, $weightValues, $weightLimits)
    {
        $steps = $solver->getSteps();
        $table = end($steps);

        $currentWeight = 0;
        $currentCount = 0;
        $limits = [];

        foreach ($table->getRows() as $index => $tableRow) {
            if (isset($weightIndex[$tableRow->getVar()])) {
                $weight = $weightValues[$tableRow->getVar()];
                $count = ceil($tableRow->getB()->toFloat());
                $limits[$weight] = $count;
                $currentWeight += $weight*$count;
                $currentCount += $count;
            }
        }

        ksort($limits);
        while ($currentWeight > $config['weight']) {
            foreach ($limits as $weight => $count) {
                if ($limits[$weight] - 1 >= 0) {
                    $limits[$weight]--;
                    $currentWeight -= $weight;
                    $currentCount--;
                    break;
                }
            }
        }

        if ($currentCount < $config['maxCount']) {
            $averageWeight = ($config['weight'] - $currentWeight) / ($config['maxCount'] - $currentCount);

            usort($pallets, function($a, $b) use ($averageWeight) {
                if ($a->getWeight() == $b->getWeight()) {
                    return 0;
                }
                return (abs($a->getWeight() - $averageWeight) < abs($b->getWeight() - $averageWeight)) ? -1 : 1;
            });

            foreach ($pallets as $item) {
                $weight = $item->getWeight();

                if (
                    ($currentWeight + $weight <= $config['weight'])
                    &&
                    ($currentCount + 1 <= $config['maxCount'])
                    &&
                    (
                        (isset($limits[$weight]) && ($weightLimits[$weight] > $limits[$weight]))
                        || $weightLimits[$weight] > 0
                    )
                ) {
                    if (!isset($limits[$weight])) {
                        $limits[$weight] = 0;
                    }
                    $limits[$weight]++;
                    $currentWeight += $weight;
                    $currentCount++;
                }
            }
        }

        $sort = ['createdAt' => 'asc'];
        $pallets = $this->usort($pallets, $sort);

        $part = [];
        foreach ($pallets as $item) {
            $weight = $item->getWeight();
            if (isset($limits[$weight]) && $limits[$weight] > 0) {
                $part[] = $item;
                $limits[$item->getWeight()]--;
            }
        }

        return $part;
    }

    private function randomize($task, $solver, $weights, $weightIndex)
    {
        $steps = $solver->getSteps();
        $table = end($steps);

        $limits = [];
        foreach ($table->getRows() as $index => $tableRow) {
            if (isset($weightIndex[$tableRow->getVar()])) {
                $count = $tableRow->getB()->toFloat();
                $limits[$tableRow->getVar()] = $count;
            }
        }

        arsort($limits);
        $params = [];
        $xk = array_keys($limits);
        $k = reset($xk);
        $c = reset($limits);
        for($j=1;$j<=count($weights);$j++) {
            $x = 'x' . $j;
            $params[$x] = 0;

            if ($k == $x) {
                $params[$x] = 1;
            }
        }
        if ($c > 2) {
            $c = $c - 2;
        }
        if ($c > 1) {
            $c = $c - 1;
        }

        $task->addRestriction(new Simplex\Restriction($params, Simplex\Restriction::TYPE_LOE, $c));

        return $task;
    }


}
