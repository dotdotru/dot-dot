<?php
namespace App\Repository\Organization;

use App\Entity\Organization\Organization;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class OrganizationTypeRepository extends EntityRepository
{
    public function getAllArray()
    {
        $qb = $this->createQueryBuilder('m');
        $query = $qb->getQuery();

        return $query->getArrayResult();
    }
}
