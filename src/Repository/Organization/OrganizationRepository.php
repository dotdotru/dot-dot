<?php
namespace App\Repository\Organization;

use App\Entity\Organization\Organization;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Organization\OrganizationDriver;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class OrganizationRepository extends EntityRepository
{
    public function getCarrierByUser(User $user)
    {
        $result = null;
        $organizations = $this->findBy(['user' => $user], ['id' => 'desc']);

        /** @var Organization $organization */
        foreach ($organizations as $organization) {
            if ($organization->getDrivers()->count()) {
                $result = $organization;
                break;
            }
        }

        return $result;
    }

    public function getCustomerByUser(User $user)
    {
        $result = null;
        $organizations = $this->findBy(['user' => $user], ['id' => 'desc']);

        /** @var Organization $organization */
        foreach ($organizations as $organization) {
            if ($organization->getDrivers()->count() == 0) {
                $result = $organization;
                break;
            }
        }

        return $result;
    }


    public function getByUser(User $user)
    {
        $result = null;
        $organizations = $this->findBy(['user' => $user], ['id' => 'desc']);

        /** @var Organization $organization */
        foreach ($organizations as $organization) {
            if (($user->isCarrierNow() && $organization->getDrivers()->count() > 0)
                || ($user->isCustomerNow() && $organization->getDrivers()->count() == 0)
            ) {
                $result = $organization;
                break;
            }
        }

        return $result;
    }

    public function getInners()
    {
        $result = null;
        $organizations = $this->findBy(['innerCarrier' => true], ['id' => 'desc']);

        return $organizations;
    }


    public function findByDriver (OrganizationDriver $driver = null)
    {
        if (!$driver) {
            return null;
        }

        $qb = $this->createQueryBuilder('o')
            ->leftJoin('o.drivers', 'd');

        $qb->andWhere('d.id = :driver');
        $qb->setParameter('driver', $driver->getId());

        $organizations = $qb->getQuery()->getResult();

        return empty($organizations) ? null : $organizations[0];
    }


    public function findCityCarriersForMiles ($city)
    {
        $qb = $this->createQueryBuilder('o');

        $query = $qb->select('o', 'u')
            ->leftJoin('o.user', 'u')
            ->join('o.drivers', 'd');

        $query->andWhere('o.city = :city')
            ->setParameter('city', $city->getId());

        $organizations = $query->getQuery()->getResult();

        $users = [];

        foreach ($organizations as $organization) {
            /** @var User $user */
            $user = $organization->getUser();

            if ($user->hasRole('ROLE_MILES')) {
                $users[] = $user;
            }
        }

        return $users;
    }


}
