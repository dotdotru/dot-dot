<?php
namespace App\Repository\Personal;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;


class AuthtokenRepository extends EntityRepository
{

    public function findByParams($params = [])
    {
        $limit = empty($params['limit']) ? 10 : (int)$params['limit'];
        $offset  = empty($params['page']) ? 0 : ($params['page'] - 1) * $limit;

        $qb = $this->createQueryBuilder('m')
            ->leftJoin('m.user', 'u')
            ->leftJoin('m.order', 'o');
        $qb->select('m', 'u', 'o');

        if (!empty($params['used'])) {
            $qb->andWhere('m.order IS NOT NULL');
        }

        if (!empty($params['user'])) {
            $qb->andWhere($qb->expr()->eq('m.user', ':user'))
                ->setParameters(['user' => $params['user']]);
        }

        $qb->setFirstResult($offset)
            ->setMaxResults($limit);

        $qb->addOrderBy('o.createdAt', 'DESC');
        $qb->addOrderBy('m.id', 'DESC');

        $paginator = new Paginator($qb);

        return $paginator;
    }

}
