<?php

namespace App\Admin;

use App\Entity\Packing;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ShippingAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('stock')
            ->add('type')
            ->add('active')
            ->add('pickAt')
            ->add('address')
            ->add('countLoader')
            ->add('hydroBoard')
            ->add('price')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('provider')
            ->add('reject')
            ->add('externalId')
            ->add('stock')
            ->add('type')
            ->add('active')
            ->add('pickAt')
            ->add('address')
            ->add('countLoader')
            ->add('hydroBoard')
            ->add('price')
            ->add('contactEmail')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add('id', null, ['disabled' => false])
			->add('externalId', null, ['disabled' => true])
            ->add('provider', null, ['disabled' => true])
            ->add('type', null, ['disabled' => true])
            ->add('reject', null, ['disabled' => false])
            ->add('stock', null, ['disabled' => false])
            ->add('active', null, ['disabled' => false])
            ->add('driverName', null, ['disabled' => false])
            ->add('driverPhone', null, ['disabled' => false])
            ->add('driverDocument', null, ['disabled' => false])
            ->add('done', null, ['disabled' => false])
            ->add('pickAt', null, ['required' => false, 'disabled' => false])
            ->add('address', null, ['required' => false, 'disabled' => false])
            ->add('countLoader', null, ['disabled' => false])
            ->add('hydroBoard', null, ['disabled' => false])
            ->add('price', null, ['required' => false, 'disabled' => false])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('externalId')
            ->add('stock')
            ->add('type')
            ->add('active')
            ->add('pickAt')
            ->add('address')
            ->add('countLoader')
            ->add('hydroBoard')
            ->add('price')
        ;
    }
}
