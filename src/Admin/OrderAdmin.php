<?php

namespace App\Admin;

use App\Entity\Order;
use App\Entity\Shipping;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\Mover\MoverMapperService;
use App\Service\Mapper\OrderMapperService;
use App\Service\MoverService;
use App\Service\OrderUpdateService;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;

class OrderAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('user')
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('payed')
            ->add('insured')
            ->add('user')
            ->add('receptionAt')
            ->add('insuredDate')
            ->add('insuredCode')
            ->add('insuredNumber')
            ->add('status', ChoiceType::class, [
                'choices' => Order::getStatusList(),
            ])
            ->add('externalId')
            ->add('direction')
            ->add('utm')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->tab('Base')
                ->with('Base', ['class'       => 'col-md-12',])
                    ->add('payed', null, ['required' => false])
					->add('byCargo')
                    ->add('externalId', null, ['required' => false])
                    ->add('status', ChoiceType::class, [
                        'choices' => Order::getStatusList(),
                    ])
                    ->add('direction')
                    ->add('stockFrom')
                    ->add('stockTo')
                    ->add('needPayNotificationDate')
                    ->add('createdAt')
                    ->add('updatedAt')
                    ->end()
                ->end()
            ->tab('Price')
                ->with('Price', ['class'       => 'col-md-12',])
                    ->add('price')
                    ->add('declaredPrice')
                    ->add('suspendedPrice')
                    ->add('minPrice')
                    ->add('maxPrice')
                    ->add('promocode', null, ['required' => false])
                    ->add('first')
                ->end()
            ->end()
            ->tab('Persons')
                ->with('Persons', ['class'       => 'col-md-12',])
                    ->add('user')
                    ->add('sender')
                    ->add('receiver')
                    ->add('payer')
                ->end()
            ->end()
            ->tab('Admin')
                ->with('Admin', ['class'       => 'col-md-12',])
                    ->add('customerDeliveryTime', null, ['attr' => ['placeholder' => 'hh:ii:ss'], 'required' => false], [])
                    ->add('stopSend', null, ['required' => false])
                    ->add('pauseDelivery', null, ['required' => false])
                    ->add('editRecevier')
                    ->add('remove')
                ->end()
            ->end()
            ->tab('Miles')
                ->with('Miles', ['class'       => 'col-md-12',])
                    ->add('shippings', CollectionType::class, ['type_options' => ['delete' => false,]], [
                        'edit' => 'inline',
                        'sortable' => 'position',

                    ])
                ->end()
            ->end()
            ->tab('Files')
                ->with('Files', ['class'       => 'col-md-12',])
                ->add('files', CollectionType::class, array(
                    'by_reference' => true,
                    'btn_add' => true,
                ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                        'limit' => 10
                    )
                )
                ->end()
            ->end()
            ->tab('Insured')
                ->with('Insured', ['class'       => 'col-md-12',])
                    ->add('insured')
                    ->add('insuredDate')
                    ->add('insuredCode')
                    ->add('insuredNumber')
                    ->add('insuredPdf')
                ->end()
            ->end()

            ->tab('Packages')
                ->with('Packages', ['class'       => 'col-md-12',])
                ->add('packages', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => '',
                    'sortable' => 'position'
                ])
                ->end()
            ->end()
            ->tab('OrderPackages')
                ->with('OrderPackages', ['class'       => 'col-md-12',])
                ->add('orderPackages', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => '',
                    'sortable' => 'position'
                ])
                ->end()
            ->end()
            ->tab('Payments')
                ->with('Payments', ['class'       => 'col-md-12',])
                ->add('payments', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ])
                ->end()
            ->end()

            ->tab('Notification')
                ->with('Notification', ['class'       => 'col-md-12',])
                ->add('notifications', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ])
                ->end()
            ->end()

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    { 
        $showMapper
            ->add('id')
            ->add('status', ChoiceType::class, [
                'choices' => Order::getStatusList(),
            ])
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    public function preUpdate($entity)
    {
        $container = $this->getConfigurationPool()->getContainer();

        /** @var OrderUpdateService $orderUpdateService */
        $entityManager = $container->get('doctrine.orm.entity_manager');

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $container->get('app.order.update');

        /** @var MoverService $moverService */
        //$moverService = $container->get('app.mover');

        /** @var MoverMapperService $moverMapperService */
        $moverMapperService = $container->get('app.mover.mapper');

        /** @var Order $entity */
        $result = $orderUpdateService->orderSynch($entity);

        // Отправим уведомление в базу об остановке или возобновлении заказа
        if (!$entity->getSuspendDate() && $entity->getExternalId()) {
            $orderUpdateService->orderSuspend($entity);
        }

        if (!$entity->getPayed() && $entity->getExternalId()) {
            $orderUpdateService->orderSuspend($entity);
        }

        $shipping = $entity->getShipping(Shipping::TYPE_PICKUP);

        if ($shipping && $shipping->getProvider() == 'mover' && $shipping->isActive() && $shipping->getPickAt() && !$shipping->getExternalId()) {
            $result = $moverService->delivery($moverMapperService->calc($entity, $shipping));
            $shipping->setExternalId($result['id']);
            $shipping->setPrice(ceil($result['cost']));

            $entityManager->persist($entity);
            $entityManager->flush(); 
        }

        $shipping = $entity->getShipping(Shipping::TYPE_DELIVER);
        
        if ($shipping && $shipping->getProvider() == 'mover' && $shipping->isActive() && $shipping->getPickAt() && !$shipping->getExternalId()) {
            $result = $moverService->delivery($moverMapperService->calc($entity, $shipping));
            $shipping->setExternalId($result['id']);
            $shipping->setPrice(ceil($result['cost']));

            $entityManager->persist($entity);
            $entityManager->flush();
        }
    }

    public function getExportFields()
    {
        return [
            'id',
            'user',
            'status',
            'direction.title',
            'stockFrom.title',
            'stockTo.title',
            'totalPrice',
            'totalPriceShipping',
            'declaredPrice',
            'minPrice',
            'maxPrice',
            'promocode',
            'sender',
            'receiver',
            'payer',
            'insured',
            'insuredDate',
            'insuredNumber',
            'totalWeight',
            'totalVolume',
            'payed',
            'shippingPickUp.formatText',
            'shippingDeliver.formatText',
            'formatOrderPackages',
            'formatPackages',
            'utm.utmSource',
            'utm.utmMedium',
            'utm.utmCampaign',
            'utm.utmContent',
            'utm.utmTerm'
        ];
    }
}
