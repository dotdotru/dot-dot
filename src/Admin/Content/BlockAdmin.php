<?php

namespace App\Admin\Content;

use App\Entity\Content\Block;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BlockAdmin extends AbstractAdmin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $subclasses = $this->getSubClasses();
        $activeSubclass = '';
        if ($subject) {
            $activeSubclass = get_class($subject);
        }

        $subclass = array_search($activeSubclass, $subclasses);

        $blockTypes = Block::getTypes();
        $keys = array_keys($blockTypes);
        $key = reset($keys);
        $blockType = $blockTypes[$key];
        if ($subject && $subject->getType()) {
            $blockType = $subject->getType();
        }

        $formMapper
            ->add('type', ChoiceType::class, [
                'choices' => $blockTypes,
                'attr' => [
                    'class' => 'block-type'
                ]
            ])
            ->add('published', CheckboxType::class, [
                'required' => false,
            ])
        ;

        $formMapper
            ->add('heading', null, [
                'attr' => [
                    'data-block' => 1,
                    'data-types' => json_encode([Block::TYPE_HEADING, Block::TYPE_IMAGE, Block::TYPE_LINK])
                ]
            ]);

        $formMapper
            ->add('link', null, [
                'attr' => [
                    'data-block' => 1,
                    'data-types' => json_encode([Block::TYPE_LINK])
                ]
            ]);

        $formMapper
            ->add('subheading', null, [
                'attr' => [
                    'data-block' => 1,
                    'data-types' => json_encode([Block::TYPE_SUBHEADING])
                ]
            ]);

        $formMapper
            ->add('description', CKEditorType::class, [
                'attr' => [
                    'data-block' => 1,
                    'data-types' => json_encode([Block::TYPE_TEXT])
                ]
            ]);

        $formMapper
            ->add('picture', ModelListType::class, [
                'btn_edit' => false,
                'btn_delete' => false,
                'attr' => [
                    'data-block' => 1,
                    'data-types' => json_encode([Block::TYPE_IMAGE])
                ]
            ], [
                'link_parameters' => ['context' => 'content']
            ]);

        $formMapper
            ->add('position', IntegerType::class, [
                'attr' => [
                    'data-sort' => 1,
                ]
            ])
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
        ;
    }
}
