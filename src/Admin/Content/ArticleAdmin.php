<?php

namespace App\Admin\Content;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ArticleAdmin extends AbstractAdmin
{
    protected $datagridValues =
        [
            '_page' => 1,
            '_sort_order' => 'DESC',
            '_sort_by' => 'id',
        ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('published')
            ->add('title')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('published')
            ->addIdentifier('title')
            ->add('slug')
            ->add('tags')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Article')
            ->add('published')
            ->add('title')
            ->add('picturePreview', ModelListType::class, [], ['link_parameters' => ['context' => 'content']])
            ->add('preview', CKEditorType::class)
            ->add('picture', ModelListType::class, ['required' => false], ['link_parameters' => ['context' => 'content']])
            ->add('tags', ModelAutocompleteType::class, [
                'property' => 'title',
                'multiple' => true,
                'required' => false
            ])
            ->end()
            ->end()
            ->tab('Blocks')
                ->add('blocks', CollectionType::class, [
                        'by_reference' => true,
                        'attr' => ['class'=>'block-items']
                    ],
                    [
                        'allow_add' => true,
                        'allow_delete' => false,
                        'edit' => 'inline',
                        'inline' => 'natural',
                        'sortable'  => 'position',
                    ]
                )
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    public function configure() {
        $this->setTemplate('edit', 'admin\editWithCollection.html.twig');
    }
}
