<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class CityAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('slug')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('slug')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                    'edit_mile' => [
                        'template' => '/admin/city/list__mile_price.html.twig',
                    ]
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Основные')
                ->with('Основные', ['class'       => 'col-md-12',])
                    ->add('published')
                    ->add('title')
                    ->add('slug', null, ['required' => false])
                    ->add('region')
                    ->add('position')
                ->end()
            ->end()
            ->tab('Цены магистрали')
                ->with('Цены магистрали', ['class'       => 'col-md-12',])
                    ->add('pricesPerKilo', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()
            /*
            ->tab('Цены ИП')
                ->with('Цены ИП', ['class'       => 'col-md-12',])
                    ->add('priceBlocks', CollectionType::class, [], [
                        'edit' => 'inline',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()*/
            ->tab('Коды конкурентов')
                ->with('Коды конкурентов', ['class'       => 'col-md-12',])
                    ->add('cityCodes', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('slug')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
