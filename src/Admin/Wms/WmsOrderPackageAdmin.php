<?php

namespace App\Admin\Wms;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class WmsOrderPackageAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('weight')
            ->add('packing')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('order')
            ->add('size')
            ->add('weight')
            ->add('packing')
            ->add('packingPrice')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
            ->add('palletId')
            

            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			->add('id', null, ['disabled' => true])
            ->add('externalId', null, ['disabled' => true])
            ->add('status', null, ['disabled' => true])
            ->add('order')
            ->add('size')
            ->add('weight')
            ->add('packing')
            ->add('packingPrice')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
            ->add('damaged', null, ['required' => false])
            ->add('arriveDamaged', null, ['required' => false])
             ->add('palletId')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('size')
            ->add('weight')
            ->add('packing')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
        ;
    }
}
