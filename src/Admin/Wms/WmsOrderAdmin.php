<?php

namespace App\Admin\Wms;

use App\Entity\Order;
use App\Entity\Wms\WmsOrder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class WmsOrderAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('tmp')
            ->add('receptionAt')
            ->add('payed')
            ->add('insured')
            ->add('sender')
            ->add('status', ChoiceType::class, [
                'choices' => WmsOrder::getStatusList(),
            ])
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->tab('Base')
				->with('Base', ['class'       => 'col-md-12',])
					->add('payed', null, ['required' => false])
					->add('tmp', null, ['required' => false])
					->add('externalId', null, ['required' => false])
					->add('status', ChoiceType::class, [
						'choices' => Order::getStatusList(),
					])

					->add('byCargo')
					->add('direction')
					->add('stockFrom')
					->add('stockTo')
					//->add('needPayNotificationDate')
					//->add('receptionAt')
					->add('createdAt')
					->add('updatedAt')
				->end()
            ->end()
            ->tab('Отправитель')
                ->with('Отправитель', ['class'       => 'col-md-12',])
                    ->add('organization', null, ['required' => false])
                    ->add('sender', null, ['required' => false])
                    ->add('receiver', null, ['required' => false])
                    ->add('payer', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Price')
				->with('Price', ['class'       => 'col-md-12',])
					->add('price')
					->add('declaredPrice')
					->add('suspendedPrice')
					->add('minPrice')
					->add('maxPrice')
				->end()
            ->end()
            ->tab('Admin')
				->with('Admin', ['class'       => 'col-md-12',])
					->add('stopSend', null, ['required' => false])
					->add('pauseDelivery', null, ['required' => false])
					->add('editRecevier')
				->end()
            ->end()
            ->tab('Receiver')
				->with('Receiver', ['class'       => 'col-md-12',])
					->add('receiver', ModelType::class, ['required' => false])
				->end()
            ->end()
            ->tab('Files')
				->with('Files', ['class'       => 'col-md-12',])
					->add('files', CollectionType::class, array(
						'by_reference' => true,
						'btn_add' => true,
					),
						array(
							'edit' => 'inline',
							'inline' => 'table',
							'sortable' => 'position',
							'limit' => 10
						)
					)
				->end()
			->end()
            ->tab('Insured')
				->with('Insured', ['class'       => 'col-md-12',])
					->add('insured')
					->add('insuredDate')
					->add('insuredCode')
					->add('insuredNumber')
					->add('insuredPdf')
				->end()
            ->end()

            ->tab('Packages')
				->with('Packages', ['class'       => 'col-md-12',])
					->add('packages', CollectionType::class, [], [
						'edit' => 'inline',
						'inline' => '',
						'sortable' => 'position'
					])
				->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('status', ChoiceType::class, [
                'choices' => Order::getStatusList(),
            ])
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
