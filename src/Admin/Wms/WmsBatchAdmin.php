<?php

namespace App\Admin\Wms;

use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class WmsBatchAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('createdAt')
            ->add('organization')
            ->add('price', null)
            ->add('currentPricePerKilo', null)
            ->add('weight', null)
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Base')
                ->with('Base', ['class'       => 'col-md-12',])
                    ->add('externalId', null, ['required' => false])
                    ->add('status', ChoiceType::class, [
                        'choices' => WmsBatch::getStatusList(),
                    ])
                    ->add('direction', null, ['required' => false])
                    ->add('stockFrom', null, ['required' => false])
                    ->add('stockTo', null, ['required' => false])
                    
                    ->add('price', null, ['required' => false])
                    ->add('currentPricePerKilo', null, ['required' => false])
                    ->add('weight', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Перевозчик')
                ->with('Перевозчик', ['class'       => 'col-md-12',])
                    ->add('organization', null, ['required' => false])
                    ->add('drivers', null, ['required' => false])
                    ->add('driver', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Pallets')
                ->with('Pallets', ['class'       => 'col-md-12',])
                    ->add('pallets', CollectionType::class, array(
                        'by_reference' => true,
                        'btn_add' => true,
                    ),
                        array(
                            'edit' => 'inline',
                            'sortable' => 'position',
                            'limit' => 10
                        )
                    )
                ->end()
            ->end()
            ->tab('Files')
                ->with('Files', ['class'       => 'col-md-12',])
                    ->add('files', CollectionType::class, array(
                        'by_reference' => true,
                        'btn_add' => true,
                    ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'table',
                            'sortable' => 'position',
                            'limit' => 10
                        )
                    )
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
