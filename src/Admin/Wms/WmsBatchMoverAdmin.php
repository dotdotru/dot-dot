<?php

namespace App\Admin\Wms;

use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class WmsBatchMoverAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('createdAt')
            ->add('organization')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Base')
                ->with('Base', ['class'       => 'col-md-12',])
                    ->add('externalId', null, ['required' => false])
                    ->add('status', ChoiceType::class, [
                        'choices' => WmsBatchMover::getStatusList(),
                    ])
                    ->add('direction', null, ['required' => false])
					->add('order', null, ['required' => false])
                    ->add('stockFrom', null, ['required' => false])
                    ->add('stockTo', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Files')
                ->with('Files', ['class'       => 'col-md-12',])
                    ->add('files', CollectionType::class, array(
                        'by_reference' => true,
                        'btn_add' => true,
                    ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'table',
                            'sortable' => 'position',
                            'limit' => 10
                        )
                    )
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
