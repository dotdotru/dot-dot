<?php

namespace App\Admin\Wms;

use App\Entity\Order;
use App\Entity\Wms\WmsOrder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class WmsPalletAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('palletId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('palleted')
            ->add('weight')
            ->add('stockFrom')
            ->add('stockTo')
            ->add('packages', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => '',
                        'sortable' => 'position'
                    ])
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
            
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
			
            ->tab('Base')
           
                ->with('Base', ['class'       => 'col-md-12',])
					->add('id', null, ['disabled' => true])
                    ->add('direction', null, ['required' => false])
					->add('palleted', null, ['required' => false])
		    ->add('batch', null, ['required' => false])
                    ->add('stockFrom', null, ['required' => false])
                    ->add('stockTo', null, ['required' => false])
                    ->add('palletId', null, ['required' => false])
                    ->add('damaged', null, ['required' => false])
                    ->add('weight', null, ['required' => false])
                    ->add('arriveWeight', null, ['required' => false])

                    ->add('palletId', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Packages')
                ->with('Packages', ['class'       => 'col-md-12',])
                    ->add('packages', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => '',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()
            ->tab('realPackages')
                ->with('realPackages', ['class'       => 'col-md-12',])
                    ->add('realPackages', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => '',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
