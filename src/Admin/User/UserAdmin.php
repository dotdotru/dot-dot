<?php

namespace App\Admin\User;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends SonataUserAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    protected $baseRouteName = 'sonata_user';

    protected function configureDatagridFilters(\Sonata\AdminBundle\Datagrid\DatagridMapper $filterMapper): void
    {
        $filterMapper->add('id');
        $filterMapper->add('username', null, ['label' => 'Логин (номер телефона)']);
        $filterMapper->add('firstname', null, ['label' => 'ФИО']);
        $filterMapper->add('email');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        parent::configureFormFields($formMapper);

        $formMapper->remove('phone');
        $formMapper->remove('firstname');
        $formMapper->remove('dateOfBirth');
        $formMapper->remove('biography');
        $formMapper->remove('biography');
        $formMapper->remove('website');
        $formMapper->remove('locale');
        $formMapper->remove('gender');
        $formMapper->remove('lastname');
        $formMapper->remove('timezone');
        $formMapper->remove('facebookUid');
        $formMapper->remove('facebookName');
        $formMapper->remove('twitterUid');
        $formMapper->remove('twitterName');
        $formMapper->remove('gplusUid');
        $formMapper->remove('gplusName');


        $formMapper
            ->tab('User')
                ->with('Profile', ['class' => 'col-xs-6',])
                    ->add('externalId', null, ['required' => true, 'disabled' => true])
                    ->add('username', null, ['label' => 'Логин', 'required' => true])
                    ->add('phone', null, ['label' => 'Телефон', 'required' => true])
                    ->add('firstname', null, ['label' => 'ФИО', 'required' => false])
                    ->add('email', null, ['label' => 'Почта', 'required' => false])
                    ->add('series', null, ['label' => 'Паспорт серия', 'required' => false])
                    ->add('number', null, ['label' => 'Паспорт номер', 'required' => false])

                    ->add('person', null, ['label' => 'Персон', 'required' => false])
                ->end()
                ->with('Настройки', ['class' => 'col-xs-6',])
                    ->add('organizationCustomer', null, ['label' => 'Компания отправитель', 'required' => false])
                    ->add('organizationCarrier', null, ['label' => 'Компания получатель', 'required' => false])
                    ->add('subscribe', null, ['label' => 'Подписка', 'required' => false])
                ->end()
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('enabled', null, ['editable' => true])
            ->add('firstname', null, ['label' => 'Логин (номер телефона)'])
            ->add('username', null, ['label' => 'ФИО'])
            ->add('email')
            ->add('phone')
            ->add('roles')
            ->add('groups')
            ->add('utm')
            ->add('switchUser', 'string', ['template' => '/admin/switch_user.html.twig'])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                )
            ))
        ;
    }

    public function getExportFields()
    {


        return ['id', 'username', 'email', 'phone', 'createdAt', 'updatedAt', 'lastLogin', 'roleNow', 'number', 'series', 'utm.utmSource', 'utm.utmMedium', 'utm.utmCampaign', 'utm.utmContent', 'utm.utmTerm'];
    }

}
