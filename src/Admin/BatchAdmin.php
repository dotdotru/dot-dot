<?php

namespace App\Admin;

use App\Entity\Batch;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class BatchAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('carrier')
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('carrier')
            ->add('direction')
            ->add('remove')
            ->add('status')
            ->add('payed')
            ->add('externalId')
            ->add('utm')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Base')
                ->with('Base', ['class'       => 'col-md-12',])
                    ->add('status', ChoiceType::class, [
                        'choices' => Batch::getStatusList(),
                    ])
                    ->add('remove')
                    ->add('payed')
                    ->add('payedNotificationDate')
                    ->add('externalId', null, ['required' => false])
                    ->add('direction')
                    ->add('sendDate')
                    ->add('price')
                    ->add('currentPricePerKilo')
                    ->add('weight')
                    ->add('stockFrom')
                    ->add('stockTo')
                    ->add('stopSend')
                    ->add('carrier',  ModelType::class, ['required' => false,  'multiple' => false])
                    ->add('weights', ModelAutocompleteType::class, ['multiple' => true, 'property' => 'externalId', 'required' => false])
                ->end()
            ->end()
            ->tab('Drivers')
                ->with('Drivers', ['class'       => 'col-md-12',])
                    ->add('drivers', ModelAutocompleteType::class, ['multiple' => true, 'property' => 'name', 'required' => false])
                    ->end()
                ->end()
            ->tab('Files')
                ->with('Files', ['class'       => 'col-md-12',])
                    ->add('files', CollectionType::class, array(
                            'by_reference' => true,
                            'btn_add' => true,
                        ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'table',
                            'sortable' => 'position',
                            'limit' => 10
                        )
                    )
                ->end()
            ->end()

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    public function getExportFields()
    {
        return [
            'id',
            'carrier',
            'carrier',
            'status',
            'direction.title',
            'stockFrom.title',
            'stockTo.title',
            'sendDate',
            'price',
            'currentPricePerKilo',
            'weight',
            'driver',
            'payed',
            'utm.utmSource',
            'utm.utmMedium',
            'utm.utmCampaign',
            'utm.utmContent',
            'utm.utmTerm'
        ];
    }
}
