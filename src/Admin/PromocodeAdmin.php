<?php

namespace App\Admin;

use App\Entity\Promocode;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PromocodeAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('typeName')
            ->add('dateActiveFrom')
            ->add('dateActiveTo')
            ->add('user')
            ->add('discountKilo')
            ->add('discountDeclaredValue')
            ->add('percent')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('published')
            ->add('title')
            ->add('dateActiveFrom')
            ->add('dateActiveTo')
            ->add('user')
            ->add('discountKilo')
            ->add('discountDeclaredValue')
            ->add('percent')
            ->add('type')
            ->add('type', ChoiceType::class, [
                'choices' => Promocode::getTypesList(),
            ])
            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
        ;
    }
}
