<?php

namespace App\Admin;

use App\Entity\Packing;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ReceiverAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('uridical')
            ->add('type')
            ->add('companyName')
            ->add('inn')
            ->add('number')
            ->add('series')
            ->add('issuance')
            ->add('code')
            ->add('contactName')
            ->add('contactPhone')
            ->add('contactEmail')
            ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('uridical')
            ->add('type')
            ->add('companyName')
            ->add('inn')
            ->add('number')
            ->add('series')
            ->add('issuance')
            ->add('code')
            ->add('contactName')
            ->add('contactPhone')
            ->add('contactEmail')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('uridical')
            ->add('type')
            ->add('companyName')
            ->add('inn')
            ->add('number')
            ->add('series')
            ->add('issuance')
            ->add('code')
            ->add('contactName')
            ->add('contactPhone')
            ->add('contactEmail')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('uridical')
            ->add('type')
            ->add('companyName')
            ->add('inn')
            ->add('number')
            ->add('series')
            ->add('issuance')
            ->add('code')
            ->add('contactName')
            ->add('contactPhone')
            ->add('contactEmail')
        ;
    }
}
