<?php

namespace App\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DirectionAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('cityFrom')
            ->add('cityTo')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId', null, ['editable' => true])
            ->add('published')
            ->add('title')
            ->add('slug')
            ->add('cityFrom')
            ->add('cityTo')
            ->add('priorityStockFrom')
            ->add('priorityStockTo')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('cityFrom')
            ->add('cityTo')
            ->add('priorityStockFrom', null, [
                'label' => 'Склад для города отправления (первая миля)',
                'help' => 'Приоритетный склад в который будет привезен заказ, если есть первая миля. Если не задано, используется ближайший.',
                'choice_label' => 'address',
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('u');
                    $qb->andWhere($qb->expr()->eq('u.published', ':published'))
                        ->setParameter('published', true);
                    return $qb;
                },
            ])
            ->add('priorityStockTo', null, [
                'label' => 'Склад для города назначения (последняя миля)',
                'help' => 'Приоритетный склад в который будет привезен заказ, если есть последняя миля. Если не задано, используется ближайший.',
                'choice_label' => 'address',
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('u');
                    $qb->andWhere($qb->expr()->eq('u.published', ':published'))
                        ->setParameter('published', true);
                    return $qb;
                },
            ])
            ->add('startPrice')
            ->add('endPrice')
            ->add('priceUpPeriod')
            ->add('priceUpInPeriod')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('cityFrom')
            ->add('cityTo')
        ;
    }
}
