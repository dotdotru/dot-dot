<?php

namespace App\Admin;

use App\Entity\Email;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class EmailAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('fromEmail')
            ->add('toEmail')
            ->add('subject')
            ->add('body')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('status', ChoiceType::class, [
                'choices' => Email::getStatusList(),
                'attr' => ['readonly' => true, 'disabled' => true]
            ])
            ->add('fromEmail')
            ->add('toEmail')
            ->add('subject')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('status', ChoiceType::class, [
                'choices' => Email::getStatusList()
            ])
            ->add('fromEmail', null, [ 'attr' => ['readonly' => true]])
            ->add('toEmail', null, [ 'attr' => ['readonly' => true]])
            ->add('subject', null, [ 'attr' => ['readonly' => true]])
            ->add('body', null, [ 'attr' => ['readonly' => true]])
            ->add('filePaths')
            ->add('createdAt', null, ['attr' => ['readonly' => true, 'disabled' => true]])
            ->add('updatedAt', null, ['attr' => ['readonly' => true, 'disabled' => true]])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('status')
            ->add('fromEmail')
            ->add('toEmail')
            ->add('subject')
            ->add('body')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
