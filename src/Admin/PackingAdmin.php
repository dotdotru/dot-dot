<?php

namespace App\Admin;

use App\Entity\Packing;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PackingAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('published')
            ->add('externalId')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('published')
            ->add('title')
            ->add('size')
            ->add('price')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('published')
            ->add('title')
            ->add('externalId')
            ->add('size')
            ->add('price')
            ->add('minPrice')
            ->add('type', ChoiceType::class, array(
                'choices' => Packing::getTypes(),
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('published')
            ->add('externalId')
            ->add('weight')
            ->add('size')
            ->add('sizeToWeight', null, ['required' => false])
            ->add('calculatedWeight', null, ['required' => false])
            ->add('estimate', null, ['required' => false])
            ->add('stockFrom')
            ->add('stockTo')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
