<?php

namespace App\Admin\Base;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class WorkingCalendarAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('day')
            ->add('stock')
            ->add('startTime')
            ->add('endTime')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('day')
            ->add('stock')
            ->add('startTime')
            ->add('endTime')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('externalId', null, ['disabled' => true])
            ->add('day', DateType::class)
            ->add('stock')
            ->add('startTime')
            ->add('endTime')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('externalId')
            ->add('day')
            ->add('stock')
            ->add('startTime')
            ->add('endTime')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    private function getDowList()
    {
        $list = [];
        foreach (range(1, 7) as $dow) {
            $list[$dow] = $dow;
        }

        return $list;
    }
}
