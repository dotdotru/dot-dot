<?php

namespace App\Admin\Base;

use App\Entity\Base\HandlingPrice;
use App\Service\Integration\HandlingPrice\DelHandlingPrice;
use App\Service\Integration\HandlingPrice\SaveHandlingPrice;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Validator\ErrorElement;

class HandlingPriceAdmin extends AbstractAdmin
{


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('weightFrom')
            ->add('weightTo')
            ->add('price')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('weightFrom', null, ['label' => 'Вес ГМ от'])
            ->add('weightTo', null, ['label' => 'Вес ГМ до (включительно)'])
            ->add('price', null, ['label' => 'Стоимость обработки 1 кг с НДС'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('externalId', null, ['disabled' => true])
            ->add('weightFrom', null, ['label' => 'Вес ГМ от'])
            ->add('weightTo', null, ['label' => 'Вес ГМ до (включительно)'])
            ->add('price', null, ['label' => 'Стоимость обработки 1 кг с НДС'])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('weightFrom', null, ['label' => 'Вес ГМ от'])
            ->add('weightTo', null, ['label' => 'Вес ГМ до (включительно)'])
            ->add('price', null, ['label' => 'Стоимость обработки 1 кг с НДС'])
        ;
    }


    public function validate(ErrorElement $errorElement, $object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $handlingPrices = $em->getRepository(HandlingPrice::class)->findAll();

        foreach ($handlingPrices as $handlingPrice) {
            if ($object->getId() && $handlingPrice->getId() == $object->getId()) {
                continue;
            }

            if ($object->getWeightFrom() >= $handlingPrice->getWeightFrom()  && $object->getWeightFrom() < $handlingPrice->getWeightTo()) {
                $errorElement->with('weightFrom')->addViolation('Уже существует интервал для этого веса');
            }

            if ($object->getWeightTo() > $handlingPrice->getWeightFrom()  && $object->getWeightTo() <= $handlingPrice->getWeightTo()) {
                $errorElement->with('weightTo')->addViolation('Уже существует интервал для этого веса');
            }

        }
    }



    public function prePersist($entity)
    {
        $this->savePrice($entity);
    }

    public function preUpdate($entity)
    {
        $this->savePrice($entity);
    }

    public function preRemove($entity)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $entityManager = $container->get('doctrine.orm.entity_manager');
        $caller = $container->get('app.integration.caller');

        $delPriceRequest = new DelHandlingPrice();
        $delPriceRequest->setId($entity->getExternalId());

        $data = $caller->exec($delPriceRequest);
    }



    protected function savePrice ($entity)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $entityManager = $container->get('doctrine.orm.entity_manager');
        $caller = $container->get('app.integration.caller');

        $savePriceRequest = new SaveHandlingPrice();

        $weight = '(' . implode(',', [(int)$entity->getWeightFrom(), $entity->getWeightTo()]) . ']';

        $savePriceRequest->setId($entity->getExternalId());
        $savePriceRequest->setWeight($weight);
        $savePriceRequest->setPrice($entity->getPrice());

        $data = $caller->exec($savePriceRequest);

        if ($data['success']) {
            $entity->setExternalId($data['data']);
        } else {
            throw new \Exception($data['error']['message']);
        }
    }






}
