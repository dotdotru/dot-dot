<?php

namespace App\Admin\Base;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DirectionAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('cityFrom')
            ->add('cityTo')
            ->add('carrierDeliveryTime')
            ->add('customerDeliveryTime')
            ->add('reloadTime')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('cityFrom')
            ->add('cityTo')
            ->add('carrierDeliveryTime')
            ->add('customerDeliveryTime')
            ->add('reloadTime')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('externalId', null, ['disabled' => true])
            ->add('cityFrom')
            ->add('cityTo')
            ->add('carrierDeliveryTime')
            ->add('customerDeliveryTime')
            ->add('reloadTime')
            ->add('directionPrices', CollectionType::class, [], [
                'edit' => 'inline',
                'inline' => 'table'
            ])

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('cityFrom')
            ->add('cityTo')
            ->add('carrierDeliveryTime')
            ->add('customerDeliveryTime')
            ->add('reloadTime')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
