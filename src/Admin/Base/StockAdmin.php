<?php

namespace App\Admin\Base;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class StockAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('address')
            ->add('city')
            ->add('timezone')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('address')
            ->add('city')
            ->add('timezone')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('extSystem')
            ->add('ext_id')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $list = [];
        foreach (timezone_identifiers_list() as $value) {
            $list[$value] = $value;
        }

        $formMapper
            ->add('externalId', null, ['disabled' => true])
            ->add('address')
            ->add('city')
            ->add('timezone', ChoiceType::class, [
                'choices'  => $list
            ])
            ->add('code', null, ['required' => false])
            ->add('login', null, ['required' => false])
            ->add('pass', null, ['required' => false])
            ->add('extSystem')
            ->add('extId')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('externalId')
            ->add('address')
            ->add('city')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
