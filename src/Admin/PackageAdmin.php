<?php

namespace App\Admin;

use App\Entity\Package;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PackageAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('externalId')
            ->add('direction')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('externalId')
            ->add('weight')
            ->add('size')
            ->add('sizeToWeight', null, ['required' => false])
            ->add('calculatedWeight', null, ['required' => false])
            ->add('estimate', null, ['required' => false])
            ->add('direction')
            ->add('stockFrom')
            ->add('stockTo')
            ->add('packing')
            ->add('packingPrice')
            ->add('sender')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('externalId')
            ->add('weight')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('packing')
            ->add('packingPrice')
            ->add('size')
            ->add('count')
            ->add('status', ChoiceType::class, [
                'choices' => Package::getStatusList(),
            ])

            ->add('moverLastMileWeight')

            ->add('moverFirstMileDamaged')
            ->add('moverLastMileDamaged')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('externalId')
            ->add('weight')
            ->add('size')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
