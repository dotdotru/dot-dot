<?php

namespace App\Admin\Organization;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Sonata\CoreBundle\Form\Type\CollectionType;

class OrganizationDriverAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Addresses', ['class'       => 'col-md-12',])
                ->add('name')
                ->add('inn')
                ->add('birthdate', DateType::class, ['years' => range(1900, date('Y')),  'widget' => 'single_text',])
                ->add('phone')
            ->end()
            ->with('Verification', ['class'       => 'col-md-8',])
                ->add('verified')
                ->add('verifiedCode')
                ->add('verifiedDate', DateType::class,  ['widget' => 'single_text', 'required' => false])
            ->end()
            ->with('Documents', ['class'       => 'col-md-8',])
                ->add('documents', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ])
            ->end()
            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
        ;
    }
}
