<?php

namespace App\Admin\Organization;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Sonata\CoreBundle\Form\Type\CollectionType;

class OrganizationAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];
	
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('user')
            ->add('verified')
            ->add('verifiedCode')
            ->add('verifiedDate')
            ->add('type')
            ->add('companyName')
            ->add('ceo')
            ->add('address')
            ->add('contactName')
            ->add('contactPhone')
            ->add('rs')
            ->add('bik')
            ->add('createdAt')
            ->add('city')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General', ['class'       => 'col-md-12'])
                    ->add('user')
                    ->add('type')
                    ->add('companyName')
                    ->add('nds')
                    ->add('inn')
                    ->add('kpp')
                    ->add('ceo')
                    ->add('uridical')
                    ->add('address')
                    ->add('contactName')
                    ->add('contactPhone')
                    ->add('rs')
                    ->add('bik')
                    ->add('city')
                ->end()
            ->end()
            ->tab('Verification')
                ->with('General', ['class'       => 'col-md-12'])
                    ->add('verified')
                    ->add('verifiedCode')
                    ->add('verifiedDate', DateType::class,  ['widget' => 'single_text', 'required' => false])
                    ->add('verifiedNotificationDate', DateTimeType::class,  ['widget' => 'single_text', 'required' => false])
                ->end()
            ->end()
            ->tab('Drivers')
                ->with('Drivers', ['class'       => 'col-md-12',])
                    ->add('drivers', CollectionType::class, [], [
                        'edit' => 'inline',
                        'inline' => '',
                        'sortable' => 'position'
                    ])
                ->end()
            ->end()
            ->tab('Config')
                ->with('Config', ['class'       => 'col-md-12',])
                    ->add('innerCarrier')
                    ->add('withoutDrivers')
                ->end()
            ->end()



            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
        ;
    }
}
