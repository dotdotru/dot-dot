<?php

namespace App\Admin\Trace;

use App\Entity\Trace\Trace;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VerificationTraceAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('organization')
            ->add('driver')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('status', ChoiceType::class, [
                'choices' => Trace::getStatusList(),
            ])
            ->add('organization')
            ->add('driver')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array()
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Параметры')
                ->with('Основное')
                    ->add('status', ChoiceType::class, [
                        'choices' => Trace::getStatusList(),
                    ])
                    ->add('organization')
                    ->add('driver')
                ->end()
            ->end()
            ->tab('Запросы')
            ->with('Основное')
                ->add('items', CollectionType::class, ['disabled' => true], [
                    'edit' => 'inline',
                    'sortable' => 'position',
                    'disabled' => false
                ])
            ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')

        ;
    }
}
