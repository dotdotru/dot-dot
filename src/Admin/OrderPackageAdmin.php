<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderPackageAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('weight')
            ->add('packing')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('size')
            ->add('calculateWeight')
            ->add('weight')
            ->add('packing')
            ->add('packingPrice')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('size')
            ->add('weight')
            ->add('calculateWeight')
            ->add('packing')
            ->add('packingPrice')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')

            ->add('moverFirstMileWeight')
            ->add('moverLastMileWeight')

            ->add('moverFirstMileDamaged')
            ->add('moverLastMileDamaged')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('size')
            ->add('weight')
            ->add('packing')
            ->add('packageType')
            ->add('packageTypeAnother')
            ->add('count')
        ;
    }
}
