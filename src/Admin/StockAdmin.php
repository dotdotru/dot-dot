<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StockAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('slug')
            ->add('city')
            ->add('groups')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('slug')
            ->add('city')
            ->add('groups')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('slug', null, ['required' => false])
            ->add('city')
            ->add('address', null, ['required' => false])
            ->add('mode', null, ['required' => false])
            ->add('phone')
            ->add('lat')
            ->add('groups')
            ->add('lng')
            ->add('howToGet')

            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('published')
            ->add('externalId')
            ->add('title')
            ->add('slug')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
