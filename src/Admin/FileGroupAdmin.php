<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class FileGroupAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('slug')
            ->add('defaultCustomer')
            ->add('defaultCarrier')
            ->add('defaultWmsOrder')
            ->add('defaultWmsBatch')
            ->add('showTitle')
            ->add('showList')
            ->add('hidden')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Base')
                ->with('Base', ['class'       => 'col-md-12',])
                    ->add('title')
                    ->add('slug')
                    ->add('defaultCustomer')
                    ->add('defaultCarrier')
                    ->add('defaultWmsOrder')
                    ->add('defaultWmsBatch')
                    ->add('showTitle')
                    ->add('showList')
                    ->add('hidden')
                    ->add('position')
                    ->add('createdAt')
                    ->add('updatedAt')
                ->end()
            ->end()
        ->tab('Files')
            ->with('Files', ['class'       => 'col-md-12',])
                ->add('files', CollectionType::class, array(
                    'by_reference' => true,
                    'btn_add' => true,
                ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                        'limit' => 10
                    )
                )
            ->end()
        ->end();
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
