<?php

namespace App\Admin;

use App\Entity\DispatchLog;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;

class DispatchLogAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('event')
            ->add('ip')
            ->add('page')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('id')
            ->add('user')
            ->add('userFio')
            ->add('userEmail')
            ->add('userPhone')
            ->add('event', ChoiceType::class, [
                'choices' => DispatchLog::getEventList(),
                'attr' => ['readonly' => true, 'disabled' => true]
            ])
            ->add('ip')
            ->add('page')
            ->add('createdAt')
        ;
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action,$object);

        $list['downloadSubscribe'] = array(
            'template' =>  'admin/custom_button.html.twig',
        );

        return $list;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('downloadSubscribe')
            ->remove('delete')
        ;
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
        ;
    }

    public function getExportFields()
    {
        //['id', 'user.firstname', 'user.email', 'user.username', 'event', 'ip', 'page', 'createdAt']
        return ['id', 'user.firstname', 'user.email', 'user.username', 'event', 'ip', 'page', 'createdAt'];
    }
}
