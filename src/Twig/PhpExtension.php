<?php

namespace App\Twig;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Person;
use App\Repository\Organization\OrganizationRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class PhpExtension extends \Twig_Extension
{

    /** @var EntityManager  */
    private $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ceil', array($this, 'ceil')),
            new \Twig_SimpleFilter('formatPhoneLink', array($this, 'formatPhoneLink')),
            new \Twig_SimpleFilter('formatPhone', array($this, 'formatPhone')),
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
            new \Twig_SimpleFilter('nformat', array($this, 'numberFormat')),
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_Function('format_user', array($this, 'formatUser')),
            new \Twig_Function('format_person', array($this, 'formatPerson')),
            new \Twig_Function('format_driver', array($this, 'formatDriver')),
        );
    }

    public function numberFormat($number)
    {
        $number = floatval($number);

        if ($number < 0.001) {
            $number = sprintf('%f', $number);
        } else {
            $number = round($number, 4);
        }

        return $number;
    }

    public function ceil($number)
    {
        return ceil($number);
    }

    private function clearPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        return $phone;
    }

    public function formatPhoneLink($phone)
    {
        $phone = $this->clearPhone($phone);
        if (in_array(substr($phone, 0, 1), array(7,8)) || strlen($phone) == 10) {
            $phone = '+7'.substr($phone, 1, strlen($phone));
        }
        return $phone;
    }

    public function formatPhone($phone)
    {
        $phone = $this->clearPhone($phone);
        if (in_array(substr($phone, 0, 1), array(7))) {
			$phone = substr($phone, 1, 11);
            $phone = '+7 ('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone, 6, 2).'-'.substr($phone, 8, 2);
        } else {
			$phone = '+7 ('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone, 6, 2).'-'.substr($phone, 8, 2);
		}

        return $phone;
    }


    public function formatUser (User $user = null, $type = 'customer', $mode = 'full')
    {
        if (!$user) {
            return '';
        }

        $contact = '';
        if ($user->isPrivatePerson() && $type == 'customer') {
            $contactName = $user->getFirstname();
            $contactPhone = $this->formatPhone($user->getUsername());
            $contactEmail = $user->getEmail();

            if ($mode == 'full') {
                $contact = sprintf('%s, %s, %s', $contactName, $contactPhone, $contactEmail);
            }
            if ($mode == 'short') {
                $contact = sprintf('%s', $contactName);
            }
        } else {
            $organization = $type == 'customer' ? $user->getOrganizationCustomer() : $user->getOrganizationCarrier();

            if ($organization) {
                $contactName = $organization->getContactName();
                $contactEmail = $user->getEmail();
                $contactPhone = $this->formatPhone($organization->getContactPhone());

                if ($mode == 'full') {
                    $contact = sprintf('%s, %s %s %s', $organization->getType() . ' «' . $organization->getCompanyName() . '»', $contactName, $contactPhone, $contactEmail);
                }
                if ($mode == 'short') {
                    $contact = sprintf('%s', $organization->getType() . ' «' . $organization->getCompanyName() . '»');
                }
            }
        }

        return $contact;
    }


    public function formatPerson (Person $person = null, $mode = 'full')
    {
        if (!$person) {
            return '';
        }

        $contact = '';
        if ($person && $person->isUridical() == false) {
            $contactName = $person->getContactName();
            $contactPhone = $this->formatPhone($person->getContactPhone());
            $contactEmail = $person->getContactEmail();
            $contactPassport = $person->getSeries().' '.$person->getNumber();

            $contact = sprintf('%s, %s, %s. Паспорт %s', $contactName, $contactPhone, $contactEmail, $contactPassport);
        } elseif($person) {
            $contactName = $person->getContactName();
            $contactPhone = $this->formatPhone($person->getContactPhone());
            $contactEmail = $person->getContactEmail();

            if ($mode == 'full') {
                $contact = sprintf('%s, %s %s %s', $person->getType(). ' «' . $person->getCompanyName() . '»', $contactName, $contactPhone, $contactEmail);
            }
            if ($mode == 'short') {
                $contact = sprintf('%s', $person->getType() . ' "' . $person->getCompanyName() . '"');
            }
        }

        return $contact;
    }


    public function formatDriver (OrganizationDriver $driver = null, $mode = 'full')
    {
        if (!$driver) {
            return '';
        }

        $driverName = $driver->getName();
        $driverPhone = $driver->getPhone();

        $driverDocument = '';
        /** @var Document $document */
        foreach ($driver->getDocuments() as $document) {
            if ($document->getType() == 'passport') {
                $driverDocument .= $document->getSeries() . ' ' . $document->getNumber();
            }
            if (false && $document->getType() == 'license') {
                $driverDocument .= $document->getSeries() . ' ' . $document->getNumber();
            }
        }

        if ($mode == 'short') {
            return sprintf('%s (паспорт %s)', $driverName, $driverDocument);
        }

        return sprintf('%s  %s (паспорт %s)', $driverName, $driverPhone, $driverDocument);
    }


    public function jsonDecode($str) {
        return json_decode($str);
    }

    public function getName()
    {
        return 'php_extension';
    }
}
