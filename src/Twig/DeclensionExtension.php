<?php

namespace App\Twig;

use morphos\Russian;

class DeclensionExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('declOfNum', array($this, 'declOfNum')),
            new \Twig_SimpleFilter('declOfNumRooms', array($this, 'declOfNumRooms')),
            new \Twig_SimpleFilter('numberInWords', array($this, 'numberInWords')),
        );
    }

    public function declOfNum($number, $title)
    {
        return Russian\pluralize($number, $title) ;
    }

    function declOfNumRooms($number, $title = 'комната')
    {
        return Russian\pluralize($number, $title) ;
    }

    function numberInWords($number)
    {
        return Russian\CardinalNumeralGenerator::getCase($number, 'именительный');
    }

    public function getName()
    {
        return 'declension_extension';
    }
}