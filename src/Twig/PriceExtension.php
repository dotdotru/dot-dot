<?php

namespace App\Twig;

use App\Entity\Currency\RedCodeCurrency;

class PriceExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('formatPrice', array($this, 'priceFilter')),
            new \Twig_SimpleFilter('formatNumber', array($this, 'numberFilter'))
        );
    }

    public function numberFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ' ')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        return $price;
    }

    public function priceFilter($number, $currency = 'RUB', $decimals = 0, $decPoint = '.', $thousandsSep = ' ')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        return $this->getCurrencyView($price, $currency);
    }

    private function getCurrencyView($price, $currencyCode)
    {
        $return = $price;
        switch ($currencyCode) {
            case 'USD':
                $return = '$' . $price;
                break;
            case 'EUR':
                $return = $price . ' €';
                break;
            case 'RUB':
                $return = $price . ' ₽';
                break;
            case 'GBP':
                $return = $price . ' ￡';
                break;
        }
        return $return;
    }

    public function getName()
    {
        return 'currency_extension';
    }
}