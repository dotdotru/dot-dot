<?php
namespace App\EventListener;

use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\Batch;

use App\Entity\InsuredQueue;

use App\Entity\Document;
use App\Entity\File;
use App\Entity\Order;
use App\Entity\Package;
use App\Entity\Promocode;
use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use App\Repository\OrderRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\CalculateService;
use App\Service\FileUpdateService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\OrderMapperService;
use App\Service\Mapper\UserMapperService;
use App\Service\NotificationService;
use App\Service\Order\OrderNotificationService;
use App\Service\OrderUpdateService;
use App\Service\PromocodeService;
use App\Service\SmsService;
use App\Service\User\UserVatService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Sonata\MediaBundle\Provider\ImageProvider;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\Router;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;
use App\Entity\Organization\Organization;
use App\Repository\Organization\OrganizationRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var IntegrationBaseService
     */
    protected $integrationBaseService;

    /**
     * @var OrderMapperService
     */
    protected $orderMapperService;

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var SmsService
     */
    protected $smsService;

    /**
     * @var MailService
     */
    protected $mailService;

    /** @var OrderRepository */
    protected $orderCustomerRepository;

    /** @var PromocodeService */
    protected $promocodeService;

    /** @var CalculateService */
    protected $calculateService;

    /** @var FileUpdateService */
    protected $fileUpdateService;

    /**
     * @var OrderNotificationService
     */
    protected $orderNotificationService;

    /**
     * @var Router
     */
    protected $router;

    public function __construct(EntityManager $entityManager, Container $container, UserVatService $userVatService, OrderNotificationService $orderNotificationService)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->userVatService = $userVatService;
        $this->orderNotificationService = $orderNotificationService;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->integrationBaseService = $this->container->get('app.integration_base');
        $this->orderMapperService = $this->container->get('app.order.mapper');
        $this->smsService = $this->container->get('app.sms');
        $this->mailService = $this->container->get('app.mail');
        $this->settings = $this->container->get('settings');
        $this->router = $this->container->get('router');
        $this->fileUpdateService = $this->container->get('app.file.update');

        /** @var OrderUpdateService $orderUpdateService */
        $this->orderUpdateService = $this->container->get('app.order.update');

        /** @var OrderRepository orderCustomerRepository */
        $this->orderCustomerRepository = $this->container->get('doctrine')->getRepository(Order::class);

        /** @var OrganizationRepository organizationRepository */
        $this->organizationRepository = $this->container->get('doctrine')->getRepository(Organization::class);

        /** @var WmsOrderRepository $wmsOrderRepository */
        $this->wmsOrderRepository = $this->container->get('doctrine')->getRepository(WmsOrder::class);

        /** @var PromocodeService promocodeService */
        $this->promocodeService = $this->container->get('app.promocode');

        /** @var CalculateService $calculateService */
        $this->calculateService = $this->container->get('app.calculate');

        /** @var NotificationService $this->notificationService */
        $this->notificationService = $this->container->get('app.notification');

        $this->recalcaulateOrderService = $this->container->get('app.service.order.recalculate');

        $entity = $args->getEntity();

        /** @var Order $entity */
        if ($entity instanceof Order) {
            if ($entity->getStatus() == Order::STATUS_BOOKED && $entity->getUser()) {
                //$this->orderUpdateService->orderSynch($entity);
            }

            if ($entity) {
                /** @var WmsOrder $wmsOrder */
                $wmsOrder = $this->wmsOrderRepository->findOneByExternalId($entity->getId());

                if ($wmsOrder && ($wmsOrder->isWaiting() || $wmsOrder->isCreated())) {
                    $wmsOrder->setByCargo($entity->isByCargo());
                    $this->entityManager->persist($wmsOrder);
                    $this->entityManager->flush();
                }
            }

            $cdt = $entity->getCustomerDeliveryTime();
            if ($cdt && !preg_match('/([0-9]){2,3}:([0-9]){2}:([0-9]){2}/', $cdt)) {
                $entity->setCustomerDeliveryTime('');
            }

            if ($entity->getUser()) {
                $orders = $this->orderCustomerRepository->getByUserForPromocode($entity->getUser());

				$applyPromocode = false;
				foreach($orders as $order) {
					if ($order->getPromocode() && ($order->getPromocode()->getType() == Promocode::TYPE_FIRSTREGISTER) && $order->isRemove() == false) {
						$applyPromocode = true;
					}
				}

				if (count($orders) == 1) {
					$entity->setFirst(true);
				}

				if (!$applyPromocode) {
					/**
					 * Нужно выбрать два последних заказа, т.к один уже почти создан и подняться на один выше
					 * @var Order $lastOrder
					 */
					$lastOrder = $this->orderCustomerRepository->findBy(
						['user' => $entity->getUser()],
						['id' => 'desc'],
						2
					);
					$lastOrder = isset($lastOrder[1]) ? $lastOrder[1] : null;

					// Когда создаем новый заказ, проверим был ли удален заказ с промокодом и пересчитаем автоматически ниже
					if ($lastOrder && $lastOrder->isRemove() &&
						$lastOrder->getPromocode() == $this->promocodeService->getFirstOrderPromocode() &&
						($lastOrder->getStatus() == Order::STATUS_BOOKED || $lastOrder->getStatus() == Order::STATUS_WAITING) &&
						$entity->isCreated()
					) {
						$entity->setFirst(true);
					}
				}
            }




            /* WMS FIX */
            /** @var WmsOrder $wmsOrder */
            $wmsOrder = $this->wmsOrderRepository->findOneByExternalId($entity->getId());

            if ($wmsOrder && $entity->getPrice()) {
                $wmsOrder->setPrice($entity->getPrice());
                $this->entityManager->persist($wmsOrder);
                $this->entityManager->flush();
            }

            // Отправка смс / email нотификации об необходимости оплаты
            $notificationExternalId = 'need_pay_order';
            if (!$entity->isPayed()
                && $entity->getUser()
                && $entity->getStatus() == Order::STATUS_NOTPAID
                && !$entity->existNotification($notificationExternalId)
            ) {
                $notification = $this->notificationService->createNotification($notificationExternalId);
                $entity->addNotification($notification);

                if ($entity->getUser()->getUserPerson() && $entity->getUser()->getUserPerson()->isUridical() == false && $entity->getPayer()->isUridical() == false) {
                    $linkToPay = $this->router->generate('app.personal.order_pay', ['orderId' => $entity->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
                    $linkToOrder = $this->router->generate('app.personal.index', ['orderId' => $entity->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
                    $this->mailService->registerEmail(
                        'need_order_pay',
                        [
                            $entity->getPayer()->getEmail(),
                            $entity->getUser()->getEmail()
                        ],
                        [
                            'entity' => $entity,
                            'link' => $linkToOrder,
                            'linkToPay' => $linkToPay
                        ]
                    );

                    $settings = $this->settings->group('general');
					if (isset($settings['sms_need_pay_order'])) {
						$text = $settings['sms_need_pay_order'];
						$text = str_replace('#NUMBER#', $entity->getId(), $text);
						$text = str_replace('#LINKTOPAY#', $linkToPay, $text);

						$this->smsService->send(
							$entity->getPayer()->getPhone(),
							$text
						);
					}
                } else {
                    $this->mailService->registerEmail(
                        'need_order_pay_uridical',
                        [
                            $entity->getPayer()->getEmail(),
                            $entity->getUser()->getEmail()
                        ],
                        ['entity' => $entity]
                    );
                }

                $this->orderNotificationService->notificationNeedPay($entity);

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            $notificationExternalId = 'payed_order';
            // Отправка смс нотификации об оплате
            if (
                $entity->isPayed()
                && $entity->getUser()
                && !$entity->existNotification($notificationExternalId)
            ) {
                $notification = $this->notificationService->createNotification($notificationExternalId);
                $entity->addNotification($notification);

                $settings = $this->settings->group('general');
                $this->smsService->send(
                    [$entity->getPayer()->getPhone(), $entity->getUser()->getPhone()],
                    str_replace('#NUMBER#', $entity->getId(), $settings['sms_order_customer_pay'])
                );

                $wmsOrder = $this->wmsOrderRepository->findOneByExternalId($entity->getId());

                if ($wmsOrder) {
                    $wmsOrder->setPayed($entity->isPayed());
                    $wmsOrder->setStatus(WmsOrder::STATUS_PAID);
                    $this->entityManager->persist($wmsOrder);
                }

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            $notificationExternalId = 'create_order';
            if (
                $entity->getStatus() == Order::STATUS_WAITING
                && !$entity->existNotification($notificationExternalId)
                && $entity->getUser()
                && $entity->getExternalId()
                && $entity->getStockFrom()
            ) {
                $adminSettings = $this->settings->group('admin');

                if (isset($adminSettings['order_notification_email'])) {
                    $createdAt = ($entity->getCreatedAt())?$entity->getCreatedAt()->format('Y-m-d H:i:s'):'';
                    $orderType = "";
                    if($entity->getPackagesTypes()) {
                        $orderType = implode(",", $entity->getPackagesTypes());
                    }

                    $orderNotificationParams = [
                        'entity' => $entity,
                        'STOCKFROM_ID' => $entity->getStockFrom()->getId(),
                        'CARGO_PRICE' => $entity->getDeclaredPrice(),
                        'CARGO_TYPE' => $orderType,
                        'STOCKFROM_ADDRESS' => $entity->getStockFrom()->getAddress(),
                        'STOCKTO_ADDRESS' => $entity->getStockTo()->getAddress(),
                        'STOCKTO_ID' => $entity->getStockTo()->getId(),
                        'TOTAL_WEIGHT' => $entity->getTotalWeight(),
                        'PLACES_COUNT' => $entity->getTotalCount(),
                        'SENDER_LOGIN' => $entity->getUser()->getUserName(),
                        'SENDER_NAME' => '',
                        'SENDER_PHONE' => '',
                        'RECEIVER_NAME' => '',
                        'RECEIVER_PHONE' => '',
                        'TIME_CREATE' => $createdAt
                    ];

                    if ($entity->getUser()) {
                        $senderOrganization = $this->organizationRepository->getByUser($entity->getUser());
                        $senderName = $entity->getUser()->getFirstName();
                        $senderPhone = $entity->getUser()->getPhone();
                        if($senderOrganization) {
                            $senderName = $senderOrganization->getCompanyName();
                            $senderName = $senderName." , ".$senderOrganization->getContactName();
                        }
                        $orderNotificationParams['SENDER_NAME'] = $senderName;
                        $orderNotificationParams['SENDER_PHONE'] = $senderPhone;
                    }

                    if($entity->getReceiver()) {
                        $receiverName = $entity->getReceiver()->getContactName();
                        if($entity->getReceiver()->getCompanyName() && $entity->getReceiver()->isUridical()) {
                            $receiverName = $entity->getReceiver()->getCompanyName()." ,".$receiverName;
                        }
                        $orderNotificationParams['RECEIVER_NAME'] = $receiverName;
                        $orderNotificationParams['RECEIVER_PHONE'] = $entity->getReceiver()->getContactPhone();
                    }

                    $this->mailService->registerEmail(
                        'admin_order_notification',
                        $adminSettings['order_notification_email'],
                        $orderNotificationParams
                    );
                }

				$shipping = $entity->getShipping(Shipping::TYPE_PICKUP);
				if (!$shipping || ($shipping && $shipping->isActive() == false)) {
					$this->mailService->registerEmail(
						'waiting3',
						[$entity->getUser()->getEmail(), $entity->getSender()->getEmail()],
						['entity' => $entity, 'STOCKFROM_ADDRESS' => $entity->getStockFrom()->getAddress()]
					);
				}

				$notification = $this->notificationService->createNotification($notificationExternalId);
				$entity->addNotification($notification);

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }


            // Отправка уведомления о необходимости выбрать адрес
            $notificationExternalId = 'need_set_datetime_lastmile';
            $shipping = $entity->getShipping(Shipping::TYPE_DELIVER);
            if ($shipping && !$shipping->getPickAt() && $entity->isEditDeliver()
                && !$entity->existNotification($notificationExternalId) && $entity->getPayed()
            ) {
                $notification = $this->notificationService->createNotification($notificationExternalId);
                $entity->addNotification($notification);

                $orderRecipient = $entity->getUser()->getEmail();

                $this->mailService->registerEmail(
                    $notificationExternalId,
                    $orderRecipient,
                    [
                        'id' => $entity->getId(),
                        'link' => 'https://dot-dot.ru/' . trim($this->router->generate('app.personal.index', ['group' => 'sender', 'orderId' => 100]), '/')
                    ]
                );

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            /** @var File $file */
            foreach ($entity->getFiles() as $file) {
                if (!$file->getExternalId() && $file->getMedia()) {
                    $file = $this->fileUpdateService->fileSynch($file);
                }
            }
        }

        /** @var File $entity */
        if ($entity instanceof File) {
            //$file = $this->fileUpdateService->fileSynch($entity);
        }
    }
}
