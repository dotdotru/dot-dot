<?php
namespace App\EventListener;

use App\Entity\Package;
use App\Entity\Stock;
use App\Service\CalculateService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PackageSubscriber implements EventSubscriber
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CalculateService
     */
    private $calculateService;

    public function __construct(EntityManagerInterface $entityManager, CalculateService $calculateService)
    {
        $this->entityManager = $entityManager;
        $this->calculateService = $calculateService;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $package = $args->getEntity();
    }
}