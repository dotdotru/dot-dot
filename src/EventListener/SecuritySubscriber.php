<?php

namespace App\EventListener;

use App\Entity\Batch;
use App\Entity\Order;
use App\Application\Sonata\UserBundle\Document\User;
use App\Service\Order\RecalculateOrderPriceService;
use App\Service\User\UserVatService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class SecuritySubscriber implements EventSubscriberInterface
{
    private $entityManager;
    private $tokenStorage;
    private $authenticationUtils;
    protected $userVatService;
    protected $recalculateOrderPriceService;

    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        AuthenticationUtils $authenticationUtils,
        SessionInterface $session,
        UserVatService $userVatService,
        RecalculateOrderPriceService $recalculateOrderPriceService
    )
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->authenticationUtils = $authenticationUtils;
        $this->session = $session;
        $this->userVatService = $userVatService;
        $this->recalculateOrderPriceService = $recalculateOrderPriceService;
    }

    public static function getSubscribedEvents()
    {
        return array(
            //AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        );
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $username = $this->authenticationUtils->getLastUsername();
        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($existingUser) {
            error_log("Log In Denied: Wrong password for User #" . $existingUser->getId()  . " (" . $existingUser->getEmail() . ")");
        } else {
            error_log("Log In Denied: User doesn't exist: " . $username);
        }
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $customerOrderId = $this->session->get('CUSTOMER_ORDER_ID');
        if ($customerOrderId > 0) {
            $customerOrder = $this->entityManager->getRepository(Order::class)->findOneBy(['id' => $customerOrderId]);


            $orderVat = false;
            if ($this->session->has('CUSTOMER_ORDER_VAT')) {
                $orderVat = $this->session->get('CUSTOMER_ORDER_VAT');
            }

            if ($customerOrder->getUser() == null && $user->isCustomer()) {
                $customerOrder->setUser($user);

                // Если в предрасчете было выбрано неправильное НО, перерасчитываем
                $userVat = false;
                if ($user instanceof \App\Application\Sonata\UserBundle\Entity\User || is_subclass_of($user, \App\Application\Sonata\UserBundle\Entity\User::class)) {
                    $userVat = $this->userVatService->get($user);
                }

                if ($orderVat !== $userVat) {
                    $this->recalculateOrderPriceService->recalculate($customerOrder, $userVat);
                    $this->session->set('CUSTOMER_ORDER_MESSAGE', 'При создании заказа №' . $customerOrder->getId() . ' было указано неправильное налогообложение. Цена была пересчитана.');
                }

                if ($user->isCustomer()) {
                    $user->setCustomerNow();
                }
                $this->entityManager->persist($user);

                $this->entityManager->persist($customerOrder);
                $this->entityManager->flush();
                $this->session->remove('CUSTOMER_ORDER_ID');
                $this->session->remove('CUSTOMER_ORDER_VAT');
            }

        }

        $orderId = $this->session->get('ORDER_ID');
        if ($orderId > 0) {
            /** @var Batch $order */
            $order = $this->entityManager->getRepository(Batch::class)->findOneBy(['id' => $orderId]);

            $orderVat = false;
            if ($this->session->has('ORDER_VAT')) {
                $orderVat = $this->session->get('ORDER_VAT');
            }

            if ($order->getCarrier() == null) {
                // Если в предрасчете было выбрано неправильное НО, перерасчитываем
                $userVat = false;
                if ($user instanceof \App\Application\Sonata\UserBundle\Entity\User || is_subclass_of($user, \App\Application\Sonata\UserBundle\Entity\User::class)) {
                    $userVat = $this->userVatService->get($user);
                }

                if ($orderVat !== $userVat) {
                    $this->session->set('ORDER_MESSAGE', 'При резервировании партии было указано неправильное налогообложение. Партия была отменена.');
                } else {
                    $order->setCarrier($user);
                }

                $this->entityManager->persist($user);

                $this->entityManager->persist($order);
                $this->entityManager->flush();
                $this->session->remove('ORDER_ID');
            }
        }
    }
}