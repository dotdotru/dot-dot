<?php

namespace App\EventListener;

use App\Service\Ad\UtmService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RouteSubscriber implements EventSubscriberInterface
{
    protected $utmService;

    public function __construct(UtmService $utmService)
    {
        $this->utmService = $utmService;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest'
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $url = $event->getRequest()->getRequestUri();

        if ($this->utmService->isUrlWithUtm($url)) {
            $this->utmService->saveUrl($url);
        }
    }
}