<?php

namespace App\EventListener\Sitemap;

use App\Entity\Content\Article;
use App\Entity\Page;
use App\Entity\Direction;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapSubscriber implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param PageRepository    $pageRepository
     */
    public function __construct(UrlGeneratorInterface $urlGenerator, EntityManagerInterface $entityManager)
    {
        $this->urlGenerator = $urlGenerator;
        $this->pageRepository = $entityManager->getRepository(Page::class);
        $this->articleRepository = $entityManager->getRepository(Article::class);
        $this->directionRepository = $entityManager->getRepository(Direction::class);
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'populate',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function populate(SitemapPopulateEvent $event): void
    {
        $this->registerPageUrls($event->getUrlContainer());
    }

    /**
     * @param UrlContainerInterface $urls
     */
    public function registerPageUrls(UrlContainerInterface $urls): void
    {
        $pages = $this->pageRepository->findAll();

        /** @var Page $page */
        foreach ($pages as $page) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.page.default',
                        ['slug' => $page->getSlug()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    $page->getUpdatedAt(),
                    null,
                    0.6
                ),
                'page'
            );
        }

        $articles = $this->articleRepository->findAll();

        /** @var Article $article */
        foreach ($articles as $article) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.article.detail',
                        ['code' => $article->getSlug()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    $article->getUpdatedAt(),
                    null,
                    0.6
                ),
                'article'
            );
        }
        
        $directions = $this->directionRepository->findBy(['published' => true]);
        foreach ($directions as $direction) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.default.direction',
                        ['slug' => $direction->getSlug()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    $direction->getUpdatedAt(),
                    null,
                    0.6
                ),
                'direction'
            );
        }
        
        foreach ($directions as $direction) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.auction.direction',
                        ['slug' => $direction->getSlug()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    $direction->getUpdatedAt(),
                    null,
                    0.6
                ),
                'direction'
            );
        }
    }
}
