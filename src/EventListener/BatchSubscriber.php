<?php
namespace App\EventListener;

use App\Entity\Batch;
use App\Entity\File;
use App\Entity\Order;
use App\Entity\Package;
use App\Entity\Promocode;
use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use App\Repository\OrderRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\CalculateService;
use App\Service\FileUpdateService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\Mover\MoverMapperService;
use App\Service\Mapper\OrderCustomerMapperService;
use App\Service\Mapper\OrderMapperService;
use App\Service\Mapper\UserMapperService;
use App\Service\MoverService;
use App\Service\NotificationService;
use App\Service\PromocodeService;
use App\Service\SmsService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\Router;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;
use App\Entity\Organization\Organization;
use App\Repository\Organization\OrganizationRepository;

class BatchSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntegrationBaseService
     */
    private $integrationBaseService;

    /**
     * @var OrderMapperService
     */
    private $orderMapperService;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var SmsService
     */
    private $smsService;

    /**
     * @var MailService
     */
    private $mailService;

    /** @var FileUpdateService */
    protected $fileUpdateService;

    /**
     * @var Router
     */
    private $router;

    public function __construct(
        EntityManager $entityManager,
        Container $container
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->integrationBaseService = $this->container->get('app.integration_base');
        $this->orderMapperService = $this->container->get('app.order.mapper');
        $this->smsService = $this->container->get('app.sms');
        $this->mailService = $this->container->get('app.mail');
        $this->settings = $this->container->get('settings');
        $this->router = $this->container->get('router');
        $this->fileUpdateService = $this->container->get('app.file.update');

        /** @var NotificationService $this->notificationService */
        $this->notificationService = $this->container->get('app.notification');

        $entity = $args->getEntity();

        /** @var Batch $entity */
        if ($entity instanceof Batch) {

            // Удаляем заказ если выставлен статус
            if ($entity->isRemove() && $entity->getExternalId()) {
                $result = $this->integrationBaseService->removeBatch($entity->getExternalId());
            }

            // Отправка email нотификации об необходимости забрать партию
            if ($entity->getStatus() == Batch::STATUS_READY
                && !$entity->getNeedPickPartyNotificationDate()
                && $entity->getSendDate()
            ) {
                $this->mailService->registerEmail(
                    'need_pick_party',
                    $entity->getCarrier()->getEmail(),
                    [
                        'entity' => $entity,
                        'senddate' => $entity->getSendDate()->format('d.m.Y H:i'),
                        'stockfrom_address' => $entity->getStockFrom()->getAddress(),
                        'link' =>
                            $this->container->getParameter('router.request_context.scheme').':'.$this->router->generate(
                                'app.personal.index',
                                ['orderId' => $entity->getId()],
                                Router::NETWORK_PATH
                            )
                    ]
                );

                $entity->setNeedPickPartyNotificationDate(new \DateTime());

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            // Отправка смс нотификации об оплате
            if ($entity->isPayed() && !$entity->getPayedNotificationDate()) {
                $settings = $this->settings->group('general');

                $this->smsService->send(
                    $entity->getCarrier()->getPhone(),
                    str_replace('#NUMBER#', $entity->getId(), $settings['sms_order_carrier_pay'])
                );

                $entity->setPayedNotificationDate(new \DateTime());

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            if (!$entity->getUpdateBatchDate() && $entity->getCarrier() && $entity->getExternalId() && count($entity->getDrivers()) > 0) {
                $data = $this->orderMapperService->mapEntityBatchToBase($entity);

                $result = $this->integrationBaseService->updateBatch($data);

                $entity->setUpdateBatchDate(new \DateTime());
                if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                    $entity->setExternalId($result['data']);
                }

                $result = $this->integrationBaseService->updateBatch($data);

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            /** Отправим уведомление об изменении статуса оплаты  */
            if (!$entity->getUpdatePayedDate() && $entity->getExternalId()) {
                $data = $this->orderMapperService->mapBatchToBasePaid($entity);
                $result = $this->integrationBaseService->batchPaid($data);
                $entity->setUpdatePayedDate(new \DateTime());
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            /** @var File $file */
            foreach ($entity->getFiles() as $file) {
                if (!$file->getExternalId() && $file->getMedia()) {
                    $file = $this->fileUpdateService->fileSynch($file);
                }
            }
        }
    }
}
