<?php
namespace App\EventListener\Wms;

use App\Entity\Batch;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Service\FileUpdateService;
use App\Repository\FileGroupRepository;
use App\Repository\Wms\WmsOrderRepository;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\Router;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;


class BatchSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        EntityManager $entityManager,
        Container $container
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entityManager = $this->container->get('doctrine')->getManager();
        $fileGroupRepository = $this->container->get('doctrine')->getRepository(FileGroup::class);
        $batchRepository = $this->container->get('doctrine')->getRepository(Batch::class);
        $orderRepository = $this->container->get('doctrine')->getRepository(Order::class);
        $wmsOrderRepository = $this->container->get('doctrine')->getRepository(WmsOrder::class);

        /** @var FileUpdateService $fileUpdateService */
        $fileUpdateService = $this->container->get('app.file.update');

        /** @var WmsBatch $entity */
        $entity = $args->getEntity();

        /** @var WmsBatch $entity */
        if ($entity instanceof WmsBatch) {

            $tnGroup = $fileGroupRepository->findOneBySlug('tn');
            $tn = $entity->getFileByGroup('tn');
            $tnShipping = $entity->getFileByGroup('tn-shipping');
            $tnDelivery = $entity->getFileByGroup('tn-delivery');

            if ($tnShipping && !$tnDelivery) {
                if (!$tn) {
                    $file = new File();
                    $file->setPublished(true);
                    $file->setTitle('ТН');
                    $file->setLocalExternalId($tnGroup->getSlug().$entity->getExternalId());
                    $file->setGroup($tnGroup);
                    $file->setMedia($tnShipping->getMedia());
                    $entity->addFile($file);
                    $entityManager->persist($file);
                    $entityManager->flush();
                } else {
                    $file = $tn;
                    $file->setMedia($tnShipping->getMedia());
                }
            } elseif ($tnDelivery) {
                if (!$tn) {
                    $file = new File();
                    $file->setPublished(true);
                    $file->setTitle('ТН');
                    $file->setLocalExternalId($tnGroup->getSlug().$entity->getExternalId());
                    $file->setGroup($tnGroup);
                    $file->setMedia($tnDelivery->getMedia());
                    $entity->addFile($file);
                } else {
                    $file = $tn;
                    $file->setMedia($tnDelivery->getMedia());
                }
            }

            /** @var Batch $batch */
            $batch = $batchRepository->find($entity->getExternalId());

            /** Синхронизируем определенные типы файлов */
            if ($tnShipping && !$tnDelivery) {
                $fileGroupName = 'tn';
                $file = $entity->getFileByGroup($fileGroupName);
                if ($file) {
                    $batch->replaceFile($file);
                    $entityManager->persist($batch);
                    $entityManager->flush();
                }
            } elseif ($tnDelivery) {
                $fileGroupName = 'tn';
                $file = $entity->getFileByGroup($fileGroupName);
                if ($file) {
                    $batch->replaceFile($file);
                    $entityManager->persist($batch);
                    $entityManager->flush();
                }
            }
            
            /*if ($tnDelivery) {
                $fileGroupName = 'tn';
                $file = $entity->getFileByGroup($fileGroupName);
                
                if ($file) {
					$packages = [];
					foreach($entity->getPallets() as $pallet) {
						if ($pallet->isPalleted() == false) {
							foreach($pallet->getRealPackages() as $package) {
								$packages[] = $package;
							}
						}
					}
				
					if ($packages) {
						$wmsOrders = $wmsOrderRepository->getByPackages($packages);
				
						foreach($wmsOrders as $wmsOrder) {
							$order = $orderRepository->getById($wmsOrder->getExternalId());

							$order->replaceFile($file);
							$wmsOrder->replaceFile($file);
							$entityManager->persist($wmsOrder);
							$entityManager->persist($order);
							$entityManager->flush();
						}
					}
				}
            }*/
        }

        /** @var WmsBatchMover $entity */
        if ($entity instanceof WmsBatchMover) {
	
            $tnGroup = $fileGroupRepository->findOneBySlug('tn');
            $tn = $entity->getFileByGroup('tn');
            $tnShipping = $entity->getFileByGroup('tn-shipping');
            $tnDelivery = $entity->getFileByGroup('tn-delivery');
            
			if ($tnDelivery) {
                if (!$tn) {
                    $file = new File();
                    $file->setPublished(true);
                    $file->setLocalExternalId($tnGroup->getSlug().$entity->getOrder()->getId());
                    $file->setGroup($tnGroup);
                    $file->setMedia($tnDelivery->getMedia());
                    $entity->addFile($file);
                } else {
                    $file = $tn;
                    $file->setMedia($tnDelivery->getMedia());
                }
            }

            if ($tnDelivery) {
                $fileGroupName = 'tn';
                $file = $entity->getFileByGroup($fileGroupName);
                
                if ($file) {
					$file->setTitle('ТН');
					$file->setLocalExternalId($tnGroup->getSlug().$entity->getOrder()->getId());
                    $order = $entity->getOrder();

                    $order->replaceFile($file);
                    $entityManager->persist($order);
                    $entityManager->flush();
                }
            }
            
            if ($tnDelivery) {
                $fileGroupName = 'tn';
                $file = $entity->getFileByGroup($fileGroupName);
                
                if ($file) {
					$file->setTitle('ТН');
					$file->setLocalExternalId($tnGroup->getSlug().$entity->getOrder()->getId());

                    $order = $wmsOrderRepository->findOneByExternalId($entity->getOrder()->getId());

                    $order->replaceFile($file);
                    $entityManager->persist($order);
                    $entityManager->flush();
                }
            }

            //$fileGroupName = 'er';
            //$file = $entity->getFileByGroup($fileGroupName);
            //if ($file) {
            //    $order = $entity->getOrder();
            //    $file->setTitle('ЭР мувер');
            //    $file->setExternalId($fileGroupName.$entity->getExternalId());
            //    $order->replaceFile($file);
            //    $entityManager->persist($order);
            //    $entityManager->flush();
            //}

            /** @var File $file */
            foreach ($entity->getFiles() as $file) {
                if (!$file->getExternalId() && $file->getMedia()) {
                    $fileUpdateService->whFileSynch($file);
                }
            }
        }
    }
}
