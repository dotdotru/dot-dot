<?php
namespace App\EventListener\Wms;

use App\Entity\Base\Stock;
use App\Entity\File;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\FileUpdateService;
use App\Service\IntegrationBaseService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\Router;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;

class OrderSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        EntityManager $entityManager,
        Container $container
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        /** @var FileUpdateService $fileUpdateService */
        $fileUpdateService = $this->container->get('app.file.update');

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->container->get('app.integration_base');

        $baseStockRepository = $this->container->get('doctrine')->getRepository(Stock::class);

        $entity = $args->getEntity();

        if (false && $entity instanceof WmsOrder && $entity->getStockTo()) {
            $baseStock = $baseStockRepository->findOneBy(['externalId' => $entity->getStockTo()->getId()]);
            $integrationBaseService->authByStock($baseStock);
            $fileUpdateService->setIntegrationBaseService($integrationBaseService);

            /** @var File $file */
            foreach ($entity->getFiles() as $file) {
                if (!$file->getExternalId() && $file->getMedia()) {
                    $fileUpdateService->whFileSynch($file);
                }
            }
        }
    }
}
