<?php
namespace App\EventListener;

use App\Entity\Base\City;
use App\Entity\Base\Direction;
use App\Entity\Base\DirectionPrice;
use App\Entity\Base\Stock;
use App\Entity\Base\StockWorkingHours;
use App\Entity\Base\WorkingCalendar;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\BaseMapperService;
use App\Service\Mapper\OrderMapperService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\Router;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;

class BaseSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntegrationBaseService
     */
    private $integrationBaseService;

    /**
     * @var BaseMapperService
     */
    private $baseMapperService;

    public function __construct(
        EntityManager $entityManager,
        Container $container
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'preRemove'
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        /** @var IntegrationBaseService integrationBaseService */
        $this->integrationBaseService = $this->container->get('app.integration_base');

        $entity = $args->getEntity();

        /** @var Direction $entity */
        if ($entity instanceof Direction) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeRoute($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        if ($entity instanceof DirectionPrice) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeRoutePrice($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        if ($entity instanceof City) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeCity($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        if ($entity instanceof Stock) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeWarehouse($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        if ($entity instanceof StockWorkingHours) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeWarehouseWorkingHour($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        if ($entity instanceof WorkingCalendar) {
            if ($entity->getExternalId()) {
                $result = $this->integrationBaseService->removeWarehouseWorkingExclusion($entity->getExternalId());

                if (!$result[IntegrationBaseService::RESULT_SUCCESS]) {
                    return false;
                }
            }
        }

        return false;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        /** @var IntegrationBaseService integrationBaseService */
        $this->integrationBaseService = $this->container->get('app.integration_base');

        /** @var BaseMapperService baseMapperService */
        $this->baseMapperService = $this->container->get('app.base.mapper');

        $entity = $args->getEntity();

        /** @var Direction $entity */
        if ($entity instanceof Direction) {
            $mapData = $this->baseMapperService->mapDirectionToBase($entity);
            $result = $this->integrationBaseService->updateRoute($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        if ($entity instanceof DirectionPrice) {
            $mapData = $this->baseMapperService->mapDirectionPriceToBase($entity);

            $result = $this->integrationBaseService->updateRoutePrice($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        if ($entity instanceof City) {
            $mapData = $this->baseMapperService->mapCityToBase($entity);
            $result = $this->integrationBaseService->updateCity($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        if ($entity instanceof Stock) {
            $mapData = $this->baseMapperService->mapStockToBase($entity);
            $result = $this->integrationBaseService->updateWarehouse($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        if ($entity instanceof StockWorkingHours) {
            $mapData = $this->baseMapperService->mapStockWorkingHoursToBase($entity);
            $result = $this->integrationBaseService->updateWarehouseWorkingHour($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        if ($entity instanceof WorkingCalendar) {
            $mapData = $this->baseMapperService->mapWorkingCalendarToBase($entity);
            $result = $this->integrationBaseService->updateWarehouseWorkingExclusion($mapData);

            $entity->setSynchAt(null);
            if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$entity->getExternalId()) {
                $entity->setExternalId($result['data']);
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }
}
