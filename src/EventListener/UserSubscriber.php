<?php
namespace App\EventListener;

use App\Entity\Organization\Organization;
use App\Entity\Package;
use App\Entity\Pallet;
use App\Entity\Person;
use App\Entity\Stock;
use App\Event\LotGenerateEvent;
use App\Service\AuctionService;
use App\Service\CalculateService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\UserMapperService;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Service\User\UserVatService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Sonata\UserBundle\Entity\BaseUser;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;

class UserSubscriber implements EventSubscriber
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntegrationBaseService
     */
    private $integrationBaseService;
    /**
     * @var UserMapperService
     */
    private $userMapperService;

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        UserMapperService $userMapperService,
        UserVatService $userVatService
    )
    {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->userMapperService = $userMapperService;
        $this->userVatService = $userVatService;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $result = [];
        $user = $args->getEntity();

        // Костыль из-за того, что в профиле редактируется только организация
        if (!($user instanceof BaseUser)) {
            if ($user instanceof Organization) {
                $user = $user->getUser();

                if ($user->getPerson()) {
                    $person = $this->mapPersonFromUser($user);
                    $resultData = $this->integrationBaseService->saveActor($this->mapPersonToActor($person, $user));
                }
            }
        }

        if ($user instanceof BaseUser) {

            if (!$user->getPerson() || !$user->getPerson()->getExternalId()) {
                $person = $this->mapPersonFromUser($user);

                $this->entityManager->persist($person);
                $this->entityManager->flush();
                $resultData = $this->integrationBaseService->saveActor($this->mapPersonToActor($person, $user));
                
                //echo '<pre>';
                //var_dump($this->mapPersonToActor($person));
                //var_dump($resultData); die();

                if ($resultData[IntegrationBaseService::RESULT_SUCCESS]) {
					$user->setPerson($person);
                    $person->setExternalId($resultData['data']);
                }
                
                unset($resultData);
                
                $this->entityManager->persist($user);
				$this->entityManager->flush();
            }

            /*
             * Обновляем контактные данные при смене данных
             * @todo объеденить это с верхнем блоком и убрать костыли
             */
            if ($user->getPerson()) {
                $tmpPerson = $this->buildPersonFromUser($user);

                if (!$this->isPersonsEquals($tmpPerson, $user->getPerson())) {
                    $person = $this->mapPersonFromUser($user);

                    $this->entityManager->persist($person);
                    $this->entityManager->flush();
                    $resultData = $this->integrationBaseService->saveActor($this->mapPersonToActor($person, $user));

                    if ($resultData[IntegrationBaseService::RESULT_SUCCESS]) {
                        $user->setPerson($person);
                        if (!empty($resultData['data'])) {
                            $person->setExternalId($resultData['data']);
                        }
                    }

                    unset($resultData);

                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                }
            }

            if (!$user->getExternalId()) {
                $userData = $this->integrationBaseService->getUser($user->getEmail());

                if(!$userData[IntegrationBaseService::RESULT_SUCCESS]) {
                    $user->setExternalId(null);
                } elseif ($userData[IntegrationBaseService::RESULT_SUCCESS]) {
					$user->setExternalId($userData['data']['id']);
				}

				$this->entityManager->persist($user);
				$this->entityManager->flush();
            }

            $organizationRepository = $this->entityManager->getRepository(Organization::class);
            $organization = $organizationRepository->getByUser($user);

            $data = $this->userMapperService->mapEntityToBase($user, $organization);

            $result = $this->integrationBaseService->updateUser($data);

            if (isset($result) && !empty($result) && $result[IntegrationBaseService::RESULT_SUCCESS] && !$user->getExternalId()) {
                $user->setExternalId($result['data']);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }

        $organization = $args->getEntity();

        if ($organization instanceof Organization) {
            $user = $organization->getUser();

            if ($user && $user->getExternalId()) {
                $userData = $this->integrationBaseService->getUser($user->getExternalId());
                if(!$userData) {
                    $user->setExternalId(null);
                }

                $data = $this->userMapperService->mapEntityToBase($user, $organization);
				$result = $this->integrationBaseService->updateUser($data);
            }
        }
    }

    /**
     * Проверяем разные ли данные в контактных данных
     *
     * @param Person $personA
     * @param Person $personB
     * @return int
     */
    public function isPersonsEquals (Person $personA, Person $personB)
    {
        $personDataA = $personA->jsonSerialize();
        $personDataB = $personB->jsonSerialize();

        unset($personDataA['id']);
        unset($personDataB['id']);

        return !count(array_diff($personDataA, $personDataB));
    }


    /**
     * Создает на основе юзера новый контакт
     * @todo вынести в отдельный сервис
     *
     * @param User $user
     * @return Person|mixed
     */
    public function buildPersonFromUser(User $user)
    {
        $person = new Person();

        $person->setVat($this->userVatService->get($user));

        if ($user->getOrganizationCustomer()) {
            $organization = $user->getOrganizationCustomer();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        } else if ($user->getOrganizationCarrier()) {
            $organization = $user->getOrganizationCarrier();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        } else {
            $person->setUridical(false);
            $person->setContactName($user->getFullname());
            $person->setContactPhone($user->getPhone());
            $person->setContactEmail($user->getEmail());

            $person->setSeries($user->getSeries());
            $person->setNumber($user->getNumber());
        }

        return $person;
    }

    public function mapPersonFromUser(User $user)
    {
        $person = new Person();
        if ($user->getPerson()) {
            $person = $user->getPerson();
        }

        $person->setUser($user);
        $person->setVat($this->userVatService->get($user));

        if ($user->getOrganizationCustomer()) {
            $organization = $user->getOrganizationCustomer();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        } else if ($user->getOrganizationCarrier()) {
            $organization = $user->getOrganizationCarrier();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        }else {
            $person->setUridical(false);
            $person->setContactName($user->getFullname());
            $person->setContactPhone($user->getPhone());
            $person->setContactEmail($user->getEmail());

            $person->setSeries($user->getSeries());
            $person->setNumber($user->getNumber());
        }

        return $person;
    }

    public function mapPersonToActor(Person $person, $user)
    {
        $data = [
            'id' => $person->getExternalId(),
            'ext_id' => $person->getId(),
            'user' => $person->getUser()->getExternalId(),
            'company' => null,
            'person' => null,
        ];
        if ($person) {
            if ($person->getUridical() > 0) {
                $data['company'] = [
                    'type' => $person->getType(),
                    'name' => $person->getCompanyName(),
                    'vat' => $person->getVat(),
                    'inn' => $person->getInn(),
                    'kpp' => '',
                    'bic' => '',
                    'account_no' => '',
                    'ceo' => '',
                    'contact_name' => $person->getContactName(),
                    'contact_phone' => $person->getContactPhone(),
                    'contact_email' => $person->getContactEmail(),
                    'address' => '',
                    'verified' => null
                ];
            } else {
                $data['person'] = [
                    'name' => $person->getContactName(),
                    'inn' => '',
                    'vat' => $person->getVat(),
                    'birth_date' => null,
                    'identity' => [
                        'type' => 'passport',
                        'no' => $person->getSeries().$person->getNumber(),
                        'issued' => null,
                        'issued_by' => '',
                    ],
                    'phone' => $person->getContactPhone(),
                    'email' => $person->getContactEmail(),
                ];
            }
        }

        return $data;
    }
}
