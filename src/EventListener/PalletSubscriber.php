<?php
namespace App\EventListener;

use App\Entity\Package;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Event\LotGenerateEvent;
use App\Service\AuctionService;
use App\Service\CalculateService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use SymfonyBundles\EventQueueBundle\Service\Dispatcher;

class PalletSubscriber implements EventSubscriber
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CalculateService
     */
    private $calculateService;
    /**
     * @var AuctionService
     */
    private $auctionService;

    public function __construct(EntityManager $entityManager, CalculateService $calculateService, AuctionService $auctionService)
    {
        $this->entityManager = $entityManager;
        $this->calculateService = $calculateService;
        $this->auctionService = $auctionService;
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $pallet = $args->getEntity();

        if ($pallet instanceof Pallet) {
            $price = 0;

            /** @var Package $item */
            foreach ($pallet->getPackages() as $item) {
                $price += $item->getEstimate();
            }

            $pallet->setPrice($price);

            $this->entityManager->persist($pallet);
            $this->entityManager->flush();
        }
    }
}