<?php

namespace App\Service;

use SoapClient;
use SoapFault;
use SimpleXMLElement;
use App\Entity\Organization\Organization;

class SvSoapService
{
    /** @var SoapClient */
    private $client;

    private $key = '';

    private $testMode = true;

    private $wsdlPath = '';

    const REQUEST_TYPE_DRIVER = 1; // водитель
    const REQUEST_TYPE_POLISE = 2; // полис

    const METHOD_REQUESTCARGO_WORK = 'REQUESTCARGO_WORK';
    const METHOD_GETCLIENTLIST_WORK = 'GETCLIENTLIST_WORK';
    const METHOD_REQUESTCARGOCANCEL_WORK = 'REQUESTCARGOCANCEL_WORK';

    public function __construct($wsdlPath, $login = null, $password = null, $key = '', $testMode = true)
    {
        $this->wsdlPath = $wsdlPath;
        $this->params = [
            'login' => $login,
            'password' => $password,
            'trace' => true,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
        ];

        $this->testMode = $testMode;

        $this->key = $key;
    }

    public function verify(SimpleXMLElement $xml)
    {
        if (!$this->testMode) {
            $xml = $this->exec($xml, self::REQUEST_TYPE_DRIVER);

            if (!$xml) {
                return false;
            }

            $result = [
                'organization' => (string)$xml->result->status[0]['code'],
                'driver' => (string)$xml->result->status[1]['code'],
            ];
        } else {
            $status = ['verified', 'delay', 'error'];
            $result = [
                'organization' => 'verified',
                'driver' => $status[rand(0, 2)],
            ];
        }

        return $result;
    }

    public function getVerificationList(SimpleXMLElement $xml)
    {
        $xml = $this->exec($xml, self::REQUEST_TYPE_DRIVER, self::METHOD_GETCLIENTLIST_WORK);

        if (!$xml) {
            return false;
        }

        $result = [];
        foreach ($xml->clients->children() as $client) {
            $id = (int)$client->attributes()['id'];
            $status = (string)$client;
            $result[strtolower($client->getName())][] = [
                'id' => $id,
                'status' => $status
            ];
        }

        foreach ($xml->drivers->children() as $client) {
            $id = (int)$client->attributes()['id'];
            $status = (string)$client;
            $result[strtolower($client->getName())][] = [
                'id' => $id,
                'status' => $status
            ];
        }

        return $result;
    }

    public function insurance(SimpleXMLElement $xml)
    {
        $xml = $this->exec($xml, self::REQUEST_TYPE_POLISE);

        if (!$xml) {
            return false;
        }

        $result = [
            'code' => (string)$xml->result->status[0]['code'],
            'number' => $xml->result->number ? (string)$xml->result->number[0] : '',
            'pdf' => $xml->result->pdf ? 'data:application/pdf;base64,'.base64_encode(gzuncompress(base64_decode((string)$xml->result->pdf[0]))) : '',
        ];

        return $result;
    }

    public function insuranceCancel(SimpleXMLElement $xml)
    {
        $xml = $this->exec($xml, self::REQUEST_TYPE_POLISE, self::METHOD_REQUESTCARGOCANCEL_WORK);

        if (!$xml) {
            return false;
        }

        $result = [
            'code' => (string)$xml->result->status[0]
        ];

        return $result;
    }

    private function exec(SimpleXMLElement $xml, $requestType, $method = self::METHOD_REQUESTCARGO_WORK)
    {
        $this->client = new SoapClient($this->wsdlPath.$method.'?wsdl', $this->params);

        $xml->addChild('key', $this->key);

        $params = [];
        $xml = html_entity_decode($xml->asXML(), ENT_NOQUOTES, 'UTF-8');

        if ($method == self::METHOD_REQUESTCARGO_WORK) {
            $params["PDATA-CLOB-IN"] = $xml;
            $params["PTYPE-NUMBER-IN"] = $requestType; // 1- водитель 2-полис
            $params["POUTDATA-CLOB-OUT"] = '';
        }

        if ($method == self::METHOD_GETCLIENTLIST_WORK) {
           $params["PREQ-CLOB-IN"] = $xml;
           //$params["PTYPE-NUMBER-IN"] = $requestType; // 1- водитель 2-полис
           //$params["POUTDATA-CLOB-OUT"] = '';
           $params['PRESP-CLOB-OUT'] = '';
        }

        if ($method == self::METHOD_REQUESTCARGOCANCEL_WORK) {
            $params["PDATA-CLOB-IN"] = $xml;
            //$params["PTYPE-NUMBER-IN"] = $requestType; // 1- водитель 2-полис
            $params["POUTDATA-CLOB-OUT"] = '';
        }

        try {
            if ($method == self::METHOD_REQUESTCARGO_WORK) {
                $result = $this->client->REQUESTCARGO_WORK($params);
                // Fix error xml
                $POUTDATA = str_replace('&', '&amp;', $result->POUTDATA);
                $xml = new \SimpleXMLElement($POUTDATA);
            } elseif ($method == self::METHOD_GETCLIENTLIST_WORK) {
                $result = $this->client->GETCLIENTLIST_WORK($params);
                $xml = new \SimpleXMLElement($result->PRESP);
            } elseif ($method == self::METHOD_REQUESTCARGOCANCEL_WORK) {
                $result = $this->client->REQUESTCARGOCANCEL_WORK($params);
                $xml = new \SimpleXMLElement($result->POUTDATA);
            }

            return $xml;
        } catch (SoapFault $e) {
			var_dump($e->getMessage());
            /*
            TODO:: Оправить сообщение админу
            */
        }

        return null;
    }

    public function getLastRequest()
    {
        if ($this->client) {
            return $this->client->__getLastRequest();
        }

        return '';
    }

    public function getLastResponse()
    {
        if ($this->client) {
            return $this->client->__getLastResponse();
        }

        return '';
    }
}
