<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use Doctrine\ORM\EntityManager;
use \GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Ratchet\Client as WebSocket;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Templating\EngineInterface;
use Vsavritsky\SettingsBundle\Service\Settings;

class BatchMoverService
{
    /** @var EntityManager|null  */
    protected $entityManager = null;

    /** @var FileUploadService|null  */
    protected $fileUploadService = null;

    /** @var null|EngineInterface  */
    protected $twig = null;

    public function __construct(
        EntityManager $entityManager,
        FileUploadService $fileUploadService,
        EngineInterface $twig,
        $knpSnappy,
        $projectDir,
        Settings $settings
    ) {
        $this->entityManager = $entityManager;
        $this->fileUploadService = $fileUploadService;
        $this->twig = $twig;
        $this->knpSnappy = $knpSnappy;
        $this->projectDir = $projectDir;
        $this->settings = $settings;
    }

    public function createBatchMover(Order $order, Shipping $shipping)
    {
        $this->setupWmsBatchMover($order, $shipping);
        $this->setupExpeditorDoc($order);
        $this->setupTnDoc($order);
    }

    private function setupWmsBatchMover(Order $order, Shipping $shipping)
    {
        $wmsBatchMoverExternalId = $order->getId().$shipping->getExternalIdPostfix();

        $wmsBatchMover = $this->entityManager->getRepository(WmsBatchMover::class)->getByExternalId($wmsBatchMoverExternalId);

        if (!$wmsBatchMover) {
            $wmsBatchMover = new WmsBatchMover();
            $wmsBatchMover->setExternalId($wmsBatchMoverExternalId);
        }

        $wmsBatchMover->setDirection($order->getDirection());
        $wmsBatchMover->setStockFrom($order->getStockFrom());
        $wmsBatchMover->setStockTo($order->getStockTo());
        $wmsBatchMover->setAddressTo($shipping->getAddress());

        $wmsBatchMover->setStatus(WmsBatchMover::STATUS_READY);
        $wmsBatchMover->setOrder($order);
        $wmsBatchMover->setShipping($shipping);

        $this->entityManager->persist($wmsBatchMover);
        $this->entityManager->flush();
    }

    private function setupExpeditorDoc(Order $order)
    {
        $type = 'er';

        $pathTempalate = "site/pdf/order/$type.html.twig";
        
        $offerDate = $this->settings->get('offer_date');


        $dataTemplate = ['order'  => $order, 'offerDate' => $offerDate];

        $html = $this->twig->render($pathTempalate, $dataTemplate);
        $data = $this->knpSnappy->getOutputFromHtml($html);

        if (is_string($data)) {
            $tmpPath = $this->projectDir.'/var/cache'.uniqid().'.pdf';
            $fs = new Filesystem();
            $fs->appendToFile($tmpPath, $data);
            $pdf = new UploadedFile($tmpPath, '', 'application/pdf');
        }

        $localExtId = sprintf('%s%s', $type, $order->getId());
        $file = $this->fileUploadService->createFile('ЭР', $type, $pdf, $localExtId, $localExtId);
        $file->setLocalExternalId($localExtId);
        $order->replaceFile($file);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $this->entityManager->getRepository(WmsOrder::class)->getByExternalId($order->getId());
        if ($wmsOrder) {
            $wmsOrder->replaceFile($file);
            $this->entityManager->persist($wmsOrder);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    private function setupTnDoc(Order $order)
    {
        $type = 'tn';

        $offerDate = $this->settings->get('offer_date');

        $pathTempalate = "site/pdf/order/$type.html.twig";
        $dataTemplate = ['order'  => $order, 'offerDate' => $offerDate];

        $html = $this->twig->render($pathTempalate, $dataTemplate);
        $data = $this->knpSnappy->getOutputFromHtml($html);

        if (is_string($data)) {
            $tmpPath = $this->projectDir.'/var/cache'.uniqid().'.pdf';
            $fs = new Filesystem();
            $fs->appendToFile($tmpPath, $data);
            $pdf = new UploadedFile($tmpPath, '', 'application/pdf');
        }

        $localExtId = sprintf('%s%s', $type, $order->getId());
        $file = $this->fileUploadService->createFile('ТН', $type, $pdf, $localExtId, $localExtId);
        $file->setLocalExternalId($localExtId);
        $order->replaceFile($file);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $this->entityManager->getRepository(WmsOrder::class)->getByExternalId($order->getId());
        if ($wmsOrder) {
            $wmsOrder->replaceFile($file);
            $this->entityManager->persist($wmsOrder);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
}
