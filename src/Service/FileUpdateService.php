<?php

namespace App\Service;

use App\Entity\Batch;
use App\Entity\File;
use App\Entity\Order;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\FileMapperService;
use Doctrine\ORM\EntityManager;

class FileUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|IntegrationBaseService */
    private  $integrationBaseService = null;

    /** @var null|FileMapperService */
    private  $fileMapperService = null;

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        FileMapperService $fileMapperService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->fileMapperService = $fileMapperService;
    }

    public function setIntegrationBaseService(IntegrationBaseService $integrationBaseService)
    {
        $this->integrationBaseService = $integrationBaseService;
    }

    public function fileSynch(File $file)
    {
        $data = $this->fileMapperService->mapFileToBaseDocument($file);
        if (!$data) {
            return false;
        }

        $result = $this->integrationBaseService->saveDocument($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$file->getExternalId()) {
            $file->setExternalId($result['data']);
            $this->entityManager->persist($file);
            $this->entityManager->flush();
        }

        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function mapBaseDocumentToFile(File $file, $data)
    {
        return $this->fileMapperService->mapBaseDocumentToFile($file, $data);
    }

    public function whFileSynch(File $file)
    {
        $data = $this->fileMapperService->mapFileToBaseDocument($file);

        if (!$data) {
            return false;
        }

        $result = $this->integrationBaseService->whSaveDocument($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$file->getExternalId()) {
            $file->setExternalId($result['data']);
            $this->entityManager->persist($file);
            $this->entityManager->flush();
        }

        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function mapListDocuments($data, $currentFiles = [])
    {
        $list = [];
        $fileRepository = $this->entityManager->getRepository(File::class);

        $oldFileIds = [];
        if ($currentFiles) {
            foreach ($currentFiles as $file) {
                $oldFileIds[] = $file->getId();
            }
        }

        foreach ($data as $documentData) {
            $needUpdate = false;

            $documentDate = new \DateTime($documentData['date']);

            /** @var File $file */
            $file = $fileRepository->findOneByExternalId($documentData['id']);

            if (!$file) {
                $file = new File();
                $needUpdate = true;
            } elseif ($file && (!$file->getDate() || $file->getDate() < $documentDate) || !in_array($file->getId(), $oldFileIds)) {
                $needUpdate = true;
            }

            if ($needUpdate) {
                $documentDataResult = $this->integrationBaseService->getDocument($documentData['id']);
                if ($documentDataResult[IntegrationBaseService::RESULT_SUCCESS]) {
                    $file = $this->mapBaseDocumentToFile($file, $documentDataResult['data']);
                    $this->entityManager->persist($file);
                    $list[] = $file;
                }
            }
        }

        return $list;
    }

    public function wmsMapListDocuments($data, $currentFiles = [])
    {
        $list = [];
        $fileRepository = $this->entityManager->getRepository(File::class);

        $oldFileIds = [];
        if ($currentFiles) {
            foreach ($currentFiles as $file) {
                $oldFileIds[] = $file->getId();
            }
        }

        foreach ($data as $documentData) {
            $needUpdate = false;

            $documentDate = new \DateTime($documentData['date']);

            /** @var File $file */
            $file = $fileRepository->findOneByExternalId($documentData['id']);

            if (!$file) {
                $file = new File();
                $needUpdate = true;
            } elseif ($file && (!$file->getDate() || $file->getDate() < $documentDate) || !in_array($file->getId(), $oldFileIds)) {
                $needUpdate = true;
            }

            if ($needUpdate) {
                $documentDataResult = $this->integrationBaseService->whGetDocument($documentData['id']);
                if ($documentDataResult[IntegrationBaseService::RESULT_SUCCESS]) {
                    $file = $this->mapBaseDocumentToFile($file, $documentDataResult['data']);
                    $this->entityManager->persist($file);
                    $list[] = $file;
                }
            }
        }

        return $list;
    }
}
