<?php

namespace App\Service\Wms;

use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use Doctrine\ORM\EntityManager;


class WmsBatchStatusUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;


    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }


    public function updateStatus(WmsBatch $wmsBatch, $status)
    {
        if (WmsBatch::STATUS_ARRIVED === $status) {
            /** @var WmsPallet $pallet */
            foreach ($wmsBatch->getPallets() as $pallet) {
                /** @var WmsPackage $package */
                foreach ($pallet->getRealPackages() as $package) {
                    $package->setArrivedAt(new \DateTime());
                }
            }
        }

    }

}
