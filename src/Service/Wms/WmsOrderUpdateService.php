<?php

namespace App\Service\Wms;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Wms\WmsOrder;
use App\Service\Mapper\Wms\WmsOrderMapperService;
use Doctrine\ORM\EntityManager;
use App\Service\IntegrationBaseService;

class WmsOrderUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|IntegrationBaseService */
    private  $integrationBaseService = null;

    /** @var null|WmsOrderMapperService */
    private  $wmsOrderMapperService = null;

    public function setIntegrationBaseService(IntegrationBaseService $integrationBaseService)
    {
        $this->integrationBaseService = $integrationBaseService;
    }

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        WmsOrderMapperService $wmsOrderMapperService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->wmsOrderMapperService = $wmsOrderMapperService;
    }

    public function orderRejected(WmsOrder $order)
    {
        $data = $this->wmsOrderMapperService->orderRejected($order);
        $response = $this->integrationBaseService->whOrderRejected($data);

        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function orderAccepted(WmsOrder $order)
    {
        $data = $this->wmsOrderMapperService->orderAccepted($order);
        $response = $this->integrationBaseService->whOrderAccepted($data);
        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function newOrderAccepted(WmsOrder $order)
    {
        $data = $this->wmsOrderMapperService->newOrderAccepted($order);
        $response = $this->integrationBaseService->whNewOrderAccepted($data);

        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function orderDelivered(WmsOrder $order)
    {
        $data = $this->wmsOrderMapperService->orderDelivered($order);
        $response = $this->integrationBaseService->whOrderDelivered($data);

        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function updateWmsOrder(WmsOrder $order)
    {
        if (!$order->getExternalId()) {
            return false;
        }

        $result = $this->integrationBaseService->getInfoOrder($order->getExternalId());

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {
            if (!empty($result['data']) && !empty($result['data']['cargos'])) {
                $order = $this->wmsOrderMapperService->mapInfoOrderToWmsOrder($order, $result['data']);

                $this->entityManager->persist($order);
                $this->entityManager->flush();
            } elseif (!empty($result['data'])) {
                $result = $this->integrationBaseService->getOrder($order->getExternalId());
                $order = $this->wmsOrderMapperService->mapOrderToWmsOrder($order, $result['data']);

                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }
        }
    }

    public function updateCarrierOrder(Batch $order)
    {
        if (!$order->getExternalId()) {
            return false;
        }

        $result = $this->integrationBaseService->getBatch($order->getExternalId());

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {

            if (!empty($result['data'])) {
                $order = $this->orderMapperService->mapBaseBatchToEntity($order, $result['data']);

                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }
            
        } else {
            if (!empty($result['error']) && preg_match('#batch (\d)+ not found#', $result['error']['details']['message_primary'])) {
                $order->setRemove();
                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }
        }
    }
}
