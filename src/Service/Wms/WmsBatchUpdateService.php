<?php

namespace App\Service\Wms;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Service\Mapper\OrderMapperService;
use App\Service\Mapper\Wms\WmsBatchMapperService;
use Doctrine\ORM\EntityManager;
use App\Service\IntegrationBaseService;

class WmsBatchUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|IntegrationBaseService */
    private  $integrationBaseService = null;

    /** @var null|OrderMapperService */
    private  $orderMapperService = null;

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        WmsBatchMapperService $orderMapperService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->orderMapperService = $orderMapperService;
    }

    public function batchLoad(WmsBatch $wmsBatch)
    {
        $data = $this->orderMapperService->batchLoad($wmsBatch);
        $result = $this->integrationBaseService->whBatchLoaded($data);
        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function batchRejected(WmsBatch $wmsBatch)
    {
        $data = $this->orderMapperService->batchRejected($wmsBatch);
        $result = $this->integrationBaseService->whBatchRejected($data);

        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function batchArrived(WmsBatch $wmsBatch)
    {
        $data = $this->orderMapperService->batchArrived($wmsBatch);
        $result = $this->integrationBaseService->whBatchArrived($data);

        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function updateWmsBatch(WmsBatch $batch)
    {
        if (!$batch->getExternalId()) {
            return false;
        }

        $result = $this->integrationBaseService->getBatch($batch->getExternalId());

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {

            if (!empty($result['data'])) {
                $order = $this->orderMapperService->mapBaseBatchToEntity($batch, $result['data']);

                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }

        } else {
            if (!empty($result['error']) && preg_match('#batch (\d)+ not found#', $result['error']['details']['message_primary'])) {
                $batch->setRemove();
                $this->entityManager->persist($batch);
                $this->entityManager->flush();
            }
        }
    }
}
