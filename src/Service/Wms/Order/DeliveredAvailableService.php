<?php


namespace App\Service\Wms\Order;


use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use Doctrine\ORM\EntityManager;

class DeliveredAvailableService
{
    /** @var null|EntityManager */
    private $em = null;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Проверяем что доступна выдача заказа на складе
     *
     * @param WmsOrder $order
     * @return bool
     */
    public function isAvailable (WmsOrder $order)
    {
        $available = !$this->hasPallets($order);


	// Если есть последняя миля, заказ будет выдан после доставки последней мили
        $shipping = $order->getShipping(Shipping::TYPE_DELIVER);
        if ($shipping && $shipping->isActive() && $shipping->getExternalId()) {
            $available = false;
        }


        return $available;
    }


    /**
     * Проверяем что доступна выдача заказа на складе
     *
     * @param WmsOrder $order
     * @return bool
     */
    public function hasPallets (WmsOrder $order)
    {
        $founded = false;

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            $palletId = $package->getPalletId();

            // Если id паллеты не установлено, значит паллета расформированна
            if ($palletId) {
                $founded = true;
            }
        }

        return $founded;
    }

}
