<?php

namespace App\Service\Wms;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPallet;
use App\Service\Mapper\Wms\WmsOrderMapperService;
use App\Service\Mapper\Wms\WmsPalletMapperService;
use Doctrine\ORM\EntityManager;
use App\Service\IntegrationBaseService;

class WmsPalletUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|IntegrationBaseService */
    private  $integrationBaseService = null;

    /** @var null|WmsPalletMapperService */
    private  $wmsPalletMapperService = null;

    public function setIntegrationBaseService(IntegrationBaseService $integrationBaseService)
    {
        $this->integrationBaseService = $integrationBaseService;
    }

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        WmsPalletMapperService $wmsPalletMapperService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->wmsPalletMapperService = $wmsPalletMapperService;
    }

    public function cargoPalletted(WmsPallet $wmsPallet)
    {
        $data = $this->wmsPalletMapperService->cargoPalletted($wmsPallet);
        $response = $this->integrationBaseService->whCargoPalletted($data);
        
        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function cargoUnPalletted(WmsPallet $wmsPallet)
    {
        $data = $this->wmsPalletMapperService->cargoUnPalletted($wmsPallet);
        $response = $this->integrationBaseService->whCargoUnpalletted($data);
        return $response[IntegrationBaseService::RESULT_SUCCESS];
    }
}
