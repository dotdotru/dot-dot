<?php

namespace App\Service\Wms;

use App\Entity\Stock;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Base;

class SecurityService
{
    const WMS_AUTH_CODE = 'wms.auth.code';

    /** @var null|EntityManager */
    private  $entityManager = null;

    public function __construct(
        EntityManager $entityManager,
        Session $session
    ) {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    private function setCode($code)
    {
        $this->session->set(self::WMS_AUTH_CODE, $code);

        return true;
    }

    private function removeCode()
    {
        $this->session->remove(self::WMS_AUTH_CODE);

        return true;
    }

    private function hasCode()
    {
        return $this->session->has(self::WMS_AUTH_CODE);
    }

    private function getCode()
    {
        $code = $this->session->get(self::WMS_AUTH_CODE);

        return $code;
    }

    public function login($code)
    {
        $stockWorker = $this->getStockWorkerByCode($code);

        if (!$stockWorker) {
            return false;
        }

        $this->setCode($stockWorker->getCode());

        return true;
    }

    private function getStockWorkerByCode($code = '')
    {
        $baseStockWorkerRepository = $this->entityManager->getRepository(Base\StockWorker::class);
        $stockWorker = $baseStockWorkerRepository->findOneBy(['code' => $code]);

        return $stockWorker;
    }

    public function logout()
    {
        return $this->removeCode();
    }

    public function isAuth()
    {
        return $this->hasCode();
    }

    public function getStock()
    {
        $stock = null;
        $baseStock = $this->getBaseStock();

        if ($baseStock) {
            $stockRepository = $this->entityManager->getRepository(Stock::class);
            $stock = $stockRepository->findOneBy(['externalId' => $baseStock->getExternalId()]);
        }

        return $stock;
    }

    public function getBaseStock()
    {
        $stock = null;
        $stockWorker = null;

        $code = $this->getCode();

        if ($code) {
            $stockWorker = $this->getStockWorkerByCode($code);
        }

        $stock = $stockWorker->getStock();

        return $stock;
    }
}
