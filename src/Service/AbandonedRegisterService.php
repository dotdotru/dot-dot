<?php

namespace App\Service;

use App\Entity\AbandonedRegister;
use App\Entity\Organization\Organization;
use App\Form\UserType;
use App\Repository\AbandonedRegisterRepository;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Vsavritsky\SettingsBundle\Service\Settings;
use SMSCenter\SMSCenter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class AbandonedRegisterService
{
    /** @var null|MailService */
    private $mailService = null;

    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|AbandonedRegisterRepository */
    private $abandonedRegisterRepository = null;

    /** @var null|RouterInterface */
    private $router = null;

    public function __construct(
        MailService $mailService,
        EntityManager $entityManager,
        RouterInterface $router
    )
    {
        $this->mailService = $mailService;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->userRepository = $this->entityManager->getRepository(User::class);
        $this->abandonedRegisterRepository = $this->entityManager->getRepository(AbandonedRegister::class);
    }

    public function execute($count = 100)
    {
        $page = 1;
        $arList = $this->abandonedRegisterRepository->getBy($page, $count);

        while (count($arList->getIterator()->getArrayCopy())) {

            $checkDate = new \DateTime();
            $checkDate->sub(new \DateInterval('PT15M'));

            /** @var AbandonedRegister $abandonedRegister */
            foreach ($arList as $abandonedRegister) {
                $user = $this->userRepository->findOneBy(['username' => $abandonedRegister->getUsername(), 'email' => $abandonedRegister->getEmail()]);
                if ($user || !$abandonedRegister->getSubscribe()) {
                    $this->entityManager->remove($abandonedRegister);
                    $this->entityManager->flush();
                    continue;
                }

                if ($abandonedRegister->getCreatedAt() < $checkDate && !$abandonedRegister->getSendAt()) {
                    $this->mailService->registerEmail(
                        'abandoned_register',
                        $abandonedRegister->getEmail(),
                        ['LINK' => $this->router->generate('app.security.register', ['ar' => $abandonedRegister->getId()], RouterInterface::ABSOLUTE_URL)]
                    );

                    $abandonedRegister->setSendAt(new \DateTime());
                    $this->entityManager->persist($abandonedRegister);
                    $this->entityManager->flush();
                }
            }

            $page++;
            $arList = $this->abandonedRegisterRepository->getBy($page, $count);
        }

        return true;
    }

    public function getGroup($id)
    {
        return $this->entityManager->getRepository(Group::class)->findOneBy(['id' => $id]);
    }

    public function getGroupByName($name)
    {
        return $this->entityManager->getRepository(Group::class)->findOneBy(['name' => ucfirst($name)]);
    }
}
