<?php

namespace App\Service;

use \SMSCenter\SMSCenter;

class SmsService
{
    /** @var null|SMSCenter */
    private $smsc = null;

    public function __construct($login, $pass)
    {
        $this->smsc = new \SMSCenter\SMSCenter($login, $pass, false, [
            'charset' => SMSCenter::CHARSET_UTF8,
            'fmt' => SMSCenter::FMT_JSON
        ]);
    }

    public function send($phones, $message, $sender = null, array $options = [])
    {
        if (is_array($phones)) {
            $phones = $this->formatPhoneList($phones);
        } else {
            $phones = $this->formatPhone($phones);
        }

        $phones = array_unique((array)$phones);

        return $this->smsc->send($phones, $message, $sender, $options);
    }

    public function getStatus($phone, $id, $mode = SMSCenter::STATUS_PLAIN)
    {
        return $this->smsc->getStatus($phone, $id, $mode);
    }

    public function isValidPhone($phone)
    {
        $phone = $this->clearPhone($phone);
        if (strlen($phone) == 10) {
            return true;
        }
        return false;
    }

    private function formatPhoneList(array $phones)
    {
        foreach ($phones as $key => $phone) {
            $phones[$key] = $this->formatPhone($phone);
        }
        return $phones;
    }

    private function formatPhone($phone)
    {
        $phone = $this->clearPhone($phone);
        $phone = '+7('.substr($phone, 0, 3).')'.substr($phone, 3, 3).'-'.substr($phone, 6, 2).'-'.substr($phone, 8, 2);

        return $phone;
    }

    public function clearPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        if (in_array(substr($phone, 0, 1), array(7,8))) {
            $phone = substr($phone, 1, strlen($phone));
        }
        return $phone;
    }
}