<?php
namespace App\Service\Ad;

use App\Entity\Ad\Utm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\DependencyInjection\Container;
use DateTime;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UtmService
{
    const SESSION_UTM_VALUE = 'adUrl';

    protected $utmParams = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
    ];

    /** @var Session */
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /** @return Utm | null */
    public function getUtm()
    {
        $url = null;

        $adUrl = $this->session->get(self::SESSION_UTM_VALUE);
        if (!$adUrl) {
            return null;
        }

        return $this->parserUtmFromUrl($adUrl);
    }

    protected function parserUtmFromUrl($url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $urlDataQuery);

        if (empty($urlDataQuery)) {
            return null;
        }

        $utm = new Utm();
        $utm->setUrl($url);

        foreach ($this->utmParams as $utmParamKey) {
            if (isset($urlDataQuery[$utmParamKey])) {
                $utm->addParameter($utmParamKey, $urlDataQuery[$utmParamKey]);
            }
        }

        return $utm;
    }

    public function saveUrl($url)
    {
        $this->session->set(self::SESSION_UTM_VALUE, $url);

        return true;
    }

    public function isUrlWithUtm($url)
    {
         parse_str(parse_url($url, PHP_URL_QUERY), $urlDataQuery);

        if (empty($urlDataQuery)) {
            return false;
        }

        foreach ($this->utmParams as $utmParamKey) {
            if (isset($urlDataQuery[$utmParamKey])) {
                return true;
            }
        }

        return false;
    }
}
