<?php
namespace App\Service;

use App\Entity\Notification\Notification;
use App\Entity\Notification\NotificationGroup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\DependencyInjection\Container;

class NotificationService
{
    /**
     * @var Container
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createNotification($group, \DateTime $dateTime = null)
    {
        if (!$dateTime) {
            $dateTime = new \DateTime();
        }

        $notificationGroupRepository = $this->entityManager->getRepository(NotificationGroup::class);
        $notificationGroup = $notificationGroupRepository->findOneBy(['externalId' => $group]);

        if (!$notificationGroup) {
            return null;
        }

        $notification = new Notification();
        $notification->setGroup($notificationGroup);
        $notification->setLeadTime($dateTime);

        return $notification;
    }
}
