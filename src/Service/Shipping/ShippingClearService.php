<?php

namespace App\Service\Shipping;

use App\Entity\OrderInterface;
use App\Entity\Shipping;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\DeleteMile;


class ShippingClearService
{

    /**
     * @var CallBaseService
     */
    protected $caller;



    public function __construct(CallBaseService $caller)
    {
        $this->caller = $caller;
    }


    /**
     * Проверяем, что в базе неосталось дублирующих доставок
     *
     * @param OrderInterface $order
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     */
    public function removeOldShippings (OrderInterface $order, Shipping $newShipping)
    {
        /** @var Shipping[] $shippings */
        $shippings = $order->getShippings();

        foreach ($shippings as $shipping) {
            if ($shipping->isReject()) {
                continue;
            }

            if ($shipping->getType() == $newShipping->getType() && $shipping->getId() !== $newShipping->getId()) {
                $shipping->setReject(true);

                if ($shipping->getExternalId() && $shipping->getExternalId() != $newShipping->getExternalId()) {
                    $deleteMileRequest = new DeleteMile();
                    $deleteMileRequest->setId($shipping->getExternalId());

                    $this->caller->exec($deleteMileRequest);
                }
            }
        }
    }

}
