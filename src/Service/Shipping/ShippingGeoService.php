<?php

namespace App\Service\Shipping;

use App\Entity\Shipping;
use App\Service\CalcDistanceService;


class ShippingGeoService
{

    protected $calcDistanceService;


    public function __construct(CalcDistanceService $calcDistanceService)
    {
        $this->calcDistanceService = $calcDistanceService;
    }


    /**
     * Заполняем доставку гео данными
     *
     * @param Shipping $shipping
     *
     * @return bool
     */
    public function fill(Shipping $shipping)
    {
        $stock = $shipping->getStock();
        $address = $shipping->getAddress();

        try {
            $geo = $this->calcDistanceService->getDirection($stock->getAddress(), $address);

            $geo = $geo['routes'][0]['legs'][0];

            $shipping->setDistance(ceil($geo['distance']['value'] / 1000));
            $shipping->setLat($geo['end_location']['lat']);
            $shipping->setLng($geo['end_location']['lng']);
        } Catch (\Exception $e) {
            return false;
        }

        return true;
    }





}
