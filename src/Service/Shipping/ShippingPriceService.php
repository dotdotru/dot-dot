<?php

namespace App\Service\Shipping;

use App\Entity\OrderInterface;
use App\Entity\Shipping;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\CalcMileCost;
use App\Service\User\UserVatService;


class ShippingPriceService
{

    /**
     * @var CallBaseService
     */
    protected $caller;


    /**
     * @var UserVatService
     */
    protected $userVatService;


    public function __construct(CallBaseService $caller, ShippingGeoService $shippingGeoService, UserVatService $userVatService)
    {
        $this->caller = $caller;
        $this->userVatService = $userVatService;
    }


    /**
     * Считаем цену доставки и устанавливаем в милю
     *
     * @param OrderInterface $order
     * @param Shipping $shipping
     */
    public function fill (OrderInterface $order, Shipping $shipping)
    {
        $price = $this->calc($order, $shipping);
        $shipping->setPrice($price);
    }


    /**
     * Считаем цену доставки
     *
     * @param OrderInterface $order
     * @param Shipping $shipping
     * @return |null
     */
    public function calc (OrderInterface $order, Shipping $shipping, $vat = false)
    {
        $calcRequest = new CalcMileCost();
        $calcRequest->setOrder($order);
        $calcRequest->setShipping($shipping);

        if ($order->getUser()) {
            $vat = $this->userVatService->get($order->getUser());
        }
        $calcRequest->setVat($vat);

        $result = $this->caller->exec($calcRequest);

        if ($result['success']) {
            return $result['data']['total']['total'];
        } else {
            return null;
        }
    }

}
