<?php

namespace App\Service\Shipping;

use App\Entity\BatchMile;
use App\Entity\Document;
use App\Entity\OrderInterface;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Entity\Wms\WmsBatchMover;
use App\Repository\Organization\OrganizationRepository;
use App\Service\MailService;
use App\Service\SmsService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Templating\EngineInterface;


class BatchNotificationService
{

    public function __construct(
        EntityManager $entityManager,
        MailService $mailService,
        SmsService $smsService,
        $notificationMail
    ){
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->notificationMail = $notificationMail;
        $this->smsService = $smsService;
    }


    public function notificationBatchReserved(BatchMile $batchMile)
    {
        $driver = $batchMile->getDriver();
        $shipping = $batchMile->getShippings()[0];
        $order = $batchMile->getOrder();

        $cityName = $shipping->getStock()->getCity()->getTitle();

        // Отправка уведомлений складу о бронировании партии
        $this->mailService->registerEmailHtmlTemplate(
            'miles/reserve.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' забронирована перевозчиком',
            $this->notificationMail,
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );


        // Отправка уведомлений перевозчику о бронировании партии
        $carrier = $batchMile->getCarrier();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/reserve_inside.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' забронирована перевозчиком',
            $carrier->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );


        // Отправка уведомлений отправитель о бронировании партии
        $clientGeneratePdf = [];
        if ($order->getUser()->getUserPerson() && $order->getSender() && $order->getUser()->getUserPerson()->getId() != $order->getSender()->getId()) {
            $clientGeneratePdf[] = [
                'callback' => 'generateClient',
                'arguments' => [$order->getId(), 'driver-confirm'],
                'filename' => 'driver-confirm.pdf'
            ];
        }

        $sender = $order->getUser();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/reserve.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' забронирована перевозчиком',
            $sender->getEmail(),
            [
                'order' => $order,
                'shipping' => $shipping,
                'driver' => $driver,
                'generatePdf' => $clientGeneratePdf
            ]
        );

        try {
            $driverName = $driver->getName();
            $driverPhone = $driver->getPhone();
            $driverDocument = '';
            /** @var Document $document */
            foreach ($driver->getDocuments() as $document) {
                if ($document->getType() == 'passport') {
                    $driverDocument .= $document->getSeries() . $document->getNumber();
                }
                if (false && $document->getType() == 'license') {
                    $driverDocument .= $document->getSeries() . ' ' . $document->getNumber();
                }
            }

            $this->smsService->send($sender->getPhone(), 'Заказ №' . $batchMile->getTitleId() . ': назначен водитель ' . $driverName . ' ' . $driverDocument . ' ' . $driverPhone);
        } Catch (\Exception $e) {}
    }


    public function notificationBatchLoaded(WmsBatchMover $batchMile)
    {
        $driver = $batchMile->getDriver();
        $shipping = $batchMile->getShipping();
        if (!count($shipping->getOrder())) {
            return;
        }
        $order = $shipping->getOrder()[0];

        $cityName = $shipping->getStock()->getCity()->getTitle();


        // Отправка уведомлений перевозчику о отгрузки партии
        $carrier = $batchMile->getCarrier();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/loaded.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' отгружена',
            $carrier->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );

        // Отправка уведомлений отправитель об отгрузки партии
        $sender = $order->getUser();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/loaded.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' отгружена',
            $sender->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );
    }


    public function notificationBatchAccepted(WmsBatchMover $batchMile)
    {
        $driver = $batchMile->getDriver();
        $shipping = $batchMile->getShipping();
        if (!count($shipping->getOrder())) {
            return;
        }
        $order = $shipping->getOrder()[0];

        $cityName = $shipping->getStock()->getCity()->getTitle();


        // Отправка уведомлений перевозчику о принятии партии
        $carrier = $batchMile->getCarrier();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/accepted.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' принята',
            $carrier->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );

        // Отправка уведомлений отправитель о принятии партии
        $sender = $order->getUser();
        $this->mailService->registerEmailHtmlTemplate(
            'miles/accepted.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' принята',
            $sender->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'driver' => $driver]
        );
    }


    public function notificationBatchCarrierCancel(BatchMile $batchMile)
    {
        $shipping = $batchMile->getShippings()[0];
        $order = $batchMile->getOrder();

        $cityName = $shipping->getStock()->getCity()->getTitle();

        $penalty = '';
        if ($shipping->getPickAt()) {
            $now = clone $shipping->getPickAt(); //new \DateTime();

            /** @var \DateTime $tmpDate1 */
            $tmpDate1 = clone $shipping->getPickAt();
            $tmpDate1->modify('-1 days')->setTime(16, 0);
            /** @var \DateTime $tmpDate2 */
            $tmpDate2 = clone $shipping->getPickAt();
            $tmpDate2->setTime(9, 0);

            if ($now > $tmpDate2) {
                $penalty .= 'Отменить партию после 9:00 в день заказа штраф 100% от стоимости партии';
            } else if ($now > $tmpDate1) {
                $penalty .= 'Отменить партию после 16:00 за день до заказа и до 9:00 в день заказа штраф 50% от стоимости партии';
            }
        }

        // Отправка уведомлений складу об отмене партии
        $this->mailService->registerEmailHtmlTemplate(
            'batch/cancel_inside.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' отменена перевозчиком',
            $this->notificationMail,
            ['order' => $order, 'shipping' => $shipping, 'penalty' => $penalty]
        );

        // Отправка уведомлений перевозчику об отмене партии
        $carrier = $batchMile->getCarrier();
        $this->mailService->registerEmailHtmlTemplate(
            'batch/cancel_carrier.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' отменена перевозчиком',
            $carrier->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'penalty' => $penalty]
        );

        // Отправка уведомлений отправитель об отмене партии
        $sender = $order->getUser();
        $this->mailService->registerEmailHtmlTemplate(
            'batch/cancel.html.twig',
            $cityName . ' Партия №' . $batchMile->getTitleId() . ' отменена перевозчиком',
            $sender->getEmail(),
            ['order' => $order, 'shipping' => $shipping, 'penalty' => $penalty]
        );

        try {
            $this->smsService->send($sender->getPhone(), 'Партия №' . $batchMile->getTitleId() . ' отменена перевозчиком');
        } Catch (\Exception $e) {}
    }

    protected function formatPhone ($str)
    {
        return $str;
    }




}
