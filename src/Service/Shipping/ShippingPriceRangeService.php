<?php

namespace App\Service\Shipping;

use App\Entity\Order;
use App\Entity\Shipping;
use App\Service\Integration\CallBaseService;


class ShippingPriceRangeService
{

    /**
     * @var CallBaseService
     */
    protected $caller;

    /**
     * @var ShippingPriceService
     */
    protected $shippingPriceService;


    public function __construct(CallBaseService $caller, ShippingGeoService $shippingGeoService, ShippingPriceService $shippingPriceService)
    {
        $this->caller = $caller;
        $this->shippingPriceService = $shippingPriceService;
    }


    /**
     * Получаем диапазон цен доставки до склада
     *
     * @return |null
     */
    public function calc (Order $order, $type = Shipping::TYPE_PICKUP, $vat = false)
    {
        $shipping = new Shipping();
        $shipping->setActive(true);
        $shipping->setCountLoader(0);
        $shipping->setHydroBoard(false);

        $shipping->setStock($type == Shipping::TYPE_PICKUP ? $order->getStockFrom() : $order->getStockTo());
        $shipping->setType($type);

        // @todo брать актуальные цифры из тарифа
        $shipping->setDistance(10);
        $min = $this->shippingPriceService->calc($order, $shipping, $vat);
        $shipping->setDistance(40);
        $max = $this->shippingPriceService->calc($order, $shipping, $vat);

        return [
            'min' => $min,
            'max' => $max
        ];
    }

}
