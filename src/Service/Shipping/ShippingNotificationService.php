<?php

namespace App\Service\Shipping;

use App\Entity\Notification\NotificationQueue;
use App\Entity\OrderInterface;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Repository\Organization\OrganizationRepository;
use App\Service\MailService;
use App\Service\SmsService;
use Doctrine\ORM\EntityManager;


class ShippingNotificationService
{

    public function __construct(
        EntityManager $entityManager,
        MailService $mailService,
        SmsService $smsService,
        $notificationMail
    ) {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->notificationMail = $notificationMail;
    }

    /**
     * Шлем уведомления, если это необходимо
     *
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     * @return mixed
     */
    public function notification(OrderInterface $order, Shipping $newShipping, Shipping $oldShipping = null)
    {

        if ($oldShipping && $newShipping && $this->shippingChanged($newShipping, $oldShipping)) {
            if ($oldShipping->isActive() && $newShipping->isActive() === false) {
                $this->notificationCancelShipping($order, $newShipping);
            } else {
                $this->notificationEditShipping($order, $oldShipping, $newShipping);
            }
        }

        // Создан заказ мили
        if (!$oldShipping && $newShipping->isActive()) {
            $this->notificationCreateShipping($order, $newShipping);
        }

        if (!$newShipping && $oldShipping) {
            $this->notificationCancelShipping($order, $oldShipping);
        }

        // Назначен водитель
        if ($newShipping->isActive() && $newShipping->getDriver() && (!$oldShipping || !$oldShipping->getDriver())) {

        }

        // Водитель был отменен
        if ($oldShipping && $newShipping->isActive() && !$newShipping->getDriver() && $oldShipping->getDriver()) {

        }

        return true;
    }


    protected function shippingChanged (Shipping $newShipping, Shipping $oldShipping)
    {
        $dateChanged = (bool)$newShipping->getPickAt() xor (bool)$oldShipping->getPickAt();
        $dateChanged = $dateChanged || $newShipping->getPickAt() && $oldShipping->getPickAt() &&  $newShipping->getPickAt()->format('d.m.Y H:i') != $oldShipping->getPickAt()->format('d.m.Y H:i');

        if ($newShipping->isActive() !== $oldShipping->isActive()
            || $newShipping->getAddress() != $oldShipping->getAddress()
            || $newShipping->getCountLoader() != $oldShipping->getCountLoader()
            || $newShipping->getHydroBoard() != $oldShipping->getHydroBoard()
            || ($dateChanged)
        ) {
            return true;
        }

        return false;
    }



    public function notificationCreateShipping(OrderInterface $order, Shipping $shipping)
    {
        $cityName = $shipping->getStock()->getCity()->getTitle();
        $titleId = $order->getId() . ($shipping->getType() == Shipping::TYPE_PICKUP ? '-1' : '-L');

        $this->mailService->registerEmailHtmlTemplate(
            'miles/create_inside.html.twig',
            $cityName . ' Заказ №' . $titleId . ' создан',
            $this->notificationMail,
            ['order' => $order, 'shipping' => $shipping]
        );

        // Отправка уведомлений о создании заказа
        if ($shipping->getPrice()) {
            $notificationQueue = new NotificationQueue();
            $notificationQueue->setType('mile_created');
            $notificationQueue->setPayload(['order_id' => $order->getId(), 'shipping' => $shipping->getId()]);

            $this->entityManager->persist($notificationQueue);
            $this->entityManager->flush($notificationQueue);
        }

    }

    public function notificationEditShipping(OrderInterface $order, Shipping $oldShipping, Shipping $newShipping = null, $changeReceiver = false)
    {
        $cityName = $newShipping->getStock()->getCity()->getTitle();
        $titleId = $order->getId() . ($newShipping->getType() == Shipping::TYPE_PICKUP ? '-1' : '-L');

        $this->mailService->registerEmailHtmlTemplate(
            'miles/edit_inside.html.twig',
            $cityName . ' Заказ №' . $titleId . ' изменен клиентом',
            $this->notificationMail,
            ['order' => $order, 'shipping' => $newShipping, 'old_shipping' => $oldShipping]
        );


        // Отправляем уведомление перевозчику
        if ($oldShipping->getDriver()) {
            $organization = $this->entityManager->getRepository(Organization::class)->findByDriver($oldShipping->getDriver());

            if ($organization) {
                $carrier = $organization->getUser();

                $this->mailService->registerEmailHtmlTemplate(
                    'miles/edit_carrier.html.twig',
                    $cityName . ' Заказ №' . $titleId . ' изменен клиентом',
                    $carrier->getEmail(),
                    ['order' => $order, 'shipping' => $newShipping, 'old_shipping' => $oldShipping]
                );
            }
        }
    }


    /**
     * Отправляем уведомления при отмене заказа
     *
     * @param OrderInterface $order
     * @param Shipping $shipping
     */
    public function notificationCancelShipping(OrderInterface $order, Shipping $shipping)
    {
        $cityName = $shipping->getStock()->getCity()->getTitle();
        $titleId = $order->getId() . ($shipping->getType() == Shipping::TYPE_PICKUP ? '-1' : '-L');

        $penalty = '';
        if ($shipping->getPickAt()) {
            $now = clone $shipping->getPickAt(); //new \DateTime();

            $tmpDate1 = (clone $shipping->getPickAt())->modify('-1 days')->setTime(16, 0);
            $tmpDate2 = (clone $shipping->getPickAt())->setTime(9, 0);

            if ($now > $tmpDate2) {
                $penalty .= 'Отменить партию после 9:00 в день заказа штраф 100% от стоимости партии';
            } else if ($now > $tmpDate1) {
                $penalty .= 'Отменить партию после 16:00 за день до заказа и до 9:00 в день заказа штраф 50% от стоимости партии';
            }
        }

        $this->mailService->registerEmailHtmlTemplate(
            'miles/cancel_inside.html.twig',
            $cityName . ' Заказ №' . $titleId . '  отменен клиентом',
            $this->notificationMail,
            ['order' => $order, 'shipping' => $shipping, 'penalty' => $penalty]
        );


        // Отправляем уведомление перевозчику
        if ($shipping->getDriver()) {
            $organization = $this->entityManager->getRepository(Organization::class)->findByDriver($shipping->getDriver());

            if ($organization) {
                $carrier = $organization->getUser();

                $this->mailService->registerEmailHtmlTemplate(
                    'miles/cancel_carrier.html.twig',
                    $cityName . ' Заказ №' . $titleId . ' отменен клиентом',
                    $carrier->getEmail(),
                    ['order' => $order, 'shipping' => $shipping]
                );


                try {
                    $this->smsService->send($carrier->getPhone(), 'Заказ №' . $titleId . ' отменен клиентом');
                } Catch (\Exception $e) {}
            }
        }
    }

}
