<?php

namespace App\Service\Shipping;

use App\Entity\OrderInterface;
use App\Entity\Shipping;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\DeleteMile;


class ShippingRemoveAllService
{

    /**
     * @var CallBaseService
     */
    protected $caller;


    /**
     * @var ShippingNotificationService
     */
    protected $shippingNotificationService;



    public function __construct(
        CallBaseService $caller,
        ShippingNotificationService $shippingNotificationService
    ) {
        $this->caller = $caller;
        $this->shippingNotificationService = $shippingNotificationService;
    }


    /**
     * Удаляем все доставки заказа
     *
     * @param OrderInterface $order
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     */
    public function removeOldShippings (OrderInterface $order)
    {
        /** @var Shipping[] $shippings */
        $shippings = $order->getShippings();

        foreach ($shippings as $shipping) {
            if ($shipping->isReject()) {
                continue;
            }

            $shipping->setReject(true);

            if ($shipping->isActive() && $shipping->getExternalId()) {
                $deleteMileRequest = new DeleteMile();
                $deleteMileRequest->setId($shipping->getExternalId());

                $this->caller->exec($deleteMileRequest);
                $this->shippingNotificationService->notificationCancelShipping($order, $shipping);
            }
        }
    }

}
