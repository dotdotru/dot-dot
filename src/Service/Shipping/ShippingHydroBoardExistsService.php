<?php

namespace App\Service\Shipping;

use App\Entity\City;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\GetMilePrices;
use App\Service\Integration\Mile\GetMileTariffs;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Cache\ItemInterface;


class ShippingHydroBoardExistsService
{

    /**
     * @var CallBaseService
     */
    protected $caller;


    /**
     * @var AdapterInterface
     */
    protected $cache;


    public function __construct(CallBaseService $caller, AdapterInterface $cache)
    {
        $this->caller = $caller;
        $this->cache = $cache;
    }


    /**
     * Проверяем, доступен ли гидроборт для адресной доставки в данном городе
     *
     * @param City $city
     * @return
     */
    public function check (City $city, $weight = null)
    {
        $cacheKey = 'miles_hydroboard_' . $city->getId();

        $available = $this->cache->get($cacheKey, function (ItemInterface $item) use($city, $weight) {
            $item->expiresAfter(24 * 3600);

            $mileTariffs = new GetMileTariffs();
            $tariffs = $this->caller->exec($mileTariffs);

            // @todo обсудить это, гидроборт привязан к грузчикам
            if ($tariffs['success']) {
                foreach ($tariffs['data'] as $tariff) {
                    if (!$tariff['tail_lift']) {
                        continue;
                    }

                    $milePrices = new GetMilePrices();
                    $milePrices->setCityId($city->getId())
                        ->setTariff($tariff['id']);

                    $prices = $this->caller->exec($milePrices);

                    if ($prices['success']) {
                        foreach ($prices['data'] as $price) {
                            $priceWeight = explode(',', trim($price['weight'], '()[]'));
                            $priceWeight = !empty($priceWeight[1]) ? $priceWeight[1] : 0;

                            if ($weight < $priceWeight) {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        });

        return $available;
    }


}
