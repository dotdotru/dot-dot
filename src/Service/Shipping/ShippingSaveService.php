<?php

namespace App\Service\Shipping;

use App\Entity\OrderInterface;
use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\DeleteMile;
use App\Service\Integration\Mile\SaveMile;



class ShippingSaveService
{

    /**
     * @var CallBaseService
     */
    protected $caller;

    /**
     * @var ShippingGeoService
     */
    protected $shippingGeoService;

    /**
     * @var ShippingPriceService
     */
    protected $shippingPriceService;


    public function __construct(CallBaseService $caller, ShippingGeoService $shippingGeoService, ShippingPriceService $shippingPriceService)
    {
        $this->caller = $caller;
        $this->shippingGeoService = $shippingGeoService;
        $this->shippingPriceService = $shippingPriceService;
    }

    /**
     * Сохраняем доставку
     *
     * @param OrderInterface $order
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     * @return mixed
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function save(OrderInterface $order, Shipping $newShipping, Shipping $oldShipping = null)
    {
        // Миля отменилась, удаляем мили и ничего не делаем
        if (!$newShipping->isActive()) {
            $this->removeOldShippings($order, $newShipping);
            return $newShipping;
        }

        // Заполняем координаты и расстояние
        $this->shippingGeoService->fill($newShipping);
        $this->shippingPriceService->fill($order, $newShipping);

        $externalId = $oldShipping ? $oldShipping->getExternalId() : null;

        if ($order instanceof WmsOrder) {
            if ($order->getOrder()) {
                $orderId = $order->getOrder()->getExternalId();
            } else {
                // Такого быть не должно
                throw new \Exception('Empty order');
            }
        } else {
            $orderId = $order->getExternalId();
        }

        $saveShipping = new SaveMile();
        $saveShipping->setId($externalId)
            ->setOrderId($orderId)
            ->setDistance($newShipping->getDistance())
            ->setLat($newShipping->getLat())
            ->setLng($newShipping->getLng())
            ->setNumLoaders($newShipping->getCountLoader())
            ->setAddress($newShipping->getAddress())
            ->setRefrigerator(false)
            ->setType($newShipping->getType())
            ->setCost($newShipping->getPrice())
            ->setType($newShipping->getType());

        if ($newShipping->getPickAt()) {
            $saveShipping->setTime($newShipping->getPickAt());
        }

        $result = $this->caller->exec($saveShipping);

        if ($result['success']) {
            $externalId = $saveShipping->getId() ? $saveShipping->getId() : $result['data']['id'];
            $newShipping->setExternalId($externalId);
        } else {
            // error
        }

        $this->removeOldShippings($order, $newShipping);

        return $newShipping;
    }


    /**
     * Проверяем, что в базе неосталось дублирующих доставок
     *
     * @param OrderInterface $order
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     */
    protected function removeOldShippings (OrderInterface $order, Shipping $newShipping)
    {
        /** @var Shipping[] $shippings */
        $shippings = $order->getShippings();

        foreach ($shippings as $shipping) {
            if ($shipping->isReject()) {
                continue;
            }

            if ($shipping->getType() == $newShipping->getType() && $shipping->getId() !== $newShipping->getId()) {
                $shipping->setReject(true);

                if ($shipping->getExternalId() && $shipping->getExternalId() != $newShipping->getExternalId()) {
                    $deleteMileRequest = new DeleteMile();
                    $deleteMileRequest->setId($shipping->getExternalId());

                    $this->caller->exec($deleteMileRequest);
                }
            }
        }
    }

}
