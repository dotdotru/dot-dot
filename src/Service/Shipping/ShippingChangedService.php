<?php

namespace App\Service\Shipping;

use App\Entity\OrderInterface;
use App\Entity\Shipping;


class ShippingChangedService
{


    /**
     * Проверяем, возможно ли редактирование доставки в данный момент
     *
     * @param OrderInterface $order
     * @param Shipping $newShipping
     * @param Shipping|null $oldShipping
     * @return bool
     */
    public function changeAvailable(OrderInterface $order, Shipping $newShipping, Shipping $oldShipping = null)
    {
        if ($oldShipping && $oldShipping->getDone()) {
            return false;
        }

        return true;
    }


    public function isChanged (Shipping $newShipping, Shipping $oldShipping = null)
    {
        if ($newShipping && !$oldShipping) {
            return true;
        }

        $dateChanged = (bool)$newShipping->getPickAt() xor (bool)$oldShipping->getPickAt();
        $dateChanged = $dateChanged || $newShipping->getPickAt() && $oldShipping->getPickAt() &&  $newShipping->getPickAt()->format('d.m.Y H:i') != $oldShipping->getPickAt()->format('d.m.Y H:i');

        if ($newShipping->isActive() !== $oldShipping->isActive()
            || $newShipping->getAddress() != $oldShipping->getAddress()
            || $newShipping->getCountLoader() != $oldShipping->getCountLoader()
            || $newShipping->getHydroBoard() != $oldShipping->getHydroBoard()
            || ($dateChanged)
        ) {
            return true;
        }

        return false;
    }






}
