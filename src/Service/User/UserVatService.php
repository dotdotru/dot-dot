<?php

namespace App\Service\User;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Authtoken;
use App\Service\Order\RecalculateOrderPriceService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class UserVatService
{

    /**
     * @var EntityManager
     */
    protected $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Проверяем НО клиента
     *
     * @param User $user
     * @param string $group
     * @return bool
     */
    public function get(User $user, $group = 'Sender')
    {
        $userVat = false;

        if ($user->getOrganizationCustomer()) {
            $customer = $user->getOrganizationCustomer();
            if ($customer && $customer->getNds()) {
                $userVat = $customer->getNds();
            }
        } else if ($user->getOrganizationCarrier()) {
            $customer = $user->getOrganizationCarrier();
            if ($customer && $customer->getNds()) {
                $userVat = $customer->getNds();
            }
        }

        return $userVat;
    }



}
