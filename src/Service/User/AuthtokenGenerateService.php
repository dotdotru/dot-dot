<?php

namespace App\Service\User;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Authtoken;
use Doctrine\ORM\EntityManager;

class AuthtokenGenerateService
{

    /**
     * @var EntityManager
     */
    protected $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Возвращает массив уникальных ключей, если в базе не хватает ключей, генерирует новые
     *
     * @param User $user
     * @param int $limit
     * @param int $length
     * @throws \Doctrine\ORM\ORMException
     *
     * @return Authtoken[]
     */
    public function generate(User $user, $limit = 100, $length = 10)
    {
        $authtokenRepository = $this->em->getRepository(Authtoken::class);
        $tokens = $authtokenRepository->findBy([
            'user' => $user,
            'order' => null
        ]);

        for ($i = count($tokens); $i < $limit; $i++) {
            $token = new Authtoken();
            $token->setCode($this->uniqueString($length))
                ->setUser($user);

            $this->em->persist($token);
            $tokens[] = $token;
        }

        try {
            $this->em->flush();
        } Catch (\Exception $e) {}

        return $tokens;
    }


    /**
     * Генерируем строку, пока не получим уникальную
     *
     * @param int $length
     * @return false|string
     */
    protected function uniqueString($length = 10)
    {
        $authtokenRepository = $this->em->getRepository(Authtoken::class);

        //$unique = strtoupper(substr(md5(microtime() . rand(1,100000)), 0, $length));
        $unique = strtoupper(substr(md5(microtime() . rand(1,100000)), 0, 10));
        // если в базе уже есть такая строка, генерируем новую
        if ($authtokenRepository->findBy(['code' => $unique])) {
            return $this->uniqueString($length);
        }

        return $unique;
    }


}
