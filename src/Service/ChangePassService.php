<?php

namespace App\Service;

use App\Entity\Organization\Organization;
use App\Form\UserType;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use SMSCenter\SMSCenter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Session;

class ChangePassService
{
    const SETTING_GROUP = 'change_pass';
    const CHANGE_CODE = 'change_code';

    /** @var null|SmsService */
    private $smsService = null;

    /** @var null|Settings */
    private $settings = null;

    /** @var null|Session */
    private $session = null;

    public function __construct(
        SmsService $smsService,
        Settings $settings,
        Session $session
    )
    {
        $this->smsService = $smsService;
        $this->settings = $settings;
        $this->session = $session;
    }

    public function sendSms($phone)
    {
        $settings = $this->settings->group(self::SETTING_GROUP);

        $code = $this->generateCode();
        $smsText = str_replace('#CODE#', $code, $settings['sms']);

        $this->session->set(self::CHANGE_CODE, $code);
        $this->smsService->send($phone, $smsText);

        return $code;
    }

    public function checkSmsCode($code)
    {
        if (trim($code) == $this->session->get(self::CHANGE_CODE)) {
            return true;
        }

        return false;
    }

    private function generateCode()
    {
        return $rand = rand('1000','9999');
    }

    public function isValidPhone($phone)
    {
        return $this->smsService->isValidPhone($phone);
    }

    public function clearPhone($phone)
    {
        return $this->smsService->clearPhone($phone);
    }
}
