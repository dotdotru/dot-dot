<?php
namespace App\Service\Pdf;

use App\Entity\AbstractBatch;
use App\Entity\BatchMile;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Templating\EngineInterface;
use Vsavritsky\SettingsBundle\Service\Settings;
use Knp\Snappy\Pdf;


class GenerateService
{

    /**
     * @var Settings
     */
    protected $settings;


    /**
     * @var EntityManagerInterface
     */
    protected $em;


    /**
     * @var EngineInterface
     */
    protected $templating;


    protected $pdfGenerator;


    public function __construct(
        Settings $settings,
        EntityManagerInterface $em,
        EngineInterface $templating,
        Pdf $pdfGenerator
    ) {
        $this->settings = $settings;
        $this->em = $em;
        $this->templating = $templating;
        $this->pdfGenerator = $pdfGenerator;
    }



    public function generateClient ($orderId, $type)
    {
        if (!in_array($type, ['tn-1', 'er', 'driver-confirm'])) {
            return false;
        }

        $order = $this->em->getRepository(Order::class)->find($orderId);
        if (!$order) {
            return false;
        }

        $offerDate = $this->settings->get('offer_date');

        $dataTemplate = [
            'order'  => $order,
            'offerDate' => $offerDate
        ];

        if ($type == 'tn-1' || $type == 'driver-confirm') {
            $shipping = $order->getShipping('pickup');
            if ($shipping->getDriver()) {
                $organization = $this->em->getRepository(Organization::class)->findByDriver($shipping->getDriver());

                $qb = $this->em->createQueryBuilder();
                $qb->select('b')
                    ->from(BatchMile::class, 'b')
                    ->join('b.shippings', 's')
                    ->andWhere('s.id = :id')
                    ->setParameter('id', $shipping->getId())
                    ->andWhere('b.status = :status')
                    ->setParameter('status', BatchMile::STATUS_WAITING);
                $batchMover =  $qb->getQuery()->getResult();

                $carrier = $organization->getUser();
                $dataTemplate['carrier'] = $carrier;
                $dataTemplate['batch'] = !empty($batchMover) ? $batchMover[0] : null;
            }
        }

        $pathTemplate = 'site/pdf/order/'.$type.'.html.twig';

        try {
            $html = $this->templating->render($pathTemplate, $dataTemplate);
            $pdf = $this->pdfGenerator->getOutputFromHtml($html);
        } Catch (\Exception $e) {
            return false;
        }

        return $pdf;
    }



}
