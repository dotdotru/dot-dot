<?php


namespace App\Service\Integration\HandlingPrice;


use App\Service\Integration\AbstractRequest;



class SaveHandlingPrice extends AbstractRequest
{

    protected $url = "/site/save/handling-price";

    protected $method = 'POST';


    protected $id = null;

    protected $weight = '';

    protected $price = 0;



    public function getParams ()
    {
        $params = [];

        if ($this->id) {
            $params['id'] = (int)$this->id;
        }
        $params['weight'] = $this->weight;
        $params['price'] = $this->price;

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getWeight(): string
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     */
    public function setWeight(string $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }



}
