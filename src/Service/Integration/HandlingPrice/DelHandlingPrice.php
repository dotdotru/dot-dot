<?php


namespace App\Service\Integration\HandlingPrice;


use App\Service\Integration\AbstractRequest;



class DelHandlingPrice extends AbstractRequest
{

    protected $url = "/site/action/del-handling-price";

    protected $method = 'POST';


    protected $id = null;



    public function getParams ()
    {
        $params = [];

        if ($this->id) {
            $params['id'] = (int)$this->id;
        }

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


}
