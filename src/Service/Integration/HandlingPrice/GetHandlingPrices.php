<?php


namespace App\Service\Integration\HandlingPrice;


use App\Service\Integration\AbstractRequest;

class GetHandlingPrices extends AbstractRequest
{

    protected $url = "/site/get/handling-prices";



    public function getParams ()
    {
        return [];
    }

}
