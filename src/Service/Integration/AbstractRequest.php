<?php

namespace App\Service\Integration;

use App\Entity\Shipping;


class AbstractRequest
{

    protected $url;


    protected $method = 'GET';


    protected $response = [];


    public function getParams()
    {
        return [];
    }


    public function setResponse($response)
    {
        $this->response = $response;
    }


    public function getResponse()
    {
        return $this->response;
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return AbstractRequest
     */
    public function setMethod(string $method): AbstractRequest
    {
        $this->method = $method;
        return $this;
    }


    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return AbstractRequest
     */
    public function setUrl(string $url): AbstractRequest
    {
        $this->url = $url;
        return $this;
    }


}
