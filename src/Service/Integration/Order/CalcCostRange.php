<?php


namespace App\Service\Integration\Order;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\OrderPackage;
use App\Entity\Wms\WmsOrder;
use App\Service\Integration\AbstractRequest;

class CalcCostRange extends AbstractRequest
{

    protected $url = "/site/post/calc-order-cost-range";


    protected $method = 'POST';

    /**
     * @var OrderInterface
     */
    protected $order = null;

    /**
     * @var bool Не используем промокод, даже если он передан в заказе
     */
    protected $notUsePromocode = false;


    protected $vat = null;

    /**
     * @var bool Из результирующей цены исключим цены за упаковку
     */
    protected $excludePackagePrice = false;


    public function getParams ()
    {
        return [
            'order' => $this->mapEntityCustomerToBase($this->order),
            'vat' => $this->vat
        ];
    }

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    /**
     * @param OrderInterface $order
     * @return CalcCostRange
     */
    public function setOrder(OrderInterface $order): CalcCostRange
    {
        $this->order = $order;
        return $this;
    }


    /**
     * @return bool
     */
    public function isNotUsePromocode(): bool
    {
        return $this->notUsePromocode;
    }

    /**
     * @param bool $notUsePromocode
     * @return CalcCostRange
     */
    public function setNotUsePromocode(bool $notUsePromocode): CalcCostRange
    {
        $this->notUsePromocode = $notUsePromocode;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExcludePackagePrice(): bool
    {
        return $this->excludePackagePrice;
    }

    /**
     * @param bool $excludePackagePrice
     * @return CalcCostRange
     */
    public function setExcludePackagePrice(bool $excludePackagePrice): CalcCostRange
    {
        $this->excludePackagePrice = $excludePackagePrice;
        return $this;
    }

    /**
     * @return null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param null $vat
     * @return CalcCostRange
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }





    /**
     * @todo вынести в общий класс
     *
     * @param OrderInterface $orderCustomer
     * @return array
     */
    public function mapEntityCustomerToBase(OrderInterface $orderCustomer)
    {

        $data = [
            'id' => $orderCustomer->getExternalId() ? $orderCustomer->getExternalId() : null,
            'route' => $orderCustomer->getDirection()->getExternalId(),
            'declared_value' => $orderCustomer->getDeclaredPrice(),
        ];

        if ($orderCustomer->getUser()) {
            $data['customer'] = $orderCustomer->getUser()->getExternalId();
        }

        $data['order_lines'] = [];
        if ($orderCustomer instanceof Order &&  $orderCustomer && $orderCustomer->getOrderPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($orderCustomer->getOrderPackages() as $orderPackage) {
                $volume = $orderPackage->getCalculateWeight() ? (float)$orderPackage->getCalculateWeight() : null;
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'volume' => $volume,
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $this->isExcludePackagePrice() ? 0 : $orderPackage->getPackingPrice(),

                ];
                if ($orderCustomer->isByCargo()) {
                    $orderPackageData['totals'] = true;
                }
                $data['order_lines'][] = $orderPackageData;
            }
        }
        if ($orderCustomer instanceof WmsOrder &&  $orderCustomer && $orderCustomer->getOrderPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($orderCustomer->getPackages() as $orderPackage) {
                $volume = $orderPackage->getCalculateWeight() ? (float)$orderPackage->getCalculateWeight() : null;
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'volume' => $volume,
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $this->isExcludePackagePrice() ? 0 : $orderPackage->getPackingPrice(),
                ];
                $data['order_lines'][] = $orderPackageData;
            }
        }



        //$data['customer_delivery_time'] = $orderCustomer->getCustomerDeliveryTime() ? $orderCustomer->getCustomerDeliveryTime() : null;
        $data['cost_min'] = $orderCustomer->getMinPrice();
        $data['cost_max'] = $orderCustomer->getMaxPrice();
        $data['insurance'] = $orderCustomer->isInsuredSuccess() ? $orderCustomer->getInsuredNumber() : '';
        $data['price'] = (float)$orderCustomer->getPrice();

        $data['warehouse_from'] = null;
        if ($orderCustomer->getStockFrom()) {
            $data['warehouse_from'] = $orderCustomer->getStockFrom()->getExternalId();
        }

        //if ($shipping = $orderCustomer->getShipping(Shipping::TYPE_PICKUP)) {
//            $data['warehouse_from'] = $shipping->getStock()->getExternalId();
//        }

        $data['warehouse_to'] = null;
        if ($orderCustomer->getStockTo()) {
            $data['warehouse_to'] = $orderCustomer->getStockTo()->getExternalId();
        }

  //      if ($shipping = $orderCustomer->getShipping(Shipping::TYPE_DELIVER)) {
    //        $data['warehouse_to'] = $shipping->getStock()->getExternalId();
      //  }

        $data['warehouse_from_pass_time'] = null;
        if ($orderCustomer->getReceptionAt()) {
            $data['warehouse_from_pass_time'] = $orderCustomer->getReceptionAt()->getTimestamp();
        }

        $data['paid'] = $orderCustomer->isPayed();
        $data['ext_id'] = $orderCustomer->getId();

        /** @var Promocode $promocode */
        $data['weight_discount'] = 0;
        $data['insurance_discount'] = 0;
        $promocode = $orderCustomer->getPromocode();
        if ($promocode && !$this->isNotUsePromocode()) {
            $data['weight_discount'] = (int)$promocode->getDiscountKilo();
            $data['insurance_discount'] = (int)$promocode->getDiscountDeclaredValue();
        }

        $data['ext_id'] = $orderCustomer->getId();

        if ($orderCustomer->getReceiver()) {
            $data['receiver'] = $orderCustomer->getReceiver()->getExternalId();
        }
        if ($orderCustomer->getSender()) {
            $data['sender'] = $orderCustomer->getSender()->getExternalId();
        }
        if ($orderCustomer->getPayer()) {
            $data['payer'] = $orderCustomer->getPayer()->getExternalId();
        }

        return $data;
    }


}