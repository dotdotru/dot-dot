<?php

namespace App\Service\Integration;

use App\Service\Integration\Order\CalcCostRange;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use App\Entity\Base;

class CallBaseService
{
    const RESULT_SUCCESS = 'success';
    const DATETIME_FORMAT = 'Y-m-d';
    const DATETIME_FORMAT_ISO = 'Y-m-d\TH:i:s';

    /** @var Client */
    private $client;

    private $url = '';
    private $username = '';
    private $password = '';
    private $logger = null;


    private $token = '';

    const URL_AUTH = '/auth';


    public function __construct(Client $client, $url, LoggerInterface $logger = null, $username = null, $password = null)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
        $this->logger = $logger;
        $this->client = $client;
    }

    private function getClient()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
        ]);
        return $this->client;
    }

    private function auth()
    {
        try {
            $res = $this->getClient()->request('POST', self::URL_AUTH, [
                RequestOptions::JSON => ['username' => $this->username, 'password' => $this->password]
            ]);

            $json = json_decode($res->getBody(), true);

            if ($json['data']['token']) {
                $this->token = $json['data']['token'];
            }

        } catch (RequestException $e) {
            echo '<pre>';
            var_dump($e->getMessage());
            var_dump($e->getResponse()->getBody());
        }

        //return $result;
    }



    public function exec(AbstractRequest $request, $requestType = 'GET')
    {
        try {
            $this->auth();

            $headers = [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept'        => 'application/json',
            ];

            $res = $this->getClient()->request($request->getMethod(), $request->getUrl(), [
                RequestOptions::JSON => $request->getParams(),
                'headers' => $headers
            ]);

            if ($this->logger) {
				$this->logger->debug(sprintf('Post To %s', $request->getUrl()));
		        $this->logger->debug(sprintf('Request %s', json_encode($request->getParams())));
			}

            $data = json_decode($res->getBody(), true);

            $this->logger->debug(sprintf('Answer %s', json_encode($data)));

            if ($this->isErrorResponse($data)) {
                return null;
            }

            return $data;

        } catch (RequestException $e) {
            if ($this->logger) {
                $this->logger->error(sprintf('Post To %s', $request->getUrl()));
                $this->logger->error(sprintf('Request %s', json_encode($request->getParams())));
                $this->logger->error(sprintf('Answer %s', $e->getMessage()));
                $this->logger->error(sprintf('Answer %s', (string)$e->getResponse()->getBody()));
            }

            //echo '<pre>';
            //var_dump($e->getMessage());
            //var_dump($requestUrl);
            //var_dump((string)$e->getResponse()->getBody());
//
            //die();

            return null;
        }
    }

    private function isErrorResponse($data)
    {
        if (isset($data['error']) && !$data['error']) {
            return true;
        }

        return false;
    }

    public function authByStock(Base\Stock $stock)
    {
        $this->setUsername($stock->getLogin());
        $this->setPassword($stock->getPass());
        $this->auth();
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

}
