<?php

namespace App\Service\Integration\Update;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

class MileUpdate
{

    /** @var EntityManager */
    private $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Обновляем мили заказа, данными из базы
     *
     * @param $orderId
     * @param $miles
     * @throws ORMException
     */
    public function update ($orderId, $miles)
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy(['externalId' => $orderId]);
        /** @var WmsOrder $order */
        $wmsOrder = $this->em->getRepository(WmsOrder::class)->findOneBy(['externalId' => $order->getId()]);

        if ($order && $wmsOrder) {
            $this->updateOrder($order, $wmsOrder, $miles);
        }


    }


    /**
     * @param OrderInterface $order
     * @param $miles
     * @throws ORMException
     */
    protected function updateOrder (Order $order, WmsOrder $wmsOrder, $tmpMiles)
    {
        // На случай если база вернет несколько миль одного типа для одного заказа
        $miles = [];
        foreach ($tmpMiles as $mile) {
            $miles[$mile['type']] = $mile;
        }


        foreach ($miles as $mile) {
            $mileShipping = null;

            // Ищем милю в заказе
            $mileShipping = $this->em->getRepository(Shipping::class)->findOneBy(['externalId' => $mile['id'], 'reject' => false]);

            // Если миля не найдена в заказе, добавим
            if (!$mileShipping) {
                $mileShipping = new Shipping();

                // Отменим все мили данного типа в заказе
                foreach ($order->getShippings() as $shipping) {
                    if ($shipping->getType() == $mile['type']) {
                        $shipping->setReject(true);
                    }
                }
                // Отменим все мили данного типа в заказе
                foreach ($wmsOrder->getShippings() as $shipping) {
                    if ($shipping->getType() == $mile['type']) {
                        $shipping->setReject(true);
                    }
                }

                $this->em->persist($mileShipping);
                $order->addShipping($mileShipping);
                $wmsOrder->addShipping($mileShipping);

                $mileShipping->setExternalId($mile['id']);
                $mileShipping->setDistance($mile['distance']);

                if ($mile['type'] == Shipping::TYPE_DELIVER) {
                    $mileShipping->setStock($order->getStockTo());
                } else {
                    $mileShipping->setStock($order->getStockFrom());
                }
            } else {
                if (!$order->getShippings()->contains($mileShipping)) {
                    $order->addShipping($mileShipping);
                }
                if (!$wmsOrder->getShippings()->contains($mileShipping)) {
                    // Отменим все мили данного типа в заказе
                    foreach ($wmsOrder->getShippings() as $shipping) {
                        if ($shipping->getType() == $mile['type']) {
                            $shipping->setReject(true);
                        }
                    }

                    $wmsOrder->addShipping($mileShipping);
                }
            }

//foreach ($order->getShippings() as $shipping) {
//if ($shipping->isReject()) $this->em->remove($shipping);
//dump('remove shipping');
//}

            try {
                $time = $mile['time'] ? new \DateTime($mile['time']) : null;
            } Catch (\Exception $e) {
                $time = null;
            }

            $mileShipping->setActive(true);
            $mileShipping->setType($mile['type']);
            $mileShipping->setDistance($mile['distance']);
            $mileShipping->setPickAt($time);
            $mileShipping->setAddress($mile['address']);
            $mileShipping->setCountLoader($mile['num_loaders']);
            $mileShipping->setHydroBoard($mile['tail_lift']);
            if (!empty($mile['cost']['total'])) {
                $mileShipping->setPrice($mile['cost']['total']['total']);

                foreach ($mile['cost'] as $mileCostName => $mileCost) {
                    $this->updateOrderPriceDetail($order, $mile['type'] . '_' . $mileCostName, $mileCost['total'], $mileCost['vat']);
                }
            }
        }

        // @todo доделать удаление мили в вмс при отказе юзера

    }



    protected function updateOrderPriceDetail(Order $order, $type, $total, $vat)
    {
        $currentPriceDetail = null;

        /** @var Order\PriceDetail $priceDetail */
        foreach ($order->getPriceDetails() as $priceDetail) {
            if ($priceDetail->getType() == $type) {
                $currentPriceDetail = $priceDetail;
            }
        }

        if (!$currentPriceDetail) {
            $currentPriceDetail = new Order\PriceDetail();
            $currentPriceDetail->setOrder($order);
        }

        $currentPriceDetail->setType($type);
        $currentPriceDetail->setPrice($total);
        $currentPriceDetail->setPriceVat($vat);

        $this->em->persist($currentPriceDetail);
    }

}
