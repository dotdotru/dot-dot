<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;
use phpDocumentor\Reflection\Types\Integer;

class SaveMilePrice extends AbstractRequest
{

    protected $url = "/site/save/mile-price";

    protected $method = 'POST';


    protected $id = null;

    protected $cityId = 0;

    protected $tarifId = 0;

    protected $distance = '';

    protected $weight = '';

    protected $price = 0;

    protected $perKm = false;



    public function __construct($tariff = [])
    {

    }


    public function getParams ()
    {
        $params = [];

        if ($this->id) {
            $params['id'] = (int)$this->id;
        }
        $params['city'] = $this->cityId;
        $params['tariff'] = $this->tarifId;
        $params['distance'] = $this->distance;
        $params['weight'] = $this->weight;
        $params['price'] = $this->price;
        $params['per_km'] = (int)$this->perKm;

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return SaveMilePrice
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     * @return SaveMilePrice
     */
    public function setCityId(int $cityId): SaveMilePrice
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTarifId(): int
    {
        return $this->tarifId;
    }

    /**
     * @param int $tarifId
     * @return SaveMilePrice
     */
    public function setTarifId(int $tarifId): SaveMilePrice
    {
        $this->tarifId = $tarifId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDistance(): string
    {
        return $this->distance;
    }

    /**
     * @param string $distance
     * @return SaveMilePrice
     */
    public function setDistance(string $distance): SaveMilePrice
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * @return string
     */
    public function getWeight(): string
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     * @return SaveMilePrice
     */
    public function setWeight(string $weight): SaveMilePrice
    {
        $this->weight = $weight;
        return $this;
    }



    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return SaveMilePrice
     */
    public function setPrice(int $price): SaveMilePrice
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerKm(): int
    {
        return $this->perKm;
    }

    /**
     * @param int $perKm
     * @return SaveMilePrice
     */
    public function setPerKm(int $perKm): SaveMilePrice
    {
        $this->perKm = $perKm;
        return $this;
    }


}