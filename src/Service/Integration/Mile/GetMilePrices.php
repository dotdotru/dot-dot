<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class GetMilePrices extends AbstractRequest
{

    protected $url = "/site/get/mile-prices/:city/:tarif";

    protected $cityId;

    protected $tariff;


    public function getParams ()
    {
        return [
            'city' => $this->cityId,
            'tariff' => $this->tariff
        ];
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param mixed $cityId
     * @return GetMilePrices
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * @param mixed $tariff
     * @return GetMilePrices
     */
    public function setTariff($tariff)
    {
        $this->tariff = $tariff;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return str_replace([':city', ':tarif'], [$this->cityId, $this->tariff], $this->url);
    }


}