<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class MileBatch extends AbstractRequest
{

    protected $url = "/site/info/mile-batch/:batchId";

    /**
     * @var null
     */
    protected $batchId = null;


    public function getParams ()
    {
        return [

        ];
    }

    /**
     * @return null
     */
    public function getBatchId()
    {
        return $this->batchId;
    }

    /**
     * @param null $batchId
     * @return GetMileTariffs
     */
    public function setBatchId($batchId)
    {
        $this->batchId = $batchId;
        return $this;
    }


    /**
     * @return string
     */
    public function getUrl(): string
    {
        return str_replace(':batchId', $this->batchId, $this->url);
    }

}