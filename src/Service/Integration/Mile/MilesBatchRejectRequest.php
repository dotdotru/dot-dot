<?php

namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class MilesBatchRejectRequest extends AbstractRequest
{

    protected $url = "/site/post/mile-batch-cancel";


    protected $method = 'POST';

    /**
     * @var Int|null
     */
    protected $batchId = null;



    public function getParams()
    {
        return [
            'id' => $this->batchId,
        ];
    }

    /**
     * @return Int|null
     */
    public function getBatchId(): ?Int
    {
        return $this->batchId;
    }

    /**
     * @param Int|null $batchId
     * @return MilesBatchRejectRequest
     */
    public function setBatchId(?Int $batchId): MilesBatchRejectRequest
    {
        $this->batchId = $batchId;
        return $this;
    }


}
