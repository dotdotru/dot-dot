<?php

namespace App\Service\Integration\Mile;


use App\Entity\Shipping;
use App\Service\Integration\AbstractRequest;

class BatchesMilesRequest extends AbstractRequest
{

    protected $url = "/site/get/mile-batches/:offset/:limit";

    /**
     * @var Int|null
     */
    protected $offset = 0;

    /**
     * @var Int|null
     */
    protected $limit = 20;


    public function getParams()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return str_replace([':offset', ':limit'], [$this->offset, $this->limit], $this->url);
    }


}
