<?php


namespace App\Service\Integration\Mile;



use App\Service\Integration\AbstractRequest;

class MileBatchArrived extends AbstractRequest
{

    protected $url = "/wh/post/mile-batch-arrived";


    protected $method = 'POST';

    /**
     * @var Int
     */
    protected $batchId = null;

    /**
     * @var Int
     */
    protected $stockId = null;


    /**
     * @var \DateTime
     */
    protected $time = null;



    public function __construct()
    {
        $this->time = new \DateTime();
    }


    public function getParams ()
    {
        return [
            'id' => $this->batchId,
            'time' => $this->time->format('c'),
            'warehouse' => (string)$this->stockId
        ];
    }

    /**
     * @return Int
     */
    public function getBatchId(): Int
    {
        return $this->batchId;
    }

    /**
     * @param Int $batchId
     * @return MileBatchArrived
     */
    public function setBatchId(Int $batchId): MileBatchArrived
    {
        $this->batchId = $batchId;
        return $this;
    }

    /**
     * @return Int
     */
    public function getStockId(): Int
    {
        return $this->stockId;
    }

    /**
     * @param Int $stockId
     * @return MileBatchArrived
     */
    public function setStockId(Int $stockId): MileBatchArrived
    {
        $this->stockId = $stockId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     * @return MileBatchArrived
     */
    public function setTime(\DateTime $time): MileBatchArrived
    {
        $this->time = $time;
        return $this;
    }



}