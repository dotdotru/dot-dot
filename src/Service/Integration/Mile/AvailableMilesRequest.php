<?php

namespace App\Service\Integration\Mile;


use App\Entity\Shipping;
use App\Service\Integration\AbstractRequest;

class AvailableMilesRequest extends AbstractRequest
{

    protected $url = "/site/get/available_miles/:city";

    /**
     * @var Int|null
     */
    protected $cityId = null;

    /**
     * @var \DateTime|null
     */
    protected $date = null;

    /**
     * @var null|string
     */
    protected $sortField = null;

    /**
     * @var null|string
     */
    protected $sortDirection = 'DESC';


    public function getParams()
    {
        return [];
    }

    /**
     * @return Int|null
     */
    public function getCityId(): ?Int
    {
        return $this->cityId;
    }

    /**
     * @param Int|null $cityId
     * @return AvailableMilesRequest
     */
    public function setCityId(?Int $cityId): AvailableMilesRequest
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * @return String
     */
    public function getSortField(): ?String
    {
        return $this->sortField;
    }

    /**
     * @param String $sortField
     * @return AvailableMilesRequest
     */
    public function setSortField(?String $sortField): AvailableMilesRequest
    {
        $this->sortField = $sortField;
        return $this;
    }

    /**
     * @return String
     */
    public function getSortDirection(): ?String
    {
        return $this->sortDirection;
    }

    /**
     * @param String $sortDirection
     * @return AvailableMilesRequest
     */
    public function setSortDirection(?String $sortDirection): AvailableMilesRequest
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return AvailableMilesRequest
     */
    public function setDate(?\DateTime $date): AvailableMilesRequest
    {
        $this->date = $date;
        return $this;
    }


    public function getResponse()
    {
        $miles = [];

        foreach ($this->response as $item) {
            //if (!rand(0,2)) continue;
            $mile = $item['mile'];
            $order = $item['order'];

            // Показываем только мили с датой, если выбрана дата, только за эту дату
            $mileTime = new \DateTime($mile['time']);
            if (!$mileTime || ($this->date && $mileTime->format('Y-m-d') != $this->date->format('Y-m-d'))) {
                continue;
            }

            if (empty($mile['coordinates']) || empty($mile['distance'])) {
                continue;
            }

            $coordinates = trim($mile['coordinates'], '()');
            $coordinates = explode(',', $coordinates);

            $packages = [];
            $volume = 0;
            $weight = 0;

            $basePackages = empty($order['cargos']) ? $order['order_lines'] : $order['cargos'];

            foreach ($basePackages as $basePackage) {
                $count = empty($basePackage['num_items']) ? 1 : $basePackage['num_items'];
                $packages[] = [
                    'title' => empty($basePackage['length']) ? $basePackage['volume'] . 'м3' : $basePackage['length'] . 'x' . $basePackage['width'] . 'x' . $basePackage['height'] . 'см',
                    'count' => !empty($basePackage['num_items']) ? $basePackage['num_items'] : 1
                ];
                $volume += (!empty($basePackage['volume']) ? $basePackage['volume'] : ($basePackage['length'] * $basePackage['width'] * $basePackage['height']) / 1000000) * $count;
                $weight += ($basePackage['weight']) * $count;
            }

            $miles[] = [
                'id' => $mile['id'],
                'name' => $order['ext_id'] . '-' . ($mile['type'] == Shipping::TYPE_PICKUP ? '1' : 'L'),
                'pickupAt' => $mile['time'],
                'type' => $mile['type'],
                'weigth' => $weight,
                'volume' => round($volume, 3),
                'packages' => $packages,
                'stockId' => $mile['type'] == Shipping::TYPE_PICKUP ? $order['warehouse_from'] : $order['warehouse_to'],
                'address' => $mile['address'],
                'latitude' => $coordinates[0],
                'lotitude' => $coordinates[1],
                'price' => $item['cost']['total'],
                "numLoaders" => $mile['num_loaders'],
                "hydroBoard" => $mile['tail_lift'],
            ];
        }

        return $miles;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return str_replace(':city', $this->cityId, $this->url);
    }

}
