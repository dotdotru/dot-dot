<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class DeleteMilePrice  extends AbstractRequest
{

    protected $url = "/site/action/del-mile-price";

    protected $method = 'POST';

    protected $id = null;


    public function getParams ()
    {
        $params = [];

        $params['id'] = (int)$this->id;

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return DeleteMilePrice
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

}