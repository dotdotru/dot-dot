<?php

namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class MilesBatchConfirmRequest extends AbstractRequest
{

    protected $url = "/site/post/mile-batch-confirm";


    protected $method = 'POST';

    /**
     * @var Int|null
     */
    protected $reserveId = null;

    /**
     * @var int
     */
    protected $driver;


    public function getParams()
    {
        return [
            'id' => $this->reserveId,
            'driver' => $this->driver
        ];
    }

    /**
     * @return Int|null
     */
    public function getReserveId(): ?Int
    {
        return $this->reserveId;
    }

    /**
     * @param Int|null $reserveId
     * @return MilesBatchConfirmRequest
     */
    public function setReserveId(?Int $reserveId): MilesBatchConfirmRequest
    {
        $this->reserveId = $reserveId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDriver(): int
    {
        return $this->driver;
    }

    /**
     * @param int $driver
     * @return MilesBatchConfirmRequest
     */
    public function setDriver(int $driver): MilesBatchConfirmRequest
    {
        $this->driver = $driver;
        return $this;
    }

}
