<?php


namespace App\Service\Integration\Mile;


use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\OrderPackage;
use App\Entity\Shipping;
use App\Entity\Wms\WmsOrder;
use App\Service\Integration\AbstractRequest;

class CalcMileCost extends AbstractRequest
{

    protected $url = "/site/post/calc-mile-cost";


    protected $method = 'POST';

    /**
     * @var OrderInterface
     */
    protected $order = null;

    /**
     * @var Shipping
     */
    protected $shipping = null;


    /**
     * @var null
     */
    protected $vat = null;


    public function getParams ()
    {
        return [
            'order' => $this->mapEntityCustomerToBase($this->order),
            'mile' => $this->mapMileToBase($this->shipping),
            'vat' => $this->vat
        ];
    }

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    /**
     * @param OrderInterface $order
     * @return CalcMileCost
     */
    public function setOrder(OrderInterface $order): CalcMileCost
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return Shipping
     */
    public function getShipping(): ?Shipping
    {
        return $this->shipping;
    }

    /**
     * @param Shipping $shipping
     * @return CalcMileCost
     */
    public function setShipping(Shipping $shipping): CalcMileCost
    {
        $this->shipping = $shipping;
        return $this;
    }

    /**
     * @return null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param null $vat
     * @return CalcMileCost
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }



    /**
     * @todo вынести в общий класс
     *
     * @param OrderInterface $orderCustomer
     * @return array
     */
    public function mapEntityCustomerToBase(OrderInterface $orderCustomer)
    {

        $data = [
            'id' => $orderCustomer->getExternalId() ? $orderCustomer->getExternalId() : null,
            'route' => $orderCustomer->getDirection()->getExternalId(),
            'declared_value' => $orderCustomer->getDeclaredPrice(),
        ];

        if ($orderCustomer->getUser()) {
            $data['customer'] = $orderCustomer->getUser()->getExternalId();
        }

        $data['order_lines'] = [];
        if ($orderCustomer instanceof Order &&  $orderCustomer && $orderCustomer->getOrderPackages()) {

            if (count($orderCustomer->getPackages())) {
                /** @var OrderPackage $orderPackage */
                foreach ($orderCustomer->getPackages() as $orderPackage) {
                    $orderPackageData = [
                        'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                        'num_items' => $orderPackage->getCount(),
                        'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                        'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                        'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                        'weight' => (float)$orderPackage->getWeight(),
                        'volume' => (float)$orderPackage->getCalculateWeight(),
                        'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                        'pack_cost' => $orderPackage->getPackingPrice(),
                    ];
                    $data['order_lines'][] = $orderPackageData;
                }
            } else {
                /** @var OrderPackage $orderPackage */
                foreach ($orderCustomer->getOrderPackages() as $orderPackage) {
                    $orderPackageData = [
                        'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                        'num_items' => $orderPackage->getCount(),
                        'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                        'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                        'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                        'weight' => (float)$orderPackage->getWeight(),
                        'volume' => (float)$orderPackage->getCalculateWeight(),
                        'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                        'pack_cost' => $orderPackage->getPackingPrice(),
                    ];
                    if ($orderCustomer->isByCargo()) {
                        $orderPackageData['totals'] = true;
                    }
                    $data['order_lines'][] = $orderPackageData;
                }
            }
        }
        if ($orderCustomer instanceof WmsOrder &&  $orderCustomer && $orderCustomer->getOrderPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($orderCustomer->getPackages() as $orderPackage) {
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'volume' => (float)$orderPackage->getCalculateWeight(),
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $orderPackage->getPackingPrice(),
                ];
                $data['order_lines'][] = $orderPackageData;
            }
        }



        //$data['customer_delivery_time'] = $orderCustomer->getCustomerDeliveryTime() ? $orderCustomer->getCustomerDeliveryTime() : null;
        $data['cost_min'] = $orderCustomer->getMinPrice();
        $data['cost_max'] = $orderCustomer->getMaxPrice();
        $data['insurance'] = $orderCustomer->isInsuredSuccess() ? $orderCustomer->getInsuredNumber() : '';
        $data['price'] = (float)$orderCustomer->getPrice();

        $data['warehouse_from'] = null;
        if ($orderCustomer->getStockFrom()) {
            $data['warehouse_from'] = $orderCustomer->getStockFrom()->getExternalId();
        }

        //if ($shipping = $orderCustomer->getShipping(Shipping::TYPE_PICKUP)) {
//            $data['warehouse_from'] = $shipping->getStock()->getExternalId();
//        }

        $data['warehouse_to'] = null;
        if ($orderCustomer->getStockTo()) {
            $data['warehouse_to'] = $orderCustomer->getStockTo()->getExternalId();
        }

  //      if ($shipping = $orderCustomer->getShipping(Shipping::TYPE_DELIVER)) {
    //        $data['warehouse_to'] = $shipping->getStock()->getExternalId();
      //  }

        $data['warehouse_from_pass_time'] = null;
        if ($orderCustomer->getReceptionAt()) {
            $data['warehouse_from_pass_time'] = $orderCustomer->getReceptionAt()->getTimestamp();
        }

        $data['paid'] = $orderCustomer->isPayed();


        $data['ext_id'] = $orderCustomer->getId();

        if ($orderCustomer->getReceiver()) {
            $data['receiver'] = $orderCustomer->getReceiver()->getExternalId();
        }
        if ($orderCustomer->getSender()) {
            $data['sender'] = $orderCustomer->getSender()->getExternalId();
        }
        if ($orderCustomer->getPayer()) {
            $data['payer'] = $orderCustomer->getPayer()->getExternalId();
        }

        return $data;
    }

    /**
     * @todo вынести в общий класс
     *
     * @param Shipping $shipping
     * @return array
     */
    public function mapMileToBase(Shipping $shipping)
    {
        $data = [
            'ext_id' => $shipping->getId(),
            'type' => $shipping->getType(),
            'distance' => round($shipping->getDistance()),
            'time' => $shipping->getPickAt() ? $shipping->getPickAt()->format('c') : null,
            'address' => $shipping->getAddress(),
            'num_loaders' => $shipping->getCountLoader(),
            'tail_lift' => $shipping->getHydroBoard(),
            'refrigerator' => false
        ];

        return $data;
    }




}
