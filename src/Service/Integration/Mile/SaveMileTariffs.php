<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class SaveMileTariffs extends AbstractRequest
{

    protected $url = "/site/save/mile-tariff";

    protected $method = 'POST';


    protected $id = null;

    protected $loaders = 0;

    protected $tailLift = 0;

    protected $refrigerator = false;


    public function __construct($tariff = [])
    {
        if (!empty($tariff['id'])) {
            $this->setId($tariff['id']);
        }
        if (!empty($tariff['loaders'])) {
            $this->setLoaders($tariff['loaders']);
        }
        if (!empty($tariff['tail_lift'])) {
            $this->setTailLift($tariff['tail_lift']);
        }
        if (!empty($tariff['refrigerator'])) {
            $this->setRefrigerator($tariff['refrigerator']);
        }

    }


    public function getParams ()
    {
        $params = [];

        if ($this->id) {
            $params['id'] = (int)$this->id;
        }
        $params['loaders'] = $this->loaders;
        $params['tail_lift'] = (bool)$this->tailLift;
        $params['refrigerator'] = $this->refrigerator;

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return SaveMileTariffs
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoaders(): int
    {
        return $this->loaders;
    }

    /**
     * @param int $loaders
     * @return SaveMileTariffs
     */
    public function setLoaders(int $loaders): SaveMileTariffs
    {
        $this->loaders = $loaders;
        return $this;
    }

    /**
     * @return int
     */
    public function getTailLift(): bool
    {
        return $this->tailLift;
    }

    /**
     * @param int $tailLift
     * @return SaveMileTariffs
     */
    public function setTailLift(bool $tailLift): SaveMileTariffs
    {
        $this->tailLift = $tailLift;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRefrigerator(): bool
    {
        return $this->refrigerator;
    }

    /**
     * @param bool $refrigerator
     * @return SaveMileTariffs
     */
    public function setRefrigerator(bool $refrigerator): SaveMileTariffs
    {
        $this->refrigerator = $refrigerator;
        return $this;
    }

}