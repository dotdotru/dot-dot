<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class GetMileTariffs extends AbstractRequest
{

    protected $url = "/site/get/mile-tariffs";


    public function getParams ()
    {
        return [];
    }


}