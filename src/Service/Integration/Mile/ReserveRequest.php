<?php

namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class ReserveRequest extends AbstractRequest
{

    protected $url = "/site/post/mile-batch-reserve";


    protected $method = 'POST';

    /**
     * @var Int|null
     */
    protected $userId = null;

    /**
     * @var []
     */
    protected $miles = [];

    /**
     * @var int
     */
    protected $auctionCoef = 60;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $stockId;


    public function getParams()
    {
        return [
            'warehouse' => $this->stockId,
            'carrier' => $this->userId,
            'miles' => $this->miles,
            'auction_coef' => $this->auctionCoef / 100,
            'type' => $this->type
        ];
    }


    /**
     * @return Int|null
     */
    public function getUserId(): ?Int
    {
        return $this->userId;
    }

    /**
     * @param Int|null $userId
     * @return ReserveRequest
     */
    public function setUserId(?Int $userId): ReserveRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiles()
    {
        return $this->miles;
    }

    /**
     * @param mixed $miles
     * @return ReserveRequest
     */
    public function setMiles($miles)
    {
        $this->miles = $miles;
        return $this;
    }

    /**
     * @return int
     */
    public function getAuctionCoef(): ?int
    {
        return $this->auctionCoef;
    }

    /**
     * @param int $auctionCoef
     * @return ReserveRequest
     */
    public function setAuctionCoef(?int $auctionCoef): ReserveRequest
    {
        $this->auctionCoef = $auctionCoef;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ReserveRequest
     */
    public function setType(string $type): ReserveRequest
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getStockId(): int
    {
        return $this->stockId;
    }

    /**
     * @param int $stockId
     * @return ReserveRequest
     */
    public function setStockId(int $stockId): ReserveRequest
    {
        $this->stockId = $stockId;
        return $this;
    }



}
