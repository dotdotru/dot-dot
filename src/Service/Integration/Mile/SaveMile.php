<?php


namespace App\Service\Integration\Mile;


use App\Service\Integration\AbstractRequest;

class SaveMile extends AbstractRequest
{

    protected $url = "/site/save/mile";

    protected $method = 'POST';


    protected $id = null;

    /**
     * @var string|null
     */
    protected $extId = null;

    protected $orderId = 0;

    /**
     * @var string
     */
    protected $type = null;

    protected $distance = 0;

    /**
     * @var \DateTime|null
     */
    protected $time = null;

    /**
     * @var string
     */
    protected $address = '';

    protected $numLoaders = 0;

    protected $tailLift = 0;

    protected $refrigerator = 0;

    protected $cost = 0;

    /**
     * @var float
     */
    protected $lat = 0;

    /**
     * @var float
     */
    protected $lng = 0;


    public function __construct()
    {

    }


    public function getParams ()
    {
        $params = [];

        if ($this->id) {
            $params['id'] = (int)$this->id;
        }
        $params['ext_id'] = $this->extId;
        $params['order'] = $this->orderId;
        $params['type'] = $this->type;
        $params['distance'] = $this->distance ? $this->distance : null;
        $params['time'] = $this->time ? $this->time->format('c') : null;
        $params['address'] = $this->address;
        $params['num_loaders'] = (int)$this->numLoaders;
        $params['tail_lift'] = (int)$this->tailLift;
        $params['refrigerator'] = (int)$this->refrigerator;
        $params['cost'] = null;
        $params['coordinates'] = '(' . $this->lat . ',' . $this->lng . ')';

        return $params;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return SaveMile
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtId(): ?string
    {
        return $this->extId;
    }

    /**
     * @param string $extId
     * @return SaveMile
     */
    public function setExtId(?string $extId): SaveMile
    {
        $this->extId = $extId;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return SaveMile
     */
    public function setOrderId(int $orderId): SaveMile
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SaveMile
     */
    public function setType(string $type): SaveMile
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }

    /**
     * @param int|null $distance
     * @return SaveMile
     */
    public function setDistance(?int $distance): SaveMile
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getTime(): ?\DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime|null $time
     * @return SaveMile
     */
    public function setTime(?\DateTime $time): SaveMile
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return SaveMile
     */
    public function setAddress(string $address): SaveMile
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumLoaders(): int
    {
        return $this->numLoaders;
    }

    /**
     * @param int $numLoaders
     * @return SaveMile
     */
    public function setNumLoaders(int $numLoaders): SaveMile
    {
        $this->numLoaders = $numLoaders;
        return $this;
    }

    /**
     * @return int
     */
    public function getTailLift(): int
    {
        return $this->tailLift;
    }

    /**
     * @param int $tailLift
     * @return SaveMile
     */
    public function setTailLift(int $tailLift): SaveMile
    {
        $this->tailLift = $tailLift;
        return $this;
    }

    /**
     * @return int
     */
    public function getRefrigerator(): int
    {
        return $this->refrigerator;
    }

    /**
     * @param int $refrigerator
     * @return SaveMile
     */
    public function setRefrigerator(int $refrigerator): SaveMile
    {
        $this->refrigerator = $refrigerator;
        return $this;
    }

    /**
     * @return int
     */
    public function getCost(): ?int
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     * @return SaveMile
     */
    public function setCost(?int $cost): SaveMile
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     * @return SaveMile
     */
    public function setLat(?float $lat): SaveMile
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     * @return SaveMile
     */
    public function setLng(?float $lng): SaveMile
    {
        $this->lng = $lng;
        return $this;
    }


    protected function formatNumrange($from, $to) {
        return '(' . ($from ? $from : '') . ',' . ($to ? $to : '') . ']';
    }


}