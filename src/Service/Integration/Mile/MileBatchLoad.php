<?php


namespace App\Service\Integration\Mile;



use App\Service\Integration\AbstractRequest;

class MileBatchLoad extends AbstractRequest
{

    protected $url = "/wh/post/mile-batch-shipped";


    protected $method = 'POST';

    /**
     * @var Int
     */
    protected $batchId = null;

    /**
     * @var Int
     */
    protected $stockId = null;


    /**
     * @var \DateTime
     */
    protected $time = null;



    public function __construct()
    {
        $this->time = new \DateTime();
    }


    public function getParams ()
    {
        return [
            'id' => $this->batchId,
            'time' => $this->time->format('c'),
            'warehouse' => (string)$this->stockId
        ];
    }

    /**
     * @return Int
     */
    public function getBatchId(): Int
    {
        return $this->batchId;
    }

    /**
     * @param Int $batchId
     * @return MileBatchLoad
     */
    public function setBatchId(Int $batchId): MileBatchLoad
    {
        $this->batchId = $batchId;
        return $this;
    }

    /**
     * @return Int
     */
    public function getStockId(): Int
    {
        return $this->stockId;
    }

    /**
     * @param Int $stockId
     * @return MileBatchLoad
     */
    public function setStockId(Int $stockId): MileBatchLoad
    {
        $this->stockId = $stockId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     * @return MileBatchLoad
     */
    public function setTime(\DateTime $time): MileBatchLoad
    {
        $this->time = $time;
        return $this;
    }

}