<?php
namespace App\Service\Mapper\Wms;

use App\Entity\Direction;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Receiver;
use App\Entity\StringEntity;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchPallet;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsOrderPackage;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Repository\Organization\OrganizationDriverRepository;
use App\Repository\Wms\WmsPalletRepository;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Stock;

class WmsBatchMapperService
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function mapEntityBatchToBase(Order $order)
    {
        $data = [
            'carrier' => $order->getCarrier() ? $order->getCarrier()->getExternalId() : null,
            'batch' => $order->getExternalId() ? $order->getExternalId() : null,
            'driver' => null,
        ];

        foreach ($order->getDrivers() as $driver) {
            $data['driver'] = $driver->getId();
        }

        $data['ext_id'] = $order->getId();

        return $data;
    }

    public function mapBatchToWmsBatch(WmsBatch $batch, $data)
    {
        $batch->setExternalId($data['ext_id']);

        $directionRepository = $this->entityManager->getRepository(Direction::class);
        $userRepository = $this->entityManager->getRepository(User::class);

        if ($data['carrier']) {
            $carrier = $userRepository->findOneBy(['externalId' => $data['carrier']['id']]);
            $batch->setCarrier($carrier);
        }

        $stockRepository = $this->entityManager->getRepository(Stock::class);

        $stockFrom = $stockRepository->findOneByExternalId($data['warehouse_from']);
        $batch->setStockFrom($stockFrom);

        $stockTo = $stockRepository->findOneByExternalId($data['warehouse_to']);
        $batch->setStockTo($stockTo);

        $direction = $directionRepository->findOneBy(['cityFrom' => $stockFrom->getCity(), 'cityTo' => $stockTo->getCity()]);
        $batch->setDirection($direction);

        $organizationRepository = $this->entityManager->getRepository(Organization::class);
        $organization = $organizationRepository->getCarrierByUser($batch->getCarrier());
        $batch->setOrganization($organization);

        /** @var OrganizationDriverRepository $organizationDriverRepository */
        $organizationDriverRepository = $this->entityManager->getRepository(OrganizationDriver::class);

		$batch->setDrivers(new ArrayCollection());

        $driver = $organizationDriverRepository->find($data['driver']['id']);
        $batch->addDriver($driver);
        $batch->setDriver($driver);


        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $this->entityManager->getRepository(WmsPallet::class);

        $pallets = $data['pallets'];

		$batch->setPallets(new ArrayCollection());
        if (isset($pallets) && !empty($pallets)) {
            foreach ($pallets as $palletId) {
                /** @var WmsPallet $pallet */
                $wmsPallet = $wmsPalletRepository->findOneByExternalId($palletId);
                $wmsPallet->setBatch($batch);
                $batch->addPalletItem($wmsPallet);
            }
        }

        $totalWeight = 0;
        $batch->setWeight(0);
        foreach ($batch->getPallets() as $wmsPallet) {
            $totalWeight += $wmsPallet->getWeight();
        }
        $batch->setWeight($totalWeight);

        if (!$batch->getStatus()) {
            $batch->setStatus(WmsBatch::STATUS_RESERVED);
        }

        if (isset($data['cost']) && !empty($data['cost'])) {
            $batch->setPrice($data['cost']['total']);
            $batch->setPriceVat($data['cost']['vat']);
        }

        return $batch;
    }

    public function batchLoad(WmsBatch $wmsBatch)
    {
        $data = [];
        $data['ext_id'] = $wmsBatch->getExternalId();
        $data['time'] = $wmsBatch->getReceptionAt()->format(IntegrationBaseService::DATETIME_FORMAT_ISO);

        foreach ($wmsBatch->getPallets() as $wmsPallet) {
            $data['pallets'][] = $wmsPallet->getExternalId();
        }

        return $data;
    }

    public function batchRejected(WmsBatch $wmsBatch)
    {
        $dateTime = new \DateTime();
        $data = [];
        $data['ext_id'] = $wmsBatch->getExternalId();
        $data['time'] = $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO);

        return $data;
    }

    public function batchArrived(WmsBatch $wmsBatch)
    {
        if (!$wmsBatch->getIssueAt()) {
            $wmsBatch->setIssueAt(new \DateTime());
        }
        $data = [];
        $data['ext_id'] = $wmsBatch->getExternalId();
        $data['time'] = $wmsBatch->getIssueAt()->format(IntegrationBaseService::DATETIME_FORMAT_ISO);
        $data['pallets'] = [];

        /** @var WmsPallet $wmsPallet */
        foreach ($wmsBatch->getPallets() as $wmsPallet) {
            $palletData = [
                'ext_id' => (string)$wmsPallet->getExternalId(),
                'weight_arrived' => $wmsPallet->getArriveWeight(),
                'damaged' => $wmsPallet->isDamaged(),
            ];
            $data['pallets'][] = $palletData;
        }

        return $data;
    }

    public function mapBatchToBasePaid(Order $order)
    {
        $data['batch'] = $order->getExternalId();
        $data['paid'] = $order->isPayed();

        return $data;
    }

    public function mapOrderToBaseConfirm(Order $order)
    {
        $data = [
            'id' => $order->getExternalId(),
            'load_time' => $order->getSendDate()->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
        ];

        return $data;
    }

}
