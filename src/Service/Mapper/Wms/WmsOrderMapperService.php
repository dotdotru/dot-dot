<?php

namespace App\Service\Mapper\Wms;

use App\Entity\Direction;
use App\Entity\OrderPackage;
use App\Entity\Organization\Organization;
use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsOrderPackage;
use App\Entity\Wms\WmsPackage;
use App\Repository\Organization\OrganizationRepository;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Stock;

class WmsOrderMapperService
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    private function mapBaseToWmsOrderPackage(WmsOrderPackage $package = null, $cargo)
    {
        $packageTypeRepository = $this->entityManager->getRepository(PackageType::class);
        $packingRepository = $this->entityManager->getRepository(Packing::class);

        if (!$package) {
            $package = new WmsOrderPackage();
        }

        //$package->setExternalId($cargo['id'].$cargo['ext_id']);

        $package->setWeight($cargo['weight']);
        $package->setWidth($cargo['width']);
        $package->setHeight($cargo['height']);
        $package->setDepth($cargo['length']);
        $package->setCalculateWeight($cargo['volume']);
        $package->setCount(isset($cargo['num_items']) && $cargo['num_items'] ? $cargo['num_items'] : 1);

        if ($cargo['type']) {
            $packageType = $packageTypeRepository->findOneBy(['title' => $cargo['type']]);

            if (!$packageType) {
				$package->setPackageTypeAnother($cargo['type']);
			} else {
				$package->setPackageType($packageType);
			}
        }

        if ($cargo['pack']) {
            $packing = $packingRepository->findOneBy(['title' => $cargo['pack']]);
            $package->setPacking($packing);
            $package->setPackingPrice($cargo['pack_cost']);
        }

        $package->setPackingPrice($cargo['pack_cost']);

        return $package;
    }

    private function mapBaseToWmsPackage(WmsPackage $package = null, $cargo)
    {
        //$this->entityManager->getRepository(Package::)

        if (!$package) {
            $package = new WmsPackage();
        }

        $package->setExternalId($cargo['id']);
        $package->setWeight($cargo['weight']);
        $package->setWidth($cargo['width']);
        $package->setHeight($cargo['height']);
        $package->setDepth($cargo['length']);
        $package->setCalculateWeight($cargo['volume']);
        $package->setCount(isset($cargo['count']) && $cargo['count'] ? $cargo['count'] : 1);
        $package->setPackageTypeAnother($cargo['type']);

        if ($cargo['status']) {
			$package->setStatus($cargo['status']);
		}

        return $package;
    }

    public function mapOrderToWmsOrder(WmsOrder $order, $data)
    {
        $packages = $order->getOrderPackages();

        $order->setOrderPackages(new ArrayCollection());
        if ($data['order_lines']) {
            foreach ($data['order_lines'] as $cargokey => $cargo) {
                $package = $this->mapBaseToWmsOrderPackage(null, $cargo);
                $this->entityManager->persist($package);
                $order->addOrderPackage($package);
                unset($data['order_lines'][$cargokey]);
            }
        }

        $order->setExternalId($data['ext_id']);

        $customerRepository = $this->entityManager->getRepository(User::class);
        $customer = $customerRepository->findOneByExternalId($data['customer']['id']);

        if (!$customer) {
            return null;
        }

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $this->entityManager->getRepository(Organization::class);
        $organization = $organizationRepository->getCustomerByUser($customer);

        if ($customer) {
            $order->setUser($customer);
        }

        $personRepository = $this->entityManager->getRepository(Person::class);

        $sender = $personRepository->findOneByExternalId($data['sender']['id']);
        $order->setSender($sender);

        $receiver = $personRepository->findOneByExternalId($data['receiver']['id']);
        $order->setReceiver($receiver);

        $payer = $personRepository->findOneByExternalId($data['payer']['id']);
        $order->setPayer($payer);

        $stockRepository = $this->entityManager->getRepository(Stock::class);

        if ($data['warehouse_from']) {
            $stockFrom = $stockRepository->findOneByExternalId($data['warehouse_from']);
            $order->setStockFrom($stockFrom);
        }

        if ($data['warehouse_to']) {
            $stockTo = $stockRepository->findOneByExternalId($data['warehouse_to']);
            $order->setStockTo($stockTo);
        }

        $organizationRepository = $this->entityManager->getRepository(Organization::class);
        $organization = $organizationRepository->getCustomerByUser($order->getCustomer());
        $order->setOrganization($organization);

        $directionRepository = $this->entityManager->getRepository(Direction::class);
        $direction = $directionRepository->findOneBy(['cityFrom' => $stockFrom->getCity(), 'cityTo' => $stockTo->getCity()]);
        $order->setDirection($direction);

        if (isset($data['cost_min'])) {
            $order->setMinPrice($data['cost_min']);
        }

        if (isset($data['cost_max'])) {
            $order->setMaxPrice($data['cost_max']);
        }

        $order->setDeclaredPrice((int)str_replace(' ', '', $data['declared_value']));

        if (!$order->getReceiver()) {
            $receiver = new Person();
        } else {
            $receiver = $order->getReceiver();
        }

        if ($data['receiver']['company']['type'] || $data['receiver']['person']['name']) {
            if ($data['receiver']['company']['type']) {
                $company = $data['receiver']['company'];

				$receiver->setUridical(true);
                $receiver->setType($company['type']);
                $receiver->setCompanyName($company['name']);
                $receiver->setInn($company['inn']);
                $receiver->setContactName($company['contact_name']);
                $receiver->setContactPhone($company['contact_phone']);

                if (isset($company['contact_email'])) {
                    $receiver->setContactEmail($company['contact_email']);
                }

            } else {
                $person = $data['receiver']['person'];
				$receiver->setUridical(false);
                $receiver->setContactName($person['name']);
                $receiver->setContactPhone($person['phone']);

                if (isset($person['email'])) {
                    $receiver->setContactEmail($person['email']);
                }

                $receiver->setSeries(substr($person['identity']['no'], 0, 4));
                $receiver->setNumber(substr($person['identity']['no'], 4, 20));
            }
            $order->setReceiver($receiver);
        }

        if (isset($data['cost']) && !empty($data['cost']['total'])) {
            $order->setPrice((float)$data['cost']['total']['total']);
        }

        if (isset($data['paid'])) {
            $order->setPayed($data['paid']);
        }

        if (!$order->getStatus()) {
            $order->setStatus(WmsOrder::STATUS_WAITING);
        }

        return $order;
    }

    public function mapInfoOrderToWmsOrder(WmsOrder $order, $data)
    {
        $packages = $order->getPackages();

        $order->setPackages(new ArrayCollection());
        if ($packages->count() > 0) {
            /** @var WmsPackage $package */
            foreach ($packages as $packageKey => $package) {
                $found = false;

                foreach ($data['cargos'] as $cargokey => $cargo) {
                    if ($package->getExternalId() == $cargo['id']) {
                        $package = $this->mapBaseToWmsPackage($package, $cargo);
                        $this->entityManager->persist($package);
                        $order->addPackage($package);
                        unset($data['cargos'][$cargokey]);
                    }
                }
            }
        }

        if ($data['cargos']) {
            foreach ($data['cargos'] as $cargokey => $cargo) {
                $package = $this->mapBaseToWmsPackage(null, $cargo);
                $this->entityManager->persist($package);
                $order->addPackage($package);
                unset($data['cargos'][$cargokey]);
            }
        }

        $order->setExternalId($data['id']);

        $stockRepository = $this->entityManager->getRepository(Stock::class);

		if ($data['warehouse_from']) {
			$stockFrom = $stockRepository->findOneByExternalId($data['warehouse_from']);
			$order->setStockFrom($stockFrom);
		}

		if ($data['warehouse_to']) {
			$stockTo = $stockRepository->findOneByExternalId($data['warehouse_to']);
			$order->setStockTo($stockTo);
		}

        $order->setStatus($data['status']);
        $order->setDeclaredPrice((int)str_replace(' ', '', $data['declared_value']));

        $customerRepository = $this->entityManager->getRepository(User::class);
        $customer = $customerRepository->findOneByExternalId($data['customer']['id']);
        if ($customer) {
            $order->setCustomer($customer);
        }

        if (isset($data['cost']) && !empty($data['cost']['total'])) {
            $order->setPrice((float)$data['cost']['total']['total']);
        }

        switch ($data['status']) {
            case WmsOrder::STATUS_ACCEPTED:
                if (!$order->getReceptionAt()) {
                    $order->setReceptionAt(new \DateTime());
                }
                break;
            case WmsOrder::STATUS_DELIVERED:
                if (!$order->getIssueAt()) {
                    $order->setIssueAt(new \DateTime());
                }
                break;
        }

        return $order;
    }

    public function orderRejected(WmsOrder $order)
    {
        $dateTime = new \DateTime();
        $data = [
            'ext_id' => $order->getExternalId(),
            'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'warehouse' => $order->getStockFrom()->getExternalId(),
        ];

        return $data;
    }

    public function orderAccepted(WmsOrder $order)
    {
        $dateTime = new \DateTime();
        $data = [
            'new' => false,
            'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'warehouse' => $order->getStockFrom()->getExternalId(),
        ];
        $data['order'] = [
            'ext_id' => $order->getExternalId(),
            'customer' => $order->getUser()->getExternalId(),
            'route' => $order->getDirection()->getExternalId(),
            'declared_value' => (int)$order->getDeclaredPrice(),

            'insurance' => '',
            'warehouse_from' => $order->getStockFrom()->getExternalId(),
            'warehouse_to' => $order->getStockTo()->getExternalId(),
            'price_max' => 0,
            'paid' => false,
            'weight_discount' => 0,
            'insurance_discount' => 0,
            'customer_delivery_time' => null,
        ];

        if ($order && $order->getPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($order->getPackages() as $orderPackage) {
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'volume' => (float)$orderPackage->getCalculateWeight(),
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $orderPackage->getPackingPrice(),
                ];

                $data['order']['order_lines'][] = $orderPackageData;
            }
        }

        $data['order']['receiver'] = $order->getReceiver()->getExternalId();
        $data['order']['sender'] = $order->getSender()->getExternalId();
        $data['order']['payer'] = $order->getPayer()->getExternalId();

        /** @var Promocode $promocode */
        $data['order']['weight_discount'] = 0;
        $data['order']['insurance_discount'] = 0;
        $promocode = $order->getPromocode();
        if ($promocode) {
            $data['order']['weight_discount'] = (int)$promocode->getDiscountKilo();
            $data['order']['insurance_discount'] = (int)$promocode->getDiscountDeclaredValue();
        }

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            $data['cargos'][] = [
                'ext_id' => $package->getId(),
                'type' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother(),
                'weight' => $package->getWeight(),
                'count' => $package->getCount(),
                'length' => $package->getDepth() ? $package->getDepth() : $package->getSizeObject()->getDepth(),
                'width' => $package->getWidth() ? $package->getWidth() : $package->getSizeObject()->getWidth(),
                'height' => $package->getHeight() ? $package->getHeight() : $package->getSizeObject()->getHeight(),
                'pack' => $package->getPacking() ? $package->getPacking()->getTitle() : '',
                'pack_cost' => (int)$package->getPackingPrice(),
            ];
        }

        return $data;
    }

    public function newOrderAccepted(WmsOrder $order)
    {
        $dateTime = new \DateTime();
        $data = [
            'new' => true,
            'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'warehouse' => $order->getStockFrom()->getExternalId(),
        ];
        $data['order'] = [
            'id' => null,
            'ext_id' => $order->getExternalId(),
            'customer' => $order->getUser()->getExternalId(),
            'route' => $order->getDirection()->getExternalId(),
            'declared_value' => (int)$order->getDeclaredPrice(),
            'cost_min' => $order->getMinPrice(),
            'cost_max' => $order->getMaxPrice(),
            'insurance' => '',
            'warehouse_from' => $order->getStockFrom()->getExternalId(),
            'warehouse_to' => $order->getStockTo()->getExternalId(),
            'price_max' => 0,
            'paid' => false,
            'weight_discount' => 0,
            'insurance_discount' => 0,
            'customer_delivery_time' => null,
        ];

        /** @var Promocode $promocode */
        $data['order']['weight_discount'] = 0;
        $data['order']['insurance_discount'] = 0;
        $promocode = $order->getPromocode();
        if ($promocode) {
            $data['order']['weight_discount'] = (int)$promocode->getDiscountKilo();
            $data['order']['insurance_discount'] = (int)$promocode->getDiscountDeclaredValue();
        }

        if ($order && $order->getPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($order->getPackages() as $orderPackage) {
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $orderPackage->getPackingPrice(),
                ];
                $data['order']['order_lines'][] = $orderPackageData;
            }
        }

        $data['order']['receiver'] = $order->getReceiver()->getExternalId();
        $data['order']['sender'] = $order->getSender()->getExternalId();
        $data['order']['payer'] = $order->getPayer()->getExternalId();

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            $data['cargos'][] = [
                'ext_id' => $package->getId(),
                'type' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                'weight' => $package->getWeight(),
                'length' => $package->getDepth() ? $package->getDepth() : $package->getSizeObject()->getDepth(),
                'width' => $package->getWidth() ? $package->getWidth() : $package->getSizeObject()->getWidth(),
                'height' => $package->getHeight() ? $package->getHeight() : $package->getSizeObject()->getHeight(),
                'pack' => $package->getPacking() ? $package->getPacking()->getTitle() : '',
                'pack_cost' => (int)$package->getPackingPrice(),
            ];
        }

        return $data;
    }


    public function orderDelivered(WmsOrder $order)
    {
        $dateTime = new \DateTime();
        $data = [
            'ext_id' => $order->getExternalId(),
            'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'warehouse' => $order->getStockTo()->getExternalId(),
        ];

        return $data;
    }
}
