<?php
namespace App\Service\Mapper\Wms;

use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Receiver;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsOrderPackage;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Stock;

class WmsPalletMapperService
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function cargoPalletted(WmsPallet $pallet)
    {
        $dateTime = new \DateTime();
        $data = [
            'warehouse' => $pallet->getStockFrom()->getExternalId(),
            'ext_id' => $pallet->getExternalId(),
            'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'weight' => $pallet->getWeight(),
        ];

        foreach ($pallet->getPackages() as $package) {
            $data['cargos'][] = $package->getId();
        }

        return $data;
    }

    public function cargoUnPalletted(WmsPallet $pallet)
    {
        $dateTime = new \DateTime();
        
        if (!$pallet->getBatch()) {
			$data = [
				'warehouse' => $pallet->getStockFrom()->getExternalId(),
				'ext_id' => $pallet->getExternalId(),
				'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
			];
		} else {
			$data = [
				'warehouse' => $pallet->getStockTo()->getExternalId(),
				'ext_id' => $pallet->getExternalId(),
				'time' => $dateTime->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
			];
		}

        return $data;
    }

}
