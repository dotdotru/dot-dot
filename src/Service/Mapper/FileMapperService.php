<?php

namespace App\Service\Mapper;

use App\Entity\Batch;
use App\Entity\Document;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use Mimey\MimeTypes;

use App\Service\IntegrationBaseService;
use Dadata\Response\Date;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Shipping;
use Sonata\MediaBundle\Extra\ApiMediaFile;
use Symfony\Component\DependencyInjection\Container;
use App\Service\FileUploadService;

class FileMapperService
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;

        $this->fileUploadService = $this->container->get('app.file_upload');
    }

    public function mapFileToBaseDocument(File $file)
    {
        $provider = $this->container->get($file->getMedia()->getProviderName());
        $path = $this->container->get('kernel')->getProjectDir().'/public'.$provider->generatePublicUrl($file->getMedia(), 'reference');

        if (!file_exists($path)) {
            return null;
        }

        $doctrine = $this->container->get('doctrine');
        $orderRepository = $doctrine->getRepository(Order::class);
        $batchRepository = $doctrine->getRepository(Batch::class);
        $wmsOrderRepository = $doctrine->getRepository(WmsOrder::class);
        $wmsBatchRepository = $doctrine->getRepository(WmsBatch::class);

        $data = file_get_contents($path);
        $content = bin2hex($data);
        $content = "\\x" . substr($content,0,-2);

        $data = [
            'id' => $file->getExternalId(),
            'ext_id' => $file->getId(),
            'order_ext_id' => null,
            'batch_ext_id' => null,
            'type' => $file->getGroup()->getSlug(),
            'date' => $file->getCreatedAt()->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'number'=> $file->getTitle(),
            'mime_type'=> $file->getMedia()->getContentType(),
            'content' => $content,
        ];

        $order = $orderRepository->getByFile($file->getId());
        if ($order) {
            $data['order_ext_id'] = $order->getId();
        }

        $batch = $batchRepository->getByFile($file->getId());
        if ($batch) {
            $data['batch_ext_id'] = $batch->getId();
        }

        if (!$data['order_ext_id'] && !$data['batch_ext_id']) {
            $wmsOrder = $wmsOrderRepository->getByFile($file->getId());
            if ($wmsOrder) {
                $data['order_ext_id'] = $wmsOrder->getExternalId();
            }

            $wmsBatch = $wmsBatchRepository->getByFile($file->getId());
            if ($wmsBatch) {
                $data['batch_ext_id'] = $wmsBatch->getExternalId();
            }
        }

        return $data;
    }

    public function mapBaseDocumentToFile(File $file, $data)
    {
        $fileGroupRepository = $this->entityManager->getRepository(FileGroup::class);

        $fileGroup = $fileGroupRepository->findOneBySlug($data['type']);

        $file->setPublished(true);
        $file->setExternalId($data['id']);
        $file->setTitle($data['number']);

        $file->setGroup($fileGroup);
        $file->setDate(new \DateTime($data['date']));
        
        $content = $data['content'];
        $content = substr($content, 2, strlen($content));
        $content = hex2bin($content);

        $handle = tmpfile();
        fwrite($handle, $content);
        $tmpFile = new ApiMediaFile($handle);

        $mimes = new MimeTypes();
        $extension = $mimes->getExtension($data['mime_type']);

        $tmpFile->setExtension($extension);
        $tmpFile->setMimetype($data['mime_type']);

        if ($file->getMedia()) {
            $file->getMedia()->setBinaryContent($tmpFile);
        } else {
            $media = $this->fileUploadService->createMedia($tmpFile);
            $file->setMedia($media);
        }

        return $file;
    }

    public function mapListDocuments($data)
    {
        $fileRepository = $this->entityManager->getRepository(File::class);

        foreach ($data as $documentData) {
            $documentDate = new \DateTime($documentData['date']);

            /** @var File $file */
            $file = $fileRepository->findOneByExternalId($documentData['id']);

            if (!$file) {
                $documentDataResult = $integrationBaseService->whGetDocument($documentData['id']);
                if ($documentDataResult[IntegrationBaseService::RESULT_SUCCESS]) {
                    $file = new File();
                    $file = $fileUpdateService->mapBaseDocumentToFile($file, $documentDataResult['data']);
                    $entityManager->persist($file);
                    $wmsOrder->addFile($file);

                    $entityManager->persist($wmsOrder);
                    $entityManager->flush();
                }
            } elseif ($file && (!$file->getDate() || $file->getDate() < $documentDate)) {
                $documentDataResult = $integrationBaseService->whGetDocument($documentData['id']);
                if ($documentDataResult[IntegrationBaseService::RESULT_SUCCESS]) {
                    $file = $fileUpdateService->mapBaseDocumentToFile($file, $documentDataResult['data']);

                    $entityManager->persist($file);

                    if (!$wmsOrder->getFileByExtId($documentData['id'])) {
                        $wmsOrder->addFile($file);
                    }

                    $entityManager->persist($wmsOrder);
                    $entityManager->flush();
                }
            }
        }
    }
}
