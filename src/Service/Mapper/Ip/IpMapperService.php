<?php
namespace App\Service\Mapper\Ip;

use App\Entity\Document;
use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\PackageType;
use App\Entity\Shipping;
use App\Service\CalculateService;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class IpMapperService
{
    /** @var EntityManager|null  */
    protected $entityManager = null;

    /** @var CalculateService|null  */
    protected $calculateService = null;

    public function __construct(EntityManager $entityManager, CalculateService $calculateService)
    {
        $this->entityManager = $entityManager;
        $this->calculateService = $calculateService;
    }

    public function calc(OrderInterface $order, Shipping $shipping)
    {
        $data = [
            'id' => $shipping->getId(),
            'from' => [
                'city' => [
                    'id' => $shipping->getStock()->getCity()->getId(),
                    'title' => $shipping->getStock()->getCity()->getTitle(),
                ],
                'addr' => ($shipping->getType() == Shipping::TYPE_PICKUP) ? $shipping->getAddress() : $shipping->getStock()->getAddress(),
            ],
            'to' => [
                'city' => [
                    'id' => $shipping->getStock()->getCity()->getId(),
                    'title' => $shipping->getStock()->getCity()->getTitle(),
                ],
                'addr' => ($shipping->getType() == Shipping::TYPE_PICKUP) ? $shipping->getStock()->getAddress() : $shipping->getAddress(),
            ],
            'distance' => $shipping->getDistance(),
            'requirements' => [
                'tail_lift' => $shipping->getHydroBoard() ? 1 : 0,
                'loaders' => (int)$shipping->getCountLoader(),
            ],
            'products' => [], 
        ];

        if ($shipping->getPickAt()) {
            $begin = $shipping->getPickAt();
            $end = clone $begin;
            $end = $end->add(new \DateInterval('PT4H'));
            $data['time_slot'] = [
                'begin' => $begin->format(DATE_ISO8601), // в формате 2017-03-23T11:46:15+0300
                'end' => $end->format(DATE_ISO8601),
            ];
        }

        if (count($order->getPackages()->toArray()) > 0) {
            /** @var Package $package */
            foreach ($order->getPackages() as $package) {
                $count = $package->getCount();
                $product = [
                    'code' => $package->getId(),
                    'name' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother(),
                    'count' => $count,
                    'cost' => $order->getDeclaredPrice(),
                    'weight' => $this->calculateService->getCalculateWeight($package, true),
                    'volume' => $package->getSizeObject()->getVolume()
                ];

                $data['products'][] = $product;
            }
        } else {
            /** @var Package $package */
            foreach ($order->getOrderPackages() as $package) {
                if ($order->isByCargo()) {
                    $package->setCount(1);
                }
                $product = [
                    'code' => $package->getId(),
                    'name' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother(),
                    'count' => $package->getCount(),
                    'cost' => $order->getDeclaredPrice(),
                    'weight' => $this->calculateService->getCalculateWeight($package, !$order->isByCargo()),
                    'volume' => $package->getSizeObject()->getVolume()
                ];

                $data['products'][] = $product;
            }
        }

        return $data;
    }

    /*public function mapDriverDataToShipping(Shipping $shipping, $driverInfo)
    {
        if (!isset($driverInfo['employee'])) {
            return false;
        }

        $driverData = $driverInfo['employee'];

        $driverDocument = (isset($driverData['passport']['series']) && isset($driverData['passport']['number'])) ?
            "{$driverData['passport']['series']} {$driverData['passport']['number']}" : '';

        $driverPhone = isset($driverData['phone']) ? $driverData['phone'] : '';

        $name = isset($driverData['name']) ? $driverData['name'] : '';
        $surname = isset($driverData['surname']) ? $driverData['surname'] : '';
        $patronymic = isset($driverData['patronymic']) ? $driverData['patronymic'] : '';
        $driverName = "$surname $name $patronymic";

        $shipping->setDriverName($driverName);
        $shipping->setDriverPhone($driverPhone);
        $shipping->setDriverDocument($driverDocument);
    }*/
}
