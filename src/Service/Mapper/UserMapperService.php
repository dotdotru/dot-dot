<?php
namespace App\Service\Mapper;

use App\Entity\Document;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;

class UserMapperService
{
    public function mapEntityToBase(User $user, Organization $organization = null)
    {
        $data = [
            'id' => $user->getExternalId() ? $user->getExternalId() : null,
            'active' => $user->isEnabled(),
            'email' => $user->getEmail(),
            'phone' => $user->getUsername(),
            'actor' => $user->getPerson()->getExternalId(),
        ];

        /*if ($organization) {
            $data['company_or_person']['company'] = [
                'type' => $organization->getType(),
                'name' => $organization->getCompanyName(),
                'vat' => (int)$organization->getNds(),
                'inn' => $organization->getInn(),
                'kpp' => $organization->getKpp(),
                'bic' => $organization->getBik(),
                'account_no' => $organization->getRs(),
                'ceo' => $organization->getCeo(),
                'contact_name' => $organization->getContactName(),
                'contact_phone' => $organization->getContactPhone(),
                'address' => $organization->getAddress(),
                'verified' => $organization->getVerified() ? $organization->getVerifiedDate()->format(IntegrationBaseService::DATETIME_FORMAT) : null,
			
            ];
        } else {
            $data['company_or_person']['person'] = [
                'name' => $user->getFirstname(),
                'inn' => '',
                'birth_date' => null,
                'identity' => [
                    'type' => 'passport',
                    'no' => $user->getSeries().$user->getNumber(),
                    'issued' => null,
                    'issued_by' =>  '',
                ],
            ];
        }*/

        $data['is_carrier'] = false;
        $organization = $user->getOrganizationCarrier();
        if ($organization && $organization->getDrivers()) {
            $data['is_carrier'] = true;
            $data['miles'] = $user->hasRole('ROLE_MILES');

            /** @var OrganizationDriver $driver */
            foreach ($organization->getDrivers() as $driver) {
                $driverMap = [
                    'id' => $driver->getId(),
                    'person' => [
                        'name' => $driver->getName(),
                        'inn' => $driver->getInn(),
                        'birth_date' => $driver->getBirthdate()->format(IntegrationBaseService::DATETIME_FORMAT),
                    ],
                    'verified' => $driver->getVerified() ? $driver->getVerifiedDate()->format(IntegrationBaseService::DATETIME_FORMAT) : null,
                ];

                /** @var Document $document */
                foreach ($driver->getDocuments() as $document) {
                    if ($document->getType() == 'passport') {
                        $driverMap['person']['identity'] = [
                            'type' => $document->getType(),
                            'no' => $document->getSeries().$document->getNumber(),
                            'issued' => $document->getDate()->format(IntegrationBaseService::DATETIME_FORMAT),
                            'issued_by' => $document->getPlace()
                        ];
                    }
                    if ($document->getType() == 'license') {
                        $driverMap['license'] = [
                            'type' => $document->getType(),
                            'no' => $document->getSeries().$document->getNumber(),
                            'issued' => $document->getDate()->format(IntegrationBaseService::DATETIME_FORMAT),
                            'issued_by' => $document->getPlace()
                        ];
                    }
                }

                $data['drivers'][] = $driverMap;
            }
        }

        return $data;
    }

    public function mapBaseToEntity($data)
    {


        return '';
    }

}
