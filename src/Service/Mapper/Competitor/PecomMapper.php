<?php
namespace App\Service\Mapper\Competitor;

use App\Entity\BaseEvent;
use App\Entity\Order;
use App\Entity\Package;

class PecomMapper extends CompetitorMapper
{
    const CODE = 'pecom';

    public function map(Order $order)
    {
        $result = [];
        /** @var Package $package */
        foreach ($order->getOrderPackages() as $package) {
            if ($order->isByCargo()) {
                $place = [];
                $place[] = $package->getWidth() ? $package->getWidth() / 100 : 0;
                $place[] = $package->getDepth() ? $package->getDepth() / 100 : 0;
                $place[] = $package->getHeight() ? $package->getHeight() / 100 : 0;
                $place[] = $package->getSizeObject()->getVolume();
                $place[] = $package->getWeight();
                $place[] = $package->getWeight() >= 1000 ? 1 : 0;

                $result['places'][] = $place;
            } else {
                for ($i=1;$i<=$package->getCount();$i++) {
                    $place = [];
                    $place[] = $package->getWidth() ? $package->getWidth() / 100 : 0;
                    $place[] = $package->getDepth() ? $package->getDepth() / 100 : 0;
                    $place[] = $package->getHeight() ? $package->getHeight() / 100 : 0;
                    $place[] = $package->getSizeObject()->getVolume();
                    $place[] = $package->getWeight();
                    $place[] = $package->getWeight() >= 1000 ? 1 : 0;

                    $result['places'][] = $place;
                }
            }

        }

        $result['take']['town'] = $order->getDirection()->getCityFrom()->getCityCode($this::CODE);
        $result['deliver']['town'] = $order->getDirection()->getCityTo()->getCityCode($this::CODE);

        $result['strah'] = $order->getDeclaredPrice();

        return $result;
    }
}
