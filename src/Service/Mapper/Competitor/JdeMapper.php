<?php
namespace App\Service\Mapper\Competitor;

use App\Entity\BaseEvent;
use App\Entity\Order;
use App\Entity\Package;

class JdeMapper extends CompetitorMapper
{
    const CODE = 'jde';

    public function map(Order $order)
    {
        $result = [];

        /** @var Package $package */
        $resultItem = ['type' => 1, 'volume' => 0, 'weight' => 0];
        foreach ($order->getOrderPackages() as $package) {
            if ($order->isByCargo()) {
                $resultItem['volume'] += $package->getSizeObject()->getVolume();
                $resultItem['weight'] += $package->getWeight();
            } else {
                for ($i=1;$i<=$package->getCount();$i++) {
                    $resultItem['volume'] += $package->getSizeObject()->getVolume();
                    $resultItem['weight'] += $package->getWeight();
                }
            }
        }

        $resultItem['width'] = 0.8;
        $resultItem['length'] = 1.2;
        $resultItem['height'] = 1.8;
        $resultItem['from'] = $order->getDirection()->getCityFrom()->getCityCode($this::CODE);
        $resultItem['to'] = $order->getDirection()->getCityTo()->getCityCode($this::CODE);
        $resultItem['declared'] = round($order->getDeclaredPrice());

        $result[] = $resultItem;

        return $result;
    }
}
