<?php
namespace App\Service\Mapper\Competitor;

use App\Entity\BaseEvent;
use App\Entity\Order;
use App\Service\Competitor\CompetitorService;
use Doctrine\ORM\EntityManager;

abstract class CompetitorMapper
{
    /** @var EntityManager  */
    protected $entityManager;

    const CODE = '';

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function isValid(CompetitorService $service)
    {
        if (trim($service->getCode()) == trim($this::CODE)) {
            return true;
        }

        return false;
    }

    public abstract function map(Order $order);

}