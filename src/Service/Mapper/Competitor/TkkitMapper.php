<?php
namespace App\Service\Mapper\Competitor;

use App\Entity\BaseEvent;
use App\Entity\Order;
use App\Entity\Package;

class TkkitMapper extends CompetitorMapper
{
    const CODE = 'tkkit';

    public function map(Order $order)
    {
        $result = [];

        /** @var Package $package */
        foreach ($order->getOrderPackages() as $package) {
            $resultItem = ['I_DELIVER' => false, 'I_PICK_UP' => false];
            $resultItem['WIDTH'] = $package->getWidth();
            $resultItem['LENGTH'] = $package->getDepth();
            $resultItem['HEIGHT'] = $package->getHeight();

            if ($order->isByCargo()) {
                $resultItem['VOLUME'] = $package->getSizeObject()->getVolume();
            } else {
                $resultItem['VOLUME'] = $package->getSizeObject()->getVolume() * $package->getCount();
            }

            if ($order->isByCargo()) {
                $resultItem['WEIGHT'] = $package->getWeight();
            } else {
                $resultItem['WEIGHT'] = $package->getWeight() * $package->getCount();
            }

            $resultItem['SCODE'] = $order->getDirection()->getCityFrom()->getCityCode($this::CODE);
            $resultItem['RCODE'] = $order->getDirection()->getCityTo()->getCityCode($this::CODE);

            $resultItem['PRICE'] = round($order->getDeclaredPrice() / $order->getPackages()->count());

            $result[] = $resultItem;
        }

        return $result;
    }
}
