<?php
namespace App\Service\Mapper\Competitor;

use App\Entity\Order;
use App\Entity\Package;

class DellinMapper extends CompetitorMapper
{
    const CODE = 'dellin';

    public function map(Order $order)
    {
        $result = [];
        $result['sizedVolume'] = 0;
        $result['sizedWeight'] = 0;

        /** @var Package $package */
        foreach ($order->getOrderPackages() as $package) {
            if ($order->isByCargo()) {
                $result['sizedVolume'] += $package->getSizeObject()->getVolume();
            } else {
                for ($i=1;$i<=$package->getCount();$i++) {
                    $result['sizedVolume'] += $package->getSizeObject()->getVolume();
                }
            }
        }

        $result['sizedWeight'] += $order->getTotalWeight();
        $result['derivalPoint'] = $order->getDirection()->getCityFrom()->getCityCode($this::CODE);
        $result['arrivalPoint'] = $order->getDirection()->getCityTo()->getCityCode($this::CODE);

        $result['statedValue'] = $order->getDeclaredPrice();

        return $result;
    }
}
