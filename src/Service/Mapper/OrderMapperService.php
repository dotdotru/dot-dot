<?php

namespace App\Service\Mapper;

use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\Document;
use App\Entity\File;
use App\Entity\Order;
use App\Entity\OrderPackage;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Person;
use App\Entity\Promocode;
use App\Entity\Receiver;
use App\Entity\Stock;
use App\Entity\StringEntity;
use App\Service\FileUpdateService;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Service\User\UserVatService;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Shipping;

class OrderMapperService
{


    public function __construct(
        EntityManager $entityManager,
        FileUpdateService $fileUpdateService,
        UserVatService $userVatService
    ) {
        $this->entityManager = $entityManager;
        $this->fileUpdateService = $fileUpdateService;
        $this->userVatService = $userVatService;
    }

    public function mapEntityToReserveBase(Batch $order, $vat = false)
    {
        $data = [
            'auction_price' => $order->getCurrentPricePerKilo() / 100,
        ];

        if ($order && $order->getWeights()) {
            /** @var StringEntity $weight */
            foreach ($order->getWeights() as $weight) {
                $data['pallets'][] = $weight->getValue();
            }
        }

        $data['warehouse_from'] = null;
        if ($order->getStockFrom()) {
            $data['warehouse_from'] = $order->getStockFrom()->getExternalId();
        }

        $data['warehouse_to'] = null;
        if ($order->getStockTo()) {
            $data['warehouse_to'] = $order->getStockTo()->getExternalId();
        }

        $data['load_time'] = null;
        if ($order->getSendDate()) {
            $data['load_time'] = $order->getSendDate()->format(IntegrationBaseService::DATETIME_FORMAT_ISO);
        }

        $data['vat'] = $vat;
        
        return $data;
    }

    public function mapOverloadToReserveBase(Batch $order)
    {
        $data = [
            'ext_id' => $order->getId(),
            'carrier' => $order->getCarrier()->getExternalId(),
        ];

        if ($order && $order->getPallets()) {
            /** @var StringEntity $pallet */
            foreach ($order->getPallets() as $pallet) {
                $data['pallets'][] = $pallet->getValue();
            }
        }


        if ($order && $order->getDrivers()) {
            /** @var OrganizationDriver $driver */
            foreach ($order->getDrivers() as $driver) {
                $data['driver'] = $driver->getId();
            }
        }

        $data['warehouse_from'] = null;
        if ($order->getStockFrom()) {
            $data['warehouse_from'] = $order->getStockFrom()->getExternalId();
        }

        $data['warehouse_to'] = null;
        if ($order->getStockTo()) {
            $data['warehouse_to'] = $order->getStockTo()->getExternalId();
        }

        return $data;
    }

    public function mapEntityBatchToBase(Batch $order)
    {
        $data = [
            'carrier' => $order->getCarrier() ? $order->getCarrier()->getExternalId() : null,
            'batch' => $order->getExternalId() ? $order->getExternalId() : null,
        ];

        foreach ($order->getDrivers() as $driver) {
            $data['driver'] = $driver->getId();
        }
        
        $data['ext_id'] = $order->getId();

        return $data;
    }

    public function mapBaseBatchToEntity(Batch $order, $data)
    {
        $batch = $data;

        $order->setExternalId($batch['id']);

        $stockRepository = $this->entityManager->getRepository(Stock::class);

        $stockFrom = $stockRepository->findOneByExternalId($batch['warehouse_from']);
        $order->setStockFrom($stockFrom);

        $stockTo = $stockRepository->findOneByExternalId($batch['warehouse_to']);
        $order->setStockTo($stockTo);

        if (isset($data['pallets']) && !empty($data['pallets'])) {
            $pallets = $data['pallets'];

            $palletIds = [];
            foreach ($pallets as $pallet) {
                $palletIds = $pallet['id'];
            }
            //$order->setPallets($palletIds);

            $totalWeight = 0;
            foreach ($pallets as $pallet) {
                $totalWeight += $pallet['weight'];
            }
            $order->setWeight($totalWeight);
        }

        foreach($batch['history'] as $historyItem) {
            if (!$order->getReceptionAt() && Batch::STATUS_SHIPPING == $historyItem['status']) {
                $order->setReceptionAt(new \DateTime($historyItem['time']));
            }
            if (!$order->getIssueAt() && Batch::STATUS_ARRIVED == $historyItem['status']) {
                $order->setIssueAt(new \DateTime($historyItem['time']));
            }
        }


        if (isset($data['documents'])) {
            $files = $this->fileUpdateService->mapListDocuments($data['documents'], $order->getFiles());

            /** @var File $file */
            foreach ($files as $file) {
                if (!$order->getFileByExtId($file->getExternalId())) {
                    $order->addFile($file);
                }
            }
        }

        if ($batch['status']) {
            $order->setStatus($batch['status']);
        }

        return $order;
    }

    public function mapEntityCustomerToBase(Order $order)
    {
        $data = [
            'id' => $order->getExternalId() ? $order->getExternalId() : null,
            'customer' => $order->getUser()->getExternalId(),
            'route' => $order->getDirection()->getExternalId(),
            'declared_value' => $order->getDeclaredPrice(),
        ];

        $data['order_lines'] = [];
        if ($order && $order->getOrderPackages()) {
            /** @var OrderPackage $orderPackage */
            foreach ($order->getOrderPackages() as $orderPackage) {
                $orderPackageData = [
                    'type' => $orderPackage->getPackageType() ? $orderPackage->getPackageType()->getTitle() : $orderPackage->getPackageTypeAnother(),
                    'num_items' => $orderPackage->getCount(),
                    'length' => (float)$orderPackage->getSizeObject()->getDepth(),
                    'width' => (float)$orderPackage->getSizeObject()->getHeight(),
                    'height' => (float)$orderPackage->getSizeObject()->getWidth(),
                    'weight' => (float)$orderPackage->getWeight(),
                    'volume' => (float)$orderPackage->getCalculateWeight(),
                    'pack' => $orderPackage->getPacking() ? $orderPackage->getPacking()->getTitle() : '',
                    'pack_cost' => $orderPackage->getPackingPrice(),
                ];
                if ($order->isByCargo()) {
                    $orderPackageData['totals'] = true;
                }
                $data['order_lines'][] = $orderPackageData;
            }
        }

        $data['customer_delivery_time'] = $order->getCustomerDeliveryTime() ? $order->getCustomerDeliveryTime() : null;
        $data['cost_min'] = $order->getMinPrice();
        $data['cost_max'] = $order->getMaxPrice();
        $data['insurance'] = $order->isInsuredSuccess() ? $order->getInsuredNumber() : '';
        $data['price'] = (float)$order->getPrice();

        $data['warehouse_from'] = null;
        if ($order->getStockFrom()) {
            $data['warehouse_from'] = $order->getStockFrom()->getExternalId();
        }
        if ($shipping = $order->getShipping(Shipping::TYPE_PICKUP)) {
            $data['warehouse_from'] = $shipping->getStock()->getExternalId();
        }


        $data['warehouse_to'] = null;
        if ($order->getStockTo()) {
            $data['warehouse_to'] = $order->getStockTo()->getExternalId();
        }
        if ($shipping = $order->getShipping(Shipping::TYPE_DELIVER)) {
            $data['warehouse_to'] = $shipping->getStock()->getExternalId();
        }


        $data['warehouse_from_pass_time'] = null;
        if ($order->getReceptionAt()) {
            $data['warehouse_from_pass_time'] = $order->getReceptionAt()->getTimestamp();
        }

        $data['paid'] = $order->isPayed();
        $data['ext_id'] = $order->getId();

        /** @var Promocode $promocode */
        $data['weight_discount'] = 0;
        $data['insurance_discount'] = 0;
        $promocode = $order->getPromocode();
        if ($promocode && $order->isFirst()) {
            $data['weight_discount'] = (int)$promocode->getDiscountKilo();
            $data['insurance_discount'] = (int)$promocode->getDiscountDeclaredValue();
        }

        $data['ext_id'] = $order->getId();

        $data['receiver'] = $order->getReceiver() ? $order->getReceiver()->getExternalId() : null;
        $data['sender'] = $order->getSender() ? $order->getSender()->getExternalId() : null;
        $data['payer'] = $order->getSender() ? $order->getPayer()->getExternalId() : null;

        return $data;
    }

    private function mapBaseToCustomerEntityPackage(Package $package = null, $cargo)
    {
        $packageTypeRepository = $this->entityManager->getRepository(PackageType::class);
        $packingRepository = $this->entityManager->getRepository(Packing::class);

        if (!$package) {
            $package = new Package();
        }

        $package->setExternalId($cargo['id'].$cargo['ext_id']);
        $package->setWeight($cargo['weight']);
        $package->setWidth($cargo['width']);
        $package->setHeight($cargo['height']);
        $package->setDepth($cargo['length']);
        $package->setCount(1);
        $package->setPackageTypeAnother($cargo['type']);

        if ($cargo['type']) {
            $packageType = $packageTypeRepository->findOneBy(['title' => $cargo['type']]);
            $package->setPackageType($packageType);
        } else {
            $package->setPackageTypeAnother($cargo['type']);
        }

        if ($cargo['pack']) {
            $packing = $packingRepository->findOneBy(['title' => $cargo['pack']]);
            $package->setPacking($packing);
            $package->setPackingPrice($cargo['pack_cost']);
        }
        
        if ($cargo['status']) {
	    $package->setStatus($cargo['status']);
	}

        return $package;
    }

    public function mapPerson($data)
    {
        $personRepository = $this->entityManager->getRepository(Person::class);
        $person = $personRepository->findOneByExternalId($data['id']);
        if (!$person) {
            $person = new Person();
        }

        $person->setExternalId($data['id']);

        if ($data['company']['type'] || $data['person']['name']) {
            if ($data['company']['type']) {
                $company = $data['company'];

                $person->setUridical(true);
                $person->setType($company['type']);
                $person->setCompanyName($company['name']);
                $person->setInn($company['inn']);
                $person->setContactName($company['contact_name']);
                $person->setContactPhone($company['contact_phone']);

                if (isset($company['contact_email'])) {
                    $person->setContactEmail($company['contact_email']);
                }
            } else {
                $personData = $data['person'];
                
                $person->setUridical(false);
                $person->setContactName($personData['name']);
                $person->setContactPhone($personData['phone']);

                if (isset($personData['email'])) {
                    $person->setContactEmail($personData['email']);
                }
                
                $personData['identity']['no'] = str_replace('тевса234ывапиик333', '', $personData['identity']['no']);
                
                $person->setSeries(substr($personData['identity']['no'], 0, 4));
                $person->setNumber(substr($personData['identity']['no'], 4, 20));
            }
        }
        
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        return $person;
    }

    public function mapPersonToData(Person $person)
    {
        $data = [
            'id' => $person->getExternalId(),
            'ext_id' => $person->getId(),
            'user' => $person->getUser()->getExternalId(),
        ];
        if ($person) {
            if ($person->getUridical() > 0) {
                $data['company'] = [
                    'type' => $person->getType(),
                    'name' => $person->getCompanyName(),
                    'vat' => $this->userVatService->get($person->getUser()),
                    'inn' => $person->getInn(),
                    'kpp' => '',
                    'bic' => '',
                    'account_no' => '',
                    'ceo' => '',
                    'contact_name' => $person->getContactName(),
                    'contact_phone' => $person->getContactPhone(),
                    'contact_email' => $person->getContactEmail(),
                    'address' => '',
                    'verified' => null
                ];
            } else {
                $data['person'] = [
                    'name' => $person->getContactName(),
                    'inn' => '',
                    'birth_date' => null,
                    'identity' => [
                        'type' => 'passport',
                        'no' => $person->getSeries().$person->getNumber(),
                        'issued' => null,
                        'issued_by' => '',
                    ],
                    'phone' => $person->getContactPhone(),
                    'email' => $person->getContactEmail(),
                ];
            }
        }

        return $data;
    }

    public function mapBaseToCustomerEntity(Order $order, $data)
    {
        $directionRepository = $this->entityManager->getRepository(Direction::class);
        $userRepository = $this->entityManager->getRepository(User::class);
        $packageRepository = $this->entityManager->getRepository(Package::class);
        $packages = $order->getPackages();

        $order->setPackages(new ArrayCollection());
        if ($packages) {
            /** @var Package $package */
            foreach ($packages as $packageKey => $package) {
                $found = false;

                foreach ($data['cargos'] as $cargokey => $cargo) {
                    if ($package->getExternalId() == $cargo['id'].$cargo['ext_id']) {
                        $package = $this->mapBaseToCustomerEntityPackage($package, $cargo);
                        $this->entityManager->persist($package);
                        $order->addPackage($package);
                        unset($data['cargos'][$cargokey]);
                    }
                }
            }
        }

        if ($data['cargos']) {
            foreach ($data['cargos'] as $cargokey => $cargo) {
                $package = $packageRepository->findOneBy(['externalId' => $cargo['id'].$cargo['ext_id']]);
                $package = $this->mapBaseToCustomerEntityPackage($package, $cargo);
                $this->entityManager->persist($package);
                $order->addPackage($package);
                unset($data['cargos'][$cargokey]);
            }
        }

        $order->setExternalId($data['id']);

        $stockRepository = $this->entityManager->getRepository(Stock::class);

		if ($data['warehouse_from']) {
			$stockFrom = $stockRepository->findOneByExternalId($data['warehouse_from']);
			$order->setStockFrom($stockFrom);

			$shipping = $order->getShipping(Shipping::TYPE_PICKUP);
			if ($shipping) {
                $shipping->setStock($stockFrom);
            } else {
                $shipping = new Shipping();
                $shipping->setStock($stockFrom);
                $shipping->setActive(false);
                $shipping->setType(Shipping::TYPE_PICKUP);
                $order->addShipping($shipping);
            }
		}

		if ($data['warehouse_to']) {
			$stockTo = $stockRepository->findOneByExternalId($data['warehouse_to']);
			$order->setStockTo($stockTo);

            $shipping = $order->getShipping(Shipping::TYPE_DELIVER);
            if ($shipping) {
                $shipping->setStock($stockTo);
            } else {
                $shipping = new Shipping();
                $shipping->setStock($stockTo);
                $shipping->setActive(false);
                $shipping->setType(Shipping::TYPE_DELIVER);
                $order->addShipping($shipping);
            }
		}

        if (!empty($data['customer'])) {
            $user = $userRepository->findOneBy(['externalId' => $data['customer']]);
            if ($user) {
                $order->setUser($user);
            }
        }

        $sender = $this->mapPerson($data['sender']);
        $sender->setUser($user);
        $order->setSender($sender);

        $receiver = $this->mapPerson($data['receiver']);
        $receiver->setUser($user);
        $order->setReceiver($receiver);

        $payer = $this->mapPerson($data['payer']);
        $payer->setUser($user);

        if ($sender->getExternalId() == $payer->getExternalId()) {
            $order->setPayer($sender);
        } else {
            $order->setPayer($payer);
        }
       
        if (!empty($data['route'])) {
            $direction = $directionRepository->findOneBy(['externalId' => $data['route']]);
            if ($direction) {
                $order->setDirection($direction);
            }
        }

        $order->setStatus($data['status']);
        $order->setDeclaredPrice(str_replace(' ', '', $data['declared_value']));

        $order->setMinPrice($data['cost_min']);
        $order->setMaxPrice($data['cost_max']);

        if ($order->isAccepted()) {
            $order->setByCargo(false);
        }

        if (isset($data['cost']) && !empty($data['cost']['total'])) {
            $order->setPrice($data['cost']['total']['total']);

            foreach ($data['cost'] as $costType => $cost) {
                $this->updateOrderPriceDetail($order, $costType, $cost['total'], $cost['vat']);
            }
        }

        if (isset($data['documents'])) {
            $files = $this->fileUpdateService->mapListDocuments($data['documents'], $order->getFiles());

            /** @var File $file */
            foreach ($files as $file) {
                if (!$order->getFileByExtId($file->getExternalId())) {
                    $order->addFile($file);
                }
            }
        }

        foreach($data['history'] as $historyItem) {
            if (!$order->getReceptionAt() && Order::STATUS_ACCEPTED == $historyItem['status']) {
                $order->setReceptionAt(new \DateTime($historyItem['time']));
            }
            if (!$order->getIssueAt() && Order::STATUS_DELIVERED == $historyItem['status']) {
                $order->setIssueAt(new \DateTime($historyItem['time']));
            }
        }

        return $order;
    }

    public function mapOrderToBaseSuspend(Order $order)
    {
        $data['id'] = $order->getExternalId();
        $data['suspended'] = $order->isSuspended();

        return $data;
    }
    
    public function mapBatchToBasePaid(Batch $order)
    {
        $data['batch'] = $order->getExternalId();
        $data['paid'] = $order->isPayed();

        return $data;
    }

    public function mapOrderToBaseConfirm(Batch $order)
    {
        $data = [
            'id' => $order->getExternalId(),
            'load_time' => $order->getSendDate()->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
        ];

        return $data;
    }


    protected function updateOrderPriceDetail(Order $order, $type, $total, $vat)
    {
        $currentPriceDetail = null;

        /** @var Order\PriceDetail $priceDetail */
        foreach ($order->getPriceDetails() as $priceDetail) {
            if ($priceDetail->getType() == $type) {
                $currentPriceDetail = $priceDetail;
            }
        }

        if (!$currentPriceDetail) {
            $currentPriceDetail = new Order\PriceDetail();
            $currentPriceDetail->setOrder($order);
            $order->addPriceDetails($currentPriceDetail);
        }

        $currentPriceDetail->setType($type);
        $currentPriceDetail->setPrice($total);
        $currentPriceDetail->setPriceVat($vat);

        $this->entityManager->persist($currentPriceDetail);
    }


}
