<?php
namespace App\Service\Mapper\Mover;

use App\Entity\Document;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\PackageType;
use App\Entity\Shipping;
use App\Repository\Organization\OrganizationRepository;
use App\Service\CalculateService;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class MoverMapperService
{
    /** @var EntityManager|null  */
    protected $entityManager = null;

    /** @var CalculateService|null  */
    protected $calculateService = null;

    public function __construct(EntityManager $entityManager, CalculateService $calculateService)
    {
        $this->entityManager = $entityManager;
        $this->calculateService = $calculateService;
    }

    public function calc(Order $order, Shipping $shipping)
    {
        /** @var User $sender */
        $sender = $order->getUser();
        $contactName = '';
        $contactPhone = '';

        if ($shipping->getType() == Shipping::TYPE_PICKUP) {
            if ($sender->isPrivatePerson()) {
                $contactName = $sender->getFirstname();
                $contactPhone = $this->formatPhone($sender->getUsername());
            } else {
                /** @var OrganizationRepository $organizationRepository */
                $organizationRepository = $this->entityManager->getRepository(Organization::class);
                $organization = $organizationRepository->getCustomerByUser($sender);
                $contactName = $organization->getContactName();
                $contactPhone = $this->formatPhone($organization->getContactPhone());
            }
        } else {
            $contactName = 'Складской работник';
            $contactPhone = $this->formatPhone($this->clearPhone($shipping->getStock()->getPhone()));
        }

        $data = [
            'name' => $contactName,
            'phone' => $contactPhone, // телефон покупателя в формате ^7[0-9]{10}$
            'from' => [
                'addr' => ($shipping->getType() == Shipping::TYPE_PICKUP) ? $shipping->getAddress() : $shipping->getStock()->getAddress(),
                'contact_name' => $contactName,
                'contact_phone' => $contactPhone,
            ],
            'to' => [
                'addr' => ($shipping->getType() == Shipping::TYPE_PICKUP) ? $shipping->getStock()->getAddress() : $shipping->getAddress(),
            ],
            'legal_entity' => '',
            'pay_method' => 'bank',
            'external_id' => $shipping->getId(),
            'requirements' => [
                'tail_lift' => $shipping->getHydroBoard() ? 1 : 0,
                'loaders' => (int)$shipping->getCountLoader(),
            ],
            'products' => [],
        ];
        
        if ($shipping->getType() == Shipping::TYPE_PICKUP) {
			if ($shipping->getPickAt()) {
				$begin = clone $shipping->getPickAt();
				$end = clone $begin;
				$end = $end->add(new \DateInterval('PT3H'));
				$data['time_slot'] = [
					'begin' => $begin->format(DATE_ISO8601), // в формате 2017-03-23T11:46:15+0300
					'end' => $end->format(DATE_ISO8601),
				];
			}
		} else {
			if ($shipping->getPickAt()) {
				$begin = clone $shipping->getPickAt();
				$end = clone $begin;
				$begin = $begin->sub(new \DateInterval('PT3H'));
				$data['time_slot'] = [
					'begin' => $begin->format(DATE_ISO8601), // в формате 2017-03-23T11:46:15+0300
					'end' => $end->format(DATE_ISO8601),
				];
			}
		}
        
        
        if (count($order->getPackages()->toArray()) > 0) {
			/** @var Package $package */
			foreach ($order->getPackages() as $package) {
				$product = [
					'code' => $package->getId(),
					'name' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother(),
					'count' => $package->getCount(),
					'cost' => $order->getDeclaredPrice(),
                    'weight' => $this->calculateService->getCalculateWeight($package, true),
					'volume' => $package->getSizeObject()->getVolume()
				];

				$data['products'][] = $product;
			}
		} else {
			/** @var Package $package */
			foreach ($order->getOrderPackages() as $package) {
                if ($order->isByCargo()) {
                    $package->setCount(1);
                }
				$product = [
					'code' => $package->getId(),
					'name' => $package->getPackageType() ? $package->getPackageType()->getTitle() : $package->getPackageTypeAnother(),
					'count' => $package->getCount(),
					'cost' => $order->getDeclaredPrice(),
                    'weight' => $this->calculateService->getCalculateWeight($package, !$order->isByCargo()),
					'volume' => $package->getSizeObject()->getVolume()
				];

				$data['products'][] = $product;
			}
		}

        return $data;
    }

    public function mapDriverDataToShipping(Shipping $shipping, $driverInfo)
    {
        if (!isset($driverInfo['employee'])) {
            return false;
        }

        $driverData = $driverInfo['employee'];

        $driverDocument = (isset($driverData['passport']['series']) && isset($driverData['passport']['number'])) ?
            "{$driverData['passport']['series']} {$driverData['passport']['number']}" : '';

        $driverPhone = isset($driverData['phone']) ? $driverData['phone'] : '';

        $name = isset($driverData['name']) ? $driverData['name'] : '';
        $surname = isset($driverData['surname']) ? $driverData['surname'] : '';
        $patronymic = isset($driverData['patronymic']) ? $driverData['patronymic'] : '';
        $driverName = "$surname $name $patronymic";

        $shipping->setDriverName($driverName);
        $shipping->setDriverPhone($driverPhone);
        $shipping->setDriverDocument($driverDocument);
    }

    private function formatPhone($phone)
    {
        $phone = $this->clearPhone($phone);
        $phone = '7'.$phone;

        return $phone;
    }

    public function clearPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        if (in_array(substr($phone, 0, 1), array(7,8))) {
            $phone = substr($phone, 1, strlen($phone));
        }
        return $phone;
    }
}
