<?php
namespace App\Service\Mapper\BaseEvent;

use App\Entity\BaseEvent;
use App\Entity\EmailTemplate;
use App\Entity\Order;
use App\Repository\EmailTemplateRepository;
use App\Repository\OrderRepository;
use App\Service\Mapper\BaseEvent\CompetitorMapper;

class CargoShippedMapper extends BaseEventMapper
{
    const EVENT = 'shipped';

    public function map(BaseEvent $event)
    {
        /** @var EmailTemplateRepository $emailTemplateRepository */
        $emailTemplateRepository = $this->entityManager->getRepository(EmailTemplate::class);

        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager->getRepository(Order::class);

        /** @var Order $order */
        $order = $orderRepository->getByExternalId($event->getEntityId());

        if (!$order) {
            throw new \Exception(sprintf('order %s not found', $event->getEntityId()));
        }

        $emailTemplate = $emailTemplateRepository->getByExternalId(self::EVENT);

        if (!$emailTemplate) {
            throw new \Exception(sprintf('emailTemplate %s not found', self::EVENT));
        }

        $email = $this->mailService->registerEmail($emailTemplate,
            [
                $order->getUser()->getEmail(),
                $order->getReceiver()->getEmail()
            ],
            [
            'entity' => $order,
            ]
        );

        return $email;
    }
}
