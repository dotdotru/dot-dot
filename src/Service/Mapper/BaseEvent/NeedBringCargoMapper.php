<?php
namespace App\Service\Mapper\BaseEvent;

use App\Entity\BaseEvent;
use App\Entity\EmailTemplate;
use App\Entity\Order;
use App\Repository\EmailTemplateRepository;
use App\Repository\OrderRepository;
use App\Service\Mapper\BaseEvent\CompetitorMapper;

class NeedBringCargoMapper extends BaseEventMapper
{
    const EVENT = 'waiting3';

    public function map(BaseEvent $event)
    {
        /** @var EmailTemplateRepository $emailTemplateRepository */
        $emailTemplateRepository = $this->entityManager->getRepository(EmailTemplate::class);

        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager->getRepository(Order::class);

        /** @var Order $order */
        $order = $orderRepository->getByExternalId($event->getEntityId());

        if (!$order) {
            throw new \Exception(sprintf('order %s not found', $event->getEntityId()));
        }

        $emailTemplate = $emailTemplateRepository->getByExternalId(self::EVENT);

        if (!$emailTemplate) {
            throw new \Exception(sprintf('emailTemplate %s not found', self::EVENT));
        }
        
        if ($order->getStockFrom()) {
            $email = $this->mailService->registerEmail($emailTemplate, $order->getUser()->getEmail(), [
				'entity' => $order,
				'STOCKFROM_ADDRESS' => $order->getStockFrom()->getAddress()
			]);
			
			return $email;
        }

        return null;
    }
}
