<?php

namespace App\Service\Mapper\BaseEvent;

use App\Entity\BaseEvent;
use App\Entity\Document;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class BaseEventMapperService
{
    protected $mappers;

    public function __construct()
    {
        $this->mappers = new ArrayCollection();
    }

    public function addMapper($mapper)
    {
        $this->mappers->add($mapper);
    }

    public function map(BaseEvent $event)
    {
        $entity = null;
        foreach ($this->mappers as $mapper) {
            if ($mapper->isValid($event)) {
                $entity = $mapper->map($event);
                break;
            }
        }

        return $entity;
    }
}
