<?php
namespace App\Service\Mapper\BaseEvent;

use App\Entity\BaseEvent;
use App\Service\MailService;
use Doctrine\ORM\EntityManager;

abstract class BaseEventMapper
{
    /** @var EntityManager  */
    protected $entityManager;

    /** @var MailService  */
    protected $mailService;

    const EVENT = '';

    public function __construct(EntityManager $entityManager, MailService $mailService)
    {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
    }

    public function isValid(BaseEvent $event)
    {
        if (trim($event->getEvent()) == trim($this::EVENT)) {
            return true;
        }

        return false;
    }

    public abstract function map(BaseEvent $event);

}