<?php
namespace App\Service\Mapper\BaseEvent;

use App\Entity\BaseEvent;
use App\Entity\EmailTemplate;
use App\Entity\Order;
use App\Repository\EmailTemplateRepository;
use App\Repository\OrderRepository;
use App\Service\Mapper\BaseEvent\CompetitorMapper;

class InternalTransportationMapper extends BaseEventMapper
{
    const EVENT = 'pallet40';

    public function map(BaseEvent $event)
    {
        /** @var EmailTemplateRepository $emailTemplateRepository */
        $emailTemplateRepository = $this->entityManager->getRepository(EmailTemplate::class);

        $emailTemplate = $emailTemplateRepository->getByExternalId(self::EVENT);

        if (!$emailTemplate) {
            throw new \Exception(sprintf('emailTemplate %s not found', self::EVENT));
        }

        $email = $this->mailService->registerEmail($emailTemplate, $this->mailService->getFromEmail());

        return $email;
    }
}
