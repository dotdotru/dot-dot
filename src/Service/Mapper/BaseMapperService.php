<?php
namespace App\Service\Mapper;

use App\Entity\Base\City;
use App\Entity\Base\Direction;
use App\Entity\Base\DirectionPrice;
use App\Entity\Base\Stock;
use App\Entity\Base\StockWorkingHours;
use App\Entity\Base\WorkingCalendar;
use App\Entity\StringEntity;
use App\Service\IntegrationBaseService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;

class BaseMapperService
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->baseCityRepository = $entityManager->getRepository(City::class);
        $this->baseDirectionRepository = $entityManager->getRepository(Direction::class);
        $this->baseDirectionPriceRepository = $entityManager->getRepository(DirectionPrice::class);
        $this->baseStockRepository = $entityManager->getRepository(Stock::class);
        $this->baseStockWorkingHoursRepository = $entityManager->getRepository(StockWorkingHours::class);
        $this->baseWorkingCalendarRepository = $entityManager->getRepository(WorkingCalendar::class);
    }

    public function mapDirectionToBase(Direction $direction)
    {
        $data = [
            'id' => $direction->getExternalId(),
            'city_from' => $direction->getCityFrom() ? $direction->getCityFrom()->getExternalId() : null,
            'city_to' => $direction->getCityTo() ? $direction->getCityTo()->getExternalId() : null,
            'carrier_delivery_time' => $direction->getCarrierDeliveryTime(),
            'customer_delivery_time' => $direction->getCustomerDeliveryTime(),
            'reload_time' => $direction->getReloadTime(),
        ];

        return $data;
    }

    public function mapBaseToDirection($data = [])
    {
        $item = $this->baseDirectionRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new Direction();
        }

        $item->setExternalId($data['id']);
        $item->setCityFrom($this->baseCityRepository->findOneByExternalId($data['city_from']));
        $item->setCityTo($this->baseCityRepository->findOneByExternalId($data['city_to']));
        $item->setCarrierDeliveryTime($data['carrier_delivery_time']);
        $item->setCustomerDeliveryTime($data['customer_delivery_time']);
        $item->setReloadTime($data['reload_time']);
        $item->setSynchAt(null);
        return $item;
    }

    public function mapCityToBase(City $city)
    {
        $data = [
            'id' => $city->getExternalId(),
            'name' => $city->getTitle(),
            'carrier_arrival_limit' => $city->getCarrierArrivalLimit(),
        ];

        return $data;
    }

    public function mapBaseToCity($data = [])
    {
        $item = $this->baseCityRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new City();
        }

        $item->setExternalId($data['id']);
        $item->setTitle($data['name']);
        $item->setCarrierArrivalLimit($data['carrier_arrival_limit']);
        $item->setSynchAt(null);

        return $item;
    }

    public function mapDirectionPriceToBase(DirectionPrice $directionPrice)
    {
        $data = [
            'id' => $directionPrice->getExternalId(),
            'route' => $directionPrice->getDirection()->getExternalId(),
            'wmin' => $directionPrice->getWmin(),
            'price' => $directionPrice->getPrice(),
        ];

        return $data;
    }

    public function mapBaseToDirectionPrice($data = [])
    {
        $item = $this->baseDirectionPriceRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new DirectionPrice();
        }

        $item->setExternalId($data['id']);
        $item->setDirection($this->baseDirectionRepository->findOneByExternalId($data['route']));
        $item->setWmin($data['wmin']);
        $item->setPrice($data['price']);
        $item->setSynchAt(null);
        //$item->setPosition($data['price']);

        return $item;
    }

    public function mapStockToBase(Stock $stock)
    {
        $data = [
            'id' => $stock->getExternalId(),
            'city' => $stock->getCity()->getExternalId(),
            'address' => $stock->getAddress(),
            'timezone' => $stock->getTimezone(),
            'ext_system' => $stock->getExtSystem() ? $stock->getExtSystem() : null,
            'ext_id' => $stock->getExtId() ? $stock->getExtId() : null,
        ];

        return $data;
    }

    public function mapBaseToStock($data = [])
    {
        $item = $this->baseStockRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new Stock();
        }

        $item->setExternalId($data['id']);
        $item->setCity($this->baseCityRepository->findOneByExternalId($data['city']));
        $item->setAddress($data['address']);
        $item->setTimezone($data['timezone']);
        $item->setExtSystem($data['ext_system']);
        $item->setExtId($data['ext_id']);

        $item->setSynchAt(null);

        return $item;
    }

    public function mapStockWorkingHoursToBase(StockWorkingHours $stockWorkingHours)
    {
        $data = [
            'id' => $stockWorkingHours->getExternalId(),
            'warehouse' => $stockWorkingHours->getStock()->getExternalId(),
            'dow' => $stockWorkingHours->getDow(),
            'start_time' => $stockWorkingHours->getStartTime(),
            'end_time' => $stockWorkingHours->getEndTime(),
        ];

        return $data;
    }

    public function mapBaseToStockWorkingHours($data = [])
    {
        $item = $this->baseStockWorkingHoursRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new StockWorkingHours();
        }

        $item->setExternalId($data['id']);
        $item->setDow($data['dow']);
        $item->setStartTime($data['start_time']);
        $item->setEndTime($data['end_time']);
        $item->setSynchAt(null);

        return $item;
    }

    public function mapWorkingCalendarToBase(WorkingCalendar $workingCalendar)
    {
        $data = [
            'id' => $workingCalendar->getExternalId(),
            'warehouse' => $workingCalendar->getStock()->getExternalId(),
            'day' => $workingCalendar->getDay()->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'start_time' => $workingCalendar->getStartTime(),
            'end_time' => $workingCalendar->getEndTime(),
        ];

        return $data;
    }

    public function mapBaseToWorkingCalendar($data = [])
    {
        $item = $this->baseWorkingCalendarRepository->findOneByExternalId($data['id']);
        if (!$item) {
            $item = new WorkingCalendar();
        }

        $item->setExternalId($data['id']);
        $item->setDay(new \DateTime($data['day'].' 00:00:00'));
        $item->setStartTime($data['start_time']);
        $item->setEndTime($data['end_time']);
        $item->setSynchAt(new \DateTime());

        return $item;
    }

}
