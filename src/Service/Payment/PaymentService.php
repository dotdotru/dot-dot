<?php

namespace App\Service\Payment;

use App\Entity\OrderInterface;
use App\Entity\PaymentInterface;
use Doctrine\ORM\EntityManager;
use SimpleXMLElement;

abstract class PaymentService implements PaymentInterface
{
    /** @var EntityManager */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function payOrder(OrderInterface $order)
    {
        if (!$order->getId() > 0) {
            throw new PaymentException('Order ID should be greater then 0');
        }

        if (!$order->getPrice() > 0) {
            throw new PaymentException('Payment amount should be greater then 0');
        }

        if ($order->isPayed()) {
            throw new PaymentException('This order is already paid');
        }

        return $this->getPaymentUrl($order);
    }

    private function getPaymentUrl()
    {

    }

    private function getClient()
    {

    }

}
