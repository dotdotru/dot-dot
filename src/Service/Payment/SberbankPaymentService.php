<?php

namespace App\Service\Payment;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\Payment;
use App\Entity\PaymentInterface;
use Doctrine\ORM\EntityManager;
use App\Service\Payment\PaymentException;

use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\OrderStatus;

class SberbankPaymentService implements PaymentInterface
{
    /*
        0 – без НДС;
        1 – НДС по ставке 0%;
        2 – НДС чека по ставке 10%;
        3 – НДС чека по ставке 18%;
        4 – НДС чека по расчетной ставке 10/110;
        5 – НДС чека по расчетной ставке 18/118.
    */
    const TAX_TYPE_OSN = 0;

    /** @var EntityManager */
    private $entityManager;

    /** @var Client $client */
    private $client;

    private $successUrl;
    private $failUrl;

    public function __construct(EntityManager $entityManager, $username, $password, $testMode = true)
    {
        $this->entityManager = $entityManager;

        /** @var Client $client */
        $this->client = new Client([
            'userName' =>  $username,
            'password' => $password,
            'language' => 'ru',
            'currency' => Currency::RUB,
            'apiUri' => $testMode ? Client::API_URI_TEST : Client::API_URI,
            'httpMethod' => 'GET',
        ]);
    }

    public function setSuccessUrl($successUrl)
    {
        $this->successUrl = $successUrl;
    }

    public function setFailUrl($failUrl)
    {
        $this->failUrl = $failUrl;
    }

    public function payOrder(OrderInterface $order)
    {
        /** @var Order $order */

        if (!$order->getExternalId() > 0) {
            throw new PaymentException('Order ID should be greater then 0');
        }

        $price = $order->getTotalPrice();
        if ($order->isSuspended()) {
            $price = $order->getSuspendedPrice();
        }

        if ($price <= 0) {
            throw new PaymentException('Payment amount should be greater then 0');
        }

        if ($order->isPayed()) {
            throw new PaymentException('This order is already paid');
        }

        $payment = new Payment();
        $order->addPayment($payment);

        $params['failUrl']  = $this->failUrl;
        $params['description'] = 'Заказ № '.$order->getId();

        $dateTime = new \DateTime();

        $price = $price * 100;

        $orderBundle = [
            //'orderCreationDate' => $dateTime->format(\DateTime::ISO8601),
            'customerDetails' => [
                'email' => $order->getUser()->getEmail(),
                'phone' => $order->getUser()->getUsername(),
            ],
            'cartItems' => [
                'items' => [
                    [
                        'positionId' => $order->getExternalId(),
                        'name' => $params['description'],
                        'quantity' => [
                            'value' => 1,
                            'measure' => 'штук',
                        ],
                        'itemAmount' =>  $price,
                        'itemCode' => $order->getExternalId(),
                        'tax' => [
                            'taxType' => self::TAX_TYPE_OSN
                        ],
                        'itemPrice' => $price,
                    ]
                ]
            ]
        ];
        
        foreach($orderBundle['cartItems'] as $keyItems => $items) {
            foreach($items as $keyItem => $item) {
                $item['itemAttributes']['attributes'][] = ['name' => 'paymentMethod', 'value' => 1]; // полная оплата в момент передачи предмета расчёта;
                $item['itemAttributes']['attributes'][] = ['name' => 'paymentObject', 'value' => 4]; // услуга;
                $orderBundle['cartItems'][$keyItems][$keyItem] = $item;
            }
        }

        $this->entityManager->persist($order);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $params['orderBundle'] = json_encode($orderBundle);
        $result = $this->client->registerOrder($payment->getId(), $price, $this->successUrl, $params);

        $payment->setExternalId($result['orderId']);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $result['formUrl'];
    }

    public function setPayed(OrderInterface $order)
    {
        if ($order->isPayed()) {
            return true;
        }

        $order->setPayed(true);
        $order->setPayedDate(new \DateTime());

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return true;
    }

    public function checkPayment(OrderInterface $order)
    {
        if ($order->isPayed() || !$order->getPayment()) {
            return false;
        }

        $info = $this->client->getOrderStatus($order->getPayment()->getExternalId());

        $result = [
            'orderId' => $info['orderNumber'],
            'paymentAmount' => $info['amount'],
            'status' => OrderStatus::statusToString($info['orderStatus'])
        ];
        return $result;
    }

    public function getPaymentStatusExtended($orderId)
    {
        $info = $this->client->getOrderStatus($orderId);
        $info['amount'] = $info['amount'] / 100;

        $result = [
            'orderId' => $info['orderNumber'],
            'paymentAmount' => $info['amount'],
            'payed' => OrderStatus::isDeposited($info['orderStatus']) ? true : false,
            'declined' => (OrderStatus::isDeclined($info['orderStatus']) || OrderStatus::isRefunded($info['orderStatus'])),
            'status' => OrderStatus::statusToString($info['orderStatus'])
        ];
        return $result;
    }

}
