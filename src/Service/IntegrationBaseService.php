<?php

namespace App\Service;

use App\Entity\Organization\Organization;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use App\Entity\Base;

class IntegrationBaseService
{
    const RESULT_SUCCESS = 'success';
    const DATETIME_FORMAT = 'Y-m-d';
    const DATETIME_FORMAT_ISO = 'c';

    /** @var Client */
    private $client;

    private $url = '';
    private $username = '';
    private $password = '';

    private $token = '';

    private $logger = null;

    const URL_AUTH = '/auth';

    public function __construct(Client $client, $url, $username = null, $password = null)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;

        $this->getClient();
    }

    public function setLogger(LoggerInterface $logger)
    {
	   $this->logger = $logger;
	}

    public function getLogger()
    {
		return $this->logger;
	}

    public function authByStock(Base\Stock $stock)
    {
        $this->setUsername($stock->getLogin());
        $this->setPassword($stock->getPass());
        $this->auth();
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    private function getClient()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
        ]);
        return $this->client;
    }

    private function auth()
    {
        try {
            $res = $this->getClient()->request('POST', self::URL_AUTH, [
                RequestOptions::JSON => ['username' => $this->username, 'password' => $this->password]
            ]);

            $json = json_decode($res->getBody(), true);

            if ($json['data']['token']) {
                $this->token = $json['data']['token'];
            }

            if ($this->logger) {
                $this->logger->debug(sprintf('Post To %s', self::URL_AUTH));
                $this->logger->debug(sprintf('Request %s', json_encode(['username' => $this->username, 'password' => $this->password])));
                $this->logger->debug(sprintf('Answer %s', json_encode($json)));
            }

        } catch (RequestException $e) {
            //echo '<pre>';
            //var_dump($e->getMessage());
            //var_dump($e->getResponse()->getBody());

            if ($this->logger) {
                $this->logger->error(sprintf('Post To %s', $e->getMessage()));
                $this->logger->error(sprintf('Request %s', $e->getResponse()->getBody()));
            }
        }

        //return $result;
    }

    public function saveDocument($data)
    {
        return $this->exec('/site/action/save-document', $data, 'POST', false);
    }

    public function getDocument($id)
    {
        return $this->exec('/site/get/document/'.$id, [], 'GET');
    }

    public function getOrderDocuments($id)
    {
        return $this->exec('/site/get/order-documents/'.$id, [], 'GET');
    }

    public function getBatchDocuments($id)
    {
        return $this->exec('/site/get/batch-documents/'.$id, [], 'POST');
    }

    public function updateUser($data)
    {
        return $this->exec('/site/save/user', $data, 'POST');
    }

    public function getUser($userId)
    {
        return $this->exec('/site/get/user/'.$userId, []);
    }

    public function updateOrder($data)
    {
        return $this->exec('/site/save/order', $data, 'POST');
    }

    public function getOrder($orderId)
    {
        return $this->exec('/site/get/order/'.(int)$orderId, []);
    }

    public function getOrders($page = 2, $count = 100)
    {
        $offset = ($page - 1) * $count;
        return $this->exec('/site/get/orders/'.$offset.'/'.$count, ['offset' => $offset, 'limit' => $count]);
    }

    public function getInfoOrder($orderId)
    {
        return $this->exec('/site/info/order/'.(int)$orderId, []);
    }

    public function getCustomerOrder($orderId)
    {
        return $this->exec('/site/info/order/'.(int)$orderId, []);
    }

    public function updateBatch($data)
    {
        return $this->exec('/site/action/batch-assign', $data, 'POST');
    }

    public function getBatch($id)
    {
        return $this->exec('/site/info/batch/'.(int)$id, [], 'GET');
    }

    public function reserve($data)
    {
        return $this->exec('/site/action/reserve', $data, 'POST');
    }

    public function getAuctionLots($params)
    {
        return $this->exec('/site/action/auction', $params, 'POST');
    }

    public function getEvents()
    {
        return $this->exec('/site/get/site-async', []);
    }

    public function getWeight($price, $params = [])
    {
        $vat = empty($params['vat']) ? 0 : 1;
        return $this->exec('/site/get/auction-weight/'.$price.'/'.$vat.'/'.$params['route'], $params, 'GET');
    }

    public function orderSuspend($params)
    {
        return $this->exec('/site/action/order-suspend', $params, 'POST');
    }

    public function batchPaid($data)
    {
        return $this->exec('/site/action/batch-paid', $data, 'POST');
    }

    public function removeBatch($id)
    {
        return $this->exec('/site/action/batch-reject', ['batch' => $id], 'POST');
    }

    public function confirmOverload($data = [])
    {
        return $this->exec('/site/action/confirm-outdated', $data, 'POST');
    }

    public function getOverloadPallets()
    {
        return $this->exec('/site/get/outdated-pallets', [], 'GET');
    }

    public function reserveOutdated($data)
    {
        return $this->exec('/site/action/reserve-outdated', $data, 'POST');
    }

    public function getCities($data = [])
    {
        return $this->exec('/site/get/city', $data);
    }

    public function getRoutes($data = [])
    {
        return $this->exec('/site/get/route', $data);
    }

    public function getPrices($id)
    {
        return $this->exec('/site/get/price/'.$id, []);
    }

    public function getWarehouses($data = [])
    {
        return $this->exec('/site/get/warehouse', $data);
    }

    public function getWarehouseWorkingHours($id)
    {
        return $this->exec('/site/get/warehouse-working-hours/'.$id, []);
    }

    public function getWarehouseWorkingExclusion($id)
    {
        return $this->exec('/site/get/warehouse-working-exclusion/'.$id, []);
    }

    public function updateCity($data = [])
    {
        return $this->exec('/site/save/city', $data, 'POST');
    }

    public function updateRoute($data = [])
    {
        return $this->exec('/site/save/route', $data, 'POST');
    }

    public function updateRoutePrice($data)
    {
        return $this->exec('/site/save/price', $data, 'POST');
    }

    public function updateWarehouse($data = [])
    {
        return $this->exec('/site/save/warehouse', $data, 'POST');
    }

    public function updateWarehouseWorkingHour($data = [])
    {
        return $this->exec('/site/save/warehouse-working-hours', $data, 'POST');
    }

    public function updateWarehouseWorkingExclusion($data = [])
    {
        return $this->exec('/site/save/warehouse-working-exclusion', $data, 'POST');
    }

    public function removeCity($id)
    {
        return $this->exec('/site/save/del-city', ['id' => $id], 'POST');
    }

    public function removeRoute($id)
    {
        return $this->exec('/site/action/del-route', ['id' => $id], 'POST');
    }

    public function removeRoutePrice($id)
    {
        return $this->exec('/site/action/del-price', ['id' => $id], 'POST');
    }

    public function removeWarehouse($id)
    {
        return $this->exec('/site/action/del-warehouse', ['id' => $id], 'POST');
    }

    public function removeWarehouseWorkingHour($id)
    {
        return $this->exec('/site/action/del-warehouse-working-hours', ['id' => $id], 'POST');
    }

    public function removeWarehouseWorkingExclusion($id)
    {
        return $this->exec('/site/action/del-warehouse-working-exclusion', ['id' => $id], 'POST');
    }

    public function saveActor($data)
    {
        return $this->exec('/site/save/actor', $data, 'POST');
    }

    public function getActor($data)
    {
        return $this->exec('/site/get/actor', $data, 'POST');
    }

    /* Методы склада*/
    public function whOrderAccepted($data = [])
    {
        return $this->exec('/wh/save/order-accepted', $data, 'POST');
    }

    public function whNewOrderAccepted($data = [])
    {
        return $this->whOrderAccepted($data);
    }

    public function whOrderRejected($data = [])
    {
        return $this->exec('/wh/action/order-rejected', $data, 'POST');
    }

    public function whCargoPalletted($data = [])
    {
        return $this->exec('/wh/action/cargo-palletted', $data, 'POST');
    }

    public function whBatchLoaded($data = [])
    {
        return $this->exec('/wh/action/batch-loaded', $data, 'POST');
    }

    public function whBatchRejected($data = [])
    {
        return $this->exec('/wh/action/batch-rejected', $data, 'POST');
    }

    public function whBatchArrived($data = [])
    {
        return $this->exec('/wh/save/batch-arrived', $data, 'POST');
    }

    public function whCargoUnpalletted($data = [])
    {
        return $this->exec('/wh/action/cargo-unpalletted', $data, 'POST');
    }

    public function whOrderDelivered($data = [])
    {
        return $this->exec('/wh/action/order-delivered', $data, 'POST');
    }

    public function whSaveDocument($data)
    {
        return $this->exec('/wh/action/save-document', $data, 'POST', false);
    }

    public function whGetDocument($id)
    {
        return $this->exec('/wh/get/document/'.$id, [], 'GET');
    }

    public function whGetOrderDocuments($id)
    {
        return $this->exec('/wh/get/order-documents/'.$id, [], 'GET');
    }

    public function whGetBatchDocuments($id)
    {
        return $this->exec('/wh/get/batch-documents/'.$id, [], 'GET');
    }

    public function exec($requestUrl, array $data, $requestType = 'GET', $saveLog = true)
    {
        try {
            $this->auth();

            $headers = [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept'        => 'application/json',
            ];

            $res = $this->getClient()->request($requestType, $requestUrl, [
                RequestOptions::JSON => $data,
                'headers' => $headers
            ]);

            $answerData = json_decode($res->getBody(), true);

            if ($this->logger && $saveLog) {
                $this->logger->debug(sprintf('Post To %s', $requestUrl));
                $this->logger->debug(sprintf('Request %s', json_encode($data)));
                $this->logger->debug(sprintf('Answer %s', json_encode($answerData)));
            }


            if ($this->isErrorResponse($answerData)) {
                return null;
            }

            return $answerData;

        } catch (RequestException $e) {

            if ($this->logger && $saveLog) {
                $this->logger->error(sprintf('Post To %s', $requestUrl));
                //$this->logger->error(sprintf('Request %s', json_encode($data)));
                $this->logger->error(sprintf('Answer %s', $e->getMessage()));
                $this->logger->error(sprintf('Answer %s', (string)$e->getResponse()->getBody()));

                die();
            }

            //echo '<pre>';
            //var_dump($e->getMessage());
            //var_dump($requestUrl);
            //var_dump((string)$e->getResponse()->getBody());
//
            //die();

            return null;
        }
    }

    private function isErrorResponse($data)
    {
        if (isset($data['error']) && !$data['error']) {
            return true;
        }

        return false;
    }
}
