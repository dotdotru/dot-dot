<?php
namespace App\Service;

use App\Admin\PackageTypeAdmin;
use App\Entity\Direction;
use App\Entity\Event\Event;
use App\Entity\Lot;
use App\Entity\Organization\OrganizationType;
use App\Entity\Package;
use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Pallet;
use App\Entity\Size;
use App\Entity\Stock;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use App\Entity\StockPricePerKilo;
use Symfony\Component\DependencyInjection\Container;
use DateTime;

class CustomerCalcService
{
    /**
     * @var Container $container
     */
    private $container;

    /** @var EntityManager  */
    private $entityManager;

    /** @var EventRepository  */
    private $settings;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
    }

    public function getSettings()
    {
        $settings = $this->container->get('settings');
        return $settings->group('customer_calc');
    }

    public function getDirections()
    {
        $repository = $this->entityManager->getRepository(Direction::class);
        return $repository->getAllArray();
    }

    public function getPackageTypes()
    {
        $repository = $this->entityManager->getRepository(PackageType::class);
        return $repository->getAllArray();
    }

    public function getPackings()
    {
        $repository = $this->entityManager->getRepository(Packing::class);
        return $repository->getAllArray();
    }

    public function getStocks()
    {
        $repository = $this->entityManager->getRepository(Stock::class);
        return $repository->getAllArray();
    }

    public function getOrganizationTypes()
    {
        $repository = $this->entityManager->getRepository(OrganizationType::class);
        return $repository->getAllArray();
    }
}