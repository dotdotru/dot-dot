<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Form\UserType;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use SMSCenter\SMSCenter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Session;

class TrackService
{
    const SETTING_GROUP_TRACK = 'track';
    const TRACK_CODE = 'track_code';

    /** @var null|SmsService */
    private $smsService = null;

    /** @var null|Settings */
    private $settings = null;

    /** @var null|Session */
    private $session = null;

    /** @var null|EntityManager */
    private  $entityManager = null;

    public function __construct(
        SmsService $smsService,
        Settings $settings,
        Session $session,
        EntityManager $entityManager
    )
    {
        $this->smsService = $smsService;
        $this->settings = $settings;
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function sendSms($phone)
    {
        $settings = $this->settings->group(self::SETTING_GROUP_TRACK);

        $code = $this->generateCode();
        $smsText = str_replace('#CODE#', $code, $settings['sms']);

        $this->session->set(self::TRACK_CODE, $code);
        $this->smsService->send($phone, $smsText);

        return $code;
    }

    public function checkSmsCode($code)
    {
        if (trim($code) == $this->session->get(self::TRACK_CODE)) {
            return true;
        }

        return false;
    }

    private function generateCode()
    {
        return $rand = rand('1000','9999');
    }

    public function isValidPhone($phone)
    {
        return $this->smsService->isValidPhone($phone);
    }

    public function clearPhone($phone)
    {
        return $this->smsService->clearPhone($phone);
    }

    public function checkOrderExist($orderId)
    {
        if ($this->entityManager->getRepository(Order::class)->getById($orderId)) {
            return true;
        }

        return false;
    }

    public function getOrder($orderId)
    {
        return $this->entityManager->getRepository(Order::class)->getById($orderId);
    }

    public function checkUserExistUsername($username)
    {
		if ($this->isValidPhone($username)) {
			$username = $this->clearPhone($username);
		}
        if ($this->entityManager->getRepository(User::class)->findOneBy(['username' => $username])) {
            return true;
        }

        return false;
    }
}
