<?php

namespace App\Service\Batch;

use App\Entity\Base\StockWorkingHours;
use App\Entity\Direction;
use App\Entity\Base\Direction as BaseDirection;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;

class SlaDateService
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;


    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Если в городе 2 склада, и заказана адресная доставка, заменяем склад на ближайший к адресу
     *
     * @param Direction $direction
     * @param Stock $stock
     * @param \DateTime $sendDate
     * @return \DateTime|null
     */
    public function get (Direction $direction, Stock $stock, \DateTime $sendDate)
    {
        $deliveryHours = $this->getDeliveryHours($direction);

        if (!$deliveryHours) {
            return null;
        }

        $workingHours = $this->em->getRepository(StockWorkingHours::class)->findBy(['stock' => $stock->getExternalId()]);

        // Не считаем выходные д
        $slaDate = clone $sendDate;
        $slaDate->modify('+' . $deliveryHours . ' hours');

        // Проверяем, выпадает ли время на рабочее время склада
        for ($i = 0; $i < 7; $i++) {
            if ($this->setWorkingTime($slaDate, $workingHours)) {
                break;
            }
            $slaDate->modify('+1 days');
            $slaDate->setTime(10, 0);
        }

        return $slaDate;
    }


    protected function getDeliveryHours (Direction $direction)
    {
        $baseDirection = $this->em->getRepository(BaseDirection::class)->findOneBy(['externalId' => $direction->getExternalId()]);
        if (!$baseDirection) {
            return null;
        }

        $carrierDeliveryTime = explode(':', $baseDirection->getCarrierDeliveryTime());
        if (!$carrierDeliveryTime) {
            return null;
        }
        $carrierDeliveryHours = $carrierDeliveryTime[0];

        return $carrierDeliveryHours;
    }


    /**
     * Если это рабочий день, проверяем время работы и устанавливаем нужное
     *
     * @param $weekDay
     * @param StockWorkingHours[] $workingHours
     * @return bool
     */
    protected function setWorkingTime (\DateTime $date, $workingHours)
    {
        foreach ($workingHours as $workingHour) {

            if ($date->format('N') == $workingHour->getDow()) {
                $start = (int)$workingHour->getStartTime();
                $end = (int)$workingHour->getEndTime();

                if ($date->format('G') < $start) {
                    $date->setTime($start, 0);
                    return true;
                } if ($date->format('G') < $end) {
                    return true;
                }

                return false;
            }
        }

        return false;
    }

}
