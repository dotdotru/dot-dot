<?php

namespace App\Service\Batch;

use App\Entity\BatchMile;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Wms\WmsOrder;
use App\Service\FileUploadService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Templating\EngineInterface;
use Vsavritsky\SettingsBundle\Service\Settings;

class BatchMoverGenerateDocs
{


    /** @var EntityManager|null  */
    protected $entityManager = null;

    /** @var FileUploadService|null  */
    protected $fileUploadService = null;

    /** @var null|EngineInterface  */
    protected $twig = null;


    public function __construct(
        EntityManager $entityManager,
        FileUploadService $fileUploadService,
        EngineInterface $twig,
        $knpSnappy,
        $projectDir,
        Settings $settings
    ) {
        $this->entityManager = $entityManager;
        $this->fileUploadService = $fileUploadService;
        $this->twig = $twig;
        $this->knpSnappy = $knpSnappy;
        $this->projectDir = $projectDir;
        $this->settings = $settings;
    }


    /**
     * Генерируем документы для мильной партии
     *
     * @todo удалить, документы должны печататься
     *
     * @param Order $order
     */
    public function generate (Order $order, BatchMile $batch)
    {
        //$this->setupExpeditorDoc($order);
        //$this->setupTnDoc($order, $batch);
    }


    private function setupExpeditorDoc(Order $order)
    {
        $type = 'er';

        $pathTempalate = "site/pdf/order/$type.html.twig";

        $offerDate = $this->settings->get('offer_date');

        $dataTemplate = ['order'  => $order, 'offerDate' => $offerDate];

        $html = $this->twig->render($pathTempalate, $dataTemplate);
        $data = $this->knpSnappy->getOutputFromHtml($html);

        $localExtId = sprintf('%s%s', $type, $order->getId());

        if (is_string($data)) {
            $tmpPath = $this->projectDir.'/var/cache'.uniqid().'.pdf';
            $fs = new Filesystem();
            $fs->appendToFile($tmpPath, $data);
            $pdf = new UploadedFile($tmpPath, $localExtId . '.pdf', 'application/pdf');
        }

        $file = $this->fileUploadService->createFile('ЭР', $type, $pdf, $localExtId, $localExtId);
        $file->setLocalExternalId($localExtId);
        $order->replaceFile($file);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $this->entityManager->getRepository(WmsOrder::class)->getByExternalId($order->getId());
        if ($wmsOrder) {
            $wmsOrder->replaceFile($file);
            $this->entityManager->persist($wmsOrder);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    private function setupTnDoc(Order $order, BatchMile $batch)
    {
        $type = 'tn';

        $offerDate = $this->settings->get('offer_date');

        $pathTempalate = "site/pdf/order/$type.html.twig";
        $dataTemplate = [
            'order'  => $order,
            'offerDate' => $offerDate,
            'carrier' => $batch->getCarrier(),
        ];

        $html = $this->twig->render($pathTempalate, $dataTemplate);
        $data = $this->knpSnappy->getOutputFromHtml($html);

        $localExtId = sprintf('%s%s', $type, $order->getId());

        if (is_string($data)) {
            $tmpPath = $this->projectDir.'/var/cache'.uniqid().'.pdf';
            $fs = new Filesystem();
            $fs->appendToFile($tmpPath, $data);
            $pdf = new UploadedFile($tmpPath, $localExtId . '.pdf', 'application/pdf');
        }

        $file = $this->fileUploadService->createFile('ТН', $type, $pdf, $localExtId, $localExtId);
        $file->setLocalExternalId($localExtId);

        $order->replaceFile($file);
        $batch->replaceFile($file);


        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $this->entityManager->getRepository(WmsOrder::class)->getByExternalId($order->getId());
        if ($wmsOrder) {
            $wmsOrder->replaceFile($file);
            $this->entityManager->persist($wmsOrder);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

}