<?php

namespace App\Service\DataNormalizer;

use Dadata\Client as BaseClient;
use \GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Dadata\Response\Address;
use Dadata\Response\Vehicle;

class DadataClient extends BaseClient implements ClientInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(GuzzleClientInterface $httpClient, array $config = [])
    {
        parent::__construct($httpClient, $config);
    }

    /**
     * Suggestion type party
     *
     * @const string
     */
    protected const SUGGESTION_TYPE_PARTY = 'party';

    /**
     * Suggestion type address
     *
     * @const string
     */
    protected const SUGGESTION_TYPE_ADDRESS = 'address';

    /**
     * Suggestion type bank
     *
     * @const string
     */
    protected const SUGGESTION_TYPE_BANK = 'bank';

    /**
     * Returns suggestion data array from dadata
     *
     * @param string $type  Suggestion type
     * @param string $query Query string
     *
     * @return array
     */
    protected function suggest(string $type, string $query)
    {
        $url    = $this->prepareSuggestionsUri('suggest/' . $type);
        $params = ['query' => $query];

        $response = $this->query($url, $params);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function suggestInn(string $number):array
    {
        return $this->suggest(self::SUGGESTION_TYPE_PARTY, $number);
    }

    /**
     * {@inheritdoc}
     */
    public function suggestBik(string $number):array
    {
        return $this->suggest(self::SUGGESTION_TYPE_BANK, $number);
    }

    /**
     * {@inheritdoc}
     */
    public function suggestAddress(string $address):array
    {
        return $this->suggest(self::SUGGESTION_TYPE_ADDRESS, $address);
    }

    /**
     * {@inheritdoc}
     */
    public function checkAddress(string $address):Address
    {
        return $this->cleanAddress($address);
    }

    /**
     * {@inheritdoc}
     */
    public function checkPassport(string $passport):bool
    {
        $passportObject = $this->cleanPassport($passport);
        if($passportObject->qc!==0) {
            return false;
        }
        return true;
    }
}
