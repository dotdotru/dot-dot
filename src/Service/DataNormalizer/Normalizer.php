<?php

namespace App\Service\DataNormalizer;

use Psr\Log\LoggerInterface;
use App\Validator\InnValidator;
use App\Validator\PassportValidator;
use App\Validator\DriverLicenseValidator;

class Normalizer
{
    /** @var ClientInterface */
    protected $normolizerClient;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(ClientInterface $normolizerClient)
    {
        $this->normolizerClient = $normolizerClient;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function errorToLog(string $error):void
    {
        if ($this->logger) {
            $this->logger->error($error);
        }
    }

    /**
     * Returns suggestion data array from dadata
     *
     * @param string $number Inn number
     *
     * @return array
     */
    public function suggestInn(string $number):array
    {
        $validator = new InnValidator();
        if ($validator->isInvalid($number)) {
            return [];
        }

        $result = [];
        try {
            $innSuggest = $this->normolizerClient->suggestInn($number);
        } catch (\Exception $e) {
            $this->errorToLog($e->getMessage());
        }

        if($innSuggest && isset($innSuggest[0]) && isset($innSuggest[0]['data'])) {
            $info = $innSuggest[0]['data'];
            $result['name'] = $innSuggest[0]['value'];
            $result['name'] = trim(str_replace("ООО", "", $result['name']));
            $result['name'] = trim(str_replace("ИП", "", $result['name'])); 
            $result['name'] = str_replace("'", "", $result['name']);
            $result['name'] = str_replace('"', '', $result['name']);
            $result['kpp'] = '';
            if(isset($info['kpp'])){
                $result['kpp'] = $info['kpp'];
            }
            $result['inn'] = $info['inn'];
            $result['ogrn'] = $info['ogrn'];
            $result['ceo'] = '';
            $result['address'] = mb_strtolower($info['address']['value']);
            //INDIVIDUAL|LEGAL
            $result['type'] = $info['type'];

            // Город с большой буквы. От дадаты приходит все с большой буквы
            $addressElements = explode(',', $result['address']);

            if($addressElements) {
                foreach($addressElements as $addressElement) {
                    if (strpos($addressElement, 'город') !== false) {
                        $city = trim(str_replace('город', '', $addressElement));
                        $uCity = mb_convert_case($city, MB_CASE_TITLE, "UTF-8");
                        $result['address'] = str_replace($city, $uCity, $result['address']);
                    }
                    if (strpos($addressElement, 'г ') !== false) {
                        $city = trim(str_replace('г ', '', $addressElement));
                        $uCity = mb_convert_case($city, MB_CASE_TITLE, "UTF-8");
                        $result['address'] = str_replace($city, $uCity, $result['address']);
                    }
                }
            }

            if(isset($info['management']['name']) && isset($info['management']['post'])) {
                if($info['management']['post'] == "ГЕНЕРАЛЬНЫЙ ДИРЕКТОР") {
                    $result['ceo'] = $info['management']['name'];
                }
            }
        }

        return $result;
    }

 /**
     * Returns suggestion data array from dadata
     *
     * @param string $number - bik
     *
     * @return array
     */
    public function suggestBik(string $number):array
    {
        $result = [];
        try {
            $suggest = $this->normolizerClient->suggestBik($number);
        } catch (\Exception $e) {
            $this->errorToLog($e->getMessage());
        }

        if($suggest && isset($suggest[0]) && isset($suggest[0]['data'])) {
            $info = $suggest[0]['data'];
            $result['name'] = $suggest[0]['value'];
            $result['correspondent_account'] = $info['correspondent_account'];
            $result['registration_number'] = $info['registration_number'];
            $result['bic'] = $info['bic'];
        }

        return $result;
    }

    /**
     * Returns address suggestion
     *
     * @param string $address - address
     *
     * @return array
     */
    public function suggestAddress(string $address):array
    {
        $result = [];
        try {
            $suggests = $this->normolizerClient->suggestAddress($address);
            foreach($suggests as $suggest) {
                $suggestAddress = $suggest['value'];
                //$suggestAddress = str_replace('г Москва', 'Москва', $suggest['value']);
                //$suggestAddress = str_replace('г Санкт-Петербург', 'Питер', $suggest['value']);
                $result[] = $suggestAddress;
            }
        } catch (\Exception $e) {
            $this->errorToLog($e->getMessage());
        }

        return $result;
    }

    /**
     * Cleans address
     *
     * @param string $address address
     *
     * @return array
     */
    public function checkAddress(string $address):string
    {
        $result = [];
        try {
            $result = $this->normolizerClient->checkAddress($address);
            if($result->qc === 0  && $result->result) {
                return (string)$result->result;
            }
        } catch (\Exception $e) {
            $this->errorToLog($e->getMessage());
        }

        return '';
    }

    /**
     * Check passport
     *
     * @param string $passport
     *
     * @return bool
     */
    public function checkPassport(string $passport):bool
    {
        $result = false;
        try {
            $result = $this->normolizerClient->checkPassport($passport);
        } catch (\Exception $e) {
            $this->errorToLog($e->getMessage());
        }

        return $result;
    }


    /**
     * Check is address exists
     *
     * @param string $address address
     *
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function existsAddress(string $address):bool
    {
        $exists = false;
        try {
            $dadataAddress = $this->normolizerClient->checkAddress($address);

            // адрес существует, когда указан дом
            if ($dadataAddress && !empty($dadataAddress->house)) {
                $exists = true;
            }
        } catch (\Exception $e) {
            // Если dadata не отвечает
            $this->errorToLog($e->getMessage());
            return true;
        }

        return $exists;
    }

}
