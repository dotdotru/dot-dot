<?php

namespace App\Service\DataNormalizer;

use Dadata\Response\Address;
use Dadata\Response\Vehicle;

interface ClientInterface
{
    /**
     * Returns suggestion for inn
     *
     * @param string $number inn
     *
     * @return array
     */
    public function suggestInn(string $number):array;

    /**
     * Returns suggestion for bik
     *
     * @param string $number bik
     *
     * @return array
     */
    public function suggestBik(string $number):array;

    /**
     * Returns suggestion for adress
     *
     * @param string $address address
     *
     * @return array
     */
    public function suggestAddress(string $address):array;

    /**
     * Cleans address
     *
     * @param string $address address
     *
     * @return Address
     */
    public function checkAddress(string $address):Address;

    /**
     * Cleans passport
     *
     * @param string $passport
     *
     * @return bool
     */
    public function checkPassport(string $passport):bool;
}
