<?php
namespace App\Service;

use App\Entity\Order;
use App\Service\Competitor\CompetitorService;
use App\Service\Mapper\Competitor\CompetitorMapper;

class CompetitorCalcService
{
    /** @var CompetitorService[]*/
    private $competitors;

    /** @var CompetitorMapper[] */
    private $mappers;

    public function addCompetitor(CompetitorService $service)
    {
        $this->competitors[] = $service;
    }

    public function addMapper($mapper)
    {
        $this->mappers[] = $mapper;
    }

    public function calc(Order $order)
    {
        $result = [];

        $index = 0;
        foreach ($this->competitors as $competitor) {
            $index++;
            foreach ($this->mappers as $mapper) {
                if ($mapper->isValid($competitor)) {
                    $price = $competitor->calc($mapper->map($order));
                    $result[] = ['index' => $index, 'name' => $competitor->getName(), 'price' => floor($price)];
                }
            }
        }

        return $result;
    }
}
