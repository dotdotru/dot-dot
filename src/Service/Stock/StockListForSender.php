<?php


namespace App\Service\Stock;


use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Stock;
use Doctrine\ORM\EntityManager;

class StockListForSender
{

    /** @var EntityManager  */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Формируем список складов, доступных для отправителя
     *
     * @param Stock $workerStock
     * @return mixed
     */
    public function getStocks(User $user)
    {
        $stocksArray = $this->em->getRepository(Stock::class)->getAllArray();

        // Проверяем специальные группы отправителя, если они есть, показываем только правильные склады
        // @todo Если это останется, проставить всем складам актуальные группы и исправить этот код, пока группа будет проставленна только у FTL складов
        $groupIds = [];
        foreach ($user->getFtlGroups() as $group) {
            $groupIds[] = $group->getId();
        }

        if (count($groupIds)) {
            foreach ($stocksArray as $key => $stockArray) {
                $founded = false;
                foreach ($stockArray['groups'] as $group) {
                    if (in_array($group['id'], $groupIds)) {
                        $founded = true;
                    }
                }

                if (!$founded) {
                    unset($stocksArray[$key]);
                }
            }
        } else {
            foreach ($stocksArray as $key => $stockArray) {
                if (!empty($stockArray['groups'])) {
                    unset($stocksArray[$key]);
                }
            }
        }

        $stocksArray = array_map(function ($item) { return $item; }, $stocksArray);

        return $stocksArray;
    }

}