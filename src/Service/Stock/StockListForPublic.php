<?php


namespace App\Service\Stock;


use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Stock;
use Doctrine\ORM\EntityManager;

class StockListForPublic
{

    /** @var EntityManager  */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Список кладов в открытом доступе
     *
     * @param Stock $workerStock
     * @return mixed
     */
    public function getStocks()
    {
        $stocksArray = $this->em->getRepository(Stock::class)->getAllArray();
        $publicStocks = [];

        // Доступны склады без группы
        // @todo Если это останется, проставить всем складам актуальные группы и исправить этот код, пока группа будет проставленна только у FTL складов
        foreach ($stocksArray as $key => $stockArray) {
            if (empty($stockArray['groups'])) {
                $publicStocks[] = $stockArray;
            }
        }

        return $publicStocks;
    }

}