<?php


namespace App\Service\Stock;

use App\Entity\Stock;
use Doctrine\ORM\EntityManager;


class StockListForWorker
{

    /** @var EntityManager  */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Формируем список складов, доступных для данного складского
     *
     * @param Stock $workerStock
     * @return mixed
     */
    public function getStocks(Stock $workerStock)
    {
        $stocksArray = $this->em->getRepository(Stock::class)->getAllArray();

        // Если у склада складского заданна группа, показываем только склады из этой группы, если нет, показываем только склады без групп
        // @todo Если это останется, проставить всем складам актуальные группы и исправить этот код, пока группа будет проставленна только у FTL складов
        if (count($workerStock->getGroups())) {
            $groupIds = [];

            foreach ($workerStock->getGroups() as $group) {
                $groupIds[] = $group->getId();
            }

            foreach ($stocksArray as $key => $stockArray) {
                $founded = false;
                foreach ($stockArray['groups'] as $group) {
                    if (in_array($group['id'], $groupIds)) {
                        $founded = true;
                    }
                }

                if (!$founded) {
                    unset($stocksArray[$key]);
                }
            }
        } else {

            foreach ($stocksArray as $key => $stockArray) {
                if (!empty($stockArray['groups'])) {
                    unset($stocksArray[$key]);
                }
            }
        }

        $stocksArray = array_map(function ($item) { return $item; }, $stocksArray);

        return $stocksArray;
    }

}