<?php


namespace App\Service\Stock;


use App\Entity\City;
use App\Entity\Order;
use App\Entity\Stock;
use App\Service\CalcDistanceService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class NearestStockForAddress
{

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var CalcDistanceService
     */
    protected $calcDistance;


    public function __construct(EntityManagerInterface $em, CalcDistanceService $calcDistance)
    {
        $this->em = $em;
        $this->calcDistance = $calcDistance;
    }


    /**
     * Ищем в городе ближайший склад для адреса
     *
     * @param $address
     * @param City $city
     * @return Stock|null
     */
    public function getNearestStock ($address, City $city)
    {
        $stockRepository = $this->em->getRepository(Stock::class);
        /** @var Stock[] $cityStocks */
        $cityStocks = $stockRepository->findBy(['city' => $city, 'published' => true]);

        // нужно только если складов больше 1
        if (count($cityStocks) <= 1) {
            return null;
        }

        $minDistance = 0;
        $nearestStock = null;
        foreach ($cityStocks as $cityStock) {
            // @todo сделать рефакторинг групп склада
            if (count($cityStock->getGroups()) || !$cityStock->getAddress()) {
                continue;
            }

            $distance = $this->calcDistance->calcDistance($address, $cityStock->getAddress());

            if (!$nearestStock || $distance < $minDistance) {
                $minDistance = $distance;
                $nearestStock = $cityStock;
            }
        }

        return $nearestStock;
    }

}