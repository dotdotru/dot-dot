<?php

namespace App\Service;

use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Cache\ItemInterface;

////https://maps.googleapis.com/maps/api/directions/json?origin=%D0%B3%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,%20%D1%83%D0%BB%20%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%BE%D0%B2%D0%BA%D0%B0%20%D0%91.,%20%D0%B4%2015&destination=%D0%9C%D0%BE%D1%81%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F%20%D0%BE%D0%B1%D0%BB.,%20%D0%A1%D0%BE%D0%BB%D0%BD%D0%B5%D1%87%D0%BD%D0%BE%D0%B3%D0%BE%D1%80%D1%81%D0%BA%D0%B8%D0%B9%20%D1%80-%D0%BD,%20%D0%B4.%20%D0%A7%D1%91%D1%80%D0%BD%D0%B0%D1%8F%20%D0%93%D1%80%D1%8F%D0%B7%D1%8C,%20%D1%83%D0%BB.%20%D0%9F%D1%80%D0%BE%D0%BC%D1%8B%D1%88%D0%BB%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F,%20%D1%81%D1%82%D1%80.%202&alternatives=true&sensor=false&key=AIzaSyCLqawvvNm0Oj1Nt8yaQ-CdvQ1190oPaug

class CalcDistanceService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var AdapterInterface
     */
    protected $cache;


    const URL_DIRECTIONS = '/maps/api/distancematrix/json?';


    public function __construct(Client $client, AdapterInterface $cache)
    {
        $this->getClient();
        $this->cache = $cache;
    }

    private function getClient()
    {
        $this->client = new Client([
            'base_uri' => 'https://maps.googleapis.com',
        ]);
        return $this->client;
    }

    public function calcDistance($addressFrom, $addressTo)
    {
        $distance = null;

        $params = [];
        $params['origins'] = $addressFrom;
        $params['destinations'] = $addressTo;
        $params['units'] = 'metric';
        $params['mode'] = 'driving';
        $params['avoid'] = 'tolls';
        $params['departure_time'] = time() + 60; // если передать время, гугл начинает передавать кратчайшее растояние
        $params['key'] = 'AIzaSyCLqawvvNm0Oj1Nt8yaQ-CdvQ1190oPaug';


        /**
        https://maps.googleapis.com/maps/api/directions/json?
        origin=Toronto&destination=Montreal
        &key=YOUR_API_KEY
         */


        try {
            $res = $this->getClient()->request('GET', self::URL_DIRECTIONS.http_build_query($params));
            $json = json_decode($res->getBody(), true);

            if ($json['rows'][0]['elements'][0]['status'] == 'OK') {
                $distance = $json['rows'][0]['elements'][0]['distance']['value'];
            }
        } catch (RequestException $e) {
            echo '<pre>';
            var_dump($e->getMessage());
            var_dump($e->getResponse()->getBody());
        }

        return $distance;
    }


    /**
     * @param $from
     * @param $to
     * @return mixed|null
     * @throws \GuzzleHttp\Exception\GuzzleException*@throws \Psr\Cache\InvalidArgumentException
     */
    public function getDirection($from, $to)
    {
        $distance = null;

        $params = [];
        $params['origin'] = $from;
        $params['destination'] = $to;
        $params['units'] = 'metric';
        $params['mode'] = 'driving';
        $params['avoid'] = 'tolls';
        $params['key'] = 'AIzaSyCLqawvvNm0Oj1Nt8yaQ-CdvQ1190oPaug';


        $cacheKey = 'geo_' . md5(json_encode($params));

        $json = $this->cache->get($cacheKey, function (ItemInterface $item) use ($params) {
            $item->expiresAfter(24 * 3600);

            // если передать время, гугл начинает передавать кратчайшее растояние
            $params['departure_time'] = time() + 60;

            $res = $this->getClient()->request('GET', "/maps/api/directions/json?" . http_build_query($params));
            $json = json_decode($res->getBody(), true);
            $json['request']['travelMode'] = ['DRIVING'];


            return $json;
        });

        // Декодируем, чтобы использовать в яваскрипте
        foreach ($json['routes'][0]['legs'][0]['steps'] as $i => $step) {
            $tmp = Polyline::decode($step['polyline']['points']);
            $json['routes'][0]['legs'][0]['steps'][$i]['path'] = [];
            for ($k = 0; $k < count($tmp); $k += 2) {
                $json['routes'][0]['legs'][0]['steps'][$i]['path'][] = ['lat' => $tmp[$k], 'lng' => $tmp[$k + 1]];
            }
        }

        return $json;
    }



}
