<?php

namespace App\Service\Insurance;


use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Base\Direction;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Entity\Trace\InsuranceTrace;
use App\Entity\Trace\Log;

class InsuranceService extends AbstractService
{

    protected $request = 'GETCERTIFICATEJSON_:mode';


    /**
     * Страхуем заказ, если заказ уже застрахован, обновляет страховой полюс
     *
     * @param Order $order
     * @return bool|mixed|string
     * @throws \Exception
     */
    public function insurance(Order $order)
    {
        // Не страхуем грузы без объявленной ценности
        if (!$order->getDeclaredPrice()) {
            return null;
        }
        if (!in_array($order->getStatus(), [Order::STATUS_WAITING, Order::STATUS_ACCEPTED])) {
            return null;
        }
        if (!$order->getCustomer()) {
            return null;
        }
        if ($order->getInsured()) {
            return null;
        }

        $params = [];
        $params["PDATA-CLOB-IN"] = $this->getJson($order);
        $params["PTYPE-NUMBER-IN"] = self::REQUEST_TYPE_POLISE;
        $params["PDATAOUT-XMLTYPE-OUT"] = '';

        $response = $this->exec($params);

        $this->log($order);

        if (!$response) {
            return false;
        }

        $order->setInsured(true);
        if (!empty($response['code'])) {
            $order->setInsuredCode($response['code']);
        }
        $order->setInsuredNumber(!empty($response['number']) ? $response['number'] : '');
        $order->setInsuredPdf(!empty($response['certificate']) ? 'data:application/pdf;base64,' . base64_encode(gzuncompress(base64_decode($response['certificate']))) : '');


        if (!$order->getInsuredDeclaredPrice()) {
            $order->setInsuredDeclaredPrice($order->getDeclaredPrice());
        }

        return $response;
    }


    /**
     * @param Order $order
     *
     * @return false|string|null
     *
     * @throws \Exception
     */
    protected function getJson(Order $order)
    {
        if (!$order->getInsuredDate()) {
            $order->setInsuredDate(new \DateTime());
        };
        $dateStart = $order->getInsuredDate();
        $dateEnd = (clone $dateStart)->modify('+19 days');

        $data = [
            'orderId' => $order->getId(),
            'declaredPrice' => (int) ($order->getInsuredDeclaredPrice() ? $order->getInsuredDeclaredPrice() : $order->getDeclaredPrice()),
            'date' => $dateStart->format('Y-m-d\TH:i:s'),
            'dateend' => $dateEnd->format('Y-m-d\TH:i:s'),
            'compress' => 1,
            'key' => $this->key
        ];


        if ($order->getDirection()) {
            $baseDirection = $this->entityManager->getRepository(Direction::class)->findOneByExternalId($order->getDirection()->getExternalId());
            list($h, $i, $s) = explode(':', $baseDirection->getCustomerDeliveryTime());
            $data['deliveryTime'] = (int)$h;
        }

        /** @var User $sender */
        $sender = $order->getCustomer();

        if (!$sender->isPrivatePerson()) {
            /** @var OrganizationRepository $organizationRepository */
            $organizationRepository = $this->entityManager->getRepository(Organization::class);
            $organization = $organizationRepository->getByUser($sender);

            if (!$organization) {
                return null;
            }

            $data['policyowner'] = [
                'juridical' => (int)!$sender->isPrivatePerson(),
                'name' => $organization->getCompanyName(),
                'type' => $organization->getType(),
                'inn' => $organization->getInn()
            ];
        } else {
            $data['policyowner'] = [
                'juridical' => (int)!$sender->isPrivatePerson(),
                'name' => $sender->getFirstname(),
                'number' => $sender->getNumber(),
                'series' => $sender->getSeries()
            ];
        }

        $routes = [];

        $shipping = $order->getShipping(Shipping::TYPE_PICKUP);
        $routes[] = [
            'id' => $order->getStockFrom()->getId(),
            'address' => $shipping->isActive() ? $shipping->getAddress() : $order->getStockFrom()->getAddress()
        ];

        $shipping = $order->getShipping(Shipping::TYPE_DELIVER);
        $routes[] = [
            'id' => $order->getStockTo()->getId(),
            'address' => $shipping->isActive() ? $shipping->getAddress() : $order->getStockTo()->getAddress()
        ];

        $data['route'] = $routes;

        $cargo = [
            "status" => "new",
            "name" => implode(',', $order->getPackagesTypes()),
            "class" => "3900", // Другие грузы
            "packing" => "Стандартная",
            "weight" => $order->getTotalWeight(),
            "count" => $order->getTotalCount()
        ];

        $data['cargo'] = $cargo;

        return json_encode($data);
    }


    /**
     * @param $order
     */
    protected function log ($order)
    {
        $insuranceTraceRepository = $this->entityManager->getRepository(InsuranceTrace::class);
        $insuranceTrace = $insuranceTraceRepository->findOneBy(['orderCustomer' => $order]);

        if (!$insuranceTrace) {
            $insuranceTrace = new InsuranceTrace();
            $insuranceTrace->setOrder($order);
            $this->entityManager->persist($insuranceTrace);
        }

        $log = new Log();
        $log->setRequest($this->getLastRequest());
        $log->setResponse($this->getLastResponse());
        $insuranceTrace->addItem($log);
        $this->entityManager->persist($log);
    }

}
