<?php

namespace App\Service\Insurance;

use SoapClient;
use SoapFault;
use SimpleXMLElement;
use Doctrine\ORM\EntityManager;

class AbstractService
{

    const REQUEST_TYPE_DRIVER = 1; // водитель
    const REQUEST_TYPE_POLISE = 2; // полис

    /**
     * @var SoapClient
     */
    private $client;

    /**
     * Какой запрос будет отправлен
     * @var string
     */
    protected $request = null;

    /**
     * @var string
     */
    protected $key = '';

    /**
     * @var bool
     */
    protected $testMode = true;


    protected $wsdlPath = '';


    protected $entityManager;


    public function __construct($wsdlPath, EntityManager $entityManager, $login = null, $password = null, $key = '', $testMode = true)
    {
        $this->wsdlPath = $wsdlPath;
        $this->params = [
            'login' => $login,
            'password' => $password,
            'trace' => true,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
        ];

        $this->testMode = $testMode;

        $this->key = $key;
        $this->entityManager = $entityManager;
    }




    public function getRequest()
    {
        if (!$this->request) {
            throw new \Exception('Request does not specified');
        }
        return str_replace(':mode', ($this->testMode ? 'TEST' : 'WORK'), $this->request);
    }



    public function getLastRequest()
    {
        if ($this->client) {
            return $this->client->__getLastRequest();
        }

        return '';
    }

    public function getLastResponse()
    {
        if ($this->client) {
            return $this->client->__getLastResponse();
        }

        return '';
    }



    public function getClient ()
    {
        if (!$this->client) {
            $this->client = new SoapClient($this->wsdlPath . $this->getRequest() . '?wsdl', $this->params);
        }

        return $this->client;
    }


    protected function exec ($params)
    {
        $client = $this->getClient();

        try{
            $result = $client->{$this->getRequest()}($params);
        } catch (SoapFault $e) {
            // TODO:: Оправить сообщение админу
            return false;
        }

        $pdataout = $result->PDATAOUT;
        $pdataout = $pdataout->any;
        $pdataout = trim(strip_tags($pdataout, "xml"), ' "');

        $pdataout = json_decode(str_replace("\n", "\\\\n", $pdataout), true);
        dump($pdataout);
        $pdataout['certificate'] = trim(str_replace("\\n", "\n", $pdataout['certificate']), '" ');

        return $pdataout;
    }










}