<?php


namespace App\Service\Order;


use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Order\CalcCostRange;

class RecalculateOrderPriceService
{

    /**
     * @var CallBaseService
     */
    protected $caller;

    public function __construct(CallBaseService $caller)
    {
        $this->caller = $caller;
    }

    /**
     * Пересчитываем цены и устанавливаем их в заказ
     *
     * @param Order $order
     */
    public function recalculate(OrderInterface $order, $vat = null)
    {
        $calcRequest = new CalcCostRange();
        $calcRequest->setOrder($order);
        $calcRequest->setVat($vat);

        // Считаем цену сначала без промокода, потом с промокодом
        $orderCostRange = $this->caller->exec($calcRequest)['data'];

        // Актуальные цены
        $order->setMinPrice($orderCostRange['cost_min']);
        $order->setMaxPrice($orderCostRange['cost_max']);


        // @todo Подумать над оптимизацией, так как в WmsOrder нет таких полей
        if ($order instanceof  Order) {
            // Сохраним цены для заказов с промокодами и без промокодов
            if (!$order->getPromocode()) {
                $order->setMinPriceWithoutPromocode($orderCostRange['cost_min']);
                $order->setMaxPriceWithoutPromocode($orderCostRange['cost_max']);

                $order->setMinPriceWithPromocode($orderCostRange['cost_min']);
                $order->setMaxPriceWithPromocode($orderCostRange['cost_max']);
            } else {
                // Если промокод задан, мы уже и так получили цены с промокодом
                $order->setMinPriceWithPromocode($orderCostRange['cost_min']);
                $order->setMaxPriceWithPromocode($orderCostRange['cost_max']);

                // Посчитаем  цену без промокода
                $calcRequest->setNotUsePromocode(true);
                $orderCostWithoutPromocodeRange = $this->caller->exec($calcRequest)['data'];

                $order->setMinPriceWithoutPromocode($orderCostWithoutPromocodeRange['cost_min']);
                $order->setMaxPriceWithoutPromocode($orderCostWithoutPromocodeRange['cost_max']);
            }
        }
    }


}