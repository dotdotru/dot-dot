<?php

namespace App\Service\Order;

use App\Entity\Order;
use App\Entity\Promocode;
use App\Repository\OrderRepository;
use App\Application\Sonata\UserBundle\Entity\User;

use App\Service\Integration\CallBaseService;
use App\Service\Integration\Order\CalcCostRange;

use App\Service\Order\RecalculateOrderPriceService;
use App\Service\OrderUpdateService;
use App\Service\PromocodeService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Vsavritsky\SettingsBundle\Service\Settings;
use Symfony\Component\DependencyInjection\Container;

class RearrangePromocodeService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PromocodeService $promoCodeService
     */
    private $promoCodeService;

    /**
     * @var OrderUpdateService
     */
    protected $orderUpdateService;


    public function __construct(PromocodeService $promoCodeService, EntityManager $entityManager, RecalculateOrderPriceService $recalculateOrder)
    {
        $this->entityManager = $entityManager;
        $this->promoCodeService = $promoCodeService;
        $this->recalculateOrder = $recalculateOrder;
    }


    /**
     * Ищем у пользователя заказ с промокодом, если такого заказа нет, добавим первому подходящему заказу промомокод
     *
     * @param User $user
     * @return Order|null|array
     */
    public function rearrange(User $user)
    {
        /** @var OrderRepository $orderCustomerRepository */
        $orderCustomerRepository = $this->entityManager->getRepository(Order::class);

        $firstOrderPromocode = $this->promoCodeService->getFirstOrderPromocode();

        $orders = $this->orderCustomerRepository->getByUserForPromocode($user);

        /**
         * Проверяем, есть ли уже хоть один примененный промокод
         * Если нет, берем первый подходящий заказ, применяем промокод и пересчитываем его
         * @todo переделать запрос, чтобы он возвращал флаг
         */
        $promocodeExists = false;
        foreach($orders as $order) {
            if ($order->getPromocode() && ($order->getPromocode()->getType() == Promocode::TYPE_FIRSTREGISTER) && $order->isRemove() == false) {
                $promocodeExists = true;
            }
        }

        if ($promocodeExists) {
            return ['error' => 'no_applicable_order'];
        }

        /** @var Order $applicableOrder */
        $applicableOrder = $orderCustomerRepository->findOneBy(
            [
                'user' => $user,
                'promocode' => null,
                'remove' => false,
                'status' => [
                    Order::STATUS_WAITING,
                    Order::STATUS_BOOKED
                ]
            ],
            ['id' => 'asc']
        );

        // @todo проверяем что нет примененного промокода
        if ($applicableOrder) {
            $applicableOrder->setPromocode($firstOrderPromocode);
            $applicableOrder->setAutoAddPromocode(true);

            $this->recalculateOrder->recalculate($applicableOrder);

            try {
                $this->entityManager->persist($applicableOrder);
                $this->promocodeOrderPriceService->calculate($applicableOrder);

                // Если к заказу в статусе waiting применился промокод, синхронизиуем с wms
                if ($applicableOrder->isWaiting()) {
                    $this->orderUpdateService->orderSynch($applicableOrder);
                }

                return $applicableOrder;
            } catch(ORMException $e) {
                return ['error' => 'update_error'];
            }
        }

        return ['error' => 'no_applicable_order'];
    }
}
