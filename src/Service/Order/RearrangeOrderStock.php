<?php

namespace App\Service\Order;

use App\Entity\Order;
use App\Service\Stock\NearestStockForAddress;

class RearrangeOrderStock
{

    /**
     * @var NearestStockForAddress
     */
    protected $nearestStockForAddress;


    /**
     * RearrangeOrderStock constructor.
     * @param NearestStockForAddress $nearestStockForAddress
     */
    public function __construct(NearestStockForAddress $nearestStockForAddress)
    {
        $this->nearestStockForAddress = $nearestStockForAddress;
    }

    /**
     * Если в городе 2 склада, и заказана адресная доставка, заменяем склад на ближайший к адресу
     *
     * @param Order $order
     * @return bool
     */
    public function rearrange (Order $order)
    {
        $stockChanged = false;

        $pickuUpShipping = $order->getShippingPickUp();
        if ($pickuUpShipping && $pickuUpShipping->isActive()) {
            $nearestStock = $this->nearestStockForAddress->nearestStock($pickuUpShipping->getAddress(), $order->getStockFrom()->getCity());
            if ($nearestStock) {
                $stockChanged = true;
                $order->setStockFrom($nearestStock);
                $pickuUpShipping->setStock($nearestStock);
            }
        }

        $deliverShipping = $order->getShippingDeliver();
        if ($deliverShipping && $deliverShipping->isActive()) {
            $nearestStock = $this->nearestStockForAddress->nearestStock($deliverShipping->getAddress(), $order->getStockTo()->getCity());
            if ($nearestStock) {
                $stockChanged = true;
                $order->setStockTo($nearestStock);
                $deliverShipping->setStock($nearestStock);
            }
        }

        return $stockChanged;
    }


}