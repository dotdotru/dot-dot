<?php

namespace App\Service\Order;

use App\Entity\Notification\NotificationQueue;
use App\Entity\OrderInterface;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Repository\Organization\OrganizationRepository;
use App\Service\MailService;
use App\Service\SmsService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;


class OrderNotificationService
{

    public function __construct(
        EntityManagerInterface $entityManager,
        MailService $mailService,
        SmsService $smsService,
        $notificationMail = null
    ) {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->notificationMail = $notificationMail;
    }


    /**
     * Отправка уведомления о необходимости оплаты
     *
     * @param OrderInterface $order
     */
    public function notificationNeedPay(OrderInterface $order)
    {
        $id = $order->getId();

        // Отправка уведомления бугалтеру
        $this->mailService->registerEmailHtmlTemplate(
            'order/need_to_pay_inside.html.twig',
            'Заказ №' . $id . ' ожидает оплаты',
            'av@dot-dot.ru', // @todo вынести в конфиг
            ['order' => $order]
        );


    }


}
