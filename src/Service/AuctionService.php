<?php
namespace App\Service;

use App\Entity\Direction;
use App\Entity\Event\Event;
use App\Entity\Event\LotGenerateEvent;
use App\Entity\Lot;
use App\Entity\Pallet;
use App\Entity\Size;
use App\Entity\Stock;
use App\Repository\EventRepository;
use App\Repository\LotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use App\Entity\StockPricePerKilo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\DependencyInjection\Container;
use DateTime;

class AuctionService
{
    /**
     * @var Container $container
     */
    private $container;

    /** @var EntityManager  */
    private $entityManager;

    /** @var EventRepository  */
    private $settings;

    /** @var Direction */
    private $direction;

    /** @var Cache */
    private $cache;

    const CACHE_WEIGHTWAREHOUSES = 'WEIGHTWAREHOUSES';
    const CACHE_WEIGHTWAREHOUSES_SECONDS = 120;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine.orm.entity_manager');

        /** @var IntegrationBaseService integrationBase */
        $this->integrationBase = $this->container->get('app.integration_base');

        $this->cache = new FilesystemCache('auction', self::CACHE_WEIGHTWAREHOUSES_SECONDS, $this->container->getParameter('kernel.cache_dir'));
    }

    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function getSettings()
    {
        $settings = $this->container->get('settings');
        $auctionSettings = $settings->group('auction');

        return $auctionSettings;
    }

    public function getCacheIdWeightWarehouses($price, $vat = false)
    {
		$params['time'] = date('d.m.Y H:').(int)(date('i') / 2);
        $params['price'] = $price;
        $params['direction'] = $this->getDirection() ? $this->getDirection()->getExternalId() : null;

        $cacheId = self::CACHE_WEIGHTWAREHOUSES.$params['price'].'_'.$params['direction'].'_'.$vat;

        return $cacheId;
    }

    public function getWeightWarehouses($price = null, $vat = false)
    {
        $realPrice = $price;

        if (!$price) {
            $realPrice  = $this->getCurrentPricePerKilo($vat);
            $price = $this->getCurrentPricePerKilo($vat) / 100;
        } else {
            $price = $price / 100;
        }

        $cacheId = $this->getCacheIdWeightWarehouses($realPrice, $vat);

        $weight = 0;
        if (!$this->cache->has($cacheId)) {
            if ($this->getDirection() && $this->getDirection()->getExternalId()) {
                $result = $this->integrationBase->getWeight(
                    $price,
                    ['route' => $this->getDirection() ? $this->getDirection()->getExternalId() : null, 'vat' => $vat]
                );
                $weight = (int)$result['data'];
            }

            $this->cache->set($cacheId, $weight, self::CACHE_WEIGHTWAREHOUSES_SECONDS);
        }

        $weight = $this->cache->get($cacheId);

        return $weight;
    }

    public function updateWeightWarehouses($price = null, $weight = null)
    {
		for (
            $price = $this->getStartPricePerKilo();
            $price < $this->getEndPricePerKilo() + $this->getPriceUpInPeriod();
            $price = $price + $this->getPriceUpInPeriod()
        ) {
			$cacheId = $this->getCacheIdWeightWarehouses($price);
			$this->cache->delete($cacheId);
        }
    }

    public function getStartPricePerKilo()
    {
        $settings = $this->getSettings();

        $startPrice = $settings['start_price'];
        if ($this->getDirection()) {
            $startPrice = $this->getDirection()->getStartPrice();
        }

        return $startPrice;
    }

    public function getPriceUpPeriod()
    {
        $settings = $this->getSettings();

        $priceUpInPeriod = $settings['price_up_period'];
        if ($this->getDirection()) {
            $priceUpInPeriod = $this->getDirection()->getPriceUpPeriod();
        }

        return $priceUpInPeriod;
    }

    public function getPriceUpInPeriod()
    {
        $settings = $this->getSettings();

        $priceUpInPeriod = $settings['price_up_in_period'];
        if ($this->getDirection()) {
            $priceUpInPeriod = $this->getDirection()->getPriceUpInPeriod();
        }

        return $priceUpInPeriod;
    }

    public function getEndPricePerKilo($vat = false)
    {
        $settings = $this->getSettings();

        $endPrice = $settings['end_price'];
        if ($this->getDirection()) {
            $endPrice = $this->getDirection()->getEndPrice();
        }

        if ($vat) {
            $endPrice *= 1.2;
        }

        $priceUpInPeriod = $this->getPriceUpInPeriod();
        $endPrice = $endPrice - ($endPrice % $priceUpInPeriod);

        return $endPrice;
    }

    public function getCurrentPricePerKilo($vat = false)
    {
        $startPrice = $this->getStartPricePerKilo();
        $priceUpInPeriod = $this->getPriceUpInPeriod();

		$currentPricePerKilo = $startPrice + $this->getPeriod($vat) * $priceUpInPeriod;

        return $currentPricePerKilo;
    }

    public function getNextPricePerKilo(Direction $direction = null, $vat = false)
    {
        $startPrice = $this->getStartPricePerKilo();
        $endPrice = $this->getEndPricePerKilo($vat);
        $priceUpInPeriod =  $this->getPriceUpInPeriod();

        $nextPricePerKilo = $this->getCurrentPricePerKilo($vat) + $priceUpInPeriod;
        if ($nextPricePerKilo > $endPrice) {
			$nextPricePerKilo = $startPrice;
		}

        return $nextPricePerKilo;
    }

    public function getPriceTime($vat = false)
    {
        $settings = $this->getSettings();

        $priceUpPeriod = $settings['price_up_period'];
        if ($this->getDirection()) {
            $priceUpPeriod = $this->getDirection()->getPriceUpPeriod();
        }

        $currentDateTime = new \DateTime();
        $checkDateTime = new \DateTime();
        $checkDateTime->setTime($currentDateTime->format('H'), 0, 0);

        $dateDiff = $currentDateTime->diff($checkDateTime);

        $weight = $this->getWeightWarehouses($this->getCurrentPricePerKilo($vat), $vat);

        if ($weight == 0) {
            $priceUpPeriod = 10;
        }

        $seconds = $priceUpPeriod - ($dateDiff->i*60 + $dateDiff->s) % $priceUpPeriod;

        return (int)$seconds;
    }

    public function getCurrentAuctionTime($vat = false)
    {
        $priceUpPeriod = $this->getPriceUpPeriod();
        $totalTime = 0;
        for (
            $price = $this->getStartPricePerKilo();
            $price < $this->getEndPricePerKilo($vat) + $this->getPriceUpInPeriod();
            $price = $price + $this->getPriceUpInPeriod()
        ) {
            $weight = $this->getWeightWarehouses($price, $vat);

            $currentUpTimePeriod = 0;
            if ($weight > 0) {
                $currentUpTimePeriod = $priceUpPeriod;
            } else {
                $currentUpTimePeriod = 10;
            }

            $totalTime += $currentUpTimePeriod;
        }

        return $totalTime;
    }

    public function getPeriod($vat = false)
    {
        $priceUpPeriod = $this->getPriceUpPeriod();
        $secondsFromStart = $this->getSecondsFromStart($vat);

        $period = 0;
        $totalTime = 0;
        for (
            $price = $this->getStartPricePerKilo();
            $price < $this->getEndPricePerKilo($vat) + $this->getPriceUpInPeriod();
            $price = $price + $this->getPriceUpInPeriod()
        ) {
            $weight = $this->getWeightWarehouses($price, $vat);

            $currentUpTimePeriod = 0;
            if ($weight > 0) {
                $currentUpTimePeriod = $priceUpPeriod;
            } else {
                $currentUpTimePeriod = 10;
            }

            if ($secondsFromStart >= $totalTime + $currentUpTimePeriod) {
                $totalTime += $currentUpTimePeriod;
                $period++;
            } else {
                break;
            }
        }

        ///$period = ($this->getSecondsFromStart() - $totalTime) / $priceUpPeriod;

        return (int)$period;
    }

    public function getSeconds()
    {
        $settings = $this->getSettings();

        $duration = 1;
        if ($this->getDirection()) {
            $startPrice = $this->getStartPricePerKilo();
            $endPrice = $this->getEndPricePerKilo();
            $priceUpPeriod = $this->getPriceUpPeriod();
            $priceUpInPeriod = $this->getPriceUpInPeriod();
            $duration = round(((($endPrice - $startPrice) / $priceUpInPeriod) * $priceUpPeriod) / 60, 2) ;
        }

        $currentDateTime = new \DateTime();
        $checkDateTime = new \DateTime();
        $checkDateTime->setTime($currentDateTime->format('H'), 0, 0);

        $dateDiff = $currentDateTime->diff($checkDateTime);

        $seconds = floor(($dateDiff->i % $duration)*60 + $dateDiff->s);

        return (int)$seconds;
    }

    public function getSecondsFromStart($vat = false)
    {
        $settings = $this->getSettings();

        $duration = 1;
        if ($this->getDirection()) {
            $startPrice = $this->getStartPricePerKilo();
            $endPrice = $this->getEndPricePerKilo($vat);
            $priceUpPeriod = $this->getPriceUpPeriod();
            $priceUpInPeriod = $this->getPriceUpInPeriod();
            $duration = round(((($endPrice + $priceUpInPeriod - $startPrice) / $priceUpInPeriod) * $priceUpPeriod) / 60, 2) ;
        }

        $currentDateTime = new \DateTime();

        $seconds = floor($currentDateTime->format('U') % $this->getCurrentAuctionTime($vat));

        return (int)$seconds;
    }

    public function getDirections()
    {
        $directionRepository = $this->entityManager->getRepository(Direction::class);
        $directions = $directionRepository->getAllArray();

        return $directions;
    }

    public function getStocks()
    {
        $stockRepository = $this->entityManager->getRepository(Stock::class);
        $stocks = $stockRepository->findBy(['published' => true]);

        return $stocks;
    }
}
