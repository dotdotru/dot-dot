<?php

namespace App\Service;

use App\Entity\File;
use App\Entity\FileGroup;
use App\Repository\FileRepository;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use Sonata\MediaBundle\Entity\MediaManager ;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadService
{
    public function __construct(EntityManager $entityManager, MediaManager $mediaManager)
    {
        $this->entityManager = $entityManager;
        $this->mediaManager = $mediaManager;
    }

    public function updateFile(File $file, $fileData)
    {
        $media = $this->createMedia($fileData, $file->getGroup()->getTitle());

        if (!$media) {
            throw new \Exception('error create media');
        }

        $file->setMedia($media);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        return $file;
    }

    public function createFile($title, $group, $fileData, $externalId = null, $fileName = '')
    {
        $fileRepository = $this->entityManager->getRepository(File::class);
        $fileGroupRepository = $this->entityManager->getRepository(FileGroup::class);

        if (!is_a($group, FileGroup::class)) {
            $group = $fileGroupRepository->findOneBySlug($group);

            if (!$group) {
                throw new \Exception('file group not found');
            }
        }

        if (!$fileName) {
            $fileName = $group->getTitle();
        }

        $media = $this->createMedia($fileData, $fileName);

        if (!$media) {
            throw new \Exception('error create media');
        }

        if (!$title) {
            $title = $group->getTitle();
        }

        $file = new File();
        $file->setLocalExternalId($externalId);
        $file->setPublished(true);
        $file->setTitle($title);
        $file->setGroup($group);
        $file->setMedia($media);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        return $file;
    }

    public function createMedia($data, $name = '', $context = 'default', $providerName = 'sonata.media.provider.file')
    {
        $this->entityManager->getConnection()->beginTransaction();

        if (!$name) {
            $name = uniqid().'.pdf';
        }

        try {
            $media = new Media();
            $media->setName($name);
            $media->setBinaryContent($data);
            $media->setCreatedAt(new \DateTime());
            $media->setContext($context);
            $media->setEnabled(true);
            $media->setProviderName($providerName);
            $this->mediaManager->save($media, true);

        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            return null;
        }

        $this->entityManager->getConnection()->commit();

        return $media;
    }

}
