<?php
namespace App\Service;

use App\Entity\City;;
use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\Package;
use App\Entity\Packing;
use App\Entity\Size;
use App\Entity\Stock;
use App\Model\PackageInterface;
use Doctrine\ORM\EntityManager;
use App\Entity\CityPricePerKilo;

class CalculateService
{

    const VOLUME_WEIGHT_COEF = 280;


    public function convertSizeToWeight(Size $size)
    {
        if ($size->getCalculateWeight()) {
            $weight = $size->getCalculateWeight() * CalculateService::VOLUME_WEIGHT_COEF;
        } else {
            $weight = ($size->getWidth()*$size->getHeight()*$size->getDepth())/1000000 * CalculateService::VOLUME_WEIGHT_COEF;
        }

        return $weight;
    }

    public function getCalculateWeight(PackageInterface $package, $forOne = false)
    {
        $size = $package->getSizeObject();
        $sizeToWeight = $this->convertSizeToWeight($size);

        $resultWeight = $sizeToWeight > $package->getWeight() ? $sizeToWeight : $package->getWeight();
        
        if ($forOne == false) {
			$resultWeight = $resultWeight * $package->getCount();
		}

        return $resultWeight;
    }

    public function getOrderCalculateWeight(Order $order, $forOne = false)
    {
        $resultWeight = 0;

        if ($order->getPackages()->count() > 0) {
            foreach ($order->getPackages() as $package) {
                $resultWeight += $this->getCalculateWeight($package, $forOne);
            }
        } else {
            foreach ($order->getOrderPackages() as $package) {
                $resultWeight += $this->getCalculateWeight($package, $forOne);
            }
        }

        return $resultWeight;
    }

    public function getCalculateWeightByCargo(PackageInterface $package)
    {
        $size = $package->getSizeObject();
        $sizeToWeight = $this->convertSizeToWeight($size);

        $resultWeight = $sizeToWeight > $package->getWeight() ? $sizeToWeight : $package->getWeight();

        return $resultWeight;
    }

    public function getPricePerKiloRoute(City $cityFrom, City $cityTo, $weight)
    {
        $result = false;
        $pricesPerKilo = $cityFrom->getPricesPerKilo($cityTo);

        /**
         * @var CityPricePerKilo $pricePerKilo
         */
        foreach ($pricesPerKilo as $pricePerKilo) {
            if (
                ($pricePerKilo->getWeightFrom() <= $weight && $pricePerKilo->getWeightTo() > $weight)
                ||
                ($pricePerKilo->getWeightFrom() <= $weight && !$pricePerKilo->getWeightTo())
            ) {
                $result = $pricePerKilo->getPrice();
                break;
            }
        }

        return $result;
    }

    /** @var Packing $packing, Size $size*/
    public function calcPackingPrice($packing, $size)
    {
        $packingPrice = 0;
        if ($packing) {
            if ($packing->getType() == 'for_one') {
                $packingPrice = $packing->getPrice();
            } elseif ($packing->getType() == Packing::TYPE_TO_SIZE) {
                $packingPrice = $packing->getPrice() * $size->getVolume();
                if ($packing->getMinPrice() && $packingPrice < $packing->getMinPrice()) {
                    $packingPrice = $packing->getMinPrice();
                }
            }
        }

        return $packingPrice;
    }

    public function calcTotalPackingPrice(OrderInterface $order)
    {
        $tmpPackingPrice = 0;

        if ($order->getPackages()->count() > 0) {
            /** @var Package $package */
            foreach ($order->getPackages() as $package) {
                if ($package->getPacking() && !$package->getPackingPrice()) {
					if ($order->isByCargo()) {
						$tmpPackingPrice += $this->calcPackingPrice($package->getPacking(), $package->getSizeObject());
					} else {
						$tmpPackingPrice += $this->calcPackingPrice($package->getPacking(), $package->getSizeObject()) * $package->getCount();
					}
                } else {
                    $tmpPackingPrice += $package->getPackingPrice();
                }
            }
        } else {
            foreach ($order->getOrderPackages() as $package) {
                if ($package->getPacking()) {
					if ($order->isByCargo()) {
						$tmpPackingPrice += $this->calcPackingPrice($package->getPacking(), $package->getSizeObject());
					} else {
						$tmpPackingPrice += $this->calcPackingPrice($package->getPacking(), $package->getSizeObject()) * $package->getCount();
					}
                }
            }
        }

        return ceil($tmpPackingPrice);
    }

    public function calcPrice(OrderInterface $order)
    {
        $tmpPackingPrice = 0;
        /** @var Package $package */
        foreach ($order->getPackages() as $package) {
            if ($package->getPacking()) {
                $tmpPackingPrice += $this->calcPackingPrice($package->getPacking(), $package->getSizeObject()) * $package->getCount();
            }
        }

        return $tmpPackingPrice;
    }

}
