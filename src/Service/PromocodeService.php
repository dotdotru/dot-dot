<?php
namespace App\Service;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Package;
use App\Entity\Promocode;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use DateTime;

class PromocodeService
{
    /**
     * @var Container $container
     */
    private $container;

    /** @var EntityManager  */
    private $entityManager;

    /** @var CalculateService */
    private $calculateService;

    private $promocodeRepository;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->promocodeRepository = $container->get('doctrine')->getRepository(Promocode::class);
        $this->calculateService = $container->get('app.calculate');
        $this->settings = $container->get('settings');
    }

    public function getFirstOrderPromocode()
    {
        $promocode = $this->promocodeRepository->findOneBy(['title' => 'REG00']);
        return $promocode;
    }

    public function getPromocode($id)
    {
        $promocode = $this->promocodeRepository->find($id);
        return $promocode;
    }

    public function applyOrder($order, Promocode $promocode = null)
    {
        $tmpTotalPrice = 0;
        $tmpTotalWeight = 0;

        $settings = $this->settings->group('customer_calc');

        $cityFrom = $order->getDirection()->getCityFrom();
        $cityTo = $order->getDirection()->getCityTo();

        $estimatesPerKilo = 0;
        $resultWeight = 0;
        if ($order->getOrderPackages()->count() > 0 && $order->getPackages()->count() == 0) {
            /** @var Package $package */
            foreach ($order->getOrderPackages() as $package) {
                if ($order->isByCargo()) {
                    $resultWeight += $this->calculateService->getCalculateWeightByCargo($package);
                } else {
                    $resultWeight += $this->calculateService->getCalculateWeight($package);
                }
            }
        } else {
            /** @var Package $package */
            foreach ($order->getGroupingPackages() as $package) {
                if ($order->isByCargo()) {
                    $resultWeight += $this->calculateService->getCalculateWeightByCargo($package);
                } else {
                    $resultWeight += $this->calculateService->getCalculateWeight($package);
                }
            }
        }

        $discountKilo = 0;
        $discountDeclaredValue = 0;
        if ($promocode) {
            switch ($promocode->getType()) {
                case Promocode::TYPE_FIRSTREGISTER:
                    // @убрать флаг first
                    if (true || $order->isFirst()) {
                        $discountKilo = $promocode->getDiscountKilo();
                        $discountDeclaredValue = $promocode->getDiscountDeclaredValue();
                    }
                    break;
            }
        }
        
        $resultWeight = $resultWeight - $discountKilo;

        $estimatePerKilo = $this->calculateService->getPricePerKiloRoute($cityFrom, $cityTo, $resultWeight);
        $tmpTotalPrice += $resultWeight * $estimatePerKilo;
        $tmpTotalWeight += $resultWeight;

        //$price = $this->applyPrice($tmpTotalPrice, $tmpTotalWeight, $discountKilo);
        $declaredPrice = $this->applyDeclaredPrice($order->getDeclaredPrice() * $settings['insurance_price_percent'], $settings['insurance_price_percent'], $discountDeclaredValue);

        return [
            'price' => $tmpTotalPrice,
            'declaredPrice' => $declaredPrice,
            'resultWeights' => $resultWeight,
            'estimatesPerKilo' => $estimatePerKilo,
        ];
    }

    public function applyPrice($price, $weight, $discountKilo)
    {
        $price = $price - $this->getDiscountPrice($price, $weight, $discountKilo);

        if ($price <= 0) {
            $price = 0;
        }

        return $price;
    }

    private function getPricePerKilo($price, $weight)
    {
        $pricePerKilo = $price / $weight;

        return $pricePerKilo;
    }

    private function getDiscountPrice($price, $weight, $discountKilo)
    {
        $pricePerKilo = $this->getPricePerKilo($price, $weight);
        $discountPrice = $discountKilo * $pricePerKilo;

        return $discountPrice;
    }

    public function applyDeclaredPrice($price, $percent, $discountDeclaredValue)
    {
        $price = $price - $discountDeclaredValue * $percent;

        if ($price <= 0) {
            $price = 0;
        }

        return $price;
    }
}
