<?php

namespace App\Service;

use App\Entity\Organization\Organization;
use App\Form\UserType;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use SMSCenter\SMSCenter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Session;

class RestorePassService
{
    const SETTING_GROUP = 'restore_pass';

    /** @var null|SmsService */
    private $smsService = null;

    /** @var null|Settings */
    private $settings = null;

    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|UserManager */
    private  $userManager = null;

    public function __construct(
        SmsService $smsService,
        Settings $settings,
        EntityManager $entityManager,
        $tokenGenerator,
        $userManager
    )
    {
        $this->smsService = $smsService;
        $this->settings = $settings;
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->userManager = $userManager;
    }

    public function sendSms($phone, $password)
    {
        $settings = $this->settings->group(self::SETTING_GROUP);

        $smsText = str_replace('#PASSWORD#', $password, $settings['sms']);

        return $this->smsService->send($phone, $smsText);
    }

    private function generatePassword()
    {
        $password = substr($this->tokenGenerator->generateToken(), 0, 6);
        return $password;
    }

    public function changePassword($phone)
    {
        $phone = $this->clearPhone($phone);

        /** @var User $user */
        $user = $this->userManager->findUserByUsernameOrEmail($phone);

        if (!$user) {
            return false;
        }

        $password = $this->generatePassword();
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $this->sendSms($phone, $password);

        $this->userManager->updatePassword($user);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }

    public function isValidPhone($phone)
    {
        return $this->smsService->isValidPhone($phone);
    }

    public function clearPhone($phone)
    {
        return $this->smsService->clearPhone($phone);
    }
}
