<?php

namespace App\Service\Competitor;

use App\Entity\Trace\CalculateTrace;
use App\Entity\Trace\Log;
use App\Service\CalculateService;
use Doctrine\ORM\EntityManager;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;

abstract class CompetitorService
{
    const NAME = '';
    const CODE = '';

    /** @var Client */
    private $client;

    protected $url = '';
    protected $login = '';
    protected $password = '';

    /** @var EntityManager */
    protected $entityManager;

    public function getName()
    {
        return $this::NAME;
    }

    public function __construct(EntityManager $entityManager, Client $client, $url, $username = null, $password = null)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
        $this->entityManager = $entityManager;

        $this->getClient();
    }

    protected function getClient()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
        ]);
        return $this->client;
    }

    abstract public function calc($data);

    public function exec($requestUrl, array $data, $requestType = 'GET')
    {
        $code = $this::CODE.date('d.m.Y');

        $calculateTraceRepository = $this->entityManager->getRepository(CalculateTrace::class);
        $calculateTrace = $calculateTraceRepository->findOneBy(['code' => $code]);

        if (!$calculateTrace) {
            $calculateTrace = new CalculateTrace();
            $calculateTrace->setCode($code);
        }

        try {
            $res = $this->getClient()->request($requestType, $requestUrl);

            $data = json_decode($res->getBody(), true);

            $log = new Log();
            $log->setRequest($requestUrl.PHP_EOL.print_r($data, true));
            $log->setResponse($res->getBody());
            $log->setRequestIp($_SERVER['REMOTE_ADDR']);
            $log->setResponseIp(gethostbyname(parse_url($this->getClient()->getConfig('base_uri'), PHP_URL_HOST)));
            $calculateTrace->addItem($log);

            $this->entityManager->persist($calculateTrace);
            $this->entityManager->flush();

            if ($this->isErrorResponse($data)) {
                return $data['error'];
            }

            return $data;

        } catch (RequestException $e) {

            //echo '<pre>';
            //var_dump($e->getMessage());
            //var_dump($requestUrl);
            //var_dump($e->getResponse()->getBody());

            return null;
        }
    }

    protected function isErrorResponse($data)
    {
        if (isset($data['errors']) && !$data['errors']) {
            return true;
        }

        return false;
    }

    public function getCode()
    {
        return $this::CODE;
    }
}
