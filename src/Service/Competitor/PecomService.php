<?php

namespace App\Service\Competitor;

use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;

class PecomService extends CompetitorService
{
    const NAME = 'ТК ПЭК';
    const CODE = 'pecom';

    public function calc($data)
    {
        $url = '/bitrix/components/pecom/calc/ajax.php?'.http_build_query($data);

        $result = $this->exec($url, []);

        $resultPrice = 0;

        if (isset($result['autonegabarit'])) {
            $resultPrice = floatval($result['autonegabarit'][2]);
        } elseif (isset($result['auto'])) {
            $resultPrice = floatval($result['auto'][2]);
        }
        if (isset($result['ADD_3'])) {
            $resultPrice += floatval($result['ADD_3']['2']);
        }

        return $resultPrice;
    }
}
