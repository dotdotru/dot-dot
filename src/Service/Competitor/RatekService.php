<?php

namespace App\Service\Competitor;

use App\Entity\Trace\CalculateTrace;
use App\Entity\Trace\Log;
use Doctrine\ORM\EntityManager;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;

class RatekService extends CompetitorService
{
    const NAME = 'Ратек';
    const CODE = 'ratek';

    private $apiKey = null;

    public function __construct(EntityManager $entityManager, Client $client, $url, $username = null, $password = null, $apiKey = null)
    {
        parent::__construct($entityManager, $client, $url, $username, $password);

        $this->apiKey = $apiKey;
    }

    public function calc($data)
    {
        $resultPrice = 0;
        $url = '/v1/calc.json';

        foreach ($data as $item) {
            $resultItem = $this->exec($url, $item, 'POST');

            if (isset($resultItem['totalAuto'])) {
                $resultPrice += $resultItem['totalAuto'];
            }
        }

        return $resultPrice;
    }

    public function exec($requestUrl, array $data, $requestType = 'GET')
    {
        $data['key'] = $this->apiKey;

        $code = $this::CODE.date('d.m.Y');

        $calculateTraceRepository = $this->entityManager->getRepository(CalculateTrace::class);
        $calculateTrace = $calculateTraceRepository->findOneBy(['code' => $code]);

        if (!$calculateTrace) {
            $calculateTrace = new CalculateTrace();
            $calculateTrace->setCode($code);
        }

        try {
            $res = $this->getClient()->request($requestType, $requestUrl, [
                'form_params' => $data
            ]);

            $responseData = json_decode($res->getBody(), true);

            if ($this->isErrorResponse($data)) {
                return $data['error'];
            }

            $log = new Log();
            $log->setRequest($requestUrl.PHP_EOL.print_r($data, true));
            $log->setResponse($res->getBody());
            $log->setRequestIp($_SERVER['REMOTE_ADDR']);
            $log->setResponseIp(gethostbyname(parse_url($this->getClient()->getConfig('base_uri'), PHP_URL_HOST)));
            $calculateTrace->addItem($log);

            $this->entityManager->persist($calculateTrace);
            $this->entityManager->flush();

            return $responseData;

        } catch (RequestException $e) {

            //echo '<pre>';
            //var_dump($e->getMessage());
            //var_dump($requestUrl);
            //var_dump($e->getResponse()->getBody());

            return null;
        }
    }
}
