<?php

namespace App\Service\Competitor;

use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;

class TkkitService extends CompetitorService
{
    const NAME = 'ТК КИТ';
    const CODE = 'tkkit';

    public function calc($data)
    {
        $resultPrice = 0;
        $url = '/API.1?f=price_order';
        foreach ($data as $item) {
            $url = $url.'&'.http_build_query($item);

            $resultItem = $this->exec($url, []);

            $resultPrice += $resultItem['PRICE']['TOTAL'];
        }

        return $resultPrice;
    }
}
