<?php

namespace App\Service;

use App\Entity\DispatchLog;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\Trace\InsuranceTrace;
use App\Entity\Trace\Log;
use App\Entity\Trace\VerificationTrace;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use SimpleXMLElement;

class DispatchService
{
    /** @var EntityManager */
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function dispatch(User $user, $dispath, $page = null)
    {
        if ($user->isSubscribe() == $dispath) {
            return false;
        }

        $dispathLog = new DispatchLog();
        if ($dispath) {
            $dispathLog->setEvent(DispatchLog::EVENT_SUBSCRIBE);
        } else {
            $dispathLog->setEvent(DispatchLog::EVENT_UNSUBSCRIBE);
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $dispathLog->setIp($ip);
        $dispathLog->setPage($page);
        $dispathLog->setUser($user);

        $user->setSubscribe($dispath);

        $this->entityManager->persist($user);
        $this->entityManager->persist($dispathLog);
        $this->entityManager->flush();
    }

    public function dispatchEmail($email, $dispath, $page = null)
    {
        $dispathLog = new DispatchLog();
        if ($dispath) {
            $dispathLog->setEvent(DispatchLog::EVENT_SUBSCRIBE);
        } else {
            $dispathLog->setEvent(DispatchLog::EVENT_UNSUBSCRIBE);
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $dispathLog->setIp($ip);
        $dispathLog->setPage($page);
        $dispathLog->setUser(null);
        $dispathLog->setEmail($email);

        $this->entityManager->persist($dispathLog);
        $this->entityManager->flush();
    }
}
