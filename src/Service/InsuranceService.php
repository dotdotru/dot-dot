<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Package;
use App\Entity\Shipping;
use App\Entity\Trace\InsuranceTrace;
use App\Entity\Trace\Log;
use App\Entity\Trace\VerificationTrace;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use App\Entity\Base;
use SimpleXMLElement;

class InsuranceService
{
    /** @var SvSoapService */
    private $svsoap;

    /** @var EntityManager */
    private $entityManager;

    public function __construct(
        SvSoapService $svsoap,
        EntityManager $entityManager
    ) {
        $this->svsoap = $svsoap;
        $this->entityManager = $entityManager;
    }

    public function insuranceAll()
    {
        $orderRepository = $this->entityManager->getRepository(Order::class);
        $orders = $orderRepository->findBy([],['id' => 'desc']);

        /** @var Order $order */
        foreach ($orders as $order) {

            $shipping = $order->getShipping(Shipping::TYPE_PICKUP);

            if (
                $order->getExternalId()
                && $order->getId() > 2209
                && !$order->isRemoveExactStatus()
                && !$order->isFinished()
                && !$order->isInsured()
                && count($order->getPackages()) > 0
            ) {
                $this->insurance($order);
            } else if (
                $order->getExternalId()
                && $order->getId() > 2209
                && !$order->isRemoveExactStatus()
                && !$order->isFinished()
                && !$order->isInsured()
                && ($shipping->isActive() && $shipping->getExternalId() && $shipping->hasDriver())
                && count($order->getOrderPackages()) > 0
            ) {
                $this->insurance($order);
            }
        }
    }

    public function insurance(Order $order)
    {
        if ($order->isInsured()) {
            return true;
        }
        
        $insuranceTraceRepository = $this->entityManager->getRepository(InsuranceTrace::class);

        $xml = $this->mapToXml($order);

        if (!$xml) {
            return false;
        }

        $insuranceTrace = $insuranceTraceRepository->findOneBy(['orderCustomer' => $order]);

        if (!$insuranceTrace) {
            $insuranceTrace = new InsuranceTrace();
            $insuranceTrace->setOrder($order);
        }

        $result = $this->svsoap->insurance($xml);

        if ($result['number']) {
            $order->setInsuredNumber($result['number']);
        }

        if ($result['pdf']) {
            $order->setInsuredPdf($result['pdf']);
        }

        if ($result['code']) {
            $order->setInsuredCode($result['code']);
        }

        $log = new Log();
        $log->setRequest($this->svsoap->getLastRequest());
        $log->setResponse($this->svsoap->getLastResponse());
        $insuranceTrace->addItem($log);

        if ($order->isInsuredSuccess()) {
            $insuranceTrace->setStatus(InsuranceTrace::STATUS_SUCCESS);
        }

        $this->entityManager->persist($order);
        $this->entityManager->persist($insuranceTrace);
        $this->entityManager->flush();

        return $order->isInsuredSuccess();
    }

    private function mapToXml(Order $order)
    {
		$baseDirection = $this->entityManager->getRepository(Base\Direction::class)->findOneByExternalId($order->getDirection()->getExternalId());
		list($h, $i, $s) = explode(':', $baseDirection->getCustomerDeliveryTime());
		
        $xml = new SimpleXMLElement('<xml></xml>');
        $orderXml = $xml->addChild('order');
        $orderXml->addAttribute('id', $order->getId());
        $orderXml->addAttribute('declared_price', $order->getDeclaredPrice());

        /** @var User $sender */
        $sender = $order->getUser();
        $sender->setCustomerNow();

        if (!$sender->isPrivatePerson()) {
            /** @var OrganizationRepository $organizationRepository */
            $organizationRepository = $this->entityManager->getRepository(Organization::class);
            $organization = $organizationRepository->getByUser($sender);

            if (!$organization) {
                return null;
            }

            $companyXml = $orderXml->addChild('policyowner');
            $companyXml->addChild('juridical', (int)!$sender->isPrivatePerson());
            $companyXml->addChild('name', $organization->getCompanyName());
            $companyXml->addChild('type', $organization->getType());
            $companyXml->addChild('inn', $organization->getInn());
        } else {
            $userXml = $orderXml->addChild('policyowner');
            $userXml->addChild('juridical', (int)!$sender->isPrivatePerson());
            $userXml->addChild('name', $sender->getFirstname());
            $userXml->addChild('number', $sender->getNumber());
            $userXml->addChild('series', $sender->getSeries());
        }

        $shipping = $order->getShipping(Shipping::TYPE_PICKUP);

        if ($shipping->isActive()) {
            $stockFromXml = $orderXml->addChild('store_from');
            if ($order->getStockFrom()) {
                $city = $shipping->getStock()->getCity();
                $address = '';
                if ($city->getRegion()) {
                    $address = $city->getRegion()->getTitle();
                } else {
                    $address = $city->getTitle();
                }
                $stockFromXml->addAttribute('id', $order->getStockFrom()->getId());
                $stockFromXml->addChild('address', $address);
            }
        } else {
            $stockFromXml = $orderXml->addChild('store_from');
            if ($order->getStockFrom()) {
                $stockFromXml->addAttribute('id', $order->getStockFrom()->getId());
                $stockFromXml->addChild('address', $order->getStockFrom()->getAddress());
            }
        }

        $shipping = $order->getShipping(Shipping::TYPE_DELIVER);
        if ($shipping->isActive()) {
            $stockFromXml = $orderXml->addChild('store_to');
            if ($order->getStockTo()) {
                $city = $shipping->getStock()->getCity();
                $address = '';
                if ($city->getRegion()) {
                    $address = $city->getRegion()->getTitle();
                } else {
                    $address = $city->getTitle();
                }
                $stockFromXml->addAttribute('id', $order->getStockTo()->getId());
                $stockFromXml->addChild('address',  $address);
            }
        } else {
            $stockFromXml = $orderXml->addChild('store_to');
            if ($order->getStockTo()) {
                $stockFromXml->addAttribute('id', $order->getStockTo()->getId());
                $stockFromXml->addChild('address', $order->getStockTo()->getAddress());
            }
        }

        $date = new \DateTime();
        $orderXml->addChild('date', $date->format('Y-m-d\TH:i:s'));
        
        $orderXml->addChild('deliveryTime', (int)$h);

        if ($order->getGroupingPackages()) {
            /** @var Package $package */
            foreach ($order->getGroupingPackages() as $package) {
                $productXml = $orderXml->addChild('product');
                $productXml->addAttribute('id', $package->getId());
                $productXml->addChild('status', 'new');
                $productXml->addChild('name', $package->getWeight());

                $productXml->addChild('weight', $package->getWeight());
                if ($package->getPacking()) {
                    $packingXml = $productXml->addChild('packing', $package->getPacking()->getTitle());
                    $packingXml->addAttribute('id', $package->getPacking()->getId());
                    $packingXml->addAttribute('external-id', $package->getPacking()->getId());
                }
                if ($package->getPackageType()) {
                    $packingXml = $productXml->addChild('package_type', $package->getPackageType()->getTitle());
                    $packingXml->addAttribute('id', $package->getPackageType()->getId());
                    $packingXml->addAttribute('external-id', $package->getPackageType()->getId());
                } elseif($package->getPackageTypeAnother()) {
                    $packingXml = $productXml->addChild('package_type', $package->getPackageTypeAnother());
                    $packingXml->addAttribute('id', 0);
                    $packingXml->addAttribute('external-id', 0);
                }
                $productXml->addChild('count', $package->getCount());
            }
        } else if ($order->getOrderPackages()) {
            /** @var Package $package */
            foreach ($order->getOrderPackages() as $package) {
                $productXml = $orderXml->addChild('product');
                $productXml->addAttribute('id', $package->getId());
                $productXml->addChild('status', 'new');
                $productXml->addChild('name', $package->getWeight());

                $productXml->addChild('weight', $package->getWeight());
                if ($package->getPacking()) {
                    $packingXml = $productXml->addChild('packing', $package->getPacking()->getTitle());
                    $packingXml->addAttribute('id', $package->getPacking()->getId());
                    $packingXml->addAttribute('external-id', $package->getPacking()->getId());
                }
                if ($package->getPackageType()) {
                    $packingXml = $productXml->addChild('package_type', $package->getPackageType()->getTitle());
                    $packingXml->addAttribute('id', $package->getPackageType()->getId());
                    $packingXml->addAttribute('external-id', $package->getPackageType()->getId());
                } elseif($package->getPackageTypeAnother()) {
                    $packingXml = $productXml->addChild('package_type', $package->getPackageTypeAnother());
                    $packingXml->addAttribute('id', 0);
                    $packingXml->addAttribute('external-id', 0);
                }
                $productXml->addChild('count', $package->getCount());
            }
        }



        return $xml;
    }

    public function insuranceCancelAll()
    {
        $orderRepository = $this->entityManager->getRepository(Order::class);
        $orders = $orderRepository->findBy(['remove' => true], ['id' => 'desc']);

        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->isInsured() && $order->isRemove()) {
                $this->insuranceCancel($order);
            }
        }
    }

    public function insuranceCancel(Order $order)
    {
        if ($order->isInsuredCancelled()) {
            return true;
        }

        $xml = $this->mapToXmlCancel($order);

        $result = $this->svsoap->insuranceCancel($xml);

        if ($result['code']) {
            $order->setInsuredCode($result['code']);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order->isInsuredSuccess();
    }

    private function mapToXmlCancel(Order $order)
    {
        $xml = new SimpleXMLElement('<xml></xml>');
        $orderXml = $xml->addChild('order');
        $orderXml->addAttribute('id', $order->getId());

        return $xml;
    }

    /*
<?xml version="1.0" encoding="UTF-8"?>
<xml>
   <key>16e93b225c730920ac5741a8d8df788f</key>
	<order>
		<declared_price>10000</declared_price>
		<store_from id="1">
			<address>Москва, большая дмитровка 15</address>
		</store_from>
		<store_to id="1">
			<address>Рязань, ул. Восточная, 23</address>
		</store_to>
		<date>2018-12-2T19:13:10</date>
		<product id="1243">
			<status>new</status>
			<weight>5</weight>
			<packing id="2" external-id="4">Обрешетка</packing>
			<package_type id="2" external-id="4">Бытовая химия</package_type>
			<count>20</count>
      </product>
		<product id="1243">
			<status>new</status>
			<weight>20</weight>
			<packing id="2" external-id="4">Короб малый</packing>
			<package_type id="2" external-id="4">Одежда и обувь</package_type>
			<count>5</count>
      </product>
	</order>
</xml>

     */


}
