<?php

namespace App\Service;

use App\Entity\Email;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Trace\Log;
use App\Entity\Trace\VerificationTrace;
use Doctrine\ORM\EntityManager;
use Vsavritsky\SettingsBundle\Service\Settings;
use SimpleXMLElement;

class VerificationService
{
    const SETTING_GROUP_VERIFICATION = 'verification';

    /** @var SvSoapService */
    private $svsoap;

    /** @var EntityManager */
    private $entityManager;

    /** @var null|SmsService */
    private $smsService = null;

    /** @var null|Settings */
    private $settings = null;

    /** @var null|MailService */
    private $mailService = null;

    public function __construct(
        SvSoapService $svsoap,
        SmsService $smsService,
        MailService $mailService,
        Settings $settings,
        EntityManager $entityManager
    ) {
        $this->smsService = $smsService;
        $this->settings = $settings;
        $this->svsoap = $svsoap;
        $this->mailService = $mailService;
        $this->entityManager = $entityManager;
    }

    public function forceUpdate()
    {
        $dateTimeStart = new \DateTime();
        $dateTimeStart->sub(new \DateInterval('P5D'));
        $dateTimeEnd = new \DateTime();
        $xml = $this->mapToXmlList($dateTimeStart, $dateTimeEnd, 1, 100);
        $list = $this->svsoap->getVerificationList($xml);

        $organizationRepository = $this->entityManager->getRepository(Organization::class);
        $organizationDriverRepository = $this->entityManager->getRepository(OrganizationDriver::class);

        if (isset($list['client'])) {
            foreach ($list['client'] as $item) {
                /** @var Organization $organization */
                $organization = $organizationRepository->find($item['id']);

                if (!$organization) {
                    continue;
                }

                if ($organization->getVerifiedCode() != $item['status']) {
                    $organization->resetVerification();
                    $organization->setVerifiedCode($item['status']);
                    $this->entityManager->persist($organization);
                }
            }
        }

        if (isset($list['driver'])) {
            foreach ($list['driver'] as $item) {
                /** @var OrganizationDriver $organizationDriver */
                $organizationDriver = $organizationDriverRepository->find($item['id']);

                if (!$organizationDriver) {
                    continue;
                }

                if ($organizationDriver->getVerifiedCode() != $item['status']) {
                    $organizationDriver->resetVerification();
                    $organizationDriver->setVerifiedCode($item['status']);
                    $this->entityManager->persist($organizationDriver);
                }
            }
        }

        $this->entityManager->flush();

        return true;
    }

    public function verifyAll()
    {
        $organizationRepository = $this->entityManager->getRepository(Organization::class);
        $organizations = $organizationRepository->findBy([],['id' => 'desc']);

        foreach ($organizations as $organization) {
			try {
			    $organizationNeedsVerify = false;

			    foreach ($organization->getDrivers() as $driver) {
                    if (!$driver->isVerifiedSuccess() && !$driver->isVerifiedError()) {
                        $organizationNeedsVerify = true;
                        break;
                    }
                }

                if ($organizationNeedsVerify) {
                    $this->verify($organization);
                }

			} catch (\Exception $e) {
				echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
			}
        }
    }

    public function verify(Organization $organization, $notificationOnError = true, $notificationOnSuccess = true, $updateAction = 'full')
    {
        if ($organization->isVerifiedAll()) {
            return true;
        }

        $verificationTraceRepository = $this->entityManager->getRepository(VerificationTrace::class);

	    $organizationVerified = $organization->isVerifiedSuccess();
        foreach ($organization->getDrivers() as $driver) {

            $driverVerifiedSuccess = $driver->isVerifiedSuccess();
            $driverVerifiedError = $driver->isVerifiedError();

            $xml = $this->mapToXml($organization, $driver);

            $verificationTrace = $verificationTraceRepository->findOneBy(['organization' => $organization, 'driver' => $driver]);

            if (!$verificationTrace) {
                $verificationTrace = new VerificationTrace();
                $verificationTrace->setOrganization($organization);
                $verificationTrace->setDriver($driver);
            }

            $result = $this->svsoap->verify($xml);

            if ($result) {
                if ($result['organization']) {
                    $organization->setVerifiedCode($result['organization']);
                }

                if ($result['driver']) {
                    if ($organization->isVerifiedError()) {
                        $driver->setVerifiedCode($organization->getVerifiedCode());
                    } else {
                        $driver->setVerifiedCode($result['driver']);
                    }
                }

                if (!$driverVerifiedSuccess && $driver->isVerifiedSuccess() && $notificationOnSuccess && $updateAction != 'organization') {
					$this->notificationSuccessDriver($organization, $driver);
				} elseif(!$driverVerifiedError && $driver->isVerifiedError() && $notificationOnError && $updateAction != 'organization') {
					$this->notificationErrorDriver($organization, $driver);
				}
            }

            $log = new Log();
            $log->setRequest($this->svsoap->getLastRequest());
            $log->setResponse($this->svsoap->getLastResponse());
            $verificationTrace->addItem($log);

            if ($organization->isVerifiedSuccess() && $driver->isVerifiedSuccess()) {
                $verificationTrace->setStatus(VerificationTrace::STATUS_SUCCESS);
            }

            $this->entityManager->persist($organization);
            $this->entityManager->persist($driver);
            $this->entityManager->persist($verificationTrace);
            $this->entityManager->flush();
        }

        if ($organization->isVerifiedSuccess() && !$organizationVerified && !$organization->getVerifiedNotificationDate()) {
            if($notificationOnSuccess && $updateAction != 'driver') {
                $this->notificationSuccess($organization);
            }

            $organization->setVerifiedNotificationDate(new \DateTime());

            $this->entityManager->persist($organization);
            $this->entityManager->flush();

            return true;
        } else if ($organization->isVerifiedError() && !$organizationVerified && !$organization->getVerifiedNotificationDate()) {
            if($notificationOnError && $updateAction != 'driver') {
                $this->notificationError($organization);
            }

            $organization->setVerifiedNotificationDate(new \DateTime());
            $this->entityManager->persist($organization);
            $this->entityManager->flush();
            return true;
        }

        return $organization->isVerifiedSuccess();
    }

    private function notificationSuccess(Organization $organization)
    {
        /* sms */
        $settings = $this->settings->group(self::SETTING_GROUP_VERIFICATION);
        $result = $this->smsService->send($organization->getUser()->getPhone(), $settings['sms_verified']);

        $this->mailService->registerEmail('verified_success', $organization->getUser()->getEmail());

        return $result;
    }

    private function notificationError(Organization $organization)
    {
        $settings = $this->settings->group(self::SETTING_GROUP_VERIFICATION);
        $result = $this->smsService->send($organization->getContactPhone(), $settings['sms_verified_error']);

        $this->mailService->registerEmail('verified_error', $organization->getUser()->getEmail());

        return $result;
    }

    private function notificationSuccessDriver(Organization $organization, OrganizationDriver $driver)
    {
        /* sms */
        $settings = $this->settings->group(self::SETTING_GROUP_VERIFICATION);

        $driverName = $driver->getName();
        $result = $this->smsService->send($organization->getUser()->getPhone(), str_replace('#FIO#', $driverName, $settings['sms_verified_driver']));

        return $result;
    }

    private function notificationErrorDriver(Organization $organization, OrganizationDriver $driver)
    {
        $settings = $this->settings->group(self::SETTING_GROUP_VERIFICATION);

        $driverName = $driver->getName();
        $result = $this->smsService->send($organization->getContactPhone(), str_replace('#FIO#', $driverName, $settings['sms_verified_driver_error']));

        return $result;
    }


    private function mapToXml(Organization $organization, OrganizationDriver $driver)
    {
        $xml = new SimpleXMLElement('<xml></xml>');
        $companyXml = $xml->addChild('company');
        $companyXml->addAttribute('id', $organization->getId());
        $companyXml->addChild('name', $organization->getCompanyName());
        $companyXml->addChild('inn', $organization->getInn());
        $companyXml->addChild('kpp', $organization->getKpp());
        $companyXml->addChild('type', $organization->getType());

        $driversXml = $xml->addChild('drivers');
        $driverXml = $driversXml->addChild('driver');
        $driverXml->addAttribute('id', $driver->getId());
        $driverXml->addChild('name', $driver->getName());
        $driverXml->addChild('gender', $driver->getGender());
        $driverXml->addChild('inn', $driver->getInn());
        $driverXml->addChild('birthday', $driver->getBirthdate()->format('d.m.Y'));
        $driverXml->addChild('phone', $driver->getPhone());

        $driverDocumentsXml = $driverXml->addChild('documents');
        foreach ($driver->getDocuments() as $document) {
            $documentXml = $driverDocumentsXml->addChild('document');
            $documentXml->addAttribute('type', $document->getType());
            $documentXml->addChild('series', $document->getSeries());
            $documentXml->addChild('number', $document->getNumber());
            $documentXml->addChild('date', $document->getDate()->format('d.m.Y'));
            $documentXml->addChild('place', $document->getPlace() != 'null' && $document->getPlace() ? $document->getPlace() : '');
        }

        return $xml;
    }

    private function mapToXmlList(\DateTime $dateFrom, \DateTime $dateTo, $page = 1, $count = 1000)
    {
        $xml = new SimpleXMLElement('<xml></xml>');
        $xml->addChild('from', $dateFrom->format('d.m.Y'));
        $xml->addChild('to', $dateTo->format('d.m.Y'));
        $xml->addChild('count', $count);
        $xml->addChild('page', $page);

        return $xml;
    }
}
