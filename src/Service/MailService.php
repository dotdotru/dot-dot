<?php
namespace App\Service;

use App\Entity\Email;
use App\Entity\EmailTemplate;
use App\Entity\File;
use App\Entity\FilePathEntity;
use App\Service\Pdf\GenerateService;
use Doctrine\ORM\EntityManager;
use Sonata\MediaBundle\Provider\BaseProvider;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Templating\EngineInterface;

class MailService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var GenerateService
     */
    protected $pdfGenerateService;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $fromEmail, EntityManager $entityManager, Container $container, GenerateService $pdfGenerateService)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->fromEmail = $fromEmail;
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->pdfGenerateService = $pdfGenerateService;
        $this->documentRoot = $this->container->get('kernel')->getRootDir() . '/../public';
    }

    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    public function getTemplateByExternalId($externalId)
    {
        $emailRepository = $this->entityManager->getRepository(EmailTemplate::class);
        $template = $emailRepository->findOneBy(['externalId' => $externalId, 'published' => true]);

        return $template;
    }

    public function getSendMail()
    {
        $emailRepository = $this->entityManager->getRepository(Email::class);
        $emails = $emailRepository->findBy(['status' => [Email::STATUS_ERROR, Email::STATUS_CREATE]]);

        return $emails;
    }

    public function prepare($template, array $data)
    {
        $result = [];
        if (isset($data['entity'])) {
            $entity = $data['entity'];
            $classMethods = get_class_methods($entity);

            foreach ($classMethods as $methodName) {
                if (strpos(strtoupper($methodName), 'GET') !== false) {
                    $key = str_replace('GET','', strtoupper($methodName));

                    try {
                        $item = $entity->$methodName();
                        if (!is_object($item) && !is_array($item)) {
                            $result[$key] = $item;
                        }
                    } catch (FatalThrowableError $e) {
                        continue;
                    } catch (\TypeError $e) {
                        continue;
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }

        foreach ($data as $key => $value) {
            $result[strtoupper($key)] = strval($value);
        }

        foreach ($result as $key => $value) {
            $template = str_replace('#'.$key.'#', $value, $template);
        }

        return $template;
    }

    public function createEmail($subject, $from = null, $to, $body, array $params = [], $template = 'empty')
    {
        if (!$from) {
            $from = $this->getFromEmail();
        }

        $from = (array)$from;
        $to = (array)$to;
		$from = array_filter($from);
		$to = array_filter($to);

        $email = new Email();
        $email->setSubject($subject);
        $email->setBody($body);
        $email->setFromEmail(implode(',', $from));
        $email->setToEmail(implode(',', $to));
        $email->setStatus(Email::STATUS_CREATE);
        $email->setTemplate($template);

        if (isset($params['files'])) {
            $email->setFilePaths((array)$params['files']);
        }
        if (!empty($params['generatePdf'])) {
            $tmp = ['generatePdf' => $params['generatePdf']];
            $email->setParams((array)$tmp);
        }

        $this->entityManager->persist($email);
        $this->entityManager->flush();

        return $email;
    }


    public function sendEmail(Email $email)
    {
        $params['subject'] = $email->getSubject();
        $params['body'] = $email->getBody();

        // Нельзя отправлять раздененные запятой емейлы
        $toEmails = explode(',', $email->getToEmail());

        $message = (new \Swift_Message($email->getSubject()))
            ->setFrom($email->getFromEmail())
            ->setTo($toEmails)
            ->setBody($params['body'], 'text/html')
        ;

        if (!empty($email->getFilePaths())) {
            /**
             * @var integer $fileKey
             * @var FilePathEntity $filePath
             */
            foreach ($email->getFilePaths() as $fileKey => $filePath) {
                if (file_exists($filePath)) {
                    $extension = pathinfo($this->documentRoot.$filePath->getValue(), PATHINFO_EXTENSION);
                    $attachment = \Swift_Attachment::fromPath($this->documentRoot.$filePath->getValue())
                        ->setFilename($filePath->getTitle() ? $filePath->getTitle().'.'.$extension : basename($filePath->getValue()));
                    $message->attach($attachment);
                }
            }
        }

        $tmpParams = $email->getParams();
        if (!empty($tmpParams['generatePdf']) && is_array($tmpParams['generatePdf'])) {
            foreach ($tmpParams['generatePdf'] as $generatePdf) {
                try {
                    if (!method_exists($this->pdfGenerateService, $generatePdf['callback'])) {
                        continue;
                    }

                    $pdf = call_user_func_array([$this->pdfGenerateService, $generatePdf['callback']], $generatePdf['arguments']);
                    if ($pdf) {
                        $attachment = new \Swift_Attachment($pdf, $generatePdf['filename'], 'application/pdf');
                        $message->attach($attachment);
                    }
                } Catch (\Exception $e) {}
            }
        }
        //    generatePdf



        $send = $this->mailer->send($message);

        if ($send) {
            $email->setStatus(Email::STATUS_SEND);
        } else {
            $email->setStatus(Email::STATUS_ERROR);
        }

        $this->entityManager->persist($email);
        $this->entityManager->flush();

        return $send;
    }

    /**
     * @param string $template
     * @param string[]|string $to
     * @param string $subject
     * @param array $params
     */
    public function send($template, $to, $subject, array $params = [])
    {
        $params['subject'] = $subject;

        $params['body'] = $this->prepare($this->templating->render('/site/email/' . $template . '.html.twig', ['params' => $params]), $params);

        $message = (new \Swift_Message($subject))
            ->setFrom($this->getFromEmail())
            ->setTo($to)
            ->setBody($params['body'], 'text/html')
        ;

        if (!empty($params['files'])) {
            foreach ($params['files'] as $fileName => $filePath) {
                $extension = pathinfo($this->documentRoot.$filePath, PATHINFO_EXTENSION);
                $attachment = \Swift_Attachment::fromPath($this->documentRoot.$filePath)
                    ->setFilename((int)$fileName > 0 ? basename($filePath) : $fileName.'.'.$extension);
                $message->attach($attachment);
            }
        }

        $send = $this->mailer->send($message);

        return $send;
    }

    public function registerEmail($emailTemplate, $to, array $params = [], $template = 'empty')
    {
        if ($emailTemplate instanceof EmailTemplate == false) {
            $emailTemplate = $this->getTemplateByExternalId($emailTemplate);
        }

        $params['body'] = $this->prepare($emailTemplate->getBody(), $params);
        $params['subject'] = $this->prepare($emailTemplate->getSubject(), $params);

        $params['body'] = $this->prepare($this->templating->render('/site/email/' . $template . '.html.twig', ['params' => $params]), $params);

        if (!empty($emailTemplate->getFiles())) {
            /** @var File $file */
            foreach ($emailTemplate->getFiles() as $fileIndex => $file) {
                /** @var BaseProvider $provider */
                $provider = $this->container->get($file->getMedia()->getProviderName());

                $filePath = $provider->generatePublicUrl($file->getMedia(), 'reference');

                $fileName = $file->getGroup().' '.$file->getTitle();
                if (trim($fileName)) {
                    $params['files'][$fileName] = $filePath;
                } else {
                    $params['files'][] = $filePath;
                }
            }
        }

        $to = (array)$to;
        $to = array_unique($to);

        $email = $this->createEmail(
            $params['subject'],
            null,
            $to,
            $params['body'],
            $params
        );

        return $email->getId() > 0 ? $email : false;
    }


    /**
     * Рендерим просто из твиг шаблона
     *
     * @param $templatePath
     * @param $subject
     * @param $to
     * @param array $params
     * @param string $template
     * @return Email|bool
     */
    public function registerEmailHtmlTemplate($templatePath, $subject, $to, array $params = [])
    {
        try {
            $params['subject'] = $subject;
            $params['body'] = $this->templating->render('/emails/' . $templatePath, $params);
        } Catch (\Exception $e) {
             // @todo добавить логирование
            return false;
        }

        $to = (array)$to;
        $to = array_unique($to);

        $email = $this->createEmail(
            $params['subject'],
            null,
            $to,
            $params['body'],
            $params
        );

        return $email->getId() > 0 ? $email : false;
    }

}
