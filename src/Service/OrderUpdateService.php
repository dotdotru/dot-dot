<?php

namespace App\Service;

use App\Entity\Batch;
use App\Entity\InsuredQueue;
use App\Entity\Order;
use App\Entity\Package;
use App\Entity\Person;
use App\Entity\Wms\WmsOrder;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use Doctrine\ORM\EntityManager;

class OrderUpdateService
{
    /** @var null|EntityManager */
    private  $entityManager = null;

    /** @var null|IntegrationBaseService */
    private  $integrationBaseService = null;

    /** @var null|OrderMapperService */
    private  $orderMapperService = null;

    public function __construct(
        EntityManager $entityManager,
        IntegrationBaseService $integrationBaseService,
        OrderMapperService $orderMapperService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationBaseService = $integrationBaseService;
        $this->orderMapperService = $orderMapperService;
    }



    public function orderSynch(Order $order)
    {
        $data = $this->orderMapperService->mapEntityCustomerToBase($order);
        $result = $this->integrationBaseService->updateOrder($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$order->getExternalId()) {
            $order->setExternalId($result['data']);
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }


        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function orderSuspend(Order $order)
    {
        if (!$order->getExternalId()) {
            return false;
        }

        $data = $this->orderMapperService->mapOrderToBaseSuspend($order);
        $result = $this->integrationBaseService->orderSuspend($data);

        return $result[IntegrationBaseService::RESULT_SUCCESS];
    }

    public function updateOrder(Order $order, $orderData)
    {
        // Костыль для завершения заказа с последней милей в вмс
        if ($order->getShippingDeliver() && $order->getShippingDeliver()->isActive()) {
            if ($orderData['status'] == Order::STATUS_DELIVERED) {
                $wmsOrder = $this->entityManager->getRepository(WmsOrder::class)->findOneBy(['externalId' => $order->getId()]);
                if ($wmsOrder && $wmsOrder->getStatus() === WmsOrder::STATUS_READY) {
                    $wmsOrder->setIssueAt(new \DateTime());
                    $wmsOrder->setStatus(WmsOrder::STATUS_DELIVERED);
                    $this->entityManager->flush();
                }
            }
        }

        $order = $this->orderMapperService->mapBaseToCustomerEntity($order, $orderData);

        return $order;
    }

    public function updatePerson(Person $person)
    {
        $data = $this->orderMapperService->mapPersonToData($person);
        $result = $this->integrationBaseService->saveActor($data);
        if ($result[IntegrationBaseService::RESULT_SUCCESS] && !$person->getExternalId()) {
            $person->setExternalId($result['data']);
            $this->entityManager->persist($person);
            $this->entityManager->flush();
        }

        return $person;
    }

    public function updateBatch(Batch $order)
    {
        if (!$order->getExternalId()) {
            return false;
        }

        $result = $this->integrationBaseService->getBatch($order->getExternalId());

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {

            if (!empty($result['data'])) {
                $order = $this->orderMapperService->mapBaseBatchToEntity($order, $result['data']);

                $this->entityManager->persist($order);

                $this->updateBatchPackages($result['data']);

                $this->entityManager->flush();
            }

        } else {
            if (!empty($result['error']) && preg_match('#batch (\d)+ not found#', $result['error']['details']['message_primary'])) {
                $order->setRemove();
                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }
        }
    }


    /**
     * Устанавливаем время прибытия грузов
     *
     * @param $data
     * @return bool
     * @throws \Exception
     */
    protected function updateBatchPackages ($data)
    {
        // Обновляем время прибытия грузов
        $arrivedDate = null;
        if ($data['status'] === Batch::STATUS_ARRIVED) {
            foreach ($data['history'] as $history) {
                if ($history['status'] === Batch::STATUS_ARRIVED) {
                    $arrivedDate = new \DateTime($history['time']);
                    break;
                }
            }
        }

        if (!$arrivedDate) {
            return false;
        }

        $packageRepository = $this->entityManager->getRepository(Package::class);
        if (!empty($data['pallets'])) {
            foreach ($data['pallets'] as $pallet) {
                if (!empty($pallet['cargos'])) {
                    foreach ($pallet['cargos'] as $cargo) {
                        $cargoId = $cargo['cargo']['id'] . $cargo['cargo']['ext_id'];
                        $cargoEntity = $packageRepository->findOneBy(['externalId' => $cargoId]);
                        if ($cargoEntity) {
                            $cargoEntity->setArrivedAt($arrivedDate);
                        }
                    }
                }
            }
        }

        return true;
    }





}
