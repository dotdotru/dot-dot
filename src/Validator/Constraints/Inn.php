<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use App\Validator\InnValidator;

/**
 * Validator constraint for INN number
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Inn extends Constraint
{
    public $message = 'Inn.not_valid_inn';

    public function validatedBy()
    {
        return InnValidator::class;
    }
}
