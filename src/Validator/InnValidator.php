<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Validator\Constraints\Inn;

/**
 * Validator constraint for INN number
 *
 */
class InnValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * Правила валидации ИНН
     * http://www.egrul.ru/test_inn.html
     */
    public function validate($value, Constraint $constraint)
    {
        $error = false;

        if (!$constraint instanceof Inn) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Inn');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $error = $this->isInvalidInn($value);

        if ($error) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }

    public function isInvalid($value):bool
    {
        $error = false;

        if (!preg_match('/^\d+$/', $value)) {
            $error = true;
        }

        $inn = str_split($value);

        if (count($inn) == 10) {
            $control = ((2 * $inn[0] + 4 * $inn[1] + 10 * $inn[2] + 3 * $inn[3]
                        + 5 * $inn[4] + 9 * $inn[5] + 4 * $inn[6] + 6 * $inn[7] + 8 * $inn[8]) % 11) % 10;

            if ($control != $inn[9]) {
                $error = true;
            }
        } elseif (count($inn) == 12) {
            $control1 = ((7 * $inn[0] + 2 * $inn[1] + 4 * $inn[2] + 10 * $inn[3]
                        + 3 * $inn[4] + 5 * $inn[5] + 9 * $inn[6] + 4 * $inn[7]
                        + 6 * $inn[8] + 8 * $inn[9]) % 11) % 10;

            $control2 = ((3 * $inn[0] + 7 * $inn[1] + 2 * $inn[2] + 4 * $inn[3]
                        + 10 * $inn[4] + 3 * $inn[5] + 5 * $inn[6] + 9 * $inn[7]
                        + 4 * $inn[8] + 6 * $inn[9] + 8 * $inn[10]) % 11) % 10;

            if ($control1 != $inn[10] || $control2 != $inn[11]) {
                $error = true;
            }
        } else {
            $error = true;
        }

        return $error;
    }
}
