<?php

namespace App\Controller;
use App\Form\UserType;
use App\Application\Sonata\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/auth", name="app.security.auth")
     */
    public function authAction(Request $request)
    {
        $response = [
            'status' => true,
            'groups' => []
        ];

        $user = $this->getUser();
        if ($user && $user->getGroups()) {
            foreach ($user->getGroups() as $group) {
                $response['groups'][] = $group->getName();
            }
        }

        return $this->json($response);
    }

    /**
     * @Route("/register", name="app.security.register")
     */
    public function registerAction(Request $request)
    {
        return $this->render(
            'site/security/register.html.twig'
        );
    }


    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('site/security/auth.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

}