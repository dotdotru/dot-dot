<?php

namespace App\Controller;

use App\Entity\Base\Direction;
use App\Entity\BatchMile;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\Pdf\GenerateService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route("/pdf")
 */
class PdfController extends Controller
{

    protected $generateService;

    public function __construct(
        EntityManagerInterface $em,
        GenerateService $generateService
    )
    {
        $this->em = $em;
        $this->generateService = $generateService;
    }

    /**
     * @Route("/order/{type}/{order}", name="app.pdf.order")
     */
    public function pdfOrderAction(Request $request, $type, Order $order)
    {
        $user = $this->getUser();
        $filename = $type . '-' . $order->getId() . '.pdf';

        if (!$user || !$order || $order->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException();
        }

        $pdf = $this->generateService->generateClient($order->getId(), $type);
        if (!$pdf) {
            throw $this->createNotFoundException();
        }

        $clientGeneratePdf[] = [
            'callback' => 'generateClient',
            'arguments' => [$order->getId(), 'driver-confirm']
        ];

        return new PdfResponse($pdf, $filename);
    }


    /**
     * @Route("/batch/mile/{type}/{batchMile}", name="app.pdf.batch.mover")
     */
    public function pdfMileBatchAction(Request $request, $type, BatchMile $batchMile)
    {
        $user = $this->getUser();
        if (!$user || !$batchMile->getCarrier() || $user->getId() !== $batchMile->getCarrier()->getId()) {
            return $this->createNotFoundException();
        }
        if (!in_array($type, ['tn', 'er'])) {
            return $this->createNotFoundException();
        }

        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        $filename = $type . $batchMile->getId()  . '.pdf';
        $dataTemplate = [
            'batch'  => $batchMile,
            'order' => $batchMile->getOrder(),
            'carrier' => $batchMile->getCarrier(),
            'offerDate' => $offerDate
        ];
        $pathTempalate = 'site/pdf/batch_mover/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }

}
