<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Repository\OrderRepository;
use App\Service\Stock\StockListForPublic;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StockController extends Controller
{

    /**
     * @Route("/stock/map", name="app.stock.map")
     */
    public function mapAction(Request $request)
    {
        /**
         * @var StockListForPublic $stockListService;
         */
        $stockListService = $this->get('app.service.stock.for_public');

        return $this->render('site/stock/map.html.twig', [
            'stocks' => $stockListService->getStocks()
        ]);
    }

    /**
     * @Route("/stock/print/{id}", name="app.stock.print")
     */
    public function mapPrintAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $stockRepository = $entityManager->getRepository(Stock::class);

        $stock = $stockRepository->find($id);
        if (!$stock) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/stock/print.html.twig', [
            'stock' => $stock
        ]);
    }

}
