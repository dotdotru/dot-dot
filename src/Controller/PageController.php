<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Stock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * @Route("/page/{slug}", name="app.page.default")
     */
    public function detailAction(Request $request, $slug)
    {
        $pageRepository = $this->getDoctrine()->getRepository(Page::class);

        $page = $pageRepository->findOneBy(['slug' => $slug, 'published' => true]);

        if (!$page) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/page/page.html.twig', ['page' => $page]);
    }
}
