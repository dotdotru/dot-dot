<?php

namespace App\Controller;

use App\Entity\Content\Block\Block;
use App\Entity\Content\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class ArticleController extends Controller
{
    /**
     * @Route("/article/{code}", name="app.article.detail")
     */
    public function aboutAction(Request $request, $code)
    {
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);

        /** @var Article $article */
        $article = $articleRepository->findOneBySlug($code);

        if (!$article || !$article->isPublished()) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/article/detail.html.twig', ['article' => $article]);
    }
}
