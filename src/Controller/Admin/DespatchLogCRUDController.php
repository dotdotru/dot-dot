<?php

namespace App\Controller\Admin;

use App\Entity\DispatchLog;
use App\Entity\Organization\Organization;
use App\Repository\Organization\OrganizationRepository;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Response;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DespatchLogCRUDController extends Controller
{

    public function downloadSubscribeAction()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $writer = new Xlsx($spreadsheet);

        $publicDirectory = $this->get('kernel')->getProjectDir() . '/web';

        $fileName = sprintf('Подписки_%s.xlsx', date('d.m.Y H:i:s'));
        $tempFile = $publicDirectory.'/tmp/'.$fileName;

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);
        $dispatchLogRepository = $this->getDoctrine()->getRepository(DispatchLog::class);

        $logs = $dispatchLogRepository->findBy([], ['id' => 'desc']);

        $result = ['Пользователь', 'Email', 'Группы', 'Зареген да/нет', 'Подписан да/нет'];
        $sheet->fromArray($result, NULL, 'A1');

        $i = 1;
        /** @var DispatchLog $log */
        foreach ($logs as $log) {
            $name = []; 
            $name[] = $log->getUserFio();
            if ($log->getUser()) {
                $organization = $organizationRepository->getCustomerByUser($log->getUser());
                if ($organization) {
                    $name[] = $organization->getCompanyName();
                }
                
                $organization = $organizationRepository->getCarrierByUser($log->getUser());
                if ($organization) {
                    $name[] = $organization->getCompanyName();
                }
            }
            
            $name = array_filter($name);
            $name = implode(',', $name);

            if (!isset($result[$log->getUserEmail()])) {
                $i++;
                $result[$log->getUserEmail()] = [];

                $item = [
                    $name,
                    $log->getUserEmail(),
                    $log->getUserGroups(),
                    $log->getUser() ? 'да' : 'нет',
                    $log->isSubscribe() ? 'да' : 'нет',
                ];

                $result[$log->getUserEmail()] = $item;

                $sheet->fromArray([$item], NULL, 'A'.$i);
            }
        }

        $writer->save($tempFile);

        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
