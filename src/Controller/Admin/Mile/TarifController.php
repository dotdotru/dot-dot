<?php

namespace App\Controller\Admin\Mile;

use App\Form\Integration\MileTariffType;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\GetMileTariffs;
use App\Service\Integration\Mile\SaveMileTariffs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/mile/tarif")
 */
class TarifController extends AbstractController
{

    /**
     * @var CallBaseService
     */
    protected $caller;


    public function __construct(CallBaseService $caller)
    {
        $this->caller = $caller;
    }


    /**
     * @Route("/", name="admin_mile_tarif", methods={"GET|POST"})
     */
    public function index(Request $request): Response
    {
        $mileTariffs = new GetMileTariffs();
        $tariffs = $this->caller->exec($mileTariffs);

        $builder = $this->createFormBuilder()
            ->add('collection', CollectionType::class, [
                'entry_type' => MileTariffType::class,
                'allow_add' => true
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить тарифы']);

        $form = $builder->getForm();
        $form->setData(['collection' => $tariffs['data']]);

        if ($form->handleRequest($request) && $form->isSubmitted() && $form->isValid()) {
            $tariffs = $form->getData()['collection'];

            foreach ($tariffs as $tariff) {
                if (!empty($tariff['delete'])) {

                } else {
                    $saveMileTariff = new SaveMileTariffs($tariff);
                    $this->caller->exec($saveMileTariff);
                }
            }
        }

        return $this->render('admin/miles_tariffs/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
