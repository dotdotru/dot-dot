<?php

namespace App\Controller\Admin\Mile;

use App\Entity\City;
use App\Form\Integration\MilePriceType;
use App\Form\Integration\MilePriceTypeCollection;
use App\Form\Integration\MileTariffType;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\DeleteMilePrice;
use App\Service\Integration\Mile\GetMilePrices;
use App\Service\Integration\Mile\GetMileTariffs;
use App\Service\Integration\Mile\SaveMilePrice;
use App\Service\Integration\Mile\SaveMileTariffs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/admin/mile/city/price")
 */
class CityPriceController extends AbstractController
{

    /**
     * @var CallBaseService
     */
    protected $caller;


    protected $em;


    public function __construct(EntityManagerInterface $em, CallBaseService $caller)
    {
        $this->caller = $caller;
        $this->em = $em;
    }


    /**
     * @Route("/{city}", name="admin_mile_city_price", methods={"GET|POST"})
     */
    public function index(Request $request, City $city): Response
    {
        $mileTariffs = new GetMileTariffs();
        $tariffs = $this->caller->exec($mileTariffs);

        $distances = ['10', '20', '30', '40', '50'];
        $weights = ['100', '250', '500', '1000', '1500', '2000', '2500', '3000', '5000', '7000', '10000','25000'];

        $pricesVariants = [];
        foreach ($weights as $weight) {
            foreach ($distances as $distance) {
                $pricesVariants[] = [
                    'distance' => $distance,
                    'weight' => $weight,
                    'price' => null,
                    'per_km' => false
                ];
            }
            $pricesVariants[] = [
                'distance' => null,
                'weight' => $weight,
                'price' => null,
                'per_km' => true
            ];
        }

        $baseCity = $this->em->getRepository(\App\Entity\Base\City::class)->findOneBy(['title' => $city->getTitle()]);

        $resultPrices = [];
        foreach ($tariffs['data'] as $key => $tariff) {
            $milePrices = new GetMilePrices();
            $milePrices->setCityId($baseCity->getExternalId())
                ->setTariff($tariff['id']);

            $prices = $this->caller->exec($milePrices);

            $resultPrices[$key]['tarif_name'] = 'Грузчиков: ' . $tariff['loaders'] . ' Гидроборт: ' . $tariff['tail_lift'];
            $resultPrices[$key]['tarif_id'] = $tariff['id'];

            $tmpPricesVariants = $pricesVariants;

            foreach ($prices['data'] as $value) {
                // @todo вынести разбор полей в сам реквест
                $distance = explode(',', trim($value['distance'], '()[]'));
                $weight = explode(',', trim($value['weight'], '()[]'));

                if (!empty($distance[0]) && !empty($distance[1])) {
                    $distance = $distance[1];
                } else if (empty($distance[0])) {
                    $distance = $distance[1];
                } else if (empty($distance[1])) {
                    $distance = $distance[0];
                }

                if (!empty($weight[0]) && !empty($weight[1])) {
                    $weight = $weight[1];
                } else if (empty($weight[0])) {
                    $weight = $weight[1];
                } else if (empty($distance[1])) {
                    $weight = $weight[0];
                }

                foreach ($tmpPricesVariants as $k => $v) {
                    if ($tmpPricesVariants[$k]['distance'] == $distance && $tmpPricesVariants[$k]['weight'] == $weight && !$value['per_km']) {
                        $tmpPricesVariants[$k]['price'] = $value['per_km'] ? $value['per_km'] : $value['price'];
                        $tmpPricesVariants[$k]['id'] = $value['id'];
                    }
                    if ($tmpPricesVariants[$k]['weight'] == $weight && $value['per_km'] && $v['per_km']) {
                        $tmpPricesVariants[$k]['price'] = $value['per_km'];
                        $tmpPricesVariants[$k]['id'] = $value['id'];
                    }
                }
            }

            $resultPrices[$key]['prices'] = $tmpPricesVariants;
        }

        $builder = $this->createFormBuilder()
            ->add('collection', CollectionType::class, [
                'entry_type' => MilePriceTypeCollection::class,
                'allow_add' => true
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить цены']);

        $form = $builder->getForm();
        $form->setData(['collection' => $resultPrices]);

        if ($form->handleRequest($request) && $form->isSubmitted() && $form->isValid()) {
            $tariffs = $form->getData()['collection'];

            $rangeCallback = function($array, $item, $lastRange = true) {
                $key = array_search($item, $array);

                if ($key === -1) {
                    return null;
                }
                if ($key - 1 < 0) {
                    return '(0,' . $item . ']';
                }
                if (!$lastRange && $key == count($array) - 1) {
                    return '(' . $item . ',]';
                }
                return '(' . $array[$key - 1] . ',' . $item . ']';
            };


            foreach ($tariffs as $tariff) {
                $this->formatPrices($tariff['prices']);

                $failedRequests = [];
                foreach ($tariff['prices'] as $key => $price) {
                    if ($price['price']) {
                        $distance = $rangeCallback($distances, $price['distance']);
                        $weight = $rangeCallback($weights, $price['weight'], true);
                        $priceValue = $price['price'];
                        $perKm = 0;

                        if ($price['per_km']) {
                            $maxDistance = 0;
                            $priceValue = null;
                            foreach ($tariff['prices'] as $tmpKey => $tmpPrice) {
                                if ($price['weight'] == $tmpPrice['weight'] && !$tmpPrice['per_km'] && $tmpPrice['price']) {
                                    $maxDistance = $tmpPrice['distance'];
                                    $priceValue = $tmpPrice['price'];
                                }
                            }

                            if (!$maxDistance || !$priceValue) {
                                continue;
                            }

                            $perKm = $price['price'];
                            $distance = '(' . $maxDistance . ',)';
                        }

                        $savePrice = new SaveMilePrice();

                        $savePrice->setId($price['id']);
                        $savePrice->setCityId($baseCity->getExternalId());
                        $savePrice->setTarifId($tariff['tarif_id']);
                        $savePrice->setDistance($distance);
                        $savePrice->setWeight($weight);
                        $savePrice->setPrice($priceValue);
                        $savePrice->setPerKm($perKm);

                        $answer = $this->caller->exec($savePrice);

                        if (!$answer['success']) {
                            $failedRequests[] = $savePrice;
                        }

                    } else {
                        if ($price['id']) {
                            $deletePrice = new DeleteMilePrice();
                            $deletePrice->setId($price['id']);

                            $this->caller->exec($deletePrice);
                        }
                    }
                }

                // Костыль из-за того, что диапазоны обновляются не по порядку
                foreach ($failedRequests as $failedRequest) {
                    $this->caller->exec($failedRequest);
                }
            }
        }

        return $this->render('admin/miles_city_price/index.html.twig', [
            'form' => $form->createView(),
            'distances' => $distances,
            'weights' => $weights
        ]);
    }


    protected function formatPrices ($prices)
    {
//dump($prices);
    }

}
