<?php

namespace App\Controller;

use App\Entity\Base\Direction;
use App\Entity\Organization\Organization;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\Response;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/wms/pdf")
 */
class WmsPdfController extends Controller
{
    /**
     * @Route("/pallet/{type}/{orderId}", name="app.wms.pdf.pallet")
     */
    public function pdfPalletAction(Request $request, $type, $orderId)
    {
        $repository = $this->getDoctrine()->getRepository(WmsPallet::class);
        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        $pallet = $repository->findOneByExternalId($orderId);

        if (!$pallet) {
            throw $this->createNotFoundException();
        }

        $filename = $type.$orderId.'.pdf';
        $dataTemplate = ['pallet'  => $pallet, 'offerDate' => $offerDate];
        $pathTempalate = 'wms/pdf/pallet/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        //return $this->render($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }

    /**
     * @Route("/order/{type}/{orderId}", name="app.wms.pdf.order")
     */
    public function pdfOrderAction(Request $request, $type, $orderId)
    {
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);
        $wmsPalletRepository = $this->getDoctrine()->getRepository(WmsPallet::class);

        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->findOneByExternalId($orderId);

        if (!$order) {
            throw $this->createNotFoundException();
        }

        $palletIds = [];

        $pallets = $wmsPalletRepository->findBy([], ['id' => 'desc']);

        /** @var WmsPackage $package */
        foreach ($pallets as $pallet) {
			foreach ($pallet->getRealPackages() as $package) {
				foreach($order->getPackages() as $opackage) {
					if ($package->getId() == $opackage->getId()) {
						$palletIds = $pallet->getId();
					}
				}
			}
        }

        $batches = [];
        if ($palletIds) {
            $pallets = $wmsPalletRepository->findBy(['id' => $palletIds]);

            /** @var WmsPallet $pallet */
            foreach ($pallets as $pallet) {
                if ($pallet->getBatch()) {
                    $batches[] = $pallet->getBatch();
                }
            }
        }

        $filename = $type.$orderId.'.pdf';
        $dataTemplate = ['order'  => $order, 'batches' => $batches, 'offerDate' => $offerDate];
        $pathTempalate = 'wms/pdf/order/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        //return $this->render($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }

    /**
     * @Route("/batch/{type}/{batchId}", name="app.wms.pdf.batch")
     */
    public function pdfBatchAction(Request $request, $type, $batchId)
    {
        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);
        $wmsBatchRepository = $this->getDoctrine()->getRepository(WmsBatch::class);
        $baseDirectionRepository = $this->getDoctrine()->getRepository(Direction::class);

        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        /** @var WmsBatch $batch */
        $batch = $wmsBatchRepository->findOneByExternalId($batchId);

        if (!$batch) {
            throw $this->createNotFoundException();
        }

        $organization = $organizationRepository->getCarrierByUser($batch->getCarrier());
        $batch->setOrganization($organization);

        $baseDirection = $baseDirectionRepository->findOneByExternalId($batch->getDirection()->getExternalId());
        list($carrierTime, $i, $s) = explode(':', $baseDirection->getCarrierDeliveryTime());

        $filename = $type.$batchId.'.pdf';
        $dataTemplate = ['batch'  => $batch, 'carrierTime' => $carrierTime, 'offerDate' => $offerDate];
        $pathTempalate = 'wms/pdf/batch/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        //return $this->render($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }

    /**
     * @Route("/batch_mover/{type}/{batchId}", name="app.wms.pdf.batch_mover")
     */
    public function pdfBatchMoverAction(Request $request, $type, $batchId)
    {
        if (!in_array($type, ['act', 'act_completed', 'act_discrepancy', 'tn'])) {
            throw new \Exception('Unable to generate doc');
        }

        $wmsBatchRepository = $this->getDoctrine()->getRepository(WmsBatchMover::class);

        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        /** @var WmsBatch $batch */
        $batch = $wmsBatchRepository->findOneByExternalId($batchId);

        if (!$batch) {
            throw $this->createNotFoundException();
        }

        $filename = $type.$batchId.'.pdf';
        $dataTemplate = ['batch'  => $batch, 'offer_date' => $offerDate];
        $pathTempalate = 'wms/pdf/batch_mover/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }

    /**
     * @Route("/order/marking-package/{orderId}/{packageGroupId}", name="app.wms.pdf.order-marking")
     */
    public function pdfMarkingOrderAction(Request $request, $orderId, $packageGroupId = false)
    {
        $type = 'marking-package';
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);

        $settings = $this->get('settings');
        $offerDate = $settings->get('offer_date');

        $order = $wmsOrderRepository->findOneByExternalId($orderId);

        if (!$order) {
            throw $this->createNotFoundException();
        }

        $filename = $type.$orderId.'.pdf';
        $dataTemplate = ['order'  => $order, 'packageGroupId'  => $packageGroupId, 'offerDate' => $offerDate];
        $pathTempalate = 'wms/pdf/order/'.$type.'.html.twig';

        $html = $this->renderView($pathTempalate, $dataTemplate);

        //return $this->render($pathTempalate, $dataTemplate);

        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html), $filename);
    }
}
