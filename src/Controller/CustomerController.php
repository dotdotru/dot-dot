<?php

namespace App\Controller;

use App\Entity\Direction;
use App\Entity\Pallet;
use App\Entity\PalletTest;
use App\Entity\Stock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends Controller
{
    /**
     * @Route("/customer", name="app.customer.index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('site/customer/index.html.twig', []);
    }
}
