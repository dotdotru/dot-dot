<?php

namespace App\Controller\Personal;

use App\Entity\Direction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class MilesController extends AbstractController
{
    /**
     * @Route("/miles", name="app.persoinal.miles", options={"sitemap" = {"priority" = 0.8 }})
     */
    public function auctionAction(Request $request)
    {
        return $this->render('site/miles/auction.html.twig', [
            'header' => '',
            'subheader' => ''
        ]);
    }

}
