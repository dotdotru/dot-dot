<?php

namespace App\Controller;

use App\Entity\Direction;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AuctionController extends Controller
{
    /**
     * @Route("/auction", name="app.auction.index", options={"sitemap" = {"priority" = 0.8 }})
     */
    public function indexAction(Request $request)
    {
        /** @var Settings $settings */
        $settings = $this->get('settings');
        $settings = $settings->group('auction');

        $direction = $this->getDoctrine()->getRepository(Direction::class)->findOneBy(['id' => 1, 'published' => true]);

		$seoPage = $this->container->get('sonata.seo.page');
		$seoPage
			->setTitle('Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
			->addMeta('name', 'title', 'Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
			->addMeta('property', 'og:title', 'Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
			->addMeta('property', 'og:image', 'https://dot-dot.ru/img/preview_auction.jpg')
			->addMeta('name', 'description', 'Заработай на грузоперевозках по РФ. У нас много груза, только паллеты. Попутная дозагрузка и обратная загрузка. Без посредников. Бесплатная регистрация. Круглосуточно 8 800 222 33 57')
			->addMeta('property', 'og:description', 'Заработай на грузоперевозках по РФ. У нас много груза, только паллеты. Попутная дозагрузка и обратная загрузка. Без посредников. Бесплатная регистрация. Круглосуточно 8 800 222 33 57')
		;

        return $this->render('site/auction/index.html.twig', [
            'direction' => $direction,
            'header' => $settings['header'],
            'subheader' => $settings['subheader'],
        ]);
    }

    /**
     * @Route("/auction/d-{slug}", name="app.auction.direction")
     */
    public function indexDirectionAction(Request $request, $slug)
    {
        /** @var Settings $settings */
        $settings = $this->get('settings');
        $settings = $settings->group('auction');

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle('Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
            ->addMeta('name', 'title', 'Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
            ->addMeta('property', 'og:title', 'Заработок на грузоперевозке: поиск попутного груза по России, дозагрузка | Точка-Точка')
            ->addMeta('property', 'og:image', 'https://dot-dot.ru/img/preview_auction.jpg')
            ->addMeta('name', 'description', 'Заработай на грузоперевозках по РФ. У нас много груза, только паллеты. Попутная дозагрузка и обратная загрузка. Без посредников. Бесплатная регистрация. Круглосуточно 8 800 222 33 57')
            ->addMeta('property', 'og:description', 'Заработай на грузоперевозках по РФ. У нас много груза, только паллеты. Попутная дозагрузка и обратная загрузка. Без посредников. Бесплатная регистрация. Круглосуточно 8 800 222 33 57')
        ;

        $direction = $this->getDoctrine()->getRepository(Direction::class)->findOneBy(['slug' => $slug, 'published' => true]);

        if (!$direction) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/auction/index.html.twig', [
            'header' => $settings['header'],
            'subheader' => $settings['subheader'],
            'direction' => $direction
        ]);
    }
}
