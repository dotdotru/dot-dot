<?php

namespace App\Controller;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Repository\OrderRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TrackController extends Controller
{
    /**
     * @Route("/track", name="app.track.index", options={"sitemap" = {"priority" = 0.8 }})
     */
    public function indexAction(Request $request)
    {
        return $this->render('site/track/index.html.twig', []);
    }

    /**
     * @Route("/track/{id}", name="app.track.detail")
     */
    public function detailAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        /** @var OrderRepository $orderCustomerRepository */
        $orderCustomerRepository = $entityManager->getRepository(Order::class);

        $order = $orderCustomerRepository->getById($id);
        $hash = $request->query->get('hash');

        if (!$order) {
            throw $this->createNotFoundException();
        }

        if (
            $user != $order->getUser()
            && $order->getHash() != $hash
        ) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/track/detail.html.twig', [
            'order' => $order
        ]);
    }
}
