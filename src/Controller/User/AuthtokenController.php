<?php

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AuthtokenController extends Controller
{

    /**
     * @Route("/personal/authtokens", name="app.personal.authtokens")
     */
    public function orderSuccessAction(Request $request)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        return $this->render('site/user/authtoken.html.twig');
    }

}
