<?php

namespace App\Controller;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPallet;
use App\Repository\DirectionRepository;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\StockRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\Wms\SecurityService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use mysql_xdevapi\Exception;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Direction;

/**
 * @Route("/wms")
 */
class WmsController extends Controller
{
    /**
     * @Route("/", name="app.wms.orders")
     */
    public function ordersAction(Request $request)
    {
        return $this->render('wms/order/index.html.twig');
    }

    /**
     * @Route("/order/delivered/{id}", name="app.wms.order_delivered")
     */
    public function orderDeliveredAction(Request $request, $id)
    {
        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);

        $wmsOrder = $wmsOrderRepository->getByExternalId($id);

        if (!$wmsOrder) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/order/delivered.html.twig', ['order' => $wmsOrder]);
    }

    /**
     * @Route("/batch", name="app.wms.batch")
     */
    public function batchAction(Request $request)
    {
        return $this->render('wms/batch/index.html.twig');
    }

    /**
     * @Route("/batch/load/{id}", name="app.wms.batch_load")
     */
    public function batchShipAction(Request $request, $id)
    {
        /** @var WmsBatchRepository $orderRepository */
        $batchRepository = $this->getDoctrine()->getRepository(WmsBatch::class);

        /** @var WmsBatch $batch */
        $batch = $batchRepository->getByExternalId($id);

        if (!$batch /*|| $batch->getReceptionAt()*/) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/batch/load.html.twig', ['order' => $batch]);
    }

    /**
     * @Route("/batch-shipping/load/{id}", name="app.wms.batch_shipping_load")
	 * @Route("/batch-mover/load/{id}", name="app.wms.batch_mover_load")
     */
    public function batchMoverShipAction(Request $request, $id)
    {
        /** @var WmsBatchRepository $orderRepository */
        $batchRepository = $this->getDoctrine()->getRepository(WmsBatchMover::class);

        /** @var WmsBatchMover $batch */
        $batch = $batchRepository->getByExternalId($id);

        if (!$batch /*|| $batch->getReceptionAt()*/) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/batch_shipping/load.html.twig', ['order' => $batch]);
    }

    /**
     * @Route("/batch/arrived/{id}", name="app.wms.batch_arrived")
     */
    public function batchArrivedAction(Request $request, $id)
    {
        /** @var WmsBatchRepository $orderRepository */
        $batchRepository = $this->getDoctrine()->getRepository(WmsBatch::class);

        $batch = $batchRepository->getByExternalId($id);

        if (!$batch) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/batch/arrived.html.twig', ['order' => $batch]);
    }

    /**
     * @Route("/batch-shipping/arrived/{id}", name="app.wms.batch_shipping_arrived")
     * @Route("/batch-mover/arrived/{id}", name="app.wms.batch_mover_arrived")
     */
    public function batchShippingArrivedAction(Request $request, $id)
    {
        /** @var WmsBatchRepository $orderRepository */
        $batchRepository = $this->getDoctrine()->getRepository(WmsBatchMover::class);

        $batch = $batchRepository->getByExternalId($id);

        if (!$batch) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/batch_shipping/arrived.html.twig', ['order' => $batch]);
    }

    /**
     * @Route("/create", name="app.wms.order_create")
     */
    public function orderCreateAction(Request $request)
    {
        /** @var DirectionRepository $directionRepository */
        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        $userRepository = $this->getDoctrine()->getRepository(User::class);

        /** @var \Vsavritsky\SettingsBundle\Service\Settings $settingsService */
        $settingsService = $this->get('settings');

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);

        /** @var StockRepository $stockRepository */
        $stockRepository = $this->getDoctrine()->getRepository(Stock::class);


        /** @var Stock $stockFrom */
        $stockFrom = $securityService->getStock();

        $wmsOrder = new WmsOrder();
        $wmsOrder->setTmp(true);

        $order = new Order();
        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();

        $wmsOrder->setExternalId($order->getId());
        $wmsOrder->setStockFrom($stockFrom);
        $wmsOrder->setOrder($order);

        // Для некоторых складских для заказа доступен только 1 клиент
        if ($ftlGroup = $stockFrom->getFtlGroup()) {
            // @todo понять как нормально работать с репозиторием
            $qb = $userRepository->createQueryBuilder('u');
            $qb->addSelect('u', 'g')
                ->leftJoin('u.groups', 'g')
                ->andWhere($qb->expr()->eq('g', ':group'))
                ->setParameters([
                    'group' => $ftlGroup,
                ]);

            $ftlUser = $qb->getQuery()->getResult();
            if (!$ftlUser) {
                throw new \Exception('Ftl user does not specified. (specify user for group "' . $ftlGroup->getName() . '")');
            }
            $ftlUser = $ftlUser[0];

            if ($ftlUser) {
                $organization = $organizationRepository->getCustomerByUser($ftlUser);

                $wmsOrder->setCustomer($ftlUser);
                if ($organization) {
                    $wmsOrder->setOrganization($organization);
                }
            }
        }


        $this->getDoctrine()->getManager()->persist($wmsOrder);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app.wms.order_create_item', ['id' => $wmsOrder->getId()]);
    }

    /**
     * @Route("/create/{id}", name="app.wms.order_create_item")
     */
    public function orderCreateItemAction(Request $request, $id)
    {
        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->findOneBy(['id' => $id]);

        if ($wmsOrder->getUser() && $wmsOrder->getSender() == null) {
            $wmsOrder->setSender($wmsOrder->getUser()->getPerson());
        }

        if (!$wmsOrder) {
            throw $this->createNotFoundException();
        }

        return $this->render('wms/order/create.html.twig', ['order' => $wmsOrder]);
    }

    /**
     * @Route("/order/{id}", name="app.wms.order")
     */
    public function orderEditAction(Request $request, $id)
    {
        /** @var WmsOrderRepository $orderRepository */
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);

        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->getDoctrine()->getRepository(Order::class);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->getByExternalId($id);

        if (!$wmsOrder) {
            throw $this->createNotFoundException();
        }

        $shippingPickup = null;
        $order = $orderRepository->find($wmsOrder->getExternalId());
        if ($order) {
            $shippingPickup = $order->getShipping(Shipping::TYPE_PICKUP);
        }

        $print = [
            'er' => $shippingPickup && $shippingPickup->isActive() ? false: true
        ];

        return $this->render('wms/order/edit.html.twig', ['order' => $wmsOrder, 'print' => $print]);
    }

    /**
     * @Route("/pallet/shipping_pallets", name="app.wms.shipping_pallets")
     */
    public function shippingPalletsAction(Request $request)
    {
        /** @var WmsOrderRepository $orderRepository */
        $orderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);

        $orders = $orderRepository->findBy(['status' => WmsOrder::STATUS_ACCEPTED]);

        return $this->render('wms/pallet/shipping_pallets.html.twig',
            ['orders' => $orders]
        );
    }

    /**
     * @Route("/pallet/form_pallets", name="app.wms.form_pallets")
     */
    public function formPalletsAction(Request $request)
    {
        return $this->render('wms/pallet/form_pallets.html.twig');
    }

    /**
     * @Route("/logout", name="app.wms.logout")
     */
    public function logoutAction(Request $request)
    {
        $securityService = $this->get('app.wms.security');

        $securityService->logout();

        return $this->redirectToRoute('app.wms.orders');
    }
}
