<?php

namespace App\Controller;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\OrderPackage;
use App\Entity\PackageType;
use App\Entity\Packing;
use App\Entity\Page;
use App\Entity\Stock;
use App\Entity\Wms\WmsOrder;
use App\Service\CalculateService;
use App\Service\CompetitorCalcService;
use App\Service\Integration\Order\CalcCostRange;
use App\Service\PromocodeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class StaticController extends Controller
{
    /**
     * @Route("/static/popup/error", name="app.static.error")
     */
    public function popupErrorAction(Request $request)
    {
        return $this->render('block/popup/error.html.twig');
    }

    /**
     * @Route("/static/popup/auth", name="app.static.popup_auth")
     */
    public function popupAuthAction(Request $request)
    {
        return $this->render('block/popup/auth.html.twig');
    }

    /**
     * @Route("/static/popup/carrier/auth", name="app.static.popup_carrier_auth")
     */
    public function popupCarrierAuthAction(Request $request)
    {
        return $this->render('block/popup/carrier.auth.html.twig');
    }

    /**
     * @Route("/static/popup/customer/auth", name="app.static.popup_sender_auth")
     */
    public function popupCustomerAuthAction(Request $request)
    {
        return $this->render('block/popup/customer.auth.html.twig');
    }


    /**
     * @Route("/static/popup/need-customer/auth", name="app.static.popup_need_sender_auth")
     */
    public function popupNeedCustomerAuthAction(Request $request)
    {
        return $this->render('block/popup/need-customer.auth.html.twig');
    }


    /**
     * @Route("/static/popup/need-carrier/auth", name="app.static.popup_need_carrier_auth")
     */
    public function popupNeedCarrierAuthAction(Request $request)
    {
        return $this->render('block/popup/need-carrier.auth.html.twig');
    }


    /**
     * @Route("/static/popup/verification_success", name="app.static.verification_success")
     */
    public function popupVerificationSuccessAction(Request $request)
    {
        return $this->render('block/popup/verification_success.html.twig');
    }

    /**
     * @Route("/static/popup/verification_error", name="app.static.verification_error")
     */
    public function popupVerificationErrorAction(Request $request)
    {
        return $this->render('block/popup/verification_error.html.twig');
    }

    /**
     * @Route("/static/popup/verification_error_special", name="app.static.verification_error_special")
     */
    public function popupVerificationErrorSpecialAction(Request $request)
    {
        return $this->render('block/popup/verification_error_special.html.twig');
    }

    /**
     * @Route("/static/popup/verification_delay", name="app.static.verification_delay")
     */
    public function popupVerificationDelayAction(Request $request)
    {
        return $this->render('block/popup/verification_delay.html.twig');
    }

    /**
     * @Route("/static/popup/wms_auth_customer_sender/{id}", name="app.static.wms_auth_customer_sender")
     */
    public function wmsAuthCustomerSenderAction(Request $request, $id)
    {
        $wmsOrder = $this->getDoctrine()->getRepository(WmsOrder::class)->find($id);

        return $this->render('block/popup/wms_auth_customer_sender.html.twig', ['order' => $wmsOrder]);
    }

    /**
     * @Route("/static/popup/verification_status", name="app.static.verification_status")
     */
    public function popupVerificationStatusAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $organizationId = $request->query->get('organizationId');
        if ($organizationId <= 0) {
            throw $this->createAccessDeniedException('organizationId empty');
        }

        $organization = $entityManager->getRepository(Organization::class)->find($organizationId);
        if (!$organization) {
            throw $this->createNotFoundException('organization not found');
        }

        return $this->render('block/popup/verification_status.html.twig', [
            'organization' => $organization
        ]);
    }

    /**
     * @Route("/static/popup/compare", name="app.static.popup_compare")
     */
    public function popupCompareAction(Request $request)
    {
        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');

        /** @var CompetitorCalcService $competitorCalcService */
        $competitorCalcService = $this->get('app.competitor_calc_service');

        $callerBase = $this->get('app.integration.caller');

        $cache = $this->get('cache.app');

        $cacheKey = 'compareCalc'.date('d.m.Y');
        $cacheCompare = $cache->getItem($cacheKey);

        $weights = [30, 100, 1000, 5000];

        $cacheResult = [];
        if (!$cacheCompare->isHit($cacheKey)) {

            $direction = $this->getDoctrine()->getRepository(Direction::class)->findOneById(1);

            $packages = [];
            foreach ($weights as $weight) {

                $package = new OrderPackage();
                if ($weight == 30) {
                    $package->setWidth(10);
                    $package->setHeight(10);
                    $package->setDepth(10);
                    $package->setCount(1);
                    $package->setWeight($weight);
                }
                if ($weight == 100) {
                    $package->setWidth(50);
                    $package->setHeight(50);
                    $package->setDepth(50);
                    $package->setCount(1);
                    $package->setWeight($weight);
                }
                if ($weight == 1000) {
                    $package->setWidth(80);
                    $package->setHeight(100);
                    $package->setDepth(100);
                    $package->setCount(2);
                    $package->setWeight(500);
                }
                if ($weight == 5000) {
                    $package->setWidth(80);
                    $package->setHeight(100);
                    $package->setDepth(100);
                    $package->setCount(10);
                    $package->setWeight(500);
                }

                $package->setPackageTypeAnother('tmp');

                $order = new Order();
                $order->setDirection($direction);
                $order->setDeclaredPrice(1);


                $order->addOrderPackage($package);

                $calcRequest = new CalcCostRange();
                $calcRequest->setOrder($order);
                $calcRequest->setVat(true);
                $orderCostRange = $callerBase->exec($calcRequest)['data'];

                $tmpPackingPrice = $calculateService->calcTotalPackingPrice($order);

                $minPrice = $orderCostRange['cost_min'] + $tmpPackingPrice;
                $maxPrice = $orderCostRange['cost_max'] + $tmpPackingPrice;

                $competitorPricesResult = $competitorCalcService->calc($order);

                array_unshift($competitorPricesResult, [
                    'index' => 0,
                    'name' => 'Точка—Точка*',
                    'price' => sprintf('%s-%s', floor($minPrice), floor($maxPrice))
                ]);

                $cacheResult[$weight] = $competitorPricesResult;
            }

            $cacheCompare->set($cacheResult);
            $cache->save($cacheCompare);
        } else {
            $cacheResult = $cacheCompare->get($cacheKey);
        }

        return $this->render('block/popup/compare.html.twig', ['data' => $cacheResult]);
    }

    /**
     * @Route("/static/popup/graph", name="app.static.popup_graph")
     */
    public function popupGraphAction(Request $request)
    {
        return $this->render('block/popup/graph.html.twig');
    }

    /**
     * @Route("/static/popup/company", name="app.static.popup_company")
     */
    public function popupCompanyAction(Request $request)
    {
        return $this->render('block/popup/company.html.twig');
    }
    /**
     * @Route("/static/popup/time", name="app.static.popup_time")
     */
    public function timeAction(Request $request)
    {
        return $this->render('block/popup/time.html.twig');
    }

    /**
     * @Route("/static/popup/no_drivers", name="app.static.no_drivers")
     */
    public function noDriversAction(Request $request)
    {
        return $this->render('block/popup/no_drivers.html.twig');
    }

    /**
     * @Route("/static/popup/in_stock_delivery", name="app.static.in_stock_delivery")
     */
    public function inStockDeliveryAction(Request $request, $orderId = null)
    {
        if (!$orderId) {
            $orderId = $request->query->getInt('orderId', null);
        }
        return $this->render('block/popup/in_stock_delivery.html.twig', ['orderId' => $orderId]);
    }

    /**
     * @Route("/offer/customer", name="app.static.offer_customer")
     */
    public function offerCustomerAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $pageRepository = $entityManager->getRepository(Page::class);

        $page = $pageRepository->findOneBy(['slug' => 'offer-customer']);

        return $this->render('site/page/offer.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/offer/carrier", name="app.static.offer_carrier")
     */
    public function offerCarrierAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $pageRepository = $entityManager->getRepository(Page::class);

        $page = $pageRepository->findOneBy(['slug' => 'offer-carrier']);

        return $this->render('site/page/offer.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/static/popup/subscribe", name="app.static.subscribe")
     */
    public function subscribeAction(Request $request)
    {
        return $this->render('block/popup/subscribe.html.twig');
    }
}
