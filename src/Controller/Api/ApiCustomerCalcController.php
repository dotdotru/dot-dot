<?php

namespace App\Controller\Api;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\OrderPackage;
use App\Entity\Packing;
use App\Entity\Promocode;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Form\OrderPackageType;
use App\Form\OrderType;
use App\Service\Ad\UtmService;
use App\Service\AuctionService;
use App\Service\CalculateService;
use App\Service\CompetitorCalcService;
use App\Service\Order\RecalculateOrderPriceService;
use App\Service\OrderUpdateService;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Service\Shipping\ShippingPriceRangeService;
use App\Service\Shipping\ShippingPriceService;
use App\Service\User\UserVatService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/customer/calc")
 */
class ApiCustomerCalcController extends Controller
{

    /**
     * @var ShippingPriceService
     */
    protected $shippingPriceService;

    /**
     * @var ShippingPriceRangeService
     */
    protected $shippingPriceRangeService;


    public function __construct(ShippingPriceService $shippingPriceService, ShippingPriceRangeService $shippingPriceRangeService, UserVatService $userVatService)
    {
        $this->shippingPriceService = $shippingPriceService;
        $this->shippingPriceRangeService = $shippingPriceRangeService;
        $this->userVatService = $userVatService;
    }


    /**
     * @Route("/price", name="app.api_customer.calc_price")
     */
    public function calculateAction(Request $request)
    {
        /** @var RecalculateOrderPriceService $recalcaulateOrderService */
        $recalcaulateOrderService = $this->get('app.service.order.recalculate');
        $callback = $request->get('callback');

        $orderData = $request->request->get('orderCustomer');
        $vat = false;
        if (!empty($orderData) && !empty($orderData['vat'])) {
            $vat = (bool)$orderData['vat'];
        }

        $order = $this->buildOrder($request);
        $recalcaulateOrderService->recalculate($order, $vat);

        $pickupRange = $this->shippingPriceRangeService->calc($order, Shipping::TYPE_PICKUP, $vat);
        $deliverRange = $this->shippingPriceRangeService->calc($order, Shipping::TYPE_DELIVER, $vat);

        $response = new JsonResponse(
            [
                'status' => true,
                'data' => [
                    'minPrice' => $order->getMinPriceWithoutPromocode(),
                    'maxPrice' => $order->getMaxPriceWithoutPromocode(),
                    'minPricePromocode' => $order->getMinPriceWithPromocode(),
                    'maxPricePromocode' => $order->getMaxPriceWithPromocode(),

                    'totalFirstMilePriceMin' => ceil($pickupRange['min']),
                    'totalFirstMilePriceMax' => ceil($pickupRange['max']),
                    'totalLastMilePriceMin' => ceil($deliverRange['min']),
                    'totalLastMilePriceMax' => ceil($deliverRange['max']),

                    'overloadWeightFirstMileShipping' => false,
                    'overloadWeightLastMileShipping' => false,
                ]
            ]
            , 200
        );

        if (!empty($callback)) {
            $response->setCallback($callback);
        }

        return $response;
    }

    /**
     * @Route("/competitor_price", name="app.api_customer.competitor_price")
     */
    public function competitorPricesAction(Request $request)
    {
        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');
        $callback = $request->get('callback');

        /** @var CompetitorCalcService $competitorCalcService */
        $competitorCalcService = $this->get('app.competitor_calc_service');

        /** @var Direction $direction */
        if (!($direction = $request->query->get('direction'))
            || !($direction = $this->getDoctrine()->getRepository(Direction::class)->findOneById($direction))
        ) {
            return new JsonResponse(['error' => 'Не указано направление'], 400);
        }

        if (!($packages = $request->query->get('packages'))) {
            return new JsonResponse(['error' => 'Не указаны товарные позиции'], 400);
        }

        $order = new Order();
        $order->setDirection($direction);
        $order->setByCargo($request->query->getBoolean('byCargo', false));
        $order->setDeclaredPrice($request->query->getInt('price'));

        foreach ($packages as $packageData) {
            $package = new OrderPackage();

            $packing = $this->getDoctrine()->getRepository(Packing::class)->findOneById($packageData['packing']);

            $package->setPacking($packing);
            $package->setCount($packageData['count'] ?? 1);
            $package->setWidth($packageData['width'] ?? null);
            $package->setHeight($packageData['height'] ?? null);
            $package->setDepth($packageData['depth'] ?? null);
            $package->setWeight($packageData['weight']);
            $package->setCalculateWeight($packageData['calculateWeight']);

            $order->addOrderPackage($package);
        }

        $competitorPricesResult = $competitorCalcService->calc($order);

        $response = new JsonResponse(
            [
                'status' => true,
                'data' => (array)$competitorPricesResult
            ]
            , 200
        );

        if (!empty($callback)) {
            $response->setCallback($callback);
        }

        return $response;
    }

    /**
     * @Route("/create_order", name="app.api_customer.create_order")
     */
    public function createOrderAction(Request $request)
    {
        /** @var RecalculateOrderPriceService $recalcaulateOrderService */
        $recalcaulateOrderService = $this->get('app.service.order.recalculate');
        $form = $this->createForm(OrderType::class);

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $this->get('app.order.update');

        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');

        /** @var UtmService $utmService */
        $utmService = $this->get('app.service.utm_service');

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $orderData = $request->request->get('orderCustomer');
        $vat = false;
        if (!empty($orderData) && !empty($orderData['vat'])) {
            $vat = (bool)$orderData['vat'];
        }
        if ($this->getUser()) {
            $vat = $this->userVatService->get($this->getUser());
        }

        $form->submit($orderData);
        if (!$form->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var Order $orderCustomer */
        $orderCustomer = $form->getData();

        $packages = $request->request->get('orderCustomer')['packages'];
        foreach ($packages as $package) {
            if ((int)$package['packageType'] <= 0) {
                $package['packageType'] = '';
            }
            if ((int)$package['packageType'] <= 0) {
                $package['packageType'] = '';
            }

            $orderCustomerPackageForm = $this->createForm(OrderPackageType::class);
            $orderCustomerPackageForm->submit($package);
            if (!$orderCustomerPackageForm->isValid()) {
                return new JsonResponse($this->getErrorsFromForm($orderCustomerPackageForm), 400);
            }
            /** @var OrderPackage $orderCustomerPackage */
            $orderCustomerPackage = $orderCustomerPackageForm->getData();

            // Если заказ по грузу, место может быть только одно
            if ($orderCustomer->isByCargo()) {
                //$orderCustomerPackage->setCount(1);
            }

            if ($orderCustomerPackage->getPacking()) {
                $count = $orderCustomer->isByCargo() ? 1 : $orderCustomerPackage->getCount();
                $pPrice = $calculateService->calcPackingPrice($orderCustomerPackage->getPacking(), $orderCustomerPackage->getSizeObject()) * $count;

                $orderCustomerPackage->setPackingPrice($pPrice);
            }

            $orderCustomer->addOrderPackage($orderCustomerPackage);
        }

        $orderCustomer->setStatus(Order::STATUS_BOOKED);

        $utm = $utmService->getUtm();
        $orderCustomer->setUtm($utm);

        $recalcaulateOrderService->recalculate($orderCustomer, $vat);

        $entityManager->persist($orderCustomer);
        $entityManager->flush();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user instanceof User && $user->isCustomer()) {
            $orderCustomer->setUser($user);
            if ($user->isCustomer()) {
                $user->setCustomerNow();
            }
            $entityManager->persist($user);
            $entityManager->persist($orderCustomer);
            $entityManager->flush();
        } else {
            $session = $request->getSession();
            $session->set('CUSTOMER_ORDER_ID', $orderCustomer->getId());
            $session->set('CUSTOMER_ORDER_VAT', $vat);
            $entityManager->flush();
        }

        $entityManager->getConnection()->commit();

        $notCustomer = false;
        if ($this->getUser() && !$this->getUser()->isCustomer()) {
            $notCustomer = true;
        }

        return new JsonResponse(['status' => true, 'id' => $orderCustomer->getId(), 'notCustomer' => $notCustomer], 200);
    }

    /**
     * @Route("/config", name="app.api_customer.calc_config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $callback = $request->get('callback');

        /**
         * @var AuctionService $customerCalcService;
         */
        $service = $this->get('app.customer_calc');

        $settings = $service->getSettings();
        $params['directions'] = $service->getDirections();
        $params['packageTypes'] = $service->getPackageTypes();
        $params['packings'] = $service->getPackings();
        $params['stocks'] = $service->getStocks();

        $params['user_vat'] = null;
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (is_object($user) && $user->getId()) {
            if ($user->hasGroup('Sender')) {
                $params['user_vat'] = false;

                $customer = $user->getOrganizationCustomer();
                if ($customer && $customer->getNds()) {
                    $params['user_vat'] = $customer->getNds();
                }
            }
        }

        $response = new JsonResponse(['status' => true, 'data' => ['settings' => $settings, 'params' => $params]], 200);


        if (!empty($callback)) {
            $response->setCallback($callback);
        }

        return $response;
    }


    protected function buildOrder (Request $request)
    {

        /** @var Direction $direction */
        /*
        if (!($direction = $request->query->get('direction'))
            || !($direction = $this->getDoctrine()->getRepository(Direction::class)->findOneById($direction))
        ) {
            return new JsonResponse(['error' => 'Не указано направление'], 400);
        }
        */
        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');

        $stockRepository = $this->getDoctrine()->getRepository(Stock::class);
        $form = $this->createForm(OrderType::class);

        $orderData = $request->request->get('orderCustomer');

        if (!$form->submit($orderData, true)->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var Order $orderCustomer */
        $orderCustomer = $form->getData();

        $packages = $request->request->get('orderCustomer')['packages'];
        foreach ($packages as $package) {
            if (empty($package['packageType'])) {
                $package['packageType'] = 'Другое';
            }

            if ((int)$package['packageType'] <= 0) {
                $package['packageType'] = '';
            }
            if ((int)$package['packageType'] <= 0) {
                $package['packageType'] = '';
            }
            $package['packageTypeAnother'] = 'Другое';

            $orderCustomerPackageForm = $this->createForm(OrderPackageType::class);
            if (!$orderCustomerPackageForm->submit($package)->isValid()) {
                return new JsonResponse($this->getErrorsFromForm($orderCustomerPackageForm), 400);
            }
            /** @var OrderPackage $orderCustomerPackage */
            $orderCustomerPackage = $orderCustomerPackageForm->getData();

            if ($orderCustomerPackage->getPacking()) {
                $count = $orderCustomer->isByCargo() ? 1 : $orderCustomerPackage->getCount();
                $packagePrice = $calculateService->calcPackingPrice($orderCustomerPackage->getPacking(), $orderCustomerPackage->getSizeObject()) * $count;
                $orderCustomerPackage->setPackingPrice($packagePrice);
            }

            $orderCustomer->addOrderPackage($orderCustomerPackage);
        }

        $stockFrom = $stockRepository->findOneBy(['city' => $orderCustomer->getDirection()->getCityFrom(), 'published' => true]);
        $stockTo = $stockRepository->findOneBy(['city' => $orderCustomer->getDirection()->getCityTo(), 'published' => true]);

        $orderCustomer->setStockFrom($stockFrom);
        $orderCustomer->setStockTo($stockTo);


        return $orderCustomer;
    }


    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(\Symfony\Component\Form\FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = current($childErrors);
                }
            }
        }

        return $errors;
    }
}
