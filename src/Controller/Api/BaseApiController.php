<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class BaseApiController extends Controller
{
    public function checkAuth()
    {
        if (false === $this->authChecker->isGranted('ROLE_USER')) {
            return $this->error('not auth');
        }
    }

    public function answer($data = [], $cookies = [])
    {
        $cookies = (array)$cookies;

        $response = new JsonResponse([
            'status' => true,
            'data' => $data
        ], 200);

        foreach ($cookies as $cookie) {
            $response->headers->setCookie($cookie);
        }

        return $response;
    }

    public function errorAnswer($error = '', $cookies = [])
    {
        $cookies = (array)$cookies;

        $response = new JsonResponse([
            'status' => false,
            'error' => $error
        ], 200);

        foreach ($cookies as $cookie) {
            $response->headers->setCookie($cookie);
        }

        return $response;
    }

    public function error($error, $errorCode = 400)
    {
        return new JsonResponse(
            [
                'status' => false,
                'error' => $error
            ]
            , $errorCode
        );
    }
}
