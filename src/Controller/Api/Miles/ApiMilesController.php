<?php

namespace App\Controller\Api\Miles;

use App\Entity\BatchMile;
use App\Entity\City;
use App\Entity\Order;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Form\Shipping\ShippingType;
use App\Repository\CityRepository;
use App\Service\CustomerCalcService;
use App\Service\Integration\CallBaseService;

use App\Service\Integration\Mile\AvailableMilesRequest;
use App\Service\Integration\Mile\CalcMileCost;
use App\Service\Integration\Mile\ReserveRequest;
use App\Service\Shipping\ShippingGeoService;
use App\Service\Shipping\ShippingPriceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vsavritsky\SettingsBundle\Service\Settings;

use App\Service\Stock\StockListForPublic;

/**
 * @Route("/api/miles")
 **/
class ApiMilesController extends AbstractController
{
    /**
     * @var Settings $settings
     */
    protected $settings;


    /**
     * @var CallBaseService
     */
    protected $caller;

    /**
     * @var ShippingGeoService
     */
    protected $shippingGeoService;

    /**
     * @var StockListForPublic
     */
    protected $stockListForPublic;

    public function __construct(
        Settings $settings,
        CallBaseService $caller,
        ShippingGeoService $shippingGeoService,
        StockListForPublic $stockListForPublic,
        ShippingPriceService $shippingPriceService
    ) {
        $this->settings = $settings;
        $this->caller = $caller;
        $this->shippingGeoService = $shippingGeoService;
        $this->shippingPriceService = $shippingPriceService;
        $this->stockListForPublic = $stockListForPublic;
    }


    /**
     * @Route("/available", name="app.api_miles.available")
     */
    public function availableAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var StockRepository $stockRepository */
        $stockRepository = $em->getRepository(Stock::class);



        $cityId = $request->get('city_id');
        $sort = $request->get('sort');

        $date = null;
        if ($request->get('date')) {
            try {
                $date = new \DateTime($request->get('date'));
            } Catch (\Exception $e) {
                throw new NotFoundHttpException();
            }
        }

        if (empty($cityId)) {
            throw new NotFoundHttpException();
        }

        $city = $em->getRepository(City::class)->find($cityId);
        $baseCity = $em->getRepository(\App\Entity\Base\City::class)->findBy(['title' => $city->getTitle()]);

        if (!count($baseCity)) {
            throw new NotFoundHttpException();
        }

	// Citi и BaseCity не связаны, ищем через костыль
        $availableMilesRequest = new AvailableMilesRequest();
        $availableMilesRequest->setCityId($baseCity[0]->getExternalId());
        $availableMilesRequest->setDate($date);

        $miles = [];
        $dates = [];
        $result = $this->caller->exec($availableMilesRequest);
        if ($result['success'] && !empty($result['data'][0]['opts'])) {
            foreach ($result['data'][0]['opts'] as $mile) {
                $miles[] = $mile;
                $date = $mile['mile']['time'] ? new \DateTime($mile['mile']['time']) : null;
                if ($date) {
                    $dates[] = $date->format('Y-m-d');
                }
            }
        }

        $availableMilesRequest->setResponse($miles);

        if (!empty($sort) && !empty($sort['field'])) {
            $availableMilesRequest->setSortField($sort['field']);
            if (!empty($sort['direction'])) {
                $availableMilesRequest->setSortDirection($sort['direction']);
            }
        }


        return new JsonResponse([
            'status' => true,
            'data' => [
                'miles' => $availableMilesRequest->getResponse(),
                'dates' => $dates
            ]
        ], 200);
    }


    /**
     * @Route("/config", name="app.api_miles.config")
     */
    public function configAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var CityRepository $cityRepository */
        $cityRepository = $em->getRepository(City::class);
        /** @var StockRepository $stockRepository */
        $stockRepository = $em->getRepository(Stock::class);

        $params['cities'] = $cityRepository->findAll();
        $params['stocks'] = $this->stockListForPublic->getStocks();
        $params['auction_settings'] = $this->settings->group('miles_auction');

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }


    /**
     * @Route("/calc/{order}", name="app.api_miles")
     */
    public function calcAction(Request $request, Order $order = null)
    {
        if ($order->getUser() != $this->getUser()) {
            return new JsonResponse(['error' => 'Запрещено'], 400);
        }

        $prices = [];

        $shippingTypes = [Shipping::TYPE_PICKUP, Shipping::TYPE_DELIVER];
        foreach ($shippingTypes as $shippingType) {
            $prices[$shippingType] = 0;
            $shippingData = $request->request->get($shippingType);

            if ($shippingData) {
                $shipping = new Shipping();
                $shipping->setType($shippingType);
                $form = $this->createForm(ShippingType::class, $shipping);

                if ($form->submit($shippingData) && !$form->isValid()) {
                    return new JsonResponse(['error' => 'Неудалось сохранить данные'], 400);
                }

                if ($shipping->isActive() && !$shipping->isReject()) {
                    $this->shippingGeoService->fill($shipping);
                    $prices[$shippingType] = $this->shippingPriceService->calc($order, $shipping);
                }
            }

        }

        return new JsonResponse(['status' => true, 'data' => [
            'pickUpPrice' => $prices[Shipping::TYPE_PICKUP],
            'deliverPrice' => $prices[Shipping::TYPE_DELIVER]
        ]], 200);
    }



}
