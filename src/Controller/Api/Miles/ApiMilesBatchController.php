<?php

namespace App\Controller\Api\Miles;

use App\Entity\Batch;
use App\Entity\BatchMile;
use App\Entity\Order;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Shipping;
use App\Entity\Wms\WmsBatchMover;
use App\Service\Batch\BatchMoverGenerateDocs;
use App\Service\FileUploadService;
use App\Service\Integration\CallBaseService;

use App\Service\Integration\Mile\MilesBatchConfirmRequest;
use App\Service\Integration\Mile\MilesBatchRejectRequest;
use App\Service\Integration\Mile\ReserveRequest;
use App\Service\MailService;
use App\Service\Shipping\BatchNotificationService;
use App\Service\Shipping\ShippingGeoService;
use App\Service\Shipping\ShippingPriceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Vsavritsky\SettingsBundle\Service\Settings;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route("/api/miles/batch")
 **/
class ApiMilesBatchController extends AbstractController
{

    /**
     * @var CallBaseService
     */
    protected $caller;

    /**
     * @var ShippingGeoService
     */
    protected $shippingGeoService;

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * @var BatchMoverGenerateDocs
     */
    protected $batchGenerateDocs;

    /**
     * @var BatchNotificationService
     */
    protected $batchNotificationService;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FileUploadService
     */
    protected $fileUploadService;

    public function __construct(
        CallBaseService $caller,
        ShippingGeoService $shippingGeoService,
        ShippingPriceService $shippingPriceService,
        Settings $settings,
        MailService $mailService,
        EntityManagerInterface $em,
        BatchMoverGenerateDocs $batchGenerateDocs,
        FileUploadService $fileUploadService,
        BatchNotificationService $batchNotificationService
    ) {
        $this->caller = $caller;
        $this->shippingGeoService = $shippingGeoService;
        $this->shippingPriceService = $shippingPriceService;
        $this->settings = $settings;
        $this->mailService = $mailService;
        $this->batchGenerateDocs = $batchGenerateDocs;
        $this->batchNotificationService = $batchNotificationService;
        $this->em = $em;
        $this->fileUploadService = $fileUploadService;
    }


    /**
     * @Route("/reserve/{mileId}", name="app.api_miles.reserve")
     */
    public function reserveAction(Request $request, $mileId)
    {
        $em = $this->getDoctrine()->getManager();
        $shippingRepository = $em->getRepository(Shipping::class);
        $orderRepository = $em->getRepository(Order::class);

        $auctionSettings = $this->settings->group('miles_auction');
        $percentStart = $auctionSettings['percent_start'] ? $auctionSettings['percent_start'] : 60;
        $percentEnd = $auctionSettings['percent_end'] ? $auctionSettings['percent_end'] : 100;

        // @todo поправить
        $auctionCoef = (int)$request->request->get('coef');
        $auctionCoef = $auctionCoef > $percentEnd ? $percentEnd : $auctionCoef;
        $auctionCoef = $auctionCoef < $percentStart ? $percentStart : $auctionCoef;

        /** @var Shipping $shipping */
        $shipping = $shippingRepository->findOneBy(['externalId' => $mileId, 'reject' => false]);
        $order = $orderRepository->getByShipping($shipping->getExternalId());

        if (!$order) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось зарезервировать милю'], 200);
        }

        $user = $this->getUser();

        $reserveRequest = new ReserveRequest();
        $reserveRequest->setMiles([(int)$mileId])
            ->setUserId($user->getExternalId())
            ->setAuctionCoef($auctionCoef)
            ->setType($shipping->getType())
            ->setStockId($shipping->getStock()->getExternalId());


        $result = $this->caller->exec($reserveRequest);

        if ($result['success']) {
            $reserveId = $result['data']['id'];

            $price = !empty($result['data']['cost']) ? $result['data']['cost']['total'] : 0;

            $batchEntity = new BatchMile();

            $batchEntity->setReserveId($reserveId);
            $batchEntity->setCarrier($user);
            $batchEntity->setStatus(BatchMile::STATUS_RESERVED);
            $batchEntity->setPrice($price);
            $batchEntity->setOrder($order);
            $batchEntity->addShipping($shipping);

            $em->persist($batchEntity);
            $em->flush();
        } else {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось зарезервировать милю'], 200);
        }

        return new JsonResponse(['status' => true, 'data' => [
            'batchId' => $batchEntity->getId()
        ]], 200);
    }



    /**
     * @Route("/confirm/{batch}/{driver}", name="app.api_miles.confirm")
     */
    public function confirmAction(Request $request, BatchMile $batch, OrganizationDriver $driver)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (!$batch->getCarrier() || $batch->getCarrier()->getId() != $user->getId()) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось назначить водителя'], 200);
        }

        // @todo проверить права  доступа на водителя
        if (!$driver) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось назначить водителя'], 200);
        }

        if (!count($batch->getDrivers())) {
            $confirmRequest = new MilesBatchConfirmRequest();
            $confirmRequest->setReserveId($batch->getReserveId())
                ->setDriver($driver->getId());
            $result = $this->caller->exec($confirmRequest);

            if (!$result['success']) {
                return new JsonResponse(['status' => false, 'error' => 'Неудалось назначить водителя'], 200);
            }
        }

        $batch->getDrivers()->clear();
        $batch->addDriver($driver);
        $batch->setStatus(BatchMile::STATUS_WAITING);
        $em->flush();

        $this->batchNotificationService->notificationBatchReserved($batch);

        // При бронировании первой мили, генерируем необходимые доки
        $shipping = $batch->getShipping();
        if ($shipping && $shipping->getType() == Shipping::TYPE_PICKUP) {
            $this->batchGenerateDocs->generate($batch->getOrder(), $batch);
        }

        return new JsonResponse(['status' => true, 'data' => [

        ]], 200);
    }



    /**
     * @Route("/reject/{batch}", name="app.api_miles.reject")
     */
    public function rejectAction(Request $request, BatchMile $batch)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (!$batch->getCarrier() || $batch->getCarrier()->getId() != $user->getId()) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось отменить партию'], 200);
        }

        $rejectRequest = new MilesBatchRejectRequest();
        $rejectRequest->setBatchId($batch->getReserveId());

        $result = $this->caller->exec($rejectRequest);

        if (!$result['success']) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось отменить партию'], 200);
        }


        $batch->setStopSend(true);
        $batch->setRemove(true);
        $batch->setStatus(BatchMile::STATUS_SUSPENDED);

        $em->flush();

        $this->batchNotificationService->notificationBatchCarrierCancel($batch);

        $em->flush();

        return new JsonResponse(['status' => true, 'data' => [

        ]], 200);
    }




    /**
     * @Route("/order-upload-file/{batchMile}", name="app.api.order-upload-file")
     */
    public function uploadFileAction(Request $request, BatchMile $batchMile)
    {
        $groupCode = $request->request->get('groupCode');
        $availableGroups = ['act-rt', 'tn'];

        if (!$this->getUser() || $batchMile->getCarrier()->getId() !== $this->getUser()->getId()) {
            throw  new \Exception('Unable to upload file');
        }
        if (!in_array($groupCode, $availableGroups)) {
            throw  new \Exception('Unable to upload file');
        }

        if ($request->files->has('file')) {
            $tmpFile = $request->files->get('file');
            if (!in_array($tmpFile->getClientOriginalExtension(), ['pdf', 'doc', 'jpg', 'png', 'jpeg'])) {
                return new JsonResponse(['status' => false], 400);
            }
        }

        $wmsBatchMile = $this->em->getRepository(WmsBatchMover::class)->findOneBy(['externalId' => $batchMile->getReserveId()]);

        $file = $batchMile->getFileByGroup($groupCode);

        if ($file) {
            $file = $this->fileUploadService->updateFile($file, $request->files->get('file'));
        } else {
            $file = $this->fileUploadService->createFile('', $groupCode, $request->files->get('file'), sprintf('%s%s', $groupCode, $batchMile->getId()));
            $batchMile->addFile($file);
            if ($wmsBatchMile) {
                $wmsBatchMile->addFile($file);
            }
        }

        $this->em->persist($batchMile);
        $this->em->flush();

        return new JsonResponse(['status' => true], 200);
    }




}
