<?php

namespace App\Controller\Api;

use App\Entity\DispatchLog;
use App\Entity\Organization\OrganizationType ;
use App\Service\DispatchService;
use App\Service\MailService;
use App\Service\RegisterService;
use App\Application\Sonata\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/subscribe")
 */
class ApiSubscribeController extends Controller
{
    const SHOW_SUBSCRIBE = 'show_subscribe';
    const EMAIL_SUBSCRIBE = 'email_subscribe';

    /**
     * @Route("/check", name="api.subscribe.check")
     */
    public function checkAction(Request $request)
    {
        $show = true;
        
        $path = $request->request->get('path');
        
        if (in_array($path, [
            $this->generateUrl('fos_user_security_login'),
            $this->generateUrl('app.security.register'),
            $this->generateUrl('app.personal.index'),  
        ])) {
            $show = false;
        }

        $session = $request->getSession();
        if ($session->get(self::SHOW_SUBSCRIBE)) {
            $show = false;
        }

        $user = $this->getUser();
        if ($user instanceof User) {
            $show = false;
        }

        $ip = $request->getClientIp();
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findByIp($ip);

        if (!empty($users)) {
            $show = false;
        }
        
        $dispatchLogRepository = $this->getDoctrine()->getRepository(DispatchLog::class);
        $logs = $dispatchLogRepository->findByIp($ip);
        if (!empty($logs)) {
            $show = false;
        }

        $session->set(self::SHOW_SUBSCRIBE, true);
        return new JsonResponse([
            'status' => true,
            'data' => [
                'show' => false
            ]
        ], 200);
    }

    /**
     * @Route("/subscribe", name="api.subscribe.subscribe")
     */
    public function subscribeAction(Request $request)
    {
        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');

        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        /** @var DispatchService $dispatchService */
        $dispatchService = $this->get('app.dispatch');

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $dispatchLogRepository = $this->getDoctrine()->getRepository(DispatchLog::class);

        $url = $request->request->get('url');
        $email = $request->request->get('email');
        $subscribe = $request->request->getBoolean('subscribe');

        $user = $userRepository->findOneByEmail($email);

        $logs = [];
        if ($user) {
            $logs = $dispatchLogRepository->findBy(['user' => $user, 'event' => DispatchLog::EVENT_SUBSCRIBE]);
        } else {
            $logs = $dispatchLogRepository->findBy(['email' => $email, 'event' => DispatchLog::EVENT_SUBSCRIBE]);
        }

        if ($email && !empty($logs)) {
            return new JsonResponse([
                'status' => true,
                'error' => 'exist'
            ], 200);
        }

        if ($email && $dispatchLogRepository->findBy(['event' => DispatchLog::EVENT_SUBSCRIBE, 'email' => $email])) {
            return new JsonResponse([
                'status' => true,
                'error' => 'exist'
            ], 200);
        }

        if (!$subscribe) {
            return new JsonResponse([
                'status' => true,
                'error' => 'error'
            ], 200);
        }

        if ($user) {
            $dispatchService->dispatch($user, $subscribe, $url);
        } else {
            $dispatchService->dispatchEmail($email, $subscribe, $url);
        }

        $emailTemplate = 'subscribe';
        $mailService->registerEmail($emailTemplate, $email, ['email' => $email]);

        $session = $request->getSession();
        $session->set(self::EMAIL_SUBSCRIBE, $email);

        return new JsonResponse([
            'status' => true,
        ], 200);
    }
}
