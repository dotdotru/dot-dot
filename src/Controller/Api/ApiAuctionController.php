<?php

namespace App\Controller\Api;

use App\Entity\Base\StockWorkingHours;
use App\Entity\Base\WorkingCalendar;
use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\Lot;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Form\BatchType;
use App\Repository\PalletRepository;
use App\Serializer\DateTimeNormalizer;
use App\Serializer\PalletNormalizer;
use App\Serializer\StockNormalizer;
use App\Service\AuctionService;
use App\Service\Batch\SlaDateService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Service\User\UserVatService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use DateTime;

/**
 * @Route("/api/auction")
 */
class ApiAuctionController extends Controller
{

    /**
     * @var UserVatService
     */
    protected $userVatService;

    /**
     * @var SlaDateService
     */
    protected $slaDateService;


    public function __construct(UserVatService $userVatService, SlaDateService $slaDateService)
    {
        $this->userVatService = $userVatService;
        $this->slaDateService = $slaDateService;
    }


    /**
     * @param stockFromId int
     * @param stockToId int
     * @param weight float
     *
     * @Route("/list", name="app.api.auction_list")
     */
    public function auctionListAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var PalletRepository $palletRepository */
        $palletRepository = $entityManager->getRepository(Pallet::class);

        if (!($direction = $request->query->get('direction'))
            || !($direction = $this->getDoctrine()->getRepository(Direction::class)->findOneById($direction))
        ) {
            return new JsonResponse(['error' => 'Не указано направление'], 400);
        }

        if (!($weight = $request->query->get('weight'))
            || $weight <= 0
        ) {
            return new JsonResponse(['error' => 'Не указан вес'], 400);
        }

        if (!($sendDate = $request->query->get('sendDate'))
        ) {
            return new JsonResponse(['error' => 'Не указана дата'], 400);
        }

        if (!($price = $request->query->get('price'))
            || $price <= 0
        ) {
            return new JsonResponse(['error' => 'Не указана цена'], 400);
        }

        /** @var AuctionService $auctionService */
        $auctionService = $this->get('app.auction');
        $auctionService->setDirection($direction);

        $maxWeight = $request->query->get('maxWeight');
        $maxCount = $request->query->get('maxCount');

        // внутреннее ограничение для паллет(зафиксить себе на будущее)- максимальное количество для расчета 33 шт, вес одной паллеты от 100 до 800
        if ($maxWeight > 1500 || !$maxWeight) {
            $maxWeight = 1500;
        }

        if ($maxCount > 33) {
            $maxCount = 33;
        }

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');


        $vat = $request->query->has('vat') ? (bool)$request->query->get('vat') : false;
        if ($this->getUser()) {
            $vat = $this->userVatService->get($this->getUser());
        }

        $sendDate = \DateTime::createFromFormat('D M d Y H:i:s e+', $sendDate);


        $result = $integrationBaseService->getAuctionLots([
            'auction_price' => $price / 100,
            'route' => $direction->getExternalId(),
            'load_time' => $sendDate->format(IntegrationBaseService::DATETIME_FORMAT_ISO),
            'max_weight' => $weight,
            'max_quantity' => $maxCount,
            'vat' => $vat,
            'max_pallet_weight' => $maxWeight,
        ]);

        $result['data']['update'] = false;

        if (isset($result['data']) && isset($result['data']['weight'])) {
            $currentWeight = $auctionService->getWeightWarehouses($price, $vat);

            if ($currentWeight != $result['data']['weight']) {
                $auctionService->updateWeightWarehouses();
            }

            $result['data']['update'] = true;
        }

        if (isset($result['data']) && isset($result['data']['opts'])) {

            $stockRepository = $this->getDoctrine()->getRepository(Stock::class);
            foreach ($result['data']['opts'] as $key => &$item) {
                /** @var Stock $stock */
                $stock = $stockRepository->findOneByExternalId($item['warehouse_from']['id']);
                $item['warehouse_to']['address'] = $stock->getAddress();
                $item['warehouse_from']['mode'] = $stock->getMode();

                $stock = $stockRepository->findOneByExternalId($item['warehouse_to']['id']);
                $item['warehouse_to']['address'] = $stock->getAddress();
                $item['warehouse_to']['mode'] = $stock->getMode();
                $item['sla_date'] = $this->slaDateService->get($direction, $stock, $sendDate);
                $result['data']['opts'][$key]['costVat'] = $result['data']['opts'][$key]['cost']['vat'];
                $result['data']['opts'][$key]['cost'] = $result['data']['opts'][$key]['cost']['total'];
            }

            return new JsonResponse(
                [
                    'status' => true,
                    'data' => (array)$result['data']
                ]
                , 200
            );
        }

        return new JsonResponse(
            [
                'status' => false,
                'data' => []
            ]
            , 200
        );
    }

    /**
     * @Route("/reserve", name="app.api_auction.reserve")
     */
    public function reserveAction(Request $request)
    {
       // if (!$this->getUser() || !$this->getUser()->isCarrier()) {
         //   return new JsonResponse(['status' => false], 403);
        //}
        $user = $this->getUser() ? $this->getUser() : null;

        $entityManager = $this->getDoctrine()->getManager();

        /** @var AuctionService $auctionService */
        $auctionService = $this->get('app.auction');

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');
        /** @var OrderMapperService $orderMapperService */
        $orderMapperService = $this->get('app.order.mapper');

        $form = $this->createForm(BatchType::class);

        $orderData = $request->request->get('order');

        $form->submit($orderData);
        if (!$form->isValid()) {
            //return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var Batch $order */
        $order = $form->getData();

        $stockRepository = $entityManager->getRepository(Stock::class);

        $stockFrom = $stockRepository->findOneByExternalId($orderData['stockFrom']);
        $order->setStockFrom($stockFrom);

        $stockTo = $stockRepository->findOneByExternalId($orderData['stockTo']);
        $order->setStockTo($stockTo);

        $order->setPrice((int)$orderData['price']);
        $order->setPriceVat((int)$orderData['priceVat']);

        $pallets = $orderData['pallets'];
        $order->setWeights($pallets);

        $entityManager = $this->getDoctrine()->getManager();

        $vat = !empty($orderData['vat']) ? (bool)$orderData['vat'] : false;
        if ($this->getUser()) {
            $vat = $this->userVatService->get($this->getUser());
        }

        $data = $orderMapperService->mapEntityToReserveBase($order, $vat);
        $result = $integrationBaseService->reserve($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {
            $order->setExternalId($result['data']['id']);
            if (!empty($result['data']['cost'])) {
                $order->setPrice($result['data']['cost']['total']);
                $order->setPriceVat($result['data']['cost']['vat']);
            }


            $order->setStatus(Batch::STATUS_RESERVED);
            $entityManager->persist($order);
            $entityManager->flush();

            $user = $this->get('security.token_storage')->getToken()->getUser();
            if ($user instanceof User) {
                $order->setCarrier($user);
            } else {
                $session = $request->getSession();
                $session->set('ORDER_ID', $order->getId());
                $session->set('ORDER_VAT', $vat);
            }

            $auctionService->setDirection($order->getDirection());
            $auctionService->updateWeightWarehouses();

            $entityManager->persist($order);
            $entityManager->flush();

            return new JsonResponse([
                'status' => true,
                'authed' => $this->getUser() ? true : false,
                'id' => $order->getId()
            ], 200);
        } else {
            return new JsonResponse(['status' => false], 200);
        }
    }

    /**
     * @Route("/create_order", name="app.api_auction.create_order")
     */
    public function createOrderAction(Request $request)
    {
        $integrationBaseService = $this->get('app.integration_base');
        $form = $this->createForm(BatchType::class);

        $orderData = $request->request->get('order');

        $form->submit($orderData);
        if (!$form->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var Batch $order */
        $order = $form->getData();

        $pallets = $request->request->get('order')['pallets'];
        $order->setWeights($pallets);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($order);
        $entityManager->flush();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user instanceof User) {
            $order->setCarrier($user);
            if ($user->isCarrier()) {
                $user->setCarrierNow();
            }
            $entityManager->persist($user);
            $entityManager->persist($order);
            $entityManager->flush();
        } else {
            $session = $request->getSession();
            $session->set('ORDER_ID', $order->getId());
        }

        $entityManager->persist($order);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'id' => $order->getId()], 200);
    }

    /**
     * @Route("/config", name="app.api.auction_config")
     */
    public function auctionConfigAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);
        $direction = $directionRepository->find($request->query->get('direction'));
        /**
         * @var AuctionService $auctionService;
         */
        $auctionService = $this->get('app.auction');

        $auctionService->setDirection($direction);

        $settings = $auctionService->getSettings();


        $vat = false;
        if ($request->query->has('vat')) {
            $vat = (bool)$request->query->get('vat');
        } else {
            if ($this->getUser()) {
                $vat = $this->userVatService->get($this->getUser());
            }
        }

        if ($this->getUser()) {
            $params['predefinedVat'] = $this->userVatService->get($this->getUser());
        }

		$params['getSeconds'] = $auctionService->getSecondsFromStart($vat);
		$params['secondsFromStart'] = $auctionService->getSecondsFromStart($vat);
		$params['currentAuctionTime'] = $auctionService->getCurrentAuctionTime($vat);

        $params['nowInWarehouses'] = $auctionService->getWeightWarehouses(null, $vat);
        $params['currentPricePerKilo'] = $auctionService->getCurrentPricePerKilo($vat);
        $params['currentPriceStep'] = $auctionService->getPeriod($vat);
        $params['nextPricePerKilo'] = $auctionService->getNextPricePerKilo(null, $vat);
        $params['nextPriceTime'] = $auctionService->getPriceTime($vat);
        $params['directions'] = $auctionService->getDirections();
        $params['stocks'] = $auctionService->getStocks();

        return new JsonResponse(['status' => true, 'data' => ['settings' => $settings, 'params' => $params]], 200);
    }

    /**
     * @Route("/stock_working_hours/", name="app.api.auction_stock_working_hours")
     */
    public function stockWorkingHours(Request $request)
    {
        $stockWorkingHoursRepository = $this->getDoctrine()->getRepository(StockWorkingHours::class);
        $stockWorkingHours = $stockWorkingHoursRepository->findAll();

        $workingCalendarRepository = $this->getDoctrine()->getRepository(WorkingCalendar::class);
        $workingCalendar = $workingCalendarRepository->findAll();

        return new JsonResponse(['data' =>
            [
                'stockWorkingHours' => $stockWorkingHours,
                'workingCalendar' => $workingCalendar
            ]
        ]);
    }

    /**
     * @Route("/graphdata/{directionId}", name="app.api.auction_graphdata")
     */
    public function graphdataAction(Request $request, $directionId)
    {
        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);
        $direction = $directionRepository->find($directionId);

        $vat = false;
        if ($request->query->has('vat')) {
            $vat = (bool)$request->query->get('vat');
        } else {
            if ($this->getUser()) {
                $vat = $this->userVatService->get($this->getUser());
            }
        }

        /**
         * @var AuctionService $auctionService;
         */
        $auctionService = $this->get('app.auction');
        $auctionService->setDirection($direction);

        $cacheResult = [];

        for (
            $price = $auctionService->getStartPricePerKilo();
            $price < $auctionService->getEndPricePerKilo($vat) + $auctionService->getPriceUpInPeriod();
            $price = $price + $auctionService->getPriceUpInPeriod()
        ) {
            $weight = $auctionService->getWeightWarehouses($price, $vat);

            $cacheResult[] = ['quantity' => $weight, 'c' => $auctionService->getCacheIdWeightWarehouses($price, $vat), 'price' => round($price / 100, 2)];
        }

        return new JsonResponse(['data' => array_values($cacheResult)]);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(\Symfony\Component\Form\FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = current($childErrors);
                }
            }
        }

        return $errors;
    }
}
