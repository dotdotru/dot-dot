<?php

namespace App\Controller\Api\Geo;

use App\Service\CalcDistanceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/geo")
 */
class ApiGeoController extends Controller
{
    /**
     * @todo Пока это нигде не используется, понять как будет вычиляться расстояние для миль
     *
     * @Route("/distance", name="app.api.geo.distance")
     */
    public function auctionListAction(Request $request)
    {
        /** @var CalcDistanceService $calc */
        $calc = $this->get('app.service.calc_distance');

        $addressFrom = $request->get('from');
        $addressTo = $request->get('to');

        if (empty($addressFrom)) {
            return new JsonResponse([
                'status' => false,
                'msg' => 'From address should be specified'
            ],200);
        }
        if (empty($addressTo)) {
            return new JsonResponse([
                'status' => false,
                'msg' => 'To address should be specified'
            ],200);
        }

        //dump($calc->calcDistance($addressFrom, $addressTo));
        try {
            $distance = $calc->getDirection($addressFrom, $addressTo);
        } Catch (\Exception $e) {
            return new JsonResponse([
                'status' => false,
                'msg' => 'Unable to find address'
            ],200);
        }

        if ($distance['status'] == 'NOT_FOUND') {
            return new JsonResponse([
                'status' => false,
                'msg' => 'Unable to find address'
            ],200);
        }

        $answer = [];

        $data = $distance['routes'][0]['legs'][0];

        $answer['distance'] = $data['distance']['value'];
        $answer['start_location'] = $data['start_location'];
        $answer['end_location'] = $data['end_location'];


        return new JsonResponse([
            'status' => true,
            'data' => $answer
        ], 200);
    }




    /**
     * Возвращаем построенный маршрут
     *
     * @Route("/route", name="app.api_customer.route")
     */
    public function routeAction(Request $request)
    {
        $positionFrom = $request->get('position_from');
        $positionTo = $request->get('position_to');

        /** @var CalcDistanceService $calc */
        $calc = $this->get('app.service.calc_distance');

        $direction = $calc->getDirection(implode(',', $positionFrom), implode(',', $positionTo));

        return new JsonResponse([
            'status' => true,
            'data' => $direction
        ], 200);
    }




}
