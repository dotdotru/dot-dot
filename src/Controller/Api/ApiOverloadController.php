<?php

namespace App\Controller\Api;

use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Lot;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Pallet;
use App\Entity\Receiver;
use App\Entity\Stock;
use App\Form\OrderOverloadType;
use App\Form\BatchType;
use App\Form\ReceiverType;
use App\Repository\OrderRepository;
use App\Repository\BatchRepository;
use App\Repository\Organization\OrganizationDriverRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\PalletRepository;
use App\Serializer\DateTimeNormalizer;
use App\Serializer\OrderNormalizer;
use App\Serializer\PalletNormalizer;
use App\Serializer\StockNormalizer;
use App\Service\AuctionService;
use App\Service\CustomerCalcService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\OrderMapperService;
use App\Service\SmsService;
use App\Service\TrackService;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/overload")
 *
 *
 **/

class ApiOverloadController extends Controller
{
    /**
     * @Route("/config", name="app.overload.config")
     */
    public function configAction(Request $request)
    {
        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);

        $organizations = $organizationRepository->getInners();

        $params = [];

        $params['organizations'] = $organizations;
        $params['directions'] = $directionRepository->findAll();

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }

    /**
     * @Route("/pallets", name="app.overload.pallets")
     */
    public function ordersAction(Request $request)
    {
        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 10);
        $filter = $request->request->get('filter', []);
        $sort = $request->request->get('sort', []);

        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);
        $stockRepository = $this->getDoctrine()->getRepository(Stock::class);

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->container->get('app.integration_base');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();


        $pallets = new ArrayCollection();
        $palletsData = $integrationBaseService->getOverloadPallets();

        foreach ($palletsData['data'] as $palletData) {
            $direction = $directionRepository->findOneById($palletData['route']);
            $stockTo = $stockRepository->findOneByExternalId($palletData['warehouse_to']);
            $stockFrom = $stockRepository->findOneByExternalId($palletData['warehouse_from']);

            $pallet = new Pallet();
            $pallet->setWeight($palletData['weight']);
            $pallet->setDirection($direction);
            $pallet->setStockTo($stockTo);
            $pallet->setStockFrom($stockFrom);
            $pallet->setId($palletData['id']);
            $pallet->setExternalId($palletData['id']);
            $pallet->setMaxDeliveryTime(new \DateTime($palletData['max_delivery_time']));

            $pallets->add($pallet);
        }

        if (isset($sort['by'])) {
            $sortBy = $sort['by'];
            $sortDirection = $sort['direction'];
        } else {
            $sortBy = 'id';
            $sortDirection = 'asc';
        }

        $palletsTotal = $pallets->toArray();
        $palletResults = $pallets->toArray();

        foreach ($palletResults as $key => $pallet) {
            if (isset($filter['direction']) && !empty($filter['direction'])) {
                if ($pallet->getDirection()->getId() != $filter['direction']) {
                    unset($palletResults[$key]);
                }
            }
            if (isset($filter['stockFrom']) && !empty($filter['stockFrom'])) {
                if ($pallet->getStockFrom()->getId() != $filter['stockFrom']) {
                    unset($palletResults[$key]);
                }
            }
            if (isset($filter['stockTo']) && !empty($filter['stockTo'])) {
                if ($pallet->getStockTo()->getId() != $filter['stockTo']) {
                    unset($palletResults[$key]);
                }
            }
        }


        usort($palletResults, function($a, $b) use ($sortBy, $sortDirection) {

            if ($sortDirection == 'asc') {
                $sortKoeff = 1;
            } else {
                $sortKoeff = -1;
            }

            if ($sortBy == 'id') {
                if ($a->getExternalId() == $b->getExternalId()) {
                    return 0;
                }
                return ($a->getExternalId() < $b->getExternalId()) ? -1 * $sortKoeff : 1 * $sortKoeff;
            }

            if ($sortBy == 'date') {
                if ($a->getMaxDeliveryTime() == $b->getMaxDeliveryTime()) {
                    return 0;
                }
                return ($a->getMaxDeliveryTime() < $b->getMaxDeliveryTime()) ? -1 * $sortKoeff : 1 * $sortKoeff;
            }

            if ($sortBy == 'weight') {
                if ($a->getWeight() == $b->getWeight()) {
                    return 0;
                }
                return ($a->getWeight() < $b->getWeight()) ? -1 * $sortKoeff : 1 * $sortKoeff;
            }

            if ($sortBy == 'direction') {
                if ($a->getDirection()->getId() == $b->getDirection()->getId()) {
                    return 0;
                }
                return ($a->getDirection()->getId() < $b->getDirection()->getId()) ? -1 * $sortKoeff : 1 * $sortKoeff;;
            }

            if ($sortBy == 'stockTo') {
                if ($a->getStockTo()->getId() == $b->getStockTo()->getId()) {
                    return 0;
                }
                return ($a->getStockTo()->getId() < $b->getStockTo()->getId()) ? -1 * $sortKoeff : 1 * $sortKoeff;;
            }

            if ($sortBy == 'stockFrom') {
                if ($a->getStockFrom()->getId() == $b->getStockFrom()->getId()) {
                    return 0;
                }
                return ($a->getStockFrom()->getId() < $b->getStockFrom()->getId()) ? -1 * $sortKoeff : 1 * $sortKoeff;;
            }

            return 0;
        });

        $result = [];

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil(count($palletResults) / $limit),
        ];

        $result['palletsTotal'] = $palletsTotal;
        //$result['palletsAll'] = $palletResults;
        $result['pallets'] = array_slice($palletResults, ($page - 1) * $limit, $limit);

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }


    /**
     * @Route("/reserve", name="app.overload.reserve")
     */
    public function reserveAction(Request $request)
    {
        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');
        /** @var OrderMapperService $orderMapperService */
        $orderMapperService = $this->get('app.order.mapper');

        $entityManager = $this->getDoctrine()->getManager();

        $driverRepository = $this->getDoctrine()->getRepository(OrganizationDriver::class);

        $form = $this->createForm(OrderOverloadType::class);

        $orderData = $request->request->get('overload');

        $form->submit($orderData);
        if (!$form->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var Batch $order */
        $order = $form->getData();

        $pallets = $orderData['pallets'];
        $order->setPallets($pallets);

        $driverIds = $orderData['drivers'];
        if ($driverIds) {
            foreach ($driverIds as $driverId) {
                $driver = $driverRepository->find($driverId);
                if ($driver) {
                    $order->addDriver($driver);
                }
            }
        }

        $order->setOverload(true);

        $entityManager->persist($order);
        $entityManager->flush();

        $data = $orderMapperService->mapOverloadToReserveBase($order);
        $result = $integrationBaseService->reserveOutdated($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {
            $order->setExternalId($result['data']);

            $order->setStatus(Batch::STATUS_NEW);
            $entityManager->persist($order);
            $entityManager->flush();

            return new JsonResponse(['status' => true, 'id' => $order->getId()], 200);
        } else {
            return new JsonResponse(['status' => false], 200);
        }
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(\Symfony\Component\Form\FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = current($childErrors);
                }
            }
        }

        return $errors;
    }
}
