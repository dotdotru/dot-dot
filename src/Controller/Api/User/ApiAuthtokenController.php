<?php

namespace App\Controller\Api\User;

use App\Controller\Api\BaseApiController;
use App\Entity\Authtoken;
use App\Entity\Order;
use App\Service\User\AuthtokenGenerateService;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/api/user/authtoken")
 **/
class ApiAuthtokenController extends BaseApiController
{
    /**
     * @Route("/used", name="app.api_personal.authtoken.used")
     */
    public function usedAction(Request $request)
    {
        $authtokenRepository = $this->getDoctrine()->getRepository(Authtoken::class);

        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('limit', 10);

        $result = $authtokenRepository->findByParams([
            'user' => $user,
            'page' => $page,
            'limit' => $limit,
            'used' => true
        ]);

        $tokens = [];
        foreach ($result as $row) {
            $tokens[] = $row;
        }

        return new JsonResponse(['status' => true, 'data' => [
            'pages' => ceil($result->count() / $limit),
            'tokens' => $tokens,
            'count' => $result->count()
        ]], 200);
    }


    /**
     * @Route("/download/{type}", name="app.api_personal.authtoken.download")
     */
    public function downloadAction(Request $request, $type)
    {
        $user = $this->getUser();
        /** @var AuthtokenGenerateService $authtokenGenerateService */
        $authtokenGenerateService = $this->get('app.service.user.authtoken_generate');

        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        $tokens = $authtokenGenerateService->generate($user, 100, 10);

        if ($type == 'txt') {
            $response = $this->render('api/user/authtoken/download/txt.html.twig', ['tokens' => $tokens]);
            $response->headers->set('Content-Type', 'text/plain; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment; filename=DOT-DOT-CODES.txt');

            return $response;
        }

        if ($type == 'csv') {
            $response = $this->render('api/user/authtoken/download/csv.html.twig', ['tokens' => $tokens]);
            $response->headers->set('Content-Type', 'text/plain; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment; filename=DOT-DOT-CODES.csv');

            return $response;
        }

        if ($type == 'pdf') {
            $tmp = $this->render('api/user/authtoken/download/pdf.html.twig', ['tokens' => $tokens])->getContent();

            return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($tmp), 'DOT-DOT-CODES.pdf');
        }


        throw $this->createNotFoundException();
    }

}
