<?php

namespace App\Controller\Api;

use App\Entity\Person;
use App\Form\PersonType;
use App\Service\OrderUpdateService;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Service\Wms\SecurityService;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/api/order/customer")
 **/
class ApiPersonController extends BaseApiController
{


    /**
     * @Route("/check-update-person", name="app.api_order_customer.check-uniq-person")
     */
    public function checkUniqPersonAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $user = $this->getUser();

        $personData = $request->request->get('person');

        $persons = $entityManager->getRepository(Person::class)->findByUser($user);
        foreach($persons as $key => $person) {
            if (!$person->getExternalId()) {
                unset($persons[$key]);
            }
        }

        $personForm = $this->createForm(PersonType::class);
        $personForm->submit($personData);

        if (!$personForm->isValid()) {
            return $this->errorAnswer('not valid');
        }

        /** @var Person $newPerson */
        $newPerson = $personForm->getData();
        $newPerson->setUser($user);

        $foundEqualUnique = false;
        $foundEqualFull = false;
        /** @var Person $person */
        foreach ($persons as $person) {

            if ($person->isUridical() == $newPerson->isUridical()
                && $person->getInn() == $newPerson->getInn()
                && ($person->getSeries() == $newPerson->getSeries() && $person->getNumber() == $newPerson->getNumber())
            ) {
                $foundEqualUnique = true;
            }

            if ($person->isUridical() == $newPerson->isUridical()
                && $person->getInn() == $newPerson->getInn()
                && ($person->getSeries() == $newPerson->getSeries() && $person->getNumber() == $newPerson->getNumber())
                && $person->getCompanyName() == $newPerson->getCompanyName()
                && $person->getContactName() == $newPerson->getContactName()
                && $person->getType() == $newPerson->getType()
                && $person->getContactEmail() == $newPerson->getContactEmail()
            ) {
                $foundEqualFull = true;
            }
        }

        if ($foundEqualUnique && !$foundEqualFull) {
            return $this->answer(['update' => true]);
        }

        return $this->answer(['update' => false]);
    }

    /**
     * @Route("/save-person", name="app.api_order_customer.save-person")
     */
    public function savePersonAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $this->get('app.order.update');

        $user = $this->getUser();


        $userData = $request->request->get('user', []);
        $personData = $request->request->get('person');


        $persons = $entityManager->getRepository(Person::class)->findByUser($user);
        foreach($persons as $key => $person) {
            if (!$person->getExternalId()) {
                unset($persons[$key]);
            }
        }

        $personForm = $this->createForm(PersonType::class);
        $personForm->submit($personData);

        if (!$personForm->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($personForm), 400);
        }

        /** @var Person $newPerson */
        $newPerson = $personForm->getData();

        if (isset($userData['id'])) {
            $newUser = $entityManager->getRepository(User::class)->find($userData['id']);
            $newPerson->setUser($newUser);
        } else {
            $newPerson->setUser($user);
        }

        /** @var Person $person */
        foreach ($persons as $person) {
            if ($person->isUridical() == $newPerson->isUridical()
                && $person->getInn() == $newPerson->getInn()
                && ($person->getSeries() == $newPerson->getSeries() && $person->getNumber() == $newPerson->getNumber())
            ) {
                $personForm = $this->createForm(PersonType::class, $person);
                $personForm->submit($personData);

                /** @var Person $newPerson */
                $newPerson = $personForm->getData();

                break;
            }
        }

        $entityManager->persist($newPerson);
        $entityManager->flush();

        if ($orderUpdateService->updatePerson($newPerson) === false) {
            $entityManager->getConnection()->rollback();
            return $this->errorAnswer('Ошибка синхронизации');
        }

        $entityManager->getConnection()->commit();

        return $this->answer(['id' => $newPerson->getId(), 'ext_id' => $newPerson->getExternalId()]);
    }


    /**
     * Меняем логику сохранения контактных данных
     *
     * Если в пришедшем контакте есть id - перезаписываем
     * Если есть флаг hidden - сохраняем, но отмечаем не показывать в списке
     *
     * @Route("/save-person-overwrite", name="app.api_order_customer.save-person-overwrite")
     */
    public function savePersonOverwriteAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $personRepository = $entityManager->getRepository(Person::class);

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $this->get('app.order.update');
        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        $user = $this->getUser();

        $userData = $request->request->get('user', []);
        $personData = $request->request->get('person');

        // Костыль для вмс, чтобы можно было сохранять контакты другому юзеру
        // @todo возможно для вмс сделать свой экшн
        $userForWmsUser = null;
        if ($securityService->isAuth() && isset($userData['id'])) {
            $userForWmsUser = $entityManager->getRepository(User::class)->find($userData['id']);
        }

        /** @var Person $person */
        $person = $personRepository->find($personData['id']);
        if ($person) {
            // Нельзя править чужие контакты
            if (($userForWmsUser && $person->getUser()->getId() !== $userForWmsUser->getId())) {
                throw new AccessDeniedException(403);
            } else if (!$userForWmsUser && (!$user || ($user->getId() !== $person->getUser()->getId()))) {
                throw new AccessDeniedException(403);
            }
        } else {
            $person = new Person();
        }

        $personForm = $this->createForm(PersonType::class, $person);
        $personForm->submit($personData);
        if (!$personForm->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($personForm), 400);
        }

        /** @var Person $newPerson */
        $person = $personForm->getData();

        if ($userForWmsUser) {
            $person->setUser($userForWmsUser);
        } else {
            $person->setUser($user);
        }

        // Если пользователь выбрал не перезаписывать существуюшие данные, сохраняем их, но не показываем в списке
        if (!empty($personData['hidden'])) {
            $person->setRemove(true);
        }

        $entityManager->persist($person);
        $entityManager->flush();

        if ($orderUpdateService->updatePerson($person) === false) {
            $entityManager->getConnection()->rollback();
            return $this->errorAnswer('Ошибка синхронизации');
        }

        $entityManager->getConnection()->commit();

        return $this->answer(['id' => $person->getId(), 'ext_id' => $person->getExternalId()]);
    }


    /**
     * @Route("/remove-person/{personId}", name="app.api_order_customer.remove-person")
     */
    public function removePersonAction(Request $request, $personId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $person = $entityManager->getRepository(Person::class)->findOneById($personId);

        if ($person) {
            $person->setRemove(true);

            $entityManager->persist($person);
            $entityManager->flush();
        }

        return $this->answer();
    }

}
