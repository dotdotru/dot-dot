<?php

namespace App\Controller\Api;

use App\Entity\AbstractBatch;
use App\Entity\Batch;
use App\Entity\BatchMile;
use App\Entity\FileGroup;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Repository\BatchRepository;
use App\Repository\Organization\OrganizationDriverRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Service\Ad\UtmService;
use App\Service\AuctionService;
use App\Service\FileUploadService;
use App\Service\Batch\SlaDateService;
use App\Service\CustomerCalcService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\OrderMapperService;
use App\Application\Sonata\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/order/carrier")
 */
class ApiOrderCarrierController extends BaseApiController
{

    /**
     * @var SlaDateService
     */
    protected $slaDateService;

    public function __construct(SlaDateService $slaDateService)
    {
        $this->slaDateService = $slaDateService;
    }

    /**
     * @Route("/config", name="app.api_order_carrier.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);
        $fileGroups = $fileGroupRepository->getDefaultCarrier();

        /** @var FileGroup $fileGroup */
        foreach ($fileGroups as $fileGroup) {
            $params['fileGroups'][] = $fileGroup->jsonSerialize();
        }

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }

    /**
     * @Route("/orders", name="app.api_order_carrier.orders")
     */
    public function ordersAction(Request $request)
    {
        $orderId = $request->query->getInt('orderId', null);
        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 10);
        $filter = $request->request->get('filter', '');
        $sort = $request->request->get('sort', []);

        /** @var User $user */
        $authChecker = $this->get('security.authorization_checker');
        $isRoleAdmin = $authChecker->isGranted('ROLE_ADMIN');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(AbstractBatch::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        $filterStatus = [];
        if ($filter) {
            if ($filter == 'reserved') {
                $filterStatus = ['status' => ['reserved']];
            } else {
                $filterStatus = ['status' => $filter];
            }
        }

        $sorts = [];
        if (isset($sort['by'])) {
            $sorts = [$sort['by'] => $sort['direction']];
        } else {
            $sorts = ['id' => 'desc'];
        }

        if ($isRoleAdmin) {
            $orders = $orderRepository->getByUser($user, $page, $limit, $sorts, $filterStatus);
        } else {
            $orders = $orderRepository->getByUser($user, $page, $limit, $sorts, $filterStatus);
        }

        $result = ['orders' => []];
        foreach ($orders as $order) {
            $orderJsonData = $order->jsonSerialize();
            $organization = $organizationRepository->getCarrierByUser($order->getCarrier());

            if ($organization) {
                $orderJsonData['organization'] = $organization;
                $orderJsonData['chooseDrivers'] = $organization->getVerifiedDrivers();
            }

            if ($order instanceof Batch && $order->getDirection() && $order->getStockTo() && $order->getSendDate()) {
                $orderJsonData['slaDate'] = $this->slaDateService->get($order->getDirection(), $order->getStockTo(), $order->getSendDate());
            }

            $result['orders'][] = $orderJsonData;
        }

        $defaultFiles = [];

        $fileGroups = $fileGroupRepository->getDefaultCarrier();
        foreach ($fileGroups as $fileGroup) {
            if ($fileGroup->getFiles()->count() > 0) {
                foreach ($fileGroup->getFiles() as $file) {
                    $file->setPath('/media/download/'.$file->getMedia()->getId());
                    $defaultFiles[] = $file;
                }
            }
        }

        /** @var FileGroup $fileGroup */
        foreach ($result['orders'] as &$item) {
			foreach ($defaultFiles as $file) {
                $item['files'][] = $file;
            }
            foreach ($item['files'] as &$file) {
                $file->setPath('/media/download/'.$file->getMedia()->getId());
            }
            if (!$item['files']) {
                $item['files'] = [];
            }
        }


        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($orders->count() / $limit),
        ];

        if ($request->getSession()->has('ORDER_MESSAGE')) {
            $result['user_message'] = $request->getSession()->get('ORDER_MESSAGE');
            $request->getSession()->remove('ORDER_MESSAGE');
        }

        return new JsonResponse([
            'status' => true,
            'data' => $result
        ], 200);
    }

    /**
     * @Route("/batch/{id}", name="app.api_order_carrier.batch")
     */
    public function batchAction(Request $request, $id)
    {
        /** @var User $user */
        $authChecker = $this->get('security.authorization_checker');
        $isRoleAdmin = $authChecker->isGranted('ROLE_ADMIN');
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(AbstractBatch::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        if ($isRoleAdmin) {
            $order = $orderRepository->findOneBy(['id' => $id]);
        } else {
            $order = $orderRepository->findOneBy(['carrier' => $user, 'id' => $id]);
        }

        $orderJsonData = $order->jsonSerialize();
        $organization = $organizationRepository->getCarrierByUser($order->getCarrier());

        if ($organization) {
            $orderJsonData['organization'] = $organization;
            $orderJsonData['chooseDrivers'] = $organization->getVerifiedDrivers();
        }

        $defaultFiles = [];

        $fileGroups = $fileGroupRepository->getDefaultCarrier();
        foreach ($fileGroups as $fileGroup) {
            if ($fileGroup->getFiles()->count() > 0) {

                foreach ($fileGroup->getFiles() as $file) {
                    $file->setPath('/media/download/'.$file->getMedia()->getId());
                    $defaultFiles[] = $file;
                }
            }
        }

        foreach ($defaultFiles as $file) {
            $orderJsonData['files'][] = $file;
        }
        foreach ($orderJsonData['files'] as &$file) {
            $file->setPath('/media/download/'.$file->getMedia()->getId());
        }
        if (!$orderJsonData['files']) {
            $orderJsonData['files'] = [];
        }

        // На случай, если у пользователя неправильно идут часы, отправляем сколько секунд осталось до конца отсчета
        if ($orderJsonData['createdAt']) {
            /** @var \DateTime $finishTime */
            $finishTime = clone $orderJsonData['createdAt'];
            $finishTime->modify('+5 minutes');
            $nowDate = new \DateTime('now', $finishTime->getTimezone());

            $diffTime = $finishTime->format('U') - $nowDate->format('U');
            $orderJsonData['finish_time_diff'] = $diffTime > 0 ? $diffTime : 100;
        }


        return $this->answer(['batch' => $orderJsonData]);
    }

    /**
     * @Route("/remove", name="app.api_order_carrier.remove")
     */
    public function removeAction(Request $request)
    {
        $ids = $request->request->get('ids', []);
        
         /** @var AuctionService $auctionService */
        $auctionService = $this->get('app.auction');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        $orderRepository = $entityManager->getRepository(Batch::class);

        if ($user->isSuperAdmin()) {
            $orders = $orderRepository->findBy(['id' => $ids]);
        } else {
            $orders = $orderRepository->findBy(['id' => $ids, 'carrier' => $user]);
        }

        $result = [];
        /** @var Batch $order */
        foreach ($orders as $order) {
            if ($order->isRemoveExactStatus()) {
                $order->setRemove(true);
                $entityManager->persist($order);
            }
        }
        $entityManager->flush();
        
        foreach($orders as $order) {
			$auctionService->setDirection($order->getDirection());
			$auctionService->updateWeightWarehouses();
		}

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/acceptance/{orderId}", name="app.api_order_carrier.acceptance")
     */
    public function acceptanceAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $integrationBaseService = $this->get('app.integration_base');

        /** @var OrderMapperService $orderMapperService */
        $orderMapperService = $this->get('app.order.mapper');

        /** @var UtmService $utmService */
        $utmService = $this->get('app.service.utm_service');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $orderRepository = $entityManager->getRepository(Batch::class);

        if ($user->isSuperAdmin()) {
            /** @var Batch $order */
            $order = $orderRepository->findOneBy(['id' => $orderId]);
        } else {
            /** @var Batch $order */
            $order = $orderRepository->findOneBy(['id' => $orderId, 'carrier' => $user]);
        }

        if (!$order) {
            return new JsonResponse(['status' => false], 200);
        }

        $order->setSendDate(new \DateTime());
        $data = $orderMapperService->mapOrderToBaseConfirm($order);
        $result = $integrationBaseService->confirmOverload($data);

        if ($result[IntegrationBaseService::RESULT_SUCCESS]) {
            $order->setAcceptanceAt(new \DateTime());
            $order->setStatus(Batch::STATUS_READY);
        }

        $utm = $utmService->getUtm();
        $order->setUtm($utm);

        $entityManager->persist($order);
        $entityManager->flush();

        return new JsonResponse(['status' => $order->getAcceptanceAt() ? true : false], 200);
    }

    /**
     * @Route("/{id}/send_drivers", name="app.api_order.send_drivers")
     */
    public function sendDriversAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Batch::class);

        /** @var Batch $order */
        $order = $orderRepository->find($id);

        /** @var OrganizationDriverRepository $driverRepository */
        $driverRepository = $entityManager->getRepository(OrganizationDriver::class);

        if (!$order) {
            return new JsonResponse(['status' => false], 400);
        }

        $driverIds = (array)$request->request->get('drivers');

        $drivers = [];
        foreach ($driverIds as $driverId) {
            $driver = $driverRepository->find($driverId);
            if ($driver) {
                $drivers[] = $driver;
            }
        }

        $order->setDrivers($drivers);

        if (!$order->getOverload()) {
            $order->setStatus(Batch::STATUS_READY);
        }
        $order->setUpdateBatchDate(null);

        $entityManager->persist($order);
        $entityManager->flush();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/{orderId}/stop_send", name="app.api_order.stop_send")
     */
    public function stopSendAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');

        /** @var Batch $order */
        $order = $entityManager->getRepository(Batch::class)->findOneBy(['id' => $orderId]);

        /** @var Batch $order */
        if (!$order) {
            return new JsonResponse(['error' => 'Не найден заказ'], 400);
        }

        if (!$this->getUser()->isSuperAdmin()) {
            /** @var Batch $order */
            if ($order->getCarrier() != $this->getUser()) {
                return new JsonResponse(['error' => 'Запрещено'], 400);
            }
        }

        $order->setStopSend(true);
        $order->setRemove(true);
        $order->setStatus(Batch::STATUS_SUSPENDED);
        $entityManager->persist($order);
        $entityManager->flush();

        $mailService->registerEmail('stop_send', $this->getParameter('admin_email'), ['entity' => $order]);

        return new JsonResponse(['status' => true], 200);
    }







}
