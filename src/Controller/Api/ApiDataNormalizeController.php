<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Service\DataNormalizer\Normalizer;

/**
 * @Route("/api/data-normalize")
 */
class ApiDataNormalizeController extends Controller
{
    /**
     * @Route("/suggest/address", name="api.data-normalize.suggest.address")
     */
    public function suggestAddressAction(Request $request)
    {
        $address = (string)$request->get('address');
        $dataNormalizer = $this->container->get(Normalizer::class);

        $addressInfo = $dataNormalizer->suggestAddress($address);
        return new JsonResponse(['status' => true, 'data' => $addressInfo]);
    }

    /**
     * @Route("/suggest/inn", name="api.data-normalize.suggest.inn")
     */
    public function suggestInnAction(Request $request)
    {
        $number = (string)$request->get('number');
        $dataNormalizer = $this->container->get(Normalizer::class);

        $companyInfo = $dataNormalizer->suggestInn($number);
        return new JsonResponse(['status' => true, 'data' => $companyInfo]);
    }

    /**
     * @Route("/suggest/bik", name="api.data-normalize.suggest.bik")
     */
    public function suggestBikAction(Request $request)
    {
        $number = (string)$request->get('number');
        $dataNormalizer = $this->container->get(Normalizer::class);

        $bankInfo = $dataNormalizer->suggestBik($number);
        return new JsonResponse(['status' => true, 'data' => $bankInfo]);
    }

    /**
     * @Route("/clean/address", name="api.data-normalize.clean.address")
     */
    public function cleanAddressAction(Request $request)
    {
        $address = (string)$request->get('address');
        $dataNormalizer = $this->container->get(Normalizer::class);
        $response = $dataNormalizer->checkAddress($address);
        return new JsonResponse(['status' => true, 'data' => $response]);
    }

    /**
     * @Route("/exists/address", name="api.data-normalize.exists.address")
     */
    public function existsAddressAction(Request $request)
    {
        $address = (string)$request->get('address');
        $dataNormalizer = $this->container->get(Normalizer::class);

        return new JsonResponse([
            'status' => true,
            'exists' => $dataNormalizer->existsAddress($address)
        ]);
    }

}
