<?php

namespace App\Controller\Api;

use App\Entity\AbandonedRegister;
use App\Entity\Direction;
use App\Entity\Lot;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Organization\OrganizationDriverDocument;
use App\Entity\Organization\OrganizationType ;
use App\Form\OrganizationType as OrganizationTypeForm;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Form\OrganizationDriverDocumentType;
use App\Form\OrganizationDriverType;
use App\Form\UserProfileType;
use App\Form\UserType;
use App\Repository\PalletRepository;
use App\Serializer\DateTimeNormalizer;
use App\Serializer\PalletNormalizer;
use App\Serializer\StockNormalizer;
use App\Service\Ad\UtmService;
use App\Service\AuctionService;
use App\Service\ChangePassService;
use App\Service\DispatchService;
use App\Service\MailService;
use App\Service\RegisterService;
use App\Service\SmsService;
use App\Service\VerificationService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/security")
 */
class ApiSecurityController extends BaseApiController
{
    /**
     * @Route("/config", name="app.apisecurity.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $organizationTypeRepository = $entityManager->getRepository(OrganizationType::class);
        $organizationTypes = $organizationTypeRepository->findBy([], ['position' => 'ASC']);

        foreach ($organizationTypes as $organizationType) {
            $params['organizationTypes'][] = $organizationType->jsonSerialize();
        }

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }

    /**
     * @param phone string
     *
     * @Route("/checkusername", name="app.apisecurity.checkusername")
     */
    public function checkusernameAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        $result['username'] = false;
        if ($request->query->get('username') && $registerService->checkUserExistUsername($request->query->get('username'))) {
            $result['username'] = true;
        }

        $result['email'] = false;
        if ($request->query->get('email') && $registerService->checkUserExistEmail($request->query->get('email'))) {
            $result['email'] = true;
        }

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @param phone string
     *
     * @Route("/reset", name="app.apisecurity.reset")
     */
    public function resetAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $restorePassService = $this->get('app.restore_pass');

        if (!($phone = $request->query->get('phone')) || !$restorePassService->isValidPhone($phone)) {
            return new JsonResponse(['error' => 'Не указан телефон'], 400);
        }

        $phone = $restorePassService->clearPhone($phone);

        return new JsonResponse(['status' => $restorePassService->changePassword($phone)], 200);
    }

    /**
     * @param phone string
     *
     * @Route("/sendsms", name="app.apisecurity.sendsms")
     */
    public function sendsmsAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        if (!($phone = $request->query->get('phone')) || !$registerService->isValidPhone($phone)) {
            return new JsonResponse(['error' => 'Не указан телефон'], 400);
        }

        return new JsonResponse(['status' => $registerService->sendSms($phone)], 200);
    }


    /**
     * Костыль для создания офлайн заказа в вмс
     * @param phone string
     *
     * @Route("/sendsms/wms", name="app.apisecurity.sendsms.wms")
     */
    public function sendsmsWmsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        if (!($phone = $request->query->get('phone')) || !$registerService->isValidPhone($phone)) {
            return new JsonResponse(['error' => 'Не указан телефон'], 400);
        }

        $user = $entityManager->getRepository(User::class)->findOneBy(['phone' => $registerService->clearPhone($phone)]);
        if (!$user) {
            return new JsonResponse(['error' => 'Юзер не найден'], 400);
        }

        return new JsonResponse(['status' => $registerService->sendSms($phone)], 200);
    }


    /**
     * @param code string
     *
     * @Route("/checksms", name="app.apisecurity.checksms")
     */
    public function checksmsAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        $code = $request->query->get('code', '');

        return new JsonResponse(['status' => $registerService->checkSmsCode($code)], 200);
    }

    /**
     * @Route("/beforeRegister", name="app.apisecurity.beforeRegister")
     */
    public function beforeRegisterAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        $entityManager = $this->getDoctrine()->getManager();

        $userData = $request->request->get('user');

        if ($registerService->checkUserExist($userData['username'])) {
            return new JsonResponse(['username' => 'Пользователь уже зарегистрирован'], 400);
        }

        if ($registerService->checkUserExist($userData['email'])) {
            return new JsonResponse(['username' => 'Пользователь уже зарегистрирован'], 400);
        }

        if ($entityManager->getRepository(AbandonedRegister::class)->findOneBy(
            ['email' => $userData['email']]
        )) {
            return new JsonResponse(['username' => 'Пользователь уже зарегистрирован'], 200);
        }

        $abandonedRegister = new AbandonedRegister();
        $abandonedRegister->setUsername($registerService->clearPhone($userData['username']));
        $abandonedRegister->setEmail($userData['email']);
        $abandonedRegister->setPassword($userData['password']);
        $abandonedRegister->setSubscribe(filter_var($userData['subscribe'], FILTER_VALIDATE_BOOLEAN));

        $entityManager->persist($abandonedRegister);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'id' => $abandonedRegister->getId()], 200);
    }

    /**
     * @Route("/register", name="app.apisecurity.register")
     */
    public function registerAction(Request $request)
    {
        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');

        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        /** @var UtmService $utmService */
        $utmService = $this->get('app.service.utm_service');

        $form = $this->createForm(UserType::class);

        $userData = $request->request->get('user');

        $form->submit($userData);
        if (!$form->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        $username = trim($form->get('username')->getData());
        if ($registerService->checkUserExist($username)) {
            return new JsonResponse(['username' => 'Пользователь уже зарегистрирован'], 400);
        }

        $email = trim($form->get('email')->getData());
        if ($registerService->checkUserExist($email)) {
            return new JsonResponse(['username' => 'Пользователь уже зарегистрирован'], 400);
        }

        $group = $registerService->getGroupByName($userData['group']);

        $userManager = $this->container->get('fos_user.user_manager');

        /** @var User $user */
        $user = $form->getData();
        $user->setUsername($registerService->clearPhone($user->getUsername()));
        $user->setCreatedAt(new \DateTime());
        $user->setGroups([$group]);
        if ($user->isCustomer()) {
            $user->setCustomerNow();
        }
        if ($user->isCarrier()) {
            $user->setCarrierNow();
        }
        $user->setEnabled(true);
        $user->setPlainPassword($userData['password']);
        $user->setRoles(['ROLE_USER']);
        $user->setPhone($user->getUsername());
        $user->setIp($request->getClientIp());

        $abandonedRegisterRepository = $this->getDoctrine()->getRepository(AbandonedRegister::class);
        $data = $abandonedRegisterRepository->findByUsername($user->getUsername());

        if (!$data) {
            $emailTemplate = 'register';
        } else {
            $emailTemplate = 'register_with_promocode';
        }

        $mailService->registerEmail($emailTemplate, $user->getEmail(), ['entity' => $user]);

        $utm = $utmService->getUtm();
        $user->setUtm($utm);

        $userManager->updateUser($user, true);

        return new JsonResponse(['status' => true, 'id' => $user->getId()], 200);
    }

    /**
     * @Route("/register_organization", name="app.apisecurity.register_organization")
     */
    public function registerOrganizationAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        $editData = $request->request->get('edit');

        if (isset($editData)) {
            $editData['organization'] = ($editData['organization'] === 'true');
            $editData['drivers'] = ($editData['drivers'] === 'true');
        }

        $userData = $request->request->get('user');

        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);
        $driverRepository = $this->getDoctrine()->getRepository(OrganizationDriver::class);
        $driverDocumentRepository = $this->getDoctrine()->getRepository(OrganizationDriverDocument::class);

        $organizationData = $request->get('organization');

        if (isset($organizationData['id'])) {
            $organization = $organizationRepository->find($organizationData['id']);
            $organizationForm = $this->createForm(OrganizationTypeForm::class, $organization);
        } else {
            $organizationForm = $this->createForm(OrganizationTypeForm::class);
        }

        if ((isset($editData) && $editData['organization']) || !isset($editData)) {
            $organizationForm->submit($request->get('organization'));
            if (!$organizationForm->isValid()) {
                return new JsonResponse($this->getErrorsFromForm($organizationForm), 400);
            }

            /** @var Organization $organization */
            $organization = $organizationForm->getData();
            $organization->resetVerification();
        }

        $oldStatus = [];
        foreach ($organization->getDrivers() as $driver) {
            $oldStatus[$driver->getInn()] = $driver->getVerifiedCode();
        }

        if ((isset($editData) && $editData['drivers']) || !isset($editData)) {
            $resultDrivers = [];
            if (isset($organizationData['drivers']) && !empty($organizationData['drivers'])) {
                $drivers = $organizationData['drivers'];
                foreach ($drivers as $driverItem) {

                    if (isset($driverItem['id'])) {
                        $driver = $driverRepository->find($driverItem['id']);
                        $organizationDriverForm = $this->createForm(OrganizationDriverType::class, $driver);
                    } else {
                        $organizationDriverForm = $this->createForm(OrganizationDriverType::class);
                    }

                    $organizationDriverForm->submit($driverItem);
                    if (!$organizationDriverForm->isValid()) {
                        return new JsonResponse($this->getErrorsFromForm($organizationDriverForm), 400);
                    }
                    /** @var OrganizationDriver $driver */
                    $driver = $organizationDriverForm->getData();

                    $driver->setDocuments(new ArrayCollection());
                    foreach ($driverItem['documents'] as $documentItem) {
                        if (isset($documentItem['id'])) {
                            $document = $driverDocumentRepository->find($documentItem['id']);
                            $organizationDriverDocumentForm = $this->createForm(OrganizationDriverDocumentType::class, $document);
                        } else {
                            $organizationDriverDocumentForm = $this->createForm(OrganizationDriverDocumentType::class);
                        }

                        $organizationDriverDocumentForm->submit($documentItem);

                        if (!$organizationDriverDocumentForm->isValid()) {
                            return new JsonResponse($this->getErrorsFromForm($organizationDriverDocumentForm), 400);
                        }
                        $driver->addDocument($organizationDriverDocumentForm->getData());
                    }

                    $resultDrivers[] = $driver;
                }

                $organization->setDrivers($resultDrivers);
            }
        }

        if (!is_object($user) && $userData) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->find($userData['id']);
        }

        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        /** @var DispatchService $dispatchService */
        $dispatchService = $this->get('app.dispatch');

        if ((isset($editData) && $editData['organization']) || !isset($editData)) {
			$url = $request->request->get('url');
            $dispatchService->dispatch($user,  $userData['subscribe'] == 'true',  $url ? $url : '/personal/profile');
        }

        if (isset($userData['group'])) {
            $group = $registerService->getGroupByName($userData['group']);
            $user->addGroup($group);
        }

        if (isset($userData['email'])) {
            $user->setEmail($userData['email']);
        }

        if (isset($userData['group']) and $userData['group'] == 'sender') {
            $user->setCustomerNow();
        } elseif (isset($userData['group']) and $userData['group'] == 'carrier') {
            $user->setCarrierNow();
        }

        if (isset($userData['group']) and $userData['group'] == 'sender') {
            $user->setCustomerNow();
        } elseif (isset($userData['group']) and $userData['group'] == 'carrier') {
            $user->setCarrierNow();
        }

        $organization->setUser($user);

        if (isset($userData['group']) and $userData['group'] == 'sender') {
            $user->setOrganizationCustomer($organization);
        } elseif (isset($userData['group']) and $userData['group'] == 'carrier') {
            $user->setOrganizationCarrier($organization);
        }

        $entityManager->persist($organization);
        $entityManager->flush();

        $verificationEvent = false;
        if (isset($organizationData['drivers']) && !empty($organizationData['drivers'])) {
            $updateAction = ($request->get('action')) ? $request->get('action') : 'full';
            $sendOnError = ($updateAction == 'register') ? false : true;
            $updateAction = ($updateAction == 'register') ? 'full' : $updateAction;

            /** @var VerificationService $verificationService */
            $verificationService = $this->get('app.verification');
            $verificationStatus = $verificationService->verify($organization, $sendOnError, true, $updateAction);
        }

        $newStatus = [];
        foreach ($organization->getDrivers() as $driver) {
            $newStatus[$driver->getInn()] = $driver->getVerifiedCode();
        }

        if ($oldStatus != $newStatus) {
            $verificationEvent = true;
        }

        return new JsonResponse([
            'status' => true,
            'id' => $organization->getId(),
            'verifiedCode' => $organization->getVerifiedCode(),
            'verificationEvent' => $verificationEvent
        ], 200);
    }

    /**
     * @Route("/register_update", name="app.apisecurity.register_update")
     */
    public function registerUpdateAction(Request $request)
    {
        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        $userData = $request->request->get('user');
        $user = $userManager->findUserBy(['id' => $userData['id']]);

        $form = $this->createForm(UserProfileType::class, $user);

        $form->submit($userData);
        if (!$form->isValid()) {
            return new JsonResponse($this->getErrorsFromForm($form), 400);
        }

        /** @var DispatchService $dispatchService */
        $dispatchService = $this->get('app.dispatch');

        /** @var User $user */
        $user = $form->getData();
        $user->setUsername($registerService->clearPhone($user->getUsername()));
        $user->setEnabled(true);
        $user->setPhone($user->getUsername());
        $userManager->updateUser($user);

        /** @var DispatchService $dispatchService */
        $dispatchService = $this->get('app.dispatch');
        if (isset($userData['subscribe'])) {
            $dispatchService->dispatch($user,  $userData['subscribe'] == 'true', $request->request->get('url'));
        }

        return new JsonResponse(['status' => true, 'id' => $user->getId()], 200);
    }

    /**
     * @Route("/abandonedRegisterData/{id}", name="app.apisecurity.abandonedRegisterData")
     */
    public function abandonedRegisterDataAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(AbandonedRegister::class);
        $data = $repository->find($id);

        return new JsonResponse(['status' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/profile", name="app.apisecurity.profile")
     */
    public function profileAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);

        $organization = $organizationRepository->getByUser($user);

        $organizationTypeRepository = $entityManager->getRepository(OrganizationType::class);
        $organizationTypes = $organizationTypeRepository->findBy([], ['position' => 'ASC']);

        foreach ($organizationTypes as $organizationType) {
            $params['organizationTypes'][] = $organizationType->jsonSerialize();
        }

        return $this->answer(['organization' => $organization, 'user' => $user, 'params' => $params]);
    }

    /**
     * @Route("/register_person", name="app.apisecurity.register_person")
     */
    public function registerPersonAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userData = $request->request->get('user');

        /** @var User $user */
        $user = $entityManager->getRepository(User::class)->find($userData['id']);

        if (!$user) {
            return new JsonResponse('user not found', 400);
        }

        /** @var RegisterService $registerService */
        $registerService = $this->get('app.security');

        $group = $registerService->getGroupByName($userData['group']);
        $user->addGroup($group);
        $user->setCustomerNow();

        $user->setFirstName($userData['firstname']);
        $user->setNumber($userData['number']);
        $user->setSeries($userData['series']);

        /** @var DispatchService $dispatchService */
        $dispatchService = $this->get('app.dispatch');

        if (isset($userDataa['subscribe'])) {
            $dispatchService->dispatch($user,  $userData['subscribe'] == 'true', $request->request->get('url'));
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'id' => $user->getId()], 200);
    }

    /**
     * @Route("/change_password/sendsms", name="app.apisecurity.change_password_sendsms")
     */
    public function changePasswordSendSmsAction(Request $request)
    {
        /** @var ChangePassService $changePassService */
        $changePassService = $this->get('app.change_pass');

        $user = $this->getUser();
        return new JsonResponse(['status' => $changePassService->sendSms($user->getPhone())], 200);
    }

    /**
     * @Route("/change_password/checksms", name="app.apisecurity.change_password_checksms")
     */
    public function changePasswordCheckSmsAction(Request $request)
    {
        /** @var ChangePassService $changePassService */
        $changePassService = $this->get('app.change_pass');

        $code = $request->query->get('code', '');

        return new JsonResponse(['status' => $changePassService->checkSmsCode($code)], 200);
    }

    /**
     * @Route("/change_password", name="app.apisecurity.change_password")
     */
    public function changePasswordAction(Request $request)
    {
        /** @var ChangePassService $changePassService */
        $changePassService = $this->get('app.change_pass');
        $userManager = $this->container->get('fos_user.user_manager');

        $password = $request->query->get('password', '');

        /** @var User $user */
        $user = $this->getUser();

        $user->setPlainPassword($password);
        $user->setEnabled(true);

        $userManager->updateUser($user, true);

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = current($childErrors);
                }
            }
        }

        return $errors;
    }
}
