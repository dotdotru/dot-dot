<?php

namespace App\Controller\Api\App\Bot;

use App\Controller\Api\BaseApiController;
use App\Entity\City;
use App\Entity\Direction;
use App\Service\AuctionService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/api/app/bot")
 * @SWG\Tag(name="Бот")
 **/
class BotController extends BaseApiController
{
    /**
     * @Route("/city/list", name="api.bot.city_list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="return list cities",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="status", type="boolean"),
     *         @SWG\Property(property="data", type="object",
     *            @SWG\Items(
     *                @SWG\Property(property="status", type="boolean"),
     *            )
     *         )
     *     ),
     * )
     * @SWG\Parameter(name="access_token", in="query", type="string", description="Токен доступа")
     */
    public function listCitiesAction(Request $request)
    {
        $cityRepository = $this->getDoctrine()->getRepository(City::class);
        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);

        $cityList = $cityRepository->findBy(['published' => true]);
        $directionList = $directionRepository->findBy(['published' => true]);

        /** @var AuctionService $auctionService */
        $auctionService = $this->get('app.auction');

        $totalWeight = [];
        /** @var Direction $direction */
        foreach ($directionList as $direction) {
            $auctionService->setDirection($direction);
            $weight = $auctionService->getWeightWarehouses(1);

            $city = $direction->getCityFrom();

            if (!isset($totalWeight[$city->getId()])) {
                $totalWeight[$city->getId()] = [
                    'city' => $city,
                    'totalWeight' => 0,
                ];
            }

            $totalWeight[$city->getId()]['totalWeight'] += $weight;
        }

        return $this->answer($totalWeight);
    }

    /**
     * @Route("/direction/list", name="api.bot.direction_list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="return list direction weight",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="status", type="boolean"),
     *         @SWG\Property(property="data", type="object",
     *            @SWG\Items(
     *                @SWG\Property(property="status", type="boolean"),
     *            )
     *         )
     *     ),
     * )
     * @SWG\Parameter(name="access_token", in="query", type="string", description="Токен доступа")
     */
    public function listDirectionWeightAction(Request $request)
    {
        $cityRepository = $this->getDoctrine()->getRepository(City::class);
        $directionRepository = $this->getDoctrine()->getRepository(Direction::class);

        $cityList = $cityRepository->findBy(['published' => true]);
        $directionList = $directionRepository->findBy(['published' => true]);

        /** @var AuctionService $auctionService */
        $auctionService = $this->get('app.auction');

        $totalWeight = [];
        /** @var Direction $direction */
        foreach ($directionList as $direction) {
            $auctionService->setDirection($direction);
            $weight = $auctionService->getWeightWarehouses(1);

            if (!isset($totalWeight[$direction->getId()])) {
                $totalWeight[$direction->getId()] = [
                    'direction' => $direction,
                    'url' => $this->generateUrl('app.auction.direction', ['slug' => $direction->getSlug(true)], UrlGeneratorInterface::ABSOLUTE_URL),
                    'totalWeight' => 0,
                ];
            }

            $totalWeight[$direction->getId()]['totalWeight'] += $weight;
        }

        return $this->answer($totalWeight);
    }
}
