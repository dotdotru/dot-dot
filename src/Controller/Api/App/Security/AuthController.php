<?php

namespace App\Controller\Api\App\Security;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Controller\Api\BaseApiController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security as CSecurity;
use GuzzleHttp;

/** @Route("/api/app/security")
 * @SWG\Tag(name="Авторизация")
 */
class AuthController extends BaseApiController
{
    /**
     * @Route("/auth", name="api.security.auth", methods={"POST"}),
     * @SWG\Response(
     *     response=200,
     *     description="return status auth",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="status", type="boolean"),
     *         @SWG\Property(property="data", type="object",
     *            @SWG\Items(
     *                @SWG\Property(property="status", type="boolean"),
     *            )
     *         )
     *     ),
     * )
     * @SWG\Parameter(name="username", in="formData", type="string", description="Логин")
     * @SWG\Parameter(name="password", in="formData", type="string", description="Пароль")
     * @SWG\Parameter(name="client_id", in="formData", type="string")
     * @SWG\Parameter(name="client_secret", in="formData", type="string")
     */
    public function authAction(Request $request)
    {
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $clientId = $request->request->get('client_id');
        $clientSecret = $request->request->get('client_secret');

        try {
            $client = new GuzzleHttp\Client();

            /** @var GuzzleHttp\Psr7\Response $response */
            $response = $client->post($this->generateUrl('fos_oauth_server_token', [], 0), [
                'form_params' => [
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                    'grant_type' => 'password',
                    "username" => $username,
                    "password" => $password,
                ],
            ]);

            if ($response->getStatusCode() == 200) {
                $json = json_decode($response->getBody()->getContents(), true);

                return $this->answer(['accsess_token' => $json['access_token']]);
            } else {
                return $this->errorAnswer('not auth 1');
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if (in_array($e->getCode(), [400, 401])) {
                return $this->errorAnswer('not auth 2');
            }
        } catch (\Exception $e) {

            return $this->errorAnswer('not auth 3');
        }

        return $this->errorAnswer('not auth 4');
    }
}


