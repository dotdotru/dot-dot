<?php

namespace App\Controller\Api;


use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\InsuredQueue;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Form\Order\SaveType;
use App\Form\Shipping\ShippingType;
use App\Repository\OrderRepository;
use App\Service\AuctionService;
use App\Service\CalculateService;
use App\Service\Integration\Mile\CalcMileCost;
use App\Service\MailService;


use App\Service\Order\RecalculateOrderPriceService;
use App\Service\OrderUpdateService;
use App\Service\PromocodeService;
use App\Service\Shipping\ShippingChangedService;
use App\Service\Shipping\ShippingGeoService;
use App\Service\Shipping\ShippingHydroBoardExistsService;
use App\Service\Shipping\ShippingNotificationService;
use App\Service\Shipping\ShippingRemoveAllService;
use App\Service\Shipping\ShippingSaveService;


use App\Service\Stock\NearestStockForAddress;
use App\Service\Stock\StockListForWorker;
use App\Service\TrackService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/order/customer")
**/
class ApiOrderController extends BaseApiController
{

    /**
     * @var ShippingChangedService
     */
    protected $shippingChangedService;

    /**
     * @var ShippingSaveService
     */
    protected $shippingSaveService;

    /**
     * @var OrderUpdateService
     */
    protected $orderUpdateService;

    /**
     * @var ShippingGeoService
     */
    protected $shippingGeoService;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ShippingNotificationService
     */
    protected $shippingNotificationService;

    /**
     * @var ShippingHydroBoardExistsService
     */
    protected $hydroBoardExistsService;


    /**
     * @var NearestStockForAddress
     */
    protected $nearestStockForAddress;

    /**
     * @var ShippingRemoveAllService
     */
    protected $shippingRemoveAllService;

    /**
     * @var RecalculateOrderPriceService
     */
    protected $recalcaulateOrderService;

    public function __construct(
        EntityManagerInterface $em,
        ShippingChangedService $shippingChangedService,
        ShippingSaveService $shippingSaveService,
        ShippingRemoveAllService $shippingRemoveAllService,
        OrderUpdateService $orderUpdateService,
        ShippingGeoService $shippingGeoService,
        ShippingNotificationService $shippingNotificationService,
        ShippingHydroBoardExistsService $hydroBoardExistsService,
        NearestStockForAddress $nearestStockForAddress,
        RecalculateOrderPriceService $recalcaulateOrderService
    )
    {
        $this->em = $em;
        $this->shippingChangedService = $shippingChangedService;
        $this->shippingSaveService = $shippingSaveService;
        $this->orderUpdateService = $orderUpdateService;
        $this->shippingGeoService = $shippingGeoService;
        $this->shippingNotificationService = $shippingNotificationService;
        $this->hydroBoardExistsService = $hydroBoardExistsService;
        $this->nearestStockForAddress = $nearestStockForAddress;
        $this->shippingRemoveAllService = $shippingRemoveAllService;
        $this->recalcaulateOrderService = $recalcaulateOrderService;
    }


    /**
     * @Route("/save-order/{order}", name="app.api_order_customer.saveOrder")
     */
    public function saveOrderAction(Request $request, Order $order)
    {

        if ($order->getUser() != $this->getUser()) {
            return new JsonResponse(['error' => 'Запрещено'], 400);
        }
        $this->em->getConnection()->beginTransaction();


        $orderData = $request->request->get('order');

        $form = $this->createForm(SaveType::class, $order);
        if ($form->submit($orderData) && !$form->isValid()) {
            return new JsonResponse(['error' => 'Неудалось сохранить данные', 'errors' => $form->getErrors(true)], 400);

        }

        if ($order->isNew()) {
            $order->setStatus(Order::STATUS_WAITING);
        }


        if ($this->orderUpdateService->orderSynch($order) === false) {
            $this->em->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        // надо делать 2 раза из-за базы
        if ($this->orderUpdateService->orderSynch($order) === false) {
            $this->em->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }


        /** @var Direction $direction */
        $direction = $order->getDirection();


        $this->em->persist($order);
        $this->em->flush();

        $shippingTypes = [Shipping::TYPE_PICKUP, Shipping::TYPE_DELIVER];
        foreach ($shippingTypes as $shippingType) {
            $shippingData = $request->request->get($shippingType);

            if ($shippingData) {
                $newShipping = new Shipping();
                $newShipping->setType($shippingType);
                $form = $this->createForm(ShippingType::class, $newShipping);

                if ($form->submit($shippingData) && $form->isValid()) {
                    $newShipping = $form->getData();

                    if ($this->shippingChangedService->isChanged($newShipping, $order->getShipping($shippingType))) {
                        if ($this->shippingChangedService->changeAvailable($order, $newShipping, $order->getShipping($shippingType))) {
                            try {
                                $oldShipping = $order->getShipping($shippingType);
                                $this->shippingSaveService->save($order, $newShipping, $order->getShipping($shippingType));

                                $this->em->persist($newShipping);
                                $order->addShipping($newShipping);

                                $this->shippingNotificationService->notification($order, $newShipping, $oldShipping);


                                if ($newShipping->isActive()) {
                                    if (in_array($order->getStatus(), [Order::STATUS_BOOKED, Order::STATUS_NEW, Order::STATUS_WAITING])) {
                                        $priorityStock = $shippingType == Shipping::TYPE_PICKUP ? $direction->getPriorityStockFrom() : $direction->getPriorityStockTo();

                                        if ($priorityStock) {
                                            $nearestStock = $priorityStock;
                                        } else {
                                            $nearestStock = $this->nearestStockForAddress->getNearestStock($newShipping->getAddress(), $shippingType == Shipping::TYPE_PICKUP ? $direction->getCityFrom() : $direction->getCityTo());
                                        }

                                        if ($nearestStock) {
                                            $newShipping->setStock($nearestStock);
                                            $order->setStockFrom($nearestStock);
                                            $shippingType == Shipping::TYPE_PICKUP ? $order->setStockFrom($nearestStock) : $order->setStockTo($nearestStock);
                                        }
                                    }

                                    $shippingType == Shipping::TYPE_PICKUP ? $order->setStockFrom($newShipping->getStock()) : $order->setStockTo($newShipping->getStock());
                                }
                            } Catch (\Exception $e) {
                                // @todo уведомлять о том, что не получилось сохранить доставку
                            }
                        } else {
                            // @todo сообщение о том, что редактирование недоступно
                        }
                    }
                } else {
                    return new JsonResponse(['error' => 'Неудалось сохранить данные2', 'errors' => $form->getErrors()], 400);
                }
            }
        }

        $this->recalcaulateOrderService->recalculate($order);

        if ($this->orderUpdateService->orderSynch($order) === false) {
            $this->em->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }


        // Отправляем обновления в страховую
        $insuredQueue = new InsuredQueue();
        $insuredQueue->setOrder($order);
        $this->em->persist($insuredQueue);


        $this->em->persist($order);
        $this->em->flush();
        $this->em->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/{orderId}/send_receiver_email", name="app.api_order_customer.send_receiver_email")
     */
    public function sendReciverEmailAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->findOneBy(['id' => $orderId]);

        /** @var Order $order */
        if (!$order) {
            return new JsonResponse(['error' => 'Не найден заказ'], 400);
        }

        /** @var Order $order */
        if ($order->getUser() != $this->getUser()) {
            return new JsonResponse(['error' => 'Запрещено'], 400);
        }

        /** @var Receiver $receiver */
        $receiver = $order->getReceiver();
        if (!$receiver) {
            return new JsonResponse(['error' => 'Не указан получатель'], 400);
        }

        $organization = $entityManager->getRepository(Organization::class)->findOneBy(['user' => $order->getUser()]);

        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');

        $to = $order->getReceiver()->getContactEmail();
        if ($receiver->isUridical()) {
            $result = $mailService->registerEmail(
                'receiver_uridical',
                $to,
                [
                    'entity' => $order,
                    'order' => $order,
                    'organization' => $organization,
                ],
                'receiveruridical'
            );
        } else {
            $result = $mailService->registerEmail(
                'receiver_nouridical',
                $to,
                [
                    'entity' => $order,
                    'order' => $order,
                    'organization' => $organization,
                ],
                'receivernouridical'
            );
        }

        $entityManager->persist($order);
        $entityManager->flush();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/{orderId}/stop_send", name="app.api_order_customer.stop_send")
     */
    public function stopSendAction(Request $request, $orderId)
    {
        $result = [];
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $this->get('app.order.update');

        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');


        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->findOneBy(['id' => $orderId]);

        /** @var Order $order */
        if (!$order) {
            return new JsonResponse(['error' => 'Не найден заказ'], 400);
        }

        /** @var Order $order */
        if ($order->getUser() != $this->getUser()) {
            return new JsonResponse(['error' => 'Запрещено'], 400);
        }

        if ($order->isCreated()||$order->isWaiting()) {
            // удаляем заказ
            $order->setRemove(true);
        }

        // Отменяем все доставки
        $this->shippingRemoveAllService->removeOldShippings($order);

        $order->setStopSend(true);
        $order->setStatus(Order::STATUS_SUSPENDED);
        $order->setSuspendDate(null);

        $entityManager->persist($order);
        $entityManager->flush();

        if (!$orderUpdateService->orderSuspend($order)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $mailService->registerEmail('stop_send', $this->getParameter('admin_email'), ['entity' => $order]);

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'result' => $result], 200);
    }

    /**
     * @Route("/{orderId}/pause_delivery", name="app.api_order_customer.pause_delivery")
     */
    public function pauseDeliveryAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $this->get('app.order.update');

        /** @var MailService $mailService */
        $mailService = $this->get('app.mail');

        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->findOneBy(['id' => $orderId]);

        /** @var Order $order */
        if (!$order) {
            return new JsonResponse(['error' => 'Не найден заказ'], 400);
        }

        /** @var Order $order */
        if ($order->getUser() != $this->getUser()) {
            return new JsonResponse(['error' => 'Запрещено'], 400);
        }

        $order->setPauseDelivery(true);
        $order->setStatus(Order::STATUS_SUSPENDED);
        $order->setSuspendDate(null);

        $entityManager->persist($order);
        $entityManager->flush();

        if (!$orderUpdateService->orderSuspend($order)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $mailService->registerEmail('pause_delivery', $this->getParameter('admin_email'), ['entity' => $order]);

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/config", name="app.api_order_customer.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var AuctionService $customerCalcService;
         */
        $service = $this->get('app.customer_calc');

        /** @var StockListForWorker $stockListService */
        $stockListService = $this->get('app.service.stock.for_sender');

        $settings = $service->getSettings();
        $params['directions'] = $service->getDirections();
        $params['packageTypes'] = $service->getPackageTypes();
        $params['packings'] = $service->getPackings();


        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);
        $fileGroups = $fileGroupRepository->getDefaultCustomer();

        /** @var FileGroup $fileGroup */
        foreach ($fileGroups as $fileGroup) {
            $params['fileGroups'][] = $fileGroup->jsonSerialize();
        }

        $organizationTypeRepository = $entityManager->getRepository(OrganizationType::class);
        $organizationTypes = $organizationTypeRepository->findBy([], ['position' => 'ASC']);

        foreach ($organizationTypes as $organizationType) {
            $params['organizationTypes'][] = $organizationType->jsonSerialize();
        }

        $user = $this->getUser();
        $personRepository = $entityManager->getRepository(Person::class);
        $persons = $personRepository->findBy(['user' => $user, 'remove' => false]);
        $listPersons = [];
        foreach($persons as $key => $person) {
			if ($person->getExternalId()) {
                $listPersons[] = $person;
			}
		}
        $params['listPersons'] = $listPersons;

        if ($user) {
            $params['stocks'] = $stockListService->getStocks($user);
        }

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }

    /**
     * @Route("/order", name="app.api_order_customer.order")
     */
    public function orderDataAction(Request $request)
    {
        $orderId = $request->request->getInt('orderId', null);

        /** @var PromocodeService $promocodeService */
        $promocodeService = $this->get('app.promocode');

        /** @var ShippingService $shippingService */
        //$shippingService = $this->get('app.service.shipping');

        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');

        $settings = $this->get('settings')->group('customer_calc');

        /** @var User $user */
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Order::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        /** @var Order $order */
        $order = $orderRepository->getOrderByUser($user, $orderId);

        $orderData = $order->jsonSerialize();

        $hydroBoardResult = [
            'pickUpHydroBoard' => $this->hydroBoardExistsService->check($order->getDirection()->getCityFrom(), $order->getTotalWeight()),
            'deliverHydroBoard' => $this->hydroBoardExistsService->check($order->getDirection()->getCityTo(), $order->getTotalWeight()),
        ];
        $orderData = array_merge($orderData, $hydroBoardResult);

        $defaultFiles = [];
        $fileGroups = $fileGroupRepository->getDefaultCustomer();
        foreach ($fileGroups as $fileGroup) {
            if ($fileGroup->getFiles()->count() > 0) {
                foreach ($fileGroup->getFiles() as $file) {
                    $file->setPath('/media/download/'.$file->getMedia()->getId());
                    $defaultFiles[] = $file;
                }
            }
        }

        foreach ($defaultFiles as $file) {
            $orderData['files'][] = $file;
        }

        foreach ($orderData['files'] as &$file) {
            $file->setPath('/media/download/'.$file->getMedia()->getId());
        }

        if (!empty($orderData['insuredPdf'])) {
            $fileGroup = $fileGroupRepository->findOneBySlug('polis');
            $fileItem = new File();
            $fileItem->setPath('/personal/insured-pdf/'.$orderData['id']);
            $fileItem->setGroup($fileGroup);
            $orderData['files'][] = $fileItem;
        }
        $orderData['insuredPdf'] = '';



        return new JsonResponse(['status' => true, 'data' => $orderData], 200);
    }

    /**
     * @Route("/orders", name="app.api_order_customer.orders")
     */
    public function ordersAction(Request $request)
    {
        $orderId = $request->query->getInt('orderId', null);
        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 10);
        $filter = $request->request->get('filter', '');
        $sort = $request->request->get('sort', []);

        /** @var PromocodeService $promocodeService */
        $promocodeService = $this->get('app.promocode');

        /** @var ShippingService $shippingService */
       // $shippingService = $this->get('app.service.shipping');

        /** @var CalculateService $calculateService */
        $calculateService = $this->get('app.calculate');

        $settings = $this->get('settings')->group('customer_calc');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderRepository $orderCustomerRepository */
        $orderCustomerRepository = $entityManager->getRepository(Order::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        $filterStatus = [];
        if ($filter) {
            $filterStatus = ['status' => $filter];
        }

        $sorts = [];
        if (isset($sort['by'])) {
            $sorts = [$sort['by'] => $sort['direction']];
        } else {
            $sorts = ['id' => 'desc'];
        }

        $orders = $orderCustomerRepository->getByUser($user, $page, $limit, $sorts, $filterStatus);

        $result = ['orders' => []];
        /** @var Order $order */
        foreach ($orders as $order) {
            $orderData = $order->jsonSerialize();

            $hydroBoardResult = []; //$shippingService->checkExistHydroBoard($order);
            $orderData = array_merge($orderData, $hydroBoardResult);

            $result['orders'][] = $orderData;
        }

        $defaultFiles = [];
        $fileGroups = $fileGroupRepository->getDefaultCustomer();
        foreach ($fileGroups as $fileGroup) {
            if ($fileGroup->getFiles()->count() > 0) {
                foreach ($fileGroup->getFiles() as $file) {
                    $file->setPath('/media/download/'.$file->getMedia()->getId());
                    $defaultFiles[] = $file;
                }
            }
        }

        /** @var FileGroup $fileGroup */
        foreach ($result['orders'] as &$item) {
            foreach ($defaultFiles as $file) {
                $item['files'][] = $file;
            }
            foreach ($item['files'] as &$file) {
				 if ($file->getMedia()) {
					 $file->setPath('/media/download/'.$file->getMedia()->getId());
				 }
            }

            if (!empty($item['insuredPdf'])) {
				$fileGroup = $fileGroupRepository->findOneBySlug('polis');
				$fileItem = new File();
				$fileItem->setPath('/personal/insured-pdf/'.$item['id']);
				$fileItem->setGroup($fileGroup);
				$item['files'][] = $fileItem;
			}
			$item['insuredPdf'] = '';
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($orders->count() / $limit),
        ];

        if ($request->getSession()->has('CUSTOMER_ORDER_MESSAGE')) {
            $result['user_message'] = $request->getSession()->get('CUSTOMER_ORDER_MESSAGE');
            $request->getSession()->remove('CUSTOMER_ORDER_MESSAGE');
        }


        return $this->answer($result);
    }

    /**
     * @Route("/remove", name="app.api_order_customer.remove")
     */
    public function removeAction(Request $request)
    {
        $ids = $request->request->get('ids', []);

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderRepository $orderCustomerRepository */
        $orderCustomerRepository = $entityManager->getRepository(Order::class);
        $orders = $orderCustomerRepository->findBy(['id' => $ids, 'sender' => $user]);

        /** @var PromocodeService $promoCodeService */
        $promoCodeService = $this->container->get('app.promocode');

        /** @var RearrangePromocodeService $rearangePromocodeService */
        $rearangePromocodeService = $this->container->get('app.order.rearrange_promocode');

        $result = [];
        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->isRemoveExactStatus()) {
                $order->setRemove(true);
                $entityManager->persist($order);
            }
        }
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'result' => $result], 200);
    }



    /**
     * @Route("/check_number", name="app.api_order_customer.checknumber")
     */
    public function checkNumberAction(Request $request)
    {
        $user = $this->getUser();

        /** @var TrackService $trackService */
        $trackService = $this->get('app.track');

        $data['result'] = false;
        $orderId = $request->query->get('number');
        if ($orderId && $trackService->checkOrderExist($orderId)) {
            /** @var Order $order */
            $order = $trackService->getOrder($orderId);

            $data['auth'] = false;
            $data['url'] = '';
            if (
                $order->getReceiver() && $order->getUser() && $order->getUser()->getId()
                && $user && $user->getId()
            ) {
                $data['auth'] = ($order->getUser()->getId() == $user->getId());
            }

            if ($data['auth']) {
                $data['url'] = $this->generateUrl('app.track.detail', ['id' => $orderId]);
            }


            $data['result'] = true;
        }


        return new JsonResponse(['status' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/sendsms", name="app.api_order_customer.sendsms")
     */
    public function sendsmsAction(Request $request)
    {
        /** @var TrackService $trackService */
        $trackService = $this->get('app.track');


        $phone = $request->query->get('phone');
        $number = $request->query->get('number');


        if (!$trackService->isValidPhone($phone)) {
            /** @var Order $order */
            $order = $trackService->getOrder($number);

            if (!$order) {
                return new JsonResponse(['error' => 'Не найден заказ'], 400);
            }

            /** @var Receiver $receiver */
            $receiver = $order->getReceiver();
            if ($receiver) {
                $phone = $receiver->getContactPhone();
            }
        }


        if (!$trackService->isValidPhone($phone)) {
            return new JsonResponse(['error' => 'Не указан телефон'], 400);
        }

        return new JsonResponse(['status' => $trackService->sendSms($phone)], 200);

    }

    /**
     * @Route("/checksms", name="app.api_order_customer.checksms")
     */
    public function checksmsAction(Request $request)
    {
        /** @var TrackService $trackService */
        $trackService = $this->get('app.track');

        if (!($code = $request->query->get('code'))) {
            return new JsonResponse(['error' => 'Не указан код'], 400);
        }

        $number = $request->query->get('number');

        /** @var Order $order */
        $order = $trackService->getOrder($number);

        if (!$order) {
            return new JsonResponse(['error' => 'Не найден заказ'], 400);
        }

        $data = [];
        $status = false;
        if ($trackService->checkSmsCode($code)) {
            $status = true;
            $data['url'] = $this->generateUrl('app.track.detail', ['id' => $order->getId(), 'hash' => $order->getHash()]);
        }

        return new JsonResponse(['status' => $status, 'data' => $data], 200);
    }

}
