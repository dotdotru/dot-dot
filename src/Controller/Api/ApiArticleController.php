<?php

namespace App\Controller\Api;

use App\Entity\Content\Article;
use App\Entity\Direction;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Lot;
use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType;
use App\Entity\Pallet;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Form\ReceiverType;
use App\Repository\OrderRepository;
use App\Repository\PalletRepository;
use App\Serializer\DateTimeNormalizer;
use App\Serializer\OrderNormalizer;
use App\Serializer\PalletNormalizer;
use App\Serializer\StockNormalizer;
use App\Service\AuctionService;
use App\Service\CalculateService;
use App\Service\CustomerCalcService;
use App\Service\MailService;
use App\Service\Mapper\Mover\MoverMapperService;
use App\Service\MoverService;
use App\Service\RearrangePromocodeService;
use App\Service\OrderUpdateService;
use App\Service\PromocodeService;
use App\Service\SmsService;
use App\Service\TrackService;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/news")
 **/
class ApiArticleController extends Controller
{
    /**
     * @Route("/list/html", name="app.api_article.list_html")
     */
    public function listHtmlAction(Request $request)
    {
        $filter = $request->request->get('filter');

        $newsRepository = $this->getDoctrine()->getRepository(Article::class);

        $news = $newsRepository->getBy(1, 100, ['id' => 'desc'], $filter);

        $tags = [];
        foreach ($news as $newsItem) {
            foreach ($newsItem->getTags() as $tag) {
                $tags[$tag->getId()] = $tag;
            }
        }

        $html = $this->renderView('block/articles.html.twig', ['articles' => $news, 'tags' => $tags]);

        return new JsonResponse(['status' => true, 'data' => ['html' => $html]], 200);
    }
}
