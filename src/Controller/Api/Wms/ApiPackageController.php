<?php

namespace App\Controller\Api\Wms;

use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Order;
use App\Entity\Stock;
use App\Entity\Base;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Repository\OrderRepository;
use App\Repository\PalletRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Repository\Wms\WmsPackageRepository;
use App\Service\CustomerCalcService;
use App\Service\Wms\SecurityService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/wms/wmspackage")
 **/
class ApiPackageController extends Controller
{
    /**
     * @Route("/packages-not-in-pallet", name="app.api_wmspackage.packages_not_in_pallet")
     */
    public function packagesNotInPalletAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        /** @var WmsPackageRepository $wmsPackageRepository */
        $wmsPackageRepository = $entityManager->getRepository(WmsPackage::class);

        /** @var BaseDirectionRepository $baseDirectionRepository */
        $baseDirectionRepository = $entityManager->getRepository(Base\Direction::class);

        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 1000);
        $status = $request->request->get('status', '');
        $filter = $request->request->get('filter', []);
        $sort = $request->request->get('sort', []);

        $sorts = [];
        if (isset($sort['by'])) {
            $sorts = [$sort['by'] => $sort['direction']];
        } else {
            $sorts = ['id' => 'desc'];
        }

        $packages = $wmsPackageRepository->getNotInPallet($securityService->getStock(), $page, $limit, $sorts, $filter);

        $result = ['packages' => []];
        /** @var WmsPackage $package */
        foreach ($packages as $package) {
            $result['packages'][] = $package->jsonSerialize();
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($packages->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @Route("/remove", name="app.api_wmspackage.remove")
     */
    public function removeAction(Request $request)
    {
        $ids = $request->request->get('ids', []);

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderRepository $orderCustomerRepository */
        $orderCustomerRepository = $entityManager->getRepository(Order::class);
        $orders = $orderCustomerRepository->findBy(['id' => $ids, 'sender' => $user]);

        $result = [];
        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->isRemoveExactStatus()) {
                $order->setRemove(true);
                $entityManager->persist($order);
            }
        }
        $entityManager->flush();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/config", name="app.api_wmspackage.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /**
         * @var CustomerCalcService $customerCalcService;
         */
        $service = $this->get('app.customer_calc');

        $params['directions'] = $service->getDirections();
        $params['packageTypes'] = $service->getPackageTypes();
        $params['packings'] = $service->getPackings();
        $params['stocks'] = $service->getStocks();

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }
}
