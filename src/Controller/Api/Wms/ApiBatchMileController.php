<?php

namespace App\Controller\Api\Wms;


use App\Entity\OrderPackage;
use App\Entity\Package;
use App\Entity\Wms\AbstractWmsBatch;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\FileUploadService;
use App\Service\Integration\CallBaseService;
use App\Service\Integration\Mile\MileBatchArrived;
use App\Service\Integration\Mile\MileBatchLoad;
use App\Service\IntegrationBaseService;
use App\Service\Shipping\BatchNotificationService;
use App\Service\Shipping\ShippingGeoService;
use App\Service\Shipping\ShippingPriceService;
use App\Service\Wms\SecurityService;
use App\Service\Wms\WmsBatchUpdateService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Vsavritsky\SettingsBundle\Service\Settings;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route("/api/wms/wmsbatch")
 **/
class ApiBatchMileController extends AbstractApiWmsController
{

    /**
     * @var CallBaseService
     */
    protected $caller;

    /**
     * @var BatchNotificationService
     */
    protected $batchNotificationService;

    /**
     * @var
     */
    protected $em;

    public function __construct(
        CallBaseService $caller,
        BatchNotificationService $batchNotificationService,
        EntityManagerInterface $em
    ) {
        $this->caller = $caller;
        $this->batchNotificationService = $batchNotificationService;
        $this->em = $em;
    }


    /**
     * @Route("/batch-mover-save/{wmsBatch}", name="app.api_wmsorder.batch_mover_save")
     */
    public function moverSaveAction(Request $request, WmsBatchMover $wmsBatch)
    {
        $this->checkAuth();

        $data = $request->request->get('batch');
        $packages = $data['package'];

        if (count($packages)) {
            foreach ($wmsBatch->getPackages() as $wmsPackage) {
                $this->em->remove($wmsPackage);
            }

            $wmsBatch->setPackages(new ArrayCollection());
            foreach ($packages as $package) {
                $wmsPackage = new WmsPackage();
                $wmsPackage->setDamaged((bool)$package['damaged']);
                $wmsBatch->addPackage($wmsPackage);

                $this->em->persist($wmsPackage);
            }
        }


        $this->em->persist($wmsBatch);
        $this->em->flush();


        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-mover-load/{orderId}", name="app.api_wmsorder.batch_mover_load")
     */
    public function moverLoadAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $this->caller->authByStock($securityService->getBaseStock());


        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatchMover::class);

        /** @var WmsBatch $wmsBatch */
        $wmsBatch = $wmsBatchRepository->find($orderId);


        $mileBatchLoadRequest = new MileBatchLoad();
        $mileBatchLoadRequest->setBatchId($wmsBatch->getExternalId())
            ->setStockId($wmsBatch->getStock()->getExternalId());

        $answer = $this->caller->exec($mileBatchLoadRequest);

        if (!$answer['success']) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось отгрузить партию']);
        }

        $wmsBatch->setStatus(AbstractWmsBatch::STATUS_SHIPPING);
        $wmsBatch->setReceptionAt(new \DateTime());
        $wmsBatch->setIssueAt(new \DateTime());

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        $this->batchNotificationService->notificationBatchLoaded($wmsBatch);

        return new JsonResponse(['status' => true], 200);
    }



    /**
     * @Route("/batch-mover-arrived/{wmsBatch}", name="app.api_wmsorder.batch_mover_arrived")
     */
    public function moverArrivedAction(Request $request, WmsBatchMover $wmsBatch)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $this->caller->authByStock($securityService->getBaseStock());

        $integrationBaseService->authByStock($securityService->getBaseStock());

        $entityManager->getConnection()->beginTransaction();


        $wmsBatch->setStatus(WmsBatchMover::STATUS_NOT_PAID);
        $wmsBatch->setReceptionAt(new \DateTime());
        $wmsBatch->setIssueAt(new \DateTime());
        $wmsBatch->getShipping()->setDone(true);

        $mileBatchArrivedRequest = new MileBatchArrived();
        $mileBatchArrivedRequest->setBatchId($wmsBatch->getExternalId())
            ->setStockId($wmsBatch->getStock()->getExternalId());

        $answer = $this->caller->exec($mileBatchArrivedRequest);

        $answer['success'] = true;
        if (!$answer['success']) {
            return new JsonResponse(['status' => false, 'error' => 'Неудалось принять партию'], 200);
        }

        $this->batchNotificationService->notificationBatchAccepted($wmsBatch);

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }


}
