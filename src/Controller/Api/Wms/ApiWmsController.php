<?php

namespace App\Controller\Api\Wms;

use App\Entity\Batch;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\InsuredQueue;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Stock;
use App\Entity\Base;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchPackage;
use App\Entity\Wms\WmsBatchPallet;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Repository\OrderRepository;
use App\Repository\PalletRepository;
use App\Repository\StockRepository;
use App\Repository\Wms\WmsBatchPalletRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\CustomerCalcService;
use App\Service\Mapper\Wms\WmsBatchMapperService;
use App\Service\Mapper\Wms\WmsOrderMapperService;
use App\Service\Wms\SecurityService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/wms")
 **/
class ApiWmsController extends Controller
{
    /**
     * @Route("/order-waiting", name="app.wms.ordersWaiting")
     */
    public function ordersWaitingAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $data = $data['Order'];

        $this->get('monolog.logger.intergation_base')->error(sprintf('Method %s', 'order-waiting'));
        $this->get('monolog.logger.intergation_base')->error(sprintf('Data %s', $request->getContent()));

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var OrderRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Order::class);

        /** @var WmsOrderMapperService $wmsOrderMapperService */
        $wmsOrderMapperService = $this->get('app.wmsorder.mapper');

        $externalId = $data['ext_id'];
        $wmsOrder = $wmsOrderRepository->findOneByExternalId($externalId);
        if (!$wmsOrder) {
            $wmsOrder = new WmsOrder();
            $wmsOrder->setExternalId($externalId);
            $wmsOrder->setTmp(false);

            /** @var Order $order */
            $order = $orderRepository->getById($wmsOrder->getExternalId());
            $wmsOrder->setOrder($order);

            if ($order && $order->getPromocode()) {
                // @todo убираем промокод
            //    $wmsOrder->setPromocode($order->getPromocode());
            }
        }

        $wmsOrder = $wmsOrderMapperService->mapOrderToWmsOrder($wmsOrder, $data);

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

    /**
     * @Route("/batch-waiting", name="app.wms.batchWaiting")
     */
    public function batchWaitingAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $this->get('monolog.logger.intergation_base')->debug(sprintf('Method %s', 'batch-waiting'));
        $this->get('monolog.logger.intergation_base')->debug(sprintf('Data %s', $request->getContent()));

        $data = $data['Batch'];

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);
        /** @var WmsBatchMapperService $wmsBatchMapperService */
        $wmsBatchMapperService = $this->get('app.wmsbatch.mapper');

        $externalId = $data['ext_id'];
        $wmsBatch = $wmsBatchRepository->findOneByExternalId($externalId);

        if (!$wmsBatch) {
            $wmsBatch = new WmsBatch();
            $wmsBatch->setExternalId($externalId);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $orderRepository = $entityManager->getRepository(Batch::class);
        /** @var Batch $order */
        $order = $orderRepository->findOneById($externalId);
        $wmsBatch->setCurrentPricePerKilo($order->getCurrentPricePerKilo());

        $wmsBatch = $wmsBatchMapperService->mapBatchToWmsBatch($wmsBatch, $data);
        foreach ($wmsBatch->getPallets() as $pallet) {
            $entityManager->persist($pallet);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

    /**
     * @Route("/order-permit", name="app.wms.orderpermit")
     */
    public function orderPermitAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $this->get('monolog.logger.intergation_base')->debug(sprintf('Method %s', 'order-permit'));
        $this->get('monolog.logger.intergation_base')->debug(sprintf('Data %s', $request->getContent()));

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        $externalId = $data['ID'];
        $permitted = (bool)$data['Permitted'];

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->findOneByExternalId($externalId);
        if ($wmsOrder) {
            $wmsOrder->setPauseDelivery($permitted);
            if ($permitted) {
                $wmsOrder->setStatus(WmsOrder::STATUS_SUSPENDED);
            } else {
                $wmsOrder->setStatus(WmsOrder::STATUS_ACCEPTED);
            }
        }

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

    /**
     * @Route("/order-suspend", name="app.wms.ordersuspend")
     */
    public function orderSuspendAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $this->get('monolog.logger.intergation_base')->debug(sprintf('Method %s', 'order-suspend'));
        $this->get('monolog.logger.intergation_base')->debug(sprintf('Data %s', $request->getContent()));

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        $externalId = $data['ID'];
        $suspended = (bool)$data['Suspended'];

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->findOneByExternalId($externalId);
        if ($wmsOrder) {
            $wmsOrder->setStopSend($suspended);
            if ($suspended) {
                $wmsOrder->setStatus(WmsOrder::STATUS_SUSPENDED);
            } else {
                $wmsOrder->setStatus(WmsOrder::STATUS_ACCEPTED);
            }
        }

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

    /**
     * @Route("/order-status", name="app.wms.orderstatus")
    */
    public function orderStatusAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $this->get('monolog.logger.intergation_base')->debug(sprintf('Method %s', 'order-status'));
        $this->get('monolog.logger.intergation_base')->debug(sprintf('Data %s', $request->getContent()));

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        $externalId = $data['ID'];
        $status = $data['Status'];

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->findOneByExternalId($externalId);
        if ($wmsOrder) {
            $wmsOrder->setStatus($status);
            $entityManager->persist($wmsOrder);

            // Отправляем обновления в страховую
            $order = $entityManager->getRepository(Order::class)->find($externalId);
            if ($order && $status == WmsOrder::STATUS_ACCEPTED) {
                $insuredQueue = new InsuredQueue();
                $insuredQueue->setOrder($order);
                $entityManager->persist($insuredQueue);
            }

			$entityManager->flush();
        }




        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

        /**
     * @Route("/batch-status", name="app.wms.batchstatus")
    */
    public function batchStatusAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $wmsBatchStatusUpdateService = $this->get('app.wms.batch_status_update');

        $this->get('monolog.logger.intergation_base')->debug(sprintf('Method %s', 'order-status'));
        $this->get('monolog.logger.intergation_base')->debug(sprintf('Data %s', $request->getContent()));

        $entityManager = $this->getDoctrine()->getManager();

        //{"ID":"664","Status":"preparing","Time":"2018-12-10 14:44:35.818246+03"}

        /** @var SecurityService $securityService */
        //$securityService = $this->get('app.wms.security');
//
        //if (!$securityService->isAuth()) {
        //    return new JsonResponse(['status' => false], 400);
        //}

        //$stock = $securityService->getStock();

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);

        $externalId = $data['ID'];
        $status = $data['Status'];

        /** @var WmsBatch $wmsBatch */
        $wmsBatch = $wmsBatchRepository->findOneByExternalId($externalId);
        if ($wmsBatch) {
            $wmsBatch->setStatus($status);

            // После отмены партии, нужно обнулить ссылку у паллет
            if ($status === WmsBatch::STATUS_REJECTED) {
                foreach ($wmsBatch->getPallets() as $pallet) {
                    $pallet->setBatch(NULL);
                }
            }

            $wmsBatchStatusUpdateService->updateStatus($wmsBatch, $status);

            $entityManager->persist($wmsBatch);
			$entityManager->flush();
        }

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }
}
