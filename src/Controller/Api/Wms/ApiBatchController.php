<?php

namespace App\Controller\Api\Wms;

use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\OrderPackage;
use App\Entity\Organization\Organization;
use App\Entity\Package;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Entity\Base;
use App\Entity\StringEntity;
use App\Entity\Wms\AbstractWmsBatch;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Repository\FileGroupRepository;
use App\Repository\OrderRepository;
use App\Repository\BatchRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\PalletRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Repository\Wms\WmsPalletRepository;
use App\Service\Batch\SlaDateService;
use App\Service\CustomerCalcService;
use App\Service\FileUploadService;
use App\Service\IntegrationBaseService;
use App\Service\Wms\Order\DeliveredAvailableService;
use App\Service\Wms\SecurityService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsPalletUpdateService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/wms/wmsbatch")
 **/
class ApiBatchController extends AbstractApiWmsController
{


    /**
     * @var SlaDateService
     */
    protected $slaDateService;

    public function __construct(SlaDateService $slaDateService)
    {
        $this->slaDateService = $slaDateService;
    }


    /**
     * @Route("/config", name="app.api_batch_wmsorder.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var FileGroupRepository $fileGroupRepository */
        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        $params['fileGroups'] = $fileGroupRepository->getDefaultWmsBatch();

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }

    /**
     * @Route("/batch/{orderId}", name="app.api_wmsbatch.batch")
     */
    public function batchAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var DeliveredAvailableService $orderDeliveredAvailableService */
        $orderDeliveredAvailableService = $this->get('app.wmsorder.delivered_available');

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(AbstractWmsBatch::class);
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsBatch $batch */
        $batch = $wmsBatchRepository->find($orderId);

        if (!$batch) {
            return $this->errorAnswer('not found batch');
        }

        $result['batch'] = $batch->jsonSerialize();
        $result['batch']['stock'] = $stock->jsonSerialize();

        // нельзя отгружать пока есть паллеты
        if ($batch instanceof WmsBatchMover) {
            $result['batch']['load_available'] = true;
            if ($batch->getType() == Shipping::TYPE_DELIVER && $batch->getShipping() && count($batch->getShipping()->getWmsOrder())) {
                $wmsOrder = $batch->getShipping()->getWmsOrder()[0];
                $result['batch']['load_available'] = !$orderDeliveredAvailableService->hasPallets($wmsOrder);
            }
        }

        if ($batch->getDirection() && $batch->getStockTo()) {
            if ($batch->getReceptionAt()) {
                $result['batch']['slaDate'] = $this->slaDateService->get($batch->getDirection(), $batch->getStockTo(), $batch->getReceptionAt());
            }
        }

        foreach ($result['batch']['files'] as &$file) {
            $provider = $this->get($file->getMedia()->getProviderName());
            $format = $provider->getFormatName($file->getMedia(), 'reference');

            /** @var File $file */
            $file->setPath($request->getUriForPath($provider->generatePublicUrl($file->getMedia(), $format)));
        }

        return $this->answer($result['batch']);
    }

    /**
     * @Route("/orders", name="app.api_batch_wmsorder.orders")
     */
    public function ordersAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 10);
        $filter = $request->request->get('filter', []);
        $sort = $request->request->get('sort', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var AbstractWmsBatch $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(AbstractWmsBatch::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (isset($filter['field']) && $filter['field']) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $sorts = [];
        if (isset($sort['by'])) {
            $sorts = [$sort['by'] => $sort['direction']];
        } else {
            $sorts = ['id' => 'desc'];
        }

        $orders = $wmsBatchRepository->getByStock($stock, $page, $limit, $sorts, $orderFilter);



        $result = ['orders' => []];
        foreach ($orders as $order) {
            $orderJsonData = $order->jsonSerialize();

            if ($order->getCarrier()) {
                $organization = $organizationRepository->getCarrierByUser($order->getCarrier());

                if ($organization) {
                    $orderJsonData['organization'] = $organization;
                    $orderJsonData['chooseDrivers'] = $organization->getVerifiedDrivers();
                }
            }

            $orderJsonData['stock'] = $stock->jsonSerialize();
            $result['orders'][] = $orderJsonData;
        }

        /** @var FileGroup $fileGroup */
        foreach ($result['orders'] as &$item) {
            foreach ($item['files'] as &$file) {
                $provider = $this->get($file->getMedia()->getProviderName());
                $format = $provider->getFormatName($file->getMedia(), 'reference');

                /** @var File $file */
                $file->setPath($request->getUriForPath($provider->generatePublicUrl($file->getMedia(), $format)));
            }
            if (!$item['files']) {
                $item['files'] = [];
            }
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($orders->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @Route("/batch-save/{orderId}", name="app.api_wmsorder.batch_save")
     */
    public function saveAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        $data = $request->request->get('wmsBatch');
        $loadPalletIds = $data['palletIds'];

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);
        /** @var WmsBatch $wmsBatch */
        $wmsBatch = $wmsBatchRepository->find($orderId);

        $wmsBatch->setPallets(new ArrayCollection());

        $unpalletedIds = [];
        /** @var WmsPallet $wmsPallet */
        foreach ($loadPalletIds as $loadPalletId) {
            $wmsPallet = $wmsPalletRepository->find($loadPalletId);
            //if (!in_array($wmsPallet->getId(), $loadPalletIds)) {
            //    $unpalletedIds[] = $wmsPallet->getId();
            //}
            $wmsBatch->addPalletItem($wmsPallet);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-load/{orderId}", name="app.api_wmsorder.batch_load")
     */
    public function loadAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);

        /** @var WmsBatch $wmsBatch */
        $wmsBatch = $wmsBatchRepository->find($orderId);

        $wmsBatch->setStatus(WmsBatch::STATUS_SHIPPING);
        $wmsBatch->setReceptionAt(new \DateTime());

        if (!$this->wmsBatchUpdateService->batchLoad($wmsBatch)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-rejected/{orderId}", name="app.api_wmsbatch.batch_rejected")
     */
    public function rejectedAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);
        $batch = $wmsBatchRepository->find($orderId);

        $batch->setStatus(WmsOrder::STATUS_REJECTED);
        $batch->setRemove(true);

        if (!$this->wmsBatchUpdateService->batchRejected($batch)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $entityManager->persist($batch);
        $entityManager->flush();
        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-arrived/{orderId}", name="app.api_wmsorder.batch_arrived")
     */
    public function arrivedAction(Request $request, $orderId)
    {
        $data = $request->request->get('batch');

        $entityManager = $this->getDoctrine()->getManager();

        $this->checkAuth();

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');

        /** @var WmsBatchUpdateService $wmsBatchUpdateService */
        $wmsBatchUpdateService = $this->get('app.wmsbatch.update');

        if (!$this->securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $integrationBaseService->authByStock($this->securityService->getBaseStock());

        $entityManager->getConnection()->beginTransaction();

        /** @var WmsBatchRepository $wmsBatchRepository */
        $wmsBatchRepository = $entityManager->getRepository(WmsBatch::class);
        /** @var WmsBatch $wmsBatch */
        $wmsBatch = $wmsBatchRepository->find($orderId);

        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);
        $wmsPackageRepository = $entityManager->getRepository(WmsPackage::class);

        if (isset($data['pallet'])) {
            foreach ($data['pallet'] as $palletData) {
                /** @var WmsPallet $wmsPallet */
                $wmsPallet = $wmsPalletRepository->find($palletData['id']);

                $wmsPallet->setDamaged($palletData['damaged']);
                $wmsPallet->setArriveWeight($palletData['arriveWeight']);

                if (isset($palletData['packages'])) {
                    foreach ($palletData['packages'] as $packageData) {
                        /** @var WmsPallet $wmsPallet */
                        $wmsPackage = $wmsPackageRepository->find($packageData['id']);
                        $wmsPackage->setArriveDamaged($packageData['arriveDamaged']);

                        $entityManager->persist($wmsPackage);
                    }
                }

                $entityManager->persist($wmsPallet);
            }
        }

        if (isset($data['arrived']) && $data['arrived'] == true) {
            if (!$wmsBatchUpdateService->batchArrived($wmsBatch)) {
                $entityManager->getConnection()->rollback();
                return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
            }

            $wmsBatch->setStatus(WmsBatch::STATUS_ARRIVED);
            $wmsBatch->setIssueAt(new \DateTime());
        }

        ///** @var WmsOrderRepository $wmsOrderRepository */
        //$wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
//
        ///** @var WmsPallet $pallet */
        //foreach ($wmsBatch->getPallets() as $pallet) {
        //    /** @var WmsPackage $package */
        //    foreach ($pallet->getPackages() as $package) {
        //        /** @var WmsOrder $wmsOrder */
        //        $wmsOrder = $wmsOrderRepository->getByPackage($package);
        //        $wmsOrder->setStatus(WmsOrder::STATUS_WAITING)
        //    }
        //}

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-mover-arrived-old/{wmsBatch}", name="app.api_wmsorder.batch_mover_arrived-old")
     */
    public function moverArrivedAction(Request $request, WmsBatchMover $wmsBatch)
    {
        $data = $request->request->get('batch');

        $entityManager = $this->getDoctrine()->getManager();

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $integrationBaseService->authByStock($securityService->getBaseStock());

        $entityManager->getConnection()->beginTransaction();

        $orderPackageRepository = $entityManager->getRepository(OrderPackage::class);
        $packageRepository = $entityManager->getRepository(Package::class);

        $wmsPackageRepository = $entityManager->getRepository(WmsPackage::class);

        if (isset($data['package'])) {
            foreach ($data['package'] as $packageData) {

                /** @var WmsPackage $wmsPackage */
                $wmsPackage = $wmsPackageRepository->find($packageData['id']);
                $wmsPackage->setWeight((float)$packageData['weight']);
                $wmsPackage->setDamaged($packageData['damaged']);
                $entityManager->persist($wmsPackage);
            }
        }

        if (isset($data['arrived']) && $data['arrived'] == true) {
            $wmsBatch->setStatus(WmsBatch::STATUS_ARRIVED);
            $wmsBatch->setIssueAt(new \DateTime());
            $wmsBatch->getShipping()->setDone(true);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/batch-upload-file/{wmsBatch}", name="app.api-wmsbatch.batch-upload-file")
     */
    public function uploadFileAction(Request $request, AbstractWmsBatch $wmsBatch)
    {
        $groupCode = $request->request->get('groupCode');

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        /** @var FileUploadService $fileUploadService */
        $fileUploadService = $this->get('app.file_upload');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $entityManager->getConnection()->beginTransaction();

        if (!$wmsBatch) {
            return new JsonResponse(['status' => false], 400);
        }

        $file = $wmsBatch->getFileByGroup($groupCode);

        if ($file) {
            $file = $fileUploadService->updateFile($file, $request->files->get('file'));
        } else {
            $file = $fileUploadService->createFile('', $groupCode, $request->files->get('file'), sprintf('%s%s', $groupCode, $wmsBatch->getExternalId()));
            $wmsBatch->addFile($file);
        }

        $entityManager->persist($wmsBatch);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

}
