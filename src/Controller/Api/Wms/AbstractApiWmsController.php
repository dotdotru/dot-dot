<?php

namespace App\Controller\Api\Wms;

use App\Controller\Api\BaseApiController;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\Wms\WmsOrderMapperService;
use App\Service\Mapper\Wms\WmsPalletMapperService;
use App\Service\Wms\SecurityService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsOrderUpdateService;
use App\Service\Wms\WmsPalletUpdateService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Container\ContainerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;


abstract class AbstractApiWmsController extends BaseApiController
{
    /** @var SecurityService $securityService */
    protected $securityService;

    /** @var IntegrationBaseService $integrationBaseService */
    protected $integrationBaseService;

    /** @var WmsOrderUpdateService $wmsOrderMapperService  */
    protected $wmsOrderUpdateService;

    /** @var WmsPalletUpdateService $wmsPalletUpdateService  */
    protected $wmsPalletUpdateService;

    /** @var WmsBatchUpdateService $wmsBatchUpdateService  */
    protected $wmsBatchUpdateService;

    public function checkAuth()
    {
        /** @var SecurityService $securityService */
        $this->securityService = $this->get('app.wms.security');

        /** @var IntegrationBaseService $integrationBaseService */
        $this->integrationBaseService = $this->get('app.integration_base');

        /** @var WmsOrderMapperService $wmsOrderMapperService */
        $this->wmsOrderUpdateService = $this->get('app.wmsorder.update');

        /** @var WmsPalletUpdateService $wmsPalletUpdateService */
        $this->wmsPalletUpdateService = $this->get('app.wmspallet.update');

        /** @var WmsBatchUpdateService $wmsBatchUpdateService */
        $this->wmsBatchUpdateService = $this->get('app.wmsbatch.update');

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $this->integrationBaseService->authByStock($securityService->getBaseStock());
        $this->wmsOrderUpdateService->setIntegrationBaseService($this->integrationBaseService);

        return true;
    }

    public function getBaseStock()
    {
        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $this->get('app.integration_base');

        /** @var WmsOrderMapperService $wmsOrderMapperService */
        $wmsOrderMapperService = $this->get('app.wmsorder.mapper');

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $integrationBaseService->authByStock($securityService->getBaseStock());
    }

    public function getStock()
    {

    }
}
