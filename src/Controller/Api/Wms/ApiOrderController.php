<?php

namespace App\Controller\Api\Wms;

use App\Entity\Direction;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\InsuredQueue;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\PackageType;
use App\Form\Package\PackageType as PackageForm;
use App\Entity\Packing;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Entity\Base;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Form\PersonType;
use App\Form\Shipping\ShippingType;
use App\Form\Wms\OrderSaveType;
use App\Repository\DirectionRepository;
use App\Repository\FileGroupRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\CalculateService;
use App\Service\CustomerCalcService;
use App\Service\FileUploadService;
use App\Service\Integration\CallBaseService;
use App\Service\Order\RecalculateOrderPriceService;
use App\Service\OrderUpdateService;
use App\Service\PromocodeService;
use App\Service\Shipping\ShippingChangedService;
use App\Service\Shipping\ShippingClearService;
use App\Service\Shipping\ShippingGeoService;
use App\Service\Shipping\ShippingNotificationService;
use App\Service\Shipping\ShippingPriceService;
use App\Service\Shipping\ShippingSaveService;
use App\Service\Stock\StockListForWorker;
use App\Service\Wms\Order\DeliveredAvailableService;
use App\Service\Wms\SecurityService;
use App\Service\Wms\WmsOrderUpdateService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Vsavritsky\SettingsBundle\Entity\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/wms/wmsorder")
 **/
class ApiOrderController extends AbstractApiWmsController
{

    /**
     * @var ShippingChangedService
     */
    protected $shippingChangedService;

    /**
     * @var ShippingSaveService
     */
    protected $shippingSaveService;

    /**
     * @var ShippingGeoService
     */
    protected $shippingGeoService;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ShippingNotificationService
     */
    protected $shippingNotificationService;

    /**
     * @var
     */
    protected $shippingClearService;

    /**
     * @var ShippingPriceService
     */
    protected $shippingPriceService;


    public function __construct(
        EntityManagerInterface $em,
        ShippingChangedService $shippingChangedService,
        ShippingSaveService $shippingSaveService,
        ShippingClearService $shippingClearService,
        ShippingPriceService $shippingPriceService,
        ShippingGeoService $shippingGeoService,
        ShippingNotificationService $shippingNotificationService
    )
    {
        $this->em = $em;
        $this->shippingChangedService = $shippingChangedService;
        $this->shippingSaveService = $shippingSaveService;
        $this->shippingGeoService = $shippingGeoService;
        $this->shippingNotificationService = $shippingNotificationService;
        $this->shippingClearService = $shippingClearService;
        $this->shippingPriceService = $shippingPriceService;
    }



    /**
     * @Route("/orders", name="app.api_wmsorder.orders")
     */
    public function ordersAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $page = $request->request->getInt('page', 1);
        $limit = $request->request->getInt('perPage', 10);
        $status = $request->request->get('status', '');
        $filter = $request->request->get('filter', []);
        $sort = $request->request->get('sort', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var DeliveredAvailableService $orderDeliveredAvailableService */
        $orderDeliveredAvailableService = $this->get('app.wmsorder.delivered_available');

        /** @var WmsOrderRepository $wmsOrderepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
        $orderRepository = $entityManager->getRepository(Order::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['stockTo']) && !empty($filter['stockTo'])) {
                $orderFilter['stockTo'] = $filter['stockTo'];
            }
            if (isset($filter['stockFrom']) && $filter['stockFrom']) {
                $orderFilter['stockFrom'] = $filter['stockFrom'];
            }
            if (isset($filter['maxWeight']) && $filter['maxWeight']) {
                $orderFilter['weight'] = ['sign' => '<=', 'value' => $filter['maxWeight']];
            }
            if (isset($filter['palletId'])) {
                $orderFilter['palletId'] = $filter['palletId'];
            }
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (isset($filter['field']) && $filter['field']) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $sorts = [];
        if (isset($sort['by'])) {
			if ($sort['by'] == 'externalId') {
				$sort['by'] = 'id';
			}
            $sorts = [$sort['by'] => $sort['direction']];
        } else {
            $sorts = ['id' => 'desc'];
        }

        $orders = $wmsOrderRepository->getByStock($stock, $page, $limit, $sorts, $orderFilter);

        $fileGroupPolis = $fileGroupRepository->findOneBySlug('polis');

        $result = ['orders' => []];
        /** @var WmsOrder $wmsOrder */
        foreach ($orders as $wmsOrder) {
            $orderJsonData = $wmsOrder->jsonSerialize();
            if ($wmsOrder->getCustomer()) {
                $organization = $organizationRepository->getByUser($wmsOrder->getCustomer());
                if ($organization) {
                    $orderJsonData['organization'] = $organization;
                }
            }

            $orderJsonData['stock'] = $stock->jsonSerialize();
            $item = $orderJsonData;

            foreach ($item['files'] as &$file) {
                if ($file->getMedia()) {
					$provider = $this->get($file->getMedia()->getProviderName());
					$format = $provider->getFormatName($file->getMedia(), 'reference');

					/** @var File $file */
					$file->setPath($request->getUriForPath($provider->generatePublicUrl($file->getMedia(), $format)));
				}
			}

            if ($wmsOrder->getOrder() && $wmsOrder->getOrder()->getInsuredPdf()) {
                $fileItem = new File();
                $fileItem->setPath('/personal/insured-pdf/'.$item['externalId']);
                $fileItem->setGroup($fileGroupPolis);
                $item['files']['insured'] = $fileItem;
            }

            if ($wmsOrder->getOrder() && $wmsOrder->getOrder()->getPromocode()) {
                $item['promocode'] = $wmsOrder->getOrder()->getPromocode()->getTitle();
            }

            // Возможна ли выдача заказа
            $orderJsonData['isDeliveredAvailable'] = $orderDeliveredAvailableService->isAvailable($wmsOrder);

            $result['orders'][] = $item;
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($orders->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @Route("/config", name="app.api_wmsorder.config")
     */
    public function configAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->checkAuth();

        /** @var FileGroupRepository $fileGroupRepository */
        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);



        $stockFrom = $this->securityService->getStock();

        /** @var StockListForWorker $stockListService */
        $stockListService = $this->get('app.service.stock.for_worker');

        /**
         * @var CustomerCalcService $customerCalcService;
         */
        $service = $this->get('app.customer_calc');

        $params['directions'] = $service->getDirections();
        $params['packageTypes'] = $service->getPackageTypes();
        $params['packings'] = $service->getPackings();
        $params['stocks'] = $stockListService->getStocks($stockFrom);
        $params['currentStock'] = $stockFrom;
        $params['fileGroups'] = $fileGroupRepository->getDefaultWmsOrder();
        $params['organizationTypes'] = $service->getOrganizationTypes();

        $settings = $service->getSettings();

        return new JsonResponse(['status' => true, 'data' => ['settings' => $settings, 'params' => $params]], 200);
    }

    /**
     * @Route("/order-rejected/{orderId}", name="app.api_wmsorder.order_rejected")
     */
    public function rejectedAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
        $order = $wmsOrderRepository->find($orderId);

        $order->setStatus(WmsOrder::STATUS_REJECTED);
        $order->setRemove(true);

        if (!$this->wmsOrderUpdateService->orderRejected($order)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $entityManager->persist($order);
        $entityManager->flush();
        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/order-save/{orderId}", name="app.api_wmsorder.order_save")
     */
    public function saveAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        $data = $request->request->get('wmsOrder');

        if (!$data['packages']) {
            return new JsonResponse(['status' => false, 'error' => 'Не указаны товарные позиции'], 400);
        }

        /** @var PromocodeService promocodeService */
        $this->promocodeService = $this->container->get('app.promocode');

        /** @var Settings settings */
        $this->settings = $this->container->get('settings');

        /** @var CalculateService $calculateService */
        $this->calculateService = $this->container->get('app.calculate');

        /** @var CallBaseService $callerBase */
        $callerBase = $this->get('app.integration.caller');

        /** @var RecalculateOrderPriceService $recalcaulateOrderService */
        $recalcaulateOrderService = $this->get('app.service.order.recalculate');

        $stockFrom = $this->securityService->getStock();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
        $stockRepository = $entityManager->getRepository(Stock::class);
        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->find($orderId);

        if ($order->getStatus() == WmsOrder::STATUS_ACCEPTED) {
            return new JsonResponse(['status' => false, 'error' => 'Заказ уже принят'], 400);
        }

        foreach ($order->getPackages() as $package) {
            $entityManager->remove($package);
        }
        $entityManager->flush();

        $order->setByCargo(false);
        $order->setOrderPackages(new ArrayCollection());
        $order->setPackages(new ArrayCollection());
        $entityManager->persist($order);
        $entityManager->flush();

        foreach ($data['packages'] as $packageData) {
            if ($packageData['weight'] <= 0) continue;
            $packageData['count'] = (int)$packageData['count'];
            if (!$packageData['count']) {
                $packageData['count'] = 1;
            }
            for($i=1;$i<=$packageData['count'];$i++) {
                $package = new WmsPackage();

                $package->setOrder($order);

                $packing = null;
                if (isset($packageData['packing']) && $packageData['packing']) {
                    $packing = $this->getDoctrine()->getRepository(Packing::class)->findOneById($packageData['packing']);
                }

                $packageType = null;
                if (!empty($packageData['packageType'])) {
                    $packageType = $this->getDoctrine()->getRepository(PackageType::class)->findOneById($packageData['packageType']);
                }
                if (!empty($packageData['packageTypeAnother'])) {
                    $package->setPackageTypeAnother($packageData['packageTypeAnother']);
                }

                $package->setPacking($packing);
                if (isset($packageData['packingPrice']) && $packageData['packingPrice']) {
                    $package->setPackingPrice($packageData['packingPrice']);
                }
                $package->setWidth($packageData['width']);
                $package->setDamaged($packageData['damaged']);
                $package->setHeight($packageData['height']);
                $package->setDepth($packageData['depth']);
                $package->setWeight($packageData['weight']);
                $package->setPackageType($packageType);
                $package->setCount(1);
                $package->setPalletId($packageData['palletId']);

                $order->addPackage($package);
            }
        }

        if (isset($data['receiver'])) {
            $receiverData = $data['receiver'];
            if (isset($receiverData['id']) && !empty($receiverData['id'])) {
                $receiver = $entityManager->getRepository(Receiver::class)->find($receiverData['id']);
                $receiverForm = $this->createForm(PersonType::class, $receiver);
            } else {
                $receiverForm = $this->createForm(PersonType::class);
            }

            $receiverForm->submit($receiverData);
            if (!$receiverForm->isValid()) {
                return new JsonResponse(['error' => 'error reciver'], 400);
            }

            /** @var Receiver $receiver */
            $receiver = $receiverForm->getData();

            if ($receiver) {
                $order->setReceiver($receiver);
            }
        }

        if (isset($data['declaredPrice'])) {
            $order->setDeclaredPrice($data['declaredPrice']);
        }

        $settings = $this->settings->group('customer_calc');

        if ($order->getPackages()->count()) {
            $recalcaulateOrderService->recalculate($order);
        }

        $entityManager->persist($order);
        $entityManager->flush();

        $wmsPalletRepository = $this->getDoctrine()->getRepository(WmsPallet::class);

        $baseDirectionRepository = $entityManager->getRepository(Base\Direction::class);

        /** @var Base\Direction $baseDirection */
        $baseDirection = $baseDirectionRepository->findOneByExternalId($order->getDirection()->getId());

        $errorPalletted = false;
        $errorPallettedIds = [];

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            /** @var wmsPallet $wmsPallet */
            $wmsPallet = $wmsPalletRepository->findOneBy(['externalId' => $package->getPalletId()]);

            if ($wmsPallet->getWeightPackages() + $package->getWeight() > WmsPallet::MAX_WEIGHT_PACKAGES) {
                $errorPalletted = true;
                $errorPallettedIds[$package->getPalletId()] = $package->getPalletId();
            }

            $wmsPallet->setDirection($order->getDirection());
            $wmsPallet->addPackage($package);
            $wmsPallet->addRealPackage($package);

            $entityManager->persist($wmsPallet);
            $entityManager->flush();
        }

        $entityManager->persist($order);
        $entityManager->flush();

        foreach ($order->getPackages() as $package) {
			$package->setExternalId($package->getTmpExternalId());
			$entityManager->persist($package);
		}
        $entityManager->flush();

        if ($errorPalletted) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'data' => ['errorPallettedIds' => array_values($errorPallettedIds)]], 200);
        }

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/order-accepted/{orderId}", name="app.api_wmsorder.order_accepted")
     */
    public function acceptedAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->find($orderId);

        if (!$this->wmsOrderUpdateService->orderAccepted($order)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $order->setReceptionAt(new \DateTime());
        $order->setStatus(WmsOrder::STATUS_ACCEPTED);

        /* Установим время достаки при акцепте */
        $wmsPalletRepository = $this->getDoctrine()->getRepository(WmsPallet::class);

        $baseDirectionRepository = $entityManager->getRepository(Base\Direction::class);

        /** @var Base\Direction $baseDirection */
        $baseDirection = $baseDirectionRepository->findOneByExternalId($order->getDirection()->getExternalId());

        $deliverToAt = new \DateTime();
        list($h, $i, $s) = explode(':', $baseDirection->getCustomerDeliveryTime());
        $deliverToAt->add(new \DateInterval('PT'.$h.'H'));

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            $package->setDeliveryToAt($deliverToAt);
            $package->setStatus(WmsPackage::STATUS_ACCEPTED);

            $entityManager->persist($package);
        }

        $entityManager->persist($order);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'd' => $package->getDeliveryToAt()->format('d.m.Y')], 200);
    }


    /**
     * @Route("/order-new-save/{wmsOrder}", name="app.api_wmsorder.order_new_save")
     *
     * @param Request $request
     * @param WmsOrder $wmsOrder
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function newSaveAction(Request $request, WmsOrder $wmsOrder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();


        /** @var DirectionRepository $directionRepository */
        $directionRepository = $entityManager->getRepository(Direction::class);

        /** @var RecalculateOrderPriceService $recalcaulateOrderService */
        $recalcaulateOrderService = $this->get('app.service.order.recalculate');

        $this->checkAuth();

        $data = $request->request->get('wmsOrder');
        $data['stockFrom'] = $this->securityService->getStock()->getId();

        $form = $this->createForm(OrderSaveType::class, $wmsOrder);
        if ($form->submit($data) && !$form->isValid()) {
            return new JsonResponse(['status' => false, 'error' => 'Указаны неправильные данные', 'errors' => $form->getErrors()], 400);
        }

        if ($wmsOrder->getStockFrom()) {
            $direction = $directionRepository->findOneBy(['cityFrom' => $wmsOrder->getStockFrom()->getCity(), 'cityTo' => $wmsOrder->getStockTo()->getCity()]);
            $wmsOrder->setDirection($direction);
        }

        if (!isset($data['packages'])) {
            return new JsonResponse(['status' => false, 'error' => 'Не указаны товарные позиции'], 400);
        }

        $wmsOrder->setByCargo(false);

        foreach ($wmsOrder->getPackages() as $package) {
            $entityManager->remove($package);
        }

        $wmsOrder->setOrderPackages(new ArrayCollection());
        $wmsOrder->setPackages(new ArrayCollection());

        foreach ($data['packages'] as $packageData) {
            $count = (int)$packageData['count'] ? (int)$packageData['count'] : 1;
            $packageData['count'] = 1;

            for($i = 1; $i <= $count; $i++) {
                $package = new WmsPackage();
                $form = $this->createForm(PackageForm::class, $package);
                if ($form->submit($packageData) && $form->isValid()) {
                    $package->setOrder($wmsOrder);
                    $wmsOrder->addPackage($package);

                    if (!empty($packageData['packageTypeAnother'])) {
                        $package->setPackageTypeAnother($packageData['packageTypeAnother']);
                    }

                    if (isset($packageData['packingPrice']) && $packageData['packingPrice']) {
                        $package->setPackingPrice($packageData['packingPrice']);
                    }

                }
            }
        }

        $recalcaulateOrderService->recalculate($wmsOrder);

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        $wmsPalletRepository = $this->getDoctrine()->getRepository(WmsPallet::class);

        $errorPalletted = false;
        $errorPallettedIds = [];

        /** @var WmsPackage $package */
        foreach ($wmsOrder->getPackages() as $package) {
            /** @var wmsPallet $wmsPallet */
            $wmsPallet = $wmsPalletRepository->findOneBy(['externalId' => $package->getPalletId()]);

            if ($wmsPallet->getWeightPackages() + $package->getWeight() > WmsPallet::MAX_WEIGHT_PACKAGES) {
                $errorPalletted = true;
                $errorPallettedIds[$package->getPalletId()] = $package->getPalletId();
            }

            $wmsPallet->setDirection($wmsOrder->getDirection());
            $wmsPallet->addPackage($package);
            $wmsPallet->addRealPackage($package);

            $entityManager->persist($wmsPallet);
            $entityManager->flush();
        }

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        if ($errorPalletted) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'data' => ['errorPallettedIds' => array_values($errorPallettedIds)]], 200);
        }

        $shippingType = 'deliver';
        if ($wmsOrder->getExternalId() && !empty($data[$shippingType])) {
            $shippingData = $data[$shippingType];

            if ($shippingData) {
                $newShipping = new Shipping();
                $newShipping->setType($shippingType);
                $form = $this->createForm(ShippingType::class, $newShipping);

                if ($form->submit($shippingData) && $form->isValid()) {
                    $newShipping = $form->getData();

                    if ($this->shippingChangedService->isChanged($newShipping, $wmsOrder->getShipping($shippingType))) {
                        $this->shippingSaveService->save($wmsOrder, $newShipping, $wmsOrder->getShipping($shippingType));

                        $this->em->persist($newShipping);
                        $wmsOrder->addShipping($newShipping);
                    }
                } else {
                    return new JsonResponse(['error' => 'Неудалось сохранить данные', 'errors' => $form->getErrors()], 400);
                }
            }
        }

        $entityManager->flush();
        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, ['data' => ['order' => $wmsOrder->getId()]]], 200);
    }

    /**
     * @Route("/order-new-accepted/{orderId}", name="app.api_wmsorder.order_new_accepted")
     */
    public function newAcceptedAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->find($orderId);

        // Используем один метод для офлайн и обычного заказа @todo удалить методы выше
        if ($order->getStatus() != WmsOrder::STATUS_WAITING) {
            if (!$this->wmsOrderUpdateService->newOrderAccepted($order)) {
                $entityManager->getConnection()->rollback();
                return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
            }
        } else {
            if (!$this->wmsOrderUpdateService->orderAccepted($order)) {
                $entityManager->getConnection()->rollback();
                return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
            }
        }

        $order->setReceptionAt(new \DateTime());
        $order->setStatus(WmsOrder::STATUS_ACCEPTED);

        $baseDirectionRepository = $entityManager->getRepository(Base\Direction::class);

        /** @var Base\Direction $baseDirection */
        $baseDirection = $baseDirectionRepository->findOneByExternalId($order->getDirection()->getExternalId());

        $deliverToAt = new \DateTime();
        list($h, $i, $s) = explode(':', $baseDirection->getCustomerDeliveryTime());
        $deliverToAt->add(new \DateInterval('PT'.$h.'H'));

        /** @var WmsPackage $package */
        foreach ($order->getPackages() as $package) {
            $package->setDeliveryToAt($deliverToAt);
            $package->setStatus(WmsPackage::STATUS_ACCEPTED);

            $entityManager->persist($package);
        }

        $order->setStatus(WmsOrder::STATUS_ACCEPTED);
        $order->setTmp(false);

        $entityManager->persist($order);
        $entityManager->flush();


        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'd' => $package->getDeliveryToAt()->format('d.m.Y')], 200);
    }

    /**
     * @Route("/order/{orderId}", name="app.api_wmsorder.order")
     */
    public function orderAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var DeliveredAvailableService $orderDeliveredAvailableService */
        $orderDeliveredAvailableService = $this->get('app.wmsorder.delivered_available');

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        $fileGroupRepository = $entityManager->getRepository(FileGroup::class);
        $fileGroupPolis = $fileGroupRepository->findOneBySlug('polis');

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->find($orderId);

        foreach ($wmsOrder->getFiles() as &$file) {
            if ($file->getMedia()) {
				$provider = $this->get($file->getMedia()->getProviderName());
				$format = $provider->getFormatName($file->getMedia(), 'reference');

				/** @var File $file */
				$file->setPath($request->getUriForPath($provider->generatePublicUrl($file->getMedia(), $format)));
			}
		}

        $orderJsonData = $wmsOrder->jsonSerialize();
        $orderJsonData['stock'] = $stock->jsonSerialize();

        // Возможна ли выдача заказа
        $orderJsonData['isDeliveredAvailable'] = $orderDeliveredAvailableService->isAvailable($wmsOrder);

        if ($wmsOrder->getOrder() && $wmsOrder->getOrder()->getInsuredPdf()) {
            $fileItem = new File();
            $fileItem->setPath('/personal/insured-pdf/' . $orderJsonData['externalId']);
            $fileItem->setGroup($fileGroupPolis);
            $orderJsonData['files']['insured'] = $fileItem;
        }


        if ($wmsOrder->getOrder() && $wmsOrder->getOrder()->getPromocode()) {
            $orderJsonData['promocode'] = $wmsOrder->getOrder()->getPromocode()->getTitle();
        }

        $personsJsonData = [];
        if ($wmsOrder->getUser()) {
            $persons = $entityManager->getRepository(Person::class)->findBy(['user' => $wmsOrder->getUser()]);
            /**
             * @var Person $person
             */
            foreach ($persons as $key => $person) {
                if ($person->getExternalId()) {
                    $personsJsonData[] = $person->jsonSerialize();
                }
            }
        }


        return $this->answer([
            'order' => $orderJsonData,
            'persons' => $personsJsonData
        ]);
    }

        /**
     * @Route("/order-save-delivered/{orderId}", name="app.api_wmsorder.order_save_delivered")
     */
    public function saveDeliveredAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $data = $request->request->get('order', []);

        $this->checkAuth();

        /** @var WmsOrderUpdateService $wmsOrderUpdateService */
        $wmsOrderUpdateService = $this->get('app.wmsorder.update');

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->find($orderId);

        if (isset($data['packages'])) {
            foreach ($order->getPackages() as &$package) {
				$package->setDamaged(false);
                foreach ($data['packages'] as $packageData) {
                    if ($package->getId() == $packageData['id']) {
                        $package->setDamaged($packageData['damaged']);
                    }
                }
                $entityManager->persist($package);
            }
        }

        $entityManager->persist($order);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/order-delivered/{orderId}", name="app.api_wmsorder.order_delivered")
     */
    public function deliveredAction(Request $request, $orderId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $data = $request->request->get('order', []);

        $this->checkAuth();

        /** @var WmsOrderUpdateService $wmsOrderUpdateService */
        $wmsOrderUpdateService = $this->get('app.wmsorder.update');

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsOrder $order */
        $order = $wmsOrderRepository->find($orderId);

        if (isset($data['packages'])) {
            foreach ($order->getPackages() as &$package) {
                foreach ($data['packages'] as $packageData) {
                    if ($package->getId() == $packageData['id']) {
                        $package->setDamaged($packageData['damaged']);
                    }
                }
                $entityManager->persist($package);
            }
        }

        $order->setIssueAt(new \DateTime());
        $order->setStatus(WmsOrder::STATUS_DELIVERED);

        if ($wmsOrderUpdateService->orderDelivered($order) == false) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $entityManager->persist($order);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }

    /**
     * @Route("/order-upload-file/{orderId}", name="app.api-wmsorder.order-upload-file")
     */
    public function uploadFileAction(Request $request, $orderId)
    {
        $groupCode = $request->request->get('groupCode');

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        /** @var FileUploadService $fileUploadService */
        $fileUploadService = $this->get('app.file_upload');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $entityManager->getConnection()->beginTransaction();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->findOneByExternalId($orderId);

        if (!$wmsOrder) {
            return new JsonResponse(['status' => false], 400);
        }

        $file = $wmsOrder->getFileByGroup($groupCode);

        if ($file) {
            $file = $fileUploadService->updateFile($file, $request->files->get('file'));
        } else {
            $file = $fileUploadService->createFile('', $groupCode, $request->files->get('file'), sprintf('%s%s', $groupCode, $orderId));
            $wmsOrder->addFile($file);
        }

        $entityManager->persist($wmsOrder);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true], 200);
    }
}
