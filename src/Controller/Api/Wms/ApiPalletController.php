<?php

namespace App\Controller\Api\Wms;

use App\Entity\Direction;
use App\Entity\File;
use App\Entity\FileGroup;
use App\Entity\Order;
use App\Entity\Stock;
use App\Entity\Base;
use App\Entity\Wms\WmsBatchPackage;
use App\Entity\Wms\WmsBatchPallet;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPackage;
use App\Entity\Wms\WmsPallet;
use App\Entity\Wms\WmsBatch;
use App\Repository\OrderRepository;
use App\Repository\PalletRepository;
use App\Repository\StockRepository;
use App\Repository\Wms\WmsBatchPalletRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Repository\Wms\WmsPalletRepository;
use App\Service\CustomerCalcService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\Wms\WmsPalletMapperService;
use App\Service\Wms\SecurityService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * @Route("/api/wms/wmspallet")
 *
 *
 **/
class ApiPalletController extends AbstractApiWmsController
{
    /**
     * @Route("/new-pallets", name="app.api_wmspallet.new_pallets")
     */
    public function newPalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $filter = $request->request->get('filter', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        $page = 1;
        $limit = 10000;

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (!empty($filter['field'])) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $pallets = $wmsPalletRepository->getByStock($stock, $page, $limit,['id' => 'desc'], $orderFilter);

        $result = ['pallets' => []];
        foreach ($pallets as $pallet) {
            $showPallet = false;
            /** @var WmsPackage $package */
            foreach ($pallet->getPackages() as $package) {
                if ($package->getOrder() && $package->getOrder()->getReceptionAt()) {
                    $showPallet = true;
                }
            }

            if ($pallet->getWeight() <= 0 && $pallet->getPackages()->count() > 0 && $showPallet) {
                $result['pallets'][] = $pallet->jsonSerialize();
            }
        }

        usort($result['pallets'], function($a, $b) {
            if (empty($a['packages']) && $b['packages']) {
                return 1;
            } elseif ($a['packages'] && empty($b['packages'])) {
                return -1;
            }

            if ($a['id'] == $b['id']) {
                return 0;
            }
            return ($a['palletId'] < $b['palletId']) ? -1 : 1;
        });

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($pallets->count() / $limit),
        ];

        return $this->answer($result);
    }

    /**
     * @Route("/send-pallets", name="app.api_wmspallet.send_pallets")
     */
    public function sendPalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $filter = $request->request->get('filter', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        $page = 1;
        $limit = 10000;

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (!empty($filter['field'])) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $pallets = $wmsPalletRepository->getByStock($stock, $page, $limit,['id' => 'desc'],  $orderFilter);

        $result = ['pallets' => []];
        foreach ($pallets as $pallet) {
            if ($pallet->getWeight() > 0 && $pallet->isPalleted() && $pallet->getBatch() && $pallet->getBatch()->getStatus() == WmsBatch::STATUS_READY) {
                $result['pallets'][] = $pallet->jsonSerialize();
            }
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($pallets->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @Route("/wait-arrival-pallets", name="app.api_wmspallet.wait_arrival_pallets")
     */
    public function waitArrivalPalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $filter = $request->request->get('filter', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        $page = 1;
        $limit = 10000;

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (!empty($filter['field'])) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $pallets = $wmsPalletRepository->getByStockTo($stock, $page, $limit, ['id' => 'desc'], $orderFilter);
        
        $result = ['pallets' => []];
        foreach ($pallets as $pallet) {
            if ($pallet->getWeight() > 0 && $pallet->isPalleted() && $pallet->getBatch() && $pallet->getBatch()->getStatus() == WmsBatch::STATUS_SHIPPING && $pallet->getStockTo()->getId() == $stock->getId()) {
                $result['pallets'][] = $pallet->jsonSerialize();
            }
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($pallets->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }

    /**
     * @Route("/issue-pallets", name="app.api_wmspallet.issue_pallets")
     */
    public function issuePalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->checkAuth();

        $filter = $request->request->get('filter', []);

        $stock = $this->securityService->getStock();

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        $page = 1;
        $limit = 10000;

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (!empty($filter['field'])) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        $pallets = $wmsPalletRepository->getByStockTo($stock, $page, $limit, ['id' => 'desc'], $orderFilter);

        $result = ['pallets' => []];
        foreach ($pallets as $pallet) {
            if ($pallet->getWeight() > 0 && $pallet->isPalleted() && $pallet->getBatch() && ($pallet->getBatch()->isArrived() || $pallet->getBatch()->isPayed())) {
                $result['pallets'][] = $pallet->jsonSerialize();
            }
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($pallets->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }
    
     /**
     * @Route("/waiting-pallets", name="app.api_wmspallet.waiting_pallets")
     */
    public function waitingPalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $filter = $request->request->get('filter', []);

        $this->checkAuth();

        $stock = $this->securityService->getStock();

        $orderFilter = [];

        if ($filter) {
            if (isset($filter['search']) && $filter['search']) {
                $orderFilter['search'] = $filter['search'];
            }
            if (!empty($filter['field'])) {
                $orderFilter['field'] = $filter['field'];
            }
            if (isset($filter['status']) && $filter['status']) {
                $orderFilter['status'] = $filter['status'];
            }
        }

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        $page = 1;
        $limit = 10000;

        $pallets = $wmsPalletRepository->getByStock($stock, $page, $limit, ['id' => 'desc'], $orderFilter);

        $result = ['pallets' => []];
        foreach ($pallets as $pallet) {
            if ($pallet->getWeight() > 0 && $pallet->isPalleted() && (!$pallet->getBatch() || ($pallet->getBatch() && $pallet->getBatch()->getStatus() == WmsBatch::STATUS_REJECTED))) {
                $result['pallets'][] = $pallet->jsonSerialize();
            }
        }

        $result['pagination'] = [
            'page' => $page,
            'perPage' => $limit,
            'pages' => ceil($pallets->count() / $limit),
        ];

        return new JsonResponse(['status' => true, 'data' => $result], 200);
    }
    
    /**
     * @Route("/add-packages", name="app.api_wmspallet.add_packages")
     */
    public function addPackagesAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();
        $stock = $this->securityService->getStock();

        $orderData = $request->request->get('pallet');
        $externalId = $orderData['palletId'];
        $packageIds = isset($orderData['packages']) ? $orderData['packages'] : '';

        $this->checkAuth();

        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);
        $wmsPackageRepository = $entityManager->getRepository(WmsPackage::class);

        /** @var WmsPallet $wmsPallet */
        $wmsPallet = $wmsPalletRepository->findOneByExternalId($externalId);

        if (!$wmsPallet) {
            return new JsonResponse(['status' => false], 400);
        }

        $errorPalletted = false;
        $errorPallettedIds = [];

        $packages = $wmsPackageRepository->findBy(['id' => $packageIds]);
        foreach ($packages as $package) {
            if ($wmsPallet->getWeightPackages() + $package->getWeight() > WmsPallet::MAX_WEIGHT_PACKAGES) {
                $errorPalletted = true;
                $errorPallettedIds[$wmsPallet->getExternalId()] = $wmsPallet->getExternalId();
            }
            $wmsPallet->addPackage($package);
            $wmsPallet->addRealPackage($package);
        }

        if ($errorPalletted) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'data' => ['errorPallettedIds' => array_values($errorPallettedIds)]], 200);
        }

        /** @var WmsPackage $package */
        foreach ($wmsPallet->getPackages() as $package) {
            $package->setPalletId($wmsPallet->getPalletId());
            $entityManager->persist($package);

            if ($wmsPallet->getDeliverToAt() > $package->getDeliveryToAt()) {
                $wmsPallet->setDeliverToAt($package->getDeliveryToAt());
            }
        }

        $wmsPallet->setPalleted(false);
        $entityManager->persist($wmsPallet);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'data' => ['id' => $wmsPallet->getId()]], 200);
    }

    /**
     * @Route("/palleted", name="app.api_wmspallet.palleted")
     */
    public function palletedAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $this->checkAuth();
        $stock = $this->securityService->getStock();

        $orderData = $request->request->get('pallet');
        $externalId = $orderData['palletId'];
        $weight = $orderData['weight'];
        $this->checkAuth();

        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        /** @var WmsPallet $wmsPallet */
        $wmsPallet = $wmsPalletRepository->find($externalId);

        if (!$wmsPallet) {
            return new JsonResponse(['status' => false], 400);
        }

        if ($weight < 30) {
            $weight = 30;
        }

        if ($weight > 1500) {
            $weight = 1500;
        }

        $wmsPallet->setWeight($weight);

        if (!$this->wmsPalletUpdateService->cargoPalletted($wmsPallet)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }
        
        /** @var WmsPackage $package */
        foreach ($wmsPallet->getPackages() as $package) {
            $package->setStatus(WmsPackage::STATUS_PREPARING);
            $entityManager->persist($package);
        }

        $wmsPallet->setPalleted(true);
        $entityManager->persist($wmsPallet);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'data' => ['id' => $wmsPallet->getId()]], 200);
    }

    /**
     * @Route("/unpalleted", name="app.api_wmspallet.unpalleted")
     */
    public function unpalletedAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();

        $data = $request->request->get('pallet');
        $id = $data['id'];

        $this->checkAuth();

        /** @var StockRepository $stockRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        /** @var WmsPallet $wmsPallet */
        $wmsPallet = $wmsPalletRepository->find($id);

        if (!$wmsPallet) {
            return new JsonResponse(['status' => false], 400);
        }

        if ($wmsPallet->isPalleted() && !$this->wmsPalletUpdateService->cargoUnPalletted($wmsPallet)) {
            $entityManager->getConnection()->rollback();
            return new JsonResponse(['status' => false, 'error' => 'Ошибка синхронизации'], 400);
        }

        $wmsPallet->setWeight(null);
        $wmsPallet->setPalleted(false);
        $wmsPallet->setDeliverToAt(null);

        /** @var WmsPackage $package */
        foreach ($wmsPallet->getPackages() as $package) {
            $package->setPalletId('');
            
            if (!$wmsPallet->getBatch()) {
                $package->setStatus(WmsPackage::STATUS_PREPARING);
            } else {
                $package->setStatus(WmsPackage::STATUS_INTRANSIT);
            }
            
            $entityManager->persist($package);
        }

        $wmsPallet->setPackages(new ArrayCollection());
        if (!$wmsPallet->getBatch()) {
			$wmsPallet->setRealPackages(new ArrayCollection());
		}
        $entityManager->persist($wmsPallet);
        $entityManager->flush();

        $entityManager->getConnection()->commit();

        return new JsonResponse(['status' => true, 'data' => ['id' => $wmsPallet->getId()]], 200);
    }


    /**
     * @Route("/create", name="app.api_wmspallet.create")
     */
    public function createAction(Request $request)
    {
        $data = $request->request->get('data');

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $stockToId = $data['stockTo'];

        if (!isset($data['stockFrom']) || empty($data['stockFrom'])) {
            $stockFromId = $securityService->getStock()->getId();
        } else {
            $stockFromId = $data['stockFrom'];
        }

        /** @var StockRepository $stockRepository */
        $stockRepository = $entityManager->getRepository(Stock::class);
        $stockTo = $stockRepository->find($stockToId);
        $stockFrom = $stockRepository->find($stockFromId);

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);
        $lastWmsPallet = $wmsPalletRepository->findOneBy(['stockFrom' => $stockFrom], ['palletId' => 'desc']);

        if ($lastWmsPallet) {
            $palletId = $lastWmsPallet->getPalletId() + 1;
        } else {
            $palletId = 1;
        }

        $wmsPallet = new WmsPallet();
        $wmsPallet->setPalletId($palletId);
        $wmsPallet->setStockTo($stockTo);
        $wmsPallet->setStockFrom($stockFrom);

        $entityManager->persist($wmsPallet);
        $entityManager->flush();

        return new JsonResponse(['status' => true, 'data' => ['palletId' => $wmsPallet->getExternalId()]], 200);
    }

    /**
     * @Route("/free_pallets", name="app.api_wmspallet.free_pallets")
     */
    public function freePalletsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = $request->request->get('data');

        /** @var StockRepository $stockRepository */
        $stockRepository = $entityManager->getRepository(Stock::class);

        /** @var WmsPalletRepository $wmsPalletRepository */
        $wmsPalletRepository = $entityManager->getRepository(WmsPallet::class);

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }
        
        if (!isset($data['stockFrom']) || !$data['stockFrom']) {
            $data['stockFrom'] = $securityService->getStock()->getId();
        }
        
        $stockTo = $stockRepository->find($data['stockTo']);
        $stockFrom = $stockRepository->find($data['stockFrom']);

        $pallets = $wmsPalletRepository->findBy(
            ['stockFrom' => $stockFrom, 'palleted' => false, 'stockTo' => $stockTo, 'weight' => null, 'batch' => null],
            ['palletId' => 'asc']
        );
        
        foreach($pallets as $keyPallet => $pallet) {
            $showPallet = false;
            
            if ($pallet->getPackages()->count() > 0) {
               /** @var WmsPackage $package */
                foreach ($pallet->getPackages() as $package) {
                    if ($package->getOrder() && $package->getOrder()->getReceptionAt()) {
                        $showPallet = true;
                    }
                }
               if (!$showPallet) {
                   unset($pallets[$keyPallet]);
               } 
            } 
        }

        usort($pallets, function($a, $b) {
            if ($a->getPackages()->count() == 0 && $b->getPackages()->count() > 0) {
                return 1;
            } elseif ($a->getPackages()->count() > 0 && $b->getPackages()->count() == 0) {
                return 0;
            }

            if ($a->getId() == $b->getId()) {
                return 0;
            }
            
            return ($a->getPalletId() < $b->getPalletId()) ? -1 : 1;
        });

        return new JsonResponse(['status' => true, 'data' => ['pallets' => $pallets]], 200);
    }


    /**
     * @Route("/config", name="app.api_wmspallet.config")
     */
    public function configAction(Request $request)
    {
        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        /**
         * @var CustomerCalcService $customerCalcService;
         */
        $service = $this->get('app.customer_calc');

        $params['directions'] = $service->getDirections();
        $params['packageTypes'] = $service->getPackageTypes();
        $params['packings'] = $service->getPackings();
        $params['stocks'] = $service->getStocks();
        $params['stocks'] = $service->getStocks();

        return new JsonResponse(['status' => true, 'data' => ['params' => $params]], 200);
    }
}
