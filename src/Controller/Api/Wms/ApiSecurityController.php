<?php

namespace App\Controller\Api\Wms;

use App\Entity\Authtoken;
use App\Entity\Base\Stock;
use App\Entity\Organization\Organization;
use App\Entity\Wms\WmsOrder;
use App\Repository\Organization\OrganizationRepository;
use App\Service\Wms\SecurityService;
use App\Application\Sonata\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/wms/security")
 **/
class ApiSecurityController extends Controller
{
    const WMS_AUTH_CODE = 'wms.auth.code';

    /**
     * @Route("/auth", name="app.api_security.auth")
     */
    public function authAction(Request $request)
    {
        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        $code = $request->request->get('code', '');

        if (!$securityService->login($code)) {
            return new JsonResponse(['status' => false], 400);
        }

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }

    /**
     * @Route("/auth_client", name="app.api_security.auth_client")
     */
    public function authClientAction(Request $request)
    {
        $wmsOrderRepository = $this->getDoctrine()->getRepository(WmsOrder::class);
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $authtokenRepository = $this->getDoctrine()->getRepository(Authtoken::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $this->getDoctrine()->getRepository(Organization::class);

        /** @var SecurityService $securityService */
        $securityService = $this->get('app.wms.security');

        if (!$securityService->isAuth()) {
            return new JsonResponse(['status' => false], 400);
        }

        $phone = $request->request->get('phone');
        $orderId = $request->request->getInt('orderId');
        $authtoken = $request->request->get('authtoken');

        /** @var WmsOrder $wmsOrder */
        $wmsOrder = $wmsOrderRepository->find($orderId);

        if (!empty($authtoken)) {
            $authtoken = str_replace('-', '', strtoupper($authtoken));
            $authtokenEntity = $authtokenRepository->findOneBy(['code' => $authtoken, 'order' => null]);
            if (!$authtokenEntity) {
                return new JsonResponse(['status' => false, 'error' => ['Не удалось найти указанный код или код уже был использован']], 200);
            }
            $user = $authtokenEntity->getUser();
            $authtokenEntity->setOrder($wmsOrder->getOrder());
        } else {
            $user = $userRepository->findOneByUsername($phone);
        }

        if (!$wmsOrder || !$user) {
            return new JsonResponse(['status' => false], 400);
        }

        $organization = $organizationRepository->getCustomerByUser($user);

        $wmsOrder->setCustomer($user);
        if ($organization) {
            $wmsOrder->setOrganization($organization);
        }

        $this->getDoctrine()->getManager()->persist($wmsOrder);
        $this->getDoctrine()->getManager()->flush($wmsOrder);

        if (!empty($authtokenEntity)) {
            $this->getDoctrine()->getManager()->flush($authtokenEntity);
        }

        return new JsonResponse(['status' => true, 'data' => []], 200);
    }
}
