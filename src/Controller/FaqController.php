<?php

namespace App\Controller;

use App\Entity\Content\Faq;
use App\Entity\Content\FaqCategory;
use App\Entity\Direction;
use FOS\UserBundle\Model\User;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Order;

class FaqController extends Controller
{
    /**
     * @Route("/faq/customer", name="app.faq.index", options={"sitemap" = {"priority" = 0.6 }})
     */
    public function indexAction(Request $request)
    {
        $faqCategoryRepository = $this->getDoctrine()->getRepository(FaqCategory::class);
        $faqRepository = $this->getDoctrine()->getRepository(Faq::class);


        $faqCategories = $faqCategoryRepository->findBy(['type' => FaqCategory::TYPE_CUSTOMER]);

        $faq = $faqRepository->findBy(['published' => true, 'category' => $faqCategories], ['position' => 'asc']);


        usort($faqCategories, function($a, $b){
            if ($a->getPosition() == $a->getPosition()) {
                return 0;
            }
            return ($a->getPosition() < $a->getPosition()) ? -1 : 1;
        });

        return $this->render('site/faq/index.html.twig', ['faq' => $faq, 'faqCategories' => $faqCategories,]);
    }

    /**
     * @Route("/faq/carrier", name="app.faq.carrier", options={"sitemap" = {"priority" = 0.6 }})
     */
    public function carrierAction(Request $request)
    {
        $faqCategoryRepository = $this->getDoctrine()->getRepository(FaqCategory::class);
        $faqRepository = $this->getDoctrine()->getRepository(Faq::class);

        $faqCategories = $faqCategoryRepository->findBy(['type' => FaqCategory::TYPE_CARRIER]);

        $faq = $faqRepository->findBy(['published' => true, 'category' => $faqCategories], ['position' => 'asc']);

        usort($faqCategories, function($a, $b){
            if ($a->getPosition() == $a->getPosition()) {
                return 0;
            }
            return ($a->getPosition() < $a->getPosition()) ? -1 : 1;
        });

        return $this->render('site/faq/index.html.twig', ['faq' => $faq, 'faqCategories' => $faqCategories,]);
    }
}
