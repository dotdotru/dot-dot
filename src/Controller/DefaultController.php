<?php

namespace App\Controller;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Direction;
use App\Entity\Order;
use App\Service\Batch\BatchMoverGenerateDocs;
use Doctrine\ORM\EntityManagerInterface;
use Vsavritsky\SettingsBundle\Service\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{

    public function __construct(EntityManagerInterface $em,BatchMoverGenerateDocs $batchGenerateDocs)
    {
        $this->batchGenerateDocs = $batchGenerateDocs;
        $this->em = $em;

        //$this->batchGenerateDocs->generate($this->em->getRepository(Order::class)->find(3733), $this->em->getRepository(User::class)->find(884));
    }

    /**
     * @Route("/", name="app.default.index", options={"sitemap" = {"priority" = 1 }})
     */
    public function indexAction(Request $request)
    {
        /** @var Settings $settings */
        $settings = $this->get('settings');
        $settings = $settings->group('customer_calc');

        $direction = $this->getDoctrine()->getRepository(Direction::class)->findOneBy(['id' => 1, 'published' => true]);

        return $this->render('site/default/index.html.twig', [
            'header' => $settings['header'],
            'subheader' => $settings['subheader'],
            'direction' => null
        ]);
    }

    /**
     * @Route("/d-{slug}", name="app.default.direction")
     */
    public function indexDirectionAction(Request $request, $slug)
    {
        /** @var Settings $settings */
        $settings = $this->get('settings');
        $settings = $settings->group('customer_calc');

        $direction = $this->getDoctrine()->getRepository(Direction::class)->findOneBy(['slug' => $slug, 'published' => true]);

        if (!$direction) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/default/index.html.twig', [
            'header' => $settings['header'],
            'subheader' => $settings['subheader'],
            'direction' => $direction,
        ]);
    }


    /**
     * @Route("/calc", name="app.default.widget-calc", options={"sitemap" = {"priority" = 1 }})
     */
    public function calcWidgetAction(Request $request)
    {
        return $this->render('site/default/calc-widget.html.twig');
    }
}
