<?php

namespace App\Controller;

use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\FileGroup;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Pallet;
use App\Entity\Stock;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\StockRepository;
use App\Service\Payment\SberbankPaymentService;
use App\Application\Sonata\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;

class PersonalController extends Controller
{
    /**
     * @Route("/personal/change_login", name="app.personal.change_login")
     */
    public function changeLoginAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $route = '';
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $parameters = [];

        if (!$user->isCarrierNow() && !$user->isCustomerNow()) {
            if ($user->isCarrier()) {
                $user->setCarrierNow();
            }

            if ($user->isCustomer()) {
                $user->setCustomerNow();
            }
        }

        if ($user->isCarrierNow() && $user->isCustomer()) {
            $user->setCustomerNow();
            $route = 'app.personal.index';
        } elseif ($user->isCustomerNow() && $user->isCarrier()) {
            $user->setCarrierNow();
            $route = 'app.personal.index';
        }

        if ($user->isCarrierNow() && !$user->isCustomer()) {
            $route = 'app.security.register';
            $parameters = ['step' => 3, 'group' => 'sender', 'userId' => $user->getId()];
        } elseif ($user->isCustomerNow() && !$user->isCarrier()) {
            $route = 'app.security.register';
            $parameters = ['step' => 3, 'group' => 'carrier', 'userId' => $user->getId()];
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute($route, $parameters);
    }

    /**
     * @Route("/personal/order/pay", name="app.personal.order_pay")
     */
    public function orderPayAction(Request $request)
    {
        throw $this->createNotFoundException();

        $entityManager = $this->getDoctrine()->getManager();

        $orderId = $request->query->get('orderId');
        if ($orderId <= 0) {
            throw $this->createAccessDeniedException('OrderId empty');
        }

        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->find($orderId);
        if (!$order) {
            throw $this->createNotFoundException('Order not found');
        }

        if ($order->isPayed()) {
            throw $this->createNotFoundException('Order is payed');
        }

        /** TODO:: fix domain name */
        /** @var SberbankPaymentService $paymentService */
        $paymentService = $this->get('app.sberbank_payment');
        $paymentService->setSuccessUrl('https://dot-dot.ru'.$this->generateUrl('app.personal.order_pay_success', [], UrlGeneratorInterface::ABSOLUTE_PATH));

        return $this->redirect($paymentService->payOrder($order));
    }

    /**
     * @Route("/personal/success", name="app.personal.order_pay_success")
     */
    public function orderSuccessAction(Request $request)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        return $this->render('site/personal/order/success.html.twig');
    }

    /**
     * @Route("/personal/profile", name="app.personal.profile")
     */
    public function profileAction(Request $request)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $organization = $organizationRepository->getByUser($user);

        return $this->render('site/personal/profile/index.html.twig', [
            'user' => $user,
            'organization' => $organization
        ]);
    }

    /**
     * @Route("/personal/profile/change_password", name="app.personal.change_password")
     */
    public function profileChangePasswordAction(Request $request)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('site/personal/profile/change_password.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/personal/insured-pdf/{orderId}", name="app.personal.insured_pdf")
    */
    public function insuredPdfAction(Request $request, $orderId)
    {
		$entityManager = $this->getDoctrine()->getManager();
		$user = $this->get('security.token_storage')->getToken()->getUser();

		/** @var OrderRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Order::class);
        $order = $orderRepository->getById($orderId);

		#return new Response($order->getInsuredPdf(), 200,
		#	array('Content-Type' => 'application/pdf')
		#);

		$response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContent(base64_decode(str_replace('data:application/pdf;base64,', '', $order->getInsuredPdf())));
        $response->send();
        return $response;

		return $this->render('site/personal/profile/insured_pdf.html.twig', [
            'order' => $order,
        ]);
	}


    /**
     * @Route("/personal", name="app.personal.index")
     */
    public function indexAction(Request $request)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        $group = $request->query->get('group', '');
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('perPage', 10);

        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($group == 'sender') {
            $user->setCustomerNow();
            $entityManager->persist($user);
            $entityManager->flush();
        } elseif ($group == 'carrier') {
            $user->setCarrierNow();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        if (!$user->isCustomerNow() && !$user->isCarrierNow() && $user->isCustomer()) {
            $user->setCustomerNow();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        if (!$user->isCustomerNow() && !$user->isCarrierNow() && $user->isCarrier()) {
            $user->setCarrierNow();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        if ($user->isCustomerNow()) {
            /** @var StockRepository $stockRepository */
            $stockRepository = $entityManager->getRepository(Stock::class);

            $stocks = $stockRepository->getBy(1, 100, ['title' => 'asc']);

            /** @var OrderRepository $orderRepository */
            $orderRepository = $entityManager->getRepository(Order::class);
            $orders = $orderRepository->getByUser($user, $page, $limit);

            /** @var OrderRepository $orderRepository */
            $fileGroupRepository = $entityManager->getRepository(FileGroup::class);
            $fileGroups = $fileGroupRepository->getDefaultCustomer();

            $maxPages = ceil($orders->count() / $limit);
            #$page = $request->query->getInt('page', 1);

            return $this->render('site/personal/index.html.twig', [
                'orders' => $orders,
                'stocks' => $stocks,
                'fileGroups' => $fileGroups,
                'pagination' => [
                    'page' => $page,
                    'maxPages' => $maxPages,
                ]
            ]);
        } elseif ($user->isCarrierNow()) {
            /** @var OrderRepository $orderRepository */
            $orderRepository = $entityManager->getRepository(Batch::class);
            $orders = $orderRepository->getByUser($user, $page, $limit);

            /** @var OrderRepository $orderRepository */
            $fileGroupRepository = $entityManager->getRepository(FileGroup::class);
            $fileGroups = $fileGroupRepository->getDefaultCarrier();

            $maxPages = ceil($orders->count() / $limit);
            #$page = $request->query->getInt('page', 1);

            return $this->render('site/personal/index.html.twig', [
                'orders' => $orders,
                'fileGroups' => $fileGroups,
                'pagination' => [
                    'page' => $page,
                    'maxPages' => $maxPages,
                ]
            ]);
        }
    }

    /**
     * @Route("/personal/order/customer/{id}", name="app.personal.order_customer")
     */
    public function OrderAction(Request $request, $id)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->addMeta('name', 'viewport', "1400");

        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Order::class);

        $order = $orderRepository->getById($id);

        return $this->render('site/personal/customer/order.html.twig', [
            'order' => $order,
        ]);
    }
}
