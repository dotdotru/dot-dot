<?php

namespace App\Controller;

use App\Entity\Content\Article;
use App\Entity\Page;
use App\Entity\Partner;
use App\Entity\Stock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PartController extends Controller
{
    /**
     * @Route("/part/partners", name="app.part.partners")
     */
    public function partnersAction(Request $request)
    {
        $partnerRepository = $this->getDoctrine()->getRepository(Partner::class);

        $partners = $partnerRepository->getBy(1, 100, ['position' => 'asc']);

        return $this->render('block/partners.html.twig', ['partners' => $partners]);
    }

    /**
     * @Route("/part/articles", name="app.part.articles")
     */
    public function articlesAction(Request $request)
    {
        $newsRepository = $this->getDoctrine()->getRepository(Article::class);

        $news = $newsRepository->getBy(1, 100, ['id' => 'desc']);

        $tags = [];
        foreach ($news as $newsItem) {
            foreach ($newsItem->getTags() as $tag) {
                $tags[$tag->getId()] = $tag;
            }
        }

        return $this->render('block/articles.html.twig', ['articles' => $news, 'tags' => $tags]);
    }
}
