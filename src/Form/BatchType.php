<?php

namespace App\Form;

use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Stock;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use Sonata\CoreBundle\Form\Type\DateTimeRangeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BatchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sendDate', TextType::class)
            ->add('weight', TextType::class)
            ->add('direction', EntityType::class, ['class' => Direction::class, 'mapped' => true])
            ->add('stockFrom', EntityType::class, ['class' => Stock::class, 'mapped' => true])
            ->add('stockTo', EntityType::class, ['class' => Stock::class, 'mapped' => true])
            ->add('maxWeight', TextType::class)
            ->add('maxCount', TextType::class)
            ->add('currentPricePerKilo', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Batch::class,
            'allow_extra_fields' => true,
        ));
    }
}