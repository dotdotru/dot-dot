<?php

namespace App\Form;

use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;

use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class OrganizationDriverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('inn', TextType::class)
            ->add('birthdate', TextType::class)
            ->add('phone', TextType::class)
            //->add('documents', CollectionType::class, array(
            //    'type' => OrganizationDriverType::class,
            //    'type_options' => array('label' => false),
            //))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => OrganizationDriver::class,
            'allow_extra_fields' => true,
        ));
    }
}