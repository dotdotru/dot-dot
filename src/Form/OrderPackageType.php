<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\OrderPackage;
use App\Entity\PackageType;
use App\Entity\Packing;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderPackageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('width', TextType::class, ['required' => false])
            ->add('height', TextType::class, ['required' => false])
            ->add('depth', TextType::class, ['required' => false])
            ->add('count', TextType::class)
            ->add('weight', TextType::class)
            ->add('calculateWeight', TextType::class, ['required' => false])
            ->add('packageType', EntityType::class, ['data' => '', 'class' => PackageType::class, 'mapped' => true, 'required' => false])
            ->add('packageTypeAnother', TextType::class)
            ->add('packing', EntityType::class, ['data' => '', 'class' => Packing::class, 'mapped' => true, 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => OrderPackage::class,
            'allow_extra_fields' => true,
        ));
    }
}