<?php

namespace App\Form\Wms;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Stock;
use App\Entity\Wms\WmsOrder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderSaveType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('stockFrom', EntityType::class, [
                'class' => Stock::class
            ])
            ->add('stockTo', EntityType::class, [
                'class' => Stock::class
            ])
            ->add('receiver', EntityType::class, [
                'class' => Person::class
            ])
            ->add('sender', EntityType::class, [
                'class' => Person::class
            ])
            ->add('payer', EntityType::class, [
                'class' => Person::class
            ])
            ->add('declaredPrice', TextType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => WmsOrder::class,
            'allow_extra_fields' => true,
        ));
    }
}