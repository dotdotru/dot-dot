<?php

namespace App\Form;

use App\Entity\Direction;
use App\Entity\Order;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('sender', TextType::class, ['mapped' => true])
            ->add('declaredPrice', TextType::class)
            ->add('direction', EntityType::class, ['class' => Direction::class, 'mapped' => true])
            ->add('maxPrice', TextType::class)
            ->add('minPrice', TextType::class)
            ->add('declaredPrice', TextType::class)
            ->add('byCargo', IntegerType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Order::class,
            'allow_extra_fields' => true,
        ));
    }
}