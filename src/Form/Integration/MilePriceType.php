<?php

namespace App\Form\Integration;

use App\Entity\CityPricePerKilo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MilePriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, [])
            ->add('distance', TextType::class, ['label' => 'Расстояние', 'required' => false, 'disabled' => true])
            ->add('weight', TextType::class, ['label' => 'Вес', 'required' => false, 'disabled' => true])
            ->add('price', TextType::class, ['label' => 'Цена', 'required' => false])
            ->add('per_km', CheckboxType::class, ['label' => 'Цена за км', 'required' => false, 'disabled' => true])
        ;
/*
        'distance_from' => $distance['from'],
        'distance_to' => $distance['to'],
        'weight_from' => $weight['from'],
        'weight_to' => $weight['to'],
        'price' => 0,
        'per_km' => false
*/

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}

