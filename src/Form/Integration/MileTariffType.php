<?php

namespace App\Form\Integration;

use App\Entity\CityPricePerKilo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MileTariffType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('loaders', TextType::class, ['label' => 'Количество грузчиков', 'required' => false])
            ->add('tail_lift', CheckboxType::class, ['label' => 'Гидроборт', 'required' => false])
            ->add('refrigerator', CheckboxType::class, ['label' => 'Наличие рефрежиратора', 'required' => false])
            ->add('delete', CheckboxType::class, ['label' => 'Удалить', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
