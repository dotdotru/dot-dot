<?php

namespace App\Form;

use App\Entity\Batch;
use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Stock;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use Sonata\CoreBundle\Form\Type\DateTimeRangeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderOverloadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weight', TextType::class)
            ->add('direction', EntityType::class, ['class' => Direction::class, 'mapped' => true])
            ->add('stockFrom', EntityType::class, ['class' => Stock::class, 'mapped' => true])
            ->add('stockTo', EntityType::class, ['class' => Stock::class, 'mapped' => true])
            ->add('carrier', EntityType::class, ['class' => User::class, 'mapped' => true])
            #->add('drivers', CollectionType::class, ['entry_type' => OrganizationDriver::class])
            #->add('pallets', CollectionType::class, ['entry_type' => TextType::class])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Batch::class,
            'allow_extra_fields' => true,
        ));
    }
}