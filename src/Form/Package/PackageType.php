<?php

namespace App\Form\Package;

use App\Entity\Packing;
use App\Entity\Wms\WmsPackage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PackageType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('packing', EntityType::class, [
                'class' => Packing::class
            ])
            // @todo исправить
            ->add('packageType', EntityType::class, [
                'class' => \App\Entity\PackageType::class
            ])
            ->add('width', NumberType::class)
            ->add('height', NumberType::class)
            ->add('depth', NumberType::class)
            ->add('weight', NumberType::class)
            ->add('packingPrice', NumberType::class, ['data' => 0])
            ->add('damaged', TextType::class)
            ->add('palletId', TextType::class)
            ->add('count', NumberType::class, ['data' => 1])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => WmsPackage::class,
            'allow_extra_fields' => true,
        ));
    }
}