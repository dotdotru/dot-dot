<?php

namespace App\Form;

use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uridical', TextType::class)
            ->add('type', TextType::class)
            ->add('inn', TextType::class)
            ->add('contactName', TextType::class)
            ->add('contactPhone', TextType::class)
            ->add('contactEmail', EmailType::class)
            ->add('code', TextType::class)
            ->add('issuance', TextType::class)
            ->add('series', TextType::class)
            ->add('number', TextType::class)
            ->add('companyName', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Person::class,
            'allow_extra_fields' => true,
        ));
    }
}