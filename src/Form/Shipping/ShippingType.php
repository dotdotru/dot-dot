<?php

namespace App\Form\Shipping;

use App\Entity\Direction;
use App\Entity\Order;
use App\Entity\Person;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Entity\Stock;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ShippingType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('stock', EntityType::class, [
                'class' => Stock::class
            ])
            ->add('address', TextType::class)
            ->add('active', TextType::class)
            ->add('countLoader', NumberType::class)
            ->add('hydroBoard', TextType::class)
            ->add('pickAt', DateTimeType::class, [
                'widget' => 'single_text',
                'input_format' => 'Y-m-d H:i:s',
            ])
        ;

        // Аякс может прислать данные в виде строки @todo подумать над этим
        $filterBool = function ($hydroBoard) {
            return filter_var($hydroBoard, FILTER_VALIDATE_BOOLEAN);
        };
        $builder->get('hydroBoard')
            ->addModelTransformer(new CallbackTransformer($filterBool, $filterBool))
        ;
        $builder->get('active')
            ->addModelTransformer(new CallbackTransformer($filterBool, $filterBool))
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Shipping::class,
            'allow_extra_fields' => true,
        ));
    }
}