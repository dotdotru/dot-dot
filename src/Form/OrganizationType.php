<?php

namespace App\Form;

use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationDriver;
use App\Application\Sonata\UserBundle\Entity\Group;
use App\Application\Sonata\UserBundle\Entity\User;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class OrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class)
            ->add('companyName', TextType::class)
            ->add('nds', TextType::class)
            ->add('inn', TextType::class)
            ->add('kpp', TextType::class)
            ->add('ceo', TextType::class)
            ->add('uridical', CheckboxType::class)
            ->add('address', TextType::class)
            ->add('contactName', TextType::class)
            ->add('contactPhone', TextType::class)
            ->add('rs', TextType::class)
            ->add('bik', TextType::class)
            //->add('drivers', CollectionType::class, array(
            //    'type' => OrganizationDriverType::class,
            //    'type_options' => array('label' => false),
            //))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Organization::class,
            'allow_extra_fields' => true,
        ));
    }
}