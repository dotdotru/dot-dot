<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\RearrangePromocodeService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Date;

class BatchRemoveCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:batches.remove')
            ->setDescription('remove batches')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Batch::class);
        /** @var OrganizationRepository $orderRepository */
        $organizationRepository = $entityManager->getRepository(Organization::class);

        $orders = $orderRepository->getNew();

        $currentAt = new \DateTime();
  
        /** @var Batch $order */
        foreach ($orders as $order) {
			if (!$order->getCarrier()) {
				continue;
			}
			
            /** @var Organization $organization */
            $organization = $organizationRepository->getByUser($order->getCarrier());

            if ($order->isRemoveExactStatus() && count($order->getDrivers()) <= 0 && $organization && !$organization->isWithoutDrivers()) {
                $output->writeln($order->getId());
                $diffInSeconds = $currentAt->getTimestamp() - $order->getCreatedAt()->getTimestamp();

                if ($diffInSeconds > 301) {
                    $order->setRemove(true);
                    $entityManager->persist($order);
                }
                $output->writeln($order->getId().' '.round($diffInSeconds / 60).' мин');
            }

            $entityManager->flush();
        }
        
        /*$orders = $orderRepository->findAll();
        foreach($orders as $order) {
			if (in_array($order->getStatus(), [Batch::STATUS_NEW, Batch::STATUS_RESERVED, Batch::STATUS_READY, Batch::STATUS_SHIPPING, Batch::STATUS_ARRIVED, Batch::STATUS_PAYED, Batch::STATUS_SUSPENDED])) {
				$order->setRemove(false);
				$entityManager->persist($order);
				$entityManager->flush();
			}
		}
		
		die(); */
    }
}
