<?php

namespace App\Command;

use App\Entity\BaseEvent;
use App\Repository\BaseEventRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class BaseEventCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:event')
            ->setDescription('get base event')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var BaseEventRepository $repository */
        $repository = $entityManager->getRepository(BaseEvent::class);

        /** @var MailService $service */
        $mailService = $container->get('app.mail');

        /** @var IntegrationBaseService $service */
        $service = $container->get('app.integration_base');
        $events = $service->getEvents();
        
        /*$events['data'][] = [
            'id' => 1,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'booked3',
        ];

        $events['data'][] = [
            'id' => 2,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'waiting3',
        ];

        $events['data'][] = [
            'id' => 3,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'shipping',
        ];

        $events['data'][] = [
            'id' => 124,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'delivered',
        ];

        $events['data'][] = [
            'id' => 4,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'shipped',
        ];

        $events['data'][] = [
            'id' => 5,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => '	pallet40',
        ];

        $events['data'][] = [
            'id' => 6,
            'entity_type' => 'order',
            'entity_id' => 1134,
            'job_data' => 'shipped3',
        ];*/

        if (!$events[IntegrationBaseService::RESULT_SUCCESS] || empty($events['data'])) {
            return 0;
        }

        foreach ($events['data'] as $event) {
            $baseEvent = $repository->getByExternalId($event['id']);
            if (!$baseEvent) {
                $baseEvent = new BaseEvent();
                $baseEvent->setStatus(BaseEvent::STATUS_CREATE);
                $baseEvent->setExternalId($event['id']);
                $baseEvent->setEntityId($event['entity_id']);
                $baseEvent->setEntityType($event['entity_type']);
                $baseEvent->setEvent($event['job_name']);
            }

            $entityManager->persist($baseEvent);
            $entityManager->flush();
        }
    }
}
