<?php

namespace App\Command;

use App\Entity\Order;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Repository\OrderRepository;
use App\Service\BatchMoverService;
use App\Service\FileUploadService;
use App\Service\MailService;
use App\Service\Mapper\Mover\MoverMapperService;
use App\Service\MoverService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Ratchet\Client\WebSocket;
use React\Promise\PromiseInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SocketMoverCommand extends Command
{
    use LockableTrait;

    /** @var MoverService $moverService */
    private $moverService;

    protected function configure()
    {
        $this
            ->setName('app:mover.connect')
            ->setDescription('Connect to mover24 websocket')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        if (!$this->lock()) {
            $this->output->writeln('The command is already running in another process.');
            return 0;
        }

        /** @var Container $container */
        $this->container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $this->entityManager = $this->container->get('doctrine')->getManager();

        /** @var MoverService $moverService */
        $this->moverService = $this->container->get('app.mover');

        /** @var PromiseInterface $wsConnection */
        $wsConnection = $this->moverService->getWsConnection();

        $wsConnection->then(function ($wsLoop) {
            /** @var WebSocket $wsLoop */

            $wsLoop->send($this->beginSubscribe());

            $wsLoop->on('message', function($wsMessage) use ($wsLoop) {
                $this->parseJson($wsMessage);
            });

        }, function ($wsException) {
            echo ("Could not connect: {$wsException->getMessage()}\n");
            return false;
        });

        return true;
    }

    private function beginSubscribe()
    {
        return json_encode(['action' => 'listen_delivery']);
    }

    private function parseJson($wsMessage)
    {
        $wsArray = json_decode($wsMessage, true);

        if (!is_array($wsArray)) {
            return false;
        }

        if (isset($wsArray['data'])) {
            $wsData = $wsArray['data'];
            $wsOrderId = $wsData['id'];

            if ($wsData['status'] == MoverService::WS_STATUS_ACCEPTED) { // 'accepted' => new driver
                $driverInfo = $this->moverService->getOrderDriver($wsOrderId);
                $this->setShippingDriver($wsOrderId, $driverInfo);
            }
        } elseif (isset($wsArray['success'])) {
            echo ("Mover WebSocket: Connected\n");
        } else {
           echo ("Mover WebSocket: Could not parse mover response\n");
           return false;
        }

        return false;
    }

    private function setShippingDriver($shippingId, $driverInfo)
    {
        /** @var Container $container */
        $this->container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $this->entityManager = $this->container->get('doctrine')->getManager();

        /** @var MoverMapperService $moverMapperService */
        $moverMapperService = $this->container->get('app.mover.mapper');

        /** @var BatchMoverService $batchMoverService */
        $batchMoverService = $this->container->get('app.batch_mover');

        $this->doctrine = $this->container->get('doctrine');

        $this->doctrine->getConnection()->beginTransaction();

        try {
            /** @var Shipping $moverShipping */
            $moverShipping =  $this->entityManager->getRepository(Shipping::class)->findOneBy(['externalId' => $shippingId]);

            /** @var Order $order */
            $order = $this->entityManager->getRepository(Order::class)->getByShipping($shippingId);

            //if ($moverShipping->getDriverName()) {
            //    throw new \Exception(sprintf('set driver is end in shipping %s', $moverShipping->getExternalId()));
            //}

            if (!$driverInfo) {
                throw new \Exception(sprintf('not found delivery %s', $moverShipping->getExternalId()));
            }

            $moverMapperService->mapDriverDataToShipping($moverShipping, $driverInfo);
            $batchMoverService->createBatchMover($order, $moverShipping);

            $this->entityManager->persist($moverShipping);
            $this->entityManager->flush();

            $this->doctrine->getConnection()->commit();
        } catch (\Exception $e) {
            $this->doctrine->getConnection()->rollBack();

            $this->output->writeln(sprintf('Ошибка при назначении водителя %s', $e->getMessage()));
        }

        return true;
    }
}
