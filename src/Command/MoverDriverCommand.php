<?php

namespace App\Command;

use App\Entity\Order;
use App\Entity\Receiver;
use App\Entity\Shipping;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Repository\OrderRepository;
use App\Service\BatchMoverService;
use App\Service\FileUploadService;
use App\Service\MailService;
use App\Service\Mapper\Mover\MoverMapperService;
use App\Service\MoverService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Ratchet\Client\WebSocket;
use React\Promise\PromiseInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MoverDriverCommand extends Command
{
    use LockableTrait;

    /** @var BatchMoverService */
    private $batchMoverService;

    /** @var MoverService */
    private $moverService;

    protected function configure()
    {
        $this
            ->setName('app:mover.check-drivers')
            ->setDescription('check fail set driver mover')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        if (!$this->lock()) {
            $this->output->writeln('The command is already running in another process.');
            return 0;
        }

        /** @var Container $container */
        $this->container =  $this->getApplication()->getKernel()->getContainer();
        $this->doctrine = $this->container->get('doctrine');
        /** @var EntityManager $entityManager */
        $this->entityManager = $this->container->get('doctrine')->getManager();

        $this->moverService = $this->container->get('app.mover');

        /** @var MoverMapperService moverMapperService */
        $this->moverMapperService = $this->container->get('app.mover.mapper');
        $this->batchMoverService = $this->container->get('app.batch_mover');

        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager->getRepository(Order::class);
        $shippingRepository = $this->entityManager->getRepository(Shipping::class);
        $shippings = $shippingRepository->getActiveWithoutDriver();
        
        //$order = $orderRepository->getByShipping(1);
        //$this->batchMoverService->createBatchMover($order, $order->getShipping(Shipping::TYPE_PICKUP));
        //
        //die();

        /** @var Shipping $shipping */
        foreach ($shippings as $shipping) {
            $order = $orderRepository->getByShipping($shipping->getExternalId());

            if ($order) {
                $this->doctrine->getConnection()->beginTransaction();
                try {
                    $driverInfo = $this->moverService->getOrderDriver($shipping->getExternalId());

                    if (!$driverInfo) {
                        throw new \Exception(sprintf('not found delivery %s', $shipping->getExternalId()));
                    }

                    $driverData = $driverInfo['employee'];
                    $driverDocument = (isset($driverData['passport']['series']) && isset($driverData['passport']['number'])) ?
                        "{$driverData['passport']['series']} {$driverData['passport']['number']}" : '';

                    if ($shipping->getDriverDocument() == $driverDocument) {
                        throw new \Exception(sprintf('driver not change %s', $shipping->getExternalId()));
                    }

                    $this->moverMapperService->mapDriverDataToShipping($shipping, $driverInfo);
                    $this->batchMoverService->createBatchMover($order, $shipping);
                } catch (\Exception $e) {
                    $this->output->writeln(sprintf('Ошибка при назначении водителя %s',  $e->getMessage()));
                    $this->output->writeln($e->getFile());
                    $this->output->writeln($e->getLine());

                    $this->doctrine->getConnection()->rollBack();
                    continue;
                }

                $this->doctrine->getConnection()->commit();
            }
        }

        return true;
    }

}
