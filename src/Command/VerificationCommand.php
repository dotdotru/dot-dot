<?php

namespace App\Command;

use App\Service\VerificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VerificationCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:verification')
            ->setDescription('verification organization')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var VerificationService $verificationService */
        $verificationService = $this->getApplication()->getKernel()->getContainer()->get('app.verification');
        $verificationService->verifyAll();
    }
}