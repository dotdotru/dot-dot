<?php

namespace App\Command;

use App\Service\InsuranceService;
use App\Service\VerificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class InsuranceCancelCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:insurance.cancel')
            ->setDescription('insurance product')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var InsuranceService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('app.insurance');
        $service->insuranceCancelAll();
    }
}