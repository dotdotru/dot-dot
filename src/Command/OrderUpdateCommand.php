<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\InsuranceService;
use App\Service\Integration\Update\MileUpdate;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\OrderUpdateService;
use App\Service\VerificationService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsOrderUpdateService;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderUpdateCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:orders.update')
            ->setDescription('update orders')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $doctrine->getRepository(Order::class);

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $container->get('app.integration_base');

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $container->get('app.order.update');

        /** @var MileUpdate $mileUpdateService */
        $mileUpdateService = $container->get('app.service.integration.mile.update');


        $page = 1;
        $count = 50;
        $ordersData = $integrationBaseService->getOrders($page, $count);

        while ($ordersData['data'] && $ordersData['data'] != null) {
            if ($page > 20) {
                break;
            }
            $i = 0;
            foreach (array_reverse($ordersData['data']) as $orderData) {
                $i++;

                $output->writeln($orderData['ext_id']);
                $order = $orderRepository->findOneBy(['id' => $orderData['ext_id']]);

                if (!$order) {
                    $order = $orderRepository->findOneBy(['externalId' => $orderData['id']]);
                }

                if (!$order) {
                    $order = new Order();
                }

                if ($order->isRemove()) {
                    continue;
                }

                $order = $orderUpdateService->updateOrder($order, $orderData);
                $entityManager->persist($order);


                // Обновляем данные доставки
                $mileUpdateService->update($orderData['id'], $orderData['miles']);


                if ($i % 10 == 0) {
                    $entityManager->flush();
                }
            }

            $entityManager->flush();

            $page++;
            $ordersData = $integrationBaseService->getOrders($page, $count);
        }

        // Remove old orders
        /*if (!empty($result['error']) && preg_match('#order (\d)+ not found#', $result['error']['details']['message_primary'])) {
            $order->setRemove();
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }*/

        $output->writeln('___________________________________________');

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Batch::class);

        $orders = $orderRepository->getByUpdate();

        /** @var Batch $order */
        foreach ($orders as $order) {
            if ($order->getExternalId() > 0) {
                $orderUpdateService->updateBatch($order);
            }
        }
    }
}
