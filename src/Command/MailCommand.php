<?php

namespace App\Command;

use App\Entity\Email;
use App\Service\InsuranceService;
use App\Service\MailService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class MailCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:mail')
            ->setDescription('get base event')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        /** @var MailService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('app.mail');
        $emails = $service->getSendMail();

        if (!count($emails)) {
            return 0;
        }

        $progressBar = new ProgressBar($output, count($emails));

        $progressBar->start();

        /** @var Email $email */
        foreach ($emails as $email) {
            if (!in_array($email->getStatus(), [Email::STATUS_ERROR, Email::STATUS_CREATE])) {
                continue;
            }

            try {
                $service->sendEmail($email);
            } catch (\Exception $e) {
                $output->writeln('id: '.$email->getId());
                $output->writeln('error: '.$e->getMessage());

                $email->setStatus(Email::STATUS_ERROR);
            }

            $entityManager->persist($email);
            $entityManager->flush();
        }
    }
}