<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\OrderUpdateService;
use App\Service\VerificationService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsOrderUpdateService;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BatchUpdateCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:batches.update')
            ->setDescription('update batches')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $container->get('app.order.update');

        /** @var BatchRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Batch::class);

        $orders = $orderRepository->getByUpdate();

        /** @var Batch $order */
        foreach ($orders as $order) {
            if ($order->getExternalId() > 0) {
                try {
                    $output->writeln($order->getExternalId());
                    $orderUpdateService->updateBatch($order);
                } Catch (\Exception $e) {
                    dump('Error on update batch');
                }
            }
        }
    }
}
