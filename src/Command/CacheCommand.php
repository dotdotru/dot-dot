<?php

namespace App\Command;

use App\Entity\Direction;
use App\Service\AuctionService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CacheCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:cache')
            ->setDescription('get cache')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        $directionRepository = $entityManager->getRepository(Direction::class);

        /** @var AuctionService $auctionService */
        $auctionService = $container->get('app.auction');

        $directions = $directionRepository->findAll();

        foreach ($directions as $direction) {
            $auctionService->setDirection($direction);

            for (
                $price = $auctionService->getStartPricePerKilo();
                $price <= $auctionService->getEndPricePerKilo();
                $price = $price + $auctionService->getPriceUpInPeriod()
            ) {
                $weight = $auctionService->getWeightWarehouses($price);
            }

        }

        return null;
    }
}