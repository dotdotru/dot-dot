<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Wms\WmsBatchMover;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\RearrangePromocodeService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Date;

class BatchMoverStatusCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:batch.movers.status')
            ->setDescription('change batch mover status to denied')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        $batchMoverRepository = $entityManager->getRepository(WmsBatchMover::class);

        $batches = $batchMoverRepository->findByStatus(['status' => WmsBatchMover::STATUS_READY]);

        $currentAt = new \DateTime();

        /** @var WmsBatchMover $batch */
        foreach ($batches as $batch) {

            if ($batch->isReady()) {
                $output->writeln($batch->getId());
                $diffInSeconds = $currentAt->getTimestamp() - $batch->getShipping()->getPickAt()->getTimestamp();

                if ($diffInSeconds > 24 * 3600) {
                    $batch->setStatus(WmsBatchMover::STATUS_REJECTED);
                    $entityManager->persist($batch);
                }
                $output->writeln($batch->getId().' '.round($diffInSeconds / 60).' мин');
            }

            $entityManager->flush();
        }
    }
}
