<?php

namespace App\Command;

use App\Entity\Email;
use App\Entity\Notification\NotificationQueue;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Shipping;
use App\Service\InsuranceService;
use App\Service\MailService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class NotificationQueueCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:notification:queue')
            ->setDescription('get base event')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        $organizationRepository = $entityManager->getRepository(Organization::class);
        $orderRepository = $entityManager->getRepository(Order::class);
        $shippingRepository = $entityManager->getRepository(Shipping::class);

        /** @var MailService $mailService */
        $mailService = $this->getApplication()->getKernel()->getContainer()->get('app.mail');

        $queues = $entityManager->getRepository(NotificationQueue::class)->findBy(['processed' => false]);

        $progressBar = new ProgressBar($output, count($queues));
        $progressBar->start();

        /** @var NotificationQueue $queue */
        foreach ($queues as $queue) {
            if ($queue->getType() === 'mile_created') {
                $params = $queue->getPayload();

                /** @var Order $order */
                $order = $orderRepository->find($params['order_id']);
                /** @var Shipping $shipping */
                $shipping = $shippingRepository->find($params['shipping']);

                if (!$shipping || !$shipping->getStock()) continue;

                $carriers = $organizationRepository->findCityCarriersForMiles($shipping->getStock()->getCity());


                $shippingType = $shipping->getType() == Shipping::TYPE_PICKUP ? 'Забор' : 'Доставка';
                foreach ($carriers as $carrier) {
                    $mailService->registerEmailHtmlTemplate(
                        'miles/create_carriers.html.twig',
                        'Москва. ' . $shippingType . ' ' . $order->getTotalWeight() . 'кг ' . $shipping->getPickAt()->format('H:i d.m.Y'),
                        $carrier->getEmail(),
                        ['order' => $order, 'shipping' => $shipping]
                    );
                }
            }

            $queue->setProcessed(true);
            $entityManager->flush($queue);
        }

        return true;
    }
}
