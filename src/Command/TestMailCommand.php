<?php

namespace App\Command;

use App\Entity\Email;
use App\Service\InsuranceService;
use App\Service\MailService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class TestMailCommand extends Command
{

    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:test-mail')
            ->setDescription('test mail sending')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container =  $this->getApplication()->getKernel()->getContainer();


        $message = new \Swift_Message();
        $message->setSubject('TEST EMAIL')
            ->setFrom('no_reply@dot-dot.ru')
            ->setTo('no_reply@dot-dot.ru')
            ->setBody('<h1>TEST</h1>', 'text/html')
        ;

        if ($this->mailer->send($message)) {
            $output->writeln('mail sended');
        } else {
            $output->writeln('error on mail sending');
        }


    }
}