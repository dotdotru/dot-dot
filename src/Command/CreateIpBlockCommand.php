<?php

namespace App\Command;

use App\Entity\City;
use App\Entity\Order;
use App\Entity\Payment;
use App\Entity\Shipping\ShippingIpBlock;
use App\Entity\Shipping\ShippingIpPrice;
use App\Service\Payment\SberbankPaymentService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateIpBlockCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:generate:ip.blocks')
            ->setDescription('generate:ip.blocks')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        $cityRepository = $entityManager->getRepository(City::class);

        $cities = $cityRepository->findAll();

        $dataEmptyBlocks = [
            [
                'title' => 'Базовый тариф',
                'hydroboard' => false,
                'countLoader' => 0,
                'weights' => ['250', '500', '1000', '1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ],
            [
                'title' => 'Тарифы на услуги одного грузчика',
                'hydroboard' => false,
                'countLoader' => 1,
                'weights' => ['250', '500', '1000', '1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ],
            [
                'title' => 'Тарифы на услуги двух грузчиков',
                'hydroboard' => false,
                'countLoader' => 2,
                'weights' => ['250', '500', '1000', '1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ],
            [
                'title' => 'Тарифы на услуги авто с гидроборотом',
                'hydroboard' => true,
                'countLoader' => 0,
                'weights' => ['1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ],
            [
                'title' => 'Тарифы на услуги одного грузчика при заказе авто с гидробортом',
                'hydroboard' => true,
                'countLoader' => 1,
                'weights' => ['1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ],
            [
                'title' => 'Тарифы на услуги двух грузчика при заказе авто с гидробортом',
                'hydroboard' => true,
                'countLoader' => 2,
                'weights' => ['1500', '3000', '5000'],
                'distances' => ['10', '20', '30', '40', '50'],
            ]
        ];

        /** @var City $city */
        foreach ($cities as $city) {

            if ($city->getPriceBlocks()->count() > 0) continue;

            foreach ($dataEmptyBlocks as $dataEmptyBlock) {
                $block = new ShippingIpBlock();
                $block->setTitle($dataEmptyBlock['title']);
                $block->setHydroboard($dataEmptyBlock['hydroboard']);
                $block->setCountLoader($dataEmptyBlock['countLoader']);

                foreach ($dataEmptyBlock['distances'] as $distance) {
                    foreach ($dataEmptyBlock['weights'] as $weight) {
                        $price = new ShippingIpPrice();
                        $price->setDistantion($distance);
                        $price->setWeightTo($weight);

                        $block->addPrice($price);
                        $entityManager->persist($price);
                    }
                }

                if ($distance == 50) {
                    foreach ($dataEmptyBlock['weights'] as $weight) {
                        $price = new ShippingIpPrice();
                        $price->setDistantion($distance);
                        $price->setWeightTo($weight);
                        $price->setPriceAfterLast(true);

                        $block->addPrice($price);
                        $entityManager->persist($price);
                    }
                }

                $entityManager->persist($block);
                $entityManager->flush();

                $city->addPriceBlock($block);
                $entityManager->persist($city);
                $entityManager->flush();
            }
        }
    }
}
