<?php

namespace App\Command;

use App\Entity\InsuredQueue;
use App\Service\Insurance\InsuranceService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class InsuranceCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:insurance')
            ->setDescription('insurance product')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $em = $container->get('doctrine')->getManager();

        /** @var InsuranceService $insuranceService */
        $insuranceService = $container->get('app.insurance_order');

        $queueRepository = $em->getRepository(InsuredQueue::class);
        /** @var InsuredQueue[] $queues */
        $queues = $queueRepository->findBy(['processed' => false]);

        foreach ($queues as $queue) {
            try {
                $insuranceService->insurance($queue->getOrder());
                $queue->setProcessed(true);
                $em->flush();
            } Catch (\Exception $e) {
                dump($e);
                // @todo добавить логирование
            }
        }
    }
}