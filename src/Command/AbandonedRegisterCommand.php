<?php

namespace App\Command;

use App\Entity\AbandonedRegister;
use App\Service\AbandonedRegisterService;
use App\Service\InsuranceService;
use App\Service\VerificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class AbandonedRegisterCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:abandoned.register')
            ->setDescription('Check abandoned register and send email')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var AbandonedRegisterService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('app.abandoned_register');
        $service->execute();
    }
}