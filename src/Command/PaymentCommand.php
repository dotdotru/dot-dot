<?php

namespace App\Command;

use App\Entity\Order;
use App\Entity\Payment;
use App\Service\Payment\SberbankPaymentService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:payment')
            ->setDescription('payment check')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        /** @var SberbankPaymentService $paymentService */
        $paymentService = $this->getApplication()->getKernel()->getContainer()->get('app.sberbank_payment');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        $orderCustomerRepository = $entityManager->getRepository(Order::class);

        $orders = $orderCustomerRepository->findAll();

        /** @var Order $order */
        foreach ($orders as $order) {
            $payment = $order->getPayment();
			if (!$payment) continue; 
            if ($payment->getStatus() != Payment::STATUS_CREATED) {
                $info = $paymentService->checkPayment($order);
                
                if ($info['status']) {
					$payment->setStatus($info['status']);
				}
            }

            if ($payment->isDeposited()) {
                $paymentService->setPayed($order);
            }

            $entityManager->persist($payment);
            $entityManager->persist($order);
        }
        $entityManager->flush();
    }
}
