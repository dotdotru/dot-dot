<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Pallet;
use App\Entity\Person;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPallet;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\OrderUpdateService;
use App\Service\Shipping\ShippingSaveService;
use App\Service\User\UserVatService;
use App\Service\VerificationService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsOrderUpdateService;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserUpdateCommand extends Command
{

    use LockableTrait;


    public function __construct(
        UserVatService $userVatService
    )
    {
        $this->userVatService = $userVatService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:user.update')
            ->setDescription('update user')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');
        $integrationBaseService = $container->get('app.integration_base');

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        $orderRepository = $entityManager->getRepository(Order::class);

                $order = $orderRepository->find(5160);


        /** @var IntegrationBaseService $integrationBaseService */
                $integrationBaseService = $container->get('app.integration_base');
                dump($integrationBaseService->getInfoOrder($order->getExternalId()));
                die();



        $organizations = $entityManager->getRepository(Organization::class)->findAll();
        /** @var Organization $organization */
        foreach ($organizations as $organization) {
            if ($organization->getCreatedAt()) {
                dump('update ' . $organization->getId());

                $user = $organization->getUser();
                $person = $this->mapPersonFromUser($user);
                $resultData = $integrationBaseService->saveActor($this->mapPersonToActor($person, $user));
                $entityManager->flush();
            }
        }

        die();


        $orderRepository = $entityManager->getRepository(Order::class);

        $order = $orderRepository->find();
        $pickupDeliver = $orderRepository->find();
        $this->get(ShippingSaveService::class);

                $order = $orderRepository->find(3130);


        /** @var IntegrationBaseService $integrationBaseService */
                $integrationBaseService = $container->get('app.integration_base');
                dump($integrationBaseService->getInfoOrder($order->getExternalId()));
                die();


        $userRepository = $doctrine->getRepository(User::class);

        /** @var OrganizationRepository $organizationRepository */
        $organizationRepository = $doctrine->getRepository(Organization::class);


        $users = $userRepository->findBy([], ['id' => 'desc']);

        $orderRepository = $entityManager->getRepository(Order::class);

        $order = $orderRepository->find(3125);

        /** @var InsuranceService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('app.insurance_order');
        $service->insurance($order);

        die();



        $personRepository = $entityManager->getRepository(Person::class);
        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $container->get('app.order.update');
        $newPerson = $personRepository->find(7790);
        $orderUpdateService->updatePerson($newPerson);
        die();


        /** @var User $user */
        foreach ($users as $user) {
			if ($user->getPerson()) {
				if (!$user->getPerson()->getExternalId()) {
					if ($user->getPerson()->isUridical()) {
						 var_dump( $user->getPerson()->getInn());
						 $persons = $doctrine->getRepository(Person::class)->findBy(['inn' => $user->getPerson()->getInn()], []);
						 foreach($persons as $person) {
							 if ($person->getExternalId()) {
								 $user->setPerson($person);
							 }
						 }

						$output->writeln($user->getId());

						$entityManager->persist($user);
						$entityManager->flush();

					}
				}
			}

            /*$output->writeln($user->getId());
            if ($user->isCustomer() && !$user->getUserPerson()) {
                $user->setTwitterUid('test');
            }

            $organization = $organizationRepository->getCustomerByUser($user);
            $user->setOrganizationCustomer($organization);

            $organization = $organizationRepository->getCarrierByUser($user);
            $user->setOrganizationCarrier($organization);

            $entityManager->persist($user);
            $entityManager->flush();*/
        }

    }




    public function mapPersonFromUser(User $user)
    {
        $person = new Person();
        if ($user->getPerson()) {
            $person = $user->getPerson();
        }

        $person->setUser($user);
        $person->setVat($this->userVatService->get($user));

        if ($user->getOrganizationCustomer()) {
            $organization = $user->getOrganizationCustomer();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        } else if ($user->getOrganizationCarrier()) {
            $organization = $user->getOrganizationCarrier();
            $person->setUridical(true);
            $person->setType($organization->getType());
            $person->setCompanyName($organization->getCompanyName());
            $person->setInn($organization->getInn());
            $person->setContactName($organization->getContactName());
            $person->setContactPhone($organization->getContactPhone());
        }else {
            $person->setUridical(false);
            $person->setContactName($user->getFullname());
            $person->setContactPhone($user->getPhone());
            $person->setContactEmail($user->getEmail());

            $person->setSeries($user->getSeries());
            $person->setNumber($user->getNumber());
        }

        return $person;
    }

    public function mapPersonToActor(Person $person, $user)
    {
        $data = [
            'id' => $person->getExternalId(),
            'ext_id' => $person->getId(),
            'user' => $person->getUser()->getExternalId(),
            'company' => null,
            'person' => null,
        ];
        if ($person) {
            if ($person->getUridical() > 0) {
                $data['company'] = [
                    'type' => $person->getType(),
                    'name' => $person->getCompanyName(),
                    'vat' => $person->getVat(),
                    'inn' => $person->getInn(),
                    'kpp' => '',
                    'bic' => '',
                    'account_no' => '',
                    'ceo' => '',
                    'contact_name' => $person->getContactName(),
                    'contact_phone' => $person->getContactPhone(),
                    'contact_email' => $person->getContactEmail(),
                    'address' => '',
                    'verified' => null
                ];
            } else {
                $data['person'] = [
                    'name' => $person->getContactName(),
                    'inn' => '',
                    'vat' => $person->getVat(),
                    'birth_date' => null,
                    'identity' => [
                        'type' => 'passport',
                        'no' => $person->getSeries().$person->getNumber(),
                        'issued' => null,
                        'issued_by' => '',
                    ],
                    'phone' => $person->getContactPhone(),
                    'email' => $person->getContactEmail(),
                ];
            }
        }

        return $data;
    }





}
