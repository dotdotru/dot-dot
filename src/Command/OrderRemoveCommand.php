<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Organization\Organization;
use App\Entity\Promocode;
use App\Order\Service\RearrangePromocodeService;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Organization\OrganizationRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;

use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Date;

class OrderRemoveCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:orders.remove')
            ->setDescription('remove orders')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $currentAt = new \DateTime();

        /** @var EntityManager $entityManager */
        $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $entityManager->getRepository(Order::class);
        $orders = $orderRepository->getByNoCreate();

        /** @var Container $container */
        $container = $this->getApplication()->getKernel()->getContainer();

        /** @var RearrangePromocodeService $rearangePromocodeService */
        $rearangePromocodeService = $container->get('app.order.rearrange_promocode');

        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->isRemoveExactStatus()) {
                $output->writeln($order->getId());
                $diffInSeconds = $currentAt->getTimestamp() - $order->getCreatedAt()->getTimestamp();

                if ($diffInSeconds > 604800) {
                    $order->setRemove(true);
                    $entityManager->persist($order);
                }
                $output->writeln($order->getId().' '.round($diffInSeconds / 60).' мин');
            }

            $entityManager->flush();
        }
    }


}
