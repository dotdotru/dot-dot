<?php

namespace App\Command;

use App\Entity\BaseEvent;
use App\Repository\BaseEventRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\BaseEvent\BaseEventMapperService;
use App\Service\VerificationService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class BaseEventMapCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:event.map')
            ->setDescription('base event map')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var BaseEventRepository $repository */
        $repository = $entityManager->getRepository(BaseEvent::class);

        /** @var BaseEvent $events */
        $events = $repository->getByStatus([BaseEvent::STATUS_CREATE, BaseEvent::STATUS_ERROR]);

        /** @var BaseEventMapperService $service */
        $baseEventMapperService = $container->get('app.base_event_map');

        /** @var BaseEvent $event */
        foreach ($events as $event) {

			try {
				$entity = $baseEventMapperService->map($event);
				
				if ($entity) {
					$entityManager->persist($entity);
					$event->setStatus(BaseEvent::STATUS_SUCCESS);
				} else {
					$event->setStatus(BaseEvent::STATUS_ERROR);
				}

				$entityManager->persist($event);
				$entityManager->flush();
				
			} catch (\Exception $e) {
				echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
			}
        }
    }
}
