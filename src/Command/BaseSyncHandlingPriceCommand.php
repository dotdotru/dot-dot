<?php

namespace App\Command;

use App\Entity\Base;
use App\Entity\BaseEvent;
use App\Repository\BaseEventRepository;
use App\Service\InsuranceService;
use App\Service\Integration\HandlingPrice\GetHandlingPrices;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\BaseMapperService;
use App\Service\VerificationService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class BaseSyncHandlingPriceCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:base.sync.handnlig_price')
            ->setDescription('get base sync')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var \App\Service\Integration\IntegrationBaseService $caller */
        $caller = $container->get('app.integration.caller');

        $handlingPriceRepository = $entityManager->getRepository(Base\HandlingPrice::class);


        $pricesRequest = new GetHandlingPrices();
        $data = $caller->exec($pricesRequest);

        dump($data);

        if ($data['success'] && !empty($data['data'])) {
            $priceIds = [];
            foreach ($data['data'] as $price) {
                $priceIds[] = $price['id'];
                $weights = trim($price['weight'], '()[]');
                $weights = explode(',', $weights);

                $handlingPrice = $handlingPriceRepository->findOneBy(['externalId' => $price['id']]);
                if (!$handlingPrice) {
                    $handlingPrice = new Base\HandlingPrice();
                    $entityManager->persist($handlingPrice);
                }

                $handlingPrice->setPrice($price['price']);
                $handlingPrice->setWeightFrom($weights[0] ? $weights[0] : null);
                $handlingPrice->setWeightTo($weights[1] ? $weights[1] : null);
                $handlingPrice->setExternalId($price['id']);

                dump($handlingPrice);
            }

            $entityManager->flush();

            // Удаляем несуществующие в базе цены
            $handlingPrices = $handlingPriceRepository->findAll();
            foreach ($handlingPrices as $handlingPrice) {
                if (!in_array($handlingPrice->getExternalId(), $priceIds)) {
                    $entityManager->remove($handlingPrice);
                }
            }

            $entityManager->flush();
        }


        return;
    }
}




















