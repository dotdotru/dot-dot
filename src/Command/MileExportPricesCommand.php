<?php

namespace App\Command;


use App\Entity\City;
use App\Entity\Shipping\ShippingIpBlock;
use App\Entity\Shipping\ShippingIpPrice;
use App\Service\Integration\IntegrationBaseService;
use App\Service\Integration\Mile\DeleteMilePrice;
use App\Service\Integration\Mile\GetMilePrices;
use App\Service\Integration\Mile\GetMileTariffs;
use App\Service\Integration\Mile\SaveMilePrice;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MileExportPricesCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:miles.prices.export')
            ->setDescription('export prices')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $em = $container->get('doctrine')->getManager();

        /** @var IntegrationBaseService $caller */
        $caller = $container->get('app.integration.caller');

        $cities = $em->getRepository(City::class)->findAll();

        $mileTariffs = new GetMileTariffs();
        $tariffs = $caller->exec($mileTariffs)['data'];

        $distances = ['10', '20', '30', '40', '50'];
        $weights = ['100', '250', '500', '1000', '1500', '3000', '5000'];


        /** @var City $city */
        foreach ($cities as $city) {
            $baseCity = $em->getRepository(\App\Entity\Base\City::class)->findOneBy(['title' => $city->getTitle()]);
            if (!$baseCity) continue;

            $tariffsEntities = $city->getPriceBlocks();
            if (empty($tariffsEntities)) {
                continue;
            }

            /** @var ShippingIpBlock $tariffsEntity */
            foreach ($tariffsEntities as $tariffsEntity) {
                $tariffId = null;

                foreach ($tariffs as $tariff) {

                    if ($tariffsEntity->getHydroboard() == $tariff['tail_lift'] && $tariffsEntity->getCountLoader() == $tariff['loaders']) {
                        $tariffId = $tariff['id'];
                    }
                }

                if ($tariffId) {
                    $pricesEntities = $tariffsEntity->getPrices();

                    $milePrices = new GetMilePrices();
                    $milePrices->setCityId($baseCity->getExternalId())
                        ->setTariff($tariffId);

                    $prices = $caller->exec($milePrices)['data'];
                    foreach ($prices as $price) {
                        $deletePrice = new DeleteMilePrice();
                        $deletePrice->setId($price['id']);

                        $caller->exec($deletePrice);
                    }


                    /** @var ShippingIpPrice $pricesEntity */
                    foreach ($pricesEntities as $pricesEntity) {
                        if ($pricesEntity->getPrice()) {
                            $distance = $this->range($distances, $pricesEntity->getDistantion());
                            $weight = $this->range($weights, $pricesEntity->getWeightTo(), true);
                            $priceValue = $pricesEntity->getPrice();
                            $perKm = 0;

                            if ($pricesEntity->getPriceAfterLast()) {
                                $maxDistance = 0;
                                $priceValue = null;

                                /** @var ShippingIpPrice $tmpPricesEntity */
                                foreach ($pricesEntities as $tmpPricesEntity) {
                                    if ($pricesEntity->getWeightTo() == $tmpPricesEntity->getWeightTo() && !$tmpPricesEntity->getPriceAfterLast() && $tmpPricesEntity->getPrice()) {
                                        $maxDistance = $tmpPricesEntity->getDistantion();
                                        $priceValue = $tmpPricesEntity->getPrice();
                                    }
                                }

                                if (!$maxDistance || !$priceValue) {
                                    continue;
                                }

                                $perKm = $pricesEntity->getPrice();
                                $distance = '(' . $maxDistance . ',)';
                            }

                            $savePrice = new SaveMilePrice();

                            $savePrice->setCityId($baseCity->getExternalId());
                            $savePrice->setTarifId($tariffId);
                            $savePrice->setDistance($distance);
                            $savePrice->setWeight($weight);
                            $savePrice->setPrice($priceValue);
                            $savePrice->setPerKm($perKm);

                            $caller->exec($savePrice);
                            dump('SAVE PRICE ' . $distance . ' ' . $weight . ' ' . $baseCity->getExternalId() . ' ' . $tariffId);
                        }
                    }




                }
            }
        }
    }

    protected function range ($array, $item, $lastRange = true) {
        $key = array_search($item, $array);

        if ($key === -1) {
            return null;
        }
        if ($key - 1 < 0) {
            return '(0,' . $item . ']';
        }
        if (!$lastRange && $key == count($array) - 1) {
            return '(' . $item . ',]';
        }
        return '(' . $array[$key - 1] . ',' . $item . ']';
    }



}
