<?php

namespace App\Command\Wms;

use App\Entity\File;
use App\Entity\Stock;
use App\Entity\Base\Stock as BaseStock;
use App\Entity\Wms\WmsOrder;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\FileUpdateService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\FileMapperService;
use Symfony\Component\Console\Helper\ProgressBar;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class WmsOrderFileUpdateCommand extends Command
{
    use LockableTrait;

    protected $count = 100;

    protected function configure()
    {
        $this
            ->setName('app:wms.order.file.update')
            ->setDescription('update wms order files')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        session_start();

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $container->get('app.integration_base');

        /** @var FileUpdateService $fileUpdateService */
        $fileUpdateService = $container->get('app.file.update');

        /** @var EntityManager $entityManager */
        $entityManager = $doctrine->getManager();

        /** @var WmsOrderRepository $wmsOrderRepository */
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
        $baseStockRepository = $entityManager->getRepository(BaseStock::class);

        $progressBar = new ProgressBar($output, $wmsOrderRepository->count([]));
        $progressBar->start();

        $page = 0;
        $offset = $page * $this->count;
        $wmsOrders = $wmsOrderRepository->findBy([], ['id' => 'desc'], $this->count, $offset);

        while ($wmsOrders) {
            /** @var WmsOrder $wmsOrder */
            foreach ($wmsOrders as $wmsOrder) {
                $progressBar->advance();
                if (!$wmsOrder->getStockTo()) continue;

                $baseStock = $baseStockRepository->findOneBy(['externalId' => $wmsOrder->getStockTo()->getId()]);

                if (!$baseStock) {
                    continue;
                }

                $integrationBaseService->authByStock($baseStock);
                $resultDocuments = $integrationBaseService->whGetOrderDocuments($wmsOrder->getExternalId());

                if ($resultDocuments[IntegrationBaseService::RESULT_SUCCESS] && $resultDocuments['data']) {
                    $files = $fileUpdateService->wmsMapListDocuments($resultDocuments['data'], $wmsOrder->getFiles());

                    /** @var File $file */
                    foreach ($files as $file) {
                        if (!$wmsOrder->getFileByExtId($file->getExternalId())) {
                            $wmsOrder->addFile($file);
                        }
                    }
                }

                $entityManager->persist($wmsOrder);
                $entityManager->flush();
            }

            $page++;
            $offset = $page * $this->count;
            $wmsOrders = $wmsOrderRepository->findBy([], ['id' => 'desc'], $this->count, $offset);
        }

        $progressBar->finish();
    }
}
