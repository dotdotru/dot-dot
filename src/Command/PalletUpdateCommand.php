<?php

namespace App\Command;

use App\Entity\Batch;
use App\Entity\Order;
use App\Entity\Pallet;
use App\Entity\Wms\WmsBatch;
use App\Entity\Wms\WmsOrder;
use App\Entity\Wms\WmsPallet;
use App\Repository\BatchRepository;
use App\Repository\OrderRepository;
use App\Repository\Wms\WmsBatchRepository;
use App\Repository\Wms\WmsOrderRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\Mapper\OrderMapperService;
use App\Service\OrderUpdateService;
use App\Service\VerificationService;
use App\Service\Wms\WmsBatchUpdateService;
use App\Service\Wms\WmsOrderUpdateService;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class PalletUpdateCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:pallet.update')
            ->setDescription('update pallets')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        $wmsPalletRepository = $doctrine->getRepository(WmsPallet::class);

        $wmsPallets = $wmsPalletRepository->findAll();

        foreach ($wmsPallets as $wmsPallet) {
            var_dump($wmsPallet->getExternalId());
            foreach ($wmsPallet->getPackages() as $package) {
                $externalId = $package->getExternalId();
                $package->setExternalId($externalId);
                $entityManager->persist($package);
                $entityManager->flush();
            }
        }

    }
}
