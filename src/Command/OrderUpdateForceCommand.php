<?php

namespace App\Command;

use App\Entity\InsuredQueue;
use App\Entity\Order;
use App\Entity\Wms\WmsOrder;
use App\Repository\OrderRepository;
use App\Service\Integration\Update\MileUpdate;
use App\Service\IntegrationBaseService;
use App\Service\OrderUpdateService;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderUpdateForceCommand extends Command
{
    use LockableTrait;


    protected function configure()
    {
        $this
            ->setName('app:orders.update.force')
            ->setDescription('update orders')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $doctrine->getRepository(Order::class);
        $wmsOrderRepository = $doctrine->getRepository(WmsOrder::class);

        /** @var IntegrationBaseService $integrationBaseService */
        $integrationBaseService = $container->get('app.integration_base');

        /** @var OrderUpdateService $orderUpdateService */
        $orderUpdateService = $container->get('app.order.update');

        /** @var MileUpdate $mileUpdateService */
        $mileUpdateService = $container->get('app.service.integration.mile.update');

        $page = 1;
        $count = 50;


        $ordersData = $integrationBaseService->getOrders($page, $count);

        if ($ordersData['data'] && $ordersData['data'] != null) {
            foreach ($ordersData['data'] as $orderData) {
                $output->writeln($orderData['ext_id']);
                $order = $orderRepository->findOneBy(['id' => $orderData['ext_id']]);

                if (!$order) {
                    $order = $orderRepository->findOneBy(['externalId' => $orderData['id']]);
                }

                $newOrder = false;
                if (!$order) {
                    $order = new Order();
                    $newOrder = true;
                }

                if ($order->isRemove()) {
                    continue;
                }

                $order = $orderUpdateService->updateOrder($order, $orderData);

                $entityManager->persist($order);
                $entityManager->flush();


                // Обновляем данные доставки
                $mileUpdateService->update($orderData['id'], $orderData['miles']);
                $entityManager->flush();


            }
            $entityManager->flush();
        }
    }
}
