<?php

namespace App\Command;

use App\Entity\Base;
use App\Entity\BaseEvent;
use App\Repository\BaseEventRepository;
use App\Service\InsuranceService;
use App\Service\IntegrationBaseService;
use App\Service\MailService;
use App\Service\Mapper\BaseMapperService;
use App\Service\VerificationService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class BaseSyncDirectoriesCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:base.sync')
            ->setDescription('get base sync')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $container =  $this->getApplication()->getKernel()->getContainer();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine')->getManager();

        /** @var BaseMapperService $baseMapperService */
        $baseMapperService = $container->get('app.base.mapper');

        /** @var IntegrationBaseService $service */
        $service = $container->get('app.integration_base');

        $baseDirectionRepository = $entityManager->getRepository(Base\Direction::class);
        $baseStockRepository = $entityManager->getRepository(Base\Stock::class);

        $list = $service->getCities();
        if (!empty($list['data'])) {
            foreach ($list['data'] as $dataItem) {
                $item = $baseMapperService->mapBaseToCity($dataItem);

                $entityManager->persist($item);
                $entityManager->flush();
            }
        }

        $list = $service->getRoutes();
        if (!empty($list['data'])) {
            foreach ($list['data'] as $dataItem) {
                $item = $baseMapperService->mapBaseToDirection($dataItem);

                $entityManager->persist($item);
                $entityManager->flush();

            }
        }

        $directions = $baseDirectionRepository->findAll();
        /** @var Base\Direction $direction */
        foreach ($directions as $direction) {
            $list = $service->getPrices($direction->getExternalId());
            if (!empty($list['data'])) {
                foreach ($list['data'] as $dataItem) {
                    $item = $baseMapperService->mapBaseToDirectionPrice($dataItem);

                    $found = false;
                    foreach ($direction->getDirectionPrices() as $directionPrice) {
                        if ($directionPrice->getExternalId() == $item->getExternalId()) {
                            $found = true;
                        }
                    }

                    if (!$found) {
                        $direction->addDirectionPrice($item);
                    }
                }
                $entityManager->persist($direction);
                $entityManager->flush();
            }
        }

        $list = $service->getWarehouses();
        if (!empty($list['data'])) {
            foreach ($list['data'] as $dataItem) {
                $item = $baseMapperService->mapBaseToStock($dataItem);

                $entityManager->persist($item);
                $entityManager->flush();

            }
        }

        $stocks = $baseStockRepository->findAll();
        foreach ($stocks as $stock) {
            if ($stock->getExternalId()) {
                $list = $service->getWarehouseWorkingHours($stock->getExternalId());
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $dataItem) {
                        $item = $baseMapperService->mapBaseToStockWorkingHours($dataItem);
                        $item->setStock($stock);
                        $entityManager->persist($item);
                        $entityManager->flush();

                    }
                }
            }
        }

        foreach ($stocks as $stock) {
            if ($stock->getExternalId()) {
                $list = $service->getWarehouseWorkingExclusion($stock->getExternalId());
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $dataItem) {
                        $item = $baseMapperService->mapBaseToWorkingCalendar($dataItem);
                        $item->setStock($stock);
                        $entityManager->persist($item);
                        $entityManager->flush();
                    }
                }
            }
        }

    }
}