<?php

namespace App\Command;


use App\Entity\BatchMile;
use App\Entity\Organization\OrganizationDriver;
use App\Entity\Shipping;
use App\Entity\Stock;
use App\Entity\Wms\WmsBatchMover;
use App\Entity\Wms\WmsOrder;
use App\Service\Integration\IntegrationBaseService;
use App\Service\Integration\Mile\MileBatch;
use App\Service\Wms\WmsOrderUpdateService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MileBatchUpdateCommand extends Command
{
    use LockableTrait;

    protected function configure()
    {
        $this
            ->setName('app:miles-batches.update')
            ->setDescription('update batches')
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        $container = $this->getApplication()->getKernel()->getContainer();
        $entityManager = $container->get('doctrine')->getManager();
        $caller = $container->get('app.integration.caller');

        // @todo пока так, потом будем брать напрямую с базы
        $batchMileRepository = $entityManager->getRepository(BatchMile::class);
        $wmsBatchMileRepository = $entityManager->getRepository(WmsBatchMover::class);
        $shippingRepository = $entityManager->getRepository(Shipping::class);
        $wmsOrderRepository = $entityManager->getRepository(WmsOrder::class);
        $organizationDriverRepository = $entityManager->getRepository(OrganizationDriver::class);
        $baseStockRepository = $entityManager->getRepository(\App\Entity\Base\Stock::class);

        /** @var WmsOrderUpdateService $wmsOrderUpdateService */
        $wmsOrderUpdateService = $container->get('app.wmsorder.update');
        $integrationBaseService = $container->get('app.integration_base');


        // @todo Добавить фильтр по статусам
        $mileBatches = $batchMileRepository->findBy([], ['id' => 'DESC'], 30);
        $now = new \DateTime();

        /** @var BatchMile $mileBatch */
        foreach ($mileBatches as $mileBatch) {
            if ($mileBatch->getReserveId()) {
                $batchInfoRequest = new MileBatch();
                $batchInfoRequest->setBatchId($mileBatch->getReserveId());

                $info = $caller->exec($batchInfoRequest);

                if ($info['success']) {
                    $batchData = $info['data'];
                    /** @var WmsBatchMover $wmsBatchMile */
                    $wmsBatchMile = $wmsBatchMileRepository->findOneBy(['externalId' => $batchData['id']]);

                    // Миля была отменена
                    if (empty($info['data']['miles'])) {
                        if ($wmsBatchMile) {
                            $wmsBatchMile->setStatus(WmsBatchMover::STATUS_REJECTED);
                        }
                        $mileBatch->setStatus(WmsBatchMover::STATUS_REJECTED);

                        continue;
                    }

                    if (!$wmsBatchMile) {
                        $wmsBatchMile = new WmsBatchMover();
                        $wmsBatchMile->setExternalId($batchData['id']);

                        // externalId мили не может поменяться, только данные, либо партия будет отменена, поэтому заполняем только при создании
                        foreach ($batchData['miles'] as $mileData) {
                            $shipping = $shippingRepository->findOneBy(['externalId' => $mileData['id'], 'reject' => false]);

                            if ($shipping) {
                                $wmsBatchMile->setShipping($shipping);
                                $mileBatch->setShippings([$shipping]);
                            }
                        }

                        // Сохраняем только те, где есть мили
                        if (!$wmsBatchMile->getShipping()) {
                            continue;
                        }
                        $entityManager->persist($wmsBatchMile);
                    } else {
                        // костыль из-за историчности миль, в большинстве случаев багов быть не должно
                        foreach ($batchData['miles'] as $mileData) {
                            $shipping = $shippingRepository->findOneBy(['externalId' => $mileData['id'], 'reject' => false]);
                            if ($shipping) {
                                $wmsBatchMile->setShipping($shipping);
                                $mileBatch->setShippings([$shipping]);
                            }
                        }
                    }

                    // Для последней мили база не поддерживает смену статусов после статуса "в пути", делаем костылем
                    if ($wmsBatchMile->getType() != Shipping::TYPE_DELIVER || !in_array($mileBatch->getStatus(), [BatchMile::STATUS_NOT_PAID, BatchMile::STATUS_PAYED])) {
                        $mileBatch->setStatus($batchData['status']);
                        $wmsBatchMile->setStatus($batchData['status']);
                    }

                    $mileBatch->setPrice($batchData['cost']['total']);

                    // Пробрасываем файлы из вмс в лк
                    $wmsTnFile = $wmsBatchMile->getFileByGroup('tn');
                    $tnFile = $mileBatch->getFileByGroup('tn');
                    if (!$tnFile && $wmsTnFile) {
                        $mileBatch->replaceFile($wmsTnFile);
                    }

                    $wmsBatchMile->setType($batchData['type']);
                    $wmsBatchMile->setPrice($batchData['cost']['total']);
                    $wmsBatchMile->setCarrier($mileBatch->getCarrier());

                    // @todo брать из базы
                    if ($mileBatch->getDriver()) {
                        $wmsBatchMile->setDriver($mileBatch->getDriver());
                    }

                    // Данные в нашей базе приоритетней
                    if (!$wmsBatchMile->getPaid()) {
                        $wmsBatchMile->setPaid($batchData['paid']);
                    }

                    if (!empty($batchData['driver'])) {
                        $driver = $organizationDriverRepository->find($batchData['driver']['id']);

                        if ($driver && $wmsBatchMile->getShipping() && !$wmsBatchMile->getShipping()->getDriver()) {
                            $wmsBatchMile->getShipping()->setDriver($driver);
                        }
                    }

                    try {
                        $entityManager->flush();
                    } Catch (\Exception $e) {
                        dump($e);
                    }
                } else {
                    // @todo пока костыль

                    $wmsBatchMile = $wmsBatchMileRepository->findOneBy(['externalId' => $mileBatch->getReserveId()]);
                    if ($wmsBatchMile) {
                        $wmsBatchMile->setStatus(BatchMile::STATUS_SUSPENDED);
                        $wmsBatchMile->setStopSend(true);

                        if ($wmsBatchMile->getShipping()) {
                            $wmsBatchMile->getShipping()->setDriver(null);
                        }
                    }

                }

            }
        }

        $entityManager->flush();

        return true;
    }
}
