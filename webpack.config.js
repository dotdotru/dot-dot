var Encore = require('@symfony/webpack-encore');
var path = require('path');
Encore
// directory where compiled assets will be stored
    .setOutputPath('public/build_pre/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    .enableSassLoader()
    .enableLessLoader()
    .enableStylusLoader()
    .enablePostCssLoader()


    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .autoProvidejQuery()
    .enableVueLoader()

    .addEntry('js/main', './assets/js/main.js')
    .addEntry('js/wms', './assets/js/wms.js')
    .addEntry('js/widget-calc', './assets/js/widget-calc.js')

    .addStyleEntry('css/main', './assets/css/main.scss')
    .addStyleEntry('css/wms', './assets/css/wms.scss')
    .addStyleEntry('css/widget-calc', './assets/css/widget-calc.scss')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())



    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    .addLoader({
        test: /\.svg$/,
        use: [
            {
                loader: 'url-loader'
            }
        ]
    })
    .addLoader({
        test: /\.(png|jpg)$/,
        use: [
            {
                loader: 'url-loader'
            }
        ]
    })
;

var config = Encore.getWebpackConfig();

var fileLoader = config.module.rules.find(rule => rule.loader === 'file-loader');
fileLoader.exclude = path.resolve('./assets/icons/');
module.exports = config;
